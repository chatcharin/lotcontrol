package com.time;
import java.util.Date;



public class timeCurrent {
	
	 Date date = new Date();
	  
     // display time and date using toString()
    
	private long  timeCurrent  = System.currentTimeMillis();

	/**
	 * @return the timeCurrent
	 */
	public long getTimeCurrent() {
		return timeCurrent;
	}

	/**
	 * @param timeCurrent the timeCurrent to set
	 */
	public void setTimeCurrent(long timeCurrent) {
		this.timeCurrent = timeCurrent;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date; 
	} 
}
