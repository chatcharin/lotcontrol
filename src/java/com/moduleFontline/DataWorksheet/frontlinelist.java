/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moduleFontline.DataWorksheet;

import com.tct.data.DetailFl;
import com.tct.data.Details;

/**
 *
 * @author Tawat Saekue
 */
public class frontlinelist {

    private Details details;
    private DetailFl detailFl;
    private String startorder;
    private String finishorder;
    private String mc;
    private String barcode;
    private String status;
    private String process;
    private String model;
    private String bobbin;
    private String ordernumber;

    public frontlinelist(Details details, DetailFl detailFl, String startorder, String finishorder, String mc,
            String barcode, String status, String process, String model, String bobbin, String ordernumber) {
        this.detailFl = detailFl;
        this.details = details;
        this.startorder = startorder;
        this.finishorder = finishorder;
        this.mc = mc;

        this.status = status;
        this.barcode = barcode;
        this.process = process;
        this.model = model;
        this.bobbin = bobbin;
        this.ordernumber = ordernumber;
    }

    public DetailFl getDetailFl() {
        return detailFl;
    }

    public void setDetailFl(DetailFl detailFl) {
        this.detailFl = detailFl;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

    public String getFinishorder() {
        return finishorder;
    }

    public void setFinishorder(String finishorder) {
        this.finishorder = finishorder;
    }

    public String getMc() {
        return mc;
    }

    public void setMc(String mc) {
        this.mc = mc;
    }

    public String getStartorder() {
        return startorder;
    }

    public void setStartorder(String startorder) {
        this.startorder = startorder;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getBobbin() {
        return bobbin;
    }

    public void setBobbin(String bobbin) {
        this.bobbin = bobbin;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
