/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moduleFontline.DataWorksheet;

/**
 *
 * @author The_Boy_Cs
 */
public class barcode {

    private int num;
    private String barcode;
    private boolean selectbarcode;
    private String barcodepicurl;
    private String qrcodepicurl;
    private String type;

    public barcode(int num, String barcode, boolean selectbarcode, String barcodepicurl, String type) {

        this.barcode = barcode;
        this.num = num;
//        this.barcodepicurl = barcodepicurl + "b/" + barcode + ".png";
//        this.qrcodepicurl = barcodepicurl + barcode + ".png";
        
        this.barcodepicurl = barcodepicurl + "1d/" + barcode + ".png";
        this.qrcodepicurl = barcodepicurl +"2d/"+ barcode + ".png";
        
        this.selectbarcode = selectbarcode;
        this.type = type;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getBarcodepicurl() {
        return barcodepicurl;
    }

    public void setBarcodepicurl(String barcodepicurl) {
        this.barcodepicurl = barcodepicurl;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getQrcodepicurl() {
        return qrcodepicurl;
    }

    public void setQrcodepicurl(String qrcodepicurl) {
        this.qrcodepicurl = qrcodepicurl;
    }

    public boolean isSelectbarcode() {
        return selectbarcode;
    }

    public void setSelectbarcode(boolean selectbarcode) {
        this.selectbarcode = selectbarcode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}