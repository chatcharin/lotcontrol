/**
 *
 */
package com.moduleFontline.DataWorksheet;

import java.util.Date;

/**
 * @author The_Boy_Cs
 *
 */
public class detailProcess {

    private String processnumber;
    private Date startdate;
    private Date finishdate;
    private Date starthold;
    private Date stophold;
    private String mc;
    private String processname;
    private String shift;
    private int inputqty;
    private int outputqty;
    private int ngqty;
    private String ordernumber;
    private String employeeno;

    public detailProcess() {
    }

    public detailProcess(String processnumber, Date startdate, Date finishdate, Date starthold, Date stophold, String mc,
            String processname, String shift, int inputqty, int outputqty, int ngqty, String ordernumber, String employeeno) {
        this.processnumber = processnumber;
        this.startdate = startdate;
        this.finishdate = finishdate;
        this.starthold = starthold;
        this.stophold = stophold;
        this.mc = mc;

        this.processname = processname;
        this.shift = shift;
        this.inputqty = inputqty;
        this.outputqty = outputqty;
        this.ngqty = ngqty;
        this.ordernumber = ordernumber;

        this.employeeno = employeeno;
    }

    public String getProcessnumber() {
        return processnumber;
    }

    public void setProcessnumber(String processnumber) {
        this.processnumber = processnumber;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getFinishdate() {
        return finishdate;
    }

    public void setFinishdate(Date finishdate) {
        this.finishdate = finishdate;
    }

    public Date getStarthold() {
        return starthold;
    }

    public void setStarthold(Date starthold) {
        this.starthold = starthold;
    }

    public Date getStophold() {
        return stophold;
    }

    public void setStophold(Date stophold) {
        this.stophold = stophold;
    }

    public String getMc() {
        return mc;
    }

    public void setMc(String mc) {
        this.mc = mc;
    }

    public String getProcessname() {
        return processname;
    }

    public void setProcessname(String processname) {
        this.processname = processname;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public int getInputqty() {
        return inputqty;
    }

    public void setInputqty(int inputqty) {
        this.inputqty = inputqty;
    }

    public int getOutputqty() {
        return outputqty;
    }

    public void setOutputqty(int outputqty) {
        this.outputqty = outputqty;
    }

    public int getNgqty() {
        return ngqty;
    }

    public void setNgqty(int ngqty) {
        this.ngqty = ngqty;
    }

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    public String getEmployeeno() {
        return employeeno;
    }

    public void setEmployeeno(String employeeno) {
        this.employeeno = employeeno;
    }
    
}