/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moduleFontline.bean;

import com.appCinfigpage.bean.userLogin;
import com.moduleFontline.DataWorksheet.detailProcess;
import com.tct.data.*;
import com.tct.data.jpa.*;
import java.util.ArrayList;
import java.util.List;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;

/**
 *
 * @author The_Boy_Cs
 */
public class editWorksheet {

    public static Details currentdetails;
    public static DetailFl currentdetailfl;
    private List<SelectItem> selectItemPlant = null;
    private List<SelectItem> resinnamelist = null;
    private List<SelectItem> resintypelist = null;
    private List<SelectItem> producttypelist = null;
    private List<SelectItem> productcodenamelist = null;
    private ModelPoolJpaController modelPoolJpaController = null;
    public static ModelPool modelPool;

    public editWorksheet() {
        loadProcess();
    }

    public DetailFl getCurrentdetailfl() {
        return currentdetailfl;
    }

    public Details getCurrentdetails() {
        print("Series ## " + currentdetails.getModelseries());
        return currentdetails;
    }

    public void ChangPlantValue(ValueChangeEvent event) throws Exception {
        currentdetailfl.setPlat((String) event.getNewValue());
        print("Plant : " + currentdetailfl.getPlat());
    }

    public void ChangProductCodeNameValue(ValueChangeEvent event) {
        String productcodename = (String) event.getNewValue();
        if ("Prototype".equals(productcodename)) {
            currentdetailfl.setProductcode("P");
        }
        if ("New product introduction".equals(productcodename)) {
            currentdetailfl.setProductcode("N");
        }
        if ("Special built request".equals(productcodename)) {
            currentdetailfl.setProductcode("S");
        }
        if ("Mass production".equals(productcodename)) {
            currentdetailfl.setProductcode("M");
        }
        currentdetailfl.setProductcodeName(productcodename);
        print("Product code name " + productcodename);
        print("Product code : " + currentdetailfl.getProductcode());
    }

    private void print(String str) {
        System.out.println(str);
    }

    public List<SelectItem> getSelectItemPlant() {
        if (this.selectItemPlant == null || this.selectItemPlant.isEmpty()) {
            selectItemPlant = new ArrayList<SelectItem>();
            selectItemPlant.add(new SelectItem("101", "Tokyo Coil Engineering(Thailand)"));
            selectItemPlant.add(new SelectItem("102", "Tokyo Coil Engineering(LAOS)"));
            selectItemPlant.add(new SelectItem("103", "DCE"));
            selectItemPlant.add(new SelectItem("104", "HTD"));
        }
        return selectItemPlant;
    }

    public List<SelectItem> getResinnamelist() {
        if (this.resinnamelist == null || this.resinnamelist.isEmpty()) {
            resinnamelist = new ArrayList<SelectItem>();
            resinnamelist.add(new SelectItem("FC110B", "FC110B"));
        }
        return resinnamelist;
    }

    public List<SelectItem> getResintypelist() {
        if (this.resintypelist == null || this.resintypelist.isEmpty()) {
            resintypelist = new ArrayList<SelectItem>();
            resintypelist.add(new SelectItem("Vergin", "Vergin"));
            resintypelist.add(new SelectItem("Recycle#1", "Recycle#1"));
            resintypelist.add(new SelectItem("Recycle#2", "Recycle#2"));
            resintypelist.add(new SelectItem("Recycle#3", "Recycle#3"));
        }
        return resintypelist;
    }

    public List<SelectItem> getProductcodenamelist() {
        if (this.productcodenamelist == null || this.productcodenamelist.isEmpty()) {
            productcodenamelist = new ArrayList<SelectItem>();
            productcodenamelist.add(new SelectItem("Prototype", "Prototype"));
            productcodenamelist.add(new SelectItem("New product introduction", "New product introduction"));
            productcodenamelist.add(new SelectItem("Special built request", "Special built request"));
            productcodenamelist.add(new SelectItem("Mass production", "Mass production"));
        }
        return productcodenamelist;
    }

    public List<SelectItem> getProducttypelist() {
        if (producttypelist == null) {
            producttypelist = new ArrayList<SelectItem>();
            producttypelist.add(new SelectItem("T", "Trigger coil"));
            producttypelist.add(new SelectItem("TR", "Tranformer"));
            producttypelist.add(new SelectItem("PCB", "Print circuit board"));
        }
        return producttypelist;
    }
    private List<SelectItem> modelseries;
    private String series;

    public void changModelpoolValue(ValueChangeEvent event) {
        print("========== modelPool : " + event.getNewValue().toString() + " ======");
        try {
            String id = event.getNewValue().toString();
            modelPool = getModelPoolJpaController().findModelPool(id);
            print("= model name : " + modelPool.getModelName());
            loadProcess();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }
private List<SelectItem> selectlist;

    public List<SelectItem> getSelectlist() {
        if(selectlist == null){
            selectlist = new ArrayList<SelectItem>();
        }
        return selectlist;
    }

    public void setSelectlist(List<SelectItem> selectlist) {
        this.selectlist = selectlist;
    }

    public void changModelSeries(ValueChangeEvent event) {
        List<ModelPool> modellist = getModelPoolJpaController().findModelPoolBySeries(currentdetails.getModelseries());
        selectlist = null;
        print("----------------------------------------------");
        for (ModelPool model : modellist) {
            print("Model name : " + model);
            getSelectlist().add(new SelectItem(model.getId(), model.getModelName()));
        }
        print("----------------------------------------------");
    }

    private ModelPoolJpaController getModelPoolJpaController() {
        if (modelPoolJpaController == null) {
            modelPoolJpaController = new ModelPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return modelPoolJpaController;
    }

    public void ChangResinTypeValue(ValueChangeEvent event) {
        currentdetailfl.setResinType((String) event.getNewValue());
        print("Resintype" + currentdetailfl.getResinType());
    }

    public List<SelectItem> getModelseries() {
        if (modelseries == null) {
            List<String> list = getModelPoolJpaController().findModelPoolGroupBySeries();
            modelseries = new ArrayList<SelectItem>();
            for (String model : list) {
                print("Series : " + model);
                modelseries.add(new SelectItem(model, model));
            }
        }
        return modelseries;
    }

    public String getSeries() {
        return series;
    }

    private List<detailProcess> detailprocess = null;

    private void loadProcess() {
        print("=================================================================");
        try {
            detailprocess = null;
            print("LoadProcess : " + modelPool.getModelSeries());
            List<Md2pc> md2pclist = getMd2pcJpaController().findBymodelPoolId(modelPool);
            for (Md2pc md2pc : md2pclist) {
                print("= md2pc : " + md2pc.getMd2pcId());
                ProcessPool pcpool = md2pc.getProcessPoolIdProc();
                print("= Process : " + pcpool.getProcName());
                detailProcess dp = new detailProcess("", null, null, null, null, "", pcpool.getProcName().toString(), "", 0, 0, 0, currentdetails.getOrderNumber(), "");
                getDetailprocess().add(dp);
            }
            print("= Complete");
        } catch (Exception e) {
            print("= Ex : " + e);
        }
    }
    private Md2pcJpaController md2pcJpaController;

    private Md2pcJpaController getMd2pcJpaController() {
        if (md2pcJpaController == null) {
            md2pcJpaController = new Md2pcJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return md2pcJpaController;
    }

    public List<detailProcess> getDetailprocess() {
        if(detailprocess == null){
            detailprocess = new ArrayList<detailProcess>();
        }
        return detailprocess;
    }

    public void ChangProductTypeValue(ValueChangeEvent event) { // product type

        currentdetailfl.setProductType((String) event.getNewValue());
        print("Product Type : " + currentdetailfl.getProductType());
    }

    public List<SelectItem> getModelPoolsList() {
        List<ModelPool> modellist = getModelPoolJpaController().findModelPoolBySeries(currentdetails.getModelseries());
         selectlist = null;
        print("----------------------------------------------");
        for (ModelPool model : modellist) {
            print("Model name : " + model);
            getSelectlist().add(new SelectItem(model.getId(), model.getModelName()));
        }
        print("----------------------------------------------");
        return getSelectlist();
    }
    private LotControlJpaController lotControlJpaController = null;
    private LotControlJpaController getLotControlJpaController(){
        if(lotControlJpaController == null){
            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotControlJpaController;
    }
    private DetailsJpaController detailsJpaController = null;
    private DetailsJpaController getDetailsJpaController(){
        if(detailsJpaController == null){
            detailsJpaController = new DetailsJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailsJpaController;
    }
    private DetailFlJpaController detailFlJpaController = null;
    private DetailFlJpaController getDetailFlJpaController(){
        if(detailFlJpaController == null){
            detailFlJpaController = new DetailFlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailFlJpaController;
    }
    public void deleteAllWorksheet(ActionEvent event){
        print("=================================================================");
        try{
            List<LotControl> lotControllist = getLotControlJpaController().findByDetailsId(currentdetails);
            for(LotControl lotControl : lotControllist){
                lotControl.setDeleted(1);
                getLotControlJpaController().edit(lotControl);
            }
            currentdetails.setDeleted(1);
            getDetailsJpaController().edit(currentdetails);
            currentdetailfl.setDeleted(1);
            getDetailFlJpaController().edit(currentdetailfl);
            
            String link="pages/moduleFontLine/worksheet.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            userLogin.getSessionWorksheet().searchWorksheet();
        }catch(Exception e){
            print("= Error : "+e);
        }
    }
}
