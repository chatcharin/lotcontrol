/**
 *
 */
package com.moduleFontline.bean;

import com.moduleBackLine.Dataworksheet.workSheetDetail;
import com.tct.data.*;
import com.tct.data.jpa.*;
import com.tct.data.jsf.util.PaginationHelper;
import java.util.ArrayList;
import java.util.List;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.persistence.Persistence;

/**
 * @author The_Boy_Cs
 *
 */
public class worksheet {

    // search===================================================================
    private String ordernumber;
    private String typemodel;
    private String process;
    private String typepoppin;
    //==========================================================================
    private final String barcodeColum = "Barcode";
    private final String orderNumberColum = "Order Number";
    private final String typeModelColum = "Model";
    private final String typeBobbinColum = "Type Bobbin";
    private final String dateCreateColum = "Date Create";
    private final String processColum = "Process";
    private final String statusColum = "Status";
    public static List<workSheetDetail> worksheetlist = null;
    private List<DetailFl> detailflList = null;
    private Details details = null;

    public worksheet() {
//        loadWorkSheet();
    }

    private void print(String str) {
        System.out.println(str);
    }

//    private void loadWorkSheet() {
//        worksheetlist = null;
//        recreateModel();
//        print("=================================================================");
//        detailflList = getDetailFlJpacontroller().findDetailFlEntities();
//        if (detailflList.isEmpty()) {
//            getWorksheetList();
//        }
////        worksheet = new workSheetDetail[detailflList.size()];
//        for (DetailFl detailfl : detailflList) {
//            details = detailfl.getDetailIdDetail();
//            lotControllist = getLotControlJpaController().findByDetailsId(details);
//            for (LotControl lotControl : lotControllist) {
//                try {
//                    if (lotControl.getMainDataId() != null) {
//                        print("T");
//                        MainData mainData = getMainDataJpaController().findMainData(lotControl.getMainDataId());
//                        ProcessPool processPool = mainData.getIdProc();
//                        CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(mainData.getCurrentNumber());
//
//                        getWorksheetList().add(new workSheetDetail(details,lotControl.getBarcode(), detailfl.getOrderNo(), lotControl.getModelPoolId().getModelName(), detailfl.getTypeBb(), details.getDateCreate(), processPool.getProcName(), currentProcess.getStatus()));
//                        print("= Detail : " + details.getDetailName() + " :: Barcode : " + lotControl.getBarcode());
//                    } else {
//                        getWorksheetList().add(new workSheetDetail(details,lotControl.getBarcode(), detailfl.getOrderNo(), lotControl.getModelPoolId().getModelName(), detailfl.getTypeBb(), details.getDateCreate(), null, null));
//                        print("Null");
//                    }
//                } catch (Exception e) {
//                    print("= Error : " + e);
//                }
//            }
//        }
//        setWorksheet();
//    }
    public static DataModel<workSheetDetail> itemsworksheet;

    private void setWorksheet() {
        itemsworksheet = getPagination().createPageDataModel();
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    return getWorksheetList().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    print("PageFirst : " + getPageFirstItem());
                    print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (getWorksheetList().size() > 0) {
                        listmodel = new ListDataModel(getWorksheetList().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(getWorksheetList());
                    }
                    return listmodel;
                }
            };
        }
        return pagination;
    }
    public static PaginationHelper pagination;
    private CurrentProcessJpaController currentProcesslist = null;

    private CurrentProcessJpaController getCurrentProcessJpaController() {
        if (currentProcesslist == null) {
            currentProcesslist = new CurrentProcessJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return currentProcesslist;
    }

    private List<workSheetDetail> getWorksheetList() {
        if (worksheetlist == null) {
            worksheetlist = new ArrayList<workSheetDetail>();
        }
        return worksheetlist;
    }
    private MainDataJpaController mianDataJpaController = null;

    private MainDataJpaController getMainDataJpaController() {
        if (mianDataJpaController == null) {
            mianDataJpaController = new MainDataJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mianDataJpaController;
    }
    private List<LotControl> lotControllist = null;
    private LotControlJpaController lotControlJpaController = null;

    private LotControlJpaController getLotControlJpaController() {
        if (lotControlJpaController == null) {
            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotControlJpaController;
    }
    private DetailsJpaController detailJpacontroller = null;
    private DetailFlJpaController detailFlJpacontroller = null;

    public DetailFlJpaController getDetailFlJpacontroller() {
        if (detailFlJpacontroller == null) {
            detailFlJpacontroller = new DetailFlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailFlJpacontroller;
    }

    private DetailsJpaController getDetailJpaController() {
        if (detailJpacontroller == null) {
            detailJpacontroller = new DetailsJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailJpacontroller;
    }

    /**
     * @return the ordernumbercolum
     */
    public String getOrdernumbercolum() {
        return orderNumberColum;
    }

    /**
     * @return the typemodelcolum
     */
    public String getTypemodelcolum() {
        return typeModelColum;
    }

    /**
     * @return the typebobbincolum
     */
    public String getTypebobbincolum() {
        return typeBobbinColum;
    }

    /**
     * @return the datecreatecolum
     */
    public String getDatecreatecolum() {
        return dateCreateColum;
    }

    /**
     * @return the processcolum
     */
    public String getProcesscolum() {
        return processColum;
    }

    /**
     * @return the statuscolum
     */
    public String getStatuscolum() {
        return statusColum;
    }

    /**
     * @return the ordernumber
     */
    public String getOrdernumber() {
        return ordernumber;
    }

    /**
     * @param ordernumber the ordernumber to set
     */
    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    /**
     * @return the typemodel
     */
    public String getTypemodel() {
        return typemodel;
    }

    /**
     * @param typemodel the typemodel to set
     */
    public void setTypemodel(String typemodel) {
        this.typemodel = typemodel;
    }

    /**
     * @return the process
     */
    public String getProcess() {
        return process;
    }

    /**
     * @param process the process to set
     */
    public void setProcess(String process) {
        this.process = process;
    }

    /**
     * @return the typepoppin
     */
    public String getTypepoppin() {
        return typepoppin;
    }

    /**
     * @param typepoppin the typepoppin to set
     */
    public void setTypepoppin(String typepoppin) {
        this.typepoppin = typepoppin;
    }

    public void searchWorksheet() {
        print("================== Search By ====================================");
        if (ordernumber != null && !ordernumber.trim().isEmpty()) {
            print("= Order Number :: " + ordernumber);
            searchByOrderNumber();
        } else if (process != null && !process.trim().isEmpty()) {
            print("= Process :: " + process);
            searchByProcess();
        } else if (typemodel != null && !typemodel.trim().isEmpty()) {
            print("= Type Model :: " + typemodel);
            searchByModel();
        } else if (typepoppin != null && !typepoppin.trim().isEmpty()) {
            print("= Type Poppin :: " + typepoppin);
            searchByTypebobin();
        } else {
//            loadWorkSheet();
        }
    }
    private ProcessPoolJpaController processPoolJpaController = null;

    private ProcessPoolJpaController getProcessPoolJpaController() {
        if (processPoolJpaController == null) {
            processPoolJpaController = new ProcessPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return processPoolJpaController;
    }

    private void searchByProcess() {
        worksheetlist = null;
        recreateModel();
        print("=================================================================");
        List<ProcessPool> pcpools = getProcessPoolJpaController().findProcessPoolByName(process);
        if (pcpools.isEmpty()) {
            getWorksheetList();
        }
        for (ProcessPool pcpool : pcpools) {
            List<MainData> maindatas = getMainDataJpaController().findMainDataByProcess(pcpool);
            for (MainData maindata : maindatas) {
                LotControl lotControl = maindata.getLotControlId();
                Details detail = lotControl.getDetailsIdDetail();
                
                if (detail.getDetailName().equals("detail_fl")) {
                    DetailFl detailfl = getDetailFlJpacontroller().findDetailFlByDetails(detail);
                    CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(maindata.getCurrentNumber());
//                    getWorksheetList().add(new workSheetDetail(detail,lotControl.getBarcode(), detailfl.getOrderNo(), detail.getModel(), detailfl.getTypeBb(), detail.getDateCreate(), pcpool.getProcName(), currentProcess.getStatus()));
                    print("= Detail : " + detail.getDetailName() + " :: Barcode : " + maindata.getLotControlId().getBarcode());
                }
            }
        }
        setWorksheet();
    }

    private void searchByModel() {
        worksheetlist = null;
        recreateModel();
        print("=================================================================");
        detailflList = getDetailFlJpacontroller().findDetailFlByModel(typemodel);
        if (detailflList.isEmpty()) {
            getWorksheetList();
        }
        for (DetailFl detailfl : detailflList) {
            details = detailfl.getDetailIdDetail();
            lotControllist = getLotControlJpaController().findByDetailsId(details);
            for (LotControl lotControl : lotControllist) {
                try {
                    if (lotControl.getMainDataId() != null) {
                        print("T");
                        MainData mainData = getMainDataJpaController().findMainData(lotControl.getMainDataId());
                        ProcessPool processPool = mainData.getIdProc();
                        CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(mainData.getCurrentNumber());
//                        getWorksheetList().add(new workSheetDetail(details,lotControl.getBarcode(), detailfl.getOrderNo(), lotControl.getModelPoolId().getModelName(), detailfl.getTypeBb(), details.getDateCreate(), processPool.getProcName(), currentProcess.getStatus()));
                        print("= Detail : " + details.getDetailName() + " :: Barcode : " + lotControl.getBarcode());
                    } else {
//                        getWorksheetList().add(new workSheetDetail(details,lotControl.getBarcode(), detailfl.getOrderNo(), lotControl.getModelPoolId().getModelName(), detailfl.getTypeBb(), details.getDateCreate(), null, null));
                        print("Null");
                    }
                } catch (Exception e) {
                    print("= Error : " + e);
                }
            }
        }
        setWorksheet();
    }

    private void searchByTypebobin() {
        worksheetlist = null;
        recreateModel();
        print("=================================================================");
        detailflList = getDetailFlJpacontroller().findDetailFlByTypeBobbin(typepoppin);
        if (detailflList.isEmpty()) {
            getWorksheetList();
        }
        for (DetailFl detailfl : detailflList) {
            details = detailfl.getDetailIdDetail();
            lotControllist = getLotControlJpaController().findByDetailsId(details);
            for (LotControl lotControl : lotControllist) {
                try {
                    if (lotControl.getMainDataId() != null) {
                        print("T");
                        MainData mainData = getMainDataJpaController().findMainData(lotControl.getMainDataId());
                        ProcessPool processPool = mainData.getIdProc();
                        CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(mainData.getCurrentNumber());
//                        getWorksheetList().add(new workSheetDetail(details,lotControl.getBarcode(), detailfl.getOrderNo(), lotControl.getModelPoolId().getModelName(), detailfl.getTypeBb(), details.getDateCreate(), processPool.getProcName(), currentProcess.getStatus()));
                        print("= Detail : " + details.getDetailName() + " :: Barcode : " + lotControl.getBarcode());
                    } else {
//                        getWorksheetList().add(new workSheetDetail(details,lotControl.getBarcode(), detailfl.getOrderNo(), lotControl.getModelPoolId().getModelName(), detailfl.getTypeBb(), details.getDateCreate(), null, null));
                        print("Null");
                    }
                } catch (Exception e) {
                    print("= Error : " + e);
                }
            }
        }
        setWorksheet();
    }

    private void searchByOrderNumber() {
        worksheetlist = null;
        recreateModel();
        print("=================================================================");
        detailflList = getDetailFlJpacontroller().findDetailFlByOrderNumber(ordernumber);
        if (detailflList.isEmpty()) {
            getWorksheetList();
        }
//        worksheet = new workSheetDetail[detailflList.size()];
        for (DetailFl detailfl : detailflList) {
            details = detailfl.getDetailIdDetail();
            lotControllist = getLotControlJpaController().findByDetailsId(details);
            for (LotControl lotControl : lotControllist) {
                try {
                    if (lotControl.getMainDataId() != null) {
                        print("T");
                        MainData mainData = getMainDataJpaController().findMainData(lotControl.getMainDataId());
                        ProcessPool processPool = mainData.getIdProc();
                        CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(mainData.getCurrentNumber());

//                        getWorksheetList().add(new workSheetDetail(details,lotControl.getBarcode(), detailfl.getOrderNo(), lotControl.getModelPoolId().getModelName(), detailfl.getTypeBb(), details.getDateCreate(), processPool.getProcName(), currentProcess.getStatus()));
                        print("= Detail : " + details.getDetailName() + " :: Barcode : " + lotControl.getBarcode());
                    } else {
//                        getWorksheetList().add(new workSheetDetail(details,lotControl.getBarcode(), detailfl.getOrderNo(), lotControl.getModelPoolId().getModelName(), detailfl.getTypeBb(), details.getDateCreate(), null, null));
                        print("Null");
                    }
                } catch (Exception e) {
                    print("= Error : " + e);
                }
            }
        }
        setWorksheet();
    }

    public DataModel<workSheetDetail> getWorksheet() {
        return itemsworksheet;
    }

    private void recreateModel() {
        itemsworksheet = null;
        pagination = null;
    }

    public void next() {
        getPagination().nextPage();
        setWorksheet();
//        recreateModel();
//        loadWorkSheet();
//        return "List";
    }

    public void previous() {
        getPagination().previousPage();
        setWorksheet();
//        recreateModel();
//        loadWorkSheet();
//        return "List";
    }

    public int getAllPages() {
        int all = getPagination().getItemsCount();
        int pagesize = getPagination().getPageSize();
        return ((int) all / pagesize) + 1;
    }

    public String getBarcodeColum() {
        return barcodeColum;
    }
    
}
