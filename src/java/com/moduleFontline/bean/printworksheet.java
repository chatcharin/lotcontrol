/**
 *
 */
package com.moduleFontline.bean;

import com.moduleFontline.DataWorksheet.barcode;
import com.tct.data.DetailFl;
import com.tct.data.Details;
import com.tct.data.LotControl;
import com.tct.data.ModelPool;
import java.util.ArrayList;
import java.util.List;

/**
 * @author The_Boy_Cs
 *
 */
public class printworksheet {

    private barcode bc;
    private List<barcode> bclist;
    public static List<barcode> flist;
    public static List<barcode> rlist;
    public static LotControl currentlotControl;
    public static Details currentDetails;
    public static DetailFl currentDetailFl;
    public static ModelPool currentModelPool;

    public void loadBarcode() {
        bclist = new ArrayList<barcode>();
    }

    public barcode getBc() {
        return bc;
    }

    public void setBc(barcode bc) {
        this.bc = bc;
    }

    public List<barcode> getBclist() {
//        loadBarcode();
        return bclist;
    }

    public void setBclist(List<barcode> bclist) {
        this.bclist = bclist;
    }

    public List<barcode> getFlist() {
        return flist;
    }

    public List<barcode> getRlist() {
        return rlist;
    }

    public DetailFl getCurrentDetailFl() {
        return currentDetailFl;
    }

    public Details getCurrentDetails() {
        return currentDetails;
    }

    public LotControl getCurrentlotControl() {
        return currentlotControl;
    }

    public  ModelPool getCurrentModelPool() {
        return currentModelPool;
    }
    
}