/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moduleFontline.bean;

import com.appCinfigpage.bean.userLogin;
import com.checkrole.bean.CheckRoleUser;
import com.google.zxing.WriterException;
import com.moduleBackLine.Dataworksheet.blBarcode;
import com.moduleBackLine.bean.printassyworksheet;
import com.moduleFontline.DataWorksheet.detailProcess;
import com.moduleFontline.DataWorksheet.frontlinelist;
import com.tct.barcodegenerate.BarCodeGenerter;
import com.tct.data.*;
import com.tct.data.jpa.*;
import com.tct.data.jsf.util.PaginationHelper;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;

/**
 *
 * @author Tawat saekue
 */
public class FrontLineControl {

//    Search ===================================================================
    private String ordernumberSearch;
    private String modelSearch;
    private String typebobinSearch;
//    createview ===============================================================
    private Details currentdetail;
    private DetailFl currentdetailfl;
    private LotControl currentlotcontrol;
    private ModelPoolJpaController modelPoolJpaController;
    private List<detailProcess> detailprocess = null;
    private Md2pcJpaController md2pcJpaController;
    private ModelPool currentModelPool;
//    listview =================================================================
    private final String barcodecolum = "Barcode";
    private final String ordernumbercolum = "Order Number";
    private final String typemodelcolum = "Model";
    private final String typebobbincolum = "Type Bobbin";
    private final String startordercolum = "Start order";
    private final String finishordercolum = "Finish order";
    private final String processcolum = "Process";
    private final String statuscolum = "Status";
    private final String mccolum = "M/C";
    private DataModel<frontlinelist> tablefrontline;
    private List<frontlinelist> frontlinelist;
//    ==========================================================================
    private BarCodeGenerter genbarcode = null;

    public FrontLineControl() {
        this.search();
    }

    public void create() {
        print("=============== create front line worksheet =====================");
        try {
            Users user = userLogin.getSessionUser();
            if (checkValueForcreate()) {
                getCurrentdetail().setDateCreate(new Date());
                getCurrentdetail().setDeleted(0);
                getCurrentdetail().setDetailName("detail_fl");
                getCurrentdetail().setDetailType("frontline");
                getCurrentdetail().setNumOnmonth(0);
                getCurrentdetail().setPdQtyByLot(1 + "");
                getCurrentdetail().setUserCreate(user.getId());
                getCurrentdetail().setUserApprove("");
                getCurrentdetail().setLine("");

                getCurrentdetailfl().setDeleted(0);
                getCurrentdetailfl().setDetailIdDetail(currentdetail);

                int maxnum = getGenbarcode().getMaxnumberOfLotcontrol() + 1;
                String barcode = "01" + getCurrentdetailfl().getProductcode()
                        + "-" + maxnum + "-" + getSimpledate("yy-MM");
                print("Barcode : " + barcode);
                String partfile = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/images/barcode");
                if (GennerateBarcode(barcode, partfile)) {
                    getCurrentlotcontrol().setBarcode(barcode);
                    getCurrentlotcontrol().setDeleted(0);
                    getCurrentlotcontrol().setDetailsIdDetail(getCurrentdetail());
                    getCurrentlotcontrol().setMainDataId("");
                    getCurrentlotcontrol().setLotNumber("");
                    getCurrentlotcontrol().setModelPoolId(currentModelPool);
                    getCurrentlotcontrol().setQtyOutput(0);
                    getCurrentlotcontrol().setStatusApprove(1);
                    getCurrentlotcontrol().setUserApprove("1");
                    getCurrentlotcontrol().setWipQty(0);

                    created(getCurrentdetail(), getCurrentdetailfl());

                    getLotControlJpaController().create(getCurrentlotcontrol());

                    getBarcodelist().add(new blBarcode(getCurrentlotcontrol().getId(), barcode, "images/barcode/", true));

                    getGenbarcode().updateMaxnumberOfLotcontrol(maxnum);
                    printassyworksheet.detail = getCurrentdetail();
                    print("Created.");
                    Thread.sleep(2000);
                    String link = "pages/moduleFontLine/printview.xhtml";
                    userLogin.getSessionMenuBarBean().setParam(link);
                }
            }
        } catch (Exception e) {
            print("== Error create : " + e);
        }
    }

    private List<blBarcode> getBarcodelist() {
        if (printassyworksheet.barcode == null) {
            printassyworksheet.barcode = new ArrayList<blBarcode>();
        }
        return printassyworksheet.barcode;
    }

    private String getSimpledate(String p) {
        return new SimpleDateFormat(p).format(getDate());
    }

    private Date getDate() {
        return new Date();
    }

    /*
     * Gennerate barcode to picture
     */
    private boolean GennerateBarcode(String barcode, String pathF) {
        try {
            getGenbarcode().createQrCode(barcode, pathF + "/2d/");
            getGenbarcode().createBarcode(barcode, pathF + "/1d/");
            return true;
        } catch (FileNotFoundException fnfex) {
            print("= Error : " + fnfex);
            return false;
        } catch (IOException ioex) {
            print("= Error : " + ioex);
            return false;
        } catch (WriterException wex) {
            print("= Error : " + wex);
            return false;
        }
    }

    private BarCodeGenerter getGenbarcode() {
        if (genbarcode == null) {
            genbarcode = new BarCodeGenerter();
        }
        return genbarcode;
    }

    private boolean checkValueForcreate() {
        boolean t = true;
        if (t) {
        }
        return t;
    }

    private boolean created(Details details, DetailFl detailfl) {
        try {
            getDetailsJpaController().create(details);
            getDetailFlJpaController().create(detailfl);
            return true;
        } catch (Exception e) {
            print("Error : " + e);
            return false;
        }
    }

    public void search() {
        print("=============== search ==========================================");
        try {
            if (false) {
            } else {
                loadFrontlineList();
            }
        } catch (Exception e) {
            print("Error : " + e);
        }
    }

    private void loadFrontlineList() {
        print("=============== load frontline list =============================");
        try {
            frontlinelist = null;

            List<DetailFl> detailFls = getDetailFlJpaController().findAll();
            for (DetailFl detailfl : detailFls) {
                Details details = detailfl.getDetailIdDetail();
                List<LotControl> lotControls = getLotControlJpaController().findByDetailsId(details);
                for (LotControl lotControl : lotControls) {
                    Mc mc = null;
                    CurrentProcess firstprocess = null;
                    CurrentProcess lastprocess = null;
                    Md2pc md2pc = null;

                    ModelPool modelPool = lotControl.getModelPoolId();
                    List<Md2pc> md2pcs = getMd2pcJpaController().findBymodelPoolId(modelPool);
                    for (Md2pc md2pcc : md2pcs) {
                        if (md2pcc.getSqProcess() == 1) {
                            md2pc = md2pcc;
                            break;
                        }
                    }

                    String maindataid = lotControl.getMainDataId();
                    Collection<MainData> collectionmaindata = lotControl.getMainDataCollection();
                    for (MainData mainData : collectionmaindata) {
                        if (mainData.getIdProc().getIdProc().equals(md2pc.getProcessPoolIdProc().getIdProc())) {
                            Collection<CurrentProcess> collectioncurrentprocess = mainData.getCurrentProcessCollection();
                            for (CurrentProcess currentProcess : collectioncurrentprocess) {
                                if (currentProcess.getStatus().equals("Open")) {
                                    firstprocess = currentProcess;
                                    break;
                                }
                            }
                        }

                        if (mainData.getId().equals(maindataid)) {
                            String currentprocessid = mainData.getCurrentNumber();
                            Collection<CurrentProcess> collectioncurrentprocess = mainData.getCurrentProcessCollection();
                            for (CurrentProcess currentProcess : collectioncurrentprocess) {
                                if (currentProcess.getStatus().equals("Finish")) {
                                    lastprocess = currentProcess;
                                }
                                if (currentProcess.getId().equals(currentprocessid)) {
                                    mc = currentProcess.getMcId();
                                    break;
                                }
                            }
                        }
                    }

                    if (md2pc != null && firstprocess != null && mc != null) {
                        if (lastprocess != null) {
                            getFrontlinelist().add(new frontlinelist(details, detailfl, getFormatDate(firstprocess.getTimeCurrent()), getFormatDate(lastprocess.getTimeCurrent()), mc.getMcName(),
                                    lotControl.getBarcode(), lastprocess.getStatus(), lastprocess.getMainDataId().getIdProc().getProcName(), getModel(lotControl), detailfl.getTypeBb(), details.getOrderNumber()));
                        } else {
                            getFrontlinelist().add(new frontlinelist(details, detailfl, getFormatDate(firstprocess.getTimeCurrent()), "", mc.getMcName(),
                                    lotControl.getBarcode(), "", "", getModel(lotControl), detailfl.getTypeBb(), details.getOrderNumber()));
                        }
                    } else {
                        getFrontlinelist().add(new frontlinelist(details, detailfl, "", "", "",
                                lotControl.getBarcode(), "", "", getModel(lotControl), detailfl.getTypeBb(), details.getOrderNumber()));
                    }
                }
            }
            setTablefrontline();
        } catch (Exception e) {
            print("Error : " + e);
        }
    }

    private String getModel(LotControl lotControl) {
        ModelPool modelPool = lotControl.getModelPoolId();
        String model = "";
        model += modelPool.getModelType() + "-";
        model += modelPool.getModelSeries() + "-";
        model += modelPool.getModelName();
        print("getmodel : " + model);
        return model;
    }

    public static String getFormatDate(Date date) {
        return new SimpleDateFormat("EEE dd/MM/yyyy HH:mm:ss").format(date);
    }

    public void changToInsertview() {
        print("================== chang to insert ==============================");
        try {
            printassyworksheet.detail = null;
            printassyworksheet.barcode = null;
            String link = "pages/moduleFontLine/createview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (Exception e) {
            print("Error : " + e);
        }
    }
    private CurrentProcessJpaController currentProcessJpaController;

    private CurrentProcessJpaController getCurrentProcessJpaController() {
        if (currentProcessJpaController == null) {
            currentProcessJpaController = new CurrentProcessJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return currentProcessJpaController;
    }
    private MainDataJpaController mainDataJpaController;

    private MainDataJpaController getMainDataJpaController() {
        if (mainDataJpaController == null) {
            mainDataJpaController = new MainDataJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mainDataJpaController;
    }
    private LotControlJpaController lotControlJpaController;

    private LotControlJpaController getLotControlJpaController() {
        if (lotControlJpaController == null) {
            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotControlJpaController;
    }
    private DetailFlJpaController detailFlJpaController;

    private DetailFlJpaController getDetailFlJpaController() {
        if (detailFlJpaController == null) {
            detailFlJpaController = new DetailFlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailFlJpaController;
    }
    private DetailsJpaController detailsJpaController;

    private DetailsJpaController getDetailsJpaController() {
        if (detailsJpaController == null) {
            detailsJpaController = new DetailsJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailsJpaController;
    }

    public Details getCurrentdetail() {
        if (currentdetail == null) {
            currentdetail = new Details(UUID.randomUUID().toString());
        }
        return currentdetail;
    }

    public DetailFl getCurrentdetailfl() {
        if (currentdetailfl == null) {
            currentdetailfl = new DetailFl(UUID.randomUUID().toString());
        }
        return currentdetailfl;
    }

    public LotControl getCurrentlotcontrol() {
        if (currentlotcontrol == null) {
            currentlotcontrol = new LotControl(UUID.randomUUID().toString());
        }
        return currentlotcontrol;
    }

    public void setCurrentlotcontrol(LotControl currentlotcontrol) {
        this.currentlotcontrol = currentlotcontrol;
    }

    public void changProductCodeNameValue(ValueChangeEvent event) {
        print("================== Product Name =================================");
        String pdname = (String) event.getNewValue();
        if ("Prototype".equals(pdname)) {
            getCurrentdetailfl().setProductcode("P");
        }
        if ("New product introduction".equals(pdname)) {
            getCurrentdetailfl().setProductcode("NPI");
        }
        if ("Special built request".equals(pdname)) {
            getCurrentdetailfl().setProductcode("SBR");
        }
        if ("Mass production".equals(pdname)) {
            getCurrentdetailfl().setProductcode("M");
        }
        getCurrentdetailfl().setProductcodeName(pdname);
        print("= Product code name " + getCurrentdetailfl().getProductcodeName());
        print("= Product code : " + getCurrentdetailfl().getProductcode());
    }

    public void changModelpoolValue(ValueChangeEvent event) {
        print("======================= changModelPool ==========================");
        String modelid = event.getNewValue().toString();
        currentModelPool = getModelPoolJpaController().findModelPool(modelid);
        currentdetail.setModelseries(currentModelPool.getModelSeries());
        loadProcess();
    }

    public void changToDeleted() {
        CheckRoleUser deleteWorksheetButtonClaim = new CheckRoleUser();
        if (deleteWorksheetButtonClaim.check("delete_fl")) {
            print("=================== Deleted front line worksheet ================");
            try {
                String barcode = getTablefrontline().getRowData().getBarcode();
                LotControl lotControls = getLotControlJpaController().findLotcontrolByBarcode(barcode).get(0);
                lotControls.setDeleted(1);
                getLotControlJpaController().edit(lotControls);
                print("= Deleted");
                search();
            } catch (Exception e) {
                print("= Error : " + e);
            }
        }
    }

    public void changToSave() {
        print("=============== chang to save ===================================");
        try {
            getDetailsJpaController().edit(getCurrentdetail());
            getDetailFlJpaController().edit(getCurrentdetailfl());
            print("Save completed.");
            String link = "pages/moduleFontLine/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            search();
        } catch (Exception e) {
            print("== Error save chang : " + e);
        }
    }

    public void changToDetail() {
        print("============== chang to detail view =============================");
        try {
            frontlinelist fll = getTablefrontline().getRowData();
            print("fll : " + fll.getBarcode());
            if (fll != null) {
                currentdetail = fll.getDetails();
                currentdetailfl = fll.getDetailFl();
                currentModelPool = getModelPoolJpaController().findModelPool(getCurrentdetail().getModel());
                loadProcess();
                String link = "pages/moduleFontLine/detailview.xhtml";
                userLogin.getSessionMenuBarBean().setParam(link);
            }
        } catch (Exception e) {
            print(" Error chang edit : " + e);
        }
    }

    public void changToEdit() {
        print("============= chang to edit view ================================");
        try {
            frontlinelist fll = getTablefrontline().getRowData();
            print("fll : " + fll.getBarcode());
            if (fll != null) {
                currentdetail = fll.getDetails();
                currentdetailfl = fll.getDetailFl();
                currentModelPool = getModelPoolJpaController().findModelPool(getCurrentdetail().getModel());
                loadProcess();
                String link = "pages/moduleFontLine/editview.xhtml";
                userLogin.getSessionMenuBarBean().setParam(link);
            }
        } catch (Exception e) {
            print(" Error chang edit : " + e);
        }
    }

    public void changToPrint() {
        print("============== chang to print ===================================");
        try {
            frontlinelist fll = getTablefrontline().getRowData();
            if (fll != null) {
                currentdetail = fll.getDetails();
                currentdetailfl = fll.getDetailFl();
                currentlotcontrol = getLotControlJpaController().findLotcontrolByBarcode(fll.getBarcode()).get(0);
                getBarcodelist().add(new blBarcode(getCurrentlotcontrol().getId(), fll.getBarcode(), "./images/barcode/", true));
                String link = "pages/moduleFontLine/printview.xhtml";
                userLogin.getSessionMenuBarBean().setParam(link);
            }
        } catch (Exception e) {
            print("= Error chang print : " + e);
        }
    }

    public void changToCancel() {
        print("========== chang to cancel ======================================");
        try {
            reset();
            String link = "pages/moduleFontLine/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            search();
        } catch (Exception e) {
            print("== Error : " + e);
        }
    }

    private void loadProcess() {
        print("=================================================================");
        try {
            detailprocess = null;
            List<Md2pc> md2pclist = getMd2pcJpaController().findBymodelPoolId(currentModelPool);
            for (Md2pc md2pc : md2pclist) {
                if (md2pc.getProcessPoolIdProc().getType().equals("fl")) {
                    print("= md2pc : " + md2pc.getMd2pcId());
                    ProcessPool pcpool = md2pc.getProcessPoolIdProc();
                    print("= Process : " + pcpool.getProcName());
                    detailProcess dp = new detailProcess("", null, null, null, null, "", pcpool.getProcName().toString(), "", 0, 0, 0, getCurrentdetail().getOrderNumber(), "");
                    getDetailprocess().add(dp);
                }
            }
        } catch (Exception e) {
            print("= Ex : " + e);
        }
    }

    private Md2pcJpaController getMd2pcJpaController() {
        if (md2pcJpaController == null) {
            md2pcJpaController = new Md2pcJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return md2pcJpaController;
    }

    public List<detailProcess> getDetailprocess() {
        if (detailprocess == null) {
            detailprocess = new ArrayList<detailProcess>();
        }
        return detailprocess;
    }

    public List<SelectItem> getMoldStamp() {
        print("=========== get mold stampp =====================================");
        List<SelectItem> moldstamp = new ArrayList<SelectItem>();
        moldstamp.add(new SelectItem("1", " 1 "));
        moldstamp.add(new SelectItem("2", " 2 "));
        return moldstamp;
    }

    public List<SelectItem> getMoldInjection() {
        print("============ get mold injection =================================");
        List<SelectItem> moldinject = new ArrayList<SelectItem>();
        moldinject.add(new SelectItem("TA 4", "TA 4"));
        moldinject.add(new SelectItem("TA 5", "TA 5"));
        moldinject.add(new SelectItem("TA 6", "TA 6"));
        moldinject.add(new SelectItem("A", "A"));
        moldinject.add(new SelectItem("F", "F"));
        moldinject.add(new SelectItem("REFU", "REFU"));
        return moldinject;
    }

    public List<SelectItem> getTypeFR() {
        print("============ get type frame or roll =============================");
        List<SelectItem> tfr = new ArrayList<SelectItem>();
        tfr.add(new SelectItem("F", "Frame"));
        tfr.add(new SelectItem("R", "Roll"));
        return tfr;
    }

    public List<SelectItem> getTypeBobbin() {
        print("============= get type bobbin ===================================");
        List<SelectItem> tbb = new ArrayList<SelectItem>();
        tbb.add(new SelectItem("TTRN-05H", "TTRN-05H"));
        tbb.add(new SelectItem("TTRN-06H", "TTRN-06H"));
        tbb.add(new SelectItem("TTRN-38H", "TTRN-38H"));
        tbb.add(new SelectItem("BO", "BO"));
        tbb.add(new SelectItem("REFU", "REFU"));
        tbb.add(new SelectItem("NORMAL", "NORMAL"));
        tbb.add(new SelectItem("CB", "CB"));
        tbb.add(new SelectItem("TTRG", "TTRG"));
        tbb.add(new SelectItem("TTRN-25C", "TTRN-25C"));
        return tbb;
    }

    public List<SelectItem> getBobbinname() {
        print("============== get type bobbin name =============================");
        List<SelectItem> bbname = new ArrayList<SelectItem>();
        bbname.add(new SelectItem("B-TTRN-05H", "B-TTRN-05H"));
        bbname.add(new SelectItem("B-TTRN-06H", "B-TTRN-06H"));
        bbname.add(new SelectItem("B-TTRN-38H", "B-TTRN-38H"));
        return bbname;
    }

    public List<SelectItem> getProductTypelist() {
        print("============== get product type list ============================");
        List<SelectItem> pdtype = new ArrayList<SelectItem>();
        List<String> modelgroup = getModelPoolJpaController().findModelPoolGroupByGroup();
        print("product type size : " + modelgroup.size());
        for (String model : modelgroup) {
            print("== Product type : " + model);
            pdtype.add(new SelectItem(model, model));
        }
        return pdtype;
    }

    public List<SelectItem> getProductcodenamelist() {
        List<SelectItem> pdnameList = new ArrayList<SelectItem>();
        pdnameList.add(new SelectItem("Prototype", "Prototype"));
        pdnameList.add(new SelectItem("New product introduction", "New product introduction"));
        pdnameList.add(new SelectItem("Special built request", "Special built request"));
        pdnameList.add(new SelectItem("Mass production", "Mass production"));
        return pdnameList;
    }

    public List<SelectItem> getPlatlist() {
        List<SelectItem> platlist = new ArrayList<SelectItem>();
        platlist.add(new SelectItem("Tokyo cail", "Tokyo cail"));
        platlist.add(new SelectItem("Farsonic", "Farsonic"));
//        plantlist.add(new SelectItem("101", "Tokyo Coil Engineering(Thailand)"));
//        plantlist.add(new SelectItem("102", "Tokyo Coil Engineering(LAOS)"));
//        plantlist.add(new SelectItem("103", "DCE"));
//        plantlist.add(new SelectItem("104", "HTD"));
        return platlist;
    }

    public List<SelectItem> getResinnamelist() {
        print("=========== get Resin name ======================================");
        List<SelectItem> resinnamelist = new ArrayList<SelectItem>();
        resinnamelist.add(new SelectItem("FC110B", "FC110B"));
        resinnamelist.add(new SelectItem("E5008B", "E5008B"));
        resinnamelist.add(new SelectItem("TN7500A", "TN7500A"));
        resinnamelist.add(new SelectItem("AC1080-N1086R", "AC1080-N1086R"));
        resinnamelist.add(new SelectItem("SFC-N45", "SFC-N45"));
        resinnamelist.add(new SelectItem("CX1301", "CX1301"));
        return resinnamelist;
    }

    public List<SelectItem> getModelpoolslist() {
        List<ModelPool> modellist = getModelPoolJpaController().findModelPoolGroupByName();
        List<SelectItem> selectlist = new ArrayList<SelectItem>();
        print("============ get model pool ============");
        for (ModelPool model : modellist) {
            print("= Model name : " + model);
            selectlist.add(new SelectItem(model.getId(), model.getModelType() + "-" + model.getModelSeries() + "-" + model.getModelName()));
        }
        return selectlist;
    }

    public List<SelectItem> getResintypelist() {
        List<SelectItem> resintypelist = new ArrayList<SelectItem>();
        resintypelist.add(new SelectItem("Vergin", "Vergin"));
        resintypelist.add(new SelectItem("Recycle 1", "Recycle 1"));
        resintypelist.add(new SelectItem("Recycle 2", "Recycle 2"));
        resintypelist.add(new SelectItem("Recycle 3", "Recycle 3"));
        return resintypelist;
    }

    private void print(String str) {
        System.out.println(str);
    }

    public String getModelSearch() {
        return modelSearch;
    }

    public void setModelSearch(String modelSearch) {
        this.modelSearch = modelSearch;
    }

    public String getOrdernumberSearch() {
        return ordernumberSearch;
    }

    public void setOrdernumberSearch(String ordernumberSearch) {
        this.ordernumberSearch = ordernumberSearch;
    }

    public String getTypebobinSearch() {
        return typebobinSearch;
    }

    public void setTypebobinSearch(String typebobinSearch) {
        this.typebobinSearch = typebobinSearch;
    }

    private ModelPoolJpaController getModelPoolJpaController() {
        if (modelPoolJpaController == null) {
            modelPoolJpaController = new ModelPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return modelPoolJpaController;
    }

    public List<frontlinelist> getFrontlinelist() {
        if (frontlinelist == null) {
            frontlinelist = new ArrayList<frontlinelist>();
        }
        return frontlinelist;
    }

    public DataModel<frontlinelist> getTablefrontline() {
        return tablefrontline;
    }

    public void setTablefrontline() {
        this.tablefrontline = getTablefrontlinepagination().createPageDataModel();
    }
    private PaginationHelper tablefrontlinepagination;

    public PaginationHelper getTablefrontlinepagination() {
        if (tablefrontlinepagination == null) {
            tablefrontlinepagination = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    return getFrontlinelist().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    if (getFrontlinelist().size() > 0) {
                        return new ListDataModel(getFrontlinelist().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        return new ListDataModel();
                    }
                }
            };
        }
        return tablefrontlinepagination;
    }

    public String getBarcodecolum() {
        return barcodecolum;
    }

    public String getOrdernumbercolum() {
        return ordernumbercolum;
    }

    public String getProcesscolum() {
        return processcolum;
    }

    public String getStatuscolum() {
        return statuscolum;
    }

    public String getTypebobbincolum() {
        return typebobbincolum;
    }

    public String getTypemodelcolum() {
        return typemodelcolum;
    }

    public String getFinishordercolum() {
        return finishordercolum;
    }

    public String getStartordercolum() {
        return startordercolum;
    }

    public String getMccolum() {
        return mccolum;
    }

    private void reset() {
        print("============ reset ==============================================");
        try {
            currentModelPool = null;
            currentdetail = null;
            currentdetailfl = null;
            currentlotcontrol = null;
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }
}
