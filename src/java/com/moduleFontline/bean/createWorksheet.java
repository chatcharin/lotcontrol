package com.moduleFontline.bean;

import com.appCinfigpage.bean.userLogin;
import com.checkrole.bean.CheckRoleUser;
import com.google.zxing.WriterException;
import com.moduleBackLine.Dataworksheet.workSheetDetail;
import com.moduleFontline.DataWorksheet.barcode;
import com.moduleFontline.DataWorksheet.detailProcess;
import com.tct.barcodegenerate.BarCodeGenerter;
import com.tct.dashboard.ReportBean;
import com.tct.data.*;
import com.tct.data.jpa.*;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;

public class createWorksheet {

    private final int rolllot = 32000;
    private final int framelot = 8000;
    private List<SelectItem> selectItemPlant = null;
    private List<SelectItem> resinnamelist = null;
    private List<SelectItem> resintypelist = null;
    private List<SelectItem> producttypelist = null;
    private List<SelectItem> productcodenamelist = null;
    private List<detailProcess> detailprocess = null;
    private List<SelectItem> modelserieslist = null;
    private DetailFl detailFl = null;
    private ModelPool modelPool = null;
    private ModelPoolJpaController modelPoolJpaController = null;
    private Md2pcJpaController md2pcJpaController = null;
    private LotControlJpaController lotControlJpaController = null;
    private LotControl lotControl = null;
    private DetailFlJpaController detailFlJpacontroller = null;
    private DetailsJpaController detailJpacontroller = null;
    // -------------------------------------------------------------
    private List<SelectItem> modellist;

    public createWorksheet() {
    }

    private ModelPool getModelPools() {
        if (modelPool == null) {
            modelPool = new ModelPool();
        }
        return modelPool;
    }

    public List<SelectItem> getModelPoolsList() {
        if (modellist == null) {
            modellist = new ArrayList<SelectItem>();
        }
        return modellist;
    }

    private ModelPoolJpaController getModelPoolJpaController() {
        if (modelPoolJpaController == null) {
            modelPoolJpaController = new ModelPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return modelPoolJpaController;
    }

    public void ChangPlantValue(ValueChangeEvent event) throws Exception {
        String plant = (String) event.getNewValue();
        print("Plant : " + plant);
        getDetailFl().setPlat(plant);
    }

    public void ChangResinNameValue(ValueChangeEvent event) {
        String resinname = (String) event.getNewValue();
        print("Resin Name " + resinname);
        getDetailFl().setResinName(resinname);
    }

    public void ChangResinTypeValue(ValueChangeEvent event) {
        print("=========== chang resin type ====================================");
        String resintype = (String) event.getNewValue();
        print("Resintype" + resintype);
        getDetailFl().setResinType(resintype);
    }

    public void ChangProductTypeValue(ValueChangeEvent event) { // product type
        print("========== chang Pd type ========================================");
        String pdtype = (String) event.getNewValue();
        print("Product Type : " + pdtype);
        getDetailFl().setProductType(pdtype);
    }

    public void ChangProductCodeNameValue(ValueChangeEvent event) {
        print("=========== chang prduct code name ==============================");
        String pdcodename = (String) event.getNewValue();
        String pdcode = "";
        if ("Prototype".equals(pdcodename)) {
            pdcode = "P";
        }
        if ("New product introduction".equals(pdcodename)) {
            pdcode = "N";
        }
        if ("Special built request".equals(pdcodename)) {
            pdcode = "S";
        }
        if ("Mass production".equals(pdcodename)) {
            pdcode = "M";
        }
        print("Product code name 1 " + pdcodename);
        getDetailFl().setProductcodeName(pdcodename);
        print("Product code 1 : " + pdcode);
        getDetailFl().setProductcode(pdcode);
        print("complete ");
    }

    private void print(String str) {
        System.out.println(str);
    }

    private void loadProcess() {
        print("=================================================================");
        try {
            detailprocess = new ArrayList<detailProcess>();
            List<Md2pc> md2pclist = getMd2pcJpaController().findBymodelPoolId(modelPool);
            for (Md2pc md2pc : md2pclist) {
                print("= md2pc : " + md2pc.getMd2pcId());
                ProcessPool pcpool = md2pc.getProcessPoolIdProc();
                print("= Process : " + pcpool.getProcName());
                detailProcess dp = new detailProcess("", null, null, null, null, "", pcpool.getProcName().toString(), "", 0, 0, 0, getDetail().getOrderNumber(), "");
                detailprocess.add(dp);
            }
        } catch (Exception e) {
            print("= Ex : " + e);
        }
    }

    private Md2pcJpaController getMd2pcJpaController() {
        if (md2pcJpaController == null) {
            md2pcJpaController = new Md2pcJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return md2pcJpaController;
    }
    private Details detail = null;
    private BarCodeGenerter genbarcode = null;

//    public void CreateWorksheetFun(ActionEvent event) {
//        String Fbarcode, Rbarcode;
//        System.out.println("start create barcode =");
//        try {
//            getDetail().setIdDetail(UUID.randomUUID().toString());
//            getDetail().setUserCreate(getUserLogin().getId());
//            getDetail().setDateCreate(new Date());
//            getDetail().setDetailType("fontline");
//            getDetail().setDetailName("detail_fl");
//            getDetail().setLine("0");
//            getDetailFl().setId(UUID.randomUUID().toString());
//            getDetailFl().setDetailIdDetail(detail);
//            getDetailFl().setTypeModel(modelPool.getModelName());
//
//            getJpaDetailsController().create(getDetail());
//            getDetailFlJpacontroller().create(getDetailFl());
//
//            String partfile = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/images/barcode");
//            print(" PartFile : " + partfile);
//            for (int i = 0; i < Integer.parseInt(getDetailFl().getQtyFrame()); i++) {
//                Fbarcode = genFramebarCode();
//                print("Frame barcode : " + Fbarcode);
//
//                getLotControl().setId(UUID.randomUUID().toString());
//                getLotControl().setBarcode(Fbarcode);
//                getLotControl().setModelPoolId(modelPool);
//                getLotControl().setDetailsIdDetail(detail);
//                getLotControl().setLotNumber((i + 1) + "");
//                if (GennerateBarcode(Fbarcode, partfile)) {
//                    getLotControlJpaController().create(lotControl);
//                    getFlist().add(new barcode((i + 1), Fbarcode, true, "./images/barcode/", "F"));
//                }
//            }
//
//            for (int j = 0; j < Integer.parseInt(getDetailFl().getQtyRoll()); j++) {
//                Rbarcode = genRollbarCode();
//                print("Roll barcode : " + Rbarcode);
//
//                getLotControl().setId(UUID.randomUUID().toString());
//                getLotControl().setBarcode(Rbarcode);
//                getLotControl().setModelPoolId(modelPool);
//                getLotControl().setDetailsIdDetail(detail);
//                getLotControl().setLotNumber("" + (j + 1));
//                if (GennerateBarcode(Rbarcode, partfile)) {
//                    getLotControlJpaController().create(lotControl);
//                    getRlist().add(new barcode((j + 1), Rbarcode, true, "./images/barcode/", "R"));
//                }
//            }
//            Thread.sleep(5000);
//            printworksheet.currentDetailFl = detailFl;
//            printworksheet.currentDetails = detail;
//            printworksheet.currentModelPool = modelPool;
//            printworksheet.currentlotControl = lotControl;
//            String link = "pages/moduleFontLine/printworksheet.xhtml";
//            userLogin.getSessionMenuBarBean().setParam(link);
//            detail = null;
//            detailFl = null;
//            lotControl = null;
//            modelPool = null;
//            detailprocess = null;
//        } catch (Exception e) {
//            print("= Error : " + e.toString());
//        }
//    }

    private List<barcode> getFlist() {
        if (printworksheet.flist == null) {
            printworksheet.flist = new ArrayList<barcode>();
        }
        return printworksheet.flist;
    }

    private List<barcode> getRlist() {
        if (printworksheet.rlist == null) {
            printworksheet.rlist = new ArrayList<barcode>();
        }
        return printworksheet.rlist;
    }

    private LotControlJpaController getLotControlJpaController() {
        if (lotControlJpaController == null) {
            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotControlJpaController;
    }

    private LotControl getLotControl() {
        if (lotControl == null) {
            lotControl = new LotControl();
        }
        return lotControl;
    }

    private Users getUserLogin() {
        Users user = userLogin.getSessionUser();
        return user;
    }

    private String genRollbarCode() {
        int number = getGenbarcode().getMaxnumberOfLotcontrol() + 1;
        getGenbarcode().updateMaxnumberOfLotcontrol(number);
        String tmp = "";
        tmp += "FLR" + getDetailFl().getProductcode() + getDetailFl().getProductType() + "-" + new SimpleDateFormat("MMyy").format(new Date()) + modelPool.getModelSeries();
        tmp += "" + number;
        return tmp;
    }

    private String genFramebarCode() {
        int number = getGenbarcode().getMaxnumberOfLotcontrol() + 1;
        getGenbarcode().updateMaxnumberOfLotcontrol(number);
        String tmp = "";
        tmp += "FLF" + getDetailFl().getProductcode() + getDetailFl().getProductType() + "-" + new SimpleDateFormat("MMyy").format(new Date()) + modelPool.getModelSeries();
        tmp += "" + number;
        return tmp;
    }

    /*
     * Gennerate barcode to picture
     */
    private boolean GennerateBarcode(String barcode, String pathF) {
        try {
            getGenbarcode().createQrCode(barcode, pathF + "/2d/");
            getGenbarcode().createBarcode(barcode, pathF + "/1d/");
            return true;
        } catch (FileNotFoundException fnfex) {
            print("= Error : " + fnfex);
            return false;
        } catch (IOException ioex) {
            print("= Error : " + ioex);
            return false;
        } catch (WriterException wex) {
            print("= Error : " + wex);
            return false;
        }
    }

    private BarCodeGenerter getGenbarcode() {
        if (genbarcode == null) {
            genbarcode = new BarCodeGenerter();
        }
        return genbarcode;
    }

    private DetailFlJpaController getDetailFlJpacontroller() {
        if (detailFlJpacontroller == null) {
            detailFlJpacontroller = new DetailFlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailFlJpacontroller;
    }

    private DetailsJpaController getJpaDetailsController() {
        if (detailJpacontroller == null) {
            detailJpacontroller = new DetailsJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailJpacontroller;
    }

    public List<SelectItem> getSelectItemPlant() {
        if (this.selectItemPlant == null || this.selectItemPlant.isEmpty()) {
            selectItemPlant = new ArrayList<SelectItem>();
            selectItemPlant.add(new SelectItem("101", "Tokyo Coil Engineering(Thailand)"));
            selectItemPlant.add(new SelectItem("102", "Tokyo Coil Engineering(LAOS)"));
            selectItemPlant.add(new SelectItem("103", "DCE"));
            selectItemPlant.add(new SelectItem("104", "HTD"));
        }
        return selectItemPlant;
    }

    public List<SelectItem> getResinnamelist() {
        if (this.resinnamelist == null || this.resinnamelist.isEmpty()) {
            resinnamelist = new ArrayList<SelectItem>();
            resinnamelist.add(new SelectItem("FC110B", "FC110B"));
        }
        return resinnamelist;
    }

    public List<SelectItem> getResintypelist() {
        if (this.resintypelist == null || this.resintypelist.isEmpty()) {
            resintypelist = new ArrayList<SelectItem>();
            resintypelist.add(new SelectItem("Vergin", "Vergin"));
            resintypelist.add(new SelectItem("Recycle 1", "Recycle 1"));
            resintypelist.add(new SelectItem("Recycle 2", "Recycle 2"));
            resintypelist.add(new SelectItem("Recycle 3", "Recycle 3"));
        }
        return resintypelist;
    }

    public List<SelectItem> getProducttypelist() {
        if (producttypelist == null) {
            producttypelist = new ArrayList<SelectItem>();
            producttypelist.add(new SelectItem("T", "Trigger coil"));
            producttypelist.add(new SelectItem("TR", "Tranformer"));
            producttypelist.add(new SelectItem("PCB", "Print circuit board"));
        }
        return producttypelist;
    }

    public List<SelectItem> getProductcodenamelist() {
        if (this.productcodenamelist == null || this.productcodenamelist.isEmpty()) {
            productcodenamelist = new ArrayList<SelectItem>();
            productcodenamelist.add(new SelectItem("Prototype", "Prototype"));
            productcodenamelist.add(new SelectItem("New product introduction", "New product introduction"));
            productcodenamelist.add(new SelectItem("Special built request", "Special built request"));
            productcodenamelist.add(new SelectItem("Mass production", "Mass production"));
        }
        return productcodenamelist;
    }
    private List<SelectItem> typeBobbinList;

    public List<SelectItem> getTypeBobbinList() {
        if (typeBobbinList == null) {
            typeBobbinList = new ArrayList<SelectItem>();
            List<ModelPool> modelPools = getModelPoolJpaController().findModelPoolByTypeFlBl("fl");
            for (ModelPool modelpool : modelPools) {
                typeBobbinList.add(new SelectItem(modelpool, modelpool.getModelName()));
            }
        }
        return typeBobbinList;
    }

    public List<detailProcess> getDetailprocess() {
        if (detailprocess == null) {
            detailprocess = new ArrayList<detailProcess>();
        }
        return detailprocess;
    }

    public void setModelseries(List<SelectItem> modelserieslist) {
        this.modelserieslist = modelserieslist;
    }

    public void changToInsertview(ActionEvent event) {
        print("=================================================================");
        detail = null;
        detailFl = null;
        detailprocess = null;
        String link = "pages/moduleFontLine/createWorksheet.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
    }

    public void changTocancel(ActionEvent event) {
        print("=================================================================");
        editWorksheet.currentdetailfl = null;
        editWorksheet.currentdetails = null;
        printworksheet.flist = null;
        printworksheet.rlist = null;
        detail = null;
        detailFl = null;
        detailprocess = null;

        String link = "pages/moduleFontLine/worksheet.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
        userLogin.getSessionWorksheet().searchWorksheet();
        print("= Cancel : ");
    }

    public ModelPool getModelPool() {
        return modelPool;
    }

    ////////////////////////// list view ///////////////////////////////////////
    public void printWorksheet(ActionEvent event) {
        CheckRoleUser printWorksheetButtonClaim = new CheckRoleUser();
        boolean checkprint = true;//printWorksheetButtonClaim.check("detail_front_line");
        print("check print : " + checkprint);
        if (checkprint) {
            print("=================================================================");
            printworksheet.flist = null;
            printworksheet.rlist = null;
            try {
                workSheetDetail worksheetdetail = (workSheetDetail) worksheet.itemsworksheet.getRowData();
                Details details = worksheetdetail.getDetail();
                List<LotControl> lotControllist = getLotControlJpaController().findLotcontrolByBarcode(worksheetdetail.getBarcode());

                DetailFl detailfl = getDetailFlJpacontroller().findDetailFlByDetails(details);
                print("Model :::: " + details.getModel() + "  Model Series :::: " + details.getModelseries());
                ModelPool modelPools = getModelPoolJpaController().findModelPool(details.getModel());

                printworksheet.currentDetailFl = detailfl;
                printworksheet.currentDetails = details;
                printworksheet.currentlotControl = lotControllist.get(0);
                printworksheet.currentModelPool = modelPools;
                String barcodetype = printworksheet.currentlotControl.getBarcode().substring(2, 3);
                print("BarcodeType : " + barcodetype);

                if ("F".equals(barcodetype)) {
                    getFlist().add(new barcode(1, printworksheet.currentlotControl.getBarcode(), true, "../images/barcode/", "F"));
                }
                if ("R".equals(barcodetype)) {
                    getRlist().add(new barcode(1, printworksheet.currentlotControl.getBarcode(), true, "../images/barcode/", "R"));
                }
                url = "";
                String link = "pages/moduleFontLine/printworksheet.xhtml";
                userLogin.getSessionMenuBarBean().setParam(link);
                print("= Print this.");
            } catch (Exception e) {
                print("= Error : " + e);
            }
        }
    }
    RoleUserJpaController roleJpaController;

    private RoleUserJpaController getRoleUserJpaController() {

        if (roleJpaController == null) {
            roleJpaController = new RoleUserJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return roleJpaController;
    }

    // Find Role by User id 
    public Collection<RoleUser> getRoleUser(Users u) {
        // User jpa find Role
        return getRoleUserJpaController().findByUserId(u);
    }

    public void editWorksheet(ActionEvent event) {
//       FacesContext context = FacesContext.getCurrentInstance();
//       Users currentuser = (Users) context.getExternalContext().getSessionMap().get("currentUser");
//       Collection<RoleUser> user_role =  getRoleUser(currentuser);        
//       for(RoleUser roleUser:user_role){
//           System.out.println( roleUser.getRoleId().getApproveAssembly()+" ======= ");
//       }
        CheckRoleUser editButtonClaim = new CheckRoleUser();
//         System.out.println("Status edit"+editButtonClaim.check("adduser"));
//        System.out.print("== Role edit"+editButton.check("EditFl"));
        if (editButtonClaim.check("edit_fl")) {
            print("=================================================================");
            try {
                workSheetDetail worksheetdetail = (workSheetDetail) worksheet.itemsworksheet.getRowData();
                Details details = worksheetdetail.getDetail();
                print("= Detail id : " + details);
                DetailFl detailfl = getDetailFlJpacontroller().findDetailFlByDetails(details);
                print("Model :::: " + details.getModel() + "  Model Series :::: " + details.getModelseries());
                ModelPool modelPools = getModelPoolJpaController().findModelPool(details.getModel());
                editWorksheet.currentdetailfl = detailfl;
                editWorksheet.modelPool = modelPools;
                editWorksheet.currentdetails = details;

                String link = "pages/moduleFontLine/editView.xhtml";
                userLogin.getSessionMenuBarBean().setParam(link);
                print("= Edit worksheet.");
            } catch (Exception e) {
                print("= Error : " + e);
            }
        }
    }

    public void changSaveworksheet(ActionEvent event) {
        print("=================================================================");
        try {
            getJpaDetailsController().edit(editWorksheet.currentdetails);
            getDetailFlJpacontroller().edit(editWorksheet.currentdetailfl);
            print("= Compete");
            editWorksheet.currentdetailfl = null;
            editWorksheet.currentdetails = null;
            editWorksheet.modelPool = null;

            String link = "pages/moduleFontLine/worksheet.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            userLogin.getSessionWorksheet().searchWorksheet();
        } catch (NonexistentEntityException nex) {
            print("= Error : " + nex);
        } catch (Exception e) {
            print("= Error2 : " + e);
        }
    }

    public void deleteWorksheet(ActionEvent event) {
        CheckRoleUser deleteWorksheetButtonClaim = new CheckRoleUser();
        if (deleteWorksheetButtonClaim.check("delete_fl")) {
            print("=================== Deleted front line worksheet ================");
            try {
                workSheetDetail worksheetdetail = (workSheetDetail) worksheet.itemsworksheet.getRowData();
                String barcode = worksheetdetail.getBarcode();
                LotControl lotControls = getLotControlJpaController().findLotcontrolByBarcode(barcode).get(0);
                lotControls.setDeleted(1);
                getLotControlJpaController().edit(lotControls);
                print("= Deleted");
                userLogin.getSessionWorksheet().searchWorksheet();
            } catch (Exception e) {
                print("= Error : " + e);
            }
        }
    }
    String url = "";

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean printAll(ActionEvent event) {
        HashMap input = new HashMap();
        input.put("detailId", printworksheet.currentDetails.getIdDetail());
        String filename = printworksheet.currentDetails.getIdDetail();
        print("= File name : " + filename);
        if (new ReportBean().pdf("/report/detialsReport.jasper", "/report/" + filename + ".pdf", input)) {
            System.out.println("complete");
            /*
             * url ="window.open(\"http://www.w3schools.com\");";
             * top.preview_html(
             *
             */
            url = " alert('Print');"
                    //                    + " window.location.href = 'report/test.pdf';";
                    + "window.open(\"./report/" + filename + ".pdf\");";
            return true;
        }
        return false;
    }

    public boolean printSelect(ActionEvent event) {
        url = "";
        String selectLot = "";
        String sql = "";
        sql = " SELECT  lot_control.details_id_detail AS lot_control_details_id_detail,"
                + " md2pc.md2pc_id AS md2pc_md2pc_id,"
                + " md2pc.process_pool_id_proc AS md2pc_process_pool_id_proc,"
                + " process_pool.proc_name AS process_pool_proc_name,"
                + " process_pool.code_operation AS process_pool_code_operation,"
                + " md2pc.sq_process AS md2pc_sq_process,"
                + " lot_control.lot_number AS lot_control_lot_number,"
                + " lot_control.id AS lot_control_id,"
                + " detail_fl.order_no AS detail_fl_order_no,"
                + " detail_fl.qty_frame AS detail_fl_qty_frame,"
                + " detail_fl.plat AS detail_fl_plat,"
                + " detail_fl.productcode_name AS detail_fl_productcode_name,"
                + " detail_fl.productcode AS detail_fl_productcode,"
                + " details.model AS details_model,"
                + " details.line AS details_line,"
                + " detail_fl.product_type AS detail_fl_product_type,"
                + " lot_control.barcode AS lot_control_barcode"
                + " FROM "
                + " lot_control lot_control RIGHT OUTER JOIN md2pc md2pc ON lot_control.model_pool_id = md2pc.model_pool_id"
                + " RIGHT OUTER JOIN process_pool process_pool ON md2pc.process_pool_id_proc = process_pool.id_proc"
                + " INNER JOIN details details ON lot_control.details_id_detail = details.id_detail"
                + " INNER JOIN detail_fl detail_fl ON details.id_detail = detail_fl.detail_id_detail"
                + " WHERE lot_control.deleted=0 and  details.id_detail='" + printworksheet.currentDetails.getIdDetail() + "' and "
                + " lot_control.barcode in (";
        if (printworksheet.flist != null) {
            for (barcode bc : printworksheet.flist) {
                if (bc.isSelectbarcode()) {
                    selectLot += "'" + bc.getBarcode() + "',";
                }
            }
        }
        if (printworksheet.rlist != null) {
            for (barcode bc : printworksheet.rlist) {
                if (bc.isSelectbarcode()) {
                    selectLot += "'" + bc.getBarcode() + "',";
                }
            }
        }
        System.out.println("id lot : " + selectLot);
        sql += selectLot;
        sql += "'') ORDER BY"
                + " lot_control.barcode ASC, "
                + " md2pc.sq_process ASC ;";
        if (!selectLot.equals("")) {
            System.out.println("SQL : " + sql);
            if (new ReportBean().pdfSelect("/report/SelectReport.jasper", "/report/" + printworksheet.currentDetails.getIdDetail() + ".pdf", sql)) {
                System.out.println("complete");
                /*
                 * url ="window.open(\"http://www.w3schools.com\");";
                 * top.preview_html(
                 *
                 */
                url = " alert('Print');"
                        + " window.open('report/" + printworksheet.currentDetails.getIdDetail() + ".pdf');";

                return true;
            }
        }
        return false;

    }

    public void changModelPoolsValue(ValueChangeEvent event) {
        print("========== chang model pool : " + event.toString() + "===============");
        try {
            modelPool = (ModelPool) event.getNewValue();
            print("= model name : " + modelPool.getModelName());
            getDetailFl().setTypeBb(modelPool.getModelName());
            getDetail().setModel(modelPool.getId());
            loadProcess();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

//    public void rollchangvalue(ValueChangeEvent event) {
//        print("========== role qty chang value =================================");
//        try {
//            String roll = event.getNewValue().toString();
//            print("= Roll : " + roll);
//            int rollqty = Integer.parseInt(roll);
//            int tmp = rollqty;
//            int i = 0;
//            while (rolllot < rollqty) {
//                rollqty = rollqty - rolllot;
//                i++;
//            }
//            if (rollqty > 0) {
//                i++;
//            }
//            getDetailFl().setQtyRollQty(tmp + "");
//            getDetailFl().setQtyRoll(i + "");
//        } catch (Exception e) {
//            print("Ex : " + e);
//        }
//    }

//    public void framechangvalue(ValueChangeEvent event) {
//        print("========== frame qty chang value =================================");
//        try {
//            String frame = event.getNewValue().toString();
//            print("= Frame : " + frame);
//            int frameqty = Integer.parseInt(frame);
//            int tmp = frameqty;
//            int i = 0;
//            while (framelot < frameqty) {
//                frameqty = frameqty - framelot;
//                i++;
//            }
//            if (frameqty > 0) {
//                i++;
//            }
//            getDetailFl().setQtyFrameQty(tmp + "");
//            getDetailFl().setQtyFrame(i + "");
//        } catch (Exception e) {
//            print("Ex : " + e);
//        }
//    }

    public void ChangModelpoolValue(ValueChangeEvent event) {
        print("========== modelPool : " + event.getNewValue().toString() + " ======");
        try {
            String id = event.getNewValue().toString();
            modelPool = getModelPoolJpaController().findModelPool(id);
            print("= model name : " + modelPool.getModelName());
            getDetailFl().setTypeBb(modelPool.getModelName());
            getDetail().setModel(id);
            loadProcess();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void ChangModelSeries(ValueChangeEvent event) {
        print("======== Find By model series ===================================");
        modellist = null;
        try {
            String modelseries = event.getNewValue().toString();
            print("= ModelSeries : " + modelseries);
            List<ModelPool> modelPoollList = getModelPoolJpaController().findModelPoolBySeries(modelseries);
            for (ModelPool modelpool : modelPoollList) {
                getModellist().add(new SelectItem(modelpool.getId(), modelpool.getModelName()));
            }
            getDetail().setModelseries(modelseries);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public List<SelectItem> getModelserieslist() {
        if (modelserieslist == null) {
            modelserieslist = new ArrayList<SelectItem>();
            List<String> seriess = getModelPoolJpaController().findModelPoolGroupBySeries();
            for (String serie : seriess) {
                modelserieslist.add(new SelectItem(serie, serie));
            }
        }
        return modelserieslist;
    }

    private List<SelectItem> getModellist() {
        if (modellist == null) {
            modellist = new ArrayList<SelectItem>();
        }
        return modellist;
    }

    public Details getDetail() {
        if (detail == null) {
            detail = new Details();
        }
        return detail;
    }

    public void setDetail(Details detail) {
        this.detail = detail;
    }

    public DetailFl getDetailFl() {
        if (detailFl == null) {
            detailFl = new DetailFl();
        }
        return detailFl;
    }

    public void setDetailFl(DetailFl detailFl) {
        this.detailFl = detailFl;
    }

    public String getScript() {
        return url;
    }
}
