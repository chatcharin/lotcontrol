package com.closeprocess;  
import java.util.Arrays;
import java.util.Comparator;
import com.tct.data.NgPool;
import com.tct.data.jpa.NgPoolJpaController;
import com.closeprocess.CloseProcess;
//import com.component.table.sortable.SortableList;  
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Persistence;
import com.component.barcodeauto.BarcodeAuto;
import com.tct.data.ProcessPool;
public   class inputNG {
	 
	private static final String ngType = "NG Type";
	private static final String ngQty  = "NG Qty";
        
        private NgPoolJpaController  ngPool  = null;
        private List<NgName>  ngName ;
        private int countNumber;
        private String  messageError;
//        private int  ngLeft  = CloseProcess.ngQty;
         
        private  NgPoolJpaController  getNgPoolJpaController(){
            if(ngPool==null){
                ngPool  = new NgPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
            }
        return ngPool;
        } 
	public inputNG(){ 
                loadNgName();
	} 
	/**
	 * @return the ordernumbercolum
	 */
	public String getOrdernumbercolum() {
		return ngType;
	}
	/**
	 * @return the typemodelcolum
	 */
	public String getTypemodelcolum() {
		return ngQty;
	} 
	public void searchWorksheet(){  
            for(NgName  ng : ngName)
            {  
                try{
                      int numQty  =  Integer.parseInt(ng.getNgNums());
                      if(numQty < 0){
                            System.out.println("Check Qty");
                            setMessageError("Check num Qty");
                            break;
                      }else{  
                            System.out.println("NG Name : "+ng.getNgname()+ " QTY : "+ng.getNgNums()); 
                            setMessageError(""); 
                      }
                }catch(NumberFormatException e)  
                {
                     setMessageError("messageError");
                     break;
                } 
               
             }
            
            //----close popup
            if(messageError.equals(""))
            {
            CloseProcess  closeInputNg  =  new CloseProcess();
            closeInputNg.closePopup("inputNG"); 
            } 
        }
	  
	////----------------------------------------------------------------------------------------------------------------------
 
     public  void loadNgName(){
        System.out.println("NgName"); 
            countNumber = 0;
            setNgName(new ArrayList<NgName>()); 
         List<NgPool>  data  =  getNgPoolJpaController().findNgName();
         for(NgPool  cl : data)
         {
             
             getNgName().add(new NgName(cl.getNgName(),"0",countNumber));
             System.out.println("Ngpool : "+cl.getNgName()+" : 0 :" + countNumber); 
           countNumber++;
         }  
     }
     
    public List<NgName> getNgName() {
        return ngName;
    }
 
    public void setNgName(List<NgName> ngName) {
        this.ngName = ngName;
    } 
    public String getMessageError() {
        return messageError;
    }
 
    public void setMessageError(String messageError) {
        this.messageError = messageError;
    }

    /**
     * @return the ngLeft
     */
//    public int getNgLeft() {
//        return ngLeft;
//    }
//
//    /**
//     * @param ngLeft the ngLeft to set
//     */
//    public void setNgLeft(int ngLeft) {
//        this.ngLeft = ngLeft;
//    }

    /**
     * @return the countNumber
     */
    public int getCountNumber() {
        return countNumber;
    }

    /**
     * @param countNumber the countNumber to set
     */
    public void setCountNumber(int countNumber) {
        this.countNumber = countNumber;
    }
     
    public class NgName{
        String ngname;
        String  ngNums;
        private int  indexNgname;
        public NgName(String ngname,String ngNums,int indexNgname){
            this.ngname  = ngname; 
            this.ngNums  = ngNums;
            this.indexNgname  = indexNgname;
        }
        public String getNgname(){
            return ngname;
        }  
        public String getNgNums(){
           return ngNums;
       } 
        public void setNgname(String ngname){
            this.ngname = ngname;
        }  
        public void setNgNums(String ngNums){
           this.ngNums = ngNums;
       } 
        public int getIndexNgname(){
            return indexNgname;
        }
         
    }
}
