package com.closeprocess;

import com.appCinfigpage.bean.fc;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped; 
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Persistence;
import com.component.barcodeauto.BarcodeAuto;
import com.email.bean.SendEmail;
import com.tct.data.Pc2ng;
import com.tct.data.jpa.Pc2ngJpaController;
import com.tct.data.jpa.CurrentProcessJpaController;
import com.tct.data.CurrentProcess;
import com.tct.data.jsf.util.JsfUtil;
import java.util.ResourceBundle;
import java.util.UUID;
import com.time.timeCurrent;
import com.tct.data.MainData;
import com.tct.data.jpa.MainDataJpaController;
import com.tct.data.NgNormal;
import com.tct.data.jpa.NgNormalJpaController;
import com.tct.data.NgRepair;
import com.tct.data.jpa.NgRepairJpaController;
import com.tct.data.PcsQty;
import com.tct.data.jpa.PcsQtyJpaController;
import com.tct.data.jpa.McJpaController;
import java.util.*;
import com.tct.data.NgPool;
import com.tct.data.jpa.NgPoolJpaController;
import com.shareData.ShareDataOfSession;
import com.sql.bean.Sql;
import com.tct.data.*;
import javax.faces.context.FacesContext;
import com.tct.data.Md2pc;
import com.tct.data.jpa.*;
import com.tct.data.ReportSetting;
import com.tct.data.ReportSettingAttribute;

@ApplicationScoped
@ManagedBean(name = "CloseProcess")
public class CloseProcess {

    private boolean closeProcessPopup = false;
    private boolean closeProcessConfirm = false;
    private boolean closeProcessConfirmYes = false;
    private boolean closeProcessConfirmNo = false;
    public static boolean inputNG = false;
    private boolean inputNGRepair = false;
    private String typeClose;
    private String visibles;
    private String ngQty = "0";
    private String ngRepairQty = "0";
    private int countNumber;
    private int countNgRepair;
    private String remark = null;//@pang
    private Pc2ngJpaController pc2ng = null;
    private CurrentProcessJpaController currentProc = null;
    private MainDataJpaController mainDataProc = null;
    private NgNormalJpaController currentNgNormalProc = null;
    private NgRepairJpaController currentNgRepairProc = null;
    private NgPoolJpaController ngPoolJpaController = null;
    private List<NgName> ngNamePool;
    private List<NgRepairList> ngRepairPool;
    private CurrentProcess currentdataAll;
    private PcsQtyJpaController pcsQtydataOut;
    private MainData currentMaindata;
    private NgNormal currentNgNormal;
    private NgRepair currentNgRepair;
    private PcsQty pcsQtydataOutPut;
    private String outPutQty = null;
    private static String ngTypeColum = "NG Type";
    private static String ngQtyColum = "NG Qty";
    private HoldJpaController holdJpaController = null;
    private McDownJpaController mcDownJpaController = null;
    private McMoveJpaController mcMoveJpaController = null;
    private static String ngRepairTypeClomn = "NG Repair type";
    private static String ngRepairQtyColumn = "NG Repair Qty";
    private String messageError;
    private int selectedItemIndex;
    private Md2pcJpaController md2pcJpaController = null;
    private LotControlJpaController lotControlJpaController = null;
    private ReportSettingAttributeJpaController reportSettingAttributeJpaController = null;
    private ReportSettingJpaController reportSettingJpaController = null;
    private SendProblemJpaController sendProblemJpaController = null;
    private String startTime;
    public BarcodeAuto getBarcodeAuto() {
        FacesContext context = FacesContext.getCurrentInstance();
        return (BarcodeAuto) context.getExternalContext().getSessionMap().get("barcode");
    }

    private SendProblemJpaController getSendProblemJpaController() {
        if (sendProblemJpaController == null) {
            sendProblemJpaController = new SendProblemJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return sendProblemJpaController;
    }

    private ReportSettingAttributeJpaController getReportSettingAttributeJpaController() {
        if (reportSettingAttributeJpaController == null) {
            reportSettingAttributeJpaController = new ReportSettingAttributeJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return reportSettingAttributeJpaController;
    }

    public ReportSettingJpaController getReportSettingJpaController() {
        if (reportSettingJpaController == null) {
            reportSettingJpaController = new ReportSettingJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return reportSettingJpaController;
    }

    public ShareDataOfSession getShareData() {
        FacesContext context = FacesContext.getCurrentInstance();
        return (ShareDataOfSession) context.getExternalContext().getSessionMap().get("sharedata");
    } 
    public CurrentProcessJpaController getCurrentProc() {
        if (currentProc == null) {
            currentProc = new CurrentProcessJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return currentProc;
    }

    public Pc2ngJpaController getPc2ngJpaController() {
        if (pc2ng == null) {
            pc2ng = new Pc2ngJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return pc2ng;
    }

    public MainDataJpaController getMaindataJpaController() {
        if (mainDataProc == null) {
            mainDataProc = new MainDataJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mainDataProc;
    }

    public NgNormalJpaController getNormalJpaController() {
        if (currentNgNormalProc == null) {
            currentNgNormalProc = new NgNormalJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return currentNgNormalProc;
    }
    public NgRepairJpaController getNgRepairJpaController() {
        if (currentNgRepairProc == null) {
            currentNgRepairProc = new NgRepairJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return currentNgRepairProc;
    }

    public PcsQtyJpaController getPcsQtyJpaController() {
        if (pcsQtydataOut == null) {
            pcsQtydataOut = new PcsQtyJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return pcsQtydataOut;
    }

    public LotControlJpaController getLotControlJpaController() {
        if (lotControlJpaController == null) {
            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotControlJpaController;

    }

    public NgPoolJpaController getNgPoolJpaController() {
        if (ngPoolJpaController == null) {
            ngPoolJpaController = new NgPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return ngPoolJpaController;
    }
    private McJpaController mcJpaController = null;

    public McJpaController getMcJpaController() {
        if (mcJpaController == null) {
            mcJpaController = new McJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mcJpaController;
    }

    private Md2pcJpaController getMd2pcJpaController() {
        if (md2pcJpaController == null) {
            md2pcJpaController = new Md2pcJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return md2pcJpaController;
    }

    public void closePopup(String typeClose) {
        showCloseButton(); 
        this.typeClose = typeClose;
        if (this.typeClose.equals("inputNG")) { 
            checkLeftProcess();
            showCloseButton(); 
        } else if (this.typeClose.equals("inputNGRepair")) {
            checkLeftNgRepair();
            showCloseButton();
        } else if (this.typeClose.equals("inputNGClose")) {
            inputNG = false;
            setValidateNgLeft("");
        } else if (this.typeClose.equals("inputNGRepairClose")) {
            inputNGRepair = false;

        } else if (this.typeClose.equals("closeProcessPopup")) {
            if (close_button) {
                closeProcessPopup = false;
                closeProcessConfirm = true;
            }
        } else if (this.typeClose.equals("closeProcessPopupClose")) {

            closeProcessPopup = false;
            setNgQty("0");
            setNgRepairQty("0"); 
        } else if (this.typeClose.equals("closeProcessConfirmNo")) {
            closeProcessConfirm = false;
            //closeProcessConfirm     = false; 
        } else if (this.typeClose.equals("closeProcessConfirmYes")) {
        
            closeProcessConfirm = false;
            getBarcodeAuto().status = "Close";
            getShareData().closeProcessButton = true;
            getBarcodeAuto().closeProcessButtonShow = "close_process_button_disable";
            getShareData().mcDownTimeButton = true;
            getBarcodeAuto().mcDownTimeButtonShow = "mc_downtime_button_disable";
            getShareData().holdTimeButton = true;
            getBarcodeAuto().holdTimeButtonShow = "hold_process_button_disable";

            fc.print("************ target ************** " + getTarget() + " percent:" + calPercent());
            if (calPercent() > getTarget()) {
                fc.print("************* Send email ************");
                if (checkSendEmailToday()) { //check mail send today.
                   // SendEmail.mails(); //send email. 

                } else {
                    fc.print("****** Send mail Allready *********");
                }
            } else {
                fc.print("************* Not send email ************");
            }
            insertNGnormaAndNgRepair();
            setNgQty("0");
            setNgRepairQty("0"); 
            ngNamePool = new ArrayList<NgName>();          //*** reset array to begin.
            ngRepairPool = new ArrayList<NgRepairList>();  //*** reset array to begin.
        } else if (this.typeClose.equals("closeProcessConfirmYesClose") || this.typeClose.equals("closeProcessConfirmNoClose")) {
            closeProcessConfirmYes = false;
            closeProcessConfirmNo = false;
        }
    }

    public boolean checkSendEmailToday() {
        String today = Sql.formatDateSqlForNgDetails(new timeCurrent().getDate());
        List<SendProblem> objSendProblem  = getSendProblemJpaController().findAllNotDeletedOrderByDate();
        boolean check = false;
        for(SendProblem objList:objSendProblem)
        {
            if(Sql.formatDateSqlForNgDetails(objList.getDated()).equals(today)&&objList.getSendEmail().equals("false"))
            {
                objList.setSendEmail("true");
                try
                {
                    getSendProblemJpaController().edit(objList);
                }
                catch(Exception e)
                {
                    fc.print("Error not to send mail"+e);
                }
                check = true; 
                break;
            }
            else
            {
                break;
            }
        }
        return check;
    }

    public double calPercent() {

        List<CurrentProcess> objCurrentProcess = getCurrentProc().findCurrentStatusAndOpen(getBarcodeAuto().mainDataId, "Open");
        double inputQty = Double.parseDouble(getPcsQtyJpaController().findPcsQtyCurrentProcessId(objCurrentProcess.get(0)).getQty());
        double ngPercent = (Double.parseDouble(ngQty) * 100) / inputQty;
        return ngPercent;
    }

    public double getTarget() {
        ReportSetting objReport = getReportSettingJpaController().findNameReportSettingNotDeleted("ngdefect");
        List<ReportSettingAttribute> objAttribute = getReportSettingAttributeJpaController().findByIdReportSetting(objReport);
        double target = 0;
        for (ReportSettingAttribute objList : objAttribute) {
            if (objList.getNameReportAtt().equals("target")) {
                target = Double.parseDouble(objList.getValueReportAtt());
                break;
            }
        }
        return target;
    }

    public void openPopupType(String visibles) {
        this.visibles = visibles;
        if (this.visibles.equals("closeProcessPopup")) {
            if (!getShareData().closeProcessButton) {
                closeProcessPopup = true;
                startTime  = startFirstTimeInprocess();
            }
            loadData();  
        } else if (this.visibles.equals("inputNG")) {
            ngNamePool = new ArrayList<NgName>();          //*** reset array to begin.
            ngRepairPool = new ArrayList<NgRepairList>();  //*** reset array to begin.
            inputNG = true;
            ngNamePool  = new ArrayList<NgName>();
            loadListDataNg();
            

        } else if (this.visibles.equals("inputNGrepair")) {
            ngNamePool = new ArrayList<NgName>();          //*** reset array to begin.
            ngRepairPool = new ArrayList<NgRepairList>();  //*** reset array to begin.
            System.out.println("====== input ng repair");
            inputNGRepair = true; 
            loadListDataNgRepair();
            startTime  = startFirstTimeInprocess();  //find first time
        }
    }
    public String startFirstTimeInprocess()
    {
        String time = "";
        CurrentProcess objCurrent  = getCurrentProc().findFirstProcessOpen(getBarcodeAuto().mainDataId);
        time  = objCurrent.getTimeCurrent().toString();
        return time;
    }
    public boolean isCloseProcessPopup() {
        return closeProcessPopup;
    }

    public boolean isCloseProcessConfirm() {
        return closeProcessConfirm;
    }

    public boolean isCloseProcessConfirmYes() {
        return closeProcessConfirmYes;
    }

    public boolean isCloseProcessConfirmNo() {
        return closeProcessConfirmNo;
    }

    public boolean isInputNG() {
        return inputNG;
    }

    public boolean isInputNGRepair() {
        return inputNGRepair;
    }
    //===========checkLeftProcess
    private int sumTotal;

    public int getSumTotal() {
        return sumTotal;
    }

    public void setSumTotal(int sumTotal) {
        this.sumTotal = sumTotal;
    }

    public void checkLeftProcess() {
        System.out.println("=============== Test sumtotal ===============");
        sumTotal = 0;
        int checkNum = 0;
        for (NgName listNgQty : ngNamePool) {
            try {
                if (Integer.parseInt(listNgQty.getNgNums()) < 0) {
                    setValidateNgLeft("Input NGQty mpre than or eqaul zero");
                    inputNG = true;
                    break;
                } else {
                    sumTotal += Integer.parseInt(listNgQty.getNgNums());
                    // System.out.println("Data list is "+listNgQty.getNgNums()); 
                }
            } catch (Exception e) {
                System.out.println("++++++++++++ Error  :" + e);
                setValidateNgLeft("Invalid input");
                checkNum = -1;
                setNgLeft("");
                inputNG = true;
                break;
            }

        }
        if (checkNum != -1) {
            calNgLeft(sumTotal);
        }

    }
    //========== Calculate NGLeft

    public void calNgLeft(int valueOfSumTotal) {
        System.out.println("==================== Value of Sum Total ==================");
        int ngLeftnumSum;
        int ngQtyNum = Integer.parseInt(ngQty);
        ngLeftnumSum = ngQtyNum - valueOfSumTotal;
        if (ngLeftnumSum != 0) {
            setNgLeft(Integer.toString(ngLeftnumSum));
            setValidateNgLeft("NgLeft eqaul zero ");
            inputNG = true;
        } else {
            setValidateNgLeft("");
            setNgLeft(Integer.toString(ngLeftnumSum));
            System.out.println("============ Ngleft sum " + ngLeftnumSum);
            inputNG = false;

        }
    }
    //=========== checkLeftNgRepair();
    private String validateNgleftRepair = null;
    private String ngRepairLeft = ngRepairQty;

    public String getNgRepairLeft() {
        return ngRepairLeft;
    }

    public void setNgRepairLeft(String ngRepairLeft) {
        this.ngRepairLeft = ngRepairLeft;
    }

    public String getValidateNgleftRepair() {
        return validateNgleftRepair;
    }

    public void setValidateNgleftRepair(String validateNgleftRepair) {
        this.validateNgleftRepair = validateNgleftRepair;
    }
    int sumNgRepair;

    public void checkLeftNgRepair() {
        System.out.println("================= Check  ng repair ==============");
        sumNgRepair = 0;
        int checkNgRepairNums = 0;
        for (NgRepairList listNgRepair : ngRepairPool) {
            try {
                if (Integer.parseInt(listNgRepair.getNgRepairNums()) < 0) {
                    setValidateNgleftRepair("Input NGQty mpre than or eqaul zero");
                    inputNGRepair = true;
                    break;
                } else {
                    sumNgRepair += Integer.parseInt(listNgRepair.getNgRepairNums());
                    inputNGRepair = false;
                }

            } catch (Exception e) {
                System.out.println("==========Ng repair error" + e);
                checkNgRepairNums = -1;
                break;
            }
        }
        if (checkNgRepairNums != -1) {
            calNgRepairLeft(sumNgRepair);
        }
    }

    //========= calNgRepairLeft
    public void calNgRepairLeft(int valueOfNgRepairLeft) {
        System.out.println("==================== Value of Sum Total ==================");
        int ngRepairLeftnumSum;
        int ngRepairQtyNum = Integer.parseInt(ngRepairQty);
        ngRepairLeftnumSum = ngRepairQtyNum - valueOfNgRepairLeft;
        if (ngRepairLeftnumSum != 0) {
            setNgRepairLeft(Integer.toString(ngRepairLeftnumSum));
            setValidateNgleftRepair("NgRepair Left eqaul zero ");
            inputNGRepair = true;
        } else {
            setValidateNgleftRepair("");
            setNgRepairLeft(Integer.toString(ngRepairLeftnumSum));
            System.out.println("============ NgRepair left sum " + ngRepairLeftnumSum);
            inputNGRepair = false;
        }
    }
    //========== 
    boolean close_button = false;

    public void showCloseButton() {
        if ((inputNgqtyButton == true && inputNgRepairQtyButton == true)) {
            if (sumTotal == Integer.parseInt(ngQty) && sumNgRepair == Integer.parseInt(ngRepairQty)) {
                setClosePopupButton(true);
                close_button = true;
                System.out.println("show");
                setValidateInputNgQty("");
                setValidateOutput("");
                validateInputNgRepairQty = "";
            } else {
                setClosePopupButton(false);
                close_button = false;
                if (sumTotal != Integer.parseInt(ngQty)) {
                    setValidateInputNgQty("Input NgQty Detail");
                }
                if (sumNgRepair != Integer.parseInt(ngRepairQty)) {
                    validateInputNgRepairQty = "Input NgRepair Qty Detail";
                }
            }
        } else if (inputNgqtyButton == true && ngRepairQty.equals("0")) {
            setValidateOutput("");
            if (sumTotal == Integer.parseInt(ngQty) && !ngNamePool.isEmpty()) {
                setClosePopupButton(true);
                close_button = true;
                System.out.println("show aassss");
                setValidateInputNgQty("");
                validateInputNgRepairQty = "";
            } else {
                setClosePopupButton(false);
                close_button = false;
                setValidateInputNgQty("Input NgQty Detail");
            }
        } else if ((ngQty.equals("0") && inputNgRepairQtyButton == true)) {
            setValidateOutput("");
            if (sumNgRepair == Integer.parseInt(ngRepairQty) && !ngRepairPool.isEmpty()) {
                setClosePopupButton(true);
                close_button = true;
                System.out.println("show aa");
                setValidateInputNgQty("");
                validateInputNgRepairQty = "";
            } else {
                setClosePopupButton(false);
                close_button = false;
                validateInputNgRepairQty = "Input NgRepair Qty Detail";
            }
        } else if ((ngQty.equals("0") && ngRepairQty.equals("0"))) {
            setClosePopupButton(true);
            close_button = true;
            System.out.println("show aa");
            setValidateInputNgQty("");
            validateInputNgRepairQty = "";
        }
    }

    public String getNgQty() {
        return ngQty;
    }

    public void setNgQty(String ngQty) {
        System.out.println("======== Change ng =========" + ngQty);
        this.ngQty = ngQty;

        checkValidateInputNgNormalQty(ngQty);
        calculateOutQty();
        showCloseButton();
    }
    private int ngqty_tmp = 0;
    //======= Show  inputNgQtyButton;
    private boolean inputNgqtyButton = false;

    public boolean getInputNgqtyButton() {
        return inputNgqtyButton;
    }

    public void setInputNgqtyButton(boolean inputNgqtyButton) {
        this.inputNgqtyButton = inputNgqtyButton;
    }
    //======= Show inputNgRepairQtyButton;
    private boolean inputNgRepairQtyButton = false;

    public boolean getInputNgRepairQtyButton() {
        return inputNgRepairQtyButton;
    }

    public void setInputNgRepairQtyButton(boolean inputNgRepairQtyButton) {
        this.inputNgRepairQtyButton = inputNgRepairQtyButton;
    }
    //======= Calculate output 
    //====== outPutQty validate
    private String validateOutput = null;

    public String getValidateOutput() {
        return validateOutput;
    }

    public void setValidateOutput(String validateOutput) {
        this.validateOutput = validateOutput;
    }

    public void calculateOutQty() {

        if ((inputNgqtyButton == true && inputNgRepairQtyButton == true) || (ngQty.equals("0") && ngRepairQty.equals("0")) || (inputNgqtyButton == true && ngRepairQty.equals("0")) || (ngQty.equals("0") && inputNgRepairQtyButton == true)) {
            //===== Get inputQty 
            CurrentProcess currentprocess = getCurrentProc().findCurrentProcess(getShareData().mainDataId.getCurrentNumber());
            PcsQty pcsQtyOutPut = getPcsQtyJpaController().findPcsQtyCurrentProcessId(currentprocess);
            int outPutOpenvalue = Integer.parseInt(pcsQtyOutPut.getQty());
            System.out.println("============ Input open value ============" + outPutOpenvalue);

            //===== Get inputNg
            int inputNgValue = Integer.parseInt(ngQty);
            ngLeft = Integer.toString(inputNgValue);

            //===== Get inputNgRepair
            int inputNgRepairValue = Integer.parseInt(ngRepairQty);

            //==== Calculate  
            int calCal = outPutOpenvalue - (inputNgValue + inputNgRepairValue);

            outPutQty = Integer.toString(calCal); //===== outputQty value
            if (calCal < 0) {
                setValidateOutput("Check NG Qty and NG Repair Qty");
                setInputNgqtyButton(false);
                setInputNgRepairQtyButton(false);
                setClosePopupButton(false);
            } else {
                showCloseButton();
            } 
        } else {
            outPutQty = null;
            setClosePopupButton(false);
            setValidateOutput("Invalid input");
        }
    }

    public String getOutPutQty() {
        return outPutQty;
    }

    public void setOutPutQty(String outPutQty) {
        this.outPutQty = outPutQty;
    } 
    private boolean saveButtonNgQty = false;

    public boolean getSaveButtonNgQty() {
        return saveButtonNgQty;
    }

    public void setSaveButtonNgQty(boolean saveButtonNgQty) {
        this.saveButtonNgQty = saveButtonNgQty;
    } 
    private boolean closePopupButton = false;

    public boolean getClosePopupButton() {
        return closePopupButton;
    }

    public void setClosePopupButton(boolean closePopupButton) {
        this.closePopupButton = closePopupButton;
    }
    //======== Validate inputNgQty
    private String validateInputNgQty = null;

    public String getValidateInputNgQty() {
        return validateInputNgQty;
    }

    public void setValidateInputNgQty(String validateInputNgQty) {
        this.validateInputNgQty = validateInputNgQty;
    }

    public void checkValidateInputNgNormalQty(String ngQty) {
        int intNgQty = 0;
        try {
            intNgQty = Integer.parseInt(ngQty);

            if (intNgQty < 0) {
                validateInputNgQty = "InputQty more than or equal zero";
                inputNgqtyButton = false;
            } else if (intNgQty == 0) {
                validateInputNgQty = "";
                inputNgqtyButton = false;
            } else {
                validateInputNgQty = "";
                inputNgqtyButton = true;

            }
        } catch (Exception e) {
            //System.out.println("++++++++++ Not number ++++++++++");
            validateInputNgQty = "Invalid input";
            inputNgqtyButton = false;
        }

    }

    public String getNgRepairQty() {

        return ngRepairQty;
    }

    public void setNgRepairQty(String ngRepairQty) {
        this.ngRepairQty = ngRepairQty;
        ngRepairLeft = this.ngRepairQty;
        System.out.println("======== Change ng Repair =========" + ngRepairQty);
        chcekValidateNgRepair(ngRepairQty);
        calculateOutQty();
    } 
    private String validateInputNgRepairQty = null;

    public String getValidateInputNgRepairQty() {
        return validateInputNgRepairQty;
    }

    public void setValidateInputNgRepairQty(String validateInputNgRepairQty) {
        this.validateInputNgRepairQty = validateInputNgRepairQty;
    }

    public void chcekValidateNgRepair(String ngRepairQty) {
        int intNgRepairQty;
        try {
            intNgRepairQty = Integer.parseInt(ngRepairQty);
            if (intNgRepairQty < 0) {
                validateInputNgRepairQty = "Input ngRepair more than or equal zero";
                inputNgRepairQtyButton = false;
            } else if (intNgRepairQty == 0) {
                validateInputNgRepairQty = "";
                inputNgRepairQtyButton = false;
            } else {
                validateInputNgRepairQty = "";
                inputNgRepairQtyButton = true;
            }
        } catch (Exception e) {
            validateInputNgRepairQty = "Invalid input";
            inputNgRepairQtyButton = false;
        }

    }
    NgPool objListOne;

    public void loadListDataNg() {
        System.out.println("BarcodeAuto.idProc" + getShareData().idProc);
        countNumber = 0;
        List<Pc2ng> dataPc2ng = getPc2ngJpaController().findIdNgPool(getShareData().idProc);
        setNgNamePool(new ArrayList<NgName>());
        List<String> objList = new ArrayList<String>();
        for (Pc2ng listdata : dataPc2ng) {
            if (listdata.getIdNg().getNgType().equals("ng")) {
                objList.add(new String(listdata.getIdNg().getNgName()));
                fc.print("name" + listdata.getIdNg().getNgName());
            }
        }
        ngNamePool = null;
        objListOne = null;
        Collections.sort(objList);
        for (String list : objList) {
            System.out.println("List data" + list);
            getNgNamePool().add(new NgName(list, "0", countNumber, ""));
            countNumber++;
        }
    }

    public void loadListDataNgRepair() {
        countNgRepair = 0;
        List<Pc2ng> dataPc2ngRepair = getPc2ngJpaController().findIdNgPool(getShareData().idProc);
        List<String> objRepairList = new ArrayList<String>();
        for (Pc2ng listdata : dataPc2ngRepair) {
            if (listdata.getIdNg().getNgType().equals("repair")) {
                objRepairList.add(listdata.getIdNg().getNgName());
            }
        }
        Collections.sort(objRepairList);
        setNgRepairPool(new ArrayList<NgRepairList>());
        for (String listdataRepair : objRepairList) {
            getNgRepairPool().add(new NgRepairList(listdataRepair, "0", countNgRepair, ""));
            countNgRepair++;
        }
    }

    public void insertNGnormaAndNgRepair() {
        System.out.println("============ Main data id : " + getShareData().mainDataId);
        String uuidCurrent = UUID.randomUUID().toString();
        getCloseProcess().setId(uuidCurrent);
        getCloseProcess().setMainDataId(getShareData().mainDataId);
        getCloseProcess().setCurrentProc("1");
        getCloseProcess().setStatus("Close");
        getCloseProcess().setRemark(remark);

        String StatusOpen = "Open";
        List<CurrentProcess> idCurrenthaveMc = getCurrentProc().findCurrentStatusAndOpen(getShareData().mainDataId, StatusOpen);
        String hr = null;
        String line = null;
        for (CurrentProcess objCurent : idCurrenthaveMc) {
            if (objCurent.getCurrentProc().equals("1")) {
                hr = objCurent.getHr();
                line = objCurent.getLine();
                break;
            }
        }
        timeCurrent timeNew = new timeCurrent();
        getCloseProcess().setTimeCurrent(timeNew.getDate());
        getCloseProcess().setLine(line);
        getCloseProcess().setHr(hr);
        createCloseProcessNewRows();

        //--update  current number
        getShareData().mainDataId.setCurrentNumber(uuidCurrent);
        try {
            getMaindataJpaController().edit(getShareData().mainDataId);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("DetailAssyUpdated"));
            //return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            //return null;
        }
        try {

            idCurrenthaveMc.get(0).getMcId().setMcStatus("good");

            try {
                System.out.println("===== Change mc status to << Use >> =====");
                getMcJpaController().edit(idCurrenthaveMc.get(0).getMcId());

            } catch (Exception e) {
                System.out.println("======== Eror=====" + e);
            }
        } catch (Exception e) {
        }

        //--insert  qty  normal  ng
        int sumOfngNormal = 0;
        //if(!ngNamePool.isEmpty()){
        if (ngNamePool != null || !(ngQty.equals("0"))) {
            for (NgName ng : ngNamePool) {
                if (!(ng.getNgNums().equals("0"))) {
                    //--create
                    String uuidNgNormal = UUID.randomUUID().toString();
                    sumOfngNormal = sumOfngNormal + Integer.parseInt(ng.getNgNums());
                    System.out.println("NG Name : " + ng.getNgname() + " QTY : " + ng.getNgNums());
                    getNgnormalProc().setIdNg(uuidNgNormal);
                    getNgnormalProc().setName(ng.getNgname());
                    getNgnormalProc().setQty(ng.getNgNums());
                    getNgnormalProc().setCurrentProcessId(currentdataAll);
                    createNgNormalProc();
                    //currentNgNormal  = new NgNormal();
                    System.out.println("================Ng normal ====================");

                }

                //}  
            }
        }
        //--insert qty  ng reapir
        System.out.println("--------------------------------------------------------------");
        int sumOfRepair = 0;
        //if(!ngRepairPool.isEmpty()){
        if (ngRepairPool != null || !(ngRepairQty.equals("0"))) {
            for (NgRepairList ngRe : ngRepairPool) {
                if (!(ngRe.getNgRepairNums().equals("0"))) {
                    String uuidNgRepair = UUID.randomUUID().toString();
                    sumOfRepair = sumOfRepair + Integer.parseInt(ngRe.getNgRepairNums());
                    System.out.println("Ngrepair name" + ngRe.getNgRepairname() + "Qty repair" + ngRe.getNgRepairNums());
                    getNgRepairPproc().setCurrentProcessId(currentdataAll);
                    getNgRepairPproc().setIdNg(uuidNgRepair);
                    getNgRepairPproc().setName(ngRe.getNgRepairname());
                    getNgRepairPproc().setQty(ngRe.getNgRepairNums());
                    createNgRepairProc();
                    //currentNgRepair  = new NgRepair();
                }
                //} 

            }
        }

        //--insert output qty in pcs_qty
        int outPutQtyNums = getShareData().outputQty - (sumOfRepair + sumOfngNormal);
        getPcsQtyProc().setCurrentProcessId(currentdataAll);
        getPcsQtyProc().setId(uuidCurrent);
        getPcsQtyProc().setQty(Integer.toString(outPutQtyNums));
        getPcsQtyProc().setQtyType("out");
        getPcsQtyProc().setWipQty("0");
        createPcsQtyOutPut(); 
        //******* check status finish
        Md2pc objDataNextProcess = getMd2pcJpaController().findSqProcess(getBarcodeAuto().idProc, getBarcodeAuto().lotControlId.getModelPoolId());
        String idworkSheet  = getBarcodeAuto().showListTypeOnly(getBarcodeAuto().lotControlId);
        Md2pc objDataCheckNewprocess = getBarcodeAuto().sqNewProcess(getBarcodeAuto().lotControlId.getModelPoolId(),idworkSheet,objDataNextProcess); 
        if (objDataCheckNewprocess == null) {
            System.out.println("========================= This process is  Finished ========================");
            getBarcodeAuto().status = "Finished";
            CurrentProcess objCurrentProcess  = getCurrentProc().findCurrentProcess(uuidCurrent);
            objCurrentProcess.setStatus("Finished");
            try {
                getCurrentProc().edit(objCurrentProcess);
            } catch (Exception e) {
                System.out.println("========== Not Change status Current process " + e);
            }
            PcsQty outPcsqty = getPcsQtyJpaController().findPcsQtyCurrentProcessId(currentdataAll);
            outPcsqty.setWipQty(outPcsqty.getQty());
            try {
                getPcsQtyJpaController().edit(outPcsqty);
            } catch (Exception e) {
                System.out.println("========== Not to add wip " + e);
            }

            //======= Update wip and qty_out in lotcontrol 
            getBarcodeAuto().lotControlId.setWipQty(Integer.parseInt(getPcsQtyProc().getQty()));
            getBarcodeAuto().lotControlId.setQtyOutput(Integer.parseInt(getPcsQtyProc().getQty()));
            try {
                getLotControlJpaController().edit(getBarcodeAuto().lotControlId);
            } catch (Exception e) {
                System.out.println("======= Error not to update wip and qtyoutput" + e);
            }
            //======================== Set menu
            getShareData().openProcessButton = true;
            getBarcodeAuto().setOpenProcessButtonShow("open_process_button_disable");
            getShareData().mcDownTimeButton = true;
            getBarcodeAuto().setMcDownTimeButtonShow("mc_downtime_button_disable");
            getShareData().stopTimeButton = true;
            getBarcodeAuto().setStopTimeButtonShow("stop_downtime_button_disable");
            getShareData().moveProcessButton = true;
            getBarcodeAuto().setMoveProcessButtonShow("move_mc_button_disable");
            getShareData().holdTimeButton = true;
            getBarcodeAuto().setHoldTimeButtonShow("hold_process_button_disable");
            getShareData().closeProcessButton = true;
            getBarcodeAuto().setCloseProcessButtonShow("close_process_button_disable");
            getBarcodeAuto().setIdCurrentFinished(currentdataAll);
            getBarcodeAuto().setQaProcessButton(false);
            getBarcodeAuto().setQaProcessButtonShow("qa_button");
        }
        getBarcodeAuto().showListDetailProc(getBarcodeAuto().lotControlId); //**** show process list
        prepareCreateClose();
    }

    public String prepareCreateClose() {
        currentdataAll = new CurrentProcess();
        selectedItemIndex = -1;
        return "Create";
    }

    public void createCloseProcessNewRows() {
        try {
            getCurrentProc().create(currentdataAll);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("MainDataCreated"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    public CurrentProcess getCloseProcess() {
        if (currentdataAll == null) {
            currentdataAll = new CurrentProcess();
        }
        return currentdataAll;
    }

    public void createNgNormalProc() {
        try {
            getNormalJpaController().create(currentNgNormal);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("MainDataCreated"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    public NgNormal getNgnormalProc() {
        if (currentNgNormal == null) {
            currentNgNormal = new NgNormal();
        }
        return currentNgNormal;
    }

    public void createNgRepairProc() {
        try {
            getNgRepairJpaController().create(currentNgRepair);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("MainDataCreated"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    public NgRepair getNgRepairPproc() {
        if (currentNgRepair == null) {
            currentNgRepair = new NgRepair();
        }
        return currentNgRepair;
    }

    public void createPcsQtyOutPut() {
        try {
            getPcsQtyJpaController().create(pcsQtydataOutPut);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("MainDataCreated"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    public PcsQty getPcsQtyProc() {
        if (pcsQtydataOutPut == null) {
            pcsQtydataOutPut = new PcsQty();
        }
        return pcsQtydataOutPut;
    }

    public int getCountNumber() {
        return countNumber;
    }

    public void setCountNumber(int countNumber) {
        this.countNumber = countNumber;
    }

    public int getCountNgRepair() {
        return countNgRepair;
    }

    public void setCountNgRepair(int countNgRepair) {
        this.countNgRepair = countNgRepair;
    }

    public List<NgName> getNgNamePool() {
        if (ngNamePool == null) {
            ngNamePool = new ArrayList<NgName>();
        }
        return ngNamePool;
    }
    //========= Ng left
    private String ngLeft;

    public String getNgLeft() {
        return ngLeft;
    }

    public void setNgLeft(String ngLeft) {
        this.ngLeft = ngLeft;
    }
    //========= Validate Sum ngleft
    private String validateNgLeft = null;

    public String getValidateNgLeft() {
        return validateNgLeft;
    }

    public void setValidateNgLeft(String validateNgLeft) {
        this.validateNgLeft = validateNgLeft;
    }

    //========= Sum ngleft
    public void setNgNamePool(List<NgName> ngNamePool) {
        this.ngNamePool = ngNamePool;
        System.out.println("++++++++++++++++ Test +++++++++++++++++");
//       
    }

    public void saveButton() {
        int sumListOne = 0;
        for (NgName countNgName : ngNamePool) {
            try {
                sumListOne += Integer.parseInt(countNgName.getNgNums());
            } catch (Exception e) {
                setNgLeft("-1");
                setValidateNgLeft("Invalid input");
                break;
            }
        }
        //setNgLeft(Integer.toString(sumListOne));
        if (!ngLeft.equals("-1")) {
            calculateNgLeft(sumListOne);
        } else {
            ngLeft = "";
        }

    }

    //======= Calculate Ng Left
    public void calculateNgLeft(int sumList) {
        if (sumList != Integer.parseInt(ngLeft)) {
            setValidateNgLeft("Invalid input");
        } else {
            int calNgLect = Integer.parseInt(ngLeft) - sumList;
            setNgLeft(Integer.toString(calNgLect));
        }

    }

    public List<NgRepairList> getNgRepairPool() {
        return ngRepairPool;
    }

    public void setNgRepairPool(List<NgRepairList> ngRepairPool) {
        this.ngRepairPool = ngRepairPool;

    }

    public static String getNgQtyColum() {
        return ngQtyColum;
    }

    public String getMessageError() {
        return messageError;
    }

    public void setMessageError(String messageError) {
        this.messageError = messageError;
    }

    public String getOrdernumbercolum() {
        return ngTypeColum;
    }

    public String getTypemodelcolum() {
        return ngQtyColum;
    }

    public String getNgRepairTypeClumn() {
        return ngRepairTypeClomn;
    }

    public String getNgRepairqtyColumn() {
        return ngRepairQtyColumn;
    }

    public void loadData() {
        getShareData().mainDataId.getCurrentNumber();
        CurrentProcess nowCurrentProcess = getCurrentProc().findCurrentProcess(getShareData().mainDataId.getCurrentNumber());
        PcsQty inputQty = getPcsQtyJpaController().findPcsQtyCurrentProcessId(nowCurrentProcess);
        outPutQty = inputQty.getQty();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    } 
}