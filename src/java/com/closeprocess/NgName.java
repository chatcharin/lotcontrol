/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.closeprocess;

/**
 *
 * @author pang
 */
public class NgName{
        String ngname;
        String  ngNums;
        private int  indexNgname;
        String  validateNgQtyOne;
        public NgName(String ngname,String ngNums,int indexNgname,String validateNgQtyOne){
            this.ngname  = ngname; 
            this.ngNums  = ngNums;
            this.indexNgname  = indexNgname;
            this.validateNgQtyOne  = validateNgQtyOne;
        }
        public String getNgname(){
            return ngname;
        }  
        public String getNgNums(){
           return ngNums;
       } 
        public void setNgname(String ngname){
            this.ngname = ngname; 
        }  
        public void setNgNums(String ngNums){
           this.ngNums = ngNums; 
           int intNgNums ;
            try{
                intNgNums = Integer.parseInt(getNgNums());
                if(intNgNums < 0)
                {
                       setValidateNgQtyOne("More than or equal zero");
                }
                else
                {
                      setValidateNgQtyOne("");   
                }
            }
            catch(Exception e)
            {
                 //===== Invalid number 
               setValidateNgQtyOne("Invalid input"); 
            }
       } 
        public int getIndexNgname(){
            return indexNgname;
        } 
        public String getValidateNgQtyOne()
        {
            return validateNgQtyOne;
        }
        public void setValidateNgQtyOne(String validateNgQtyOne)
        {
            this.validateNgQtyOne  = validateNgQtyOne;
        } 
        
    } 
