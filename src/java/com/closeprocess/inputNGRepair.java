package com.closeprocess; 
import java.util.Arrays;
import java.util.Comparator;

import com.component.table.sortable.SortableList;

/**
 * @author The_Boy_Cs
 *
 */
public class inputNGRepair extends SortableList{
	
	private String ordernumber;
	private String typemodel;
	private String process;
	private String typepoppin;
	
	private static final String ngType = "NG Repair Type";
	private static final String ngQty  = "NG Repair Qty"; 
	 
	public inputNGRepair(){
		super(ngType);
	}

	/**
	 * @return the ordernumbercolum
	 */
	public String getOrdernumbercolum() {
		return ngType;
	}
	/**
	 * @return the typemodelcolum
	 */
	public String getTypemodelcolum() {
		return ngQty;
	}

	/**
	 * @return the typebobbincolum
	 */
	/*public String getTypebobbincolum() {
		return typeBobbinColum;
	}

	*//**
	 * @return the datecreatecolum
	 *//*
	public String getDatecreatecolum() {
		return dateCreateColum;
	}

	*//**
	 * @return the processcolum
	 *//*
	public String getProcesscolum() {
		return processColum;
	}

	*//**
	 * @return the statuscolum
	 *//*
	public String getStatuscolum() {
		return statusColum;
	}*/

	/**
	 * @return the ordernumber
	 */
	public String getOrdernumber() {
		return ordernumber;
	}

	/**
	 * @param ordernumber the ordernumber to set
	 */
	public void setOrdernumber(String ordernumber) {
		this.ordernumber = ordernumber;
	}

	/**
	 * @return the typemodel
	 */
	public String getTypemodel() {
		return typemodel;
	}

	/**
	 * @param typemodel the typemodel to set
	 */
	public void setTypemodel(String typemodel) {
		this.typemodel = typemodel;
	}

	/**
	 * @return the process
	 */
	public String getProcess() {
		return process;
	}

	/**
	 * @param process the process to set
	 */
	public void setProcess(String process) {
		this.process = process;
	}

	/**
	 * @return the typepoppin
	 */
	public String getTypepoppin() {
		return typepoppin;
	}

	/**
	 * @param typepoppin the typepoppin to set
	 */
	public void setTypepoppin(String typepoppin) {
		this.typepoppin = typepoppin;
	}
	
	public void searchWorksheet(){
		
	}
	
	protected boolean isDefaultAscending(String sortColumn) {
        return true;
    }
	
	@SuppressWarnings("unchecked")
	protected void sort() {
	    Comparator comparator = new Comparator() {
	        public int compare(Object o1, Object o2) {
	            InventoryItem c1 = (InventoryItem) o1;
	            InventoryItem c2 = (InventoryItem) o2;
	            if (sortColumnName == null) {
	                return 0;
	            }
	           /* if (sortColumnName.equals(ngType)) {
	                return ascending ?
	                Integer.valueOf(c1.getStock()).compareTo(Integer.valueOf(c2.getStock())) :
	                Integer.valueOf(c2.getStock()).compareTo(Integer.valueOf(c1.getStock()));
	            } else */if (sortColumnName.equals(ngQty)) {
	                return ascending ? c1.getModel().compareTo(c2.getModel()) :
	                c2.getModel().compareTo(c1.getModel());
	            } /*else if (sortColumnName.equals(typeBobbinColum)) {
	                return ascending ? c1.getDescription().compareTo(c2.getDescription()) :
	                c2.getDescription().compareTo(c1.getDescription());
	            } else if (sortColumnName.equals(dateCreateColum)) {
	                return ascending ?
	                Integer.valueOf(c1.getOdometer()).compareTo(Integer.valueOf(c2.getOdometer())) :
	                Integer.valueOf(c2.getOdometer()).compareTo(Integer.valueOf(c1.getOdometer()));
	            } else if (sortColumnName.equals(processColum)) {
	                return ascending ?
	                Integer.valueOf(c1.getPrice()).compareTo(Integer.valueOf(c2.getPrice())) :
	                Integer.valueOf(c2.getPrice()).compareTo(Integer.valueOf(c1.getPrice()));
	            }  else if (sortColumnName.equals(statusColum)) {
	                return ascending ?
	                Integer.valueOf(c1.getPrice()).compareTo(Integer.valueOf(c2.getPrice())) :
	                Integer.valueOf(c2.getPrice()).compareTo(Integer.valueOf(c1.getPrice()));
	            } */ else return 0;
	        }
	    };
	    Arrays.sort(carInventory, comparator);
	}
	
	////----------------------------------------------------------------------------------------------------------------------
	private InventoryItem[] carInventory = new InventoryItem[]{
			
			 new InventoryItem("Dodge Grand Caravan"),
	         new InventoryItem("Dodge SX 2.0") 
    };

    /**
     * Gets the inventoryItem array of car data.
     * @return array of car inventory data.
     */
    public InventoryItem[] getCarInventory() {
    	if (!oldSort.equals(sortColumnName) ||
    	        oldAscending != ascending){
    	        sort();
    	        oldSort = sortColumnName;
    	        oldAscending = ascending;
    	    }
        return carInventory;
    }
	public class InventoryItem {
        // slock number
       /* int stock;*/
        // model or type of inventory
        String model;
        // description of item
       /* String description;
        // number of miles on odometer
        int odometer;
        // price of car in Canadian dollars
        int price;*/

        /**
         * Creates a new instance of InventoryItem.
         * @param stock stock number.
         * @param model model or type of inventory.
         * @param description description of item.
         * @param odometer number of miles on odometer.
         * @param price price of care in Canadian dollars.
         */
        //public InventoryItem(int stock, String model, String description, int odometer, int price) {
        /*	public InventoryItem(int stock, String model) {	*/
        public InventoryItem( String model) {	
          /*  this.stock = stock;*/
            this.model = model;
          /*  this.description = description;
            this.odometer = odometer;
            this.price = price;*/
        }

        /**
         * Gets the stock number of this iventory item.
         * @return stock number.
         */
     /*   public int getStock() {
            return stock;
        }*/

        /**
         * Gets the model number of this iventory item.
         * @return model number.
         */
        public String getModel() {
            return model;
        }

        /**
         * Gets the description of the this iventory item.
         * @return description
         */
       /* public String getDescription() {
            return description;
        }

        *//**
         * Gets the odometer reading from this iventory item.
         * @return  odometer reading.
         *//*
        public int getOdometer() {
            return odometer;
        }

        *//**
         * Gets the price of this item in Canadian Dollars.
         * @return price.
         *//*
        public int getPrice() {
            return price;
        }*/

    }

}
