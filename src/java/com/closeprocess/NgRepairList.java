/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.closeprocess;

/**
 *
 * @author pang
 */
 public class NgRepairList{
        String  ngRepairname;
        String  ngRepairNums;
        private int  indexNgRepairname;
        String  validateNgRepairOne;
        public NgRepairList(String ngRepairname,String ngRepairNums,int indexNgRepairname,String validateNgRepairOne){
            this.ngRepairname        = ngRepairname; 
            this.ngRepairNums        = ngRepairNums;
            this.indexNgRepairname   = indexNgRepairname;
            this.validateNgRepairOne = validateNgRepairOne;
        }
        public String getNgRepairname(){
            return ngRepairname;
        }  
        public String getNgRepairNums(){
           return ngRepairNums;
       } 
        public void setNgRepairname(String ngRepairname){
            this.ngRepairname = ngRepairname;
        }  
        public void setNgRepairNums(String ngRepairNums){
           this.ngRepairNums = ngRepairNums; 
           int intNgRepairNums ;
            try{
                intNgRepairNums = Integer.parseInt(getNgRepairNums());
                if(intNgRepairNums < 0)
                {
                       setValidateNgRepairOne("More than or equal zero");
                }
                else
                {
                       setValidateNgRepairOne("");   
                }
            }
            catch(Exception e)
            {
                 //===== Invalid number 
               setValidateNgRepairOne("Invalid input"); 
            } 
       } 
        public int getIndexNgRepairname(){
            return indexNgRepairname;
        }  
        public String getValidateNgRepairOne()
        {
            return validateNgRepairOne;
        }
        public void setValidateNgRepairOne(String validateNgRepairOne)
        {
            this.validateNgRepairOne = validateNgRepairOne;
        }
    } 
