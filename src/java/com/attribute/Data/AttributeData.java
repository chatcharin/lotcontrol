/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.attribute.Data;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 17, 2012, Time : 4:54:40 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class AttributeData {
    private String indexField;
    private String indexValue;
    private String nameValue;

    public AttributeData(String indexField, String indexValue, String nameValue) {
        this.indexField = indexField;
        this.indexValue = indexValue;
        this.nameValue = nameValue;
    }

    public String getIndexField() {
        return indexField;
    }

    public void setIndexField(String indexField) {
        this.indexField = indexField;
    }

    public String getIndexValue() {
        return indexValue;
    }

    public void setIndexValue(String indexValue) {
        this.indexValue = indexValue;
    }

    public String getNameValue() {
        return nameValue;
    }

    public void setNameValue(String nameValue) {
        this.nameValue = nameValue;
    } 
}
