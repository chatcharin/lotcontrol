/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.attribute.Data;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 18, 2012, Time : 11:22:21 AM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class AttributeDetailTable {
    private String index;
    private String type;
    private String nameFiled;
    private String detail;

    public AttributeDetailTable(String index, String type, String nameFiled, String detail) {
        this.index = index;
        this.type = type;
        this.nameFiled = nameFiled;
        this.detail = detail;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getNameFiled() {
        return nameFiled;
    }

    public void setNameFiled(String nameFiled) {
        this.nameFiled = nameFiled;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
