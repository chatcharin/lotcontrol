/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.attribute.bean;

import com.appCinfigpage.bean.fc;
import com.appCinfigpage.bean.userLogin;
import com.attribute.Data.AttributeData;
import com.attribute.Data.AttributeDetailTable;
import com.tct.data.FieldName;
import com.tct.data.GroupField;
import com.tct.data.jpa.FieldNameJpaController;
import com.tct.data.jpa.GroupFieldJpaController;
import com.tct.data.jsf.util.PaginationHelper;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.ArrayDataModel;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;
/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 17, 2012, Time : 4:00:25 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class Attribute {
    private List<SelectItem>  selectAttributeType; 
    private String selectAttributeTypeSearch;
    private boolean valueAttribute  = false;
    private List<AttributeData>  itemAttributeValue;
    private String nameValue = null;
    private int requestNum = 0;
    private String nameField = null;
    private String indexField = null;
    private FieldName currentFilename;
    private GroupField  currentGroupField;
    private FieldNameJpaController  fieldNameJpaController = null;
    private GroupFieldJpaController groupFieldJpaController = null; 
    private List<AttributeDetailTable>   detailTable  ;
    private PaginationHelper worksheetpagination;
    private DataModel worksheet;
    
     public Attribute() {
        loadAttributeType();
         loadDataTable();
    }
    public FieldNameJpaController getFieldNameJpaController()
    {
        if(fieldNameJpaController ==null)
        {
            fieldNameJpaController  =  new FieldNameJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return fieldNameJpaController;
    }
    public GroupFieldJpaController getGroupFieldJpaController()
    {
        if(groupFieldJpaController ==null)
        {
            groupFieldJpaController  =  new GroupFieldJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return groupFieldJpaController;
    }
    
   
    public void resetAllData()
    {
        worksheetpagination = null;  //**** Reset 
        currentFilename   = null;
        currentGroupField = null;
        requestNum        = 0;
        itemAttributeValue = new ArrayList<AttributeData>();
    }
    public void changeToInsert()
    {
        resetAllData();
        indexField  = UUID.randomUUID().toString();
        String link = "pages/setting/attribute/createview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
    }
    public void changeToCreate()
    {
        getCurrentFilename().setId(indexField);
        //getCurrentFilename().setName(nameField);
        //getCurrentFilename().setName(getCurrentFilename().getName());
        if(!selectAttributeTypeSearch.equals("selectbox")){
            getCurrentFilename().setType("inputbox");
        }
        else
        {
            getCurrentFilename().setType("selectbox");
        }
        try
        {
            getFieldNameJpaController().create(currentFilename);
        }
        catch(Exception e)
        {
            fc.print("Error not to insert"+e);
        }
        
        if(selectAttributeTypeSearch.equals("selectbox"))
        {
            for(AttributeData  objlistAttribute:getItemAttributeValue()){ 
                getCurrentGroupField().setId(gemId());
                getCurrentGroupField().setDataText(objlistAttribute.getNameValue());
                getCurrentGroupField().setIdFieldName(currentFilename);
//                getCurrentGroupField().set
                try
                {
                    getGroupFieldJpaController().create(currentGroupField);
                    currentGroupField  = new GroupField();
                }
                catch(Exception e)
                {
                    fc.print("Error not to insert group data");
                }
            }
        }
        resetAllData();
        worksheet  = null;
        detailTable  = new  ArrayList<AttributeDetailTable>();
         search();
        String link = "pages/setting/attribute/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
    } 
    public boolean  requestBooleanValue()
    {
        boolean value ;
        if(currentFilename.getRequest()==1 && selectAttributeTypeSearch.equals("selectbox"))
        {
            value  = true;
        }
        else
        {
            value = false;
        }     
        return value;  
    }
    public void changeToEdit(String indexEdit)
    {
        resetAllData();
        currentFilename  = getFieldNameJpaController().findFieldName(indexEdit);
        if(currentFilename.getType().equals("selectbox")){
            fc.print("Show value");
           // setValueAttribute(true); 
            valueAttribute  = true;
            List<GroupField>  objGroub  = getGroupFieldJpaController().findIdFieldName(currentFilename);
            for(GroupField  listObj:objGroub)
            {
               getItemAttributeValue().add(new AttributeData(listObj.getIdFieldName().toString(),listObj.getId(),listObj.getDataText() )); 
            } 
        }   
        else
        {
            valueAttribute  = false;
        }
        selectAttributeTypeSearch = currentFilename.getType(); 
        String link = "pages/setting/attribute/editview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
    }
    public void createFieldName()
    {
        try
        {
            getFieldNameJpaController().edit(getCurrentFilename());
        }
        catch(Exception e)
        {
            fc.print("Error not to update field name "+e);
        }
    }
    
    public void changeToSave()
    { 
        if(!selectAttributeTypeSearch.equals("null")){
            currentFilename.setType(selectAttributeTypeSearch);
            if(selectAttributeTypeSearch.equals("selectbox"))
            {
                currentFilename.setUseFieldName(1);
            }
            else
            {
                currentFilename.setUseFieldName(0);
                List<GroupField> objGroup  = getGroupFieldJpaController().findIdFieldName(getCurrentFilename());
                if(!objGroup.isEmpty()){ 
                    for(GroupField objlist:objGroup)
                    {
                        objlist.setDeleted(1);
                        try
                        {
                            getGroupFieldJpaController().edit(objlist);
                            //currentGroupField  = new GroupField();
                            fc.print("del");
                        }
                        catch(Exception e)
                        {
                            fc.print("Error not to del "+e);
                        }
                        }
                }
            }
        } 
        editFieldName(); 
        if(getCurrentFilename().getType().equals("selectbox")){
        for(AttributeData  listObj:getItemAttributeValue())
            {  
//                currentGroupField  = new GroupField();
                fc.print("index id : "+listObj.getIndexValue());
                String IndexValue = listObj.getIndexValue();
                currentGroupField  = getGroupFieldJpaController().findIdFieldGroubId(IndexValue);
                fc.print("Current group field : "+currentGroupField);
                if(getCurrentGroupField().getId()==null||getCurrentGroupField().getId().equals(""))
                { 
                    getCurrentGroupField().setId(listObj.getIndexValue());
                    getCurrentGroupField().setDataText(listObj.getNameValue());
                    getCurrentGroupField().setIdFieldName(currentFilename);
                    try
                    {
                        getGroupFieldJpaController().create(getCurrentGroupField());
                        //currentGroupField  = new GroupField();
                        fc.print("create");
                    }
                    catch(Exception e)
                    {
                        fc.print("Error not to seve "+e);
                    }
                }
            }
        }
        resetAllData();
        worksheet  = null;
        detailTable  = new  ArrayList<AttributeDetailTable>();
        search();
        String link = "pages/setting/attribute/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
   
    }
    public void changeToCancel()
    {
        String link = "pages/setting/attribute/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
    }
    public void changeToDelete(String indexDel)
    {
        resetAllData();
        worksheet  = null;
        detailTable  = new  ArrayList<AttributeDetailTable>();
        currentFilename = getFieldNameJpaController().findFieldName(indexDel);
        currentFilename.setDeleted(1);
        editFieldName();
        if(currentFilename.getType().equals("selectbox")){
            List<GroupField>  objGroub = getGroupFieldJpaController().findIdFieldName(currentFilename);
            for(GroupField objlist:objGroub)
            {   
                objlist.setDeleted(1);
                 try
                {
                    getGroupFieldJpaController().edit(objlist);

                }
                catch(Exception e)
                {
                    fc.print("Error not to deleted group field");
                }
            }
        }
         search();
        String link = "pages/setting/attribute/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
    }     
    public String gemId()
    {
        String id  = UUID.randomUUID().toString();
        return id;
    } 
    public void editFieldName()
    {
        try
        {
            getFieldNameJpaController().edit(currentFilename);
        }
        catch(Exception e)
        {
            fc.print("Not to delete :"+e);
        } 
    } 
    private void loadAttributeType() {
        getSelectAttributeType().add(new SelectItem("null","---- Select one -----"));
        getSelectAttributeType().add(new SelectItem("inputbox","Input box"));
        getSelectAttributeType().add(new SelectItem("selectbox","Select box"));
    } 
    public void selectItemChangeValue(ValueChangeEvent event)
    {
        selectAttributeTypeSearch = (String) event.getNewValue();
        fc.print("selectAttributeSearch : "+selectAttributeTypeSearch); 
        //showValue();
         if(selectAttributeTypeSearch.equals("selectbox")&& !(selectAttributeTypeSearch.equals("null")))
        {
           valueAttribute = true;
           
        }
        else
        {
          valueAttribute = false;
          
        }
        
        fc.print("valueAttribute : "+valueAttribute);
    }
    public int requestBoolean(ValueChangeEvent event)
    {
        boolean  request = (Boolean)event.getNewValue();
        if(request)
        {
            requestNum = 1; 
            
        }
        else
        {
            requestNum = 0; 
        }
        currentFilename.setRequest(requestNum);
        fc.print("request num : "+requestNum);
        return requestNum;
    }
    
    public boolean showValue()
    {
        if(selectAttributeTypeSearch.equals("selectbox")&& !(selectAttributeTypeSearch.equals("null")))
        {
           valueAttribute = true;
        }
        else
        {
          valueAttribute = false;
        }
        return valueAttribute;
    }
    public void addValue()
    {
        fc.print("======== Add value ========");
        String indexValue  = UUID.randomUUID().toString();
        getItemAttributeValue().add(new AttributeData(indexField, indexValue, nameValue));
        resetValue();
        //getItemAttributeValue().add();
    }
    public void addValueEdit()
    {
        fc.print("======== Add value edit  ========");
        //String indexValue  = UUID.randomUUID().toString();
        getItemAttributeValue().add(new AttributeData(indexField,gemId(),nameValue));
        resetValue();
        //getItemAttributeValue().add();
    }
    public void delValueEdit(String indexValue)
    {
        for(AttributeData listAttribute : getItemAttributeValue())
        {
            if(listAttribute.getIndexValue().equals(indexValue))
            {
                currentGroupField  = new GroupField();
                fc.print("index id : "+indexValue);
                currentGroupField  = getGroupFieldJpaController().findGroupField(indexValue);
              
                if(getCurrentGroupField()!=null)
                    {
                        getCurrentGroupField().setDeleted(1);
                    try
                    {
                        getGroupFieldJpaController().edit(getCurrentGroupField());

                    }
                    catch(Exception e)
                    {
                        fc.print("Error not to del value edit"+e);
                    }
                     
                 }
                 getItemAttributeValue().remove(listAttribute);
                 break;
            }
        }
    } 
    
    public void resetValue()
    {
         nameValue  = "";
    }
    public void actionDeleteValue(String indexValue)
    {
        for(AttributeData listAttribute : getItemAttributeValue())
        {
            if(listAttribute.getIndexValue().equals(indexValue))
            {
                 getItemAttributeValue().remove(listAttribute);
                 break;
            }
        }
    }
    public void search()
    {
        if(!selectAttributeTypeSearch.equals("null"))
        {
            resetAllData();
            worksheet  = null;
            detailTable  = new  ArrayList<AttributeDetailTable>();
            searchbyType();
            resetKeySearch();
             fc.print("search by type field");
        }
        else if(nameField!=null && !nameField.trim().isEmpty() )
        {
            resetAllData();
            worksheet  = null;
            detailTable  = new  ArrayList<AttributeDetailTable>();
            serchByFilename();
            fc.print("search by name field");
        }
        else
        {
            resetAllData();
            worksheet  = null;
            detailTable  = new  ArrayList<AttributeDetailTable>(); 
            loadDataTable();
            fc.print("search by all ");
        }
        
    }
    public void resetKeySearch()
    {
        selectAttributeTypeSearch   = null;
        nameField                   = null;  
    }
    public void searchbyType()
    { 
        List<FieldName> objFiledName = getFieldNameJpaController().findByType(selectAttributeTypeSearch);
        for(FieldName objList : objFiledName)
        {
            getDetailTable().add(new AttributeDetailTable(objList.getId(),objList.getType(), objList.getName(), objList.getDetail()));
        }
        setWorksheet(); 
    } 
     public void serchByFilename()
    { 
        List<FieldName> objFiledName = getFieldNameJpaController().findByFieldName(nameField);
        for(FieldName objList : objFiledName)
        {
            getDetailTable().add(new AttributeDetailTable(objList.getId(),objList.getType(), objList.getName(), objList.getDetail()));
        }
        setWorksheet(); 
    } 
    public void  loadDataTable()
    {
        List<FieldName> objFiledName = getFieldNameJpaController().findAllNotDeleted();
        for(FieldName objList : objFiledName)
        {
            getDetailTable().add(new AttributeDetailTable(objList.getId(),objList.getType(), objList.getName(), objList.getDetail()));
        }
        setWorksheet(); 
    }      
    public DataModel getWorksheet() {
        return worksheet;
    }

    public void setWorksheet() {
        this.worksheet = getWorksheetpagination().createPageDataModel();
    }
    public void previous() {
        getWorksheetpagination().previousPage();
        setWorksheet();
    }

    public void next() {
        getWorksheetpagination().nextPage();
        setWorksheet();
    }

    public int getAllPages() {
        int all = getWorksheetpagination().getItemsCount();
        int pagesize = getWorksheetpagination().getPageSize();
        return ((int) all / pagesize) + 1;
    } 
    public PaginationHelper getWorksheetpagination() {
        if (worksheetpagination == null) {
            worksheetpagination = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    return getDetailTable().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    fc.print("PageFirst : " + getPageFirstItem());
                    fc.print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (getDetailTable().size() > 0) {
                        listmodel = new ListDataModel(getDetailTable().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(getDetailTable());
                    }
                    return listmodel;
                }
            };
        }
        return worksheetpagination;
    }  
    public List<SelectItem> getSelectAttributeType() {
        if(selectAttributeType == null)
        {
            selectAttributeType  = new ArrayList<SelectItem>();
        }
        return selectAttributeType;
    }

    public void setSelectAttributeType(List<SelectItem> selectAttributeType) {
        this.selectAttributeType = selectAttributeType;
    }

    public String getSelectAttributeTypeSearch() {
        return selectAttributeTypeSearch;
    }

    public void setSelectAttributeTypeSearch(String selectAttributeTypeSearch) {
        this.selectAttributeTypeSearch = selectAttributeTypeSearch;
    }

    public boolean isValueAttribute() {
        return valueAttribute;
    }

    public void setValueAttribute(boolean valueAttribute) {
        this.valueAttribute = valueAttribute;
    }

    public List<AttributeData> getItemAttributeValue() {
        if(itemAttributeValue == null)
        {
            itemAttributeValue  = new ArrayList<AttributeData>();
        }
        return itemAttributeValue;
    }

    public void setItemAttributeValue(List<AttributeData> itemAttributeValue) {
        this.itemAttributeValue = itemAttributeValue;
    }

    public String getNameValue() {
        return nameValue;
    }

    public void setNameValue(String nameValue) {
        this.nameValue = nameValue;
    }

    public String getIndexField() {
        return indexField;
    }

    public void setIndexField(String indexField) {
        this.indexField = indexField; 
    }

    public FieldName getCurrentFilename() {
        if(currentFilename == null)
        {
            currentFilename  = new FieldName();
        }
        return currentFilename;
    }

    public void setCurrentFilename(FieldName currentFilename) {
        this.currentFilename = currentFilename;
    }

    public GroupField getCurrentGroupField() {
        if(currentGroupField == null)
        {
            currentGroupField  = new GroupField();
        }
        return currentGroupField;
    }

    public void setCurrentGroupField(GroupField currentGroupField) {
        this.currentGroupField = currentGroupField;
    }

    public String getNameField() {
        return nameField;
    }

    public void setNameField(String nameField) {
        this.nameField = nameField;
    }

    public List<AttributeDetailTable> getDetailTable() {
        if(detailTable  == null)
        {
            detailTable  = new ArrayList<AttributeDetailTable>();
        }
        return detailTable;
    }

    public void setDetailTable(List<AttributeDetailTable> detailTable) {
        this.detailTable = detailTable;
    }

    public int getRequestNum() {
        return requestNum;
    }

    public void setRequestNum(int requestNum) {
        this.requestNum = requestNum;
    }
    
}
