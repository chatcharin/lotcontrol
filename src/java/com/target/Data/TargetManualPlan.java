/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.target.Data;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 25, 2012, Time : 5:19:11 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class TargetManualPlan {
    private String indexLine;
    private String indexManualPlan;
    private String time;
    private String qty;
    private String result;
    private String defect;
    private String defectRate;

    public TargetManualPlan(String indexLine, String indexManualPlan, String time, String qty, String result, String defect, String defectRate) {
        this.indexLine = indexLine;
        this.indexManualPlan = indexManualPlan;
        this.time = time;
        this.qty = qty;
        this.result = result;
        this.defect = defect;
        this.defectRate = defectRate;
    } 
    public String getDefect() {
        return defect;
    }

    public void setDefect(String defect) {
        this.defect = defect;
    }

    public String getDefectRate() {
        return defectRate;
    }

    public void setDefectRate(String defectRate) {
        this.defectRate = defectRate;
    }

    public String getIndexLine() {
        return indexLine;
    }

    public void setIndexLine(String indexLine) {
        this.indexLine = indexLine;
    }

    public String getIndexManualPlan() {
        return indexManualPlan;
    }

    public void setIndexManualPlan(String indexManualPlan) {
        this.indexManualPlan = indexManualPlan;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
    
}
