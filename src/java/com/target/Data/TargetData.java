/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.target.Data;

import com.tct.data.ModelPool;
import java.util.Date;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 22, 2012, Time : 3:02:40 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class TargetData {
    private String  index;
    private int  num;
    private Date date;
    private String startTime;
    private String endTime;
    private String qty;
    private String modelName;
    private ModelPool indexModel;

    public TargetData(String index, int num, Date date, String startTime, String endTime, String qty, String modelName, ModelPool indexModel) {
        this.index = index;
        this.num = num;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.qty = qty;
        this.modelName = modelName;
        this.indexModel = indexModel;
    } 
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    } 
    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public ModelPool getIndexModel() {
        return indexModel;
    }

    public void setIndexModel(ModelPool indexModel) {
        this.indexModel = indexModel;
    } 
}
