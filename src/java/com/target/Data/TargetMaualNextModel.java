/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.target.Data;

import java.util.Date;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 25, 2012, Time : 5:19:34 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class TargetMaualNextModel {
    private String indexLine;
    private String indexManualNextModel;
    private Date date;
    private String model;
    private String quatility;

    public TargetMaualNextModel(String indexLine, String indexManualNextModel, Date date, String model, String quatility) {
        this.indexLine = indexLine;
        this.indexManualNextModel = indexManualNextModel;
        this.date = date;
        this.model = model;
        this.quatility = quatility;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getIndexLine() {
        return indexLine;
    }

    public void setIndexLine(String indexLine) {
        this.indexLine = indexLine;
    }

    public String getIndexManualNextModel() {
        return indexManualNextModel;
    }

    public void setIndexManualNextModel(String indexManualNextModel) {
        this.indexManualNextModel = indexManualNextModel;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getQuatility() {
        return quatility;
    }

    public void setQuatility(String quatility) {
        this.quatility = quatility;
    }
    
}
