/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setting.sdttime;

import com.appCinfigpage.bean.fc;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tawat Seakue
 */
public class SdtTimeSettingControl {

    private String headername;
    private List<HeaderDetail> headerDetails;

    public void changToAdd() {
        fc.print("================= chang to add header name ===================");
        try {
            fc.print("Step ------------ 1");
            if (checkHeaderName()) {

                fc.print("Step ------------ 2");
                fc.print("Header name : " + headername);
            }
            fc.print("Step ------------ 3");
        } catch (Exception e) {
            fc.print("Error : " + e);
        }
    }

    private boolean checkHeaderName() {
        if (headername != null && !headername.isEmpty()) {
            return true;
        }
        return false;
    }

    public List<HeaderDetail> getHeaderDetails() {
        if (headerDetails == null) {
            headerDetails = new ArrayList<HeaderDetail>();
            headerDetails.add(new HeaderDetail("24/10/2012", "Test"));
        }
        return headerDetails;
    }

    public void setHeaderDetails(List<HeaderDetail> headerDetails) {
        this.headerDetails = headerDetails;
    }

    public String getHeadername() {
        return headername;
    }

    public void setHeadername(String headername) {
        this.headername = headername;
    }

    public class HeaderDetail {

        private String datecreate;
        private String name;

        public HeaderDetail(String datecreate, String name) {
            this.datecreate = datecreate;
            this.name = name;
        }

        public String getDatecreate() {
            return datecreate;
        }

        public void setDatecreate(String datecreate) {
            this.datecreate = datecreate;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
