/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setting.bean;

import com.appCinfigpage.bean.userLogin;
import com.tct.data.Settimetorefresh;
import com.tct.data.jpa.SettimetorefreshJpaController;
import java.util.Date;
import java.util.UUID;
import javax.persistence.Persistence;

/**
 *
 * @author TheBoyCs
 */
public class SetTimeToRefreshControl {

    private Settimetorefresh current;
    private String inserterror;

    public SetTimeToRefreshControl() {
    }

    public void changToSave() {
        print("================ save ===========================================");
        if (check()) {
            inserterror = "";
            try {
                String userid = userLogin.getSessionUser().getId();
                getCurrent().setUserCreate(userid);
                getCurrent().setDateCreate(new Date());
                getSettimetorefreshJpaController().create(current);
                print("=== Complete ===");
                String link = "report/index.xhtml";
                userLogin.getSessionMenuBarBean().setParam(link);
                current = null;
            } catch (Exception e) {
                print("Insert Error : " + e);
                inserterror = "Error :: Don't setting value.";
            }
        } else {
            inserterror = "Check value to input.";
        }
    }
    private SettimetorefreshJpaController settimetorefreshJpaController;

    private SettimetorefreshJpaController getSettimetorefreshJpaController() {
        if (settimetorefreshJpaController == null) {
            settimetorefreshJpaController = new SettimetorefreshJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return settimetorefreshJpaController;
    }

    public Settimetorefresh getCurrent() {
        if (current == null) {
            current = new Settimetorefresh(UUID.randomUUID().toString());
        }
        return current;
    }

    public void setCurrent(Settimetorefresh current) {
        this.current = current;
    }

    public String getInserterror() {
        return inserterror;
    }

    public void setInserterror(String inserterror) {
        this.inserterror = inserterror;
    }

    private void print(String _save_) {
        System.out.println(_save_);
    }

    private boolean check() {
        print("=============== check ===========================================");
        boolean c = true;
        try {
            if (getCurrent().getRefreshAuto() == null || getCurrent().getRefreshAuto() <= 0) {
                c = false;
            }
            if (getCurrent().getRefreshLine() == null || getCurrent().getRefreshLine() <= 0) {
                c = false;
            }
            if (getCurrent().getRefreshManual() == null || getCurrent().getRefreshManual() <= 0) {
                c = false;
            }
            if (getCurrent().getSlideAuto() == null || getCurrent().getSlideAuto() <= 0) {
                c = false;
            }
            if (getCurrent().getSlideLine() == null || getCurrent().getSlideLine() <= 0) {
                c = false;
            }
            if (getCurrent().getSlideManual() == null || getCurrent().getSlideManual() <= 0) {
                c = false;
            }
            return c;
        } catch (Exception e) {
            print("Check Error : " + e);
            return false;
        }
    }
}
