/**
 *
 */
package com.setting.bean;

import com.appCinfigpage.bean.userLogin;
import com.icesoft.faces.component.ext.RowSelectorEvent;
import com.tct.data.Md2pc;
import com.tct.data.ModelPool;
import com.tct.data.ProcessPool;
import com.tct.data.jpa.Md2pcJpaController;
import com.tct.data.jpa.ModelPoolJpaController;
import com.tct.data.jpa.ProcessPoolJpaController;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;

/**
 * @author The_Boy_Cs
 *
 */
public class editModulesStep {  //  this class name is "editmodelstep" page.

    private List<SelectItem> type;
    private String typeselected;
    private List<SelectItem> Series;
    private String seriesselected;
    private List<SelectItem> modelselect;
    private String modelselected;
    private String newtype;
    private String newseries;
    private String newmodel;
    private String newgroup;
    private List<Md2pc> processSteplist;
    private List<ProcessPool> processlistpool;
    private boolean selectedStatus = true;

    public editModulesStep() {
        loadModel();
        loadProcessPool();
    }

    private void loadModel() {
        modelselect = new ArrayList<SelectItem>();
        List<ModelPool> models = getModelPoolJpaController().findModelPoolGroupByName();
        for (ModelPool model : models) {
            modelselect.add(new SelectItem(model.getModelName(), model.getModelName()));
        }
    }

    private void loadProcessPool() {
        print("=================================================================");
        try {
            processlistpool = getProcessPoolJpaController().findProcessPoolEntities();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }
    private ProcessPoolJpaController processPoolJpaController = null;

    private ProcessPoolJpaController getProcessPoolJpaController() {
        if (processPoolJpaController == null) {
            processPoolJpaController = new ProcessPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return processPoolJpaController;
    }
    private ModelPoolJpaController modelPoolJpaController = null;

    private ModelPoolJpaController getModelPoolJpaController() {
        if (modelPoolJpaController == null) {
            modelPoolJpaController = new ModelPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return modelPoolJpaController;
    }

    public void modelChangValueListener(ValueChangeEvent event) {
        print("==================== Chang model name =======================");
        selectedStatus = true;
        typeselected = "";
        processSteplist = null;
        modelselected = event.getNewValue().toString();
        print("= Modelselected : " + modelselected);
        print("= New Value : " + modelselected);
        try {
            List<ModelPool> models = getModelPoolJpaController().findByModelname(modelselected);
            Series = new ArrayList<SelectItem>();
            for (ModelPool model : models) {
                print("= Series : " + model.getModelSeries());
                Series.add(new SelectItem(model.getId(), model.getModelSeries()));
            }
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }
    private ModelPool modelPool;

    public void seriesChangValueListener(ValueChangeEvent event) {
        print("=============================================================");
//            seriesselected = event.getNewValue().toString();
        String modelId = event.getNewValue().toString();
        print("= new Value" + modelId);
        try {
            modelPool = getModelPoolJpaController().findModelPool(modelId);
            seriesselected = modelPool.getModelSeries();
            typeselected = modelPool.getModelType();
            selectedStatus = false;
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }
    private Md2pcJpaController md2pcJpaController = null;

    private Md2pcJpaController getMd2pcJpaController() {
        if (md2pcJpaController == null) {
            md2pcJpaController = new Md2pcJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return md2pcJpaController;
    }

    public void actionSelectedModel(ActionEvent event) {
        print("=================================================================");
        try {
            loadProcessOnModel();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    private void loadProcessOnModel() {
        loadProcessPool();
        if (modelPool != null) {
            processSteplist = getMd2pcJpaController().findBymodelPoolId(modelPool);
            for (Md2pc md2pcs : processSteplist) {
                ProcessPool pcpools = md2pcs.getProcessPoolIdProc();
                try {
                    if (processlistpool.remove(pcpools)) {
                        print("= Remove pcpool : " + pcpools.getProcName());
                    }
                } catch (Exception e) {
                    print("= Error : " + e);
                }
            }
            print("= Mc2pcData : ");
        } else {
            print("= You don't Select Model.");
        }
    }
    private ProcessPool pcpool;

    public void selectProcessPoolListener(RowSelectorEvent event) {
        print("=================================================================");
        print("= new Value Selector num : " + event.getRow());
        int index = event.getRow();
        pcpool = processlistpool.get(index);
        print("= ProcessPool selected : " + pcpool);
    }
    private Md2pc md2pc = null;

    public void selectMd2pcListener(RowSelectorEvent event) {
        print("=================================================================");
        print("= new Value Selector num : " + event.getRow());
        int index = event.getRow();
        md2pc = processSteplist.get(index);
        print("= Md2pc selected id" + md2pc.getMd2pcId());
    }

    private int getSQnumber() {
        print("=================================================================");
        try {
            List<Md2pc> md2pclists = getMd2pcJpaController().findBymodelPoolId(modelPool);
            int sqnumber = 0;
            for (Md2pc md2pcs : md2pclists) {
                int sq = md2pcs.getSqProcess();
                print("= this ");
                if (sqnumber < sq) {
                    sqnumber = sq;
                    print("= Sq : " + sq);
                }
            }
            sqnumber++;
            return sqnumber;
        } catch (Exception e) {
            print("= Error : " + e);
            return 1;
        }
    }

    public void addProcessToModel(ActionEvent event) {
        print("=================================================================");
        try {
            if (modelPool != null && pcpool != null) {
                md2pc = new Md2pc();
                md2pc.setCreateDate(new Date());
                md2pc.setMd2pcId(UUID.randomUUID().toString());
                md2pc.setModelPoolId(modelPool);
                md2pc.setProcessPoolIdProc(pcpool);
                md2pc.setUserCreate(userLogin.getSessionUser().getId());
                md2pc.setSqProcess(getSQnumber());

                getMd2pcJpaController().create(md2pc);
                print("= InsertComplete.");
                md2pc = null;
                loadProcessOnModel();
            } else {
                print("= It not have Model ro Process");
            }
        } catch (PreexistingEntityException peex) {
            print("= Error1 : " + peex);
        } catch (Exception e) {
            print("= Error : " + e);
        }
        print("=");
    }

    public void deletProcessOnModel(ActionEvent event) {
        print("=================================================================");
        print("= Delect Process On Model");
        try {
            if (md2pc != null) {
                int sq = md2pc.getSqProcess();
                getMd2pcJpaController().destroy(md2pc.getMd2pcId());
                print("= Deleted");
                resetSQ(sq);
            }
            md2pc = null;
        } catch (Exception e) {
            print("= Error : " + e);
        }
        loadProcessOnModel();
    }

    private void resetSQ(int sq) {
        print("= Reset SQ.");
        try {
            List<Md2pc> md2pclist = getMd2pcJpaController().findBymodelPoolId(modelPool);
            for (Md2pc md2pcs : md2pclist) {
                int sqs = md2pcs.getSqProcess();
                int tmp = 0;
                print("= First SQ : " + sq + " SQS : " + sqs);
                if (sq < sqs) {
                    tmp = sqs;
                    sqs = sq;
                    sq = tmp;
                    md2pcs.setSqProcess(sqs);
                    getMd2pcJpaController().edit(md2pcs);
                }
                print("= Sescund SQ : " + sq + " SQS : " + sqs);
            }
            print("= Reset SQ Completed.");
        } catch (Exception e) {
            print("= Error reset SQ : " + e);
        }
    }

    public void createModel(ActionEvent event) {
        print("=================================================================");
        try {
            modelPool = new ModelPool();
            modelPool.setId(UUID.randomUUID().toString());
            modelPool.setModelGroup(newgroup);
            modelPool.setModelName(newmodel);
            modelPool.setModelSeries(newseries);
            modelPool.setModelType(newtype);
            modelPool.setUserCreate(userLogin.getSessionUser().getId());
            modelPool.setCreateDate(new Date());
            modelPool.setPlat("");

            getModelPoolJpaController().create(modelPool);
            modelPool = null;
            print("= Completed");
            loadModel();
//            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("DetailAssyCreated"));
        } catch (Exception e) {
//            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            print("= Error : " + e);
        }
    }

    public List<SelectItem> getType() {
        return type;
    }

    public void setType(List<SelectItem> type) {
        this.type = type;
    }

    /**
     * @return the series
     */
    public List<SelectItem> getSeries() {
        return Series;
    }

    /**
     * @param series the series to set
     */
    public void setSeries(List<SelectItem> series) {
        Series = series;
    }

    /**
     * @return the model
     */
    public List<SelectItem> getModelselect() {

        return modelselect;
    }

    /**
     * @return the newtype
     */
    public String getNewtype() {
        return newtype;
    }

    /**
     * @param newtype the newtype to set
     */
    public void setNewtype(String newtype) {
        this.newtype = newtype;
    }

    /**
     * @return the newseries
     */
    public String getNewseries() {
        return newseries;
    }

    /**
     * @param newseries the newseries to set
     */
    public void setNewseries(String newseries) {
        this.newseries = newseries;
    }

    /**
     * @return the newmodel
     */
    public String getNewmodel() {
        return newmodel;
    }

    /**
     * @param newmodel the newmodel to set
     */
    public void setNewmodel(String newmodel) {
        this.newmodel = newmodel;
    }

    /**
     * @return the typeselected
     */
    public String getTypeselected() {
        return typeselected;
    }

    /**
     * @param typeselected the typeselected to set
     */
    public void setTypeselected(String typeselected) {
        this.typeselected = typeselected;
    }

    /**
     * @return the seriesselected
     */
    public String getSeriesselected() {
        return seriesselected;
    }

    /**
     * @param seriesselected the seriesselected to set
     */
    public void setSeriesselected(String seriesselected) {
        this.seriesselected = seriesselected;
    }

    /**
     * @return the moduleselected
     */
    public String getModelselected() {
        return modelselected;
    }

    /**
     * @param moduleselected the moduleselected to set
     */
    public void setModelselected(String moduleselected) {
        this.modelselected = moduleselected;
    }

    /**
     * @return the processSteplist
     */
    public List<Md2pc> getProcessSteplist() {
        return processSteplist;
    }

    /**
     * @return the processlistpool
     */
    public List<ProcessPool> getProcesslistpool() {
        return processlistpool;
    }

    private void print(String str) {
        System.out.println(str);
    }

    public boolean isSelectedStatus() {
        return selectedStatus;
    }

    public void setSelectedStatus(boolean selectedStatus) {
        this.selectedStatus = selectedStatus;
    }

    public String getNewgroup() {
        return newgroup;
    }

    public void setNewgroup(String newgroup) {
        this.newgroup = newgroup;
    }
}