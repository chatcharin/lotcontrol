/**
 * 
 */
package com.setting.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import com.icesoft.faces.component.ext.RowSelectorEvent;
import com.setting.DataProcess.DataProcess;
import com.setting.DataProcess.NGPool;

/**
 * @author The_Boy_Cs
 * 
 */
public class editNGBean {
	private String ngname;
	private String usercreate;
	private String status;
	private List<SelectItem> statuslist;
	private Date createdate;// = new Date();

	private List<NGPool> ngpoollist;
	private List<NGPool> processnglist;
	private List<DataProcess> proclist;

	private int intable = 0;
	private NGPool ng = null;

	public editNGBean() {
		statuslist = new ArrayList<SelectItem>();
		for (int i = 0; i < 10; i++) {
			statuslist.add(new SelectItem("AAAA" + i));
		}
		createdate = new GregorianCalendar().getTime();
		setNgpoollist(loadNGPool());
		ngpoollist.add(new NGPool("dfffff"));

		proclist = loadProcList();

		processnglist = loadProcessNG(null);
	}

	public void ProcessPoolListener(RowSelectorEvent event) {
		System.out.println("Processlist : " + event.getRow());
		intable = 1;
		DataProcess pro = proclist.get(event.getRow());
		processnglist = loadProcessNG(pro);
	}

	public void ProcessNGListener(RowSelectorEvent event) {
		System.out.println("ProcessNG : " + event.getRow());
		intable = 2;
		ng = processnglist.get(event.getRow());
	}

	public void NGPoolListener(RowSelectorEvent event) {
		System.out.println("NGPoll : " + event.getRow());
		intable = 3;
		ng = ngpoollist.get(event.getRow());
	}

	public void addNgToProcess() {
		if (ng != null && intable == 3) {
			processnglist.add(ng);
			// ngpoollist.remove(ng);
		}
		ng = null;
	}

	public void delNgFromProcess() {
		if (ng != null && intable == 2) {
			// ngpoollist.add(ng);
			processnglist.remove(ng);
		}
		ng = null;
	}

	private List<NGPool> loadProcessNG(DataProcess proc) {

		List<NGPool> pngl = new ArrayList<NGPool>();
		if (proc != null) {
			pngl.add(new NGPool("ff"));
			pngl.add(new NGPool("ff"));
			pngl.add(new NGPool("ff"));
		}
		return pngl;
	}

	private List<NGPool> loadNGPool() {

		return new ArrayList<NGPool>();
	}

	private List<DataProcess> loadProcList() {
		List<DataProcess> pl = new ArrayList<DataProcess>();
		pl.add(new DataProcess("ssss1"));
		pl.add(new DataProcess("ssss2"));
		pl.add(new DataProcess("ssss3"));
		return pl;
	}

	public void valueChangStatus(ValueChangeEvent event) {
		System.out.println("Eventchang" + event.getNewValue().toString());
	}

	public void CreateNewNG(ActionEvent event) {
		if (!ngname.isEmpty()) {
			ngpoollist.add(new NGPool(ngname));
		} else {
			System.out.println("Error::");
		}
		ngname = "";
		usercreate = "";
	}

	/**
	 * @return the ngname
	 */
	public String getNgname() {
		return ngname;
	}

	/**
	 * @param ngname
	 *            the ngname to set
	 */
	public void setNgname(String ngname) {
		this.ngname = ngname;
	}

	/**
	 * @return the usercreate
	 */
	public String getUsercreate() {
		return usercreate;
	}

	/**
	 * @param usercreate
	 *            the usercreate to set
	 */
	public void setUsercreate(String usercreate) {
		this.usercreate = usercreate;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the createdate
	 */
	public Date getCreatedate() {
		return createdate;
	}

	/**
	 * @param createdate
	 *            the createdate to set
	 */
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	/**
	 * @return the statuslist
	 */
	public List<SelectItem> getStatuslist() {
		return statuslist;
	}

	/**
	 * @param statuslist
	 *            the statuslist to set
	 */
	public void setStatuslist(List<SelectItem> statuslist) {
		this.statuslist = statuslist;
	}

	/**
	 * @return the ngpoollist
	 */
	public List<NGPool> getNgpoollist() {
		return ngpoollist;
	}

	/**
	 * @param ngpoollist
	 *            the ngpoollist to set
	 */
	public void setNgpoollist(List<NGPool> ngpoollist) {
		this.ngpoollist = ngpoollist;
	}

	/**
	 * @return the proclist
	 */
	public List<DataProcess> getProclist() {
		return proclist;
	}

	/**
	 * @param proclist
	 *            the proclist to set
	 */
	public void setProclist(List<DataProcess> proclist) {
		this.proclist = proclist;
	}

	/**
	 * @return the processnglist
	 */
	public List<NGPool> getProcessnglist() {
		return processnglist;
	}

	/**
	 * @param processnglist
	 *            the processnglist to set
	 */
	public void setProcessnglist(List<NGPool> processnglist) {
		this.processnglist = processnglist;
	}

	/**
	 * @return the ng
	 */
	public NGPool getNg() {
		return ng;
	}

	/**
	 * @param ng
	 *            the ng to set
	 */
	public void setNg(NGPool ng) {
		this.ng = ng;
	}
}
