/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.setting.bean; 

import com.appCinfigpage.bean.fc;
import com.appCinfigpage.bean.userLogin;
import com.setting.DataProcess.NGPool;
import com.setting.Dataworksheet.editngDetail;
import com.tct.data.NgPool;
import java.util.ArrayList;
import java.util.List;
import com.tct.data.Pc2ng;
import com.tct.data.jpa.NgPoolJpaController; 
import com.tct.data.jsf.util.PaginationHelper;
import com.time.timeCurrent;
import java.awt.event.ActionEvent;
import java.util.UUID;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence; 
/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 6, 2012, Time : 10:20:44 AM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class editngControl {
    private List<editngDetail>  editngcontrolData;
    private NgPoolJpaController  ngPoolJpaController  = null;
    private List<NgPool>         itemPc2ng = null;   
    private DataModel worksheet; 
    private PaginationHelper worksheetpagination;
    private List<SelectItem>   itemNgType ; 
    private List<SelectItem>   itemNgStatus;
    private String  ngNameSearch;
    private String  ngCodeSearch;
    private String  ngTypeSearch;
    private String  ngStatusSearch;
    private NgPool  currentPool;
    
    public editngControl() {
        searchEditngList();
        loadItemNgType();
        loadItemNgStatus();
    }
    private  NgPoolJpaController  getNgPoolJpaController()
    {
        if(ngPoolJpaController == null)
        {
            ngPoolJpaController  = new  NgPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return ngPoolJpaController;
    }
    public List<editngDetail> getEditngcontrolData() {
        if(editngcontrolData  == null)
        {
            editngcontrolData  = new ArrayList<editngDetail>();
        }
        return editngcontrolData;
    }
    public  void changeToInsert()
    {  
        currentPool  = null;
        editngcontrolData  = null;
        itemPc2ng    = null;
        String link  = "pages/setting/editng/createview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link); 
    }
    public void changeToSave()
    {
        getCurrentPool().setNgCode(currentPool.getNgCode());
        getCurrentPool().setNgName(currentPool.getNgName());
        getCurrentPool().setStatus(currentPool.getStatus());
        getCurrentPool().setNgType(currentPool.getNgType());
        try
        {
            getNgPoolJpaController().edit(getCurrentPool());
        }
        catch(Exception e)
        {
            fc.print("Error not changeToSave"+e);
        }
        loadSearchEditListAll(); 
        String link = "pages/setting/editng/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link); 
    }
    public void changeToEdit(String index)
    { 
        currentPool  = null;
        itemPc2ng    = null;
        //editngDetail  editdetail  = (editngDetail)  worksheet.getRowData();
        currentPool  = getNgPoolJpaController().findNgPool(index);
        String link = "pages/setting/editng/editview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link); 
    }
    public void changToDelete(String index)
    {
        currentPool  = null;
        itemPc2ng    = null;
        currentPool  = getNgPoolJpaController().findNgPool(index);
        getCurrentPool().setDeleted(1);
        try
        {
            getNgPoolJpaController().edit(getCurrentPool());
        }
        catch(Exception e)
        {
            fc.print("Error"+e);
        }
        String link = "pages/setting/editng/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link); 
        loadSearchEditListAll(); 
    }        
    public void changeToCreate()
    {
        String  idngNew  = UUID.randomUUID().toString();
        getCurrentPool().setIdNg(idngNew);
        getCurrentPool().setNgCode(currentPool.getNgCode());
        getCurrentPool().setNgName(currentPool.getNgName());
        getCurrentPool().setStatus(currentPool.getStatus());
        getCurrentPool().setCreateDate(new timeCurrent().getDate());
        getCurrentPool().setUserCreate(userLogin.getSessionUser().getUserName());
        getCurrentPool().setUserApprove("");
        getCurrentPool().setUserModifire("");
        getCurrentPool().setDateModifire(new timeCurrent().getDate());
        try{
            getNgPoolJpaController().create(getCurrentPool());
            fc.print("======== Insert new ng ========");
        }
        catch(Exception e )
        {
            fc.print("Eror "+e);
        }
        String link = "pages/setting/editng/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);  
        currentPool  = null;
        editngcontrolData  = null;
        itemPc2ng    = null;
        loadSearchEditListAll();
    } 
    public void changeToCancel()
    {
        String link = "pages/setting/editng/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link); 
    }
    
    private void loadSearchEditListAll() {
         editngcontrolData  = null;
         itemPc2ng  = null;
         itemPc2ng  = getNgPoolJpaController().findByNotdeleted();
         for(NgPool itempc2ndg : itemPc2ng)
         {
             getEditngcontrolData().add(new editngDetail(itempc2ndg.getIdNg(),itempc2ndg.getNgCode(),itempc2ndg.getNgName(), itempc2ndg.getNgType(),itempc2ndg.getStatus(),itempc2ndg.getCreateDate().toString(), itempc2ndg.getUserCreate(),null, null));
             fc.print(itempc2ndg.getNgCode());
         }
         setWorksheet();
     }
     public void previous() {
        getWorksheetpagination().previousPage();
        setWorksheet();
    }

    public void next() {
        getWorksheetpagination().nextPage();
        setWorksheet();
    }

    public int getAllPages() {
        int all = getWorksheetpagination().getItemsCount();
        int pagesize = getWorksheetpagination().getPageSize();
        return ((int) all / pagesize) + 1;
    }
    public PaginationHelper getWorksheetpagination() {
        if (worksheetpagination == null) {
            worksheetpagination = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    return getEditngcontrolData().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    fc.print("PageFirst : " + getPageFirstItem());
                    fc.print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (getEditngcontrolData().size() > 0) {
                        listmodel = new ListDataModel(getEditngcontrolData().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(getEditngcontrolData());
                    }
                    return listmodel;
                }
            };
        }
        return worksheetpagination;
    }
    
    private void loadItemNgType() {
        getItemNgType().add(new SelectItem("","--- Select one ----"));
        getItemNgType().add(new SelectItem("consider","    Consider      "));
        getItemNgType().add(new SelectItem("ng","    Ng      "));
        getItemNgType().add(new SelectItem("repair","    Repair      "));
       
    }
    private void loadItemNgStatus() {
        getItemNgStatus().add(new SelectItem("","--- Select one ----"));
        getItemNgStatus().add(new SelectItem("Active","    Active         "));
        getItemNgStatus().add(new SelectItem("Inactive","    Inactive       ")); 
    }
    public void ChangeValueNgStatus(ValueChangeEvent event)
    {
        ngStatusSearch  = (String) event.getNewValue();
        fc.print("ngNameSearch"+ngNameSearch);
    }
    public void ChangeValueNgType(ValueChangeEvent event)
    {
        ngTypeSearch  = (String) event.getNewValue();
        fc.print("ngTypeSearch"+ngTypeSearch);
    }
    
    public void setEditngcontrolData(List<editngDetail> editngcontrolData) {
        this.editngcontrolData = editngcontrolData;
    }

    public void searchEditngList() { 
        currentPool  = null; 
        worksheetpagination = null;
        if(ngNameSearch != null && !ngNameSearch.trim().isEmpty())
        {
            searchByNgName(); 
            fc.print("ngNameSearch"+ngNameSearch);
        }
        else if(ngCodeSearch != null && !ngCodeSearch.trim().isEmpty())
        {
            searchByNgCode();
            fc.print("ngCodeSearch");
        }
        else if(ngTypeSearch != null && !ngTypeSearch.trim().isEmpty())
        {
            searchByNgType();
            fc.print("ngTypeSearch"+ngTypeSearch);
        }
        else if(ngStatusSearch != null && !ngStatusSearch.trim().isEmpty())
        {
            searchByNgStatus();
            fc.print("ngStatusSearch"+ngStatusSearch);
        }
        else
        {
            loadSearchEditListAll();
        }
       
    }
    public void resetData()
    {
        ngNameSearch  = null;
        ngTypeSearch  = null;
        ngTypeSearch  = null;
        ngStatusSearch = null;
    }  
    public void  searchByNgName()
    {
//       editngcontrolData  = null;
//       itemPc2ng  = null;
      try{
       fc.print("testsearch"); 
       //editngcontrolData  = new ArrayList<editngDetail>();  
       editngcontrolData  = null;
       itemPc2ng  = null;
       itemPc2ng  = getNgPoolJpaController().findByNgNameList(ngNameSearch);
       if(itemPc2ng.isEmpty())
       {
           fc.print("null");
       }
       for(NgPool itempc2ndg : itemPc2ng)
         {   
             getEditngcontrolData().add(new editngDetail(itempc2ndg.getIdNg(),itempc2ndg.getNgCode(),itempc2ndg.getNgName(), itempc2ndg.getNgType(),itempc2ndg.getStatus(),itempc2ndg.getCreateDate().toString(), itempc2ndg.getUserCreate(),null, null));
             fc.print(itempc2ndg.getNgCode());
         }
         setWorksheet();
         resetData();
      }
      catch(Exception e){
          fc.print("Error"+e);
      }
    }
    public void  searchByNgCode()
    {   
        editngcontrolData  = null;
        itemPc2ng  = null;
        itemPc2ng  = getNgPoolJpaController().findByNgCode(ngCodeSearch);
        for(NgPool itempc2ndg : itemPc2ng)
         {
             getEditngcontrolData().add(new editngDetail(itempc2ndg.getIdNg(),itempc2ndg.getNgCode(),itempc2ndg.getNgName(), itempc2ndg.getNgType(),itempc2ndg.getStatus(),itempc2ndg.getCreateDate().toString(), itempc2ndg.getUserCreate(),null, null));
             fc.print(itempc2ndg.getNgCode());
         }
         setWorksheet();
         resetData();
    }
    public void searchByNgType()
    {
        editngcontrolData  = null;
        itemPc2ng  = null;
        itemPc2ng  = getNgPoolJpaController().findByNgType(ngTypeSearch);
         for(NgPool itempc2ndg : itemPc2ng)
         {
             getEditngcontrolData().add(new editngDetail(itempc2ndg.getIdNg(),itempc2ndg.getNgCode(),itempc2ndg.getNgName(), itempc2ndg.getNgType(),itempc2ndg.getStatus(),itempc2ndg.getCreateDate().toString(), itempc2ndg.getUserCreate(),null, null));
             fc.print(itempc2ndg.getNgCode());
         }
         setWorksheet(); 
         resetData();
    }
    public void searchByNgStatus()
    {
         editngcontrolData  = null;
         itemPc2ng  = null; 
         itemPc2ng  = getNgPoolJpaController().findByNgStatus(ngStatusSearch);
         for(NgPool itempc2ndg : itemPc2ng)
         {
             getEditngcontrolData().add(new editngDetail(itempc2ndg.getIdNg(),itempc2ndg.getNgCode(),itempc2ndg.getNgName(), itempc2ndg.getNgType(),itempc2ndg.getStatus(),itempc2ndg.getCreateDate().toString(), itempc2ndg.getUserCreate(),null, null));
             fc.print(itempc2ndg.getNgCode());
         }
         setWorksheet(); 
         resetData();
    }
    public List<NgPool> getItemPc2ng() {
       
        return itemPc2ng;
    }

    public void setItemPc2ng(List<NgPool> itemPc2ng) {
        if(itemPc2ng ==null)
        {
            itemPc2ng  = new ArrayList<NgPool>();
        } 
        this.itemPc2ng = itemPc2ng;
    }

    public DataModel getWorksheet() {
        return worksheet;
    }

    public void setWorksheet() {
        this.worksheet = getWorksheetpagination().createPageDataModel();
    }

    public List<SelectItem> getItemNgType() {
        if(itemNgType == null)
        {
            itemNgType  = new ArrayList<SelectItem>();
        }
        return itemNgType;
    }

    public void setItemNgType(List<SelectItem> itemNgType) {
        this.itemNgType = itemNgType;
    }

    public List<SelectItem> getItemNgStatus() {
        if(itemNgStatus == null)
        {
            itemNgStatus  = new ArrayList<SelectItem>();
        }
        return itemNgStatus;
    }

    public void setItemNgStatus(List<SelectItem> itemNgStatus) {
        this.itemNgStatus = itemNgStatus;
    }

    public String getNgCodeSearch() {
        return ngCodeSearch;
    }

    public void setNgCodeSearch(String ngCodeSearch) {
        this.ngCodeSearch = ngCodeSearch;
    }

    public String getNgNameSearch() {
        return ngNameSearch;
    }

    public void setNgNameSearch(String ngNameSearch) {
        this.ngNameSearch = ngNameSearch;
    }

    public String getNgStatusSearch() {
        return ngStatusSearch;
    }

    public void setNgStatusSearch(String ngStatusSearch) {
        this.ngStatusSearch = ngStatusSearch;
    }

    public String getNgTypeSearch() {
        return ngTypeSearch;
    }

    public void setNgTypeSearch(String ngTypeSearch) {
        this.ngTypeSearch = ngTypeSearch;
    }

    public NgPool getCurrentPool() {
        if(currentPool  == null)
        {
            currentPool  = new NgPool();
        }
        return currentPool;
    }

    public void setCurrentPool(NgPool currentPool) {
        if(currentPool  == null)
        {
            currentPool  = new NgPool();
        }
        this.currentPool = currentPool;
    }
    
}
