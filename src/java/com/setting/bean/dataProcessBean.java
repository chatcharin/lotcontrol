/**
 * 
 */
package com.setting.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.faces.event.ActionEvent;
import com.icesoft.faces.component.ext.RowSelectorEvent;
import com.setting.DataProcess.DataProcess;

/**
 * @author The_Boy_Cs
 *
 */
public class dataProcessBean {
	private String textfieldname;
	private List<DataProcess> textfieldlist;// = new ArrayList<DataProcess>();
	private DataProcess data;
	private String comboboxname;
	private List<DataProcess> comboList;
	private Vector<String> combooption;// = new Vector<String>();
	private String optioninput;
	
	private String[] datalist;
	
	private int index = -1;
	private int intable = 0;

	public dataProcessBean(){
		
		textfieldlist = loadDataTextFieldProcess();
		setComboList(loadDataComboListProcess());
		Vector<String> v = new Vector<String>();
		v.add("a");
		v.add("b");
		Object[] s = null ;
		s = v.toArray();
		System.out.print(s[0].toString());
		combooption = new Vector<String>();
	}
	
	private List<DataProcess> loadDataTextFieldProcess(){
		List<DataProcess> ll = new ArrayList<DataProcess>();
		//ll.add(new DataProcess("testf1", "textfield", "xfhghjk"));
		//ll.add(new DataProcess("textf2", "textfield", "xfhghjk"));
		//ll.add(new DataProcess("textf3", "textfield", "xfhghjk"));
		return ll;
	}
	
	private List<DataProcess> loadDataComboListProcess(){
		List<DataProcess> ll = new ArrayList<DataProcess>();
		//for(int i=0;i<20;i++){
		//ll.add(new DataProcess("testf1"+i, "combobox", new String[]{"t1"+i,"t2"+i,"t3"+i,"t4"+i,"t5"+i,"t6"+i,"t7"+i,"t8"+i,"t9"+i}));
		//}
		return ll;
	}

	public void createTextField(ActionEvent event){
		DataProcess textf = new DataProcess(textfieldname, "textfield", textfieldname);
		textfieldlist.add(textf);
	}
	
	public void rowSelectionListener(RowSelectorEvent event){
		index = event.getRow();
		intable = 1;
		System.out.println("Select Listener: Textfield : "+event.getRow());
	}
	
	public void rowSelectionListenert(RowSelectorEvent event){
		index = event.getRow();
		intable = 2;  //Textfield
		data = textfieldlist.get(index);
		datalist = data.getDescliption();
		//System.out.println("Select Listener: Textfield : "+event.getRow());
	}
	
	public void rowSelectionListenerc(RowSelectorEvent event){
		index = event.getRow();
		intable = 3; //Combobox
		data = comboList.get(index);
		datalist = data.getDescliption();
		//System.out.println("Select Listener: Textfield : "+event.getRow());
	}
	
/*	public void SelectAddOptionList(ActionEvent event){
		if(intable == 2){
			datalist = data.getDescliption();
		}else if(intable == 3){
			datalist = data.getDescliption();
		}else{
			System.err.println("Error : "+data);
		}
		data = null;//**  Retset value
		index = -1;
		intable = 0;
		System.out.println("Print Acction: "+Event.ACTION_EVENT);
	}*/
	
	public void addOption(){
		if(!optioninput.isEmpty()){
			combooption.add(optioninput);
		}
	}
	
	public void createNewCombobox(){
		if(!comboboxname.isEmpty()){
			String[] st = new String[combooption.size()];
			for(int i=0;i<st.length;i++){
				st[i] = combooption.get(i);
			}
			System.out.print(st);
			comboList.add(new DataProcess(comboboxname, "combobox", st));
		}
		comboboxname = "";
		optioninput = "";
		combooption = new Vector<String>();
	}
	
	/**
	 * @return the textfieldname
	 */
	public String getTextfieldname() {
		return textfieldname;
	}

	/**
	 * @param textfieldname the textfieldname to set
	 */
	public void setTextfieldname(String textfieldname) {
		this.textfieldname = textfieldname;
	}

	/**
	 * @return the textfieldlist
	 */
	public List<DataProcess> getTextfieldlist() {
		return textfieldlist;
	}

	/**
	 * @param textfieldlist the textfieldlist to set
	 */
	public void setTextfieldlist(List<DataProcess> textfieldlist) {
		this.textfieldlist = textfieldlist;
	}

	/**
	 * @return the comboboxname
	 */
	public String getComboboxname() {
		return comboboxname;
	}

	/**
	 * @param comboboxname the comboboxname to set
	 */
	public void setComboboxname(String comboboxname) {
		this.comboboxname = comboboxname;
	}

	/**
	 * @return the textfieldselected
	 */

	/**
	 * @return the comboList
	 */
	public List<DataProcess> getComboList() {
		return comboList;
	}

	/**
	 * @param comboList the comboList to set
	 */
	public void setComboList(List<DataProcess> comboList) {
		this.comboList = comboList;
	}

	/**
	 * @return the datalist
	 */
	public String[] getDatalist() {
		return datalist;
	}

	/**
	 * @param datalist the datalist to set
	 */
	public void setDatalist(String[] datalist) {
		this.datalist = datalist;
	}

	/**
	 * @return the data
	 */
	public DataProcess getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(DataProcess data) {
		this.data = data;
	}

	/**
	 * @return the combooption
	 */
	public Vector<String> getCombooption() {
		return combooption;
	}

	/**
	 * @param combooption the combooption to set
	 */
	public void setCombooption(Vector<String> combooption) {
		this.combooption = combooption;
	}

	/**
	 * @return the optioninput
	 */
	public String getOptioninput() {
		return optioninput;
	}

	/**
	 * @param optioninput the optioninput to set
	 */
	public void setOptioninput(String optioninput) {
		this.optioninput = optioninput;
	}
	
}
