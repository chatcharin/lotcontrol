/**
 * 
 */
package com.setting.bean;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ActionEvent;

import com.icesoft.faces.component.ext.RowSelectorEvent;
import com.setting.DataProcess.DataProcess;

/**
 * @author The_Boy_Cs
 * 
 */
public class config_Process {
	private String processnumber;
	private List<DataProcess> dataprocess = new ArrayList<DataProcess>();
	private List<DataProcess> dataprocessT = new ArrayList<DataProcess>();
	private List<DataProcess> dataprocessC = new ArrayList<DataProcess>();
	private DataProcess data;
	private String[] option;
	private String[] machinelist;
	private String machine;
	private String connumber;

	public config_Process() {
		for (int i = 0; i < 10; i++) {
			// dataprocess.add(new DataProcess("Name " + i));
			dataprocessT.add(new DataProcess("Namet " + i, "textfield","descliptionT"+i));
			dataprocessC.add(new DataProcess("Namec " + i, "combobox",new String[]{"descliptionC1"+i,"descliptionC2"+i,"descliptionC3"+i,"descliptionC4"+i,"descliptionC5"+i,"descliptionC6"+i,"descliptionC7"+i,"descliptionC8"+i,"descliptionC9"+i}));
		}
		setMachinelist(new String[]{" a "," b "," c "," d "," e "," f "," g "});
		connumber = "T_0021";
	}

	private int index = -1;
	private int intable = 0;

	public void rowSelectionListener(RowSelectorEvent event) {
		intable = 1;
		index = event.getRow();
		// event.getSource().toString();
		System.out.println("Print Selected " + index + " " + intable);
		ShowOption(index,intable);
	}

	public void rowSelectionListenert(RowSelectorEvent event) {
		intable = 2;
		index = event.getRow();
		// event.getSource().toString();
		System.out.println("Print Selected " + index + " " + intable);
		ShowOption(index,intable);
	}

	public void rowSelectionListenerc(RowSelectorEvent event) {
		intable = 3;
		index = event.getRow();
		// event.getSource().toString();
		System.out.println("Print Selected " + index + " " + intable);
		ShowOption(index,intable);
	}

	public void AddProcessData(ActionEvent event) {
		if (intable == 0 || index == -1) {
			System.out.println("Errur 0");
		}
		if (intable == 2 && dataprocessT.size() > 0) {
			data = dataprocessT.get(index);
			dataprocess.add(data);
			dataprocessT.remove(index);
		} else if (intable == 3 && dataprocessC.size() > 0) {
			data = dataprocessC.get(index);
			dataprocess.add(data);
			dataprocessC.remove(index);
		} else {
			System.out.println("Error 1");
		}
		intable = 0;
		index = -1;
	}

	public void DelectProcessData(ActionEvent event) {
		if (intable == 1 && dataprocess.size() > 0) {
			data = dataprocess.get(index);
			addDataToProcess(data);
			dataprocess.remove(index);
		} else {
			System.out.println("Error 1");
		}
		intable = 0;
		index = -1;
	}

	private void addDataToProcess(DataProcess data) {
		if (data.getType() == "textfield") {
			dataprocessT.add(data);
		} else if (data.getType() == "combobox") {
			dataprocessC.add(data);
		} else {
			System.out.println("Error : 1234");
		}
	}

	private void ShowOption(int index, int intable) {
		switch (intable) {
		case 1:
				data = dataprocess.get(index);
				option = data.getDescliption();
			break;
		case 2:
			data = dataprocessT.get(index);
			option = data.getDescliption();

			break;
		case 3:
			data = dataprocessC.get(index);
			option = data.getDescliption();

			break;

		default:
			break;
		}
	}

	/**
	 * @return the processnumber
	 */
	public String getProcessnumber() {
		return processnumber;
	}

	/**
	 * @param processnumber
	 *            the processnumber to set
	 */
	public void setProcessnumber(String processnumber) {
		this.processnumber = processnumber;
	}

	/**
	 * @return the dataprocess
	 */
	public List<DataProcess> getDataprocess() {
		return dataprocess;
	}

	/**
	 * @param dataprocess
	 *            the dataprocess to set
	 */
	public void setDataprocess(List<DataProcess> dataprocess) {
		this.dataprocess = dataprocess;
	}

	/**
	 * @return the dataprocessT
	 */
	public List<DataProcess> getDataprocessT() {
		return dataprocessT;
	}

	/**
	 * @param dataprocessT
	 *            the dataprocessT to set
	 */
	public void setDataprocessT(List<DataProcess> dataprocessT) {
		this.dataprocessT = dataprocessT;
	}

	/**
	 * @return the dataprocessC
	 */
	public List<DataProcess> getDataprocessC() {
		return dataprocessC;
	}

	/**
	 * @param dataprocessC
	 *            the dataprocessC to set
	 */
	public void setDataprocessC(List<DataProcess> dataprocessC) {
		this.dataprocessC = dataprocessC;
	}

	/**
	 * @return the data
	 */
	public DataProcess getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(DataProcess data) {
		this.data = data;
	}

	/**
	 * @return the option
	 */
	public String[] getOption() {
		System.out.println("Option : "+option);
		return option;
	}

	/**
	 * @param option
	 *            the option to set
	 */
	public void setOption(String[] option) {
		this.option = option;
	}

	/**
	 * @return the machine
	 */
	public String getMachine() {
		return machine;
	}

	/**
	 * @param machine the machine to set
	 */
	public void setMachine(String machine) {
		this.machine = machine;
	}

	/**
	 * @return the machinelist
	 */
	public String[] getMachinelist() {
		return machinelist;
	}

	/**
	 * @param machinelist the machinelist to set
	 */
	public void setMachinelist(String[] machinelist) {
		this.machinelist = machinelist;
	}

	/**
	 * @return the connumber
	 */
	public String getConnumber() {
		return connumber;
	}

	/**
	 * @param connumber the connumber to set
	 */
	public void setConnumber(String connumber) {
		this.connumber = connumber;
	}

}
