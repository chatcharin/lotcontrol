///**
// *
// */
//package com.setting.bean;
//
//import com.appCinfigpage.bean.userLogin;
//import com.component.table.sortable.SortableList;
//import com.icesoft.faces.component.ext.RowSelectorEvent;
//import com.setting.DataProcess.Process;
//import com.tct.data.ProcessPool;
//import com.tct.data.Users;
//import com.tct.data.jpa.ProcessPoolJpaController;
//import com.tct.data.jpa.UsersJpaController;
//import java.sql.Timestamp;
//import java.util.*;
//import javax.faces.event.ActionEvent;
//import javax.faces.model.SelectItem;
//import javax.persistence.Persistence;
//
///**
// * @author The_Boy_Cs
// *
// */
//public class newProcess extends SortableList {
//
//    public static final String columnNomber = "Number";
//    public static final String columnProcessName = "ProcessName";
//    public static final String columnStatus = "Status";
//    public static final String columnDateCreate = "Date create";
//    public static final String columnUsercreate = "User create";
//    private String processname;
//    private String status;
//    private String codeoperation;
//    private String costprocess;
//    private String usercreate;
//    private Date createdate;
//    private Timestamp createdate2;
//    private Process process;
//    private int count = 0;
//    private Process[] listProcess = null;
//    private List<ProcessPool> processpoollist = null;
//    private String userid;
//
//    public newProcess() {
//        super(columnNomber);
//        loadProcessList();
//    }
//
//    private void loadProcessList() {
//        print("=================================================================");
//        try {
//            processpoollist = getProcessPoolJpaController().findProcessPoolEntities();
//            listProcess = new Process[processpoollist.size()];
//            int i = 0;
//            for (ProcessPool pcpool : processpoollist) {
//                print("= ProcessPool : " + pcpool.getProcName());
//                Users user = getUsersJpaController().findUsers(pcpool.getUserCreate());
//                print("= Users : " + user.getFirstName());
////                listProcess[i] = new Process((i + 1), pcpool.getProcName(), pcpool.getStatus(), user.getFirstName() + " " + user.getLastName(), pcpool.getCreateDate());
//                i++;
//            }
//        } catch (Exception e) {
//            print("= Error : " + e);
//        }
//    }
//    private UsersJpaController usersJpaController = null;
//
//    private UsersJpaController getUsersJpaController() {
//        if (usersJpaController == null) {
//            usersJpaController = new UsersJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
//        }
//        return usersJpaController;
//    }
//
//    private void print(String str) {
//        System.out.println(str);
//    }
//    private ProcessPool processPool = null;
//
//    private ProcessPool getProcessPool() {
//        if (processPool == null) {
//            processPool = new ProcessPool();
//        }
//        return processPool;
//    }
//    private ProcessPoolJpaController processPoolJpaController = null;
//
//    private ProcessPoolJpaController getProcessPoolJpaController() {
//        if (processPoolJpaController == null) {
//            processPoolJpaController = new ProcessPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
//        }
//        return processPoolJpaController;
//    }
//
//    public TimeZone getTimeZone() {
//        print("=================================================================");
//        return TimeZone.getDefault();
//    }
//
//    private void recliptData() {
//        codeoperation = "";
//        costprocess = "";
//        processname = "";
//        status = "";
//    }
//
//    public void CreatProcess(ActionEvent event) {
//        print("=================================================================");
//        try {
//            getProcessPool().setIdProc(UUID.randomUUID().toString());
//            getProcessPool().setCreateDate(new Date());
//            getProcessPool().setCodeOperation(codeoperation);
//            getProcessPool().setCostProc(costprocess);
//            getProcessPool().setProcName(processname);
//            getProcessPool().setStatus(status);
//            getProcessPool().setUserCreate(userLogin.getSessionUser().getId());
//            getProcessPool().setType("");
//
//            getProcessPoolJpaController().create(getProcessPool());
//            recliptData();
//            print("= Insert Complete.");
//        } catch (Exception e) {
//            print("= Error : " + e);
//        }
//    }
//
//    public void rowSelectionListener(RowSelectorEvent event) {
//        System.out.println("Print Selected " + event.getRow());
//    }
//
//    public String getProcessname() {
//        return processname;
//    }
//
//    public void setProcessname(String processname) {
//        this.processname = processname;
//    }
//
//    public String getStatus() {
//        return status;
//    }
//
//    public void setStatus(String status) {
//        this.status = status;
//    }
//
//    public String getUsercreate() {
//        Users user = userLogin.getSessionUser();
//        usercreate = user.getFirstName() + "  " + user.getLastName();
//        userid = user.getId();
//        return usercreate;
//    }
//
//    public Date getCreatedate() {
//        return createdate;
//    }
//
//    public void setCreatedate(Date createdate) {
//        this.createdate = createdate;
//    }
//
//    public List<SelectItem> getStatuslist() {
//        List<SelectItem> statuslist = new ArrayList<SelectItem>();
//        try {
//            statuslist.add(new SelectItem("active", "Active"));
//            statuslist.add(new SelectItem("Inactive", "Inactice"));
//            ///  รอเพิ่มเติม
//        } catch (Exception e) {
//            print("= Statuslist Error :" + e);
//        }
//        return statuslist;
//    }
//
//    public Process getProcess() {
//        return process;
//    }
//
//    public void setProcess(Process process) {
//        this.process = process;
//    }
//
//    public Process[] getListProcess() {
//        if (!oldSort.equals(sortColumnName)
//                || oldAscending != ascending) {
////            sort();
//            oldSort = sortColumnName;
//            oldAscending = ascending;
//        }
//        return listProcess;
//    }
//
//    public int getCount() {
//        if (count >= listProcess.length) {
//            count = 0;
//        }
//        count++;
//        return count;
//    }
//
//    public void setCount(int count) {
//        this.count = count;
//    }
//
//    public Timestamp getCreatedate2() {
//        return createdate2;
//    }
//
//    public void setCreatedate2(Timestamp createdate2) {
//        this.createdate2 = createdate2;
//    }
//
//    public String getColumnDateCreate() {
//        return columnDateCreate;
//    }
//
//    public String getColumnNomber() {
//        return columnNomber;
//    }
//
//    public String getColumnProcessName() {
//        return columnProcessName;
//    }
//
//    public String getColumnStatus() {
//        return columnStatus;
//    }
//
//    public String getColumnUsercreate() {
//        return columnUsercreate;
//    }
//
//    @Override
////    protected void sort() {
////        // TODO Auto-generated method stub
////        Comparator comparator = new Comparator() {
////
////            public int compare(Object o1, Object o2) {
////                Process c1 = (Process) o1;
////                Process c2 = (Process) o2;
////                if (sortColumnName == null) {
////                    return 0;
////                }
//////                if (sortColumnName.equals(columnNomber)) {
//////                    return ascending ? c1.getNumber() - c2.getNumber() 
//////                            : c2.getNumber() - c1.getNumber();
////                } else if (sortColumnName.equals(columnProcessName)) {
////                    return ascending ? c1.getProcessname().compareTo(c2.getProcessname())
////                            : c2.getProcessname().compareTo(c1.getProcessname());
////                } else if (sortColumnName.equals(columnStatus)) {
////                    return ascending
////                            ? c1.getStatus().compareTo(c2.getStatus())
////                            : c2.getStatus().compareTo(c1.getStatus());
////                } else if (sortColumnName.equals(columnDateCreate)) {
////                    return ascending
////                            ? c1.getCreatedate().compareTo(c2.getCreatedate())
////                            : c2.getCreatedate().compareTo(c1.getCreatedate());
////                } else if (sortColumnName.equals(columnUsercreate)) {
////                    return ascending
////                            ? c1.getUsercreate().compareTo(c2.getUsercreate())
////                            : c2.getUsercreate().compareTo(c1.getUsercreate());
////                } else {
////                    return 0;
////                }
////            }
////        };
////        Arrays.sort(listProcess, comparator);
////    }
//
////    @Override
//    protected boolean isDefaultAscending(String sortColumn) {
//        return true;
//    }
//
//    public String getCodeoperation() {
//        return codeoperation;
//    }
//
//    public void setCodeoperation(String codeoperation) {
//        this.codeoperation = codeoperation;
//    }
//
//    public String getCostprocess() {
//        return costprocess;
//    }
//
//    public void setCostprocess(String costprocess) {
//        this.costprocess = costprocess;
//    }
//}
