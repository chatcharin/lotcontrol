/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setting.model.bean;

import com.appCinfigpage.bean.fc;
import com.appCinfigpage.bean.userLogin;
import com.mysql.jdbc.Connection;
import com.setting.DataProcess.NGPool;
import com.setting.model.Data.ModelListData;
import com.sql.bean.Sql;
import com.tct.data.Md2pc;
import com.tct.data.ModelPool;
import com.tct.data.ProcessPool;
import com.tct.data.Users;
import com.tct.data.jpa.Md2pcJpaController;
import com.tct.data.jpa.ModelPoolJpaController;
import com.tct.data.jpa.ProcessPoolJpaController;
import com.tct.data.jsf.util.PaginationHelper;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.ArrayDataModel;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;
import com.tct.data.jpa.DetailSpecialJpaController;
/**
 *
 * @author The_Boy_Cs
 */
public class ModelControl {
//    search -------------------------------------------------------------------
   
    private String modelgroupSearch;
    private String modelnameSearch;
    private String modeltypeSearch;
    private String modelstatusSearch;
    private String modelseriesSearch;
//    model list ---------------------------------------------------------------
    private List<ModelPool> modelpoollist;
    private DataModel<ModelPool> modelpooltable;
//  create ---------------------------------------------------------------------
    private ModelPool currentmoModelPool;
    private List<Processed> processpoollist;
    private List<SelectItem> processPools;
    private DataModel<Processed> processtable;
    private String pcid;
//    Error --------------------------------------------------------------------
    private String processerror;
    private String modelgrouperror;
    private String modelnameerror;
    private String modeltypeerror;
    private String modelserieserror;
    private String modelstatuserror;
    private List<ModelListData> itemListData;
    private Connection conn;
    private DetailSpecialJpaController detailSpecialJpaController = null;
    public ModelControl() {
        searchModelPool();
        
    }
    
    private  DetailSpecialJpaController getDetailSpecialJpaController()
    {
        if(detailSpecialJpaController ==null)
        {
            detailSpecialJpaController = new DetailSpecialJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailSpecialJpaController;
    }
    
    public void changToAddforEdit() {
        print("============== chang to add process for edit ====================");
        md2pc = null;
        try {
            if (checkProcessError()) {
                ProcessPool processPool = getProcessPoolJpaController().findProcessPool(pcid);
                print("Process : " + processPool.getProcName());
                int index = getProcesspoollist().size();
                if (true) {
                    getMd2pc().setCreateDate(new Date());
                    getMd2pc().setDeleted(0);
                    getMd2pc().setModelPoolId(currentmoModelPool);
                    getMd2pc().setProcessPoolIdProc(processPool);
                    int sq = checkSq() + 1;
                    getMd2pc().setSqProcess(sq);
                    getMd2pc().setUserCreate(getUser().getId());
                    getMd2pc().setUserApprove("");
                    getMd2pcJpaController().create(md2pc);
                    print("Sq : " + sq);
                }
                String statusProcess =  getDetailSpecialJpaController().findDetailSpecial(processPool.getType()).getName();
                getProcesspoollist().add(index, new Processed(getMd2pc().getMd2pcId(), getMd2pc(), processPool,statusProcess));
                setProcesstable();
            }
            pcid = "";
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }
    
    private int checkSq() {
        int index = 0;
        for (Processed processed : getProcesspoollist()) {
            if (processed.getMd2pc().getSqProcess() > index) {
                index = processed.getMd2pc().getSqProcess();
            }
        }
        return index;
    }

    private void recheckSq(int i) {
        print("== i : " + i);
        List<Processed> temp = new ArrayList<Processed>();
        for (Processed processed : getProcesspoollist()) {
            md2pc = processed.getMd2pc();
            if (md2pc.getSqProcess() >= i) {
                int j = md2pc.getSqProcess() - 1;
                md2pc.setSqProcess(j);
                processed.setMd2pc(md2pc);
                temp.add(processed);
            } else {
                temp.add(processed);
            }
            print("== Sq : " + md2pc.getSqProcess());
        }
        setProcesspoollist(temp);
        setProcesstable();
    }

    public void changToDelforEdit() {
        print("============== chang to del process for edit ====================");
        try {
            Processed processed = getProcesstable().getRowData();
            md2pc = processed.getMd2pc();
            md2pc.setDeleted(1);
            getMd2pcJpaController().edit(md2pc);
            print("Del sq : " + md2pc.getSqProcess());
            recheckSq(md2pc.getSqProcess());
            getProcesspoollist().remove(processed);
            setProcesstable();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    private Users getUser() {
        return userLogin.getSessionUser();
    }

    public void create(ActionEvent event) {
        print("============= create model ======================================");
        try {
//            if (checkProcessError() && checkModelError()) {
              if ( checkModelError()) {
                getCurrentmoModelPool().setDeleted(0);
                getCurrentmoModelPool().setCostModel(0 + "");
                getCurrentmoModelPool().setCreateDate(new Date());
                getCurrentmoModelPool().setUserCreate(getUser().getId());
                getCurrentmoModelPool().setPlat("");
                getCurrentmoModelPool().setTypeFlbl(pcid); 
                getModelPoolJpaController().create(currentmoModelPool);
                int i = 0;
                for (Processed pcPool : getProcesspoollist()) {
                    md2pc = pcPool.getMd2pc();
                    md2pc.setCreateDate(new Date());
                    md2pc.setModelPoolId(currentmoModelPool);
                    md2pc.setProcessPoolIdProc(pcPool.getPcname());
                    md2pc.setUserCreate(getUser().getId());
                    md2pc.setSqProcess(i + 1);
                    md2pc.setUserApprove(getUser().getId());
                    md2pc.setDeleted(0); 
                    print("Process" + md2pc.getProcessPoolIdProc().getProcName());
                    getMd2pcJpaController().create(md2pc);
                    i++;
                }
                String link = "pages/setting/model/listview.xhtml";
                userLogin.getSessionMenuBarBean().setParam(link);
                print("= complete.");
            } else {
                print("you don't select process or input model detail value.");
            }
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changTocancel(ActionEvent event) {
        print("================ chang to cancel ================================");
        try {
            resetParam();
            String link = "pages/setting/model/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            searchModelPool();
        } catch (Exception e) {
            print(" Error : " + e);
        }
    }

    public void changToEdit(String idEdit) {
        print("================= chang to edit ================================= : "+idEdit);
        try { 
//            currentmoModelPool = getModelpooltable().getRowData();
            currentmoModelPool = getModelPoolJpaController().findModelPool(idEdit);
            List<Md2pc> md2pcs = getMd2pcJpaController().findBymodelPoolId(currentmoModelPool);

//            Collection<Md2pc> md2pcs = currentmoModelPool.getMd2pcCollection();
//            String processName = null;
            for (Md2pc md2pc : md2pcs) {
                //md2pc.getProcessPoolIdProc().setType(getDetailSpecialJpaController().findDetailSpecial(md2pc.getProcessPoolIdProc().getType()).getName());
//                processName  = getDetailSpecialJpaController().findDetailSpecial(md2pc.getProcessPoolIdProc().getType()).getName();
                getProcesspoollist().add(new Processed(md2pc.getMd2pcId(), md2pc, md2pc.getProcessPoolIdProc(),getDetailSpecialJpaController().findDetailSpecial(md2pc.getProcessPoolIdProc().getType()).getName()));
                print("SQ : " + md2pc.getSqProcess());
            }
            setProcesstable();
            String link = "pages/setting/model/editview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }
    //**** Change to view detail
     public void changeToView(String idEdit) {
        print("================= chang to edit ================================= : "+idEdit);
        try { 
            currentmoModelPool  = new ModelPool();
            currentmoModelPool = getModelPoolJpaController().findModelPool(idEdit);
            List<Md2pc> md2pcs = getMd2pcJpaController().findBymodelPoolId(currentmoModelPool);
            for (Md2pc md2pc : md2pcs) {
                String  type = getDetailSpecialJpaController().findDetailSpecial(md2pc.getProcessPoolIdProc().getType()).getName();
                String processName = getDetailSpecialJpaController().findDetailSpecial(md2pc.getProcessPoolIdProc().getType()).getName();//*** set type 
                getProcesspoollist().add(new Processed(md2pc.getMd2pcId(), md2pc, md2pc.getProcessPoolIdProc(),processName));
                print("SQ : " + md2pc.getSqProcess());
            }
            setProcesstable();
            String link = "pages/setting/model/detailview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    } 
    public void changToSave(ActionEvent event) {
        print("================= chang to save =================================");
        try {
            getModelPoolJpaController().edit(currentmoModelPool);
            print("model : " + currentmoModelPool.getModelName());
            
            //update sqprocess
            
            int sqProcess = 0;
            for (Processed pcPool : getProcesspoollist()) 
            {   sqProcess++;
                fc.print(" pcpool getId: "+pcPool.getId());
                md2pc = new Md2pc(); 
                md2pc  = getMd2pcJpaController().findByMd2pcId(pcPool.getId());
                md2pc.setSqProcess(sqProcess);
                try
                {
                    getMd2pcJpaController().edit(md2pc);
                }
                catch(Exception e)
                {
                    fc.print("Error not to update sqprocess"+e);
                }
            }
             
            String link = "pages/setting/model/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            searchModelPool();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changToAddProcess(ActionEvent event) {
        print("================= chang to add process ==========================");
        md2pc = null;
        try {
            if (checkProcessError()) {
                ProcessPool processPool = getProcessPoolJpaController().findProcessPool(pcid);
                print("Process : " + processPool.getProcName());
                int index = getProcesspoollist().size(); 
                String processName  = getDetailSpecialJpaController().findDetailSpecial(processPool.getType()).getName(); //**** set name type process from detailspecail detail;
                getProcesspoollist().add(index, new Processed(getMd2pc().getMd2pcId(), getMd2pc(), processPool,processName));
                setProcesstable();
            }
            pcid = "";
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }
    private Md2pc md2pc;

    private Md2pc getMd2pc() {
        if (md2pc == null) {
            md2pc = new Md2pc(UUID.randomUUID().toString());
        }
        return md2pc;
    }

    public void changToDelProcess() {
        print("================== chang to del process =========================");
        try {
            Processed pcPool = getProcesstable().getRowData();
            print("= Del process : " + pcPool.getPcname().getProcName());
            processpoollist.remove(pcPool);
            setProcesstable();
        } catch (Exception e) {
            print(" Error : " + e);
        }
    }
    public void changeToDeletedModel(String idDelModel)
    {
        fc.print("*************** change to delete model ********** : "+idDelModel);
        resetParam();
        currentmoModelPool  = getModelPoolJpaController().findModelPool(idDelModel);
        getCurrentmoModelPool().setDeleted(1);
        editModelPool(); //delted model pool
        List<Md2pc> objM2pcs  = getMd2pcJpaController().findBymodelPoolId(currentmoModelPool);
        if(!objM2pcs.isEmpty())
        {
            for(Md2pc objList:objM2pcs)
            {
                objList.setDeleted(1);
                try
                {
                    getMd2pcJpaController().edit(objList);
                }
                catch(Exception e)
                {
                    fc.print("Not to deleted md2pc"+e);
                }
            }
        }
        searchModelPool();
    }
    private void editModelPool()
    {
        try
        {
            getModelPoolJpaController().edit(currentmoModelPool);
        }
        catch(Exception e)
        {
            fc.print("Not to delete model pool"+e);
        }
    }
    
    private boolean checkModelError() {
        boolean t = true;
        if (modeltypeerror != null && !modeltypeerror.isEmpty()) {
            modeltypeerror = "";
        } else {
            modeltypeerror = "Input this.";
            t = false;
        }
        if (modelgrouperror != null && !modelgrouperror.isEmpty()) {
            modelgrouperror = "";
        } else {
            modelgrouperror = "Input this.";
            t = false;
        }
        if (modelnameerror != null && !modelnameerror.isEmpty()) {
            modelnameerror = "";
        } else {
            modelnameerror = "Input this.";
            t = false;
        }
        if (modelserieserror != null && !modelserieserror.isEmpty()) {
            modelserieserror = "";
        } else {
            modelserieserror = "Input this.";
            t = false;
        }
        if (modelstatuserror != null && !modelstatuserror.isEmpty()) {
            modelstatuserror = "";
        } else {
            modelstatuserror = "Input this.";
            t = false;
        }
        return t;
    }

    private boolean checkProcessError() {
        boolean t = true;
        if (pcid != null && !pcid.isEmpty()) {
            processerror = "";
        } else {
            processerror = "Select this.";
            t = false;
        }
        return t;
    }

    public void changToInsert(ActionEvent event) {
        print("================ chang to insert ================================");
        try {
            resetParam();
            String link = "pages/setting/model/createview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            
        } catch (Exception e) {
            print("= Error : " + e);
        }
        
    }
    public void resetAllSearchKey()
    {
          modelgroupSearch = null;
          modelseriesSearch = null;
          modeltypeSearch = null;
          modelnameSearch  = null;
          modelstatusSearch  = null;
    } 
    public void searchModelPool() {
        //@pang
        resetParam();
        conn = Sql.getConnection("mysql"); 
        //Check model use 
//      modelpoollist = null;
        if(modelgroupSearch != null && !modelgroupSearch.trim().isEmpty())
        {
            searchByModelgroup();
        }
        else if(modelseriesSearch != null && !modelseriesSearch.trim().isEmpty())
        {
            searchByModelSeries();
        }
        else if(modeltypeSearch != null && !modeltypeSearch.trim().isEmpty())
        {
            searchByModelType();
        }
        else if(modelnameSearch != null && !modelnameSearch.trim().isEmpty() )
        {
            searchByModelName();
        }
        else if(modelstatusSearch != null && !modelstatusSearch.trim().isEmpty())
        {
            searchByModelStatus();
        }
        else{
        // if (true) {
            loadModelpool();
        //}
        }
        resetAllSearchKey();  // reset all key
    }
    public void searchByModelgroup()
    {
       fc.print("========= search by model group ===========");
        try{
           modelpoollist = getModelPoolJpaController().findModelPoolByGroup(modelgroupSearch);
           itemListData = findItemModel(modelpoollist);
           setModelpooltable();
       }
       catch(Exception e)
       {
           fc.print(" Error not to search by model group");
       }
    }
    public void searchByModelSeries()
    {
       fc.print("========= search by model series ===========");
       try{
           modelpoollist = getModelPoolJpaController().findModelPoolBySeries(modelseriesSearch);
           itemListData = findItemModel(modelpoollist);
           setModelpooltable();
       }
       catch(Exception e)
       {
           fc.print(" Error not to search by model series");
       }
    }
    public void searchByModelType()
    {
        fc.print("========== search  by model type ===========");
        try{
           modelpoollist = getModelPoolJpaController().findModelPoolByModelType(modeltypeSearch);
           itemListData = findItemModel(modelpoollist);
           setModelpooltable();
       }
       catch(Exception e)
       {
           fc.print(" Error not to search by model type");
       }
    }
    public void searchByModelName()
    {
        fc.print("=========== search by model name ============");
        try
        {
              modelpoollist = getModelPoolJpaController().findByModelname(modelnameSearch);
              itemListData = findItemModel(modelpoollist);
              setModelpooltable();
        }
        catch(Exception e)
        {
            fc.print("Error not   search by model name");
        }
    }
    public void searchByModelStatus()
    {
        fc.print("============== search by status =============");
        try
        {
            modelpoollist = getModelPoolJpaController().findModelPoolByStatus(modelstatusSearch);
            itemListData = findItemModel(modelpoollist);
            setModelpooltable();
        }
        catch(Exception e)
        {
           fc.print("Error not search by status"); 
        }
    } 
    public List<String> checkLockModelEdit() throws SQLException
    {
        List<String> lockModel  = new ArrayList<String>();
        String sql = Sql.generateSqlForModelUse(); 
//        zConnection conn = Sql.getConnection("mysql"); 
        Statement stm = conn.createStatement();
        if (stm.execute(sql)) {
            ResultSet result = stm.executeQuery(sql);
            int i = 1; 
            while (result.next()) {
                lockModel.add(result.getObject(1).toString());
            } 
        } 
        return lockModel;
    }
    public List<ModelListData> findItemModel(List<ModelPool> objModelPool) throws SQLException
    {
         for(ModelPool objList:objModelPool)
            {
                String lock  = "false";
                String edit  = "true";
                for(String checkLockModel : checkLockModelEdit())
                {
                    if(objList.getId().equals(checkLockModel))
                    {
                        lock = "true";
                        edit = "false";
                        break;
                    }
                }
                getItemListData().add(new ModelListData(objList.getId(), objList.getModelGroup(), objList.getModelType(), objList.getModelSeries(), objList.getModelName(), objList.getStatus(), lock, edit));
            } 
         return itemListData;
    }
    private void loadModelpool() {
        print("============== load model list ==================================");
        try {
            modelpoollist = getModelPoolJpaController().findModelPoolByNotDeleted();
            print("step 1");
            if(!modelpoollist.isEmpty()) itemListData = findItemModel(modelpoollist);
            print("step 2");
            print("= modelpoollist size : " + modelpoollist.size());
            print("step 3");
            setModelpooltable();
            print("step 4");
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public int getAllpages() {
        int allpages = getModelpagination().getItemsCount() / getModelpagination().getPageSize();
        return allpages + 1;
    }

    public void previous(ActionEvent event) {
        getModelpagination().previousPage();
        setModelpooltable();
    }

    public void next(ActionEvent event) {
        getModelpagination().nextPage();
        setModelpooltable();
    }
    private Md2pcJpaController md2pcJpaController;

    private Md2pcJpaController getMd2pcJpaController() {
        if (md2pcJpaController == null) {
            md2pcJpaController = new Md2pcJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return md2pcJpaController;
    }

    public List<SelectItem> getProcessPools() {
        print("=============== processpool selectbox ===========================");
        processPools = new ArrayList<SelectItem>();
        List<ProcessPool> processpools = getProcessPoolJpaController().findProcessPoolByNotdeleted();
        for (ProcessPool pcpPool : processpools) {
            processPools.add(new SelectItem(pcpPool.getIdProc(), pcpPool.getProcName()));
        }
        return processPools;
    }
    public List<ModelPool> getModelpoollist() {
        if (modelpoollist == null) {
            modelpoollist = new ArrayList<ModelPool>();
        }
        return modelpoollist;
    }

    public DataModel<ModelPool> getModelpooltable() {
        return modelpooltable;
    }

    public void setModelpooltable() {
        this.modelpooltable = getModelpagination().createPageDataModel();
    }

    public List<Processed> getProcesspoollist() {
        if (processpoollist == null) {
            processpoollist = new ArrayList<Processed>();
        }
        return processpoollist;
    }

    public void setProcesspoollist(List<Processed> processpoollist) { 
        if(processpoollist == null)
        {
            processpoollist  = new ArrayList<Processed>();
        }    
        this.processpoollist = processpoollist;
    }

    public DataModel<Processed> getProcesstable() {
        return processtable;
    }

    public void setProcesstable() {
        this.processtable = getProcesspagination().createPageDataModel();
    }
    private PaginationHelper modelpagination;

    public PaginationHelper getModelpagination() {
        if (modelpagination == null) {
            modelpagination = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    return getItemListData().size();
                }

                @Override
                public DataModel<NGPool> createPageDataModel() {
                    print("PageFirst : " + getPageFirstItem());
                    print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (getItemListData().size() > 0) {
                        listmodel = new ListDataModel(getItemListData().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(getItemListData());
                    }
                    return listmodel;
                }
            };
        }
        return modelpagination;
    }
    private PaginationHelper processpagination;

    public PaginationHelper getProcesspagination() {
        if (processpagination == null) {
            processpagination = new PaginationHelper(getProcesspoollist().size()) {

                @Override
                public int getItemsCount() {
                    return getProcesspoollist().size();
                }

                @Override
                public DataModel<NGPool> createPageDataModel() {
                    print("PageFirst : " + getPageFirstItem());
                    print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel = null;
//                    if (getProcesspoollist().size() > 0) {
//                        listmodel = new ListDataModel(getProcesspoollist().subList(getPageFirstItem(), getPageLastItem() + 1));
//                    } else {
                    listmodel = new ListDataModel(getProcesspoollist());
//                    }
                    return listmodel;
                }
            };
        }
        return processpagination;
    }
    public void up()
    { 
       Processed rowdataUp = getProcesstable().getRowData();
       if(getProcesstable().getRowIndex()!=0){
        for(Processed  objListProcess: processpoollist)
        {
            if(rowdataUp.equals(objListProcess))
            {
                Collections.swap(processpoollist,getProcesstable().getRowIndex(),getProcesstable().getRowIndex()-1);
                fc.print("up");
                break;
            }
        }
        setProcesstable();
       }
      // fc.print("========== up ========== : "+processed);
    }
    public void down()
    { 
       Processed rowdataDown = getProcesstable().getRowData();
       try{
            int moveDownIndex   =  getProcesstable().getRowIndex()+1; 
            for(Processed  objListProcess: processpoollist)
            {
                if(rowdataDown.equals(objListProcess))
                {
                    Collections.swap(processpoollist,getProcesstable().getRowIndex(),moveDownIndex);
                    fc.print("down index "+getProcesstable().getRowIndex()+" move to index "+moveDownIndex);
                    break;
                }
            }
            setProcesstable();}
       catch(Exception e)
       {
           fc.print("Error  : not to down"+e);
       }
    } 
    private ProcessPoolJpaController processPoolJpaController;

    private ProcessPoolJpaController getProcessPoolJpaController() {
        if (processPoolJpaController == null) {
            processPoolJpaController = new ProcessPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return processPoolJpaController;
    }
    private ModelPoolJpaController modelPoolJpaController;

    private ModelPoolJpaController getModelPoolJpaController() {
        if (modelPoolJpaController == null) {
            modelPoolJpaController = new ModelPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return modelPoolJpaController;
    }

    private void resetParam() {
        modelpagination = null;
        itemListData  = new ArrayList<ModelListData>();
        modelpoollist = null;
        currentmoModelPool = null;
        processpoollist = null; 
        modelpooltable  = new ArrayDataModel<ModelPool>();
        processtable  = new ArrayDataModel<Processed>();
    }

    public String getModelgroupSearch() {
        return modelgroupSearch;
    }

    public void setModelgroupSearch(String modelgroupSearch) {
        this.modelgroupSearch = modelgroupSearch;
    }

    public String getModelnameSearch() {
        return modelnameSearch;
    }

    public void setModelnameSearch(String modelnameSearch) {
        this.modelnameSearch = modelnameSearch;
    }

    public String getModelstatusSearch() {
        return modelstatusSearch;
    }

    public void setModelstatusSearch(String modelstatusSearch) {
        this.modelstatusSearch = modelstatusSearch;
    }

    public String getModeltypeSearch() {
        return modeltypeSearch;
    }

    public void setModeltypeSearch(String modeltypeSearch) {
        this.modeltypeSearch = modeltypeSearch;
    }

    public String getModelseriesSearch() {
        return modelseriesSearch;
    }

    public void setModelseriesSearch(String modelseriesSearch) {
        this.modelseriesSearch = modelseriesSearch;
    }

    public ModelPool getCurrentmoModelPool() {
        if (currentmoModelPool == null) {
            currentmoModelPool = new ModelPool(UUID.randomUUID().toString());
        }
        return currentmoModelPool;
    }

    public void setCurrentmoModelPool(ModelPool currentmoModelPool) {
        this.currentmoModelPool = currentmoModelPool;
    }

    public String getPcid() {
        return pcid;
    }

    public void setPcid(String pcid) {
        this.pcid = pcid;
    }

    public String getProcesserror() {
        return processerror;
    }

    public void setProcesserror(String processerror) {
        this.processerror = processerror;
    }

    public String getModelgrouperror() {
        return modelgrouperror;
    }

    public void setModelgrouperror(String modelgrouperror) {
        this.modelgrouperror = modelgrouperror;
    }

    public String getModelnameerror() {
        return modelnameerror;
    }

    public void setModelnameerror(String modelnameerror) {
        this.modelnameerror = modelnameerror;
    }

    public String getModelserieserror() {
        return modelserieserror;
    }

    public void setModelserieserror(String modelserieserror) {
        this.modelserieserror = modelserieserror;
    }

    public String getModelstatuserror() {
        return modelstatuserror;
    }

    public void setModelstatuserror(String modelstatuserror) {
        this.modelstatuserror = modelstatuserror;
    }

    public String getModeltypeerror() {
        return modeltypeerror;
    }

    public void setModeltypeerror(String modeltypeerror) {
        this.modeltypeerror = modeltypeerror;
    }

    private void print(String str) {
        System.out.println(str);
    }

    public List<ModelListData> getItemListData() {
        if(itemListData ==null)
        {
            itemListData  = new ArrayList<ModelListData>();
        }
        return itemListData;
    }

    public void setItemListData(List<ModelListData> itemListData) {
        this.itemListData = itemListData;
    }

    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }
    
}
