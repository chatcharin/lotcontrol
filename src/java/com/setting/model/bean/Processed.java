/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setting.model.bean;

import com.tct.data.Md2pc;
import com.tct.data.ProcessPool;

/**
 *
 * @author The_Boy_Cs
 */
public class Processed {
    private String id;
    private ProcessPool pcname;
    private Md2pc md2pc;
    private String statusProcess;

    public Processed(String id,Md2pc md2pc,ProcessPool pcname,String statusProcess) {
        this.id = id;
        this.md2pc = md2pc;
        this.pcname = pcname;
        this.statusProcess = statusProcess;
    }

    public String getStatusProcess() {
        return statusProcess;
    }

    public void setStatusProcess(String statusProcess) {
        this.statusProcess = statusProcess;
    } 
    public Md2pc getMd2pc() {
        return md2pc;
    }

    public void setMd2pc(Md2pc md2pc) {
        this.md2pc = md2pc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProcessPool getPcname() {
        return pcname;
    }

    public void setPcname(ProcessPool pcname) {
        this.pcname = pcname;
    }
    
}
