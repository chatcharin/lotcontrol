/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.setting.model.Data;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Oct 24, 2012, Time : 3:11:35 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class ModelListData {
    private String index;
    private String modelGroup;
    private String modelType;
    private String modelSeries;
    private String modelName;
    private String status;
    private String lock;
    private String edit; 
    public ModelListData(String index, String modelGroup, String modelType, String modelSeries, String modelName, String status, String lock, String edit) {
        this.index = index;
        this.modelGroup = modelGroup;
        this.modelType = modelType;
        this.modelSeries = modelSeries;
        this.modelName = modelName;
        this.status = status;
        this.lock = lock;
        this.edit = edit;
    } 
    public String getEdit() {
        return edit;
    }

    public void setEdit(String edit) {
        this.edit = edit;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getLock() {
        return lock;
    }

    public void setLock(String lock) {
        this.lock = lock;
    }

    public String getModelGroup() {
        return modelGroup;
    }

    public void setModelGroup(String modelGroup) {
        this.modelGroup = modelGroup;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getModelSeries() {
        return modelSeries;
    }

    public void setModelSeries(String modelSeries) {
        this.modelSeries = modelSeries;
    }

    public String getModelType() {
        return modelType;
    }

    public void setModelType(String modelType) {
        this.modelType = modelType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    } 
}
