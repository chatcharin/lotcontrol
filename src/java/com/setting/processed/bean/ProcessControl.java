/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setting.processed.bean;

import com.appCinfigpage.bean.fc;
import com.appCinfigpage.bean.userLogin;
import com.setting.DataProcess.NGPool;
import com.setting.DataProcess.Process;
import com.tct.data.*;
import com.tct.data.jpa.NgPoolJpaController;
import com.tct.data.jpa.Pc2ngJpaController;
import com.tct.data.jpa.ProcessPoolJpaController;
import com.tct.data.jpa.UsersJpaController;
import com.tct.data.jpa.exceptions.IllegalOrphanException;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jsf.util.PaginationHelper;
import com.time.timeCurrent;
import java.util.*;
import javax.faces.event.ActionEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;
import com.tct.data.jpa.DetailSpecialJpaController;
/**
 *
 * @author The_Boy_Cs
 */
public class ProcessControl {
    // search ------------------------------------------------------------------

    private String processnameSearch;
    private String codeoperationSearch;
    private String statusSearch;
    private String typeprocessSearch;
    // listview ----------------------------------------------------------------
    private DataModel<Process> processtable;
    private List<Process> processlist;
//  ----------------------------------------------------------------------------
    private ProcessPool currentprocesspool;
    private NgPool currentNgPool;
//  ng list --------------------------------------------------------------------
    private DataModel<NGPool> ngtabel;
    private List<NGPool> nglist;
    private String ngid;
    private Pc2ng pc2ng;
//  ---- Error -----------------------------------------------------------------
    private String NgnameError;
    private DetailSpecialJpaController  detailSpecialJpaController = null;    
    public ProcessControl() {
        searchProcess();
    }

    private Users getUsers() {
        return userLogin.getSessionUser();
    }
    
    private DetailSpecialJpaController  getDetailSpecialJpaController()
    {
        if(detailSpecialJpaController ==null)
        {
            detailSpecialJpaController  = new DetailSpecialJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailSpecialJpaController;
    }
    
    public void create() {
        print("=================== create ======================================");
        try {
            if (!nglist.isEmpty()) {
                 
                getCurrentprocesspool().setCreateDate(new Date());
                getCurrentprocesspool().setIdProc(UUID.randomUUID().toString());
                getCurrentprocesspool().setUserCreate(getUsers().getId());
                getCurrentprocesspool().setDeleted(0);

                getProcessPoolJpaController().create(getCurrentprocesspool());
                for (NGPool nGPool : getNglist()) {
                    pc2ng = null;
//                    currentNgPool = nGPool;//.getNgPool();
                    print("NgPool : " + nGPool.getNgPool());
                    getPc2ng().setCreateDate(new Date());
                    getPc2ng().setIdNg(nGPool.getNgPool());
                    getPc2ng().setIdProc(currentprocesspool);
                    getPc2ng().setUserCreate(userLogin.getSessionUser().getUserName());
//                    getPc2ng().set
                    getPc2ngJpaController().create(getPc2ng());
//                    getNgPoolJpaController().create(currentNgPool);
                }
                String link = "pages/setting/processed/listview.xhtml";
                Thread.sleep(500);
                userLogin.getSessionMenuBarBean().setParam(link);
                searchProcess();
            } else {
                print("nglist is emply");
            }
        } catch (Exception e) {
            print("Error : " + e);
        }
    }

    private Pc2ng getPc2ng() {
        if (pc2ng == null) {
            pc2ng = new Pc2ng(UUID.randomUUID().toString());
        }
        return pc2ng;
    }

    private void complieProcessList(List<ProcessPool> processPools) {
        try {
            for (ProcessPool pcpool : processPools) {
                print("= ProcessPool : " + pcpool.getProcName());
                Users user = getUsersJpaController().findUsers(pcpool.getUserCreate());
                print("= CodeOperation : " + pcpool.getCodeOperation());
                print("== User : " + user);
                if (user != null) {
                    print("= Users : " + user.getFirstName());
                    getProcesslist().add(new Process(user.getFirstName() + " " + user.getLastName(), pcpool));
                } else {
                    getProcesslist().add(new Process(user.getFirstName() + " " + user.getLastName(), pcpool));
                }
            }
            setProcesstable();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    private void loadProcessList() {
        print("=================================================================");
        processlist = null;
        List<ProcessPool> processpoollist = getProcessPoolJpaController().findProcessPoolByNotdeleted();
        for(ProcessPool pcpool : processpoollist)
        {
             getProcesslist().add(new Process("" + " " + "", pcpool));
            
        }
        setProcesstable();
//        try {
//            List<ProcessPool> processpoollist = getProcessPoolJpaController().findProcessPoolByNotdeleted();
//            complieProcessList(processpoollist);
//        } catch (Exception e) {
//            print("= Error : " + e);
//        }
    }

    public void changToInsert(ActionEvent event) {
        print("================== chang to insert ==============================");
        resetParam();
        try {
            String link = "pages/setting/processed/createview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (Exception e) {
            print(" Error : " + e);
        }
    }

    private void resetParam() {
        print("================== reset paramiter ===============================");
        processmodelpagination = null;
        currentprocesspool = null;
        processlist = null;
        nglist = null;
        ngtabel = null;
    }

    public void changToAddNg(ActionEvent event) {
        print("================== chng to add ng ===============================");
        boolean t = true;
        NGPool ngp = null;
        try {
            if (checkError()) {
                print("===== Ngid : " + ngid);
                NgPool ngPool = getNgPoolJpaController().findNgPool(ngid);
                print("==== Ng : " + ngPool.getNgName());
                ngp = new NGPool(getPc2ng().getPc2ngId(), ngPool);  
                if(ngp == null)
                {
                    fc.print("ngp is null");
                }
                else
                {   
                    fc.print("============================");
                    fc.print("ng name :"+ngp.getNgname());
                    fc.print("============================");
                }
                if(getNglist().isEmpty()) //@pang this contition to check size of nglist 
                {
                    getNglist().add(ngp);
                }
                fc.print("Size of ng :"+getNglist().size());
                fc.print("============================");
                for (int i = 0; i < getNglist().size(); i++) {
                    if (ngp.getNgcode().equals(getNglist().get(i).getNgcode())) {
                        t = false;
                        //fc.print(" Break ngp.getNgcode() :"+ngp.getNgcode()+" getNglist().get(i).getNgcode() : "+getNglist().get(i).getNgcode()); //@pang
                        break;
                         
                    }
                    
                    fc.print("ngp.getNgcode() :"+ngp.getNgcode()+" getNglist().get(i).getNgcode() : "+getNglist().get(i).getNgcode()); //@pang
                }
//                getNglist().add(new NGPool("", ngPool));
                print("===== add to list ");
                setNgtabel();
                ngid = "";
            } else {
                print("NgId is null.");
            }
            if (t) {
                getNglist().add(ngp);
                NgnameError = "";
            } else {
                print("it's Repeated.");
                NgnameError = "It's Repeated.";
            }
        } catch (Exception e) {
            print("= Error add ng : " + e);
        }
    }

    public void changToDelNg(ActionEvent evnt) {
        print("================== chang to del ng ==============================");
        try {
            NGPool ngPool = getNgtabel().getRowData();
//            currentNgPool = ngPool.getNgPool();
            nglist.remove(ngPool);
            setNgtabel();
        } catch (Exception e) {
            print("Error : " + e);
        }
    }

    public void changToDelNgforEdit(ActionEvent event) {
        print("================== chang to Del for edit ========================");
        try {
            NGPool nGPool = getNgtabel().getRowData();
            print("currentNgPool : " + nGPool);
            pc2ng = getPc2ngJpaController().findPc2ng(nGPool.getPc2ngid());
            print("Pc2ng : " + pc2ng);
            pc2ng.setDeleted(1);
            getPc2ngJpaController().edit(pc2ng);
            print("edit pc2ng complete.");
            nglist.remove(nGPool);
            setNgtabel();
        } catch (Exception e) {
            print("Error : " + e);
        }
    }

    public void changToAddNgforEdit(ActionEvent event) {
        print("================== chang to Add ng for edit =====================");
        currentNgPool = null;
        pc2ng = null;
        boolean t = true;
        NGPool ngp = null;
        try {
            if (checkError()) {
                print("Ng id : " + ngid);
                currentNgPool = getNgPoolJpaController().findNgPool(ngid);
                String  idPc2ng  = UUID.randomUUID().toString();
                getPc2ng().setPc2ngId(idPc2ng);  
                getPc2ng().setCreateDate(new Date());
                getPc2ng().setDeleted(0);
                getPc2ng().setIdNg(currentNgPool);
                getPc2ng().setIdProc(currentprocesspool);
                getPc2ng().setUserCreate(getUsers().getId());
                getPc2ngJpaController().create(getPc2ng());
                ngp = new NGPool(getPc2ng().getPc2ngId(), getPc2ng().getIdNg());
                if(getNglist().isEmpty()) //@pang this contition to check size of nglist 
                {
                    getNglist().add(ngp);
                }
                for (int i = 0; i < getNglist().size(); i++) {
                    if (ngp.getNgcode().equals(getNglist().get(i).getNgcode())) {
                        t = false;
                        break;
                    }
                }
                setNgtabel();
                ngid = "";
            } else {
                print("ng id is emply.");
            }
            if (t) {
                getNglist().add(ngp);
                NgnameError = "";
            } else {
                //print("it's Repeated.");
                //NgnameError = "It's Repeated.";
            }
        } catch (Exception e) {
            print("Error : " + e);
        }
    }

    private boolean checkError() {
        if (ngid != null && !ngid.isEmpty()) {
            NgnameError = "";
            return true;
        } else {
            NgnameError = "Select NG!";
            return false;
        }
    }

    public void changToSave()  {
        print("================== chang to save ================================");
            currentprocesspool.setCreateDate(new Date());
         try {
             
             getProcessPoolJpaController().edit(currentprocesspool); 
           } catch (Exception ioex) {
             print("Error ioex : " + ioex);
          }   
            String link = "pages/setting/processed/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            searchProcess();
       
    }

    public void changToCancel(ActionEvent event) {
        print("================== chang to cancel ==============================");
        resetParam();
        try {
            String link = "pages/setting/processed/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            Thread.sleep(500);
            searchProcess();
        } catch (Exception e) {
            print(" cancel Error : " + e);
        }
    }

    public void changToEdit(ActionEvent event) {
        print("=================== chang to edit ===============================");
        try {
            nglist = null;
            currentprocesspool  = null;
            Process process = getProcesstable().getRowData();
            currentprocesspool = process.getProcessPool();
            Collection<Pc2ng> pc2ngcollection = currentprocesspool.getPc2ngCollection();
            for (Pc2ng pc2ng2 : pc2ngcollection) {
                if (pc2ng2.getDeleted() == 0) {
                    getNglist().add(new NGPool(pc2ng2.getPc2ngId(), pc2ng2.getIdNg()));
                }
            }
            setNgtabel();
            String link = "pages/setting/processed/editview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (Exception e) {
            print(" Error : " + e);
        }
    }

    public void changToDelete(ActionEvent event) {
        print("================== chang to delete ==============================");
        try {
            Process process = getProcesstable().getRowData();
            currentprocesspool = process.getProcessPool();
            currentprocesspool.setStatus("Inactive");
            currentprocesspool.setDeleted(1);
            getProcessPoolJpaController().edit(currentprocesspool);
            print("Deleted : completed.");
            searchProcess();
        } catch (Exception e) {
            print(" Error : " + e);
        }
    }

    public List<SelectItem> getNgname() {
        List<SelectItem> nglists = new ArrayList<SelectItem>();
        try {
            List<NgPool> ngPools = getNgPoolJpaController().findByNotdeleted();
            for (NgPool ngPool : ngPools) {
                nglists.add(new SelectItem(ngPool.getIdNg(), ngPool.getNgName()));
            }
            return nglists;
        } catch (Exception e) {
            print("Error getNgname : " + e);
            return nglists;
        }
    }
    private Pc2ngJpaController pc2ngJpaController;

    private Pc2ngJpaController getPc2ngJpaController() {
        if (pc2ngJpaController == null) {
            pc2ngJpaController = new Pc2ngJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return pc2ngJpaController;
    }
    private NgPoolJpaController ngPoolJpaController;

    private NgPoolJpaController getNgPoolJpaController() {
        if (ngPoolJpaController == null) {
            ngPoolJpaController = new NgPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return ngPoolJpaController;
    }
    private UsersJpaController usersJpaController;

    private UsersJpaController getUsersJpaController() {
        if (usersJpaController == null) {
            usersJpaController = new UsersJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return usersJpaController;
    }
    private ProcessPoolJpaController processPoolJpaController;

    private ProcessPoolJpaController getProcessPoolJpaController() {
        if (processPoolJpaController == null) {
            processPoolJpaController = new ProcessPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return processPoolJpaController;
    }

    private void print(String str) {
        System.out.println(str);
    }

    public ProcessPool getCurrentprocesspool() {
        if (currentprocesspool == null) {
            currentprocesspool = new ProcessPool();
        }
        return currentprocesspool;
    }

    public void setCurrentprocesspool(ProcessPool currentprocesspool) {
        this.currentprocesspool = currentprocesspool;
        if(this.currentprocesspool == null)
        {
            currentprocesspool  = new ProcessPool();
        }
    }

    public List<Process> getProcesslist() {
        if (processlist == null) {
            processlist = new ArrayList<Process>();
        }
        return processlist;
    }

    public String getProcessnameSearch() {
        return processnameSearch;
    }

    public void setProcessnameSearch(String processnameSearch) {
        this.processnameSearch = processnameSearch;
    }

    public DataModel<Process> getProcesstable() {
        if (processtable == null) {
            processtable = new ListDataModel<Process>();
        }
        return processtable;
    }

    public String getNgid() {
        return ngid;
    }

    public void setNgid(String ngid) {
        this.ngid = ngid;
        NgnameError = "";
    }

    public void setProcesstable() {
        this.processtable = getProcessmodelpagination().createPageDataModel();
    }

    public int getAllpages() {
        int allpages = getProcessmodelpagination().getItemsCount() / getProcessmodelpagination().getPageSize();
        return allpages + 1;
    }

    public void previous(ActionEvent event) {
        getProcessmodelpagination().previousPage();
        setProcesstable();
    }

    public void next(ActionEvent event) {
        getProcessmodelpagination().nextPage();
        setProcesstable();
    }
    private PaginationHelper processmodelpagination;

    public PaginationHelper getProcessmodelpagination() {
        if (processmodelpagination == null) {
            processmodelpagination = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    return getProcesslist().size();
                }

                @Override
                public DataModel<ProcessPool> createPageDataModel() {
                    print("PageFirst : " + getPageFirstItem());
                    print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (getProcesslist().size() > 0) {
                        listmodel = new ListDataModel(getProcesslist().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(getProcesslist());
                    }
                    return listmodel;
                }
            };
        }
        return processmodelpagination;
    }

    public List<NGPool> getNglist() {
        if (nglist == null) {
            nglist = new ArrayList<NGPool>();
        }
        return nglist;
    }

    public DataModel<NGPool> getNgtabel() {
        return ngtabel;
    }

    public void setNgtabel() {
        this.ngtabel = getNgpagination().createPageDataModel();
    }
    private PaginationHelper ngpagination;

    public PaginationHelper getNgpagination() {
        if (ngpagination == null) {
            ngpagination = new PaginationHelper(getNglist().size()) {

                @Override
                public int getItemsCount() {
                    return getNglist().size();
                }

                @Override
                public DataModel<NGPool> createPageDataModel() {
                    print("PageFirst : " + getPageFirstItem());
                    print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel = null;
                    if (getNglist().size() > 0) {
//                        listmodel = new ListDataModel(getNglist().subList(getPageFirstItem(), getPageLastItem() + 1));
//                    } else {
                        listmodel = new ListDataModel(getNglist());
                    }
                    return listmodel;
                }
            };
        }
        return ngpagination;
    }

    public void searchProcess() {
        print("============= Search ============================================");
        resetParam();
        if (codeoperationSearch != null && !codeoperationSearch.isEmpty()) {
            searchProcessByCodeopration();
        } else if (statusSearch != null && !statusSearch.isEmpty()) {
            searchProcessByStatus();
        } else if (processnameSearch != null && !processnameSearch.isEmpty()) {
            searchProcessByProcessname();
        } else if (typeprocessSearch != null && !typeprocessSearch.isEmpty()) {
            searchProcessByType();
        } else {
            loadProcessList();
        }
    }

    public String getNgnameError() {
        return NgnameError;
    }

    public void setNgnameError(String NgnameError) {
        this.NgnameError = NgnameError;
    }

    public String getCodeoperationSearch() {
        return codeoperationSearch;
    }

    public void setCodeoperationSearch(String codeoperationSearch) {
        this.codeoperationSearch = codeoperationSearch;
    }

    public NgPool getCurrentNgPool() {
        return currentNgPool;
    }

    public void setCurrentNgPool(NgPool currentNgPool) {
        this.currentNgPool = currentNgPool;
    }

    public String getStatusSearch() {
        return statusSearch;
    }

    public void setStatusSearch(String statusSearch) {
        this.statusSearch = statusSearch;
    }

    public String getTypeprocessSearch() {
        return typeprocessSearch;
    }

    public void setTypeprocessSearch(String typeprocessSearch) {
        this.typeprocessSearch = typeprocessSearch;
    }

    private void searchProcessByCodeopration() {
        print("================ search by code operation =======================");
        try {
            List<ProcessPool> processPools = getProcessPoolJpaController().findProcessPoolByCodeOperation(codeoperationSearch);
            complieProcessList(processPools);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    private void searchProcessByStatus() {
        print("=============== search by status ================================");
        try {
            List<ProcessPool> processPools = getProcessPoolJpaController().findProcessPoolByStatus(statusSearch);
            complieProcessList(processPools);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    private void searchProcessByProcessname() {
        print("=============== search by process name ==========================");
        try {
            List<ProcessPool> processPools = getProcessPoolJpaController().findProcessPoolByName(processnameSearch);
            complieProcessList(processPools);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    private void searchProcessByType() {
        print("================= search by type process ========================");
        try {
            List<ProcessPool> processPools = getProcessPoolJpaController().findProcessPoolByType(typeprocessSearch);
            complieProcessList(processPools);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void setPc2ng(Pc2ng pc2ng) {
        this.pc2ng = pc2ng;
        if(this.pc2ng == null)
        {
            pc2ng  = new Pc2ng();
        }
    }
    //@pang edit
    public List<SelectItem> loadProcessType()
    {
        List<SelectItem>  processType  = new ArrayList<SelectItem>();
        processType.add(new SelectItem("null","--- Select one ---"));
        List<DetailSpecial>  objDetailSpecial  = getDetailSpecialJpaController().findAlltNotDelete();
        if(!objDetailSpecial.isEmpty()){
            for(DetailSpecial objList :objDetailSpecial)
            {
                processType.add(new SelectItem(objList.getId(),objList.getName()));
            }
        }
        return processType;
    }
}
