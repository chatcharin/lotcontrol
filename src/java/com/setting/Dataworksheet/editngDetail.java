/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.setting.Dataworksheet;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 6, 2012, Time : 10:23:09 AM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class editngDetail {
    private String index;
    private String codeoperation ;
    private String processName;
    private String typeProcess;
    private String status;
    private String dateCreate;
    private String userCreate;
    private String dateModify;
    private String userModify;

    public editngDetail(String index,String codeoperation, String processName, String typeProcess, String status, String dateCreate, String userCreate, String dateModify, String userModify) {
        this.index = index;
        this.codeoperation = codeoperation;
        this.processName = processName;
        this.typeProcess = typeProcess;
        this.status = status;
        this.dateCreate = dateCreate;
        this.userCreate = userCreate;
        this.dateModify = dateModify;
        this.userModify = userModify;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }
    
    public String getCodeoperation() {
        return codeoperation;
    }

    public void setCodeoperation(String codeoperation) {
        this.codeoperation = codeoperation;
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getDateModify() {
        return dateModify;
    }

    public void setDateModify(String dateModify) {
        this.dateModify = dateModify;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTypeProcess() {
        return typeProcess;
    }

    public void setTypeProcess(String typeProcess) {
        this.typeProcess = typeProcess;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserModify() {
        return userModify;
    }

    public void setUserModify(String userModify) {
        this.userModify = userModify;
    }
    
}
