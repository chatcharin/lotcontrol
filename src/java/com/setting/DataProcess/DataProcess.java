/**
 * 
 */
package com.setting.DataProcess;

/**
 * @author The_Boy_Cs
 *
 */
public class DataProcess {
	private String name;
	private String type;
	private String[] descliption;

	public DataProcess(String name){
		this.setName(name);
	}

	public DataProcess(String name, String type,String descliption) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.type = type;
		this.descliption = new String[]{descliption};
	}
	public DataProcess(String name, String type,String[] descliption) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.type = type;
		this.descliption = descliption;
	}

	public DataProcess(){}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the descliption
	 */
	public String[] getDescliption() {
		return descliption;
	}

	/**
	 * @param descliption the descliption to set
	 */
	public void setDescliption(String[] descliption) {
		this.descliption = descliption;
	}
}
