/**
 *
 */
package com.setting.DataProcess;

/**
 * @author The_Boy_Cs
 *
 */
import com.tct.data.NgPool;

public class NGPool {

    private String pc2ngid;
    private NgPool ngPool;
    private String ngcode;
    private String ngname;
    private String ngtype;
    private String ngstatus;

    public NGPool(String name) {
        this.pc2ngid = name;
    }

    public NGPool(String pc2ngid, NgPool ngPool) {
        this.pc2ngid = pc2ngid;
        this.ngPool = ngPool;
        this.ngcode = ngPool.getNgCode();
        this.ngname = ngPool.getNgName();
        this.ngstatus = ngPool.getStatus();
        this.ngtype = ngPool.getNgType();
    }

    public String getPc2ngid() {
        return pc2ngid;
    }

    public void setPc2ngid(String pc2ngid) {
        this.pc2ngid = pc2ngid;
    }

    public NgPool getNgPool() {
        return ngPool;
    }

    public void setNgPool(NgPool ngPool) {
        this.ngPool = ngPool;
    }

    public String getNgcode() {
        return ngcode;
    }

    public void setNgcode(String ngcode) {
        this.ngcode = ngcode;
    }

    public String getNgname() {
        return ngname;
    }

    public void setNgname(String ngname) {
        this.ngname = ngname;
    }

    public String getNgstatus() {
        return ngstatus;
    }

    public void setNgstatus(String ngstatus) {
        this.ngstatus = ngstatus;
    }

    public String getNgtype() {
        return ngtype;
    }

    public void setNgtype(String ngtype) {
        this.ngtype = ngtype;
    }
    
}
