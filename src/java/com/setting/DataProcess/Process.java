/**
 *
 */
package com.setting.DataProcess;

import com.tct.data.ProcessPool;

/**
 * @author The_Boy_Cs
 *
 */
public class Process {

    private ProcessPool processPool;
    private String usercreate;

    public Process(String usercreate,ProcessPool processPool) {

        this.usercreate = usercreate;
        this.processPool = processPool;
    }

    /**
     * @return the usercreate
     */
    public String getUsercreate() {
        return usercreate;
    }

    public ProcessPool getProcessPool() {
        return processPool;
    }

    public void setProcessPool(ProcessPool processPool) {
        this.processPool = processPool;
    }

}
