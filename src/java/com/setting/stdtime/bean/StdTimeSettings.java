/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setting.stdtime.bean;

import com.appCinfigpage.bean.fc;
import com.setting.stdtime.data.StdTimeData;
import com.sql.bean.Sql;
import com.tct.data.StdTimeSetting;
import com.tct.data.jpa.StdTimeSettingJpaController;
import com.tct.data.jsf.util.PaginationHelper;
import com.time.timeCurrent;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.persistence.Persistence;
import com.tct.data.jpa.StdDataShowInputJpaController;
import com.tct.data.StdDataShowInput;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import com.tct.data.StdDataShow;
import com.tct.data.jpa.StdDataShowJpaController;
import javax.faces.context.FacesContext;

/**
 *
 * Machine User
 *
 * @author pang Development by Chusak laojang Date : Nov 1, 2012, Time :
 * 11:06:30 AM Copy Right 4 Xtreme Co.,Ltd.
 */
public class StdTimeSettings {

    private List<StdTimeData> itemStdTime;
    private StdTimeData currentStdTime;
    private boolean statusAddButton = true;
    private boolean statusSaveButton = false;
    private StdTimeSettingJpaController stdTimeSettingJpaController = null;
    private StdTimeSetting currentStdTimeSetting;
    private String validateHeaderName;
    private String validateHours;
    private String validateMinutes;
    private StdDataShowInputJpaController stdDataShowInputJpaController = null;
    private StdDataShowInput currentDataShowInput;
    private StdDataShowJpaController currentStdDataShowJpaController = null;

    public StdTimeSettings() {
        loadDefaultData();
    }

    public StdDataShowJpaController getStdDataShowJpaController() {
        if (currentStdDataShowJpaController == null) {
            currentStdDataShowJpaController = new StdDataShowJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return currentStdDataShowJpaController;
    }

    public StdDataShowInputJpaController getStdDataShowInputJpaController() {
        if (stdDataShowInputJpaController == null) {
            stdDataShowInputJpaController = new StdDataShowInputJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return stdDataShowInputJpaController;
    }

    public void loadDefaultData() {
        List<StdTimeSetting> objStdTimeSetting = getSettingJpaController().findAllNotDelete();
        if (!objStdTimeSetting.isEmpty()) {
            int number = 0;
            for (StdTimeSetting objList : objStdTimeSetting) {
                number++;
                getItemStdTime().add(new StdTimeData(objList.getIdStdSetting(), objList.getDateCreate(), objList.getHeaderName(), objList.getHours(), objList.getMinute(), Integer.toString(number)));
            }
        }
    }

    public StdTimeSettingJpaController getSettingJpaController() {
        if (stdTimeSettingJpaController == null) {
            stdTimeSettingJpaController = new StdTimeSettingJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return stdTimeSettingJpaController;
    }

    public boolean checkHeaderName() {
        boolean headerStatus = false;
        List<StdTimeSetting> objSetting = getSettingJpaController().findAllNotDelete();
        if (!objSetting.isEmpty()) {
            for (StdTimeSetting objList : objSetting) {
                if (objList.getHeaderName().equals(currentStdTime.getHeaderName())) {
                    headerStatus = true;
                    break;
                }
            }
        }
        return headerStatus;
    }

    public boolean checkHours() {
        boolean hours = false;
        try {
            int numhours = Integer.parseInt(currentStdTime.getHours());
            fc.print("numhorus *** " + numhours);
        } catch (Exception e) {
            fc.print("**** error" + e);
            hours = true;
        }
        return hours;
    }

    public boolean checkMinute() {
        boolean minute = false;
        try {
            int numhours = Integer.parseInt(currentStdTime.getMinute());
            fc.print("numhorus *** " + numhours);
        } catch (Exception e) {
            fc.print("**** error" + e);
            minute = true;
        }
        return minute;
    }

    public void changeToAdd() throws SQLException {
        prepareCurrentStdTimeSetting();
        if (getCurrentStdTime().getHeaderName().trim().isEmpty()) {
            validateHeaderName = "Invalid input";
            validateHours = "";
            validateMinutes = "";
        } else if (checkHeaderName()) {
            validateHeaderName = "Already input";
            validateHours = "";
            validateMinutes = "";
        } else if (getCurrentStdTime().getHours().trim().isEmpty() || checkHours()) {
            validateHeaderName = "";
            validateHours = "Invalid input";
            validateMinutes = "";
        } else if (getCurrentStdTime().getMinute().trim().isEmpty() || checkMinute()) {
            validateHeaderName = "";
            validateMinutes = "Invalid input";
            validateHours = "";
        } else {
            validateHeaderName = "";
            validateHours = "";
            validateMinutes = "";
            fc.print("******** Change to add *******");
            String id = gemId();
            currentStdTimeSetting.setDateCreate(new timeCurrent().getDate());
            currentStdTimeSetting.setHeaderName(getCurrentStdTime().getHeaderName());
            currentStdTimeSetting.setHours(getCurrentStdTime().getHours());
            currentStdTimeSetting.setMinute(getCurrentStdTime().getMinute());
            currentStdTimeSetting.setIdStdSetting(id);
            currentStdTimeSetting.setDeleted(0);
            create();
            addAllNewRecord(id);
            resetDataAll();
            List<StdTimeSetting> objStdTimeSetting = getSettingJpaController().findAllNotDelete();
            if (!objStdTimeSetting.isEmpty()) {
                int number = 0;
                for (StdTimeSetting objList : objStdTimeSetting) {
                    number++;
                    getItemStdTime().add(new StdTimeData(objList.getIdStdSetting(), objList.getDateCreate(), objList.getHeaderName(), objList.getHours(), objList.getMinute(), Integer.toString(number)));
                }
            }
            statusAddButton = true;
            statusSaveButton = false;
            prepareStdSetting();
        }
    }

    public void addAllNewRecord(String stdid) throws SQLException {
        fc.print("new id " + stdid);
        List<String> idShow = new ArrayList<String>();
        
        String sql = Sql.generateSqlForInsetNeWSettingRecord();
        com.mysql.jdbc.Connection conn = Sql.getConnection("mysql"); //connection SQL
        Statement stm = conn.createStatement();
        if (stm.execute(sql)) {
            ResultSet result = stm.executeQuery(sql);
            while (result.next()) {
                idShow.add(result.getObject(2).toString());
            }
        }
        if (!idShow.isEmpty()) {
            for (String objList : idShow) {
                prepareCurrentDataShowInput();
                currentDataShowInput.setIdShow(getStdDataShowJpaController().findStdDataShow(objList));
                currentDataShowInput.setIdStdShowDataInput(gemId());
                currentDataShowInput.setIdSettingName(getSettingJpaController().findStdTimeSetting(stdid));
                currentDataShowInput.setValue("0");
                try {
                    getStdDataShowInputJpaController().create(currentDataShowInput);
                } catch (Exception e) {
                    fc.print("Error " + e);
                }
            }
        } 
    } 
    public void prepareCurrentDataShowInput() {
        currentDataShowInput = new StdDataShowInput();
    }

    public void resetDataAll() {
        itemStdTime = new ArrayList<StdTimeData>();
        prepareCurrentStdTimeSetting();
    }

    public void changeToDelete(String indexDel) {
        fc.print("******** Change To deleted ******* id del : " + indexDel);
        StdTimeSetting objStdTimeSettings = getSettingJpaController().findStdTimeSetting(indexDel);
        objStdTimeSettings.setDeleted(1);
        try {
            getSettingJpaController().edit(objStdTimeSettings);
        } catch (Exception e) {
            fc.print("Error not to add :" + e);
        }
        deleteStdShowDataInput(objStdTimeSettings);
        resetDataAll();
        List<StdTimeSetting> objStdTimeSetting = getSettingJpaController().findAllNotDelete();
        if (!objStdTimeSetting.isEmpty()) {
            int number = 0;
            for (StdTimeSetting objList : objStdTimeSetting) {
                number++;
                getItemStdTime().add(new StdTimeData(objList.getIdStdSetting(), objList.getDateCreate(), objList.getHeaderName(), objList.getHours(), objList.getMinute(), Integer.toString(number)));
            }
        }

    }

    public void deleteStdShowDataInput(StdTimeSetting objStdTimeSettings) {
        fc.print("****** change to delete  std data ******"+objStdTimeSettings.getIdStdSetting());

        List<StdDataShowInput> objDataShowInput = getStdDataShowInputJpaController().findStdSetting(objStdTimeSettings);
        if (!objDataShowInput.isEmpty()) {
            for (StdDataShowInput objList : objDataShowInput) {
                try {
                    getStdDataShowInputJpaController().destroy(objList.getIdStdShowDataInput());
                } catch (Exception e) {
                    fc.print("Error " + e);
                }
            }
        }
    }

    public void changeToEdit(String idexEdit) {//StdTimeSetting idexEdit
        validateHeaderName = "";
        validateHours = "";
        validateMinutes = "";
        statusSaveButton = true;
        statusAddButton = false;
        prepareCurrentStdTimeSetting();
        fc.print("********* Change To edit ******* id edit : " );//+ idexEdit);
        
        for (StdTimeData objList : getItemStdTime()) {
            if (objList.getIndex().equals(idexEdit)) {
                currentStdTime.setIndex(objList.getIndex());
                currentStdTime.setDate(objList.getDate());
                currentStdTime.setNumber(objList.getNumber());
                currentStdTime.setHeaderName(objList.getHeaderName());
                currentStdTime.setMinute(objList.getMinute());
                currentStdTime.setHours(objList.getHours());
                currentStdTimeSetting.setIdStdSetting(objList.getIndex());
                break;
            }
        }
    }

    public void changeToSave() {
        fc.print("****** change to save *****");
        if (getCurrentStdTime().getHeaderName().trim().isEmpty()) {
            validateHeaderName = "Invalid input";
            validateHours = "";
            validateMinutes = "";
        } else if (checkHeaderName()) {
            validateHeaderName = "Already input";
            validateHours = "";
            validateMinutes = "";
        } else if (getCurrentStdTime().getHours().trim().isEmpty() || checkHours()) {
            validateHeaderName = "";
            validateHours = "Invalid input";
            validateMinutes = "";
        } else if (getCurrentStdTime().getMinute().trim().isEmpty() || checkMinute()) {
            validateHeaderName = "";
            validateMinutes = "Invalid input";
            validateHours = "";
        } else {
            validateHeaderName = "";
            validateHours = "";
            validateMinutes = "";
            statusSaveButton = false;
            statusAddButton = true;
            currentStdTimeSetting.setHeaderName(getCurrentStdTime().getHeaderName());
            currentStdTimeSetting.setHours(getCurrentStdTime().getHours());
            currentStdTimeSetting.setMinute(getCurrentStdTime().getMinute());
            currentStdTimeSetting.setDateCreate(getCurrentStdTime().getDate());
            currentStdTimeSetting.setDeleted(0);
            edit();
            prepareStdSetting();
            resetDataAll();
            List<StdTimeSetting> objStdTimeSetting = getSettingJpaController().findAllNotDelete();
            if (!objStdTimeSetting.isEmpty()) {
                int number = 0;
                for (StdTimeSetting objList : objStdTimeSetting) {
                    number++;
                    getItemStdTime().add(new StdTimeData(objList.getIdStdSetting(), objList.getDateCreate(), objList.getHeaderName(), objList.getHours(), objList.getMinute(), Integer.toString(number)));
                }
            }
        }
    }

    public void edit() {
        try {
            getSettingJpaController().edit(currentStdTimeSetting);
        } catch (Exception e) {
            fc.print("***** Error ****:" + e);
        }

    }

    public void changeToCancel() {
        fc.print("****** change to cancel *******");
        statusAddButton = true;
        statusSaveButton = false;
        prepareStdSetting();
    }

    public void prepareCurrentStdTimeSetting() {
        currentStdTimeSetting = new StdTimeSetting();
    }

    public void create() {
        try {
            getSettingJpaController().create(currentStdTimeSetting);
        } catch (Exception e) {
            fc.print("***** Not to create ***** :" + e);
        }
    }

    public String gemId() {
        return UUID.randomUUID().toString();
    }

    public void prepareStdSetting() {
        currentStdTime = new StdTimeData(null, null, null, null, null, null);
    }

    public List<StdTimeData> getItemStdTime() {
        if (itemStdTime == null) {
            itemStdTime = new ArrayList<StdTimeData>();
        }
        return itemStdTime;
    }

    public void setItemStdTime(List<StdTimeData> itemStdTime) {
        this.itemStdTime = itemStdTime;
    }

    public StdTimeData getCurrentStdTime() {
        if (currentStdTime == null) {
            currentStdTime = new StdTimeData(null, null, null, null, null, null);
        }
        return currentStdTime;
    }

    public void setCurrentStdTime(StdTimeData currentStdTime) {
        this.currentStdTime = currentStdTime;
    }

    public boolean isStatusAddButton() {
        return statusAddButton;
    }

    public void setStatusAddButton(boolean statusAddButton) {
        this.statusAddButton = statusAddButton;
    }

    public boolean isStatusSaveButton() {
        return statusSaveButton;
    }

    public void setStatusSaveButton(boolean statusSaveButton) {
        this.statusSaveButton = statusSaveButton;
    }

    public StdTimeSetting getCurrentStdTimeSetting() {
        if (currentStdTimeSetting == null) {
            currentStdTimeSetting = new StdTimeSetting();
        }
        return currentStdTimeSetting;
    }

    public void setCurrentStdTimeSetting(StdTimeSetting currentStdTimeSetting) {
        this.currentStdTimeSetting = currentStdTimeSetting;
    }

    public String getValidateHeaderName() {
        return validateHeaderName;
    }

    public void setValidateHeaderName(String validateHeaderName) {
        this.validateHeaderName = validateHeaderName;
    }

    public String getValidateHours() {
        return validateHours;
    }

    public void setValidateHours(String validateHours) {
        this.validateHours = validateHours;
    }

    public String getValidateMinutes() {
        return validateMinutes;
    }

    public void setValidateMinutes(String validateMinutes) {
        this.validateMinutes = validateMinutes;
    }

//    private String testinput;
//    public void changeToTest()
//    {
//        System.out.println("Test : "+testinput);
//        //test in sert data 
//             
//        
//    }
//
//    public String getTestinput() {
//        return testinput;
//    }
//
//    public void setTestinput(String testinput) {
//        this.testinput = testinput;
//        currentStdTimeSetting  = new StdTimeSetting();
//        currentStdTimeSetting.setDateCreate(new timeCurrent().getDate());
//        currentStdTimeSetting.setDeleted(0);
//        currentStdTimeSetting.setHeaderName("sdfaf");
//        currentStdTimeSetting.setHours(testinput);
//        currentStdTimeSetting.setMinute(testinput); 
//        currentStdTimeSetting.setIdStdSetting("2");
//        try
//        {
//            getSettingJpaController().create(currentStdTimeSetting);
//        }
//        catch(Exception e)
//        {
//            System.out.print(e);
//        }
//    } 
//    private String indexEdit = "2";
//    public void editTest()
//    {
//         currentStdTimeSetting  = new StdTimeSetting();
//         currentStdTimeSetting  = getSettingJpaController().findIdForEdit(indexEdit);
//         currentStdTimeSetting.setHeaderName("test2");
//         currentStdTimeSetting.setHours("20");
//        try
//        {
//            getSettingJpaController().edit(currentStdTimeSetting);
//        }
//        catch(Exception e)
//        {
//            System.out.print(e);
//        } 
//    }
//
//    public String getIndexEdit() {
//        return indexEdit;
//    }
//
//    public void setIndexEdit(String indexEdit) {
//        this.indexEdit = indexEdit;
//    }
//    private List<StdTimeData>  itemList ;
//    
//    public void showListData()
//    {
//          
////         getItemList().add(new StdTimeData(indexEdit, null, "header name", validateHours, testinput, indexEdit));
////           getItemList().add(new StdTimeData(indexEdit, null, "header name", validateHours, testinput, indexEdit));
//         itemList = new  ArrayList<StdTimeData>();
//         List<StdTimeSetting>  objData  =  getSettingJpaController().findAllNotDelete();
//         for(StdTimeSetting objList : objData )
//         {
//              getItemList().add(new StdTimeData(indexEdit, null,objList.getHeaderName(), validateHours, testinput, indexEdit));
//         }
//         setMasterModel();
//    }
//
//    public List<StdTimeData> getItemList() {
//        if(itemList == null)
//        {
//            itemList = new  ArrayList<StdTimeData>();
//        }
//        return itemList;
//    }
//
//    public void setItemList(List<StdTimeData> itemList) {
//        this.itemList = itemList;
//    }
//    private DataModel masterModel;
//    private PaginationHelper worksheetpagination;
//     public DataModel getMasterModel() {
//        return masterModel;
//    }
//
//    public void setMasterModel() {
//        this.masterModel = getWorksheetpagination().createPageDataModel();
//    }
//    
//    public void previous() {
//        getWorksheetpagination().previousPage();
//        setMasterModel();
//    }
//
//    public void next() {
//        getWorksheetpagination().nextPage();
//        setMasterModel();
//    }
//
//    public int getAllPages() {
//        int all = getWorksheetpagination().getItemsCount();
//        int pagesize = getWorksheetpagination().getPageSize();
//        return ((int) all / pagesize) + 1;
//    }
//
//    public PaginationHelper getWorksheetpagination() {
//        if (worksheetpagination == null) {
//            worksheetpagination = new PaginationHelper(2) {
//
//                @Override
//                public int getItemsCount() {
//                    //fc.print("size of data : "+getMasterListViewData().size());
//                    return getItemList().size();
//                }
//
//                @Override
//                public DataModel createPageDataModel() {
//                    fc.print("PageFirst : " + getPageFirstItem());
//                    fc.print("PageLase : " + getPageLastItem());
//                    ListDataModel listmodel;
//                    if (getItemList().size() > 0) {
//                        listmodel = new ListDataModel(getItemList().subList(getPageFirstItem(), getPageLastItem() + 1));
//                    } else {
//                        listmodel = new ListDataModel(getItemList());
//                    }
//                    return listmodel;
//                }
//            };
//        }
//        return worksheetpagination;
//    }
//    private boolean showPopup = false;
//    public void showPopup()
//    {
//        showPopup = true;
//    }
//    public void closePopup()
//    {
//        showPopup =false;
//    }
//    public boolean isShowPopup() {
//        return showPopup;
//    }
//
//    public void setShowPopup(boolean showPopup) {
//        this.showPopup = showPopup;
//    }
//    
    public StdDataShowInput getCurrentDataShowInput() {
        return currentDataShowInput;
    }

    public void setCurrentDataShowInput(StdDataShowInput currentDataShowInput) {
        this.currentDataShowInput = currentDataShowInput;
    }
}
