/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.setting.stdtime.data;

import java.util.Date;

/**
 *
 * Machine User
 *
 * @author pang Development by Chusak laojang Date : Nov 1, 2012, Time :
 * 11:12:04 AM Copy Right 4 Xtreme Co.,Ltd.
 */
public class StdTimeData {

    private String index;
    private Date date;
    private String headerName;
    private String hours;
    private String minute;
    private String number;

    public StdTimeData(String index, Date date, String headerName, String hours, String minute, String number) {
        this.index = index;
        this.date = date;
        this.headerName = headerName;
        this.hours = hours;
        this.minute = minute;
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getHeaderName() {
        return headerName;
    }

    public void setHeaderName(String headerName) {
        this.headerName = headerName;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }
}
