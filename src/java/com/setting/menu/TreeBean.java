package com.setting.menu;

import com.icesoft.faces.component.tree.IceUserObject; 
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

/**
 * <p>
 * A basic backing bean for a ice:tree component.  The only instance variable
 * needed is a DefaultTreeModel Object which is bound to the icefaces tree
 * component in the jspx code.</p>
 * <p>
 * The tree created by this backing bean is very simple, containing only text
 * nodes.  The plus and minus icons which expand the tree are rendered because
 * of attributes set at the component level.
 * </p>
 *
 */
public class TreeBean {

    // tree default model, used as a value for the tree component
    private DefaultTreeModel model;
    private DefaultMutableTreeNode addMenu(String name,String action,boolean leaf){
          DefaultMutableTreeNode branchNode = new DefaultMutableTreeNode();
            IceUserObject branchObject = new IceUserObject(branchNode);
            branchObject.setText(name);
            branchObject.setTooltip(action);
            branchNode.setUserObject(branchObject);
            branchObject.setLeaf(leaf);
        return branchNode;
    }
    public TreeBean(){
        // create root node with its children expanded
        DefaultMutableTreeNode rootTreeNode = new DefaultMutableTreeNode();
        IceUserObject rootObject = new IceUserObject(rootTreeNode);
        rootObject.setText("Main Menu");
        rootObject.setExpanded(false);
        rootTreeNode.setUserObject(rootObject);
         
        // model is accessed by by the ice:tree component
            model =  new DefaultTreeModel(rootTreeNode);
            DefaultMutableTreeNode frontNode = addMenu("Module Front line", "",false);
            frontNode.add(addMenu("Front line","worksheetlist",true));
            rootTreeNode.add(frontNode);
            DefaultMutableTreeNode backNode = addMenu("Module Back line", "",false);
            // <f:param name="myParam" value="createAutoWorksheet">Create Auto worksheet</f:param>  
            backNode.add(addMenu("Create Assy worksheet","createAssyWorksheet",true));
            // <f:param name="myParam" value="assyWoeksheetList">Assy worksheet list</f:param>  
            backNode.add(addMenu("Assy worksheet list","assyWoeksheetList",true));
            //  <f:param name="myParam" value="createAutoWorksheet">Create Auto worksheet</f:param>  
            backNode.add(addMenu("Create Auto worksheet","createAutoWorksheet",true));
            //  <f:param name="myParam" value="autoworksheetList">Auto worksheet list</f:param>  
            backNode.add(addMenu("Auto worksheet list","autoworksheetList",true)); 
            //  <f:param name="myParam" value="createLineworksheet">Create line worksheet</f:param>  
            backNode.add(addMenu("Create line worksheet","createLineworksheet",true));
            //  <f:param name="myParam" value="lineWorksheetList">Line worksheet list</f:param>  
            backNode.add(addMenu("Line worksheet list","lineWorksheetList",true)); 
            //  <f:param name="myParam" value="createpacking">Create Packing</f:param>  
            backNode.add(addMenu("Create Packing","createpacking",true));
            //  <f:param name="myParam" value="packing">Packing</f:param>  
            backNode.add(addMenu("Packing","packing",true));
            //  <f:param name="myParam" value="refu">Refu</f:param> 
            backNode.add(addMenu("Refu","refu",true));
            //  <f:param name="myParam" value="refulist">Refu List</f:param>  
            backNode.add(addMenu("Refu List","refulist",true));
            //  <f:param name="myParam" value="pcb">PCB</f:param>  
            backNode.add(addMenu("PCB","pcb",true));
            //  <f:param name="myParam" value="pcblist">PCB List</f:param>  
            backNode.add(addMenu("PCB List","pcblist",true));
            rootTreeNode.add(backNode);  
           
            DefaultMutableTreeNode reportNode = addMenu("Report", "",false);
            reportNode.add(addMenu("Report List","reportList",true));
            reportNode.add(addMenu("R Chart","rChart",true));
            rootTreeNode.add(reportNode);
            
            DefaultMutableTreeNode settingNode = addMenu("Setting", "",false);
            settingNode.add(addMenu("Edit model step","editModelStep",true));
            settingNode.add(addMenu("R Chart","rChart",true));
            settingNode.add(addMenu("Data process","dataProcess",true));
            settingNode.add(addMenu("Edit NG","editNG",true));
            settingNode.add(addMenu("Select barcode type","selectBarcodeType",true));
            rootTreeNode.add(settingNode);
            
            DefaultMutableTreeNode machineNode = addMenu("Machine", "",false);
            machineNode.add(addMenu("M/C#","mc",true));
            machineNode.add(addMenu("M/C# Performance","mcPerformance",true));
            rootTreeNode.add(machineNode);
            
            DefaultMutableTreeNode configNode = addMenu("App Config", "",false);
            configNode.add(addMenu("Create user","createUser",true));
            configNode.add(addMenu("User config","userConfig",true));
            configNode.add(addMenu("Role","role",true));
            rootTreeNode.add(configNode);

    }

    /**
     * Gets the tree's default model.
     *
     * @return tree model.
     */
    public DefaultTreeModel getModel() {
        return model;
    }

}
