/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moduleBackLine.bean;

import com.component.table.sortable.SortableList;
import com.moduleBackLine.Dataworksheet.workSheetDetail;
import com.tct.data.*;
import com.tct.data.jpa.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import javax.faces.event.ActionEvent;
import javax.persistence.Persistence;

/**
 *
 * @author The_Boy_Cs
 */
public class packingList extends SortableList {

    private static final String orderNumberColum = "Order Number";
    private static final String typeModelColum = "Model";
//    private static final String typeBobbinColum = "Product code name";
    private static final String dateCreateColum = "Date Create";
    private static final String processColum = "Process";
    private static final String statusColum = "Status";
    private String ordernumber;
    private String typemodel;
    private String process;
    private workSheetDetail[] worksheet;
    private List<DetailPacking> detailkinglist = null;
    private Details details;
    private DetailPackingJpaController detailPackingJpaController = null;

    public packingList() {
        super(orderNumberColum);
        loadWorkSheet();
    }

    public void searchWorksheet(ActionEvent event) {
        print("=================================================================");
        if (ordernumber != null && !ordernumber.trim().isEmpty()) {
            print("= Order Number :: " + ordernumber);
            searchByBarcode();
        } else if (process != null && !process.trim().isEmpty()) {
            print("= Process :: " + process);
            searchByProcess();
        } else if (typemodel != null && !typemodel.trim().isEmpty()) {
            print("= Type Model :: " + typemodel);
            searchByModel();
        } else if ((ordernumber != null && process != null) && (!ordernumber.trim().isEmpty() && !process.trim().isEmpty())) {
            print("= Order Number : " + ordernumber + " And Process : " + process);
            searchByBarcodeAndProcess();
        } else if ((ordernumber != null && typemodel != null) && (!ordernumber.trim().isEmpty() && !typemodel.trim().isEmpty())) {
            print("= Ordernumber : " + ordernumber + " And Model : " + typemodel);
            searchByBarcodeAndModel();
        } else if ((typemodel != null && process != null) && (!typemodel.trim().isEmpty() && !process.trim().isEmpty())) {
            print("= Model : " + typemodel + " And Process : " + process);
            searchByModelAndProcess();
        } else {
            loadWorkSheet();
        }
    }

    private void searchByModelAndProcess() {
        worksheetlist = null;
        print("================== Search By Model Process ======================");
        List<Details> detaillist = getDetailsJpaController().findDetailsByModelAndDetailNamePacking(typemodel);
        List<ProcessPool> pcpools = getProcessPoolJpaController().findProcessPoolByName(process);
        if(detaillist.isEmpty() || pcpools.isEmpty()){
            getWorksheetList();
        }
        for (Details detail : detaillist) {
            List<LotControl> lotcontrols = getLotControlJpaController().findByDetailsId(detail);
            for (LotControl lotcontrol : lotcontrols) {
                for (ProcessPool pcpool : pcpools) {
                    if (lotcontrol.getMainDataId() != null && !lotcontrol.getMainDataId().trim().isEmpty()) {
                        MainData maindata = getMainDataJpaController().findMainDataByProcessAndLotControl(pcpool, lotcontrol);
                        CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(maindata.getCurrentNumber());
                        getWorksheetList().add(new workSheetDetail(detail,lotcontrol.getBarcode(),lotcontrol.getBarcode(), detail.getModel(), "", detail.getDateCreate(), pcpool.getProcName(), currentProcess.getStatus()));
                        print("= Detail : " + detail.getDetailName() + " :: Barcode : " + maindata.getLotControlId().getBarcode());
                    }
                }
            }
        }
    }

    private void searchByBarcodeAndModel() {
        worksheetlist = null;
        print("====================Search By Model Order");
        lotControllist = getLotControlJpaController().findLotcontrolByBarcode(ordernumber);
        List<Details> detaillist = getDetailsJpaController().findDetailsByModelAndDetailNamePacking(typemodel);
        if(lotControllist.isEmpty() || detaillist.isEmpty()){
            getWorksheetList();
        }
        for (LotControl lotcontrol : lotControllist) {
            for (Details detail : detaillist) {
                if (detail.getDetailName().equals("detail_packing")) {
                    MainData maindata = getMainDataJpaController().findMainDataByLotControl(lotcontrol);
                    if (lotcontrol.getMainDataId() != null && !lotcontrol.getMainDataId().trim().isEmpty()) {
                        ProcessPool pcpool = maindata.getIdProc();
                        CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(maindata.getCurrentNumber());
                        getWorksheetList().add(new workSheetDetail(detail,lotcontrol.getBarcode(),lotcontrol.getBarcode(), detail.getModel(), "", detail.getDateCreate(), pcpool.getProcName(), currentProcess.getStatus()));
                        print("= Detail : " + detail.getDetailName() + " :: Barcode : " + maindata.getLotControlId().getBarcode());
                    } else {
                        getWorksheetList().add(new workSheetDetail(detail,lotcontrol.getBarcode(),lotcontrol.getBarcode(), detail.getModel(), "", detail.getDateCreate(), "", ""));
                        print("= Detail : " + detail.getDetailName() + " :: Barcode : " + lotcontrol.getBarcode());
                    }
                }
            }
        }
    }

    private void searchByBarcodeAndProcess() {
        worksheetlist = null;
        print("================ Search by barcode And Process==============================");
        lotControllist = getLotControlJpaController().findLotcontrolByBarcode(ordernumber);
        List<ProcessPool> pcpools = getProcessPoolJpaController().findProcessPoolByName(process);
        if(pcpools.isEmpty() || lotControllist.isEmpty()){
            getWorksheetList();
        }
        for (LotControl lotcontrol : lotControllist) {
            Details detail = lotcontrol.getDetailsIdDetail();
            if (detail.getDetailName().equals("detail_packing")) {
                for (ProcessPool pcpool : pcpools) {
                    MainData maindata = getMainDataJpaController().findMainDataByProcessAndLotControl(pcpool, lotcontrol);
                    if (lotcontrol.getMainDataId() != null && !lotcontrol.getMainDataId().trim().isEmpty()) {
                        CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(maindata.getCurrentNumber());
                        getWorksheetList().add(new workSheetDetail(detail,lotcontrol.getBarcode(),lotcontrol.getBarcode(), detail.getModel(), "", detail.getDateCreate(), pcpool.getProcName(), currentProcess.getStatus()));
                        print("= Detail : " + detail.getDetailName() + " :: Barcode : " + maindata.getLotControlId().getBarcode());
                    } else {
                        getWorksheetList().add(new workSheetDetail(detail,lotcontrol.getBarcode(),lotcontrol.getBarcode(), detail.getModel(), "", detail.getDateCreate(), "", ""));
                        print("= Detail : " + detail.getDetailName() + " :: Barcode : " + lotcontrol.getBarcode());
                    }
                }
            }
        }
        setWorksheet();
    }
    private DetailsJpaController detailsJpaController = null;

    private DetailsJpaController getDetailsJpaController() {
        if (detailsJpaController == null) {
            detailsJpaController = new DetailsJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailsJpaController;
    }

    private void searchByModel() {
        worksheetlist = null;
        print("=================================================================");
        List<Details> detailslist = getDetailsJpaController().findDetailsByModelAndDetailNamePacking(typemodel);
        if(detailslist.isEmpty()){
            getWorksheetList();
        }
//        worksheet = new workSheetDetail[detailflList.size()];
        for (Details detail : detailslist) {
//            details = detailfl.getDetailIdDetail();
            lotControllist = getLotControlJpaController().findByDetailsId(detail);
            for (LotControl lotControl : lotControllist) {
                try {
                    if (lotControl.getMainDataId() != null) {
                        print("T");
                        MainData mainData = getMainDataJpaController().findMainData(lotControl.getMainDataId());
                        ProcessPool processPool = mainData.getIdProc();
                        CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(mainData.getCurrentNumber());
                        getWorksheetList().add(new workSheetDetail(detail,lotControl.getBarcode(),lotControl.getBarcode(), detail.getModel(), "", detail.getDateCreate(), processPool.getProcName(), currentProcess.getStatus()));
                        print("= Detail : " + detail.getDetailName() + " :: Barcode : " + lotControl.getBarcode());
                    } else {
                        getWorksheetList().add(new workSheetDetail(detail,lotControl.getBarcode(),lotControl.getBarcode(), detail.getModel(), "", details.getDateCreate(), null, null));
                        print("Null");
                    }
                } catch (Exception e) {
                    print("= Error : " + e);
                }
            }
        }
        setWorksheet();
    }

    private void searchByProcess() {
        worksheetlist = null;
        print("=================================================================");
        List<ProcessPool> pcpools = getProcessPoolJpaController().findProcessPoolByName(process);
        if(pcpools.isEmpty()){
            getWorksheetList();
        }
        for (ProcessPool pcpool : pcpools) {
            List<MainData> maindatas = getMainDataJpaController().findMainDataByProcess(pcpool);
            for (MainData maindata : maindatas) {
                LotControl lotControl = maindata.getLotControlId();
                Details detail = lotControl.getDetailsIdDetail();

                if (detail.getDetailName().equals("detail_packing")) {
//                    DetailAssy detailassy = getDetailAssyJpaController().findDetailFlByDetails(detail);
                    CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(maindata.getCurrentNumber());
                    getWorksheetList().add(new workSheetDetail(detail,lotControl.getBarcode(),lotControl.getBarcode(), detail.getModel(), "", detail.getDateCreate(), pcpool.getProcName(), currentProcess.getStatus()));
                    print("= Detail : " + detail.getDetailName() + " :: Barcode : " + maindata.getLotControlId().getBarcode());
                }
            }
        }
        setWorksheet();
    }
    private ProcessPoolJpaController processPoolJpaController = null;

    private ProcessPoolJpaController getProcessPoolJpaController() {
        if (processPoolJpaController == null) {
            processPoolJpaController = new ProcessPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return processPoolJpaController;
    }

    private void searchByBarcode() {
        worksheetlist = null;
        print("================ search by barcode ==============================");
        lotControllist = getLotControlJpaController().findLotcontrolByBarcode(ordernumber);
        if(lotControllist.isEmpty()){
            getWorksheetList();
        }
        for (LotControl lotcontrol : lotControllist) {
            Details detail = lotcontrol.getDetailsIdDetail();
            if (detail.getDetailName().equals("detail_packing")) {
                print("= Maindata id :: " + lotcontrol.getMainDataId());
                if (lotcontrol.getMainDataId() != null && !lotcontrol.getMainDataId().trim().isEmpty()) {
                    MainData maindata = getMainDataJpaController().findMainData(lotcontrol.getMainDataId());
                    ProcessPool pcpool = maindata.getIdProc();
//                DetailFl detailfl = getDetailFlJpacontroller().findDetailFlByDetails(detail);
                    CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(maindata.getCurrentNumber());
                    getWorksheetList().add(new workSheetDetail(detail,lotcontrol.getBarcode(),lotcontrol.getBarcode(), detail.getModel(), "", detail.getDateCreate(), pcpool.getProcName(), currentProcess.getStatus()));
                    print("= Detail : " + detail.getDetailName() + " :: Barcode : " + maindata.getLotControlId().getBarcode());
                } else {
                    getWorksheetList().add(new workSheetDetail(detail,lotcontrol.getBarcode(),lotcontrol.getBarcode(), detail.getModel(), "", detail.getDateCreate(), "", ""));
                    print("= Detail : " + detail.getDetailName() + " :: Barcode : " + lotcontrol.getBarcode());
                }
            }
        }
        setWorksheet();
    }

    private DetailPackingJpaController getDetailPackingJpaController() {
        if (detailPackingJpaController == null) {
            detailPackingJpaController = new DetailPackingJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailPackingJpaController;
    }

    private void loadWorkSheet() {
        worksheetlist = null;
        print("=================================================================");
        detailkinglist = getDetailPackingJpaController().findDetailPackingEntities();
        if(detailkinglist.isEmpty()){
            getWorksheetList();
        }
//        worksheet = new workSheetDetail[detailflList.size()];
        for (DetailPacking detailking : detailkinglist) {
            details = detailking.getDetailsIdDetail();
            lotControllist = getLotControlJpaController().findByDetailsId(details);
            for (LotControl lotControl : lotControllist) {
                try {
                    if (lotControl.getMainDataId() != null) {
                        print("T");
                        MainData mainData = getMainDataJpaController().findMainData(lotControl.getMainDataId());
                        ProcessPool processPool = mainData.getIdProc();
                        CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(mainData.getCurrentNumber());

                        getWorksheetList().add(new workSheetDetail(details,lotControl.getBarcode(),lotControl.getBarcode(), details.getModel(), null, details.getDateCreate(), processPool.getProcName(), currentProcess.getStatus()));
                        print("= Detail : " + details.getDetailName() + " :: Barcode : " + lotControl.getBarcode());
                    } else {
                        getWorksheetList().add(new workSheetDetail(details,lotControl.getBarcode(),lotControl.getBarcode(), details.getModel(), null, details.getDateCreate(), "", ""));
                        print("Null");
                    }
                } catch (Exception e) {
                    print("= Error : " + e);
                }
            }
        }
        setWorksheet();
    }

    private void setWorksheet() {
        int i = 0;
        worksheet = new workSheetDetail[worksheetlist.size()];
        for (workSheetDetail worksheets : worksheetlist) {
            worksheet[i++] = worksheets;
            print("= I : " + i + " :: worksheet : " + worksheets.getProcess());
        }
    }
    private List<LotControl> lotControllist = null;
    private LotControlJpaController lotControlJpaController = null;

    private LotControlJpaController getLotControlJpaController() {
        if (lotControlJpaController == null) {
            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotControlJpaController;
    }
    private MainDataJpaController mianDataJpaController = null;

    private MainDataJpaController getMainDataJpaController() {
        if (mianDataJpaController == null) {
            mianDataJpaController = new MainDataJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mianDataJpaController;
    }
    private CurrentProcessJpaController currentProcesslist = null;

    private CurrentProcessJpaController getCurrentProcessJpaController() {
        if (currentProcesslist == null) {
            currentProcesslist = new CurrentProcessJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return currentProcesslist;
    }
    private List<workSheetDetail> worksheetlist = null;

    private List<workSheetDetail> getWorksheetList() {
        if (worksheetlist == null) {
            worksheetlist = new ArrayList<workSheetDetail>();
        }
        return worksheetlist;
    }

    private void print(String str) {
        System.out.println(str);
    }

    public workSheetDetail[] getWorksheet() {
        if (!oldSort.equals(sortColumnName)
                || oldAscending != ascending) {
            sort();
            oldSort = sortColumnName;
            oldAscending = ascending;
        }
        return worksheet;
    }

    @Override
    protected void sort() {

        // TODO Auto-generated method stub
        Comparator comparator = new Comparator() {

            public int compare(Object o1, Object o2) {
                workSheetDetail c1 = (workSheetDetail) o1;
                workSheetDetail c2 = (workSheetDetail) o2;
                if (sortColumnName == null) {
                    return 0;
                }
                if (sortColumnName.equals(orderNumberColum)) {
                    return ascending
                            ? c1.getOrdernumber().compareTo(c2.getOrdernumber())
                            : c2.getOrdernumber().compareTo(c1.getOrdernumber());
                } else if (sortColumnName.equals(typeModelColum)) {
                    return ascending ? c1.getTypemodel().compareTo(c2.getTypemodel())
                            : c2.getTypemodel().compareTo(c1.getTypemodel());
//                } else if (sortColumnName.equals(typeBobbinColum)) {
//                    return ascending ? c1.getTypebobbin().compareTo(c2.getTypebobbin())
//                            : c2.getTypebobbin().compareTo(c1.getTypebobbin());
                } else if (sortColumnName.equals(dateCreateColum)) {
                    return ascending
                            ? c1.getCreatedate().compareTo(c2.getCreatedate())
                            : c2.getCreatedate().compareTo(c1.getCreatedate());
                } else if (sortColumnName.equals(processColum)) {
                    return ascending
                            ? c1.getProcess().compareTo(c2.getProcess())
                            : c2.getProcess().compareTo(c1.getProcess());
                } else if (sortColumnName.equals(statusColum)) {
                    return ascending
                            ? c1.getStatus().compareTo(c2.getStatus())
                            : c2.getStatus().compareTo(c1.getStatus());
                } else {
                    return 0;
                }
            }
        };
        Arrays.sort(worksheet, comparator);
    }

    @Override
    protected boolean isDefaultAscending(String sortColumn) {
        return true;
    }

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getTypemodel() {
        return typemodel;
    }

    public void setTypemodel(String typemodel) {
        this.typemodel = typemodel;
    }

    public String getDateCreateColum() {
        return dateCreateColum;
    }

    public String getOrderNumberColum() {
        return orderNumberColum;
    }

    public String getProcessColum() {
        return processColum;
    }

    public String getStatusColum() {
        return statusColum;
    }

    public String getTypeModelColum() {
        return typeModelColum;
    }
}
