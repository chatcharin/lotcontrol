/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moduleBackLine.bean;

import com.appCinfigpage.bean.userLogin;
import com.moduleBackLine.Dataworksheet.blBarcode;
import com.moduleFontline.DataWorksheet.detailProcess;
import com.tct.barcodegenerate.BarCodeGenerter;
import com.tct.data.*;
import com.tct.data.jpa.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;

/**
 *
 * @author The_Boy_Cs
 */
public class Refu {

    private String newlotcontrol;
    private String newstamp;
    private ModelPool modelPool;
    private String l1;
    private String l2;
    private String refutype;
    private List<ModelPool> modelPoollist = null;
    private List<SelectItem> model;
    private List<SelectItem> series;
    private List<detailProcess> detailprocess;
    private BarCodeGenerter barCodeGenerter;
    private Details detail = null;

    public Refu() {
//        loadModelName();
        printassyworksheet.barcode = null;
    }
    
    public void changRefuTypeListener(ValueChangeEvent event){
        print("=================================================================");
        refutype = event.getNewValue().toString();
        print("= New Refu type : "+refutype);
    }

    public boolean createRefuFun(ActionEvent event) {
        print("=================================================================");
        try {
            int lot = Integer.parseInt(newlotcontrol);
            detail = new Details();
            detail.setIdDetail(UUID.randomUUID().toString());
            detail.setDateCreate(new Date());
            detail.setDetailName("refu01");
            detail.setDetailType("backline");
            detail.setLotcontrol(newlotcontrol);
            detail.setModel(modelPool.getModelName());
            detail.setUserCreate(userLogin.getSessionUser().getId());
            detail.setLine("");

            Refu01 refu = new Refu01();
            refu.setId(UUID.randomUUID().toString());
            refu.setL1(l1);
            refu.setL2(l2);
            refu.setStamp(newstamp);
            refu.setDetailIdDetail(detail);
            refu.setType(refutype);

            getDetailsJpaController().create(detail);
            getRefu01JpaController().create(refu);

            for (int i = 0; i < lot; i++) {

                String barcode = GenBarcode(lot, i);
                print("= Barcode : "+barcode);
                LotControl lotControl = new LotControl();
                lotControl.setId(UUID.randomUUID().toString());
                lotControl.setLotNumber(i + "");
                lotControl.setModelPoolId(modelPool);
                lotControl.setDetailsIdDetail(detail);
                lotControl.setBarcode(barcode);

                if (genBarCode(barcode, "C:/tct_project_netbean/web/images/barcode/b/refu/")) {
                    getLotControlJpaController().create(lotControl);
                    getBarcodelist().add(new blBarcode(lotControl.getId(), barcode, "../images/barcode/b/refu/", true));
                    print("= Barcode : " + barcode);
                }
            }
            Thread.sleep(5000);
            return true;
        } catch (Exception e) {
            print("= Error : " + e);
            return false;
        }
    }
    
    private List<blBarcode> getBarcodelist() {
        if (printassyworksheet.barcode == null) {
            printassyworksheet.barcode = new ArrayList<blBarcode>();
        }
        return printassyworksheet.barcode;
    }
    
    private LotControlJpaController lotControlJpaController = null;
    private LotControlJpaController getLotControlJpaController(){
        if(lotControlJpaController == null){
            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotControlJpaController;
    }

    private BarCodeGenerter getBarCodeGenerter() {
        if (barCodeGenerter == null) {
            barCodeGenerter = new BarCodeGenerter();
        }
        return barCodeGenerter;
    }

    private boolean genBarCode(String code, String filePath) {
        print("=================================================================");
        try {
            getBarCodeGenerter().createBarcode(code, filePath + "b/");
            getBarCodeGenerter().createQrCode(code, filePath);
            print("= GenCode : Complete");
            return true;
        } catch (Exception e) {
            print("= Error can not genbarcode." + e.toString());
            return false;
        }
    }
    private DetailsJpaController detailsJpaController = null;

    private DetailsJpaController getDetailsJpaController() {
        if (detailsJpaController == null) {
            detailsJpaController = new DetailsJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailsJpaController;
    }
    private Refu01JpaController refu01JpaController = null;

    private Refu01JpaController getRefu01JpaController() {
        if (refu01JpaController == null) {
            refu01JpaController = new Refu01JpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return refu01JpaController;
    }

    private void loadProcess() {
        print("=================================================================");
        try {
            detailprocess = new ArrayList<detailProcess>();
            List<Md2pc> md2pclist = getMd2pcJpaController().findBymodelPoolId(modelPool);
            for (Md2pc md2pc : md2pclist) {
                print("= md2pc : " + md2pc.getMd2pcId());
                ProcessPool pcpool = md2pc.getProcessPoolIdProc();
                print("= Process : " + pcpool.getProcName());
                detailProcess dp = new detailProcess("", null, null, null, null, "", pcpool.getProcName().toString(), "", 0, 0, 0, "", "");
                detailprocess.add(dp);
            }
        } catch (Exception e) {
            print("= Ex : " + e);
        }
    }
    private Md2pcJpaController md2pcJpaController = null;

    private Md2pcJpaController getMd2pcJpaController() {
        if (md2pcJpaController == null) {
            md2pcJpaController = new Md2pcJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return md2pcJpaController;
    }

    public void changModelSeriesListener(ValueChangeEvent event) {
        print("=================================================================");
        try {
            String modelId = event.getNewValue().toString();
            print("= New Value : " + modelId);
            modelPool = getModelPoolJpaController().findModelPool(modelId);
            print("= Model : " + modelPool.getModelName() + " modelSeries : " + modelPool.getModelSeries());
            loadProcess();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changModelNameListener(ValueChangeEvent event) {
        detailprocess = new ArrayList<detailProcess>();
        print("=================================================================");
        try {
            String modelname = event.getNewValue().toString();
            print("= New Value : " + modelname);
            modelPoollist = getModelPoolJpaController().findByModelname(modelname);
            series = new ArrayList<SelectItem>();
            for (ModelPool models : modelPoollist) {
                print("= Series : " + models.getModelSeries());
                series.add(new SelectItem(models.getId(), models.getModelSeries()));
            }
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    private void loadModelName() {
        print("=================================================================");
        try {
            List<ModelPool> modellist = getModelPoolJpaController().findModelPoolGroupByName();
            model = new ArrayList<SelectItem>();
            for (ModelPool modellis : modellist) {
                model.add(new SelectItem(modellis.getModelName(), modellis.getModelName()));
            }
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    private void print(String str) {
        System.out.println(str);
    }
    private ModelPoolJpaController modelPoolJpaController = null;

    private ModelPoolJpaController getModelPoolJpaController() {
        if (modelPoolJpaController == null) {
            modelPoolJpaController = new ModelPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return modelPoolJpaController;
    }

    public String getL1() {
        return l1;
    }

    public void setL1(String l1) {
        this.l1 = l1;
    }

    public String getL2() {
        return l2;
    }

    public void setL2(String l2) {
        this.l2 = l2;
    }

    public String getNewlotcontrol() {
        return newlotcontrol;
    }

    public void setNewlotcontrol(String newlotcontrol) {
        this.newlotcontrol = newlotcontrol;
    }

    public String getNewstamp() {
        return newstamp;
    }

    public void setNewstamp(String newstamp) {
        this.newstamp = newstamp;
    }

    public List<SelectItem> getModel() {
        loadModelName();
        return model;
    }

    public List<SelectItem> getSeries() {
        if (series == null) {
            series = new ArrayList<SelectItem>();
        }
        return series;
    }

    public List<detailProcess> getDetailprocess() {
        return detailprocess;
    }

    private String GenBarcode(int lot, int i) {
        String barcode = "BRS" + "-" + getBarCodeGenerter().UseNumberToString(Integer.parseInt(detail.getLotcontrol())) + new SimpleDateFormat("MM").format(new Date());
        barcode += "" + new SimpleDateFormat("yy").format(new Date()) + modelPool.getModelSeries() + getBarCodeGenerter().UseNumberToString(i);
        return barcode;
    }
}