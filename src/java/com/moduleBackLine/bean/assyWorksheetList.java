package com.moduleBackLine.bean;

import com.component.table.sortable.SortableList;
import com.moduleBackLine.Dataworksheet.workSheetDetail;
import com.tct.data.*;
import com.tct.data.jpa.*;
import com.tct.data.jsf.util.PaginationHelper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import javax.faces.event.ActionEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.persistence.Persistence;

public class assyWorksheetList {

    //============= search =====================================================
    private String ordernumber;
    private String typemodel;
    private String process;
    private String typebobbin;
    private String series;
    // =========================================================================
    private static final String orderNumberColum = "Barcode";
    private static final String typeModelColum = "Model";
//    private static final String typeBobbinColum = "Producttype";
    private static final String dateCreateColum = "Date Create";
    private static final String processColum = "Process";
    private static final String statusColum = "Status";
    private List<workSheetDetail> worksheetlist;
    private List<DetailAssy> detailassylist;
    private Details details;
    private DataModel worksheet;
    private PaginationHelper pagination;

    public assyWorksheetList() {
    }
    private List<LotControl> lotControllist = null;
    private LotControlJpaController lotControlJpaController = null;

    private LotControlJpaController getLotControlJpaController() {
        if (lotControlJpaController == null) {
            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotControlJpaController;
    }
    private MainDataJpaController mianDataJpaController = null;

    private MainDataJpaController getMainDataJpaController() {
        if (mianDataJpaController == null) {
            mianDataJpaController = new MainDataJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mianDataJpaController;
    }
    private CurrentProcessJpaController currentProcesslist = null;

    private CurrentProcessJpaController getCurrentProcessJpaController() {
        if (currentProcesslist == null) {
            currentProcesslist = new CurrentProcessJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return currentProcesslist;
    }
    private DetailAssyJpaController detailAssyJpaController = null;

    private DetailAssyJpaController getDetailAssyJpaController() {
        if (detailAssyJpaController == null) {
            detailAssyJpaController = new DetailAssyJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailAssyJpaController;
    }
    private ProcessPoolJpaController processPoolJpaController = null;

    private ProcessPoolJpaController getProcessPoolJpaController() {
        if (processPoolJpaController == null) {
            processPoolJpaController = new ProcessPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return processPoolJpaController;
    }
    private DetailsJpaController detailsJpaController = null;

    private DetailsJpaController getDetailsJpaController() {
        if (detailsJpaController == null) {
            detailsJpaController = new DetailsJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailsJpaController;
    }

    public void searchWorksheet(ActionEvent event) {
        print("=================================================================");
        if (ordernumber != null && !ordernumber.trim().isEmpty()) {
            print("= Order Number :: " + ordernumber);
            searchByBarcode();
        } else if (process != null && !process.trim().isEmpty()) {
            print("= Process :: " + process);
            searchByProcess();
        } else if (typemodel != null && !typemodel.trim().isEmpty()) {
            print("= Type Model :: " + typemodel);
            searchByModel();
        } else {
            loadWorkSheet();
        }
    }

    private void searchByModel() {
        worksheetlist = null;
        print("=================================================================");
        List<Details> detailslist = getDetailsJpaController().findDetailsByModelAndDetailNameAssy(typemodel);
        if(detailslist.isEmpty()){
            getWorksheetList();
        }
//        worksheet = new workSheetDetail[detailflList.size()];
        for (Details detail : detailslist) {
//            details = detailfl.getDetailIdDetail();
            lotControllist = getLotControlJpaController().findByDetailsId(detail);
            for (LotControl lotControl : lotControllist) {
                try {
                    if (lotControl.getMainDataId() != null) {
                        print("T");
                        MainData mainData = getMainDataJpaController().findMainData(lotControl.getMainDataId());
                        ProcessPool processPool = mainData.getIdProc();
                        CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(mainData.getCurrentNumber());
                        getWorksheetList().add(new workSheetDetail(detail,lotControl.getBarcode(),lotControl.getBarcode(), detail.getModel(), "", detail.getDateCreate(), processPool.getProcName(), currentProcess.getStatus()));
                        print("= Detail : " + detail.getDetailName() + " :: Barcode : " + lotControl.getBarcode());
                    } else {
                        getWorksheetList().add(new workSheetDetail(detail,lotControl.getBarcode(),lotControl.getBarcode(), detail.getModel(), "", details.getDateCreate(), null, null));
                        print("Null");
                    }
                } catch (Exception e) {
                    print("= Error : " + e);
                }
            }
        }
        setWorksheet();
    }

    private void searchByBarcode() {
        worksheetlist = null;
        print("================ search by barcode ==============================");
        lotControllist = getLotControlJpaController().findLotcontrolByBarcode(ordernumber);
        if(lotControllist.isEmpty()){
            getWorksheetList();
        }
        for (LotControl lotcontrol : lotControllist) {
            Details detail = lotcontrol.getDetailsIdDetail();
            if (detail.getDetailName().equals("detail_assy")) {
                print("= Maindata id :: " + lotcontrol.getMainDataId());
                if (lotcontrol.getMainDataId() != null && !lotcontrol.getMainDataId().trim().isEmpty()) {
                    MainData maindata = getMainDataJpaController().findMainData(lotcontrol.getMainDataId());
                    ProcessPool pcpool = maindata.getIdProc();
//                DetailFl detailfl = getDetailFlJpacontroller().findDetailFlByDetails(detail);
                    CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(maindata.getCurrentNumber());
                    getWorksheetList().add(new workSheetDetail(detail,lotcontrol.getBarcode(),lotcontrol.getBarcode(), detail.getModel(), "", detail.getDateCreate(), pcpool.getProcName(), currentProcess.getStatus()));
                    print("= Detail : " + detail.getDetailName() + " :: Barcode : " + maindata.getLotControlId().getBarcode());
                }else{
                    getWorksheetList().add(new workSheetDetail(detail,lotcontrol.getBarcode(),lotcontrol.getBarcode(), detail.getModel(), "", detail.getDateCreate(), "", ""));
                    print("= Detail : " + detail.getDetailName() + " :: Barcode : " + lotcontrol.getBarcode());
                }
            }
        }
        setWorksheet();
    }

    private void searchByProcess() {
        worksheetlist = null;
        print("=================================================================");
        List<ProcessPool> pcpools = getProcessPoolJpaController().findProcessPoolByName(process);
        if(pcpools.isEmpty()){
            getWorksheetList();
        }
        for (ProcessPool pcpool : pcpools) {
            List<MainData> maindatas = getMainDataJpaController().findMainDataByProcess(pcpool);
            for (MainData maindata : maindatas) {
                LotControl lotControl = maindata.getLotControlId();
                Details detail = lotControl.getDetailsIdDetail();

                if (detail.getDetailName().equals("detail_assy")) {
//                    DetailAssy detailassy = getDetailAssyJpaController().findDetailFlByDetails(detail);
                    CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(maindata.getCurrentNumber());
                    getWorksheetList().add(new workSheetDetail(detail,lotControl.getBarcode(),lotControl.getBarcode(), detail.getModel(), "", detail.getDateCreate(), pcpool.getProcName(), currentProcess.getStatus()));
                    print("= Detail : " + detail.getDetailName() + " :: Barcode : " + maindata.getLotControlId().getBarcode());
                }
            }
        }
        setWorksheet();
    }

    private void loadWorkSheet() {
        worksheetlist = null;
        print("=================================================================");
        detailassylist = getDetailAssyJpaController().findDetailAssyEntities();
        if(detailassylist.isEmpty()){
            getWorksheetList();
        }
        for (DetailAssy detailassy : detailassylist) {
            details = detailassy.getDetailIdDetail();
            lotControllist = getLotControlJpaController().findByDetailsId(details);
            for (LotControl lotControl : lotControllist) {
                try {
                    if (lotControl.getMainDataId() != null) {
                        print("T");
                        MainData mainData = getMainDataJpaController().findMainData(lotControl.getMainDataId());
                        ProcessPool processPool = mainData.getIdProc();
                        CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(mainData.getCurrentNumber());

                        getWorksheetList().add(new workSheetDetail(details,lotControl.getBarcode(),lotControl.getBarcode(), details.getModel(), null, details.getDateCreate(), processPool.getProcName(), currentProcess.getStatus()));
                        print("= Detail : " + details.getDetailName() + " :: Barcode : " + lotControl.getBarcode());
                    } else {
                        getWorksheetList().add(new workSheetDetail(details,lotControl.getBarcode(),lotControl.getBarcode(), details.getModel(), null, details.getDateCreate(), "", ""));
                        print("Null");
                    }
                } catch (Exception e) {
                    print("= Error : " + e);
                }

            }
        }
        setWorksheet();
    }

    private void setWorksheet() {
        worksheet = getPagination().createPageDataModel();
    }

    private void print(String str) {
        System.out.println(str);
    }
    private List<DetailAssy> detailAssylist = null;

    /**
     * @return the ordernumbercolum
     */
    public String getOrdernumbercolum() {
        return orderNumberColum;
    }

    /**
     * @return the typemodelcolum
     */
    public String getTypemodelcolum() {
        return typeModelColum;
    }

//    /**
//     * @return the typebobbincolum
//     */
//    public String getTypebobbincolum() {
//        return typeBobbinColum;
//    }
    /**
     * @return the datecreatecolum
     */
    public String getDatecreatecolum() {
        return dateCreateColum;
    }

    /**
     * @return the processcolum
     */
    public String getProcesscolum() {
        return processColum;
    }

    /**
     * @return the statuscolum
     */
    public String getStatuscolum() {
        return statusColum;
    }

    /**
     * @return the ordernumber
     */
    public String getOrdernumber() {
        return ordernumber;
    }

    /**
     * @param ordernumber the ordernumber to set
     */
    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    /**
     * @return the typemodel
     */
    public String getTypemodel() {
        return typemodel;
    }

    /**
     * @param typemodel the typemodel to set
     */
    public void setTypemodel(String typemodel) {
        this.typemodel = typemodel;
    }

    /**
     * @return the process
     */
    public String getProcess() {
        return process;
    }

    /**
     * @param process the process to set
     */
    public void setProcess(String process) {
        this.process = process;
    }

    /**
     * @return the typebobbin
     */
    public String getTypebobbin() {
        return typebobbin;
    }

    /**
     * @param typebobbin the typebobbin to set
     */
    public void setTypebobbin(String typebobbin) {
        this.typebobbin = typebobbin;
    }

    public DataModel getWorksheet() {
        return worksheet;
    }

    private List<workSheetDetail> getWorksheetList() {
        if (worksheetlist == null) {
            worksheetlist = new ArrayList<workSheetDetail>();
        }
        return worksheetlist;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }
    
    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    return getWorksheetList().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    print("PageFirst : " + getPageFirstItem());
                    print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (getWorksheetList().size() > 0) {
                        listmodel = new ListDataModel(getWorksheetList().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(getWorksheetList());
                    }
                    return listmodel;
                }
            };
        }
        return pagination;
    }
}
