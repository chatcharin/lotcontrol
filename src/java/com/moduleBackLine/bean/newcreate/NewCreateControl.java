/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moduleBackLine.bean.newcreate;

import com.appCinfigpage.bean.userLogin;
import com.moduleBackLine.Dataworksheet.blBarcode;
import com.moduleBackLine.Dataworksheet.workSheetDetail;
import com.moduleBackLine.bean.printassyworksheet;
import com.moduleFontline.DataWorksheet.detailProcess;
import com.tct.barcodegenerate.BarCodeGenerter;
import com.tct.data.*;
import com.tct.data.jpa.*;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jsf.util.PaginationHelper;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;

/**
 *
 * @author The_Boy_Cs
 */
public class NewCreateControl {
//+============ Search ======================

    private String barcodeSearch;
    private String modelSearch;
    private String seriesSearch;
    private String stampSearch;
    private String dateSearch;
    private String processSearch;
//    ===============================
//------------- select box ------------------
    private List<SelectItem> typelist;
    private List<SelectItem> modelpoollist;
//    ---------------------------------------
    private List<String> modelserieslist;
    private List<detailProcess> detailprocess;
    private ModelPool currentModelPool;
    private List<ModelPool> modelPoolslist;
    private Details currentdetails;
    private Refu01 currentRefu01;
    private LotControl currentLotControl;
//  for listview ========================
    private List<workSheetDetail> worksheetlist;
    private DataModel<workSheetDetail> worksheet;
    private List<Refu01> detailRefulist;
    private List<LotControl> lotControllist;

    public NewCreateControl() {
        searchNewcreateWorksheet();
    }
    
    public void changToSave(ActionEvent event){
        print("================== Chang to Save ================================");
        try {
            getDetailsJpaController().edit(currentdetails);
            getRefu01JpaController().edit(currentRefu01);
            String link = "pages/moduleBackLine/auto/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (NonexistentEntityException nex) {
            print("= Error : " + nex);
        } catch (Exception e) {
            print("= Error2 : " + e);
        }
    }

    public void searchNewcreateWorksheet() {
        print("==================== Search worksheet Auto ======================");
        try {
            if (barcodeSearch != null && !barcodeSearch.trim().isEmpty()) {
                print("= Order Number :: " + barcodeSearch);
//                searchByBarcode();
            } else if (processSearch != null && !processSearch.trim().isEmpty()) {
                print("= Process :: " + processSearch);
//                searchByProcess();
            } else if (modelSearch != null && !modelSearch.trim().isEmpty()) {
                print("= Type Model :: " + modelSearch);
//                searchByModel();
            } else {
                loadWorkSheet();
            }
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changToPrint(ActionEvent event) {
        print("=================== Chang to Print ==============================");
        try {
            printassyworksheet.barcode = null;
            printassyworksheet.detail = null;
            workSheetDetail workSheetDetails = (workSheetDetail) getWorksheet().getRowData();
            currentLotControl = getLotControlJpaController().findLotcontrolByBarcode(workSheetDetails.getBarcode()).get(0);
            currentdetails = currentLotControl.getDetailsIdDetail();
            currentModelPool = currentLotControl.getModelPoolId();
            currentRefu01 = getRefu01JpaController().findDataRefu01List(currentdetails).get(0);
            getBarcodelist().add(new blBarcode(currentLotControl.getId(), currentLotControl.getBarcode(), "./images/barcode/", true));
            printassyworksheet.detail = currentdetails;
            String link = "pages/moduleBackLine/newcreate/printview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changToEdit(ActionEvent event) {
        print("===================== Chang to edit =============================");
        detailprocess = null;
        try {
            workSheetDetail workSheetDetails = (workSheetDetail) getWorksheet().getRowData();
            currentLotControl = getLotControlJpaController().findLotcontrolByBarcode(workSheetDetails.getBarcode()).get(0);
            currentdetails = currentLotControl.getDetailsIdDetail();
            currentModelPool = currentLotControl.getModelPoolId();
            currentRefu01 = getRefu01JpaController().findDataRefu01List(currentdetails).get(0);

            List<ModelPool> modelpoollis = getModelPoolJpaController().findModelPoolBySeries(currentdetails.getModelseries());
            for (ModelPool modelpool : modelpoollis) {
                getModelpoollist().add(new SelectItem(modelpool.getId(), modelpool.getModelName()));
                print("= modelpool : " + modelpool.getModelName());
            }
            List<Md2pc> md2pcs = getMd2pcJpaController().findBymodelPoolId(currentModelPool);
            for (Md2pc md2pc : md2pcs) {
                getDetailprocess().add(new detailProcess(md2pc.getProcessPoolIdProc().getCodeOperation(), null, null, null, null, "", md2pc.getProcessPoolIdProc().getProcName(), "", 0, 0, 0, "", ""));
            }

            String link = "pages/moduleBackLine/newcreate/editview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            print("Go to Edit view.");
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changTodelAll(ActionEvent event) {
        print("====================== Del All ==================================");
        try {
            lotControllist = getLotControlJpaController().findByDetailsId(currentdetails);
            currentdetails.setDeleted(1);
            getDetailsJpaController().edit(currentdetails);
            for (LotControl lotcontrol : lotControllist) {
                lotcontrol.setDeleted(1);
                getLotControlJpaController().edit(lotcontrol);
                print("= Deleted : Lot " + lotcontrol.getId());
            }
            reset();
            String link = "pages/moduleBackLine/newcreate/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            print("= Deleted Complete.");
            searchNewcreateWorksheet();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changToDelOne(ActionEvent event) {
        print("================== Del select One. ==============================");
        try {
            workSheetDetail workSheetDetails = (workSheetDetail) getWorksheet().getRowData();
            currentLotControl = getLotControlJpaController().findLotcontrolByBarcode(workSheetDetails.getBarcode()).get(0);
            currentLotControl.setDeleted(1);
            getLotControlJpaController().edit(currentLotControl);
            print("= Del completed.");
            searchNewcreateWorksheet();
        } catch (NonexistentEntityException nex) {
            print("Error : " + nex);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void previous() {
        getWorksheetpagination().previousPage();
        setWorksheet();
    }

    public void next() {
        getWorksheetpagination().nextPage();
        setWorksheet();
    }

    public int getAllPages() {
        int all = getWorksheetpagination().getItemsCount();
        int pagesize = getWorksheetpagination().getPageSize();
        return ((int) all / pagesize) + 1;
    }

    private void loadWorkSheet() {
        worksheetlist = null;
        print("=================================================================");
        detailRefulist = getRefu01JpaController().findRefu01Entities();
        if (detailRefulist.isEmpty()) {
            getWorksheetList();
        }
        for (Refu01 refu : detailRefulist) {
            currentdetails = refu.getDetailIdDetail();
            lotControllist = getLotControlJpaController().findByDetailsId(currentdetails);
            for (LotControl lotControl : lotControllist) {
                try {
                    currentModelPool = lotControl.getModelPoolId();
                    if (lotControl.getMainDataId() != null) {
                        print("T");
                        MainData mainData = getMainDataJpaController().findMainData(lotControl.getMainDataId());
                        ProcessPool processPool = mainData.getIdProc();
                        CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(mainData.getCurrentNumber());
                        getWorksheetList().add(new workSheetDetail(currentdetails, lotControl.getBarcode(), lotControl.getBarcode(), currentModelPool.getModelName(), currentModelPool.getModelSeries(), currentdetails.getDateCreate(), processPool.getProcName(), currentProcess.getStatus()));
                        print("= Detail : " + currentdetails.getDetailName() + " :: Barcode : " + lotControl.getBarcode());
                    } else {
                        getWorksheetList().add(new workSheetDetail(currentdetails, lotControl.getBarcode(), lotControl.getBarcode(), currentModelPool.getModelName(), currentModelPool.getModelSeries(), currentdetails.getDateCreate(), "", ""));
                        print("Null");
                    }
                } catch (Exception e) {
                    print("= Error : " + e);
                }
            }
        }
        setWorksheet();
    }
    private CurrentProcessJpaController currentProcessJpaController;

    private CurrentProcessJpaController getCurrentProcessJpaController() {
        if (currentProcessJpaController == null) {
            currentProcessJpaController = new CurrentProcessJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return currentProcessJpaController;
    }
    private MainDataJpaController mainDataJpaController;

    private MainDataJpaController getMainDataJpaController() {
        if (mainDataJpaController == null) {
            mainDataJpaController = new MainDataJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mainDataJpaController;
    }

    public void create(ActionEvent event) {
        print("=================== Create ======================================");
        printassyworksheet.barcode = null;
        printassyworksheet.detail = null;
        try {
            if (!getCurrentdetails().getLotcontrol().isEmpty() && getCurrentdetails().getLotcontrol() != null) {
                int lot = Integer.parseInt(getCurrentdetails().getLotcontrol());
                getCurrentdetails().setIdDetail(UUID.randomUUID().toString());
                getCurrentdetails().setDateCreate(new Date());
                getCurrentdetails().setDeleted(0);
                getCurrentdetails().setDetailName("refu01");
                getCurrentdetails().setDetailType("newcreate");
                getCurrentdetails().setNumOnmonth(0);
                Users user = userLogin.getSessionUser();
                getCurrentdetails().setUserCreate(user.getId());
//              ============ Details ===========================================
                getCurrentRefu01().setId(UUID.randomUUID().toString());
                getCurrentRefu01().setDetailIdDetail(currentdetails);

                getDetailsJpaController().create(currentdetails);
                getRefu01JpaController().create(currentRefu01);

                String partfile = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/images/barcode/");
                print(" PartFile : " + partfile);

                for (int i = 0; i < lot; i++) {
                    int maxnum = getBarCodeGenerter().getMaxnumberOfLotcontrol() + 1;
                    String barcode = "NC" + currentModelPool.getModelName() + "-" + new SimpleDateFormat("MMyy").format(new Date()) + currentModelPool.getModelSeries() + maxnum;
                    try {
                        getCurrentLotControl().setId(UUID.randomUUID().toString());
                        getCurrentLotControl().setBarcode(barcode);
                        getCurrentLotControl().setDeleted(0);
                        getCurrentLotControl().setDetailsIdDetail(currentdetails);
                        getCurrentLotControl().setLotNumber((1+i) + "");
                        getCurrentLotControl().setModelPoolId(currentModelPool);
                        getCurrentLotControl().setQtyOutput(0);
                        getCurrentLotControl().setWipQty(0);
                        if (genBarCode(barcode, partfile)) {
                            getLotControlJpaController().create(currentLotControl);
                            getBarcodelist().add(new blBarcode(getCurrentLotControl().getId(), barcode, "./images/barcode/", true));
                            print("= Barcode : " + barcode);
                            getBarCodeGenerter().updateMaxnumberOfLotcontrol(maxnum);
                        }
                    } catch (Exception e) {
                        print("= Create Lotcontrol Error : " + e);
                    }
                }
            }
            printassyworksheet.detail = currentdetails;
            Thread.sleep(500);
            String link = "pages/moduleBackLine/newcreate/printview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (Exception e) {
            print("= Create Error : " + e);
        }
    }

    private List<blBarcode> getBarcodelist() {
        if (printassyworksheet.barcode == null) {
            printassyworksheet.barcode = new ArrayList<blBarcode>();
        }
        return printassyworksheet.barcode;
    }

    private boolean genBarCode(String code, String filePath) {
        print("===================== genbarcode file ===========================");
        try {
            getBarCodeGenerter().createBarcode(code, filePath + "/1d/");
            getBarCodeGenerter().createQrCode(code, filePath + "/2d/");
            print("= GenCode : Complete");
            return true;
        } catch (Exception e) {
            print("= Error can not genbarcode." + e.toString());
            return false;
        }
    }
    private BarCodeGenerter barCodeGenerter;

    private BarCodeGenerter getBarCodeGenerter() {
        if (barCodeGenerter == null) {
            barCodeGenerter = new BarCodeGenerter();
        }
        return barCodeGenerter;
    }
    private LotControlJpaController lotControlJpaController;

    private LotControlJpaController getLotControlJpaController() {
        if (lotControlJpaController == null) {
            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotControlJpaController;
    }
    private DetailsJpaController detailsJpaController;

    private DetailsJpaController getDetailsJpaController() {
        if (detailsJpaController == null) {
            detailsJpaController = new DetailsJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailsJpaController;
    }
    private Refu01JpaController refu01JpaController;

    private Refu01JpaController getRefu01JpaController() {
        if (refu01JpaController == null) {
            refu01JpaController = new Refu01JpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return refu01JpaController;
    }

    private void reset() {
        currentLotControl = null;
        currentModelPool = null;
        currentRefu01 = null;
        currentdetails = null;
        detailprocess = null;
        detailRefulist = null;
    }

    public void changToCancel(ActionEvent event) {
        print("============= Cancel ============================================");
        try {
            reset();
            String link = "pages/moduleBackLine/newcreate/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            searchNewcreateWorksheet();
        } catch (Exception e) {
            print("= Cancel Error : " + e);
        }
    }

    public List<SelectItem> getModelserieslist() {
        print("================= getModelserieslist ============================");
        try {
            modelserieslist = getModelPoolJpaController().findModelPoolGroupBySeries();
            List<SelectItem> seriesList = new ArrayList<SelectItem>();
            for (String series : modelserieslist) {
                print("= Series : " + series);
                seriesList.add(new SelectItem(series, series));
            }
            return seriesList;
        } catch (Exception e) {
            print("= Error : " + e);
            return null;
        }
    }

    /*
     * ChangvalueListener function ---------------------------------------------
     */
    public void changToInsert(ActionEvent event) {
        print("================= Inser view ====================================");
        try {
            reset();
            String link = "pages/moduleBackLine/newcreate/createview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (Exception e) {
            print("= Insert Error : " + e);
        }
    }

    public void changModelseriesValue(ValueChangeEvent event) {
        print("================== Series chang value ===========================");
        modelpoollist = null;
        detailprocess = null;
        try {
            String series = event.getNewValue().toString();
            print("= series : " + series);
            modelPoolslist = getModelPoolJpaController().findModelPoolBySeries(series);
            print("=== Size : " + modelPoolslist.size());
//            getCurrentdetails().setModelseries(series);
            for (ModelPool modelPool : modelPoolslist) {
                print("= Model : " + modelPool.getModelName());
                getModelpoollist().add(new SelectItem(modelPool.getId(), modelPool.getModelName()));
                print("Size : " + getModelpoollist().size());
            }
//            checkRequer();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changModelValue(ValueChangeEvent event) {
        print("================== Model chang value ============================");
        try {
            String modelId = event.getNewValue().toString();
            print(" ModelId : " + modelId);
            detailprocess = null;
            currentModelPool = getModelPoolJpaController().findModelPool(modelId);
            print("Modelpool : " + currentModelPool);
            getCurrentdetails().setModel(modelId);
            List<Md2pc> md2pcs = getMd2pcJpaController().findBymodelPoolId(currentModelPool);
//            checkRequer();
            for (Md2pc md2pc : md2pcs) {
                getDetailprocess().add(new detailProcess(md2pc.getProcessPoolIdProc().getCodeOperation(), null, null, null, null, "", md2pc.getProcessPoolIdProc().getProcName(), "", 0, 0, 0, "", ""));
            }
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    /*
     * End ChangvalueListener function -----------------------------------------
     */
    public List<SelectItem> getTypelist() {
        if (typelist == null) {
            typelist = new ArrayList<SelectItem>();
            typelist.add(new SelectItem("refu01", "REFU01"));
            typelist.add(new SelectItem("refu02", "REFU02"));
            typelist.add(new SelectItem("refu03", "REFU03"));
            typelist.add(new SelectItem("pcb01", "PCB01"));
            typelist.add(new SelectItem("pcb02", "PCB02"));
            typelist.add(new SelectItem("pcb03", "PCB03"));
        }
        return typelist;
    }

    public List<detailProcess> getDetailprocess() {
        if (detailprocess == null) {
            detailprocess = new ArrayList<detailProcess>();
        }
        return detailprocess;
    }

    private void print(String str) {
        System.out.println(str);
    }
    private Md2pcJpaController md2pcJpaController;

    private Md2pcJpaController getMd2pcJpaController() {
        if (md2pcJpaController == null) {
            md2pcJpaController = new Md2pcJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return md2pcJpaController;
    }
    private ModelPoolJpaController modelPoolJpaController;

    private ModelPoolJpaController getModelPoolJpaController() {
        if (modelPoolJpaController == null) {
            modelPoolJpaController = new ModelPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return modelPoolJpaController;
    }

    public ModelPool getCurrentModelPool() {
        return currentModelPool;
    }

    public void setCurrentModelPool(ModelPool currentModelPool) {
        this.currentModelPool = currentModelPool;
    }

    public Details getCurrentdetails() {
        if (currentdetails == null) {
            currentdetails = new Details();
        }
        return currentdetails;
    }

    public void setCurrentdetails(Details currentdetails) {
        this.currentdetails = currentdetails;
    }

    public List<SelectItem> getModelpoollist() {
        if (modelpoollist == null) {
            modelpoollist = new ArrayList<SelectItem>();
        }
        return modelpoollist;
    }

    public void setModelpoollist(List<SelectItem> modelpoollist) {
        this.modelpoollist = modelpoollist;
    }

    public Refu01 getCurrentRefu01() {
        if (currentRefu01 == null) {
            currentRefu01 = new Refu01();
        }
        return currentRefu01;
    }

    public void setCurrentRefu01(Refu01 currentRefu01) {
        this.currentRefu01 = currentRefu01;
    }

    public LotControl getCurrentLotControl() {
        if (currentLotControl == null) {
            currentLotControl = new LotControl();
        }
        return currentLotControl;
    }

    public void setCurrentLotControl(LotControl currentLotControl) {
        this.currentLotControl = currentLotControl;
    }

    public String getBarcodeSearch() {
        return barcodeSearch;
    }

    public void setBarcodeSearch(String barcodeSearch) {
        this.barcodeSearch = barcodeSearch;
    }

    public String getDateSearch() {
        return dateSearch;
    }

    public void setDateSearch(String dateSearch) {
        this.dateSearch = dateSearch;
    }

    public String getModelSearch() {
        return modelSearch;
    }

    public void setModelSearch(String modelSearch) {
        this.modelSearch = modelSearch;
    }

    public String getSeriesSearch() {
        return seriesSearch;
    }

    public void setSeriesSearch(String seriesSearch) {
        this.seriesSearch = seriesSearch;
    }

    public String getStampSearch() {
        return stampSearch;
    }

    public void setStampSearch(String stampSearch) {
        this.stampSearch = stampSearch;
    }

    public DataModel<workSheetDetail> getWorksheet() {
        return worksheet;
    }

    private void setWorksheet() {
        worksheet = getWorksheetpagination().createPageDataModel();
    }
    private PaginationHelper worksheetpagination;

    public PaginationHelper getWorksheetpagination() {
        if (worksheetpagination == null) {
            worksheetpagination = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    return getWorksheetList().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    print("PageFirst : " + getPageFirstItem());
                    print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (getWorksheetList().size() > 0) {
                        listmodel = new ListDataModel(getWorksheetList().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(getWorksheetList());
                    }
                    return listmodel;
                }
            };
        }
        return worksheetpagination;
    }

    private List<workSheetDetail> getWorksheetList() {
        if (worksheetlist == null) {
            worksheetlist = new ArrayList<workSheetDetail>();
        }
        return worksheetlist;
    }

    public String getProcessSearch() {
        return processSearch;
    }

    public void setProcessSearch(String processSearch) {
        this.processSearch = processSearch;
    }
}
