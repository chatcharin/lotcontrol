/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moduleBackLine.bean.assembly;

import com.appCinfigpage.bean.userLogin;
import com.moduleBackLine.Dataworksheet.blBarcode;
import com.moduleBackLine.Dataworksheet.workSheetDetail;
import com.moduleBackLine.bean.printassyworksheet;
import com.moduleFontline.DataWorksheet.detailProcess;
import com.tct.barcodegenerate.BarCodeGenerter;
import com.tct.data.*;
import com.tct.data.jpa.*;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jsf.util.PaginationHelper;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;

/**
 *
 * @author The_Boy_Cs
 */
public class assemblyControl {
//======= Search =====================================

    private String modelsearch;
    private String barcodesearch;
    private String seriessearch;
    private String processsearch;
//====================================================
    private DataModel worksheet;
    private List<workSheetDetail> worksheetlist;
    private List<LineAssembly> detailAssemblys;
    private Details currentdetails;
    private List<LotControl> lotControllist;
    private ModelPool currentModelPool;
    private List<ModelPool> modelPoolslist;
    private List<String> modelPools;
    private LotControl currentlotControl = null;
    private LineAssembly currentdetailassembly;
    private List<SelectItem> modelpoollist;
    private List<detailProcess> detailprocess;
    private List<SelectItem> productcodename = null;
    private List<SelectItem> producttype = null;
    // new combobox
    private List<SelectItem> inklist = null;
    private List<SelectItem> solderlist = null;
    private List<SelectItem> potcorelist = null;
    private List<SelectItem> bondlist = null;
    // end----------------

    public assemblyControl() {
        searchAssemblyWorksheet();
    }

    public void create(ActionEvent event) {
        print("==================== Create =====================================");
        printassyworksheet.barcode = null;
        printassyworksheet.detail = null;
        currentlotControl = null;
        try {
            Users user = userLogin.getSessionUser();
            getCurrentdetails().setIdDetail(UUID.randomUUID().toString());
            getCurrentdetails().setDateCreate(new Date());
            getCurrentdetails().setDeleted(0);
            getCurrentdetails().setDetailName("line_assembly");
            getCurrentdetails().setDetailType("backline");
            getCurrentdetails().setUserCreate(user.getId());
//            getCurrentdetails().set

            getCurrentdetailassembly().setDetailIdDetail(getCurrentdetails());
            getCurrentdetailassembly().setId(UUID.randomUUID().toString());
            getCurrentdetailassembly().setDetailproductreport("");

            getDetailsJpaController().create(getCurrentdetails());
            getLineAssemblyJpaController().create(getCurrentdetailassembly());

            int lot = Integer.parseInt(getCurrentdetails().getLotcontrol());
            String partfile = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/images/barcode/");
            print(" PartFile : " + partfile);

            for (int i = 0; i < lot; i++) {
                int maxnum = getBarcodeGenerater().getMaxnumberOfLotcontrol() + 1;
                String barcode = "BSB" + getCurrentdetailassembly().getPdCode() + getCurrentdetailassembly().getPdType() + "-"
                        + new SimpleDateFormat("MMyy").format(new Date()) + currentModelPool.getModelSeries() + maxnum;

                getCurrentlotControl().setId(UUID.randomUUID().toString());
                getCurrentlotControl().setBarcode(barcode);
                getCurrentlotControl().setDeleted(0);
                getCurrentlotControl().setDetailsIdDetail(getCurrentdetails());
                getCurrentlotControl().setLotNumber((1+i) + "");
                getCurrentlotControl().setModelPoolId(getCurrentModelPool());

                if (genBarCode(barcode, partfile)) {
                    getLotControlJpaController().create(getCurrentlotControl());
                    getBarcodelist().add(new blBarcode(getCurrentlotControl().getId(), barcode, "./images/barcode/", true));
                    print("= Barcode : " + barcode);
                    getBarcodeGenerater().updateMaxnumberOfLotcontrol(maxnum);
                }
                print("= Insert Lotcontrol : " + currentlotControl);
            }
            printassyworksheet.detail = currentdetails;
            Thread.sleep(1000);
            String link = "pages/moduleBackLine/assembly/printview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            searchAssemblyWorksheet();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    private List<blBarcode> getBarcodelist() {
        if (printassyworksheet.barcode == null) {
            printassyworksheet.barcode = new ArrayList<blBarcode>();
        }
        return printassyworksheet.barcode;
    }

    private boolean genBarCode(String code, String filePath) {
        print("===================== genbarcode file ===========================");
        try {
            getBarcodeGenerater().createBarcode(code, filePath + "/1d/");
            getBarcodeGenerater().createQrCode(code, filePath + "/2d/");
            print("= GenCode : Complete");
            return true;
        } catch (Exception e) {
            print("= Error can not genbarcode." + e.toString());
            return false;
        }
    }

    public void changProductTypevalue(ValueChangeEvent event) {
        print("=================== Product type ================================");
        String pdType = event.getNewValue().toString();
        getCurrentdetailassembly().setPdType(pdType);
        print("= PdType : " + getCurrentdetailassembly().getPdType());
    }

    public void changProductcodeName(ValueChangeEvent event) {
        print("================== Productcode Chang ============================");
        String pdcodename = event.getNewValue().toString();
        String pd_code = null;
        try {
            if (pdcodename.equals("Prototype")) {
                pd_code = "P";
            } else if (pdcodename.equals("New product introduction")) {
                pd_code = "N";
            } else if (pdcodename.equals("Special built request")) {
                pd_code = "S";
            } else if (pdcodename.equals("Mass production")) {
                pd_code = "M";
            }
            getCurrentdetailassembly().setPdCodeName(pdcodename);
            getCurrentdetailassembly().setPdCode(pd_code);
        } catch (Exception e) {
            print("= Error : " + e);
        }
        print("= Product name : " + event.getNewValue());
        print("= Product Code : " + pd_code);
    }

    public void changModelseriesValue(ValueChangeEvent event) {
        print("================== Series chang value ===========================");
        modelpoollist = null;
        detailprocess = null;
        try {
            String series = event.getNewValue().toString();
            print("= series : " + series);
            modelPoolslist = getModelPoolJpaController().findModelPoolBySeries(series);
            for (ModelPool modelPool : modelPoolslist) {
                getModelpoollist().add(new SelectItem(modelPool.getId(), modelPool.getModelName()));
            }
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changModelValue(ValueChangeEvent event) {
        print("================== Model chang value ============================");
        String modelId = event.getNewValue().toString();
        print(" ModelId : " + modelId);
        try {
            currentModelPool = getModelPoolJpaController().findModelPool(modelId);
            print("Modelpool : " + currentModelPool);
            getCurrentdetails().setModel(modelId);
            List<Md2pc> md2pcs = getMd2pcJpaController().findBymodelPoolId(currentModelPool);
            for (Md2pc md2pc : md2pcs) {
                getDetailprocess().add(new detailProcess(md2pc.getProcessPoolIdProc().getCodeOperation(), null, null, null, null, "", md2pc.getProcessPoolIdProc().getProcName(), "", 0, 0, 0, "", ""));
            }
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public List<SelectItem> getModelserieslist() {
        print("================= getModelserieslist ============================");
        try {
            modelPools = getModelPoolJpaController().findModelPoolGroupBySeries();
            List<SelectItem> seriesList = new ArrayList<SelectItem>();
            for (String series : modelPools) {
                seriesList.add(new SelectItem(series, series));
            }
            return seriesList;
        } catch (Exception e) {
            print("= Error : " + e);
            return null;
        }
    }

    public Details getCurrentdetails() {
        if (currentdetails == null) {
            currentdetails = new Details();
        }
        return currentdetails;
    }

    public LineAssembly getCurrentdetailassembly() {
        if (currentdetailassembly == null) {
            currentdetailassembly = new LineAssembly();
        }
        return currentdetailassembly;
    }

    public void searchAssemblyWorksheet() {
        print("==================== Search worksheet Assembly ==================");
        try {
            if (barcodesearch != null && !barcodesearch.trim().isEmpty()) {
                print("= Order Number :: " + barcodesearch);
                searchByBarcode();
            } else if (processsearch != null && !processsearch.trim().isEmpty()) {
                print("= Process :: " + processsearch);
                searchByProcess();
            } else if (modelsearch != null && !modelsearch.trim().isEmpty()) {
                print("= Type Model :: " + modelsearch);
                searchByModel();
            } else {
                loadWorkSheet();
            }
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    private void searchByModel() {
        worksheetlist = null;
        print("===================== Search By Model ===========================");
//        List<Details> detailslist = getDetailsJpaController().findDetailsByModelAndDetailNameAuto(modelSearch);
        List<ModelPool> modelPoolslist = getModelPoolJpaController().findByModelname(modelsearch);

        if (modelPoolslist.isEmpty()) {
            getWorksheetList();
        }
        for (ModelPool modelpool : modelPoolslist) {
            lotControllist = (List<LotControl>) modelpool.getLotControlCollection();
            for (LotControl lotControl : lotControllist) {
                try {
                    if (lotControl.getDeleted() == 0) {
                        currentdetails = lotControl.getDetailsIdDetail();
                        if (currentdetails.getDetailName().equals("line_assembly") && currentdetails.getDeleted() == 0) {
                            if (lotControl.getMainDataId() != null) {
                                print("T");
                                MainData mainData = getMainDataJpaController().findMainData(lotControl.getMainDataId());
                                ProcessPool processPool = mainData.getIdProc();
                                CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(mainData.getCurrentNumber());
                                getWorksheetList().add(new workSheetDetail(currentdetails, lotControl.getBarcode(), lotControl.getBarcode(), lotControl.getModelPoolId().getModelName(), lotControl.getModelPoolId().getModelSeries(), currentdetails.getDateCreate(), processPool.getProcName(), currentProcess.getStatus()));
                                print("= Detail : " + currentdetails.getDetailName() + " :: Barcode : " + lotControl.getBarcode());
                            } else {
                                getWorksheetList().add(new workSheetDetail(currentdetails, lotControl.getBarcode(), lotControl.getBarcode(), lotControl.getModelPoolId().getModelName(), lotControl.getModelPoolId().getModelSeries(), currentdetails.getDateCreate(), null, null));
                                print("Null");
                            }
                        }
                    }
                } catch (Exception e) {
                    print("= Error : " + e);
                }
            }
        }
        setWorksheet();
    }

    private void searchByProcess() {
        worksheetlist = null;
        print("=================================================================");
        List<ProcessPool> pcpools = getProcessPoolJpaController().findProcessPoolByName(processsearch);
        if (pcpools.isEmpty()) {
            getWorksheetList();
        }
        for (ProcessPool pcpool : pcpools) {
            List<MainData> maindatas = getMainDataJpaController().findMainDataByProcess(pcpool);
            for (MainData maindata : maindatas) {
                currentlotControl = maindata.getLotControlId();
                currentdetails = currentlotControl.getDetailsIdDetail();
                if (currentdetails.getDetailName().equals("line_assembly")) {
                    CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(maindata.getCurrentNumber());
                    getWorksheetList().add(new workSheetDetail(currentdetails, currentlotControl.getBarcode(), currentlotControl.getBarcode(), currentlotControl.getModelPoolId().getModelName(), currentlotControl.getModelPoolId().getModelSeries(), currentdetails.getDateCreate(), pcpool.getProcName(), currentProcess.getStatus()));
                    print("= Detail : " + currentdetails.getDetailName() + " :: Barcode : " + maindata.getLotControlId().getBarcode());
                }
            }
        }
        setWorksheet();
    }

    private void searchByBarcode() {
        worksheetlist = null;
        print("================ search by barcode ==============================");
        lotControllist = getLotControlJpaController().findLotcontrolByBarcode(barcodesearch);
        if (lotControllist.isEmpty()) {
            getWorksheetList();
        }
        for (LotControl lotcontrol : lotControllist) {
            currentdetails = lotcontrol.getDetailsIdDetail();
            if (currentdetails.getDetailName().equals("line_assembly")) {
                print("= Maindata id :: " + lotcontrol.getMainDataId());
                if (lotcontrol.getMainDataId() != null && !lotcontrol.getMainDataId().trim().isEmpty()) {
                    MainData maindata = getMainDataJpaController().findMainData(lotcontrol.getMainDataId());
                    ProcessPool pcpool = maindata.getIdProc();
                    CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(maindata.getCurrentNumber());
                    getWorksheetList().add(new workSheetDetail(currentdetails, lotcontrol.getBarcode(), lotcontrol.getBarcode(), lotcontrol.getModelPoolId().getModelName(), lotcontrol.getModelPoolId().getModelSeries(), currentdetails.getDateCreate(), pcpool.getProcName(), currentProcess.getStatus()));
                    print("= Detail : " + currentdetails.getDetailName() + " :: Barcode : " + maindata.getLotControlId().getBarcode());
                } else {
                    getWorksheetList().add(new workSheetDetail(currentdetails, lotcontrol.getBarcode(), lotcontrol.getBarcode(), lotcontrol.getModelPoolId().getModelName(), lotcontrol.getModelPoolId().getModelSeries(), currentdetails.getDateCreate(), "", ""));
                    print("= Detail : " + currentdetails.getDetailName() + " :: Barcode : " + lotcontrol.getBarcode());
                }
            }
        }
        setWorksheet();
    }

    public void changToSave(ActionEvent event) {
        print("=================== Save ========================================");
        try {
            getDetailsJpaController().edit(currentdetails);
            getLineAssemblyJpaController().edit(currentdetailassembly);
            String link = "pages/moduleBackLine/assembly/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (Exception e) {
            print("= SaveError : " + e);
        }
    }

    public void changTocancel(ActionEvent event) {
        print("====================== Cancel ===================================");
        currentModelPool = null;
        currentdetailassembly = null;
        currentdetails = null;
        currentlotControl = null;
        modelpoollist = null;
        detailprocess = null;
        String link = "pages/moduleBackLine/assembly/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
    }

    public void changToInsert(ActionEvent event) {
        print("=============== Insert ==========================================");
        currentModelPool = null;
        currentdetailassembly = null;
        currentdetails = null;
        currentlotControl = null;
        detailprocess = null;
        String link = "pages/moduleBackLine/assembly/createview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
    }

    public void changTodelAll(ActionEvent event) {
        print("====================== Del All ==================================");
        try {
            lotControllist = getLotControlJpaController().findByDetailsId(currentdetails);
            currentdetails.setDeleted(1);
            getDetailsJpaController().edit(currentdetails);
            currentdetailassembly.setDeleted(1);
            getLineAssemblyJpaController().edit(currentdetailassembly);
            for (LotControl lotcontrol : lotControllist) {
                lotcontrol.setDeleted(1);
                getLotControlJpaController().edit(lotcontrol);
                print("= Deleted : Lot " + lotcontrol.getId());
            }
            String link = "pages/moduleBackLine/assembly/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            print("= Deleted Complete.");
            currentdetailassembly = null;
            currentdetails = null;
            lotControllist = null;
            searchAssemblyWorksheet();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changToDelOne(ActionEvent event) {
        print("============= Del One selected ==================================");
        try {
            workSheetDetail workSheetDetails = (workSheetDetail) getWorksheet().getRowData();
            currentlotControl = getLotControlJpaController().findLotcontrolByBarcode(workSheetDetails.getBarcode()).get(0);
            currentlotControl.setDeleted(1);
            getLotControlJpaController().edit(currentlotControl);
            print("= Del completed.");
            currentlotControl = null;
            searchAssemblyWorksheet();
        } catch (NonexistentEntityException nex) {
            print("Error : " + nex);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changToEdit(ActionEvent event) {
        print("=================== Edit ========================================");
        try {
            modelpoollist = null;
            detailprocess = null;
            workSheetDetail workSheetDetails = (workSheetDetail) getWorksheet().getRowData();
            currentlotControl = getLotControlJpaController().findLotcontrolByBarcode(workSheetDetails.getBarcode()).get(0);
            currentdetails = currentlotControl.getDetailsIdDetail();
            currentModelPool = currentlotControl.getModelPoolId();
            currentdetailassembly = getLineAssemblyJpaController().findLineAssemblyList(currentdetails).get(0);
            List<ModelPool> modelpools = getModelPoolJpaController().findModelPoolBySeries(currentdetails.getModelseries());
            for (ModelPool modelpool : modelpools) {
                getModelpoollist().add(new SelectItem(modelpool.getId(), modelpool.getModelName()));
                print("= modelpool : " + modelpool.getModelName());
            }
            List<Md2pc> md2pcs = getMd2pcJpaController().findBymodelPoolId(currentModelPool);
            for (Md2pc md2pc : md2pcs) {
                getDetailprocess().add(new detailProcess(md2pc.getProcessPoolIdProc().getCodeOperation(), null, null, null, null, "", md2pc.getProcessPoolIdProc().getProcName(), "", 0, 0, 0, "", ""));
            }
            String link = "pages/moduleBackLine/assembly/editview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            print("= GotoEdit : " + link);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changToPrint(ActionEvent event) {
        print("==================== Print one ==================================");
        try {
            modelpoollist = null;
            detailprocess = null;
            printassyworksheet.barcode = null;
            printassyworksheet.detail = null;
            workSheetDetail workSheetDetails = (workSheetDetail) getWorksheet().getRowData();
            currentlotControl = getLotControlJpaController().findLotcontrolByBarcode(workSheetDetails.getBarcode()).get(0);
            currentdetails = currentlotControl.getDetailsIdDetail();
            currentModelPool = currentlotControl.getModelPoolId();
            currentdetailassembly = getLineAssemblyJpaController().findLineAssemblyList(currentdetails).get(0);
            getBarcodelist().add(new blBarcode(currentlotControl.getId(), currentlotControl.getBarcode(), "./images/barcode/", true));
            printassyworksheet.detail = currentdetails;
            String link = "pages/moduleBackLine/assembly/printview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            print("= Printview. : Link : " + link);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void previous() {
        getWorksheetpagination().previousPage();
        setWorksheet();
    }

    public void next() {
        getWorksheetpagination().nextPage();
        setWorksheet();
    }

    public int getAllPages() {
        int all = getWorksheetpagination().getItemsCount();
        int pagesize = getWorksheetpagination().getPageSize();
        return ((int) all / pagesize) + 1;
    }

    private void loadWorkSheet() {
        worksheetlist = null;
        print("=================================================================");
        detailAssemblys = getLineAssemblyJpaController().findLineAssemblysAll();
        if (detailAssemblys.isEmpty()) {
            getWorksheetList();
        }
        for (LineAssembly detailaAssembly : detailAssemblys) {
            currentdetails = detailaAssembly.getDetailIdDetail();
            if (currentdetails.getDetailName().equals("line_assembly")) {
                lotControllist = getLotControlJpaController().findByDetailsId(currentdetails);
                for (LotControl lotControl : lotControllist) {
                    try {
                        currentModelPool = lotControl.getModelPoolId();
                        if (lotControl.getMainDataId() != null) {
                            print("T");

                            MainData mainData = getMainDataJpaController().findMainData(lotControl.getMainDataId());
                            ProcessPool processPool = mainData.getIdProc();
                            CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(mainData.getCurrentNumber());

                            getWorksheetList().add(new workSheetDetail(currentdetails, lotControl.getBarcode(), lotControl.getBarcode(), currentModelPool.getModelName(), currentModelPool.getModelSeries(), currentdetails.getDateCreate(), processPool.getProcName(), currentProcess.getStatus()));
                            print("= Detail : " + currentdetails.getDetailName() + " :: Barcode : " + lotControl.getBarcode());
                        } else {
                            getWorksheetList().add(new workSheetDetail(currentdetails, lotControl.getBarcode(), lotControl.getBarcode(), currentModelPool.getModelName(), currentModelPool.getModelSeries(), currentdetails.getDateCreate(), "", ""));
                            print("Null");
                        }
                    } catch (Exception e) {
                        print("= Error : " + e);
                    }

                }
            }
        }
//        currentdetails = null;
//        lotControllist = null;
//        currentModelPool = null;
        setWorksheet();
    }
    private DetailsJpaController detailsJpaController;

    private DetailsJpaController getDetailsJpaController() {
        if (detailsJpaController == null) {
            detailsJpaController = new DetailsJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailsJpaController;
    }
    private Md2pcJpaController md2pcJpaController = null;

    private Md2pcJpaController getMd2pcJpaController() {
        if (md2pcJpaController == null) {
            md2pcJpaController = new Md2pcJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return md2pcJpaController;
    }
    private ModelPoolJpaController modelPoolJpaController;

    private ModelPoolJpaController getModelPoolJpaController() {
        if (modelPoolJpaController == null) {
            modelPoolJpaController = new ModelPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return modelPoolJpaController;
    }
    private ProcessPoolJpaController processPoolJpaController;

    private ProcessPoolJpaController getProcessPoolJpaController() {
        if (processPoolJpaController == null) {
            processPoolJpaController = new ProcessPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return processPoolJpaController;
    }
    private CurrentProcessJpaController currentProcessJpaController;

    private CurrentProcessJpaController getCurrentProcessJpaController() {
        if (currentProcessJpaController == null) {
            currentProcessJpaController = new CurrentProcessJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return currentProcessJpaController;
    }
    private MainDataJpaController mainDataJpaController;

    private MainDataJpaController getMainDataJpaController() {
        if (mainDataJpaController == null) {
            mainDataJpaController = new MainDataJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mainDataJpaController;
    }
    private LotControlJpaController lotControlJpaController;

    private LotControlJpaController getLotControlJpaController() {
        if (lotControlJpaController == null) {
            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotControlJpaController;
    }
    private LineAssemblyJpaController lineAssemblyJpaController;

    private LineAssemblyJpaController getLineAssemblyJpaController() {
        if (lineAssemblyJpaController == null) {
            lineAssemblyJpaController = new LineAssemblyJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lineAssemblyJpaController;
    }

    private List<workSheetDetail> getWorksheetList() {
        if (worksheetlist == null) {
            worksheetlist = new ArrayList<workSheetDetail>();
        }
        return worksheetlist;
    }
    private PaginationHelper worksheetpagination;

    public PaginationHelper getWorksheetpagination() {
        if (worksheetpagination == null) {
            worksheetpagination = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    return getWorksheetList().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    print("PageFirst : " + getPageFirstItem());
                    print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (getWorksheetList().size() > 0) {
                        listmodel = new ListDataModel(getWorksheetList().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(getWorksheetList());
                    }
                    return listmodel;
                }
            };
        }
        return worksheetpagination;
    }

    public DataModel getWorksheet() {
        return worksheet;
    }

    public void setWorksheet() {
        this.worksheet = getWorksheetpagination().createPageDataModel();
    }

    public String getBarcodesearch() {
        return barcodesearch;
    }

    public void setBarcodesearch(String barcodesearch) {
        this.barcodesearch = barcodesearch;
    }

    public String getModelsearch() {
        return modelsearch;
    }

    public void setModelsearch(String modelsearch) {
        this.modelsearch = modelsearch;
    }

    public String getProcesssearch() {
        return processsearch;
    }

    public void setProcesssearch(String processsearch) {
        this.processsearch = processsearch;
    }

    public String getSeriessearch() {
        return seriessearch;
    }

    public void setSeriessearch(String seriessearch) {
        this.seriessearch = seriessearch;
    }

    private void print(String str) {
        System.out.println(str);
    }

    public List<SelectItem> getModelpoollist() {
        if (modelpoollist == null) {
            modelpoollist = new ArrayList<SelectItem>();
        }
        return modelpoollist;
    }

    public List<detailProcess> getDetailprocess() {
        if (detailprocess == null) {
            detailprocess = new ArrayList<detailProcess>();
        }
        return detailprocess;
    }

    public void setDetailprocess(List<detailProcess> detailprocess) {
        this.detailprocess = detailprocess;
    }

    public List<SelectItem> getProductcodename() {
        if (productcodename == null) {
            productcodename = new ArrayList<SelectItem>();
            productcodename.add(new SelectItem("Prototype", "Prototype"));
            productcodename.add(new SelectItem("New product introduction", "New product introduction"));
            productcodename.add(new SelectItem("Special built request", "Special built request"));
            productcodename.add(new SelectItem("Mass production", "Mass production"));
        }
        return productcodename;
    }

    public List<SelectItem> getProducttype() {
        if (producttype == null) {
            producttype = new ArrayList<SelectItem>();
            producttype.add(new SelectItem("T", "Trigger coil"));
            producttype.add(new SelectItem("TR", "Tranformer"));
            producttype.add(new SelectItem("PCB", "Print circuit board"));
        }
        return producttype;
    }

    public LotControl getCurrentlotControl() {
        if (currentlotControl == null) {
            currentlotControl = new LotControl();
        }
        return currentlotControl;
    }

    public void setCurrentlotControl(LotControl currentlotControl) {
        this.currentlotControl = currentlotControl;
    }
    private BarCodeGenerter barCodeGenerter;

    private BarCodeGenerter getBarcodeGenerater() {
        if (barCodeGenerter == null) {
            barCodeGenerter = new BarCodeGenerter();
        }
        return barCodeGenerter;
    }

    public ModelPool getCurrentModelPool() {
        return currentModelPool;
    }

    public void setCurrentModelPool(ModelPool currentModelPool) {
        this.currentModelPool = currentModelPool;
    }

    public List<SelectItem> getBondlist() {
        if(bondlist == null){
            bondlist = new ArrayList<SelectItem>();
            bondlist.add(new SelectItem("ER-E-1350Z", "ER-E-1350Z"));
            bondlist.add(new SelectItem("ER-E-5270F", "ER-E-5270F"));
            bondlist.add(new SelectItem("ER-ME-5015", "ER-ME-5015"));
            bondlist.add(new SelectItem("THREEBOND 2206S", "THREEBOND 2206S"));
        }
        return bondlist;
    }

    public void setBondlist(List<SelectItem> bondlist) {
        this.bondlist = bondlist;
    }

    public List<SelectItem> getInklist() {
        if(inklist == null){
            inklist = new ArrayList<SelectItem>();
            inklist.add(new SelectItem("BONMARQUE C BLACK", "BONMARQUE C BLACK"));
            inklist.add(new SelectItem("BONMARQUE C SILVER", "BONMARQUE C SILVER"));
            inklist.add(new SelectItem("BONMARQUE C WHITE", "BONMARQUE C WHITE"));
        }
        return inklist;
    }

    public void setInklist(List<SelectItem> inklist) {
        this.inklist = inklist;
    }

    public List<SelectItem> getPotcorelist() {
        if(potcorelist == null){
            potcorelist = new ArrayList<SelectItem>();
            potcorelist.add(new SelectItem("C-TTRN-0520H/D/HT-A", "C-TTRN-0520H/D/HT-A"));
            potcorelist.add(new SelectItem("C-TTRN-0520H/P/HT-A", "C-TTRN-0520H/P/HT-A"));
            potcorelist.add(new SelectItem("C-TTRN-0522H/D/HT", "C-TTRN-0522H/D/HT"));
            potcorelist.add(new SelectItem("C-TTRN-0522H/P/HT", "C-TTRN-0522H/P/HT"));
            potcorelist.add(new SelectItem("C-TTRN-0525HA/D/HT-A", "C-TTRN-0525HA/D/HT-A"));
            potcorelist.add(new SelectItem("C-TTRN-0525H/D/HT-A", "C-TTRN-0525H/D/HT-A"));
            potcorelist.add(new SelectItem("C-TTRN-0525H/P/HT-A", "C-TTRN-0525H/P/HT-A"));
            potcorelist.add(new SelectItem("C-TTRN-0525PH/D/HT-A", "C-TTRN-0525PH/D/HT-A"));
            potcorelist.add(new SelectItem("C-TTRN-0525PH/P/HT-A", "C-TTRN-0525PH/P/HT-A"));
            potcorelist.add(new SelectItem("C-TTRN-0530HA/D/HT", "C-TTRN-0530HA/D/HT"));
            potcorelist.add(new SelectItem("C-TTRN-0530H/D/HT", "C-TTRN-0530H/D/HT"));
            potcorelist.add(new SelectItem("C-TTRN-0530H/D/D RD-500/DCE", "C-TTRN-0530H/D/D RD-500/DCE"));
            potcorelist.add(new SelectItem("C-TTRN-0530H/P/HT", "C-TTRN-0530H/P/HT"));
            potcorelist.add(new SelectItem("C-TTRN-0530H/P/RD-500/DCE-A", "C-TTRN-0530H/P/RD-500/DCE-A"));
            potcorelist.add(new SelectItem("C-TTRN-0535H/D/HT", "C-TTRN-0535H/D/HT"));
            potcorelist.add(new SelectItem("C-TTRN-0535H/P/HT", "C-TTRN-0535H/P/HT"));
            potcorelist.add(new SelectItem("C-TTRN-0630HA/D/HT", "C-TTRN-0630HA/D/HT"));
            potcorelist.add(new SelectItem("C-TTRN-0630H/D/HT", "C-TTRN-0630H/D/HT"));
            potcorelist.add(new SelectItem("C-TTRN-0630H/D RD-500/DCE", "C-TTRN-0630H/D RD-500/DCE"));
            potcorelist.add(new SelectItem("C-TTRN-0630H/P/HT", "C-TTRN-0630H/P/HT"));
            potcorelist.add(new SelectItem("C-TTRN-0630H/P/RD-500/DCE-A", "C-TTRN-0630H/P/RD-500/DCE-A"));
            potcorelist.add(new SelectItem("C-TTRN-0635H/D/HT", "C-TTRN-0635H/D/HT"));
            potcorelist.add(new SelectItem("C-TTRN-0635H/P/HT", "C-TTRN-0635H/P/HT"));
        }
        return potcorelist;
    }

    public void setPotcorelist(List<SelectItem> potcorelist) {
        this.potcorelist = potcorelist;
    }

    public List<SelectItem> getSolderlist() {
        if(solderlist == null){
            solderlist = new ArrayList<SelectItem>();
            solderlist.add(new  SelectItem("HD-LFM-62H PB FREE", "HD-LFM-62H PB FREE"));
            solderlist.add(new SelectItem("FLUX FS-ES-1040S", "FLUX FS-ES-1040S"));
        }
        return solderlist;
    }

    public void setSolderlist(List<SelectItem> solderlist) {
        this.solderlist = solderlist;
    }
    
}
