/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moduleBackLine.bean.auto;

import com.appCinfigpage.bean.userLogin;
import com.icesoft.faces.component.ext.ClickActionEvent;
import com.moduleBackLine.Dataworksheet.blBarcode;
import com.moduleBackLine.Dataworksheet.workSheetDetail;
import com.moduleBackLine.bean.printassyworksheet;
import com.moduleBackLine.popup.bean.pastCode;
import com.moduleBackLine.popup.bean.popupAutoassyDetail;
import com.moduleBackLine.popup.bean.popupAutoassySelected;
import com.moduleFontline.DataWorksheet.detailProcess;
import com.tct.barcodegenerate.BarCodeGenerter;
import com.tct.data.*;
import com.tct.data.jpa.*;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jsf.util.PaginationHelper;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;

/**
 *
 * @author The_Boy_Cs
 */
public class autoControl {
//======== For Search ==============================

    private String barcodeSearch;
    private String processSearch;
    private String modelSearch;
    private String seriesSearch;
//==================================================
    private List<detailProcess> detailprocess = null;
    private List<SelectItem> productcodename = null;
    private List<SelectItem> producttype = null;
    private List<SelectItem> modelserieslist = null;
    private List<SelectItem> modellist = null;
    private Details currentdetails = null;
    private DetailAuto currentdetailauto = null;
    private ModelPool currentModelPool;
    private LotControl currentlotControl;
    private boolean popupvisible = false;
    private List<popupAutoassyDetail> popAutoassyDetaillist;//this is popup detail on selected
    private DataModel<popupAutoassyDetail> popupdetailmodel;
    private List<popupAutoassySelected> popupAutoassySelecteds;
    private DataModel<popupAutoassySelected> popupselected;
    private boolean onqtyTextvisible = true;
    private String popuperror = "";
    private String errorLot = "";
    private int onqty = -1;
    private int inqty = 0;
    private List<workSheetDetail> worksheetlist;
    private List<DetailAuto> detailAutolist;
    private List<LotControl> lotControllist;
    private DataModel<workSheetDetail> worksheet;
//    ===== past code and invoice =====
    private List<SelectItem> pastcodelist;
    private String pastcode;
    private String invoice;
    private List<pastCode> pcodelist;
    private DataModel<pastCode> datamodelPcodelist;
    private PaginationHelper pcodepagination;
    private List<CopperWireInvoice> copperWireInvoicelist;
//=====================================

    public autoControl() {
        searchAutoWorksheet();
    }

    public void delPastcodeAndInvoiceFromEditview(ActionEvent event) {
        print("================ del from editview ==============================");
        try {
            pastCode currrentPastCode = datamodelPcodelist.getRowData();
            CopperWireInvoice currentCopperWireInvoicet = currrentPastCode.getCopperWireInvoice();
            print("= Passcode : " + currentCopperWireInvoicet.getPastCode());
            print("= Passcode id : " + currentCopperWireInvoicet.getId());
            getCopperWireInvoiceJpaController().destroy(currentCopperWireInvoicet.getId());
            pcodelist.remove(currrentPastCode);
            setDatamodelPcodelist();
        } catch (Exception e) {
            print(" Error : " + e);
        }
    }

    public void delPastcodeAndInvoice(ActionEvent event) {
        print("================= del new past code =============================");
        try {
            pastCode currrentPastCode = datamodelPcodelist.getRowData();
            pcodelist.remove(currrentPastCode);
            setDatamodelPcodelist();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public PaginationHelper getPcodepagination() {
        if (pcodepagination == null) {
            pcodepagination = new PaginationHelper(getPcodelist().size()) {

                @Override
                public int getItemsCount() {
                    return getPcodelist().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    print("PageFirst : " + getPageFirstItem());
                    print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
//                    if (getPcodelist().size() > 0) {
//                        listmodel = new ListDataModel(getPcodelist().subList(getPageFirstItem(), getPageLastItem() + 1));
//                    } else {
                    listmodel = new ListDataModel(getPcodelist());
//                    }
                    return listmodel;
                }
            };
        }
        return pcodepagination;
    }

    public void addPastcodeAndInvoiceFromEditview(ActionEvent event) {
        print("================ Add from editview ==============================");
        try {
            if ((!invoice.isEmpty() && invoice != null) && (!pastcode.isEmpty() && pastcode != null)) {
                getCurrentcopperWireInvoice().setId(UUID.randomUUID().toString());
                getCurrentcopperWireInvoice().setDetailsIdDetail(currentdetails);
                getCurrentcopperWireInvoice().setInvoiceCopper(invoice);
                getCurrentcopperWireInvoice().setPastCode(pastcode);
                getPcodelist().add(new pastCode(getCurrentcopperWireInvoice(), pastcode, invoice));
                getCopperWireInvoiceJpaController().create(currentcopperWireInvoice);
            }
            pastcode = "";
            invoice = "";
            setDatamodelPcodelist();
        } catch (Exception e) {
            print("Add from edit view Error : " + e);
        }
    }

    public void addPastcodeAndInvoice(ActionEvent event) {
        print("================ Add new past code ==============================");
        try {
            if ((!invoice.isEmpty() && invoice != null) && (!pastcode.isEmpty() && pastcode != null)) {
                getPcodelist().add(new pastCode(getCurrentcopperWireInvoice(), pastcode, invoice));
            }
            setDatamodelPcodelist();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changPastcodeValue(ValueChangeEvent event) {
        print("================== chang to pastcode value ======================");
        try {
            pastcode = event.getNewValue().toString();
            print("= Past code : " + pastcode);
//            getCurrentdetailauto().setPastcode(pastcode);
//            print("= new P : "+getCurrentdetailauto().getPastcode());
        } catch (Exception e) {
            print("= past code chang value Error : " + e);
        }
    }

    public List<SelectItem> getPastcodelist() {
        if (pastcodelist == null) {
            pastcodelist = new ArrayList<SelectItem>();
            pastcodelist.add(new SelectItem("1 PSW-F 0.025", "1 PSW-F 0.025"));
            pastcodelist.add(new SelectItem("1 SPEWN 0.005", "1 SPEWN 0.005"));
            pastcodelist.add(new SelectItem("1 SPEWN 0.025", "1 SPEWN 0.025"));
            pastcodelist.add(new SelectItem("1 SPEWN 0.03", "1 SPEWN 0.03"));
            pastcodelist.add(new SelectItem("1 SPEWN 0.035", "1 SPEWN 0.035"));
            pastcodelist.add(new SelectItem("1 SPEWN 0.05", "1 SPEWN 0.05"));
            pastcodelist.add(new SelectItem("1 SPEWN 0.06", "1 SPEWN 0.06"));
            pastcodelist.add(new SelectItem("1 SPEWN 0.07", "1 SPEWN 0.07"));
            pastcodelist.add(new SelectItem("1 SPEWN 0.09", "1 SPEWN 0.09"));
            pastcodelist.add(new SelectItem("1 SPWF 0.025", "1 SPWF 0.025"));
            pastcodelist.add(new SelectItem("1SPEWN 0.025", "1SPEWN 0.025"));
            pastcodelist.add(new SelectItem("1SPEWN 0.035", "1SPEWN 0.035"));
            pastcodelist.add(new SelectItem("1SPEWN 0.04", "1SPEWN 0.04"));
            pastcodelist.add(new SelectItem("2 SPEWN 0.07", "2 SPEWN 0.07"));
            pastcodelist.add(new SelectItem("2 SPEWN 0.08", "2 SPEWN 0.08"));
            pastcodelist.add(new SelectItem("2 SPEWN 0.09", "2 SPEWN 0.09"));
            pastcodelist.add(new SelectItem("2 SPEWN 0.10", "2 SPEWN 0.10"));
            pastcodelist.add(new SelectItem("2 SPEWN 0.12", "2 SPEWN 0.12"));
            pastcodelist.add(new SelectItem("2 SPEWN 0.14", "2 SPEWN 0.14"));
            pastcodelist.add(new SelectItem("2//1SPEWN 0.07", "2//1SPEWN 0.07"));
            pastcodelist.add(new SelectItem("2//2SPEWN 0.08", "2//2SPEWN 0.08"));
        }
        return pastcodelist;
    }

    public void setPastcodelist(List<SelectItem> pastcodelist) {
        this.pastcodelist = pastcodelist;
    }

    public void changToSave(ActionEvent event) {
        print("================== Chang to Save ================================");
        try {
            getDetailsJpaController().edit(currentdetails);
            getDetailAutoJpaController().edit(currentdetailauto);
            String link = "pages/moduleBackLine/auto/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (NonexistentEntityException nex) {
            print("= Error : " + nex);
        } catch (Exception e) {
            print("= Error2 : " + e);
        }
    }

    public void changToPrint(ActionEvent event) {
        print("=================== Chang to Print ==============================");
        try {
            printassyworksheet.barcode = null;
            printassyworksheet.detail = null;
            workSheetDetail workSheetDetails = (workSheetDetail) getWorksheet().getRowData();
            currentlotControl = getLotControlJpaController().findLotcontrolByBarcode(workSheetDetails.getBarcode()).get(0);
            currentdetails = currentlotControl.getDetailsIdDetail();
            currentModelPool = currentlotControl.getModelPoolId();
            currentdetailauto = getDetailAutoJpaController().findDataDetailList(currentdetails).get(0);
            getBarcodelist().add(new blBarcode(currentlotControl.getId(), currentlotControl.getBarcode(), "./images/barcode/", true));
printassyworksheet.detail = currentdetails;
            String link = "pages/moduleBackLine/auto/printview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changToEdit(ActionEvent event) {
        print("===================== Chang to edit =============================");
        detailprocess = null;
        pcodelist = null;
        try {
            workSheetDetail workSheetDetails = (workSheetDetail) getWorksheet().getRowData();
            currentlotControl = getLotControlJpaController().findLotcontrolByBarcode(workSheetDetails.getBarcode()).get(0);
            currentdetails = currentlotControl.getDetailsIdDetail();
            currentModelPool = currentlotControl.getModelPoolId();
            currentdetailauto = getDetailAutoJpaController().findDataDetailList(currentdetails).get(0);
            List<ModelPool> modelpoollist = getModelPoolJpaController().findModelPoolBySeries(currentdetails.getModelseries());
            for (ModelPool modelpool : modelpoollist) {
                getModellist().add(new SelectItem(modelpool.getId(), modelpool.getModelName()));
                print("= modelpool : " + modelpool.getModelName());
            }
            List<Md2pc> md2pcs = getMd2pcJpaController().findBymodelPoolId(currentModelPool);
            for (Md2pc md2pc : md2pcs) {
                getDetailprocess().add(new detailProcess(md2pc.getProcessPoolIdProc().getCodeOperation(), null, null, null, null, "", md2pc.getProcessPoolIdProc().getProcName(), "", 0, 0, 0, "", ""));
            }
            copperWireInvoicelist = getCopperWireInvoiceJpaController().findCopperWireInvoicesByDetails(currentdetails);
            for (CopperWireInvoice copperWireInvoice : copperWireInvoicelist) {
                getPcodelist().add(new pastCode(copperWireInvoice, copperWireInvoice.getPastCode(), copperWireInvoice.getInvoiceCopper()));
            }
            setDatamodelPcodelist();
            String link = "pages/moduleBackLine/auto/editview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            print("Go to Edit view.");
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changTodelAll(ActionEvent event) {
        print("====================== Del All ==================================");
        try {
            lotControllist = getLotControlJpaController().findByDetailsId(currentdetails);
            currentdetails.setDeleted(1);
            getDetailsJpaController().edit(currentdetails);
            currentdetailauto.setDeleted(1);
            getDetailAutoJpaController().edit(currentdetailauto);
            for (LotControl lotcontrol : lotControllist) {
                lotcontrol.setDeleted(1);
                getLotControlJpaController().edit(lotcontrol);
                print("= Deleted : Lot " + lotcontrol.getId());
            }
            String link = "pages/moduleBackLine/auto/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            print("= Deleted Complete.");
            searchAutoWorksheet();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changToDelOne(ActionEvent event) {
        print("================== Del select One. ==============================");
        try {
            workSheetDetail workSheetDetails = (workSheetDetail) getWorksheet().getRowData();
            currentlotControl = getLotControlJpaController().findLotcontrolByBarcode(workSheetDetails.getBarcode()).get(0);
            currentlotControl.setDeleted(1);
            getLotControlJpaController().edit(currentlotControl);
            print("= Del completed.");
            searchAutoWorksheet();
        } catch (NonexistentEntityException nex) {
            print("Error : " + nex);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void searchAutoWorksheet() {
        print("==================== Search worksheet Auto ======================");
        try {
            if (barcodeSearch != null && !barcodeSearch.trim().isEmpty()) {
                print("= Order Number :: " + barcodeSearch);
                searchByBarcode();
            } else if (processSearch != null && !processSearch.trim().isEmpty()) {
                print("= Process :: " + processSearch);
                searchByProcess();
            } else if (modelSearch != null && !modelSearch.trim().isEmpty()) {
                print("= Type Model :: " + modelSearch);
                searchByModel();
            } else {
                loadWorkSheet();
            }
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    private void searchByModel() {
        worksheetlist = null;
        print("===================== Search By Model ===========================");
//        List<Details> detailslist = getDetailsJpaController().findDetailsByModelAndDetailNameAuto(modelSearch);
        List<ModelPool> modelPoolslist = getModelPoolJpaController().findByModelname(modelSearch);

        if (modelPoolslist.isEmpty()) {
            getWorksheetList();
        }
        for (ModelPool modelpool : modelPoolslist) {
            lotControllist = (List<LotControl>) modelpool.getLotControlCollection();
            for (LotControl lotControl : lotControllist) {
                try {
                    if (lotControl.getDeleted() == 0) {
                        currentdetails = lotControl.getDetailsIdDetail();
                        if (currentdetails.getDetailName().equals("detail_auto") && currentdetails.getDeleted() == 0) {
                            if (lotControl.getMainDataId() != null) {
                                print("T");
                                MainData mainData = getMainDataJpaController().findMainData(lotControl.getMainDataId());
                                ProcessPool processPool = mainData.getIdProc();
                                CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(mainData.getCurrentNumber());
                                getWorksheetList().add(new workSheetDetail(currentdetails, lotControl.getBarcode(), lotControl.getBarcode(), lotControl.getModelPoolId().getModelName(), lotControl.getModelPoolId().getModelSeries(), currentdetails.getDateCreate(), processPool.getProcName(), currentProcess.getStatus()));
                                print("= Detail : " + currentdetails.getDetailName() + " :: Barcode : " + lotControl.getBarcode());
                            } else {
                                getWorksheetList().add(new workSheetDetail(currentdetails, lotControl.getBarcode(), lotControl.getBarcode(), lotControl.getModelPoolId().getModelName(), lotControl.getModelPoolId().getModelSeries(), currentdetails.getDateCreate(), null, null));
                                print("Null");
                            }
                        }
                    }
                } catch (Exception e) {
                    print("= Error : " + e);
                }
            }
        }
        setWorksheet();
    }

    private void searchByProcess() {
        worksheetlist = null;
        print("=================================================================");
        List<ProcessPool> pcpools = getProcessPoolJpaController().findProcessPoolByName(processSearch);
        if (pcpools.isEmpty()) {
            getWorksheetList();
        }
        for (ProcessPool pcpool : pcpools) {
            List<MainData> maindatas = getMainDataJpaController().findMainDataByProcess(pcpool);
            for (MainData maindata : maindatas) {
                currentlotControl = maindata.getLotControlId();
                currentdetails = currentlotControl.getDetailsIdDetail();
                if (currentdetails.getDetailName().equals("detail_auto")) {
                    CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(maindata.getCurrentNumber());
                    getWorksheetList().add(new workSheetDetail(currentdetails, currentlotControl.getBarcode(), currentlotControl.getBarcode(), currentlotControl.getModelPoolId().getModelName(), currentlotControl.getModelPoolId().getModelSeries(), currentdetails.getDateCreate(), pcpool.getProcName(), currentProcess.getStatus()));
                    print("= Detail : " + currentdetails.getDetailName() + " :: Barcode : " + maindata.getLotControlId().getBarcode());
                }
            }
        }
        setWorksheet();
    }

    private void searchByBarcode() {
        worksheetlist = null;
        print("================ search by barcode ==============================");
        lotControllist = getLotControlJpaController().findLotcontrolByBarcode(barcodeSearch);
        if (lotControllist.isEmpty()) {
            getWorksheetList();
        }
        for (LotControl lotcontrol : lotControllist) {
            currentdetails = lotcontrol.getDetailsIdDetail();
            if (currentdetails.getDetailName().equals("detail_auto")) {
                print("= Maindata id :: " + lotcontrol.getMainDataId());
                if (lotcontrol.getMainDataId() != null && !lotcontrol.getMainDataId().trim().isEmpty()) {
                    MainData maindata = getMainDataJpaController().findMainData(lotcontrol.getMainDataId());
                    ProcessPool pcpool = maindata.getIdProc();
                    CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(maindata.getCurrentNumber());
                    getWorksheetList().add(new workSheetDetail(currentdetails, lotcontrol.getBarcode(), lotcontrol.getBarcode(), lotcontrol.getModelPoolId().getModelName(), lotcontrol.getModelPoolId().getModelSeries(), currentdetails.getDateCreate(), pcpool.getProcName(), currentProcess.getStatus()));
                    print("= Detail : " + currentdetails.getDetailName() + " :: Barcode : " + maindata.getLotControlId().getBarcode());
                } else {
                    getWorksheetList().add(new workSheetDetail(currentdetails, lotcontrol.getBarcode(), lotcontrol.getBarcode(), lotcontrol.getModelPoolId().getModelName(), lotcontrol.getModelPoolId().getModelSeries(), currentdetails.getDateCreate(), "", ""));
                    print("= Detail : " + currentdetails.getDetailName() + " :: Barcode : " + lotcontrol.getBarcode());
                }
            }
        }
        setWorksheet();
    }

    public void previous() {
        getWorksheetpagination().previousPage();
        setWorksheet();
    }

    public void next() {
        getWorksheetpagination().nextPage();
        setWorksheet();
    }

    public int getAllPages() {
        int all = getWorksheetpagination().getItemsCount();
        int pagesize = getWorksheetpagination().getPageSize();
        return ((int) all / pagesize) + 1;
    }

    private void loadWorkSheet() {
        worksheetlist = null;
        print("=================================================================");
        detailAutolist = getDetailAutoJpaController().findDetailAutoEntities();
        if (detailAutolist.isEmpty()) {
            getWorksheetList();
        }
        for (DetailAuto detailauto : detailAutolist) {
            currentdetails = detailauto.getDetailIdDetail();
            lotControllist = getLotControlJpaController().findByDetailsId(currentdetails);
            for (LotControl lotControl : lotControllist) {
                try {
                    currentModelPool = lotControl.getModelPoolId();
                    if (lotControl.getMainDataId() != null) {
                        print("T");
                        MainData mainData = getMainDataJpaController().findMainData(lotControl.getMainDataId());
                        ProcessPool processPool = mainData.getIdProc();
                        CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(mainData.getCurrentNumber());
                        getWorksheetList().add(new workSheetDetail(currentdetails, lotControl.getBarcode(), lotControl.getBarcode(), currentModelPool.getModelName(), currentModelPool.getModelSeries(), currentdetails.getDateCreate(), processPool.getProcName(), currentProcess.getStatus()));
                        print("= Detail : " + currentdetails.getDetailName() + " :: Barcode : " + lotControl.getBarcode());
                    } else {
                        getWorksheetList().add(new workSheetDetail(currentdetails, lotControl.getBarcode(), lotControl.getBarcode(), currentModelPool.getModelName(), currentModelPool.getModelSeries(), currentdetails.getDateCreate(), "", ""));
                        print("Null");
                    }
                } catch (Exception e) {
                    print("= Error : " + e);
                }
            }
        }
        setWorksheet();
    }

    public void popupdetailIsselected(ClickActionEvent event) {
        print("===================== selected ==================================");
        try {
//            popupAutoassySelecteds = null;
            popupAutoassyDetail ppaadetail = (popupAutoassyDetail) popupdetailmodel.getRowData();
            LotControl lotControl = ppaadetail.getLotControl();
            print("= Lotcontrol : " + lotControl);
            if (lotControl.getWipQty() > 0) {
                int qty = lotControl.getWipQty();




//            PcsQty pcsQty = ppaadetail.getPcsqty();
//            print("= pcsQty : " + pcsQty);
//            int qty = Integer.parseInt(pcsQty.getWipQty());
                print("= Qty1 : " + qty);
                int ans;
                if (onqty != -1) {
                    popuperror = "";
                    if (onqty >= qty) {
                        onqty = onqty - qty;
                        ans = qty;
                        qty = 0;
                        lotControl.setWipQty(qty);
                    } else {
                        qty -= onqty;
                        ans = onqty;
                        onqty = 0;
                        lotControl.setWipQty(qty);
                    }
                    print("= Qty2 : " + qty);
                    print("= Ans : " + ans);
                    print("= Onqty : " + onqty);
//                getPcsQtyJpaController().edit(pcsQty);
                    getPopupAutoassySelecteds().add(new popupAutoassySelected(lotControl, ans, inqty));
                    setPopupselected();
                    print("POPDetail : " + ppaadetail.getPcsqty().getQty());
                } else {
                    print("= Error : Is not select onqty.");
                    popuperror = "Select this.";
                }
            }
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changOnqtyValue(ValueChangeEvent event) {
        print("===================== chang on qty value ========================");
        try {
            int nums = Integer.parseInt(event.getNewValue().toString());
            nums = nums * Integer.parseInt(getCurrentdetails().getLotcontrol());
            setOnqty(nums);
            popuperror = "";
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }
    private String num = "-1";

    public void changToOnqty(ValueChangeEvent event) {
        print("====================== Chang selecter ===========================");
        try {
            print("= New value : " + event.getNewValue());
            num = event.getNewValue().toString();
            if (!num.equals("-1")) {
                int nums = Integer.parseInt(num);
                inqty = nums;
                onqty = nums * Integer.parseInt(getCurrentdetails().getLotcontrol());
                print("= Onqty : " + onqty);
                onqtyTextvisible = false;
            } else {
                onqty = inqty * Integer.parseInt(getCurrentdetails().getLotcontrol());
                onqtyTextvisible = true;
            }
            popuperror = "";
            print("= Num Value : " + onqty);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    private void loadAssy() {
        print("===================== load Assy to auto =========================");
        popAutoassyDetaillist = null;
        try {
            List<Details> detailsonassy = getDetailsJpaController().findDetailsByDetailName("detail_assy");
            for (Details detail : detailsonassy) {
                print("= Detail : " + detail);
                Collection<LotControl> lotCollection = detail.getLotControlCollection();
                for (LotControl lotControl : lotCollection) {
                    if (lotControl.getWipQty() > 0) {
                        popupAutoassyDetail popa = new popupAutoassyDetail(lotControl);
                        getPopAutoassyDetaillist().add(popa);
                    }
                }
            }
            setPopupdetailmodel();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changToInsert(ActionEvent event) {
        print("==================== insert =====================================");
        try {
            currentdetails = null;
            currentdetailauto = null;
            currentlotControl = null;
            currentModelPool = null;

            String link = "pages/moduleBackLine/auto/createview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void addMatrail(ActionEvent event) {
        print("=================== Add matrail =================================");
        if (!getCurrentdetails().getLotcontrol().equals("") && getCurrentdetails().getLotcontrol() != null) {
            loadAssy();
            popupvisible = true;
            errorLot = "";
        } else {
            errorLot = "Input lot.";
            print("= Input Lot count.");
        }
        print("= Visible : " + popupvisible);
    }

    public void changTocancel(ActionEvent event) {
        print("===================== Cancel ====================================");
        try {
            currentModelPool = null;
            currentdetailauto = null;
            currentdetails = null;
            currentlotControl = null;
            pcodelist = null;
            datamodelPcodelist = null;
            pcodepagination = null;
            pastcode = "";
            invoice = "";
            searchAutoWorksheet();
            String link = "pages/moduleBackLine/auto/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changTodelMatrail(ActionEvent event) {
        print("================ Del matrail ====================================");
        try {
            popupAutoassySelected popause = getPopupselected().getRowData();
            popupAutoassySelecteds.remove(popause);
            setPopupselected();
        } catch (Exception e) {
            print("= Del matrail Error : " + e);
        }
    }

    public void changToSaveAddmatrail(ActionEvent event) {
        print("================= Save addmatrail ===============================");
        popupvisible = false;
        print("= Visible : " + popupvisible);
    }

    public void changTocancelAddmatrail(ActionEvent event) {
        print("==================== Cancel add matirail ========================");
        popAutoassyDetaillist = null;
        popupAutoassySelecteds = null;
        popupdetailmodel = null;
        onqty = 0;
        inqty = 0;
        num = "-1";
        popupselected = null;
        popupvisible = false;
        print("= Visible : " + popupvisible);
    }

    public void create(ActionEvent event) {
        print("======================== Create =================================");
        printassyworksheet.barcode = null;
        printassyworksheet.detail = null;
        try {
            if (currentModelPool != null && getPcodelist().size() > 0) {
                Users user = userLogin.getSessionUser();
                //--- Details -----
                getCurrentdetails().setIdDetail(UUID.randomUUID().toString());
                getCurrentdetails().setDateCreate(new Date());
                getCurrentdetails().setDeleted(0);
                getCurrentdetails().setDetailName("detail_auto");
                getCurrentdetails().setDetailType("backline");
                getCurrentdetails().setUserCreate(user.getId());
                // ----- DetailAuto -----
                getCurrentdetailauto().setId(UUID.randomUUID().toString());
                getCurrentdetailauto().setDetailIdDetail(getCurrentdetails());
                getCurrentdetailauto().setLotcontrolassy("");

                getDetailsJpaController().create(getCurrentdetails());
                print("= Insert Details : " + currentdetails);
                getDetailAutoJpaController().create(getCurrentdetailauto());
                print("= Insert Detailauto : " + currentdetailauto);

                int lot = Integer.parseInt(getCurrentdetails().getLotcontrol());

                String partfile = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/images/barcode/");
                print(" PartFile : " + partfile);
                for (int i = 0; i < lot; i++) {
                    int maxnum = getBarCodeGenerter().getMaxnumberOfLotcontrol() + 1;
                    String barcode = "BAU" + currentdetailauto.getProductCode() + currentdetailauto.getProductType() + "-"
                            + new SimpleDateFormat("MMyy").format(new Date()) + currentModelPool.getModelSeries() + maxnum;
                    getBarCodeGenerter().updateMaxnumberOfLotcontrol(maxnum);

                    getCurrentlotControl().setId(UUID.randomUUID().toString());
                    getCurrentlotControl().setBarcode(barcode);
                    getCurrentlotControl().setDeleted(0);
                    getCurrentlotControl().setDetailsIdDetail(getCurrentdetails());
                    getCurrentlotControl().setLotNumber((i+1) + "");
                    getCurrentlotControl().setModelPoolId(currentModelPool);
                    if (genBarCode(barcode, partfile)) {
                        getLotControlJpaController().create(currentlotControl);
                        getBarcodelist().add(new blBarcode(getCurrentlotControl().getId(), barcode, "./images/barcode/", true));
                        print("= Barcode : " + barcode);
                    }
//                    getLotControlJpaController().create(currentlotControl);
                    print("= Insert Lotcontrol : " + currentlotControl);

                    for (popupAutoassySelected autoselected : popupAutoassySelecteds) {
                        Assy2auto assy2auto = new Assy2auto();
                        assy2auto.setId(UUID.randomUUID().toString());
                        assy2auto.setDetailId(currentdetails);
                        assy2auto.setQty(autoselected.getInqty() + "");
                        assy2auto.setLotcontrolId(currentlotControl);
                        getAssy2autoJpaController().create(assy2auto);
                        getLotControlJpaController().edit(autoselected.getLotControl());
//                        getPcsQtyJpaController().edit(autoselected.getPcsqty());
                    }
                    currentlotControl = null;
                }
                for (pastCode pcode : pcodelist) {
                    getCurrentcopperWireInvoice().setId(UUID.randomUUID().toString());
                    getCurrentcopperWireInvoice().setDetailsIdDetail(currentdetails);
                    getCurrentcopperWireInvoice().setInvoiceCopper(pcode.getInvoid());
                    getCurrentcopperWireInvoice().setPastCode(pcode.getPastcode());

                    getCopperWireInvoiceJpaController().create(currentcopperWireInvoice);
                }
                printassyworksheet.detail = currentdetails;
                print("Detail print : "+printassyworksheet.detail);
                Thread.sleep(1000);
                String link = "pages/moduleBackLine/auto/printview.xhtml";
                userLogin.getSessionMenuBarBean().setParam(link);
            } else {
                print("== Yoy don't select modelpool.");
            }
        } catch (Exception e) {
            print("= Error: " + e);
        }
    }
    private CopperWireInvoiceJpaController copperWireInvoiceJpaController;

    private CopperWireInvoiceJpaController getCopperWireInvoiceJpaController() {
        if (copperWireInvoiceJpaController == null) {
            copperWireInvoiceJpaController = new CopperWireInvoiceJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return copperWireInvoiceJpaController;
    }
    private CopperWireInvoice currentcopperWireInvoice;

    public CopperWireInvoice getCurrentcopperWireInvoice() {
        if (currentcopperWireInvoice == null) {
            currentcopperWireInvoice = new CopperWireInvoice();
        }
        return currentcopperWireInvoice;
    }

    private List<blBarcode> getBarcodelist() {
        if (printassyworksheet.barcode == null) {
            printassyworksheet.barcode = new ArrayList<blBarcode>();
        }
        return printassyworksheet.barcode;
    }

    private boolean genBarCode(String code, String filePath) {
        print("===================== genbarcode file ===========================");
        try {
            getBarCodeGenerter().createBarcode(code, filePath + "/1d/");
            getBarCodeGenerter().createQrCode(code, filePath + "/2d/");
            print("= GenCode : Complete");
            return true;
        } catch (Exception e) {
            print("= Error can not genbarcode." + e.toString());
            return false;
        }
    }

    public void changProductCodenameValue(ValueChangeEvent event) {
        print("======================== Product code ===========================");
        String pd_code = null;
        try {
            if (event.getNewValue().equals("Prototype")) {
                pd_code = "P";
            } else if (event.getNewValue().equals("New product introduction")) {
                pd_code = "N";
            } else if (event.getNewValue().equals("Special built request")) {
                pd_code = "S";
            } else if (event.getNewValue().equals("Mass production")) {
                pd_code = "M";
            }
            getCurrentdetailauto().setProductCodeName(event.getNewValue().toString());
            getCurrentdetailauto().setProductCode(pd_code);
        } catch (Exception e) {
            print("= Error : " + e);
        }
        print("= Product name : " + event.getNewValue());
        print("= Product Code : " + pd_code);
    }

    public void changProductTypeValue(ValueChangeEvent event) {
        print("=================== chang product type ==========================");
        try {
            String pdType = event.getNewValue().toString();
            getCurrentdetailauto().setProductType(pdType);
            print("= PdType : " + getCurrentdetailauto().getProductType());
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    private void print(String str) {
        System.err.println(str);
    }

    public List<SelectItem> getProductcodename() {
        if (productcodename == null) {
            productcodename = new ArrayList<SelectItem>();
            productcodename.add(new SelectItem("Prototype", "Prototype"));
            productcodename.add(new SelectItem("New product introduction", "New product introduction"));
            productcodename.add(new SelectItem("Special built request", "Special built request"));
            productcodename.add(new SelectItem("Mass production", "Mass production"));
        }
        return productcodename;
    }

    public List<SelectItem> getProducttype() {
        if (producttype == null) {
            producttype = new ArrayList<SelectItem>();
            producttype.add(new SelectItem("T", "Trigger coil"));
            producttype.add(new SelectItem("TR", "Tranformer"));
            producttype.add(new SelectItem("PCB", "Print circuit board"));
        }
        return producttype;
    }

    public void changModelValue(ValueChangeEvent event) {
        print("==================== chang model ================================");
        detailprocess = null;
        try {
            String modelId = event.getNewValue().toString();
            print("= ModelId : " + modelId);
            currentModelPool = getModelPoolJpaController().findModelPool(modelId);
            getCurrentdetails().setModel(modelId);
            List<Md2pc> md2pcs = getMd2pcJpaController().findBymodelPoolId(currentModelPool);
            for (Md2pc md2pc : md2pcs) {
                getDetailprocess().add(new detailProcess(md2pc.getProcessPoolIdProc().getCodeOperation(), null, null, null, null, "", md2pc.getProcessPoolIdProc().getProcName(), "", 0, 0, 0, "", ""));
            }
            print("= Model : " + currentModelPool);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changModelSerieslistValue(ValueChangeEvent event) {
        print("============ chang model series =================================");
        modellist = null;
        detailprocess = null;
        try {
            String series = event.getNewValue().toString();
            List<ModelPool> modelpoollist = getModelPoolJpaController().findModelPoolBySeries(series);
            for (ModelPool modelpool : modelpoollist) {
                getModellist().add(new SelectItem(modelpool.getId(), modelpool.getModelName()));
                print("= modelpool : " + modelpool.getModelName());
            }
            getCurrentdetails().setModelseries(series);
            print("= Mosel Series : " + getCurrentdetails().getModelseries());
        } catch (Exception e) {
            print(" Error : " + e);
        }
    }

    public List<SelectItem> getModellist() {
        if (modellist == null) {
            modellist = new ArrayList<SelectItem>();
        }
        return modellist;
    }

    public List<SelectItem> getModelserieslist() {
        if (modelserieslist == null) {
            modelserieslist = new ArrayList<SelectItem>();
            List<String> serieslist = getModelPoolJpaController().findModelPoolGroupBySeries();
            for (String series : serieslist) {
                modelserieslist.add(new SelectItem(series, series));
            }
        }
        return modelserieslist;
    }

    public DetailAuto getCurrentdetailauto() {
        if (currentdetailauto == null) {
            currentdetailauto = new DetailAuto();
        }
        return currentdetailauto;
    }

    public void setCurrentdetailauto(DetailAuto currentdetailauto) {
        this.currentdetailauto = currentdetailauto;
    }

    public Details getCurrentdetails() {
        if (currentdetails == null) {
            currentdetails = new Details();
        }
        return currentdetails;
    }

    public void setCurrentdetails(Details currentdetails) {
        this.currentdetails = currentdetails;
    }
    private ModelPoolJpaController modelPoolJpaController = null;

    private ModelPoolJpaController getModelPoolJpaController() {
        if (modelPoolJpaController == null) {
            modelPoolJpaController = new ModelPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return modelPoolJpaController;
    }
    private DetailsJpaController detailsJpaController = null;

    private DetailsJpaController getDetailsJpaController() {
        if (detailsJpaController == null) {
            detailsJpaController = new DetailsJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailsJpaController;
    }
    private DetailAutoJpaController detailAutoJpaController = null;

    private DetailAutoJpaController getDetailAutoJpaController() {
        if (detailAutoJpaController == null) {
            detailAutoJpaController = new DetailAutoJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailAutoJpaController;
    }

    public ModelPool getCurrentModelPool() {
        return currentModelPool;
    }

    public void setCurrentModelPool(ModelPool currentModelPool) {
        this.currentModelPool = currentModelPool;
    }

    public LotControl getCurrentlotControl() {
        if (currentlotControl == null) {
            currentlotControl = new LotControl();
        }
        return currentlotControl;
    }

    public void setCurrentlotControl(LotControl currentlotControl) {
        this.currentlotControl = currentlotControl;
    }
    private LotControlJpaController lotControlJpaController = null;

    private LotControlJpaController getLotControlJpaController() {
        if (lotControlJpaController == null) {
            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotControlJpaController;
    }

    public boolean isPopupvisible() {
        return popupvisible;
    }

    public void setPopupvisible(boolean popupvisible) {
        this.popupvisible = popupvisible;
    }
    private MainDataJpaController mainDataJpaController = null;

    private MainDataJpaController getMainDataJpaController() {
        if (mainDataJpaController == null) {
            mainDataJpaController = new MainDataJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mainDataJpaController;
    }
    private CurrentProcessJpaController currentProcessJpaController = null;

    private CurrentProcessJpaController getCurrentProcessJpaController() {
        if (currentProcessJpaController == null) {
            currentProcessJpaController = new CurrentProcessJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return currentProcessJpaController;
    }
    private PcsQtyJpaController pcsQtyJpaController = null;

    private PcsQtyJpaController getPcsQtyJpaController() {
        if (pcsQtyJpaController == null) {
            pcsQtyJpaController = new PcsQtyJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return pcsQtyJpaController;
    }
    private Assy2autoJpaController assy2autoJpaController = null;

    private Assy2autoJpaController getAssy2autoJpaController() {
        if (assy2autoJpaController == null) {
            assy2autoJpaController = new Assy2autoJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return assy2autoJpaController;
    }

    public List<popupAutoassyDetail> getPopAutoassyDetaillist() {
        if (popAutoassyDetaillist == null) {
            popAutoassyDetaillist = new ArrayList<popupAutoassyDetail>();
        }
        return popAutoassyDetaillist;
    }

    public List<popupAutoassySelected> getPopupAutoassySelecteds() {
        if (popupAutoassySelecteds == null) {
            popupAutoassySelecteds = new ArrayList<popupAutoassySelected>();
        }
        return popupAutoassySelecteds;
    }

    public void setPopupAutoassySelecteds(List<popupAutoassySelected> popupAutoassySelecteds) {
        this.popupAutoassySelecteds = popupAutoassySelecteds;
    }

    public DataModel<popupAutoassyDetail> getPopupdetailmodel() {
        return popupdetailmodel;
    }

    public void setPopupdetailmodel() {
        this.popupdetailmodel = getPopupdetailModelPagination().createPageDataModel();
    }

    public DataModel<popupAutoassySelected> getPopupselected() {
        return popupselected;
    }

    public void setPopupselected() {
        this.popupselected = getPopupselect().createPageDataModel();
    }
    private PaginationHelper popupdetailModelPagination;

    private PaginationHelper getPopupdetailModelPagination() {
        popupdetailModelPagination = new PaginationHelper(popAutoassyDetaillist.size()) {

            @Override
            public int getItemsCount() {
                return getPopAutoassyDetaillist().size();
            }

            @Override
            public DataModel createPageDataModel() {
                print("PageFirst : " + getPageFirstItem());
                print("PageLase : " + getPageLastItem());
                ListDataModel listmodel;
                if (getPopAutoassyDetaillist().size() > 0) {
                    listmodel = new ListDataModel(getPopAutoassyDetaillist().subList(getPageFirstItem(), getPageLastItem() + 1));
                } else {
                    listmodel = new ListDataModel(getPopAutoassyDetaillist());
                }
                return listmodel;
            }
        };
        return popupdetailModelPagination;
    }
    private PaginationHelper popupselect;

    private PaginationHelper getPopupselect() {
        popupselect = new PaginationHelper(getPopupAutoassySelecteds().size()) {

            @Override
            public int getItemsCount() {
                return getPopupAutoassySelecteds().size();
            }

            @Override
            public DataModel createPageDataModel() {
                print("PageFirst : " + getPageFirstItem());
                print("PageLase : " + getPageLastItem());
                ListDataModel listmodel;
                if (getPopupAutoassySelecteds().size() > 0) {
                    listmodel = new ListDataModel(getPopupAutoassySelecteds().subList(getPageFirstItem(), getPageLastItem() + 1));
                } else {
                    listmodel = new ListDataModel(getPopupAutoassySelecteds());
                }
                return listmodel;
            }
        };
        return popupselect;
    }

    public int getOnqty() {
        return onqty;
    }

    public void setOnqty(int onqty) {
        this.onqty = onqty;
        print("= Onqty : " + onqty);
    }

    public boolean isOnqtyTextvisible() {
        return onqtyTextvisible;
    }

    public void setOnqtyTextvisible(boolean onqtyTextvisible) {
        this.onqtyTextvisible = onqtyTextvisible;
    }

    public String getPopuperror() {
        return popuperror;
    }

    public String getErrorLot() {
        return errorLot;
    }

    public void setErrorLot(String errorLot) {
        this.errorLot = errorLot;
    }

    public int getInqty() {
        return inqty;
    }

    public void setInqty(int inqty) {
        this.inqty = inqty;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }
    private BarCodeGenerter barCodeGenerter = null;

    private BarCodeGenerter getBarCodeGenerter() {
        if (barCodeGenerter == null) {
            barCodeGenerter = new BarCodeGenerter();
        }
        return barCodeGenerter;
    }

    public String getBarcodeSearch() {
        return barcodeSearch;
    }

    public void setBarcodeSearch(String barcodeSearch) {
        this.barcodeSearch = barcodeSearch;
    }

    public List<detailProcess> getDetailprocess() {
        if (detailprocess == null) {
            detailprocess = new ArrayList<detailProcess>();
        }
        return detailprocess;
    }

    public void setDetailprocess(List<detailProcess> detailprocess) {
        this.detailprocess = detailprocess;
    }

    public String getModelSearch() {
        return modelSearch;
    }

    public void setModelSearch(String modelSearch) {
        this.modelSearch = modelSearch;
    }

    public String getProcessSearch() {
        return processSearch;
    }

    public void setProcessSearch(String processSearch) {
        this.processSearch = processSearch;
    }

    private List<workSheetDetail> getWorksheetList() {
        if (worksheetlist == null) {
            worksheetlist = new ArrayList<workSheetDetail>();
        }
        return worksheetlist;
    }

    public DataModel<workSheetDetail> getWorksheet() {
        return worksheet;
    }

    private void setWorksheet() {
        worksheet = getWorksheetpagination().createPageDataModel();
    }
    private PaginationHelper worksheetpagination;

    public PaginationHelper getWorksheetpagination() {
        if (worksheetpagination == null) {
            worksheetpagination = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    return getWorksheetList().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    print("PageFirst : " + getPageFirstItem());
                    print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (getWorksheetList().size() > 0) {
                        listmodel = new ListDataModel(getWorksheetList().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(getWorksheetList());
                    }
                    return listmodel;
                }
            };
        }
        return worksheetpagination;
    }

    public String getSeriesSearch() {
        return seriesSearch;
    }

    public void setSeriesSearch(String seriesSearch) {
        this.seriesSearch = seriesSearch;
    }
    private Md2pcJpaController md2pcJpaController = null;

    private Md2pcJpaController getMd2pcJpaController() {
        if (md2pcJpaController == null) {
            md2pcJpaController = new Md2pcJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return md2pcJpaController;
    }
    private ProcessPoolJpaController processPoolJpaController = null;

    private ProcessPoolJpaController getProcessPoolJpaController() {
        if (processPoolJpaController == null) {
            processPoolJpaController = new ProcessPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return processPoolJpaController;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getPastcode() {
        return pastcode;
    }

    public void setPastcode(String pastcode) {
        this.pastcode = pastcode;
    }

    public List<pastCode> getPcodelist() {
        if (pcodelist == null) {
            pcodelist = new ArrayList<pastCode>();
        }
        return pcodelist;
    }

    public void setPcodelist(List<pastCode> pcodelist) {
        this.pcodelist = pcodelist;
    }

    public DataModel<pastCode> getDatamodelPcodelist() {
        return datamodelPcodelist;
    }

    public void setDatamodelPcodelist() {
        this.datamodelPcodelist = getPcodepagination().createPageDataModel();
    }

    public List<CopperWireInvoice> getCopperWireInvoicelist() {
        if (copperWireInvoicelist == null) {
            copperWireInvoicelist = new ArrayList<CopperWireInvoice>();
        }
        return copperWireInvoicelist;
    }

    public void setCopperWireInvoicelist(List<CopperWireInvoice> copperWireInvoicelist) {
        this.copperWireInvoicelist = copperWireInvoicelist;
    }
}
