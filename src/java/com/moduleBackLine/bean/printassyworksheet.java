/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moduleBackLine.bean;

import com.moduleBackLine.Dataworksheet.blBarcode;
import com.tct.dashboard.ReportBean;
import com.tct.data.Details;
import com.tct.data.LotControl;
import com.tct.data.jpa.LotControlJpaController;
import java.util.HashMap;
import java.util.List;
import javax.faces.event.ActionEvent;
import javax.persistence.Persistence;

/**
 *
 * @author The_Boy_Cs
 */

public class printassyworksheet {   // this printBlworksheet

    public static List<blBarcode> barcode = null;
    public static Details detail;

    public printassyworksheet() {
    }
    String url = "";

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    private LotControlJpaController lotControlJpaController;

    private LotControlJpaController getLotControlJpaController() {
        if (lotControlJpaController == null) {
            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotControlJpaController;
    }

    public boolean printFrontlineAll(){
        HashMap input = new HashMap();
        String barcodes = getBarcode().get(0).getBarcode();
        LotControl lotControl = getLotControlJpaController().findProcessNameAndStatus(barcodes);
        String filename = lotControl.getDetailsIdDetail().getIdDetail();
        input.put("detailId", filename);
        
        print("= File name : " + filename);
        if (new ReportBean().pdf("/report/frontline.jasper", "/report/" + filename + ".pdf", input)) {
            System.out.println("complete");
            /*
             * url ="window.open(\"http://www.w3schools.com\");";
             * top.preview_html(
             *
             */
            url = " alert('Print');"
                    //                    + " window.location.href = 'report/test.pdf';";
                    + "window.open(\"./report/" + filename + ".pdf\");";
            return true;
        }
        return false;
    }
    
    public boolean printFrontlineSelection(){
        String selectLot = "";
        String sql = "";
        sql = " SELECT "
                + "lot_control.details_id_detail AS lot_control_details_id_detail,"
                + "md2pc.process_pool_id_proc AS md2pc_process_pool_id_proc,"
                + "process_pool.proc_name AS process_pool_proc_name,"
                + "process_pool.code_operation AS process_pool_code_operation,"
                + "md2pc.sq_process AS md2pc_sq_process,"
                + "lot_control.lot_number AS lot_control_lot_number,"
                + "details.model AS details_model,"
                + "details.line AS details_line,"
                + "lot_control.barcode AS lot_control_barcode,"
                + "model_pool.model_series AS model_pool_model_series,"
                + "model_pool.model_name AS model_pool_model_name,"
                + "detail_assy.drcore_lot_no AS detail_assy_drcore_lot_no,"
                + "detail_assy.pd_code AS detail_assy_pd_code,"
                + "detail_assy.pd_codename AS detail_assy_pd_codename,"
                + "detail_assy.product_qty AS detail_assy_product_qty,"
                + "detail_assy.stamp AS detail_assy_stamp "
                + "FROM lot_control lot_control "
                + "RIGHT OUTER JOIN md2pc md2pc ON lot_control.model_pool_id = md2pc.model_pool_id "
                + "RIGHT OUTER JOIN process_pool process_pool ON md2pc.process_pool_id_proc = process_pool.id_proc "
                + "INNER JOIN details details ON lot_control.details_id_detail = details.id_detail "
                + "INNER JOIN model_pool model_pool ON lot_control.model_pool_id = model_pool.id "
                + "INNER JOIN detail_assy detail_assy ON details.id_detail = detail_assy.detail_id_detail "
                + "WHERE "
                + "lot_control.details_id_detail = '" + getDetail().getIdDetail() + "' and "
                + " lot_control.barcode in (";
        if (getBarcode() != null) {
            for (blBarcode bc : getBarcode()) {
                if (bc.isSelected()) {
                    selectLot += "'" + bc.getBarcode() + "',";
                }
            }
        }
        System.out.println("id lot : " + selectLot);
        sql += selectLot;
        sql += "'') ORDER BY"
                + " lot_control.barcode ASC, "
                + " md2pc.sq_process ASC ;";
        if (!selectLot.equals("")) {
            System.out.println("SQL : " + sql);
            print("detail Id : "+getDetail().getIdDetail());
            if (new ReportBean().pdfSelect("/report/frontlineselected.jasper", "/report/" + getDetail().getIdDetail() + ".pdf", sql)) {
                System.out.println("complete");
                /*
                 * url ="window.open(\"http://www.w3schools.com\");";
                 * top.preview_html(
                 *
                 */
                url = " alert('Print');"
                        + " window.open('./report/" + getDetail().getIdDetail() + ".pdf');";
                print(" URL : " + url);
                return true;
            }
        }
        return false;
    }
    
    public boolean printAssyAll(ActionEvent event) {
        HashMap input = new HashMap();
        String barcodes = getBarcode().get(0).getBarcode();
        LotControl lotControl = getLotControlJpaController().findProcessNameAndStatus(barcodes);
        String filename = lotControl.getDetailsIdDetail().getIdDetail();
        input.put("detailId", filename);
        
        print("= File name : " + filename);
        if (new ReportBean().pdf("/report/assy.jasper", "/report/" + filename + ".pdf", input)) {
            System.out.println("complete");
            /*
             * url ="window.open(\"http://www.w3schools.com\");";
             * top.preview_html(
             *
             */
            url = " alert('Print');"
                    //                    + " window.location.href = 'report/test.pdf';";
                    + "window.open(\"./report/" + filename + ".pdf\");";
            return true;
        }
        return false;
    }

    public boolean printAssySelection(ActionEvent event) {
        String selectLot = "";
        String sql = "";
        sql = " SELECT "
                + "lot_control.details_id_detail AS lot_control_details_id_detail,"
                + "md2pc.process_pool_id_proc AS md2pc_process_pool_id_proc,"
                + "process_pool.proc_name AS process_pool_proc_name,"
                + "process_pool.code_operation AS process_pool_code_operation,"
                + "md2pc.sq_process AS md2pc_sq_process,"
                + "lot_control.lot_number AS lot_control_lot_number,"
                + "details.model AS details_model,"
                + "details.line AS details_line,"
                + "lot_control.barcode AS lot_control_barcode,"
                + "model_pool.model_series AS model_pool_model_series,"
                + "model_pool.model_name AS model_pool_model_name,"
                + "detail_assy.drcore_lot_no AS detail_assy_drcore_lot_no,"
                + "detail_assy.pd_code AS detail_assy_pd_code,"
                + "detail_assy.pd_codename AS detail_assy_pd_codename,"
                + "detail_assy.product_qty AS detail_assy_product_qty,"
                + "detail_assy.stamp AS detail_assy_stamp "
                + "FROM lot_control lot_control "
                + "RIGHT OUTER JOIN md2pc md2pc ON lot_control.model_pool_id = md2pc.model_pool_id "
                + "RIGHT OUTER JOIN process_pool process_pool ON md2pc.process_pool_id_proc = process_pool.id_proc "
                + "INNER JOIN details details ON lot_control.details_id_detail = details.id_detail "
                + "INNER JOIN model_pool model_pool ON lot_control.model_pool_id = model_pool.id "
                + "INNER JOIN detail_assy detail_assy ON details.id_detail = detail_assy.detail_id_detail "
                + "WHERE "
                + "lot_control.details_id_detail = '" + getDetail().getIdDetail() + "' and "
                + " lot_control.barcode in (";
        if (getBarcode() != null) {
            for (blBarcode bc : getBarcode()) {
                if (bc.isSelected()) {
                    selectLot += "'" + bc.getBarcode() + "',";
                }
            }
        }
        System.out.println("id lot : " + selectLot);
        sql += selectLot;
        sql += "'') ORDER BY"
                + " lot_control.barcode ASC, "
                + " md2pc.sq_process ASC ;";
        if (!selectLot.equals("")) {
            System.out.println("SQL : " + sql);
            print("detail Id : "+getDetail().getIdDetail());
            if (new ReportBean().pdfSelect("/report/assyselected.jasper", "/report/" + getDetail().getIdDetail() + ".pdf", sql)) {
                System.out.println("complete");
                /*
                 * url ="window.open(\"http://www.w3schools.com\");";
                 * top.preview_html(
                 *
                 */
                url = " alert('Print');"
                        + " window.open('./report/" + getDetail().getIdDetail() + ".pdf');";
                print(" URL : " + url);
                return true;
            }
        }
        return false;

    }

    public boolean printAutoAll(ActionEvent event) {
        HashMap input = new HashMap();
        String barcodes = getBarcode().get(0).getBarcode();
        LotControl lotControl = getLotControlJpaController().findProcessNameAndStatus(barcodes);
        String filename = lotControl.getDetailsIdDetail().getIdDetail();
        input.put("detailId", filename);
        
        print("= File name : " + filename);
        if (new ReportBean().pdf("/report/auto.jasper", "/report/" + filename + ".pdf", input)) {
            System.out.println("complete");
            /*
             * url ="window.open(\"http://www.w3schools.com\");";
             * top.preview_html(
             *
             */
            url = " alert('Print');"
                    //                    + " window.location.href = 'report/test.pdf';";
                    + "window.open(\"./report/" + filename + ".pdf\");";
            return true;
        }
        return false;
    }

    public boolean printAutoSelection(ActionEvent event) {
        String selectLot = "";
        String sql = "";
        sql = "SELECT"
                + " lot_control.details_id_detail AS lot_control_details_id_detail,"
                + "md2pc.process_pool_id_proc AS md2pc_process_pool_id_proc,"
                + "process_pool.proc_name AS process_pool_proc_name,"
                + "process_pool.code_operation AS process_pool_code_operation,"
                + "md2pc.sq_process AS md2pc_sq_process,"
                + "lot_control.lot_number AS lot_control_lot_number,"
                + "details.model AS details_model,"
                + "details.line AS details_line,"
                + "lot_control.barcode AS lot_control_barcode,"
                + "detail_auto.stamp AS detail_auto_stamp,"
                + "detail_auto.dr_core AS detail_auto_dr_core,"
                + "detail_auto.product_code_name AS detail_auto_product_code_name,"
                + "detail_auto.product_code AS detail_auto_product_code,"
                + "detail_auto.product_type AS detail_auto_product_type,"
                + "model_pool.model_series AS model_pool_model_series,"
                + "model_pool.model_name AS model_pool_model_name"
                + " FROM lot_control lot_control "
                + "RIGHT OUTER JOIN md2pc md2pc ON lot_control.model_pool_id = md2pc.model_pool_id"
                + " RIGHT OUTER JOIN process_pool process_pool ON md2pc.process_pool_id_proc = process_pool.id_proc"
                + " INNER JOIN details details ON lot_control.details_id_detail = details.id_detail"
                + " INNER JOIN model_pool model_pool ON lot_control.model_pool_id = model_pool.id"
                + " INNER JOIN detail_auto detail_auto ON details.id_detail = detail_auto.detail_id_detail"
                + " WHERE"
                + " lot_control.details_id_detail = '" + getDetail().getIdDetail() + "' and "
                + " lot_control.barcode in (";
        if (getBarcode() != null) {
            for (blBarcode bc : getBarcode()) {
                if (bc.isSelected()) {
                    selectLot += "'" + bc.getBarcode() + "',";
                }
            }
        }
        System.out.println("id lot : " + selectLot);
        sql += selectLot;
        sql += "'') ORDER BY"
                + " lot_control.barcode ASC, "
                + " md2pc.sq_process ASC ;";
        if (!selectLot.equals("")) {
            System.out.println("SQL : " + sql);
            print("detail Id : "+getDetail().getIdDetail());
            if (new ReportBean().pdfSelect("/report/autoselected.jasper", "/report/" + getDetail().getIdDetail() + ".pdf", sql)) {
                System.out.println("complete");
                /*
                 * url ="window.open(\"http://www.w3schools.com\");";
                 * top.preview_html(
                 *
                 */
                url = " alert('Print');"
                        + " window.open('./report/" + getDetail().getIdDetail() + ".pdf');";
                print(" URL : " + url);
                return true;
            }
        }
        return false;

    }
    
     public boolean printAssemblyAll(ActionEvent event) {
        HashMap input = new HashMap();
        String barcodes = getBarcode().get(0).getBarcode();
        LotControl lotControl = getLotControlJpaController().findProcessNameAndStatus(barcodes);
        String filename = lotControl.getDetailsIdDetail().getIdDetail();
        input.put("detailId", filename);
        
        print("= File name : " + filename);
        if (new ReportBean().pdf("/report/assembly.jasper", "/report/" + filename + ".pdf", input)) {
            System.out.println("complete");
            /*
             * url ="window.open(\"http://www.w3schools.com\");";
             * top.preview_html(
             *
             */
            url = " alert('Print');"
                    //                    + " window.location.href = 'report/test.pdf';";
                    + "window.open(\"./report/" + filename + ".pdf\");";
            return true;
        }
        return false;
    }

    public boolean printAssemblySelection(ActionEvent event) {
        String selectLot = "";
        String sql = "";
        sql = "SELECT"
                + " lot_control.details_id_detail AS lot_control_details_id_detail,"
                + "     md2pc.process_pool_id_proc AS md2pc_process_pool_id_proc,"
                + "     process_pool.proc_name AS process_pool_proc_name,"
                + "     process_pool.code_operation AS process_pool_code_operation,"
                + "     md2pc.sq_process AS md2pc_sq_process,"
                + "     lot_control.lot_number AS lot_control_lot_number,"
                + "     details.model AS details_model,"
                + "     details.line AS details_line,"
                + "     lot_control.barcode AS lot_control_barcode,"
                + "     line_assembly.remark AS line_assembly_remark,"
                + "     line_assembly.stamp AS line_assembly_stamp,"
                + "     line_assembly.pd_code_name AS line_assembly_pd_code_name,"
                + "     line_assembly.pd_code AS line_assembly_pd_code,"
                + "     model_pool.model_series AS model_pool_model_series,"
                + "     model_pool.model_name AS model_pool_model_name,"
                + "     line_assembly.pd_type AS line_assembly_pd_type,"
                + "     line_assembly.potcore AS line_assembly_potcore"
                + " FROM lot_control"
                + " lot_control RIGHT OUTER JOIN md2pc md2pc ON lot_control.model_pool_id = md2pc.model_pool_id"
                + "     RIGHT OUTER JOIN process_pool process_pool ON md2pc.process_pool_id_proc = process_pool.id_proc"
                + "     INNER JOIN details details ON lot_control.details_id_detail = details.id_detail"
                + "     INNER JOIN model_pool model_pool ON lot_control.model_pool_id = model_pool.id"
                + "     INNER JOIN line_assembly line_assembly ON details.id_detail = line_assembly.detail_id_detail"
                + " WHERE"
                + "     lot_control.details_id_detail = '" + getDetail().getIdDetail() + "' and "
                + " lot_control.barcode in (";
        if (getBarcode() != null) {
            for (blBarcode bc : getBarcode()) {
                if (bc.isSelected()) {
                    selectLot += "'" + bc.getBarcode() + "',";
                }
            }
        }
        System.out.println("id lot : " + selectLot);
        sql += selectLot;
        sql += "'') ORDER BY"
                + " lot_control.barcode ASC, "
                + " md2pc.sq_process ASC ;";
        if (!selectLot.equals("")) {
            System.out.println("SQL : " + sql);
            print("detail Id : "+getDetail().getIdDetail());
            if (new ReportBean().pdfSelect("/report/assemblyselected.jasper", "/report/" + getDetail().getIdDetail() + ".pdf", sql)) {
                System.out.println("complete");
                /*
                 * url ="window.open(\"http://www.w3schools.com\");";
                 * top.preview_html(
                 *
                 */
                url = " alert('Print');"
                        + " window.open('./report/" + getDetail().getIdDetail() + ".pdf');";
                print(" URL : " + url);
                return true;
            }
        }
        return false;

    }
    
    public boolean printpackingAll(ActionEvent event) {
        HashMap input = new HashMap();
        String barcodes = getBarcode().get(0).getBarcode();
        LotControl lotControl = getLotControlJpaController().findProcessNameAndStatus(barcodes);
        String filename = lotControl.getDetailsIdDetail().getIdDetail();
        input.put("detailId", filename);
        
        print("= File name : " + filename);
        if (new ReportBean().pdf("/report/packing.jasper", "/report/" + filename + ".pdf", input)) {
            System.out.println("complete");
            /*
             * url ="window.open(\"http://www.w3schools.com\");";
             * top.preview_html(
             *
             */
            url = " alert('Print');"
                    //                    + " window.location.href = 'report/test.pdf';";
                    + "window.open(\"./report/" + filename + ".pdf\");";
            return true;
        }
        return false;
    }

    public boolean printpackingSelection(ActionEvent event) {
        String selectLot = "";
        String sql = "";
        sql = "SELECT"
                + "     lot_control.details_id_detail AS lot_control_details_id_detail,"
                + "     md2pc.process_pool_id_proc AS md2pc_process_pool_id_proc,"
                + "     process_pool.proc_name AS process_pool_proc_name,"
                + "     process_pool.code_operation AS process_pool_code_operation,"
                + "     md2pc.sq_process AS md2pc_sq_process,"
                + "     lot_control.lot_number AS lot_control_lot_number,"
                + "     details.line AS details_line,"
                + "     lot_control.barcode AS lot_control_barcode,"
                + "     model_pool.model_series AS model_pool_model_series,"
                + "     model_pool.model_name AS model_pool_model_name,"
                + "     detail_packing.pd_code_name AS detail_packing_pd_code_name,"
                + "     detail_packing.pd_code AS detail_packing_pd_code,"
                + "     detail_packing.stamp AS detail_packing_stamp"
                + " FROM"
                + "     lot_control lot_control"
                + " RIGHT OUTER JOIN md2pc md2pc ON lot_control.model_pool_id = md2pc.model_pool_id"
                + "     RIGHT OUTER JOIN process_pool process_pool ON md2pc.process_pool_id_proc = process_pool.id_proc"
                + "     INNER JOIN details details ON lot_control.details_id_detail = details.id_detail"
                + "     INNER JOIN model_pool model_pool ON lot_control.model_pool_id = model_pool.id"
                + "     INNER JOIN detail_packing detail_packing ON details.id_detail = detail_packing.details_id_detail"
                + " WHERE"
                + "     lot_control.details_id_detail  = '" + getDetail().getIdDetail() + "' and "
                + " lot_control.barcode in (";
        if (getBarcode() != null) {
            for (blBarcode bc : getBarcode()) {
                if (bc.isSelected()) {
                    selectLot += "'" + bc.getBarcode() + "',";
                }
            }
        }
        System.out.println("id lot : " + selectLot);
        sql += selectLot;
        sql += "'') ORDER BY"
                + " lot_control.barcode ASC, "
                + " md2pc.sq_process ASC ;";
        if (!selectLot.equals("")) {
            System.out.println("SQL : " + sql);
            print("detail Id : "+getDetail().getIdDetail());
            if (new ReportBean().pdfSelect("/report/packingselected.jasper", "/report/" + getDetail().getIdDetail() + ".pdf", sql)) {
                System.out.println("complete");
                /*
                 * url ="window.open(\"http://www.w3schools.com\");";
                 * top.preview_html(
                 *
                 */
                url = " alert('Print');"
                        + " window.open('./report/" + getDetail().getIdDetail() + ".pdf');";
                print(" URL : " + url);
                return true;
            }
        }
        return false;

    }
    
    public boolean printnewcreateAll(ActionEvent event) {
        HashMap input = new HashMap();
        String barcodes = getBarcode().get(0).getBarcode();
        LotControl lotControl = getLotControlJpaController().findProcessNameAndStatus(barcodes);
        String filename = lotControl.getDetailsIdDetail().getIdDetail();
        input.put("detailId", filename);
        
        print("= File name : " + filename);
        if (new ReportBean().pdf("/report/newcreate.jasper", "/report/" + filename + ".pdf", input)) {
            System.out.println("complete");
            /*
             * url ="window.open(\"http://www.w3schools.com\");";
             * top.preview_html(
             *
             */
            url = " alert('Print');"
                    //                    + " window.location.href = 'report/test.pdf';";
                    + "window.open(\"./report/" + filename + ".pdf\");";
            return true;
        }
        return false;
    }

    public boolean printnewcreateSelection(ActionEvent event) {
        String selectLot = "";
        String sql = "";
        sql = "SELECT"
                + "     lot_control.details_id_detail AS lot_control_details_id_detail,"
                + "     md2pc.process_pool_id_proc AS md2pc_process_pool_id_proc,"
                + "     process_pool.proc_name AS process_pool_proc_name,"
                + "     process_pool.code_operation AS process_pool_code_operation,"
                + "     md2pc.sq_process AS md2pc_sq_process,"
                + "     lot_control.lot_number AS lot_control_lot_number,"
                + "     details.model AS details_model,"
                + "     details.line AS details_line,"
                + "     lot_control.barcode AS lot_control_barcode,"
                + "     refu01.stamp AS refu01_stamp,"
                + "     refu01.l2 AS refu01_l2,"
                + "     refu01.l1 AS refu01_l1,"
                + "     refu01.type AS refu01_type,"
                + "     model_pool.model_series AS model_pool_model_series,"
                + "     model_pool.model_name AS model_pool_model_name"
                + " FROM"
                + "     lot_control lot_control "
                + " RIGHT OUTER JOIN md2pc md2pc ON lot_control.model_pool_id = md2pc.model_pool_id"
                + "     RIGHT OUTER JOIN process_pool process_pool ON md2pc.process_pool_id_proc = process_pool.id_proc"
                + "     INNER JOIN details details ON lot_control.details_id_detail = details.id_detail"
                + "     INNER JOIN model_pool model_pool ON lot_control.model_pool_id = model_pool.id"
                + "     INNER JOIN refu01 refu01 ON details.id_detail = refu01.detail_id_detail"
                + " WHERE"
                + "     lot_control.details_id_detail = '" + getDetail().getIdDetail() + "' and "
                + " lot_control.barcode in (";
        if (getBarcode() != null) {
            for (blBarcode bc : getBarcode()) {
                if (bc.isSelected()) {
                    selectLot += "'" + bc.getBarcode() + "',";
                }
            }
        }
        System.out.println("id lot : " + selectLot);
        sql += selectLot;
        sql += "'') ORDER BY"
                + " lot_control.barcode ASC, "
                + " md2pc.sq_process ASC ;";
        if (!selectLot.equals("")) {
            System.out.println("SQL : " + sql);
            print("detail Id : "+getDetail().getIdDetail());
            if (new ReportBean().pdfSelect("/report/newcreateselected.jasper", "/report/" + getDetail().getIdDetail() + ".pdf", sql)) {
                System.out.println("complete");
                /*
                 * url ="window.open(\"http://www.w3schools.com\");";
                 * top.preview_html(
                 *
                 */
                url = " alert('Print');"
                        + " window.open('./report/" + getDetail().getIdDetail() + ".pdf');";
                print(" URL : " + url);
                return true;
            }
        }
        return false;

    }
    
    public List<blBarcode> getBarcode() {
        url = "";
        return barcode;
    }

    public Details getDetail() {
        return detail;
    }

    public static void setDetail(Details detail) {
        printassyworksheet.detail = detail;
    }

    private void print(String str) {
        System.out.println(str);
    }
}
