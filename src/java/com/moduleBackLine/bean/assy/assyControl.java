/**
 *
 */
package com.moduleBackLine.bean.assy;

import com.appCinfigpage.bean.userLogin;
import com.moduleBackLine.Dataworksheet.addAssypopupDetail;
import com.moduleBackLine.Dataworksheet.blBarcode;
import com.moduleBackLine.Dataworksheet.workSheetDetail;
import com.moduleBackLine.bean.printassyworksheet;
import com.moduleBackLine.popup.bean.addMaterail2assy;
import com.moduleFontline.DataWorksheet.detailProcess;
import com.tct.barcodegenerate.BarCodeGenerter;
import com.tct.data.*;
import com.tct.data.jpa.*;
import com.tct.data.jsf.util.PaginationHelper;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;

public class assyControl {

    private String stamp;
    private List<detailProcess> detailprocess = null;
    private List<SelectItem> modellist;
    private DetailAssy currentdetailassy;
    private Details currentdetail;
    private DetailAssyJpaController assyJpacontroller = null;
    private DetailsJpaController detailJpacontroller = null;
    private BarCodeGenerter barCodeGenerter = null;
    private List<SelectItem> modelserieslist = null;
    private int productqty;
    private final int lot = 4320;
    private ModelPool modelPool = null;
    private String pd_qty_error_ms;
    //============= search =====================================================
    private String ordernumber;
    private String typemodel;
    private String process;
    private String typebobbin;
    private String series;
    // =========================================================================

    public assyControl() {
//        loadProcess();
        this.searchWorksheet();
        printassyworksheet.barcode = null;
    }

    private void loadProcess() {
        print("=================================================================");
        try {
            detailprocess = new ArrayList<detailProcess>();
            List<Md2pc> md2pclist = getMd2pcJpaController().findBymodelPoolId(modelPool);
            for (Md2pc md2pc : md2pclist) {
                print("= md2pc : " + md2pc.getMd2pcId());
                ProcessPool pcpool = md2pc.getProcessPoolIdProc();
                print("= Process : " + pcpool.getProcName());
                detailProcess dp = new detailProcess("", null, null, null, null, "", pcpool.getProcName().toString(), "", 0, 0, 0, "", "");
                detailprocess.add(dp);
            }
        } catch (Exception e) {
            print("= Ex : " + e);
        }
    }
    private Md2pcJpaController md2pcJpaController = null;

    private Md2pcJpaController getMd2pcJpaController() {
        if (md2pcJpaController == null) {
            md2pcJpaController = new Md2pcJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return md2pcJpaController;
    }

    public void ChangModelValue(ValueChangeEvent event) {
        print("========== modelPool : " + event.getNewValue().toString() + " ======");
        try {
            String id = event.getNewValue().toString();
            modelPool = getModelPoolJpaController().findModelPool(id);
            print("= model name : "+modelPool.getModelName());
            loadProcess();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void ChangModelSeries(ValueChangeEvent event) {
        print("======== Find By model series ===================================");
        modellist = null;
        try {
            String modelseries = event.getNewValue().toString();
            print("= ModelSeries : " + modelseries);
            List<ModelPool> modelPoollList = getModelPoolJpaController().findModelPoolBySeries(modelseries);
            for (ModelPool modelpool : modelPoollList) {
                getModellist().add(new SelectItem(modelpool.getId(), modelpool.getModelName()));
            }
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public ModelPool getModelPool() {
        return modelPool;
    }

    public List<SelectItem> getModelserieslist() {
        if (modelserieslist == null) {
            modelserieslist = new ArrayList<SelectItem>();
            List<String> seriess = getModelPoolJpaController().findModelPoolGroupBySeries();
            for (String serie : seriess) {
                modelserieslist.add(ConvertToSelected(serie, serie));
            }
        }
        return modelserieslist;
    }

    public void ChangProductCodenameValue(ValueChangeEvent event) {
        print("====== chang product code name ==================================");
        print("Product Code Name : " + event.getNewValue());
        try {
            currentdetailassy.setPdCodename(event.getNewValue().toString());
            if (event.getNewValue().equals("Prototype")) {
                currentdetailassy.setPdCode("P");
            } else if (event.getNewValue().equals("New product introduction")) {
                currentdetailassy.setPdCode("N");
            } else if (event.getNewValue().equals("Special built request")) {
                currentdetailassy.setPdCode("S");
            } else if (event.getNewValue().equals("Mass production")) {
                currentdetailassy.setPdCode("M");
            }
//        currentdetailassy.setPdCodename(event.getNewValue().toString());
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void ChangProductTypeValue(ValueChangeEvent event) {
        System.out.println("Product Type : " + event.getNewValue());
        try {
            currentdetailassy.setPdType(event.getNewValue().toString());
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }
    private List<SelectItem> productcodename = null;
    private List<SelectItem> producttype = null;

    public List<SelectItem> getProductcodename() {
        if (productcodename == null) {
            productcodename = new ArrayList<SelectItem>();
            productcodename.add(new SelectItem("Prototype", "Prototype"));
            productcodename.add(new SelectItem("New product introduction", "New product introduction"));
            productcodename.add(new SelectItem("Special built request", "Special built request"));
            productcodename.add(new SelectItem("Mass production", "Mass production"));
        }
        return productcodename;
    }

    public List<SelectItem> getProducttype() {
        if (producttype == null) {
            producttype = new ArrayList<SelectItem>();
            producttype.add(new SelectItem("T", "Trigger coil"));
            producttype.add(new SelectItem("TR", "Tranformer"));
            producttype.add(new SelectItem("PCB", "Print circuit board"));
        }
        return producttype;
    }

    public DetailAssy getCurrentdetailassy() {
        if (currentdetailassy == null) {
            currentdetailassy = new DetailAssy();
        }
        return currentdetailassy;
    }

    public Details getCurrentdetail() {
        if (currentdetail == null) {
            currentdetail = new Details();
        }
        return currentdetail;
    }

    private DetailAssyJpaController getJpaDetailAssyControler() {
        if (assyJpacontroller == null) {
            assyJpacontroller = new DetailAssyJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return assyJpacontroller;
    }

    private DetailsJpaController getJpaDetailsController() {
        if (detailJpacontroller == null) {
            detailJpacontroller = new DetailsJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailJpacontroller;
    }

    public void changProductQty(ValueChangeEvent event) {
        productqty = Integer.parseInt(event.getNewValue().toString());
        long pdqty = productqty;
        System.out.println("New Value : " + productqty);
        int i = 0;
        while (lot < pdqty) {
            pdqty -= lot;
            i++;
        }
        if (pdqty > 0) {
            i++;
        }
        currentdetail.setLotcontrol("" + i);
    }

    private Users getUserlogin() {
        FacesContext context = FacesContext.getCurrentInstance();
        Users currentuser = (Users) context.getExternalContext().getSessionMap().get("currentUser");
        return currentuser;
    }

    private BarCodeGenerter getBarCodeGenerter() {
        if (barCodeGenerter == null) {
            barCodeGenerter = new BarCodeGenerter();
        }
        return barCodeGenerter;
    }

    private boolean genBarCode(String code, String filePath) {
        print("=================================================================");
        try {
            getBarCodeGenerter().createBarcode(code, filePath + "/1d/");
            getBarCodeGenerter().createQrCode(code, filePath + "/2d/");
            print("= GenCode : Complete");
            return true;
        } catch (Exception e) {
            print("= Error can not genbarcode." + e.toString());
            return false;
        }
    }

    public void createAssyline() {
        try {
            print("===================== Create assy line ======================");
            printassyworksheet.detail = null;
            List<addAssypopupDetail> addassypop = addMaterail2assy.assypopuplist;
            if (!addassypop.isEmpty() && addassypop != null) {
                currentdetail.setIdDetail("" + UUID.randomUUID());
                currentdetail.setDateCreate(new Date());
                currentdetail.setUserCreate(getUserlogin().getId());
                currentdetail.setDetailName("detail_assy");
                currentdetail.setDetailType("backline");
                currentdetail.setModel(modelPool.getId());

                currentdetailassy.setId("" + Math.random());
                currentdetailassy.setProductQty(productqty);
                currentdetailassy.setAssyDate(new Date());
                currentdetailassy.setRollNo("");
                currentdetailassy.setNumOnDate(0);
                currentdetailassy.setDetailIdDetail(currentdetail);
                getJpaDetailsController().create(currentdetail);
                getJpaDetailAssyControler().create(currentdetailassy);

                String partfile = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/images/barcode/");
                print(" PartFile : " + partfile);

                for (int jj = 0; jj < Integer.parseInt(currentdetail.getLotcontrol()); jj++) {
                    int maxnum = getBarCodeGenerter().getMaxnumberOfLotcontrol() + 1;
                    String barcode = "BAS" + currentdetailassy.getPdCode() + currentdetailassy.getPdType() + "-" + new SimpleDateFormat("MMyy").format(new Date()) + modelPool.getModelSeries() + maxnum;
                    print("= Barcode" + barcode);
                    getLotControl().setId(UUID.randomUUID().toString());
                    getLotControl().setBarcode(barcode);
                    getLotControl().setModelPoolId(modelPool);
                    getLotControl().setDetailsIdDetail(currentdetail);
                    getLotControl().setLotNumber((1+jj) + "");
                    if (genBarCode(barcode, partfile)) {
                        getLotControlJpaController().create(lotControl);
                        getBarcodelist().add(new blBarcode(getLotControl().getId(), barcode, "./images/barcode/", true));
                        print("= Barcode : " + barcode);
                    }
                    getBarCodeGenerter().updateMaxnumberOfLotcontrol(maxnum);
                }
                for (addAssypopupDetail addassy : addassypop) {
                    Front2back front2back = new Front2back();
                    front2back.setFront2backId(UUID.randomUUID().toString());
                    front2back.setDetailsIdDetail(currentdetail);
                    front2back.setLotcontrolId(addassy.getLotControl());
                    getFront2backJpaController().create(front2back);
//                    Thread.sleep(500);
                }
                printassyworksheet.detail = currentdetail;

//                JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("DetailAssyCreated"));
//                prepareCreate();
                Thread.sleep(3000);
                String link = "pages/moduleBackLine/assy/printview.xhtml";
                userLogin.getSessionMenuBarBean().setParam(link);
                print("= Complete.");
                popError = "";
//                return "";
            }
            print("canot insert");
            setPopError("Insert Material.");
//            return "";
        } catch (Exception e) {
            print("= Error : " + e);
//            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
//            return "";
        }
    }
    private String popError = "";

    public String getPopError() {
        return popError;
    }

    public void setPopError(String popError) {
        this.popError = popError;
    }
    private Front2backJpaController front2backJpaController = null;

    private Front2backJpaController getFront2backJpaController() {
        if (front2backJpaController == null) {
            front2backJpaController = new Front2backJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return front2backJpaController;
    }

    private void print(String str) {
        System.out.println(str);
    }

    private List<blBarcode> getBarcodelist() {
        if (printassyworksheet.barcode == null) {
            printassyworksheet.barcode = new ArrayList<blBarcode>();
        }
        return printassyworksheet.barcode;
    }
    private LotControlJpaController lotControlJpaController = null;

    private LotControlJpaController getLotControlJpaController() {
        if (lotControlJpaController == null) {
            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotControlJpaController;
    }
    private LotControl lotControl = null;

    private LotControl getLotControl() {
        if (lotControl == null) {
            lotControl = new LotControl();
        }
        return lotControl;
    }

    public String prepareCreate() {
        currentdetailassy = new DetailAssy();
        currentdetail = new Details();
        addMaterail2assy.assypopuplist = null;
//        selectedItemIndex = -1;
        return "Create";
    }

    /**
     * @return the stamp
     */
    public String getStamp() {
        return stamp;
    }

    /**
     * @param stamp the stamp to set
     */
    public void setStamp(String stamp) {
        this.stamp = stamp;
    }

    /**
     * @return the detailprocess
     */
    public List<detailProcess> getDetailprocess() {
        return detailprocess;
    }

    /**
     * @param detailprocess the detailprocess to set
     */
    public void setDetailprocess(List<detailProcess> detailprocess) {
        this.detailprocess = detailprocess;
    }

    public int getProductqty() {
        return productqty;
    }

    public void setProductqty(int productqty) {
        this.productqty = productqty;
    }

    private SelectItem ConvertToSelected(String id, String modelName) {
        return new SelectItem(id, modelName);
    }
    private ModelPoolJpaController modelpoolJpacontroller = null;

    private ModelPoolJpaController getModelPoolJpaController() {
        if (modelpoolJpacontroller == null) {
            modelpoolJpacontroller = new ModelPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return modelpoolJpacontroller;
    }

    public List<SelectItem> getModellist() {
        if (modellist == null) {
            modellist = new ArrayList<SelectItem>();
        }
        return modellist;
    }

    public void setModellist(List<SelectItem> modellist) {
        this.modellist = modellist;
    }

    public String getPd_qty_error_ms() {
        return pd_qty_error_ms;
    }

    public void setPd_qty_error_ms(String pd_qty_error_ms) {
        this.pd_qty_error_ms = pd_qty_error_ms;
    }

    public void changTocancel(ActionEvent event) {
        print("===================== go to list view ===========================");
        try {
//            worksheet = null;
            prepareCreate();
            searchWorksheet();
            String link = "pages/moduleBackLine/assy/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            print("= Go to : " + link);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changToInsert(ActionEvent event) {
        print("===================== go to create view =========================");
        try {
            currentdetail = null;
            currentdetailassy = null;
            String link = "pages/moduleBackLine/assy/createview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            print("= Go to : " + link);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changToEdit(ActionEvent event) {
        print("===================== go to edit view ===========================");
        try {


            String link = "pages/moduleBackLine/assy/editview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changToPrint(ActionEvent event) {
        print("================== print One assy worksheet =====================");
        try {
            printassyworksheet.barcode = null;
            workSheetDetail worksheetdetail = (workSheetDetail) getWorksheet().getRowData();
            print("= detail : " + worksheetdetail.getDetail());
            currentdetail = worksheetdetail.getDetail();
            currentdetailassy = getDetailAssyJpaController().findDetailFlByDetails(currentdetail);
            modelPool = getModelPoolJpaController().findModelPool(currentdetail.getModel());
            lotControl = getLotControlJpaController().findLotcontrolByBarcode(worksheetdetail.getBarcode()).get(0);
            getBarcodelist().add(new blBarcode(getLotControl().getId(), worksheetdetail.getBarcode(), "./images/barcode/", true));
            String link = "pages/moduleBackLine/assy/printview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }
    private List<workSheetDetail> worksheetlist;
    private DataModel worksheet;

    public DataModel getWorksheet() {
//        searchWorksheet();
        return worksheet;
    }

    public void setWorksheet() {
        this.worksheet = getPagination().createPageDataModel();
    }

    public List<workSheetDetail> getWorksheetlist() {
        if (worksheetlist == null) {
            worksheetlist = new ArrayList<workSheetDetail>();
        }
        return worksheetlist;
    }

    public void setWorksheetlist(List<workSheetDetail> worksheetlist) {
        this.worksheetlist = worksheetlist;
    }
    private PaginationHelper pagination;

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    return getWorksheetlist().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    print("PageFirst : " + getPageFirstItem());
                    print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (getWorksheetlist().size() > 0) {
                        listmodel = new ListDataModel(getWorksheetlist().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(getWorksheetlist());
                    }
                    return listmodel;
                }
            };
        }
        return pagination;
    }

    public void next() {
        getPagination().nextPage();
        setWorksheet();
    }

    public void previous() {
        getPagination().previousPage();
        setWorksheet();
    }

    public int getAllPages() {
        int all = getPagination().getItemsCount();
        int pagesize = getPagination().getPageSize();
        return ((int) all / pagesize) + 1;
    }

    public void searchWorksheet() {
        print("=================================================================");
        if (ordernumber != null && !ordernumber.trim().isEmpty()) {
            print("= Order Number :: " + ordernumber);
            searchByBarcode();
        } else if (process != null && !process.trim().isEmpty()) {
            print("= Process :: " + process);
            searchByProcess();
        } else if (typemodel != null && !typemodel.trim().isEmpty()) {
            print("= Type Model :: " + typemodel);
            searchByModel();
        } else {
            loadWorkSheet();
        }
    }
    private DetailsJpaController detailsJpaController = null;

    private DetailsJpaController getDetailsJpaController() {
        if (detailsJpaController == null) {
            detailsJpaController = new DetailsJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailsJpaController;
    }

    private void searchByModel() {
        worksheetlist = null;
        print("=================================================================");
        List<Details> detailslist = getDetailsJpaController().findDetailsByModelAndDetailNameAssy(typemodel);
        if (detailslist.isEmpty()) {
            getWorksheetlist();
        }
//        worksheet = new workSheetDetail[detailflList.size()];
        for (Details detail : detailslist) {
//            details = detailfl.getDetailIdDetail();
            lotControllist = getLotControlJpaController().findByDetailsId(detail);
            for (LotControl lotControl : lotControllist) {
                try {
                    if (lotControl.getMainDataId() != null) {
                        print("T");
                        MainData mainData = getMainDataJpaController().findMainData(lotControl.getMainDataId());
                        ProcessPool processPool = mainData.getIdProc();
                        CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(mainData.getCurrentNumber());
                        getWorksheetlist().add(new workSheetDetail(detail, lotControl.getBarcode(), lotControl.getBarcode(), lotControl.getModelPoolId().getModelName(), detail.getModelseries(), detail.getDateCreate(), processPool.getProcName(), currentProcess.getStatus()));
                        print("= Detail : " + detail.getDetailName() + " :: Barcode : " + lotControl.getBarcode());
                    } else {
                        getWorksheetlist().add(new workSheetDetail(detail, lotControl.getBarcode(), lotControl.getBarcode(), lotControl.getModelPoolId().getModelName(), detail.getModelseries(), detail.getDateCreate(), null, null));
                        print("Null");
                    }
                } catch (Exception e) {
                    print("= Error : " + e);
                }
            }
        }
        setWorksheet();
    }

    private void searchByBarcode() {
        worksheetlist = null;
        print("================ search by barcode ==============================");
        lotControllist = getLotControlJpaController().findLotcontrolByBarcode(ordernumber);
        if (lotControllist.isEmpty()) {
            getWorksheetlist();
        }
        for (LotControl lotcontrol : lotControllist) {
            currentdetail = lotcontrol.getDetailsIdDetail();
            if (currentdetail.getDetailName().equals("detail_assy")) {
                print("= Maindata id :: " + lotcontrol.getMainDataId());
                if (lotcontrol.getMainDataId() != null && !lotcontrol.getMainDataId().trim().isEmpty()) {
                    MainData maindata = getMainDataJpaController().findMainData(lotcontrol.getMainDataId());
                    ProcessPool pcpool = maindata.getIdProc();
//                DetailFl detailfl = getDetailFlJpacontroller().findDetailFlByDetails(detail);
                    CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(maindata.getCurrentNumber());
                    getWorksheetlist().add(new workSheetDetail(currentdetail, lotcontrol.getBarcode(), lotcontrol.getBarcode(), lotcontrol.getModelPoolId().getModelName(), currentdetail.getModelseries(), currentdetail.getDateCreate(), pcpool.getProcName(), currentProcess.getStatus()));
                    print("= Detail : " + currentdetail.getDetailName() + " :: Barcode : " + maindata.getLotControlId().getBarcode());
                } else {
                    getWorksheetlist().add(new workSheetDetail(currentdetail, lotcontrol.getBarcode(), lotcontrol.getBarcode(), lotcontrol.getModelPoolId().getModelName(), currentdetail.getModelseries(), currentdetail.getDateCreate(), "", ""));
                    print("= Detail : " + currentdetail.getDetailName() + " :: Barcode : " + lotcontrol.getBarcode());
                }
            }
        }
        setWorksheet();
    }

    private void searchByProcess() {
        worksheetlist = null;
        print("=================================================================");
        List<ProcessPool> pcpools = getProcessPoolJpaController().findProcessPoolByName(process);
        if (pcpools.isEmpty()) {
            getWorksheetlist();
        }
        for (ProcessPool pcpool : pcpools) {
            List<MainData> maindatas = getMainDataJpaController().findMainDataByProcess(pcpool);
            for (MainData maindata : maindatas) {
                LotControl lotControl = maindata.getLotControlId();
                Details detail = lotControl.getDetailsIdDetail();

                if (detail.getDetailName().equals("detail_assy")) {
//                    DetailAssy detailassy = getDetailAssyJpaController().findDetailFlByDetails(detail);
                    CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(maindata.getCurrentNumber());
                    getWorksheetlist().add(new workSheetDetail(detail, lotControl.getBarcode(), lotControl.getBarcode(), lotControl.getModelPoolId().getModelName(), currentdetail.getModelseries(), detail.getDateCreate(), pcpool.getProcName(), currentProcess.getStatus()));
                    print("= Detail : " + detail.getDetailName() + " :: Barcode : " + maindata.getLotControlId().getBarcode());
                }
            }
        }
        setWorksheet();
    }
    private List<DetailAssy> detailassylist;
    private List<LotControl> lotControllist;

    private void loadWorkSheet() {
        worksheetlist = null;
        print("=================================================================");
        detailassylist = getDetailAssyJpaController().findDetailAssyEntities();
        if (detailassylist.isEmpty()) {
            getWorksheetlist();
        }
        for (DetailAssy detailassy : detailassylist) {
            currentdetail = detailassy.getDetailIdDetail();
            lotControllist = getLotControlJpaController().findByDetailsId(currentdetail);
            for (LotControl lotControl : lotControllist) {
                try {
                    if (lotControl.getMainDataId() != null) {
                        print("T");
                        MainData mainData = getMainDataJpaController().findMainData(lotControl.getMainDataId());
                        ProcessPool processPool = mainData.getIdProc();
                        CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(mainData.getCurrentNumber());

                        getWorksheetlist().add(new workSheetDetail(currentdetail, lotControl.getBarcode(), lotControl.getBarcode(), lotControl.getModelPoolId().getModelName(), currentdetail.getModelseries(), currentdetail.getDateCreate(), processPool.getProcName(), currentProcess.getStatus()));
                        print("= Detail : " + currentdetail.getDetailName() + " :: Barcode : " + lotControl.getBarcode());
                    } else {
                        getWorksheetlist().add(new workSheetDetail(currentdetail, lotControl.getBarcode(), lotControl.getBarcode(), lotControl.getModelPoolId().getModelName(), currentdetail.getModelseries(), currentdetail.getDateCreate(), "", ""));
                        print("Null");
                    }
                } catch (Exception e) {
                    print("= Error : " + e);
                }
            }
        }
        setWorksheet();
    }
    private MainDataJpaController mianDataJpaController = null;

    private MainDataJpaController getMainDataJpaController() {
        if (mianDataJpaController == null) {
            mianDataJpaController = new MainDataJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mianDataJpaController;
    }
    private CurrentProcessJpaController currentProcesslist = null;

    private CurrentProcessJpaController getCurrentProcessJpaController() {
        if (currentProcesslist == null) {
            currentProcesslist = new CurrentProcessJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return currentProcesslist;
    }
    private DetailAssyJpaController detailAssyJpaController = null;

    private DetailAssyJpaController getDetailAssyJpaController() {
        if (detailAssyJpaController == null) {
            detailAssyJpaController = new DetailAssyJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailAssyJpaController;
    }
    private ProcessPoolJpaController processPoolJpaController = null;

    private ProcessPoolJpaController getProcessPoolJpaController() {
        if (processPoolJpaController == null) {
            processPoolJpaController = new ProcessPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return processPoolJpaController;
    }

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getTypebobbin() {
        return typebobbin;
    }

    public void setTypebobbin(String typebobbin) {
        this.typebobbin = typebobbin;
    }

    public String getTypemodel() {
        return typemodel;
    }

    public void setTypemodel(String typemodel) {
        this.typemodel = typemodel;
    }
}