/**
 *
 */
package com.moduleBackLine.bean;

import com.moduleBackLine.Dataworksheet.blBarcode;
import com.moduleFontline.DataWorksheet.detailProcess;
import com.tct.barcodegenerate.BarCodeGenerter;
import com.tct.data.*;
import com.tct.data.jpa.*;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import com.tct.data.jsf.util.JsfUtil;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;

/**
 * @author The_Boy_Cs
 *
 */
public class createAutoWorkSheet {

    private String auto_lotcontrol;
    private String auto_remark;
    private String pd_code;
    private String dr_core;
    private List<detailProcess> detailprocess = null;
    private List<SelectItem> productcodename = null;
    private List<SelectItem> producttype = null;
    private List<SelectItem> serieslist = null;
    private DetailAuto detailAuto = null;
    private DetailAutoJpaController jpadetailAutocontroller = null;
    private Details details = null;

    public createAutoWorkSheet() {
        
        printassyworksheet.barcode = null;
    }

     private void loadProcess() {
        print("=================================================================");
        try {
            detailprocess = new ArrayList<detailProcess>();
            List<Md2pc> md2pclist = getMd2pcJpaController().findBymodelPoolId(modelPool);
            for (Md2pc md2pc : md2pclist) {
                print("= md2pc : " + md2pc.getMd2pcId());
                ProcessPool pcpool = md2pc.getProcessPoolIdProc();
                print("= Process : " + pcpool.getProcName());
                detailProcess dp = new detailProcess("", null, null, null, null, "", pcpool.getProcName().toString(), "", 0, 0, 0, "", "");
                detailprocess.add(dp);
            }
        } catch (Exception e) {
            print("= Ex : " + e);
        }
    }
    private Md2pcJpaController md2pcJpaController = null;

    private Md2pcJpaController getMd2pcJpaController() {
        if (md2pcJpaController == null) {
            md2pcJpaController = new Md2pcJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return md2pcJpaController;
    }

    public void ChangValueModelList(ValueChangeEvent event) {
        print("=================================================================");
        print("= New value : " + event.getNewValue());
//        print("= ");
        String modelname = event.getNewValue().toString();
        List<ModelPool> models = getModelPoolJpaController().findByModelname(modelname);

        serieslist = new ArrayList<SelectItem>();

        for (ModelPool model : models) {
            serieslist.add(new SelectItem(model.getId(), model.getModelSeries()));
            print("= Series : " + model.getModelSeries());
        }
//        print("=================================================================");
    }

    public void ChangValueSeriesList(ValueChangeEvent event) {
        print("=================================================================");
        print("= Series new value : " + event.getNewValue());
        String model_id = event.getNewValue().toString();
        modelPool = getModelPoolJpaController().findModelPool(model_id);
        print("= Model : " + modelPool.getModelName());
        loadProcess();
    }

    private void print(String str) {
        System.out.println(str);
    }

    public List<SelectItem> getModellist() {
        List<ModelPool> lists = getModelPoolJpaController().findModelPoolGroupByName();
        List<SelectItem> models = new ArrayList<SelectItem>();
        for (ModelPool list : lists) {
            models.add(new SelectItem(list.getModelName(), list.getModelName()));
        }
        return models;
    }
    private ModelPoolJpaController modelPoolJpaController = null;

    private ModelPoolJpaController getModelPoolJpaController() {
        if (modelPoolJpaController == null) {
            modelPoolJpaController = new ModelPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return modelPoolJpaController;
    }

    public Details getDetails() {
        if (details == null) {
            details = new Details();
        }
        return details;
    }

    private void recriptDetailAuto() {
        details = new Details();
        detailAuto = new DetailAuto();
        lotControl = new LotControl();
    }

    public DetailAuto getDetailAuto() {
        if (detailAuto == null) {
            detailAuto = new DetailAuto();
        }
        return detailAuto;
    }

    private Users getUserlogin() {
        FacesContext context = FacesContext.getCurrentInstance();
        Users currentuser = (Users) context.getExternalContext().getSessionMap().get("currentUser");
        return currentuser;
    }

    private List<blBarcode> getBarcodelist() {
        if (printassyworksheet.barcode == null) {
            printassyworksheet.barcode = new ArrayList<blBarcode>();
        }
        return printassyworksheet.barcode;
    }
    
    public boolean CreateAutoworksheet(ActionEvent event) {
        print("=================================================================");
        print("Lotcontrol : " + auto_lotcontrol);
        int lot = Integer.parseInt(auto_lotcontrol);
        try {
            details.setLotcontrol(auto_lotcontrol);
            details.setIdDetail(UUID.randomUUID().toString());
            details.setDateCreate(new Date());
            details.setUserCreate(getUserlogin().getId());
            details.setDetailType("backline");
            details.setDetailName("detail_auto");
            details.setModel(modelPool.getModelName());

            detailAuto.setId(UUID.randomUUID().toString());
            detailAuto.setLotcontrolassy("");
            detailAuto.setDetailIdDetail(details);
            detailAuto.setProductCode(pd_code);
            detailAuto.setRemark(auto_remark);
            detailAuto.setDrCore(dr_core);

            getDetailsJpaController().create(details);
            getDetailAutoJpaController().create(detailAuto);

            for (int j = 0; j < lot; j++) {
                String barcode = GenbarCode(lot, j);
                getLotControl().setId(UUID.randomUUID().toString());
                getLotControl().setBarcode(barcode);
                getLotControl().setDetailsIdDetail(details);
                getLotControl().setLotNumber(j + "");
                getLotControl().setModelPoolId(modelPool);
                if (GenneratebarCode(barcode, "C:/tct_project_netbean/web/images/barcode/b/auto/")) {
                    getLotControlJpaController().create(getLotControl());
                    getBarcodelist().add(new blBarcode(getLotControl().getId(), barcode, "../images/barcode/b/auto/", true));
                    print("= Barcode : "+barcode);
                }
            }
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("DetailAssyCreated"));
            print("= Complete.");
            recriptDetailAuto();
            Thread.sleep(5000);
            return true;
        } catch (PreexistingEntityException prex) {
            JsfUtil.addErrorMessage(prex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            System.out.println("=Error : " + prex);
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            System.out.println("=Error2 : " + e);
        }
        return false;
    }
    private ModelPool modelPool = null;

    private ModelPool getModelPool() {
        if (modelPool == null) {
            modelPool = new ModelPool();
        }
        return modelPool;
    }
    private LotControl lotControl = null;

    private LotControl getLotControl() {
        if (lotControl == null) {
            lotControl = new LotControl();
        }
        return lotControl;
    }
    private LotControlJpaController lotControlJpaController = null;

    private LotControlJpaController getLotControlJpaController() {
        if (lotControlJpaController == null) {
            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotControlJpaController;
    }
    private DetailAutoJpaController detailAutoJpaController = null;
    private DetailsJpaController detailsJpaController = null;

    private DetailsJpaController getDetailsJpaController() {
        if (detailsJpaController == null) {
            detailsJpaController = new DetailsJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailsJpaController;
    }

    private DetailAutoJpaController getDetailAutoJpaController() {
        if (detailAutoJpaController == null) {
            detailAutoJpaController = new DetailAutoJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailAutoJpaController;
    }
    private BarCodeGenerter barCodeGenerter = null;

    private BarCodeGenerter getBarCodeGenerter() {
        if (barCodeGenerter == null) {
            barCodeGenerter = new BarCodeGenerter();
        }
        return barCodeGenerter;
    }

    private boolean GenneratebarCode(String barcode, String filePath) {
        try {
            getBarCodeGenerter().createBarcode(barcode, filePath + "b/");
            getBarCodeGenerter().createQrCode(barcode, filePath);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private String GenbarCode(int lot, int i) {
        String bc = "BAU" + detailAuto.getProductCode() + detailAuto.getProductType() + "-" + getBarCodeGenerter().UseNumberToString(lot)
                + new SimpleDateFormat("MM").format(new Date()) + "" + new SimpleDateFormat("yy").format(new Date()) + modelPool.getModelSeries()
                + getBarCodeGenerter().UseNumberToString(i);
        return bc;
    }

    public void ChangProductCodenameValue(ValueChangeEvent event) {
        print("=================================================================");
        if (event.getNewValue().equals("Prototype")) {
            pd_code = "P";
//            detailAuto.setProductCode("P");
        } else if (event.getNewValue().equals("New product introduction")) {
            pd_code = "N";
//            detailAuto.setProductCode("N");
        } else if (event.getNewValue().equals("Special built request")) {
            pd_code = "S";
//            detailAuto.setProductCode("S");
        } else if (event.getNewValue().equals("Mass production")) {
            pd_code = "M";
//            detailAuto.setProductCode("M");
        }
        detailAuto.setProductCodeName(event.getNewValue().toString());
        print("= Product name : " + event.getNewValue());
        System.out.println("= Product Code : " + pd_code);
    }

    public List<SelectItem> getProductcodename() {
        if (productcodename == null) {
            productcodename = new ArrayList<SelectItem>();
            productcodename.add(new SelectItem("Prototype", "Prototype"));
            productcodename.add(new SelectItem("New product introduction", "New product introduction"));
            productcodename.add(new SelectItem("Special built request", "Special built request"));
            productcodename.add(new SelectItem("Mass production", "Mass production"));
        }
        return productcodename;
    }

    public List<SelectItem> getProducttype() {
        if (producttype == null) {
            producttype = new ArrayList<SelectItem>();
            producttype.add(new SelectItem("T", "Trigger coil"));
            producttype.add(new SelectItem("TR", "Tranformer"));
            producttype.add(new SelectItem("PCB", "Print circuit board"));
        }
        return producttype;
    }

    public void ChangProductTypeValue(ValueChangeEvent event) {
        print("=================================================================");
        print("Product Type : " + event.getNewValue());
        detailAuto.setProductType(event.getNewValue().toString());
    }

    /**
     * @return the auto_lotcontrol
     */
    public String getAuto_lotcontrol() {
        return auto_lotcontrol;
    }

    /**
     * @param auto_lotcontrol the auto_lotcontrol to set
     */
    public void setAuto_lotcontrol(String auto_lotcontrol) {
        this.auto_lotcontrol = auto_lotcontrol;
    }

    public String getAuto_remark() {
        return auto_remark;
    }

    public void setAuto_remark(String auto_remark) {
        this.auto_remark = auto_remark;
    }

    public String getPd_code() {
        return pd_code;
    }

    public void setPd_code(String pd_code) {
        this.pd_code = pd_code;
    }

    /**
     * @return the detailprocess
     */
    public List<detailProcess> getDetailprocess() {
        return detailprocess;
    }

    /**
     * @param detailprocess the detailprocess to set
     */
    public void setDetailprocess(List<detailProcess> detailprocess) {
        this.detailprocess = detailprocess;
    }

    public List<SelectItem> getSerieslist() {
        if (serieslist == null) {
            serieslist = new ArrayList<SelectItem>();
        }
        return serieslist;
    }

    public String getDr_core() {
        return dr_core;
    }

    public void setDr_core(String dr_core) {
        this.dr_core = dr_core;
    }
    
}
