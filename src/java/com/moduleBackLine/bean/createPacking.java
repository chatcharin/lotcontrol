/**
 *
 */
package com.moduleBackLine.bean;

import com.moduleBackLine.Dataworksheet.blBarcode;
import com.moduleFontline.DataWorksheet.detailProcess;
import com.tct.barcodegenerate.BarCodeGenerter;
import com.tct.data.*;
import com.tct.data.jpa.*;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import com.tct.data.jsf.util.JsfUtil;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;

/**
 * @author The_Boy_Cs
 *
 */
public class createPacking {

    private String pack_lotcontrol;
    private String pack_model;
    private String pack_line;
    private String pack_stamp;
    private List<detailProcess> detailprocess = null;
    private DetailPacking detailPacking = null;
    private Details details = null;

    public createPacking() {
        printassyworksheet.barcode = null;
    }

     private void loadProcess() {
        print("=================================================================");
        try {
            detailprocess = new ArrayList<detailProcess>();
            List<Md2pc> md2pclist = getMd2pcJpaController().findBymodelPoolId(modelPool);
            for (Md2pc md2pc : md2pclist) {
                print("= md2pc : " + md2pc.getMd2pcId());
                ProcessPool pcpool = md2pc.getProcessPoolIdProc();
                print("= Process : " + pcpool.getProcName());
                detailProcess dp = new detailProcess("", null, null, null, null, "", pcpool.getProcName().toString(), "", 0, 0, 0, "", "");
                detailprocess.add(dp);
            }
        } catch (Exception e) {
            print("= Ex : " + e);
        }
    }
    private Md2pcJpaController md2pcJpaController = null;

    private Md2pcJpaController getMd2pcJpaController() {
        if (md2pcJpaController == null) {
            md2pcJpaController = new Md2pcJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return md2pcJpaController;
    }
    
    private BarCodeGenerter barCodeGenerter = null;

    private BarCodeGenerter getBarCodeGenerter() {
        if (barCodeGenerter == null) {
            barCodeGenerter = new BarCodeGenerter();
        }
        return barCodeGenerter;
    }

    private boolean GenneratebarCode(String barcode, String filePath) {
        try {
            print("=============================================================");
            print("= Barcode : "+barcode);
            getBarCodeGenerter().createBarcode(barcode, filePath + "b/");
            getBarCodeGenerter().createQrCode(barcode, filePath);
            print("= Complete.");
            return true;
        } catch (Exception e) {
            print("= Error : " + e);
            return false;
        }
    }

    private String GenbarCode(int lot, int i) {
        String bc = "BP" + pdCode + pdType + "-" + getBarCodeGenerter().UseNumberToString(lot)
                + new SimpleDateFormat("MM").format(new Date()) + "" + new SimpleDateFormat("yy").format(new Date())
                + modelPool.getModelSeries()+ getBarCodeGenerter().UseNumberToString(i);
        return bc;
    }
    private String pdCode;
    private String pdType;
    private String pdCodename;

    public void ChangProductTypeValue(ValueChangeEvent event) {
        print("=================================================================");
        pdType = event.getNewValue().toString();
        print("= Product Type : " + pdType);
    }

    public void ChangProductCodenameValue(ValueChangeEvent event) {
        print("=================================================================");
        if (event.getNewValue().equals("Prototype")) {
            pdCode = "P";
        } else if (event.getNewValue().equals("New product introduction")) {
            pdCode = "N";
        } else if (event.getNewValue().equals("Special built request")) {
            pdCode = "S";
        } else if (event.getNewValue().equals("Mass production")) {
            pdCode = "M";
        }
        pdCodename = event.getNewValue().toString();
        print("= Product code name : " + pdCodename);
        print("= Product code : " + pdCode);
    }

    public boolean createPacking(ActionEvent event) {
        print("=================================================================");
        try {
            getDetails().setIdDetail(UUID.randomUUID().toString());
            getDetails().setLine(pack_line);
            getDetails().setLotcontrol(pack_lotcontrol);
            getDetails().setDateCreate(new Date());
            getDetails().setModel(pack_model);
            getDetails().setUserCreate(getUserlogin().getId());
            getDetails().setDetailType("backline");
            getDetails().setDetailName("detail_packing");

            getDetailPacking().setId(UUID.randomUUID().toString());
            getDetailPacking().setPdCode(pdCode);
            getDetailPacking().setPdCodeName(pdCodename);
            getDetailPacking().setPdType(pdType);
            getDetailPacking().setDetailsIdDetail(getDetails());
            getDetailPacking().setStamp(pack_stamp);
            int lot = Integer.parseInt(pack_lotcontrol);

            getDetailsJpaController().create(getDetails());
            getDetailPackingJpaController().create(getDetailPacking());

            for (int i = 0; i < lot; i++) {
                getLotControl().setId(UUID.randomUUID().toString());
                String barcode = GenbarCode(lot, i);
                getLotControl().setBarcode(barcode);
                getLotControl().setDetailsIdDetail(getDetails());
                getLotControl().setLotNumber("" + i);
                getLotControl().setModelPoolId(modelPool);
                if (GenneratebarCode(barcode, "C:/tct_project_netbean/web/images/barcode/b/packing/")) {
                    getLotControlJpaController().create(lotControl);
                    getBarcodelist().add(new blBarcode(getLotControl().getId(), barcode, "../images/barcode/b/packing/", true));
                    print("= Barcode : " + barcode);
                }
            }
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("DetailAssyCreated"));
            print("= Insert Complete.");
            Thread.sleep(5000);
            return true;
        } catch (PreexistingEntityException ptex) {
            JsfUtil.addErrorMessage(ptex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            print("= Error : " + ptex);
            return false;
        } catch (Exception ex) {
            JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            print("= Error2 : " + ex);
            return false;
        }
    }
    private List<blBarcode> getBarcodelist() {
        if (printassyworksheet.barcode == null) {
            printassyworksheet.barcode = new ArrayList<blBarcode>();
        }
        return printassyworksheet.barcode;
    }
    private ModelPool modelPool = null;

    private ModelPool getModelPool() {
        if (modelPool == null) {
            modelPool = new ModelPool();
        }
        return modelPool;
    }
    private List<SelectItem> modelSeries = null;

    public List<SelectItem> getModelSeries() {
        if (modelSeries == null) {
            modelSeries = new ArrayList<SelectItem>();
        }
        return modelSeries;
    }

    public void ChangModelSeriesValue(ValueChangeEvent event) {
        print("=================================================================");
        String modelId = event.getNewValue().toString();
        print("= Model id : " + modelId);
        try {
            modelPool = getModelPoolJpaController().findModelPool(modelId);
            print("= Model : " + modelPool.getModelName());
            loadProcess();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void ChangModelPoolvalue(ValueChangeEvent event) {
        print("=================================================================");
        try {
            pack_model = event.getNewValue().toString();
            print("= Model : " + pack_model);
            modelSeries = new ArrayList<SelectItem>();
            List<ModelPool> models = getModelPoolJpaController().findByModelname(pack_model);
            for (ModelPool model : models) {
                modelSeries.add(new SelectItem(model.getId(), model.getModelSeries()));
                print("= Series : " + model.getModelSeries());
            }
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public List<SelectItem> getModelPoollist() {
        print("=================================================================");
        try {
            List<ModelPool> modelpools = getModelPoolJpaController().findModelPoolGroupByName();
            List<SelectItem> modelPoollist = new ArrayList<SelectItem>();
            for (ModelPool model : modelpools) {
                modelPoollist.add(new SelectItem(model.getModelName(), model.getModelName()));
            }
            return modelPoollist;
        } catch (Exception e) {
            print("= Error : " + e);
            return new ArrayList<SelectItem>();
        }
    }
    private ModelPoolJpaController modelPoolJpaController = null;

    private ModelPoolJpaController getModelPoolJpaController() {
        if (modelPoolJpaController == null) {
            modelPoolJpaController = new ModelPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return modelPoolJpaController;
    }
    private DetailPackingJpaController detailPackingJpaController = null;

    private DetailPackingJpaController getDetailPackingJpaController() {
        if (detailPackingJpaController == null) {
            detailPackingJpaController = new DetailPackingJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailPackingJpaController;
    }
    private LotControlJpaController lotControlJpaController = null;

    private LotControlJpaController getLotControlJpaController() {
        if (lotControlJpaController == null) {
            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotControlJpaController;
    }
    private LotControl lotControl = null;

    private LotControl getLotControl() {
        if (lotControl == null) {
            lotControl = new LotControl();
        }
        return lotControl;
    }
    private DetailsJpaController detailsJpaController = null;

    private DetailsJpaController getDetailsJpaController() {
        if (detailsJpaController == null) {
            detailsJpaController = new DetailsJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailsJpaController;
    }

    private Users getUserlogin() {
        FacesContext context = FacesContext.getCurrentInstance();
        Users currentuser = (Users) context.getExternalContext().getSessionMap().get("currentUser");
        return currentuser;
    }

    private void print(String str) {
        System.out.println(str);
    }

    public String getPack_lotcontrol() {
        return pack_lotcontrol;
    }

    public void setPack_lotcontrol(String pack_lotcontrol) {
        this.pack_lotcontrol = pack_lotcontrol;
    }

    public String getPack_model() {
        return pack_model;
    }

    public void setPack_model(String pack_model) {
        this.pack_model = pack_model;
    }

    public String getPack_line() {
        return pack_line;
    }

    public void setPack_line(String pack_line) {
        this.pack_line = pack_line;
    }

    public String getPack_stamp() {
        return pack_stamp;
    }

    public void setPack_stamp(String pack_stamp) {
        this.pack_stamp = pack_stamp;
    }

    public List<detailProcess> getDetailprocess() {
        return detailprocess;
    }

    public void setDetailprocess(List<detailProcess> detailprocess) {
        this.detailprocess = detailprocess;
    }

    public DetailPacking getDetailPacking() {
        if (detailPacking == null) {
            detailPacking = new DetailPacking();
        }
        return detailPacking;
    }

    public Details getDetails() {
        if (details == null) {
            details = new Details();
        }
        return details;
    }

    public String getPdCode() {
        return pdCode;
    }

    public void setPdCode(String pdCode) {
        this.pdCode = pdCode;
    }
}
