/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moduleBackLine.bean.packing;

import com.appCinfigpage.bean.userLogin;
import com.moduleBackLine.Dataworksheet.blBarcode;
import com.moduleBackLine.Dataworksheet.workSheetDetail;
import com.moduleBackLine.bean.printassyworksheet;
import com.moduleBackLine.popup.bean.popupAssembly2Packing;
import com.moduleFontline.DataWorksheet.detailProcess;
import com.tct.barcodegenerate.BarCodeGenerter;
import com.tct.data.*;
import com.tct.data.jpa.*;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jsf.util.PaginationHelper;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;

/**
 *
 * @author The_Boy_Cs
 */
public class packingControl {
    //===== Search =============================================================

    private String barcodeSearch;
    private String processSearch;
    private String modelSearch;
    private String seriesSearch;
//==============================================================================
    private DataModel worksheet;
    private List<workSheetDetail> worksheetlist;
    private List<DetailPacking> detailpackinglist;
    private Details currentdetails;
    private List<LotControl> lotControllist;
    private ModelPool currentModelPool;
    private List<String> modelPools;
    private List<ModelPool> modelPoolslist;
    private List<SelectItem> modelpoollist;
    private List<detailProcess> detailprocess;
    private DetailPacking currentdetailpacking;
    private List<SelectItem> productcodename;
    private List<SelectItem> producttype;
    private LotControl currentLotControl;
    private LotControl poplotControl;
    private String errorrequerpdt = "";
    private String errorrequerpd = "";
    private String errorrequermodelseries = "";
    private String errorrequermodel = "";
//========================= for popup ==========================================
    private List<popupAssembly2Packing> assembly2Packingslist;
    private String popupbarcode;
    private int popQty;
    private int popWip;
    private boolean popvisible = false;
    private Back2pack currentBack2pack;
    private String poperror = "";
//==============================================================================

    public packingControl() {
        searchPackingWorksheet();
    }

    public void changToSave(ActionEvent event) {
        print("=============== Save for edit ==================================");
        try {
            lotControllist = getLotControlJpaController().findByDetailsId(currentdetails);
            getDetailPackingJpaController().edit(currentdetailpacking);
            getDetailsJpaController().edit(currentdetails);
            for (LotControl lotControl : lotControllist) {
                lotControl.setModelPoolId(currentModelPool);
                getLotControlJpaController().edit(lotControl);
            }
            String link = "pages/moduleBackLine/packing/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            searchPackingWorksheet();
        } catch (Exception e) {
            print(" Error save : " + e);
        }
    }

    public void changToExtracQty(ActionEvent event) {
        print("===================== ExtracQty =================================");
        assembly2Packingslist = null;
        try {
            int loop = Math.round(popWip / 2000);
            print("= loop : " + loop);
            getCurrentdetails().setLotcontrol("" + loop);
            for (int i = 0; i < loop; i++) {
                int maxnum = getBarcodeGenerater().getMaxnumberOfLotcontrol() + 1;
                String barcode = "BP" + getCurrentdetailpacking().getPdCode() + getCurrentdetailpacking().getPdType() + "-"
                        + new SimpleDateFormat("MMyy").format(new Date()) + currentModelPool.getModelSeries() + maxnum;
                print("= Barcode : " + barcode);
                popWip = popWip - 2000;
                getAssembly2Packingslist().add(new popupAssembly2Packing(poplotControl, barcode, 2000));
                
                getBarcodeGenerater().updateMaxnumberOfLotcontrol(maxnum);
            }
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void addMatrial(ActionEvent event) {
        print("================ Add Matrial ====================================");
        try {
            if (checkRequer()) {
                popvisible = true;
                poperror = "";
            }
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changToSaveMatrail(ActionEvent event) {
        print("==================== Save matrail ===============================");
        try {
            if (assembly2Packingslist != null && !assembly2Packingslist.isEmpty()) {
                popvisible = false;
                poperror = "";
            } else {
                popvisible = true;
                poperror = "Gennerate code!";
            }
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changTocancelAddmatrail(ActionEvent event) {
        print("===================== Cancel admatrial ==========================");
        try {
            popQty = 0;
            popWip = 0;
            popupbarcode = "";
            assembly2Packingslist = null;
            popvisible = false;
            poperror = "";
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changToEdit(ActionEvent event) {
        print("====================== Edit =====================================");
        try {
            modelpoollist = null;
            detailprocess = null;
            workSheetDetail workSheetDetails = (workSheetDetail) getWorksheet().getRowData();
            currentLotControl = getLotControlJpaController().findLotcontrolByBarcode(workSheetDetails.getBarcode()).get(0);
            currentdetails = currentLotControl.getDetailsIdDetail();
            currentModelPool = currentLotControl.getModelPoolId();
            print("= Modelpool : " + currentModelPool.getModelName());
            print("= modelpool series : " + currentModelPool.getModelSeries());
            currentdetailpacking = getDetailPackingJpaController().findDetialPackingList(currentdetails).get(0);
            print("= detail modelseries : " + currentdetails.getModelseries());
            List<ModelPool> modelpools = getModelPoolJpaController().findModelPoolBySeries(currentdetails.getModelseries());

            for (ModelPool modelpool : modelpools) {
                getModelpoollist().add(new SelectItem(modelpool.getId(), modelpool.getModelName()));
                print("= modelpool : " + modelpool.getModelName());
            }

            List<Md2pc> md2pcs = getMd2pcJpaController().findBymodelPoolId(currentModelPool);
            for (Md2pc md2pc : md2pcs) {
                getDetailprocess().add(new detailProcess(md2pc.getProcessPoolIdProc().getCodeOperation(), null, null, null, null, "", md2pc.getProcessPoolIdProc().getProcName(), "", 0, 0, 0, "", ""));
            }
            String link = "pages/moduleBackLine/packing/editview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            print("= GotoEdit : " + link);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changTodelAll(ActionEvent event) {
        print("====================== Del All ==================================");
        try {
            lotControllist = getLotControlJpaController().findByDetailsId(currentdetails);
            currentdetails.setDeleted(1);
            getDetailsJpaController().edit(currentdetails);
            currentdetailpacking.setDeleted(1);
            getDetailPackingJpaController().edit(currentdetailpacking);
            for (LotControl lotcontrol : lotControllist) {
                lotcontrol.setDeleted(1);
                getLotControlJpaController().edit(lotcontrol);
                print("= Deleted : Lot " + lotcontrol.getId());
            }
            String link = "pages/moduleBackLine/assembly/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            print("= Deleted Complete.");
            currentdetailpacking = null;
            currentdetails = null;
            lotControllist = null;
            searchPackingWorksheet();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changToDelOne(ActionEvent event) {
        print("============= Del One selected ==================================");
        try {
            workSheetDetail workSheetDetails = (workSheetDetail) getWorksheet().getRowData();
            currentLotControl = getLotControlJpaController().findLotcontrolByBarcode(workSheetDetails.getBarcode()).get(0);
            currentLotControl.setDeleted(1);
            getLotControlJpaController().edit(currentLotControl);
            print("= Del completed.");
            currentLotControl = null;
            searchPackingWorksheet();
        } catch (NonexistentEntityException nex) {
            print("Error : " + nex);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void create(ActionEvent event) {
        print("======================= Create ==================================");
        printassyworksheet.barcode = null;
        printassyworksheet.detail = null;
        try {
            Users user = userLogin.getSessionUser();
            getCurrentdetails().setIdDetail(UUID.randomUUID().toString());
            getCurrentdetails().setDateCreate(new Date());
            getCurrentdetails().setDeleted(0);
            getCurrentdetails().setDetailName("detail_packing");
            getCurrentdetails().setDetailType("backline");
            getCurrentdetails().setUserCreate(user.getId());

            getCurrentdetailpacking().setDetailsIdDetail(getCurrentdetails());
            getCurrentdetailpacking().setId(UUID.randomUUID().toString());

            getDetailsJpaController().create(currentdetails);
            getDetailPackingJpaController().create(currentdetailpacking);
            currentLotControl = null;
//            int lot = Integer.parseInt(getCurrentdetails().getLotcontrol());
            String partfile = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/images/barcode/");
            print(" PartFile : " + partfile);
            print(" assp Size : "+assembly2Packingslist.size());
            int i = 0;
            for (popupAssembly2Packing a2p : assembly2Packingslist) {
//                int maxnum = getBarcodeGenerater().getMaxnumberOfLotcontrol() + 1;
//                String barcode = "BP" + getCurrentdetailpacking().getPdCode() + getCurrentdetailpacking().getPdType() + "-"
//                        + new SimpleDateFormat("MMyy").format(new Date()) + currentModelPool.getModelSeries() + maxnum;
                String barcode = a2p.getPackingBarcode();
                getCurrentLotControl().setId(UUID.randomUUID().toString());
                getCurrentLotControl().setBarcode(barcode);
                getCurrentLotControl().setDeleted(0);
                getCurrentLotControl().setDetailsIdDetail(currentdetails);
                getCurrentLotControl().setLotNumber((i +1)+ "");
                getCurrentLotControl().setModelPoolId(currentModelPool);

                if (genBarCode(barcode, partfile)) {
                    getLotControlJpaController().create(currentLotControl);
                    getBarcodelist().add(new blBarcode(getCurrentLotControl().getId(), barcode, "./images/barcode/", true));
                    print("= Barcode : " + barcode);

                    getCurrentBack2pack().setBack2packId(UUID.randomUUID().toString());
                    getCurrentBack2pack().setCreatedate(new Date());
                    getCurrentBack2pack().setDetailsIdDetail(currentdetails);
                    getCurrentBack2pack().setLotcontrolId(currentLotControl);
                    getCurrentBack2pack().setQty(a2p.getQty() + "");

                    getBack2packJpaController().create(currentBack2pack);
                    print("= Insert Lotcontrol : " + currentLotControl);
                    print("= Insert currentBack2pack : " + currentBack2pack);
//                    currentBack2pack = null;
//                    currentLotControl = null;
                }
                i++;
            }
            printassyworksheet.detail = currentdetails;
            print("== ControlWip : " + poplotControl.getWipQty());
            print("== PopWip : " + popWip);
            poplotControl.setWipQty(popWip);
            getLotControlJpaController().edit(poplotControl);
            Thread.sleep(1000);
            String link = "pages/moduleBackLine/packing/printview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);

            assembly2Packingslist = null;
            popupbarcode = "";

//            searchPackingWorksheet();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void searchPackingWorksheet() {
        print("==================== Search worksheet Assembly ==================");
        worksheet = null;
        try {
            if (barcodeSearch != null && !barcodeSearch.trim().isEmpty()) {
                print("= Order Number :: " + barcodeSearch);
                searchByBarcode();
            } else if (processSearch != null && !processSearch.trim().isEmpty()) {
                print("= Process :: " + processSearch);
                searchByProcess();
            } else if (modelSearch != null && !modelSearch.trim().isEmpty()) {
                print("= Type Model :: " + modelSearch);
                searchByModel();
            } else {
                loadWorkSheet();
            }
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changToPrint(ActionEvent event) {
        print("==================== Print one ==================================");
        try {
            modelpoollist = null;
            detailprocess = null;
            printassyworksheet.barcode = null;
            printassyworksheet.detail = null;
            workSheetDetail workSheetDetails = (workSheetDetail) getWorksheet().getRowData();
            currentLotControl = getLotControlJpaController().findLotcontrolByBarcode(workSheetDetails.getBarcode()).get(0);
            currentdetails = currentLotControl.getDetailsIdDetail();
            currentModelPool = currentLotControl.getModelPoolId();
            currentdetailpacking = getDetailPackingJpaController().findDetialPackingList(currentdetails).get(0);
            getBarcodelist().add(new blBarcode(currentLotControl.getId(), currentLotControl.getBarcode(), "./images/barcode/", true));
            printassyworksheet.detail = currentdetails;
            String link = "pages/moduleBackLine/packing/printview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            print("= Printview. : Link : " + link);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    private void searchByModel() {
        worksheetlist = null;
        print("===================== Search By Model ===========================");
//        List<Details> detailslist = getDetailsJpaController().findDetailsByModelAndDetailNameAuto(modelSearch);
        List<ModelPool> modelPoolslist = getModelPoolJpaController().findByModelname(modelSearch);

        if (modelPoolslist.isEmpty()) {
            getWorksheetlist();
        }
        for (ModelPool modelpool : modelPoolslist) {
            lotControllist = (List<LotControl>) modelpool.getLotControlCollection();
            for (LotControl lotControl : lotControllist) {
                try {
                    if (lotControl.getDeleted() == 0) {
                        currentdetails = lotControl.getDetailsIdDetail();
                        if (currentdetails.getDetailName().equals("detail_packing") && currentdetails.getDeleted() == 0) {
                            if (lotControl.getMainDataId() != null) {
                                print("T");
                                MainData mainData = getMainDataJpaController().findMainData(lotControl.getMainDataId());
                                ProcessPool processPool = mainData.getIdProc();
                                CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(mainData.getCurrentNumber());
                                getWorksheetlist().add(new workSheetDetail(currentdetails, lotControl.getBarcode(), lotControl.getBarcode(), lotControl.getModelPoolId().getModelName(), lotControl.getModelPoolId().getModelSeries(), currentdetails.getDateCreate(), processPool.getProcName(), currentProcess.getStatus()));
                                print("= Detail : " + currentdetails.getDetailName() + " :: Barcode : " + lotControl.getBarcode());
                            } else {
                                getWorksheetlist().add(new workSheetDetail(currentdetails, lotControl.getBarcode(), lotControl.getBarcode(), lotControl.getModelPoolId().getModelName(), lotControl.getModelPoolId().getModelSeries(), currentdetails.getDateCreate(), null, null));
                                print("Null");
                            }
                        }
                    }
                } catch (Exception e) {
                    print("= Error : " + e);
                }
            }
        }
        setWorksheet();
    }

    private void searchByProcess() {
        worksheetlist = null;
        print("=================================================================");
        List<ProcessPool> pcpools = getProcessPoolJpaController().findProcessPoolByName(processSearch);
        if (pcpools.isEmpty()) {
            getWorksheetlist();
        }
        for (ProcessPool pcpool : pcpools) {
            List<MainData> maindatas = getMainDataJpaController().findMainDataByProcess(pcpool);
            for (MainData maindata : maindatas) {
                currentLotControl = maindata.getLotControlId();
                currentdetails = currentLotControl.getDetailsIdDetail();
                if (currentdetails.getDetailName().equals("detail_packing")) {
                    CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(maindata.getCurrentNumber());
                    getWorksheetlist().add(new workSheetDetail(currentdetails, currentLotControl.getBarcode(), currentLotControl.getBarcode(), currentLotControl.getModelPoolId().getModelName(), currentLotControl.getModelPoolId().getModelSeries(), currentdetails.getDateCreate(), pcpool.getProcName(), currentProcess.getStatus()));
                    print("= Detail : " + currentdetails.getDetailName() + " :: Barcode : " + maindata.getLotControlId().getBarcode());
                }
            }
        }
        setWorksheet();
    }

    private void searchByBarcode() {
        worksheetlist = null;
        print("================ search by barcode ==============================");
        lotControllist = getLotControlJpaController().findLotcontrolByBarcode(barcodeSearch);
        if (lotControllist.isEmpty()) {
            getWorksheetlist();
        }
        for (LotControl lotcontrol : lotControllist) {
            currentdetails = lotcontrol.getDetailsIdDetail();
            if (currentdetails.getDetailName().equals("detail_packing")) {
                print("= Maindata id :: " + lotcontrol.getMainDataId());
                if (lotcontrol.getMainDataId() != null && !lotcontrol.getMainDataId().trim().isEmpty()) {
                    MainData maindata = getMainDataJpaController().findMainData(lotcontrol.getMainDataId());
                    ProcessPool pcpool = maindata.getIdProc();
                    CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(maindata.getCurrentNumber());
                    getWorksheetlist().add(new workSheetDetail(currentdetails, lotcontrol.getBarcode(), lotcontrol.getBarcode(), lotcontrol.getModelPoolId().getModelName(), lotcontrol.getModelPoolId().getModelSeries(), currentdetails.getDateCreate(), pcpool.getProcName(), currentProcess.getStatus()));
                    print("= Detail : " + currentdetails.getDetailName() + " :: Barcode : " + maindata.getLotControlId().getBarcode());
                } else {
                    getWorksheetlist().add(new workSheetDetail(currentdetails, lotcontrol.getBarcode(), lotcontrol.getBarcode(), lotcontrol.getModelPoolId().getModelName(), lotcontrol.getModelPoolId().getModelSeries(), currentdetails.getDateCreate(), "", ""));
                    print("= Detail : " + currentdetails.getDetailName() + " :: Barcode : " + lotcontrol.getBarcode());
                }
            }
        }
        setWorksheet();
    }
    private ProcessPoolJpaController processPoolJpaController;

    public ProcessPoolJpaController getProcessPoolJpaController() {
        if (processPoolJpaController == null) {
            processPoolJpaController = new ProcessPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return processPoolJpaController;
    }

    public void changModelseriesValue(ValueChangeEvent event) {
        print("================== Series chang value ===========================");
        modelpoollist = null;
        detailprocess = null;
        try {
            String series = event.getNewValue().toString();
            print("= series : " + series);
            modelPoolslist = getModelPoolJpaController().findModelPoolBySeries(series);
            getCurrentdetails().setModelseries(series);
            for (ModelPool modelPool : modelPoolslist) {
                getModelpoollist().add(new SelectItem(modelPool.getId(), modelPool.getModelName()));
            }
            checkRequer();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changModelValue(ValueChangeEvent event) {
        print("================== Model chang value ============================");
        try {
            String modelId = event.getNewValue().toString();
            print(" ModelId : " + modelId);
            detailprocess = null;
            currentModelPool = getModelPoolJpaController().findModelPool(modelId);
            print("Modelpool : " + currentModelPool);
            getCurrentdetails().setModel(modelId);
            List<Md2pc> md2pcs = getMd2pcJpaController().findBymodelPoolId(currentModelPool);
            checkRequer();
            for (Md2pc md2pc : md2pcs) {
                getDetailprocess().add(new detailProcess(md2pc.getProcessPoolIdProc().getCodeOperation(), null, null, null, null, "", md2pc.getProcessPoolIdProc().getProcName(), "", 0, 0, 0, "", ""));
            }
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public List<SelectItem> getModelserieslist() {
        print("================= getModelserieslist ============================");
        try {
            modelPools = getModelPoolJpaController().findModelPoolGroupBySeries();
            List<SelectItem> seriesList = new ArrayList<SelectItem>();
            for (String series : modelPools) {
                seriesList.add(new SelectItem(series, series));
            }
            return seriesList;
        } catch (Exception e) {
            print("= Error : " + e);
            return null;
        }
    }

    public void changToCancel(ActionEvent event) {
        print("================= Cancel ========================================");
        try {
            currentLotControl = null;
            currentModelPool = null;
            currentdetailpacking = null;
            currentdetails = null;
            String link = "pages/moduleBackLine/packing/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            searchPackingWorksheet();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changToInsert(ActionEvent event) {
        print("============== Insert ===========================================");
        try {
            currentLotControl = null;
            currentModelPool = null;
            currentdetailpacking = null;
            currentdetails = null;
            detailprocess = null;
            String link = "pages/moduleBackLine/packing/createview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public List<SelectItem> getProductcodename() {
        if (productcodename == null) {
            productcodename = new ArrayList<SelectItem>();
            productcodename.add(new SelectItem("Prototype", "Prototype"));
            productcodename.add(new SelectItem("New product introduction", "New product introduction"));
            productcodename.add(new SelectItem("Special built request", "Special built request"));
            productcodename.add(new SelectItem("Mass production", "Mass production"));
        }
        return productcodename;
    }

    public List<SelectItem> getProducttype() {
        if (producttype == null) {
            producttype = new ArrayList<SelectItem>();
            producttype.add(new SelectItem("T", "Trigger coil"));
            producttype.add(new SelectItem("TR", "Tranformer"));
            producttype.add(new SelectItem("PCB", "Print circuit board"));
        }
        return producttype;
    }

    public void changProductTypevalue(ValueChangeEvent event) {
        print("=================== Product type ================================");
        try {
            String pdType = event.getNewValue().toString();
            getCurrentdetailpacking().setPdType(pdType);
            print("= PdType : " + getCurrentdetailpacking().getPdType());
            checkRequer();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changProductcodeName(ValueChangeEvent event) {
        print("================== Productcode Chang ============================");
        String pd_code = null;
        try {
            String pdcodename = event.getNewValue().toString();
            if (pdcodename.equals("Prototype")) {
                pd_code = "P";
            } else if (pdcodename.equals("New product introduction")) {
                pd_code = "N";
            } else if (pdcodename.equals("Special built request")) {
                pd_code = "S";
            } else if (pdcodename.equals("Mass production")) {
                pd_code = "M";
            }
            getCurrentdetailpacking().setPdCodeName(pdcodename);
            getCurrentdetailpacking().setPdCode(pd_code);
            checkRequer();
        } catch (Exception e) {
            print("= Error : " + e);
        }
        print("= Product name : " + event.getNewValue());
        print("= Product Code : " + pd_code);
    }

    private void loadWorkSheet() {
        worksheetlist = null;
        print("=================================================================");
        detailpackinglist = getDetailPackingJpaController().findDetailPackingsAll();
        if (detailpackinglist.isEmpty()) {
            getWorksheetlist();
        }
        for (DetailPacking detailPacking : detailpackinglist) {
            currentdetails = detailPacking.getDetailsIdDetail();
            if (currentdetails.getDetailName().equals("detail_packing")) {
                lotControllist = getLotControlJpaController().findByDetailsId(currentdetails);
                for (LotControl lotControl : lotControllist) {
                    try {
                        currentModelPool = lotControl.getModelPoolId();
                        if (lotControl.getMainDataId() != null) {
                            print("T");

                            MainData mainData = getMainDataJpaController().findMainData(lotControl.getMainDataId());
                            ProcessPool processPool = mainData.getIdProc();
                            CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(mainData.getCurrentNumber());

                            getWorksheetlist().add(new workSheetDetail(currentdetails, lotControl.getBarcode(), lotControl.getBarcode(), currentModelPool.getModelName(), currentModelPool.getModelSeries(), currentdetails.getDateCreate(), processPool.getProcName(), currentProcess.getStatus()));
                            print("= Detail : " + currentdetails.getDetailName() + " :: Barcode : " + lotControl.getBarcode());
                        } else {
                            getWorksheetlist().add(new workSheetDetail(currentdetails, lotControl.getBarcode(), lotControl.getBarcode(), currentModelPool.getModelName(), currentModelPool.getModelSeries(), currentdetails.getDateCreate(), "", ""));
                            print("Null");
                        }
                    } catch (Exception e) {
                        print("= Error : " + e);
                    }
                }
            }
        }
        setWorksheet();
    }

    public int getAllPages() {
        int all = getWorksheetpagination().getItemsCount();
        int pagesize = getWorksheetpagination().getPageSize();
        return ((int) all / pagesize) + 1;
    }
    private PaginationHelper worksheetpagination;

    public PaginationHelper getWorksheetpagination() {
        if (worksheetpagination == null) {
            worksheetpagination = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    return getWorksheetlist().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    print("PageFirst : " + getPageFirstItem());
                    print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (getWorksheetlist().size() > 0) {
                        listmodel = new ListDataModel(getWorksheetlist().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(getWorksheetlist());
                    }
                    return listmodel;
                }
            };
        }
        return worksheetpagination;
    }

    private boolean genBarCode(String code, String filePath) {
        print("===================== genbarcode file ===========================");
        try {
            getBarcodeGenerater().createBarcode(code, filePath + "/1d/");
            getBarcodeGenerater().createQrCode(code, filePath + "/2d/");
            print("= GenCode : Complete");
            return true;
        } catch (Exception e) {
            print("= Error can not genbarcode." + e.toString());
            return false;
        }
    }

    private List<blBarcode> getBarcodelist() {
        if (printassyworksheet.barcode == null) {
            printassyworksheet.barcode = new ArrayList<blBarcode>();
        }
        return printassyworksheet.barcode;
    }
    private BarCodeGenerter barCodeGenerter;

    private BarCodeGenerter getBarcodeGenerater() {
        if (barCodeGenerter == null) {
            barCodeGenerter = new BarCodeGenerter();
        }
        return barCodeGenerter;
    }
    private DetailsJpaController detailsJpaController;

    private DetailsJpaController getDetailsJpaController() {
        if (detailsJpaController == null) {
            detailsJpaController = new DetailsJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailsJpaController;
    }
    private Md2pcJpaController md2pcJpaController;

    private Md2pcJpaController getMd2pcJpaController() {
        if (md2pcJpaController == null) {
            md2pcJpaController = new Md2pcJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return md2pcJpaController;
    }
    private ModelPoolJpaController modelPoolJpaController;

    private ModelPoolJpaController getModelPoolJpaController() {
        if (modelPoolJpaController == null) {
            modelPoolJpaController = new ModelPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return modelPoolJpaController;
    }
    private CurrentProcessJpaController currentProcessJpaController;

    private CurrentProcessJpaController getCurrentProcessJpaController() {
        if (currentProcessJpaController == null) {
            currentProcessJpaController = new CurrentProcessJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return currentProcessJpaController;
    }
    private MainDataJpaController mainDataJpaController;

    private MainDataJpaController getMainDataJpaController() {
        if (mainDataJpaController == null) {
            mainDataJpaController = new MainDataJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mainDataJpaController;
    }
    private LotControlJpaController lotControlJpaController;

    private LotControlJpaController getLotControlJpaController() {
        if (lotControlJpaController == null) {
            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotControlJpaController;
    }
    private DetailPackingJpaController detailPackingJpaController;

    private DetailPackingJpaController getDetailPackingJpaController() {
        if (detailPackingJpaController == null) {
            detailPackingJpaController = new DetailPackingJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailPackingJpaController;
    }

    private void print(String str) {
        System.out.println(str);
    }

    public List<workSheetDetail> getWorksheetlist() {
        if (worksheetlist == null) {
            worksheetlist = new ArrayList<workSheetDetail>();
        }
        return worksheetlist;
    }

    public void setWorksheetlist(List<workSheetDetail> worksheetlist) {
        this.worksheetlist = worksheetlist;
    }

    public DataModel getWorksheet() {
        return worksheet;
    }

    public void setWorksheet() {
        this.worksheet = getWorksheetpagination().createPageDataModel();
    }

    public String getBarcodeSearch() {
        return barcodeSearch;
    }

    public void setBarcodeSearch(String barcodeSearch) {
        this.barcodeSearch = barcodeSearch;
    }

    public String getModelSearch() {
        return modelSearch;
    }

    public void setModelSearch(String modelSearch) {
        this.modelSearch = modelSearch;
    }

    public String getProcessSearch() {
        return processSearch;
    }

    public void setProcessSearch(String processSearch) {
        this.processSearch = processSearch;
    }

    public String getSeriesSearch() {
        return seriesSearch;
    }

    public void setSeriesSearch(String seriesSearch) {
        this.seriesSearch = seriesSearch;
    }

    public List<SelectItem> getModelpoollist() {
        if (modelpoollist == null) {
            modelpoollist = new ArrayList<SelectItem>();
        }
        return modelpoollist;
    }

    public ModelPool getCurrentModelPool() {
        return currentModelPool;
    }

    public void setCurrentModelPool(ModelPool currentModelPool) {
        this.currentModelPool = currentModelPool;
    }

    public Details getCurrentdetails() {
        if (currentdetails == null) {
            currentdetails = new Details();
        }
        return currentdetails;
    }

    public void setCurrentdetails(Details currentdetails) {
        this.currentdetails = currentdetails;
    }

    public List<detailProcess> getDetailprocess() {
        if (detailprocess == null) {
            detailprocess = new ArrayList<detailProcess>();
        }
        return detailprocess;
    }

    public void setDetailprocess(List<detailProcess> detailprocess) {
        this.detailprocess = detailprocess;
    }

    public DetailPacking getCurrentdetailpacking() {
        if (currentdetailpacking == null) {
            currentdetailpacking = new DetailPacking();
        }
        return currentdetailpacking;
    }

    public void setCurrentdetailpacking(DetailPacking currentdetailpacking) {
        this.currentdetailpacking = currentdetailpacking;
    }

    public LotControl getCurrentLotControl() {
        if (currentLotControl == null) {
            currentLotControl = new LotControl();
        }
        return currentLotControl;
    }

    public void setCurrentLotControl(LotControl currentLotControl) {
        this.currentLotControl = currentLotControl;
    }

    public void previous() {
        getWorksheetpagination().previousPage();
        setWorksheet();
    }

    public void next() {
        getWorksheetpagination().nextPage();
        setWorksheet();
    }

    public List<popupAssembly2Packing> getAssembly2Packingslist() {
        if (assembly2Packingslist == null) {
            assembly2Packingslist = new ArrayList<popupAssembly2Packing>();
        }
        return assembly2Packingslist;
    }

    public void setAssembly2Packingslist(List<popupAssembly2Packing> assembly2Packingslist) {
        this.assembly2Packingslist = assembly2Packingslist;
    }

    public String getPopupbarcode() {
        return popupbarcode;
    }

    public void setPopupbarcode(String popupbarcode) {
        this.popupbarcode = popupbarcode;

//        getPopupBarcodeQty();
    }

    public int getPopQty() {
        return popQty;
    }

    public void setPopQty(int popQty) {
        this.popQty = popQty;
    }

    public void changPopBarcodeValue(ValueChangeEvent event) {
        print("===================== chang value ===============================");
        try {
            String bbc = event.getNewValue().toString();
            print("= Barcode : " + bbc);
            setPopupbarcode(bbc);
            getPopupBarcodeQty();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    private void getPopupBarcodeQty() {
        print("=============== getPopupQty =====================================");
        poperror = "";
        try {
            poplotControl = getLotControlJpaController().findLotcontrolByBarcode(popupbarcode).get(0);
            if (poplotControl.getWipQty() > 0) {
                popQty = poplotControl.getQtyOutput();
                popWip = poplotControl.getWipQty();
            }
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public boolean isPopvisible() {
        return popvisible;
    }

    public void setPopvisible(boolean popvisible) {
        this.popvisible = popvisible;
    }

    public int getPopWip() {
        return popWip;
    }

    public void setPopWip(int popWip) {
        this.popWip = popWip;
    }

    public Back2pack getCurrentBack2pack() {
        if (currentBack2pack == null) {
            currentBack2pack = new Back2pack();
        }
        return currentBack2pack;
    }
    private Back2packJpaController back2packJpaController;

    private Back2packJpaController getBack2packJpaController() {
        if (back2packJpaController == null) {
            back2packJpaController = new Back2packJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return back2packJpaController;
    }

    public String getPoperror() {
        return poperror;
    }

    public void setPoperror(String poperror) {
        this.poperror = poperror;
    }

    public String getErrorrequermodel() {
        return errorrequermodel;
    }

    public void setErrorrequermodel(String errorrequermodel) {
        this.errorrequermodel = errorrequermodel;
    }

    public String getErrorrequermodelseries() {
        return errorrequermodelseries;
    }

    public void setErrorrequermodelseries(String errorrequermodelseries) {
        this.errorrequermodelseries = errorrequermodelseries;
    }

    public String getErrorrequerpd() {
        return errorrequerpd;
    }

    public void setErrorrequerpd(String errorrequerpd) {
        this.errorrequerpd = errorrequerpd;
    }

    public String getErrorrequerpdt() {
        return errorrequerpdt;
    }

    public void setErrorrequerpdt(String errorrequerpdt) {
        this.errorrequerpdt = errorrequerpdt;
    }

    private boolean checkRequer() {
        boolean tt = true;
        if (getCurrentdetailpacking().getPdCodeName() == null || getCurrentdetailpacking().getPdCodeName().isEmpty()) {
            errorrequerpd = "Select this.";
            tt = false;
        } else {
            errorrequerpd = "";
        }
        if (getCurrentdetailpacking().getPdType() == null || getCurrentdetailpacking().getPdType().isEmpty()) {
            errorrequerpdt = "Select this.";
            tt = false;
        } else {
            errorrequerpdt = "";
        }
        if (getCurrentdetails().getModel() == null || getCurrentdetails().getModel().isEmpty()) {
            errorrequermodel = "Select this.";
            tt = false;
        } else {
            errorrequermodel = "";
        }
        if (getCurrentdetails().getModelseries() == null || getCurrentdetails().getModelseries().isEmpty()) {
            errorrequermodelseries = "Select this.";
            tt = false;
        } else {
            errorrequermodelseries = "";
        }
        return tt;
    }
}
