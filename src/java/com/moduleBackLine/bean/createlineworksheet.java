/**
 *
 */
package com.moduleBackLine.bean;

import com.moduleBackLine.Dataworksheet.blBarcode;
import com.moduleFontline.DataWorksheet.detailProcess;
import com.tct.barcodegenerate.BarCodeGenerter;
import com.tct.data.*;
import com.tct.data.jpa.*;
import com.tct.data.jsf.util.JsfUtil;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;

/**
 * @author The_Boy_Cs
 *
 */
public class createlineworksheet {

    private String line_lotcontrol;
    private String line_remark;
    private String line_stamp;
    private Details details = null;
    private LineAssembly lineDetails = null;
    private List<detailProcess> detailprocess = null;
    private List<SelectItem> serieslist = null;
    private ModelPool modelPool = null;

    public createlineworksheet() {
        printassyworksheet.barcode=null;
    }
    
    private List<blBarcode> getBarcodelist() {
        if (printassyworksheet.barcode == null) {
            printassyworksheet.barcode = new ArrayList<blBarcode>();
        }
        return printassyworksheet.barcode;
    }

     private void loadProcess() {
        print("=================================================================");
        try {
            detailprocess = new ArrayList<detailProcess>();
            List<Md2pc> md2pclist = getMd2pcJpaController().findBymodelPoolId(modelPool);
            for (Md2pc md2pc : md2pclist) {
                print("= md2pc : " + md2pc.getMd2pcId());
                ProcessPool pcpool = md2pc.getProcessPoolIdProc();
                print("= Process : " + pcpool.getProcName());
                detailProcess dp = new detailProcess("", null, null, null, null, "", pcpool.getProcName().toString(), "", 0, 0, 0, "", "");
                detailprocess.add(dp);
            }
        } catch (Exception e) {
            print("= Ex : " + e);
        }
    }
    private Md2pcJpaController md2pcJpaController = null;

    private Md2pcJpaController getMd2pcJpaController() {
        if (md2pcJpaController == null) {
            md2pcJpaController = new Md2pcJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return md2pcJpaController;
    }

    public void ChangProductCodenameValue(ValueChangeEvent event) {
        print("=================================================================");
        print("Product code name : " + event.getNewValue());
        try {
            if (event.getNewValue().equals("Prototype")) {
                lineDetails.setPdCode("P");
            } else if (event.getNewValue().equals("New product introduction")) {
                lineDetails.setPdCode("N");
            } else if (event.getNewValue().equals("Special built request")) {
                lineDetails.setPdCode("S");
            } else if (event.getNewValue().equals("Mass production")) {
                lineDetails.setPdCode("M");
            }
            lineDetails.setPdCodeName(event.getNewValue().toString());
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    private void print(String str) {
        System.out.println(str);
    }

    public void ChangProductTypeValue(ValueChangeEvent event) {
        print("=================================================================");
        print("= Product type : " + event.getNewValue());
        try {
            lineDetails.setPdType(event.getNewValue().toString());
        } catch (Exception e) {
            print("= Error2 : " + e);
        }
    }

    public void ChangValueModelPoolList(ValueChangeEvent event) {
        print("=================================================================");
        print("= Model : " + event.getNewValue());
        String modelname = event.getNewValue().toString();
        List<ModelPool> models = getModelPoolJpaController().findByModelname(modelname);
        serieslist = new ArrayList<SelectItem>();
        for (ModelPool model : models) {
            serieslist.add(new SelectItem(model.getId(), model.getModelSeries()));
            print("= Series : " + model.getModelSeries());
        }
//        print("=================================================================");
    }

    public void ChangValueSeriesList(ValueChangeEvent event) {
        print("=================================================================");
        String model_id = event.getNewValue().toString();
        print("= Model id : " + model_id);
        modelPool = getModelPoolJpaController().findModelPool(model_id);
        print("= Model name : " + modelPool.getModelName());
        loadProcess();
    }
    private ModelPoolJpaController modelPoolJpaController = null;

    private ModelPoolJpaController getModelPoolJpaController() {
        if (modelPoolJpaController == null) {
            modelPoolJpaController = new ModelPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return modelPoolJpaController;
    }

    public List<SelectItem> getModellist() {
        List<ModelPool> models = getModelPoolJpaController().findModelPoolGroupByName();
        List<SelectItem> lists = new ArrayList<SelectItem>();
        for (ModelPool model : models) {
            lists.add(new SelectItem(model.getModelName(), model.getModelName()));
        }
        return lists;
    }

    public boolean CreateLineworksheet(ActionEvent event) {
        print("=================================================================");
        int lot = Integer.parseInt(line_lotcontrol);
        try {
            details.setIdDetail(UUID.randomUUID().toString());
            details.setDateCreate(new Date());
            details.setDetailType("backline");
            details.setDetailName("line_assembly");
            details.setUserCreate(getUserlogin().getId());
            details.setModel(modelPool.getModelName());
            details.setLotcontrol(line_lotcontrol);

            lineDetails.setId(UUID.randomUUID().toString());
            lineDetails.setDetailIdDetail(details);
            lineDetails.setRemark(line_remark);

            getDetailsJpaController().create(details);
            getLineAssemblyJpaController().create(lineDetails);

            for (int i = 0; i < lot; i++) {
                String barcode = getBarcode(lot, i);
                getLotControl().setId(UUID.randomUUID().toString());
                getLotControl().setBarcode(barcode);
                getLotControl().setDetailsIdDetail(details);
                getLotControl().setModelPoolId(modelPool);
                getLotControl().setLotNumber(i + "");

                if (GenneratebarCode(barcode, "C:/tct_project_netbean/web/images/barcode/b/lineassembly/")) {
                    getLotControlJpaController().create(getLotControl());
                    getBarcodelist().add(new blBarcode(getLotControl().getId(), barcode, "../images/barcode/b/lineassembly/", true));
                    print("= Barcode : "+barcode);
                }
            }
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("DetailAssyCreated"));
            print("= Create Complete.");
            Thread.sleep(5000);
            return true;
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            print("= Error2 : " + e);
            return false;
        }
    }
    private LotControl lotControl = null;

    private LotControl getLotControl() {
        if (lotControl == null) {
            lotControl = new LotControl();
        }
        return lotControl;
    }
    private LotControlJpaController lotControlJpaController = null;

    private LotControlJpaController getLotControlJpaController() {
        if (lotControlJpaController == null) {
            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotControlJpaController;
    }
    private LineAssemblyJpaController lineAssemblyJpaController = null;

    private LineAssemblyJpaController getLineAssemblyJpaController() {
        if (lineAssemblyJpaController == null) {
            lineAssemblyJpaController = new LineAssemblyJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lineAssemblyJpaController;
    }
    private DetailsJpaController detailsJpaController = null;

    private DetailsJpaController getDetailsJpaController() {
        if (detailsJpaController == null) {
            detailsJpaController = new DetailsJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailsJpaController;
    }
    private BarCodeGenerter barCodeGenerter = null;

    private BarCodeGenerter getBarCodeGenerter() {
        if (barCodeGenerter == null) {
            barCodeGenerter = new BarCodeGenerter();
        }
        return barCodeGenerter;
    }

    private boolean GenneratebarCode(String barcode, String filePath) {
        try {
            getBarCodeGenerter().createBarcode(barcode, filePath + "b/");
            getBarCodeGenerter().createQrCode(barcode, filePath);
            return true;
        } catch (Exception e) {
            print("=================== Error genbarcode pic ====================");
            print("= Error : " + e);
            return false;
        }
    }

    public String getBarcode(int lot, int i) {
        String bc = "BAL" + lineDetails.getPdCode() + lineDetails.getPdType() + "-" + getConvertNumber(lot)
                + new SimpleDateFormat("MM").format(new Date()) + "" + new SimpleDateFormat("yy").format(new Date())
                + modelPool.getModelSeries() + getConvertNumber(i);
        return bc;
    }

    private String getConvertNumber(int i) {
        return getBarCodeGenerter().UseNumberToString(i);
    }

    private Users getUserlogin() {
        FacesContext context = FacesContext.getCurrentInstance();
        Users currentuser = (Users) context.getExternalContext().getSessionMap().get("currentUser");
        return currentuser;
    }

    public Details getDetails() {
        if (details == null) {
            details = new Details();
        }
        return details;
    }

    public LineAssembly getLineDetails() {
        if (lineDetails == null) {
            lineDetails = new LineAssembly();
        }
        return lineDetails;
    }

    /**
     * @return the line_lotcontrol
     */
    public String getLine_lotcontrol() {
        return line_lotcontrol;
    }

    /**
     * @param line_lotcontrol the line_lotcontrol to set
     */
    public void setLine_lotcontrol(String line_lotcontrol) {
        this.line_lotcontrol = line_lotcontrol;
    }

    /**
     * @return the line_stamp
     */
    public String getLine_stamp() {
        return line_stamp;
    }

    /**
     * @param line_stamp the line_stamp to set
     */
    public void setLine_stamp(String line_stamp) {
        this.line_stamp = line_stamp;
    }

    /**
     * @return the detailprocess
     */
    public List<detailProcess> getDetailprocess() {
        return detailprocess;
    }

    /**
     * @param detailprocess the detailprocess to set
     */
    public void setDetailprocess(List<detailProcess> detailprocess) {
        this.detailprocess = detailprocess;
    }

    public String getLine_remark() {
        return line_remark;
    }

    public void setLine_remark(String line_remark) {
        this.line_remark = line_remark;
    }

    public List<SelectItem> getSerieslist() {
        if (serieslist == null) {
            serieslist = new ArrayList<SelectItem>();
        }
        return serieslist;
    }

    public void setSerieslist(List<SelectItem> serieslist) {
        this.serieslist = serieslist;
    }
}