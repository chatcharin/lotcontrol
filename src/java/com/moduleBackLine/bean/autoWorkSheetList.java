/**
 *
 */
package com.moduleBackLine.bean;

import com.component.table.sortable.SortableList;
import com.moduleBackLine.Dataworksheet.workSheetDetail;
import com.tct.data.*;
import com.tct.data.jpa.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import javax.faces.event.ActionEvent;
import javax.persistence.Persistence;

/**
 * @author The_Boy_Cs
 *
 */
public class autoWorkSheetList extends SortableList {

    private static final String orderNumberColum = "Barcode";
    private static final String typeModelColum = "Model";
//    private static final String typeBobbinColum = "Product code name";
    private static final String dateCreateColum = "Date Create";
    private static final String processColum = "Process";
    private static final String statusColum = "Status";
    private String ordernumber;
    private String typemodel;
    private String process;
    private String typebobbin;
    private workSheetDetail[] worksheet;
    private Details details;

    public autoWorkSheetList() {
        super(orderNumberColum);
        loadWorkSheet();
    }

    public void searchWorksheet(ActionEvent event) {
        print("=================================================================");
        if (ordernumber != null && !ordernumber.trim().isEmpty()) {
            print("= Order Number :: " + ordernumber);
            searchByBarcode();
        } else if (process != null && !process.trim().isEmpty()) {
            print("= Process :: " + process);
            searchByProcess();
        } else if (typemodel != null && !typemodel.trim().isEmpty()) {
            print("= Type Model :: " + typemodel);
            searchByModel();
        } else {
            loadWorkSheet();
        }
    }
    private DetailsJpaController detailsJpaController = null;

    private DetailsJpaController getDetailsJpaController() {
        if (detailsJpaController == null) {
            detailsJpaController = new DetailsJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailsJpaController;
    }
    private void searchByModel() {
        worksheetlist = null;
        print("=================================================================");
        List<Details> detailslist = getDetailsJpaController().findDetailsByModelAndDetailNameAuto(typemodel);
        if(detailslist.isEmpty()){
            getWorksheetList();
        }
//        worksheet = new workSheetDetail[detailflList.size()];
        for (Details detail : detailslist) {
//            details = detailfl.getDetailIdDetail();
            lotControllist = getLotControlJpaController().findByDetailsId(detail);
            for (LotControl lotControl : lotControllist) {
                try {
                    if (lotControl.getMainDataId() != null) {
                        print("T");
                        MainData mainData = getMainDataJpaController().findMainData(lotControl.getMainDataId());
                        ProcessPool processPool = mainData.getIdProc();
                        CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(mainData.getCurrentNumber());
                        getWorksheetList().add(new workSheetDetail(detail,lotControl.getBarcode(),lotControl.getBarcode(), detail.getModel(), "", detail.getDateCreate(), processPool.getProcName(), currentProcess.getStatus()));
                        print("= Detail : " + detail.getDetailName() + " :: Barcode : " + lotControl.getBarcode());
                    } else {
                        getWorksheetList().add(new workSheetDetail(detail,lotControl.getBarcode(),lotControl.getBarcode(), detail.getModel(), "", details.getDateCreate(), null, null));
                        print("Null");
                    }
                } catch (Exception e) {
                    print("= Error : " + e);
                }
            }
        }
        setWorksheet();
    }
    
    private void searchByProcess() {
        worksheetlist = null;
        print("=================================================================");
        List<ProcessPool> pcpools = getProcessPoolJpaController().findProcessPoolByName(process);
        if(pcpools.isEmpty()){
            getWorksheetList();
        }
        for (ProcessPool pcpool : pcpools) {
            List<MainData> maindatas = getMainDataJpaController().findMainDataByProcess(pcpool);
            for (MainData maindata : maindatas) {
                LotControl lotControl = maindata.getLotControlId();
                Details detail = lotControl.getDetailsIdDetail();

                if (detail.getDetailName().equals("detail_auto")) {
//                    DetailAssy detailassy = getDetailAssyJpaController().findDetailFlByDetails(detail);
                    CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(maindata.getCurrentNumber());
                    getWorksheetList().add(new workSheetDetail(detail,lotControl.getBarcode(),lotControl.getBarcode(), detail.getModel(), "", detail.getDateCreate(), pcpool.getProcName(), currentProcess.getStatus()));
                    print("= Detail : " + detail.getDetailName() + " :: Barcode : " + maindata.getLotControlId().getBarcode());
                }
            }
        }
        setWorksheet();
    }
    private ProcessPoolJpaController processPoolJpaController = null;

    private ProcessPoolJpaController getProcessPoolJpaController() {
        if (processPoolJpaController == null) {
            processPoolJpaController = new ProcessPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return processPoolJpaController;
    }
    
    private void searchByBarcode() {
        worksheetlist = null;
        print("================ search by barcode ==============================");
        lotControllist = getLotControlJpaController().findLotcontrolByBarcode(ordernumber);
        if(lotControllist.isEmpty()){
            getWorksheetList();
        }
        for (LotControl lotcontrol : lotControllist) {
            Details detail = lotcontrol.getDetailsIdDetail();
            if (detail.getDetailName().equals("detail_auto")) {
                print("= Maindata id :: " + lotcontrol.getMainDataId());
                if (lotcontrol.getMainDataId() != null && !lotcontrol.getMainDataId().trim().isEmpty()) {
                    MainData maindata = getMainDataJpaController().findMainData(lotcontrol.getMainDataId());
                    ProcessPool pcpool = maindata.getIdProc();
//                DetailFl detailfl = getDetailFlJpacontroller().findDetailFlByDetails(detail);
                    CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(maindata.getCurrentNumber());
                    getWorksheetList().add(new workSheetDetail(detail,lotcontrol.getBarcode(),lotcontrol.getBarcode(), detail.getModel(), "", detail.getDateCreate(), pcpool.getProcName(), currentProcess.getStatus()));
                    print("= Detail : " + detail.getDetailName() + " :: Barcode : " + maindata.getLotControlId().getBarcode());
                }else{
                    getWorksheetList().add(new workSheetDetail(detail,lotcontrol.getBarcode(),lotcontrol.getBarcode(), detail.getModel(), "", detail.getDateCreate(), "", ""));
                    print("= Detail : " + detail.getDetailName() + " :: Barcode : " + lotcontrol.getBarcode());
                }
            }
        }
        setWorksheet();
    }
    
    private List<LotControl> lotControllist = null;
    private LotControlJpaController lotControlJpaController = null;

    private LotControlJpaController getLotControlJpaController() {
        if (lotControlJpaController == null) {
            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotControlJpaController;
    }
    private MainDataJpaController mianDataJpaController = null;

    private MainDataJpaController getMainDataJpaController() {
        if (mianDataJpaController == null) {
            mianDataJpaController = new MainDataJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mianDataJpaController;
    }
    private CurrentProcessJpaController currentProcesslist = null;

    private CurrentProcessJpaController getCurrentProcessJpaController() {
        if (currentProcesslist == null) {
            currentProcesslist = new CurrentProcessJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return currentProcesslist;
    }

    private void loadWorkSheet() {
        worksheetlist = null;
        print("=================================================================");
        detailAutolist = getDetailAutoJpaController().findDetailAutoEntities();
        if(detailAutolist.isEmpty()){
            getWorksheetList();
        }
//        worksheet = new workSheetDetail[detailflList.size()];
        for (DetailAuto detailauto : detailAutolist) {
            details = detailauto.getDetailIdDetail();
            lotControllist = getLotControlJpaController().findByDetailsId(details);
            for (LotControl lotControl : lotControllist) {
                try {
                    if (lotControl.getMainDataId() != null) {
                        print("T");
                        MainData mainData = getMainDataJpaController().findMainData(lotControl.getMainDataId());
                        ProcessPool processPool = mainData.getIdProc();
                        CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(mainData.getCurrentNumber());

                        getWorksheetList().add(new workSheetDetail(details,lotControl.getBarcode(),lotControl.getBarcode(), details.getModel(), null, details.getDateCreate(), processPool.getProcName(), currentProcess.getStatus()));
                        print("= Detail : " + details.getDetailName() + " :: Barcode : " + lotControl.getBarcode());
                    } else {
                        getWorksheetList().add(new workSheetDetail(details,lotControl.getBarcode(),lotControl.getBarcode(), details.getModel(), null, details.getDateCreate(), "", ""));
                        print("Null");
                    }
                } catch (Exception e) {
                    print("= Error : " + e);
                }

            }
        }
        setWorksheet();
    }
    
    private void setWorksheet(){
        int i = 0;
        worksheet = new workSheetDetail[worksheetlist.size()];
        for (workSheetDetail worksheets : worksheetlist) {
            worksheet[i++] = worksheets;
            print("= I : " + i + " :: worksheet : " + worksheets.getProcess());
        }
    }
    
    private List<workSheetDetail> worksheetlist = null;

    private List<workSheetDetail> getWorksheetList() {
        if (worksheetlist == null) {
            worksheetlist = new ArrayList<workSheetDetail>();
        }
        return worksheetlist;
    }

    private void print(String str) {
        System.out.println(str);
    }
    private List<DetailAuto> detailAutolist = null;
    private DetailAutoJpaController detailAutoJpaController = null;

    private DetailAutoJpaController getDetailAutoJpaController() {
        if (detailAutoJpaController == null) {
            detailAutoJpaController = new DetailAutoJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailAutoJpaController;
    }

    /**
     * @return the worksheet
     */
    public workSheetDetail[] getWorksheet() {
        if (!oldSort.equals(sortColumnName) || oldAscending != ascending) {
            sort();
            oldSort = sortColumnName;
            oldAscending = ascending;
        }
        return worksheet;
    }

    /**
     * @return the ordernumber
     */
    public String getOrdernumber() {
        return ordernumber;
    }

    /**
     * @return the typemodel
     */
    public String getTypemodel() {
        return typemodel;
    }

    /**
     * @return the process
     */
    public String getProcess() {
        return process;
    }

    /**
     * @return the typebobbin
     */
    public String getTypebobbin() {
        return typebobbin;
    }

    /**
     * @param typebobbin the typebobbin to set
     */
    public void setTypebobbin(String typebobbin) {
        this.typebobbin = typebobbin;
    }

    /**
     * @param process the process to set
     */
    public void setProcess(String process) {
        this.process = process;
    }

    /**
     * @param typemodel the typemodel to set
     */
    public void setTypemodel(String typemodel) {
        this.typemodel = typemodel;
    }

    /**
     * @param ordernumber the ordernumber to set
     */
    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    /**
     * @return the ordernumbercolum
     */
    public String getOrdernumbercolum() {
        return orderNumberColum;
    }

    /**
     * @return the typemodelcolum
     */
    public String getTypemodelcolum() {
        return typeModelColum;
    }

    /**
     * @return the datecreatecolum
     */
    public String getDatecreatecolum() {
        return dateCreateColum;
    }

    /**
     * @return the processcolum
     */
    public String getProcesscolum() {
        return processColum;
    }

    /**
     * @return the statuscolum
     */
    public String getStatuscolum() {
        return statusColum;
    }

    @Override
    protected void sort() {

        // TODO Auto-generated method stub
        Comparator comparator = new Comparator() {

            public int compare(Object o1, Object o2) {
                workSheetDetail c1 = (workSheetDetail) o1;
                workSheetDetail c2 = (workSheetDetail) o2;
                if (sortColumnName == null) {
                    return 0;
                }
                if (sortColumnName.equals(orderNumberColum)) {
                    return ascending
                            ? c1.getOrdernumber().compareTo(c2.getOrdernumber())
                            : c2.getOrdernumber().compareTo(c1.getOrdernumber());
                } else if (sortColumnName.equals(typeModelColum)) {
                    return ascending ? c1.getTypemodel().compareTo(c2.getTypemodel())
                            : c2.getTypemodel().compareTo(c1.getTypemodel());
//                } else if (sortColumnName.equals(typeBobbinColum)) {
//                    return ascending ? c1.getTypebobbin().compareTo(c2.getTypebobbin())
//                            : c2.getTypebobbin().compareTo(c1.getTypebobbin());
                } else if (sortColumnName.equals(dateCreateColum)) {
                    return ascending
                            ? c1.getCreatedate().compareTo(c2.getCreatedate())
                            : c2.getCreatedate().compareTo(c1.getCreatedate());
                } else if (sortColumnName.equals(processColum)) {
                    return ascending
                            ? c1.getProcess().compareTo(c2.getProcess())
                            : c2.getProcess().compareTo(c1.getProcess());
                } else if (sortColumnName.equals(statusColum)) {
                    return ascending
                            ? c1.getStatus().compareTo(c2.getStatus())
                            : c2.getStatus().compareTo(c1.getStatus());
                } else {
                    return 0;
                }
            }
        };
        Arrays.sort(worksheet, comparator);
    }

    @Override
    protected boolean isDefaultAscending(String sortColumn) {
        // TODO Auto-generated method stub
        return true;
    }
}
