/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moduleBackLine.bean;

import com.appCinfigpage.bean.userLogin;
import com.moduleBackLine.Dataworksheet.blBarcode;
import com.moduleFontline.DataWorksheet.detailProcess;
import com.tct.barcodegenerate.BarCodeGenerter;
import com.tct.data.*;
import com.tct.data.jpa.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;

/**
 *
 * @author The_Boy_Cs
 */
public class Pcb {

    private String newlotcontrol;
    private String newstamp;
    private String type;
    private ModelPool modelPool;
    private List<ModelPool> modelPoollist = null;
    private List<SelectItem> model;
    private List<SelectItem> series;
    private List<detailProcess> detailprocess;
    private BarCodeGenerter barCodeGenerter;
    private Details detail = null;

    public Pcb() {
    }

    public boolean createRefuFun(ActionEvent event) {
        print("=================================================================");
        try {
            int lot = Integer.parseInt(newlotcontrol);
            detail = new Details();
            detail.setIdDetail(UUID.randomUUID().toString());
            detail.setDateCreate(new Date());
            detail.setDetailName("pcb01");
            detail.setDetailType("backline");
            detail.setLotcontrol(newlotcontrol);
            detail.setModel(modelPool.getModelName());
            detail.setUserCreate(userLogin.getSessionUser().getId());
            detail.setLine("");

            Pcb01 pcb = new Pcb01();
            pcb.setId(UUID.randomUUID().toString());
            pcb.setStamp(newstamp);
            pcb.setType(type);
            pcb.setDetailIdDetail(detail);

            getDetailsJpaController().create(detail);
            getPcb01JpaController().create(pcb);

            for (int i = 0; i < lot; i++) {

                String barcode = GenBarcode(lot, i);
                print("= Barcode : " + barcode);
                LotControl lotControl = new LotControl();
                lotControl.setId(UUID.randomUUID().toString());
                lotControl.setLotNumber(i + "");
                lotControl.setModelPoolId(modelPool);
                lotControl.setDetailsIdDetail(detail);
                lotControl.setBarcode(barcode);

                if (genBarCode(barcode, "C:/tct_project_netbean/web/images/barcode/b/pcb/")) {
                    getLotControlJpaController().create(lotControl);
                    getBarcodelist().add(new blBarcode(lotControl.getId(), barcode, "../images/barcode/b/pcb/", true));
                    print("= Barcode : " + barcode);
                }
            }
            Thread.sleep(5000);
            return true;
        } catch (Exception e) {
            print("= Error : " + e);
            return false;
        }
    }

    private List<blBarcode> getBarcodelist() {
        if (printassyworksheet.barcode == null) {
            printassyworksheet.barcode = new ArrayList<blBarcode>();
        }
        return printassyworksheet.barcode;
    }
    private LotControlJpaController lotControlJpaController = null;

    private LotControlJpaController getLotControlJpaController() {
        if (lotControlJpaController == null) {
            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotControlJpaController;
    }

    private boolean genBarCode(String code, String filePath) {
        print("=================================================================");
        try {
            getBarCodeGenerter().createBarcode(code, filePath + "b/");
            getBarCodeGenerter().createQrCode(code, filePath);
            print("= GenCode : Complete");
            return true;
        } catch (Exception e) {
            print("= Error can not genbarcode." + e.toString());
            return false;
        }
    }

    private String GenBarcode(int lot, int i) {
        String barcode = "BPS" + "-" + getBarCodeGenerter().UseNumberToString(Integer.parseInt(detail.getLotcontrol())) + new SimpleDateFormat("MM").format(new Date());
        barcode += "" + new SimpleDateFormat("yy").format(new Date()) + modelPool.getModelSeries() + getBarCodeGenerter().UseNumberToString(i);
        return barcode;
    }

    private BarCodeGenerter getBarCodeGenerter() {
        if (barCodeGenerter == null) {
            barCodeGenerter = new BarCodeGenerter();
        }
        return barCodeGenerter;
    }
    private Pcb01JpaController pcb01JpaController = null;

    private Pcb01JpaController getPcb01JpaController() {
        if (pcb01JpaController == null) {
            pcb01JpaController = new Pcb01JpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return pcb01JpaController;
    }
    private DetailsJpaController detailsJpaController = null;

    private DetailsJpaController getDetailsJpaController() {
        if (detailsJpaController == null) {
            detailsJpaController = new DetailsJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailsJpaController;
    }

    private void loadProcess() {
        print("=================================================================");
        try {
            detailprocess = new ArrayList<detailProcess>();
            List<Md2pc> md2pclist = getMd2pcJpaController().findBymodelPoolId(modelPool);
            for (Md2pc md2pc : md2pclist) {
                print("= md2pc : " + md2pc.getMd2pcId());
                ProcessPool pcpool = md2pc.getProcessPoolIdProc();
                print("= Process : " + pcpool.getProcName());
                detailProcess dp = new detailProcess("", null, null, null, null, "", pcpool.getProcName().toString(), "", 0, 0, 0, "", "");
                detailprocess.add(dp);
            }
        } catch (Exception e) {
            print("= Ex : " + e);
        }
    }
    private Md2pcJpaController md2pcJpaController = null;

    private Md2pcJpaController getMd2pcJpaController() {
        if (md2pcJpaController == null) {
            md2pcJpaController = new Md2pcJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return md2pcJpaController;
    }

    public void changModelSeriesListener(ValueChangeEvent event) {
        print("=================================================================");
        try {
            String modelId = event.getNewValue().toString();
            print("= New Value : " + modelId);
            modelPool = getModelPoolJpaController().findModelPool(modelId);
            print("= Model : " + modelPool.getModelName() + " modelSeries : " + modelPool.getModelSeries());
            loadProcess();
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void changModelNameListener(ValueChangeEvent event) {
        detailprocess = new ArrayList<detailProcess>();
        print("=================================================================");
        try {
            String modelname = event.getNewValue().toString();
            print("= New Value : " + modelname);
            modelPoollist = getModelPoolJpaController().findByModelname(modelname);
            series = new ArrayList<SelectItem>();
            for (ModelPool models : modelPoollist) {
                print("= Series : " + models.getModelSeries());
                series.add(new SelectItem(models.getId(), models.getModelSeries()));
            }
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    private void loadModelName() {
        print("=================================================================");
        try {
            List<ModelPool> modellist = getModelPoolJpaController().findModelPoolGroupByName();
            model = new ArrayList<SelectItem>();
            for (ModelPool modellis : modellist) {
                model.add(new SelectItem(modellis.getModelName(), modellis.getModelName()));
            }
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    private void print(String str) {
        System.out.println(str);
    }
    private ModelPoolJpaController modelPoolJpaController = null;

    private ModelPoolJpaController getModelPoolJpaController() {
        if (modelPoolJpaController == null) {
            modelPoolJpaController = new ModelPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return modelPoolJpaController;
    }

    public String getNewlotcontrol() {
        return newlotcontrol;
    }

    public void setNewlotcontrol(String newlotcontrol) {
        this.newlotcontrol = newlotcontrol;
    }

    public String getNewstamp() {
        return newstamp;
    }

    public void setNewstamp(String newstamp) {
        this.newstamp = newstamp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<SelectItem> getModel() {
        loadModelName();
        return model;
    }

    public List<SelectItem> getSeries() {
        return series;
    }

    public List<detailProcess> getDetailprocess() {
        return detailprocess;
    }

}
