/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moduleBackLine.popup.bean;

import com.tct.data.LotControl;
import com.tct.data.PcsQty;

/**
 *
 * @author The_Boy_Cs
 */
public class popupAutoassySelected {
    private LotControl lotControl;
    private int qty;
  
    private int inqty;

    public popupAutoassySelected(LotControl lotControl,int qty,int inqty){
        this.lotControl = lotControl;
        this.qty = qty;
       
        this.inqty = inqty;
    }
    public LotControl getLotControl() {
        return lotControl;
    }

    public void setLotControl(LotControl lotControl) {
        this.lotControl = lotControl;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

 

    public int getInqty() {
        return inqty;
    }

    public void setInqty(int inqty) {
        this.inqty = inqty;
    }
    
}
