/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moduleBackLine.popup.bean;

import com.tct.data.Details;
import com.tct.data.LotControl;

/**
 *
 * @author The_Boy_Cs
 */
public class popupAssembly2Packing {

    private LotControl lotControl;
    private String packingBarcode;
    private int qty;

    public popupAssembly2Packing(LotControl lotControl, String packingBarcode, int qty) {
        this.lotControl = lotControl;
        this.packingBarcode = packingBarcode;
        this.qty = qty;
    }

    public LotControl getLotControl() {
        return lotControl;
    }

    public void setLotControl(LotControl lotControl) {
        this.lotControl = lotControl;
    }

    public String getPackingBarcode() {
        return packingBarcode;
    }

    public void setPackingBarcode(String packingBarcode) {
        this.packingBarcode = packingBarcode;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}
