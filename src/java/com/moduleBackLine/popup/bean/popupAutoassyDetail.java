/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moduleBackLine.popup.bean;

import com.tct.data.CurrentProcess;
import com.tct.data.LotControl;
import com.tct.data.MainData;
import com.tct.data.PcsQty;

/**
 *
 * @author The_Boy_Cs
 */
public class popupAutoassyDetail {
    private LotControl lotControl;
    private MainData mainData;
    private PcsQty pcsqty;
    private CurrentProcess currentProcess;

    public popupAutoassyDetail(LotControl lotControl){
        this.lotControl = lotControl;
    }
    
    public popupAutoassyDetail(LotControl lotControl,MainData mainData,PcsQty pcsqty,CurrentProcess currentProcess){
        this.lotControl = lotControl;
        this.mainData = mainData;
        this.pcsqty = pcsqty;
        this.currentProcess = currentProcess;
    }
            
    public CurrentProcess getCurrentProcess() {
        return currentProcess;
    }

    public void setCurrentProcess(CurrentProcess currentProcess) {
        this.currentProcess = currentProcess;
    }

    public LotControl getLotControl() {
        return lotControl;
    }

    public void setLotControl(LotControl lotControl) {
        this.lotControl = lotControl;
    }

    public MainData getMainData() {
        return mainData;
    }

    public void setMainData(MainData mainData) {
        this.mainData = mainData;
    }

    public PcsQty getPcsqty() {
        return pcsqty;
    }

    public void setPcsqty(PcsQty pcsqty) {
        this.pcsqty = pcsqty;
    }
    
}
