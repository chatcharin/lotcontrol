/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moduleBackLine.popup.bean;

import com.appCinfigpage.bean.userLogin;
import com.moduleBackLine.Dataworksheet.addAssypopupDetail;
import com.moduleBackLine.bean.createAssyWorksheet;
import com.tct.data.LotControl;
import com.tct.data.PcsQty;
import com.tct.data.jpa.LotControlJpaController;
import com.tct.data.jpa.PcsQtyJpaController;
import java.util.ArrayList;
import java.util.List;
import javax.faces.event.ActionEvent;
import javax.persistence.Persistence;

/**
 *
 * @author The_Boy_Cs
 */
public class addMaterail2assy {

    private final String Barcode = "Barcode";
    private final String Type = "Type";
    private final String Qty = "Qty";
    private boolean visible = false;
    private String newbarcode;
    private String delbarcode;
    private long sumqty = 0;
    private List<LotControl> lotControls;
    public static List<addAssypopupDetail> assypopuplist = null;
    private String completed;

    public addMaterail2assy() {
    }

    public void removeNewbarcode(ActionEvent event) {
        print("=================================================================");
        print("= NewBarcode : " + delbarcode);
        try {
            lotControls = getLotControlJpaController().findLotcontrolByBarcode(delbarcode);
            if (lotControls.size() == 1) {
                LotControl lotControl = lotControls.get(0);
                long qty = 4320;//getQty(lotControl);
                print("= Qty2 : " + qty);
                addAssypopupDetail addassypop = new addAssypopupDetail(lotControl.getBarcode(), "", qty, lotControl);
                boolean equered = false;
                for (addAssypopupDetail addassyp : getAssypopupDetails()) {
                    if (addassyp.getBarcode().equals(addassypop.getBarcode())) {
                        equered = true;
                        getAssypopupDetails().remove(addassyp);
                        sumqty -= qty;
                    }
                }
//                if (equered) {
//                    getAssypopupDetails().remove(addassypop);
//                    print("= Completed.");
//                    completed = "Completed";
//                } else {
//                    print("Not Complete or ซ้ำ");
//                    completed = "Not Complete";
//                }
            }
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public void createNewbarcode(ActionEvent event) {
        print("=================================================================");
        print("= NewBarcode : " + newbarcode);
        try {
            lotControls = getLotControlJpaController().findLotcontrolByBarcode(newbarcode);
            if (lotControls.size() == 1) {
                LotControl lotControl = lotControls.get(0);
                long qty = getQty(lotControl);//4320;
                print("= Qty2 : " + qty);
                addAssypopupDetail addassypop = new addAssypopupDetail(lotControl.getBarcode(), "", qty, lotControl);
                boolean equered = true;
                for (addAssypopupDetail addassyp : getAssypopupDetails()) {
                    if (addassyp.getBarcode().equals(addassypop.getBarcode())) {
                        equered = false;
                    }
                }
                if (equered) {
                    getAssypopupDetails().add(addassypop);
                    sumqty += qty;
                    print("= Completed.");
                    completed = "Completed";
                } else {
                    print("Not Complete or ซ้ำ");
                    completed = "Not Complete";
                }
            }
        } catch (Exception e) {
            print("= Error" + e);
        }
    }

    private long getQty(LotControl lotControl) {
        try {
            long qty = 0;
            List<PcsQty> pcsqtylist = getPcsQtyJpaController().findPcsQtyByType();
            for (PcsQty pcsqty : pcsqtylist) {
                LotControl lotcontrol = pcsqty.getCurrentProcessId().getMainDataId().getLotControlId();
                if (lotControl.equals(lotcontrol)) {
                    print("= True");
                    print("= Qty1 : " + pcsqty.getQty());
                    print("= wip : "+pcsqty.getWipQty());
                    qty += Integer.parseInt(pcsqty.getWipQty());
                } else {
                    print("= False");
                }
            }
            return qty;
        } catch (Exception e) {
            print("= Error : " + e);
            return 0;
        }
    }
    private PcsQtyJpaController pcsQtyJpaController = null;

    private PcsQtyJpaController getPcsQtyJpaController() {
        if (pcsQtyJpaController == null) {
            pcsQtyJpaController = new PcsQtyJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return pcsQtyJpaController;
    }

    public List<addAssypopupDetail> getAssypopupDetails() {
        if (assypopuplist == null) {
            assypopuplist = new ArrayList<addAssypopupDetail>();
        }
        return assypopuplist;
    }

    public void showAddMaterail2assy(ActionEvent event) {
        print("=================================================================");
        createAssyWorksheet createassyworksheet = userLogin.getSessionCreateAssyworksheet();
        if (createassyworksheet.getProductqty() > 0) {
            createassyworksheet.setPd_qty_error_ms("");
            visible = true;
        } else {
            createassyworksheet.setPd_qty_error_ms("Please input product qty.");
        }
        print("= Visible : " + visible);
    }

    public void saveAddMatrail2assy(ActionEvent event) {
        print("=================================================================");
//        assypopuplist = null;
        print(" haved : "+assypopuplist.isEmpty());
        visible = false;
        print("= Visible : " + visible);
    }
    private LotControlJpaController lotControlJpaController = null;

    private LotControlJpaController getLotControlJpaController() {
        if (lotControlJpaController == null) {
            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotControlJpaController;
    }

    private void print(String str) {
        System.out.println(str);
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getNewbarcode() {
        newbarcode = "";
        return newbarcode;
    }
//    private int k = 0;

    public void setNewbarcode(String newbarcode) {
        this.newbarcode = newbarcode;
//        print("- New barcode : " + newbarcode);
    }

    public void cancelAddMatrail2assy(ActionEvent event) {
        print("============== Cancel add matrail2assy ==========================");
        try {
            assypopuplist = null;
            visible = false;
            sumqty = 0;
            print("= Visible : " + visible);
        } catch (Exception e) {
            print("= Error : " + e);
        }
    }

    public String getBarcode() {
        return Barcode;
    }

    public String getQty() {
        return Qty;
    }

    public String getType() {
        return Type;
    }

    public long getSumqty() {
        return sumqty;
    }

    public String getCompleted() {
        return completed;
    }

    public String getDelbarcode() {
        delbarcode = "";
        return delbarcode;
    }

    public void setDelbarcode(String delbarcode) {
        this.delbarcode = delbarcode;
    }
}
