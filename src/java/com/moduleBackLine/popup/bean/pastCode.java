/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moduleBackLine.popup.bean;

import com.tct.data.CopperWireInvoice;

/**
 *
 * @author The_Boy_Cs
 */
public class pastCode {
    private String pastcode;
    private String invoid;
    private CopperWireInvoice copperWireInvoice;

    public pastCode(CopperWireInvoice copperWireInvoice,String pastcode, String invoid) {
        this.copperWireInvoice = copperWireInvoice;
        this.pastcode = pastcode;
        this.invoid = invoid;
    }

    public CopperWireInvoice getCopperWireInvoice() {
        return copperWireInvoice;
    }

    public void setCopperWireInvoice(CopperWireInvoice copperWireInvoice) {
        this.copperWireInvoice = copperWireInvoice;
    }

    public String getInvoid() {
        return invoid;
    }

    public void setInvoid(String invoid) {
        this.invoid = invoid;
    }

    public String getPastcode() {
        return pastcode;
    }

    public void setPastcode(String pastcode) {
        this.pastcode = pastcode;
    }
    
}
