/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moduleBackLine.Dataworksheet;

import com.tct.data.LotControl;

/**
 *
 * @author The_Boy_Cs
 */
public class addAssypopupDetail {
    private String barcode;
    private String type;
    private long qty;
    private LotControl lotControl;

    public addAssypopupDetail(String barcode,String type,long qty, LotControl lotControl){
        this.barcode = barcode;
        this.type = type;
        this.qty = qty;
        this.lotControl = lotControl;
    }
    
    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public long getQty() {
        return qty;
    }

    public void setQty(long qty) {
        this.qty = qty;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LotControl getLotControl() {
        return lotControl;
    }

    public void setLotControl(LotControl lotControl) {
        this.lotControl = lotControl;
    }
    
}
