/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moduleBackLine.Dataworksheet;

/**
 *
 * @author The_Boy_Cs
 */
public class blBarcode {
    private String id;
    private String qrcode;
    private String qrcodeUrl;
    private String barcode;
    private String barcodeUrl;
    private boolean selected;

    public blBarcode(String id, String barcode,  String barcodeUrl, boolean selected) {
        this.id = id;
        this.qrcode = barcode;
        this.qrcodeUrl = barcodeUrl+"2d/"+barcode+".png";
        this.barcode = barcode;
        this.barcodeUrl = barcodeUrl+"1d/"+barcode+".png";
        this.selected = selected;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getBarcodeUrl() {
        return barcodeUrl;
    }

    public void setBarcodeUrl(String barcodeUrl) {
        this.barcodeUrl = barcodeUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public String getQrcodeUrl() {
        return qrcodeUrl;
    }

    public void setQrcodeUrl(String qrcodeUrl) {
        this.qrcodeUrl = qrcodeUrl;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    
}
