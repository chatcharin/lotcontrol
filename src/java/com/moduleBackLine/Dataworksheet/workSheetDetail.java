/**
 *
 */
package com.moduleBackLine.Dataworksheet;

import com.tct.data.Details;
import java.util.Date;

/**
 * @author The_Boy_Cs
 *
 */
public class workSheetDetail {

    private String ordernumber;
    private String typemodel;
    private String typebobbin;
    private Date createdate;
    private String process;
    private String status;
    private Details detail;
    private String barcode;

    public workSheetDetail(Details detail,String barcode, String ordernumber, String typemodel, String typebobbin, Date createdate, String process, String status) {
        this.ordernumber = ordernumber;
        this.typebobbin = typebobbin;
        this.typemodel = typemodel;
        this.createdate = createdate;
        this.process = process;
        this.status = status;
        this.detail = detail;
        this.barcode = barcode;
        print(detail);
    }
private void print(Object obj){
    System.out.println(obj);
}
    /**
     * @return the ordernumber
     */
    public String getOrdernumber() {
        return ordernumber;
    }

    /**
     * @param ordernumber the ordernumber to set
     */
    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    /**
     * @return the typemodel
     */
    public String getTypemodel() {
        return typemodel;
    }

    /**
     * @param typemodel the typemodel to set
     */
    public void setTypemodel(String typemodel) {
        this.typemodel = typemodel;
    }

    /**
     * @return the typebobbin
     */
    public String getTypebobbin() {
        return typebobbin;
    }

    /**
     * @param typebobbin the typebobbin to set
     */
    public void setTypebobbin(String typebobbin) {
        this.typebobbin = typebobbin;
    }

    /**
     * @return the createdate
     */
    public Date getCreatedate() {
        return createdate;
    }

    /**
     * @param createdate the createdate to set
     */
    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    /**
     * @return the process
     */
    public String getProcess() {
        return process;
    }

    /**
     * @param process the process to set
     */
    public void setProcess(String process) {
        this.process = process;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    public Details getDetail() {
        return detail;
    }

    public void setDetail(Details detail_id) {
        this.detail = detail_id;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
    
}
