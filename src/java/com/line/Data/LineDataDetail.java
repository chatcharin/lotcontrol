/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.line.Data;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 22, 2012, Time : 11:30:49 AM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class LineDataDetail {
    private String indeLine;
    private int numLine;
    private String lineName;
    private String Description;

    public LineDataDetail(String indeLine, int numLine, String lineName, String Description) {
        this.indeLine = indeLine;
        this.numLine = numLine;
        this.lineName = lineName;
        this.Description = Description;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getIndeLine() {
        return indeLine;
    }

    public void setIndeLine(String indeLine) {
        this.indeLine = indeLine;
    }

    public String getLineName() {
        return lineName;
    }

    public void setLineName(String lineName) {
        this.lineName = lineName;
    }

    public int getNumLine() {
        return numLine;
    }

    public void setNumLine(int numLine) {
        this.numLine = numLine;
    } 
}
