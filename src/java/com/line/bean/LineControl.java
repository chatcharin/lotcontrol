/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.line.bean;

import com.appCinfigpage.bean.fc;
import com.appCinfigpage.bean.userLogin;
import com.line.Data.LineDataDetail;
import com.target.Data.TargetData;
import com.target.Data.TargetManualPlan;
import com.target.Data.TargetMaualNextModel;
import com.tct.data.*;
import com.tct.data.jpa.*;
import com.tct.data.jsf.util.PaginationHelper;
import com.time.timeCurrent;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;
import org.icefaces.ace.component.fileentry.FileEntry;
import org.icefaces.ace.component.fileentry.FileEntryEvent;
import org.icefaces.ace.component.fileentry.FileEntryResults;
/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 22, 2012, Time : 10:09:01 AM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class LineControl {
    private List<Line> tableLine ;
    private List<LineDataDetail> itemLine ;
    private LineJpaController  lineJpaController =null;
    private List<TargetData>  itemTargetData;
    private Line  currentLine ;
    private Target currentTarget;
    private TargetJpaController  targetJpaController = null;
    private int num = 0;
    private int num_line = 0;
    private Date selectDate ; 
    private List<SelectItem> startHour ;
    private List<SelectItem> startMinute;
    private List<SelectItem> endHour;
    private List<SelectItem> endMinute;
    private String startHourUse;
    private String startMinuteUse;
    private String endHourUse;
    private String endMinuteUse;
    private String qtyTarget;
    private DataModel targeGetTable;
    private DataModel lineGetTable;
    private List<SelectItem> itemModel;
    private ModelPoolJpaController modelPoolJpaController = null;
    private String modeluse;
    private ModelPool currentModel;
    private boolean autoManual = false;
    private boolean noneAutoManual = true;
    private List<TargetManualPlan> itemManualPlan;
    private List<TargetMaualNextModel> itemManualNextModel;
    private ManualTargetJpaController manualTargetJpaController = null;
    private ManualTarget currentManualTarget ;
    private NextModelJpaController nextModelJpaController = null;
    private NextModel currentNextModel ;
    private String linkImage ;
    private String closepopup = "window.close();";
    private String showpopup;
    private String idPic;
    public TargetJpaController getTargetJpaController()
    {
        if(targetJpaController ==null)
        {
            targetJpaController = new TargetJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return targetJpaController;
    }
    public LineJpaController getLineJpaController()
    {
        if(lineJpaController ==null)
        {
            lineJpaController = new LineJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lineJpaController;
    }
    public ModelPoolJpaController getModelPoolJpaController()
    {
        if(modelPoolJpaController == null)
        {
            modelPoolJpaController  = new ModelPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return modelPoolJpaController;
    }
    public ManualTargetJpaController getManualTargetJpaController()
    {
        if(manualTargetJpaController == null)
        {
            manualTargetJpaController = new ManualTargetJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return manualTargetJpaController;
    }
    public NextModelJpaController getNextModelJpaController()
    {
        if(nextModelJpaController == null)
        {
            nextModelJpaController  = new NextModelJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return nextModelJpaController;
    }    
    public LineControl() {
        loadLineAll(); 
        loadMinute();
        loadHour(); 
    }
    public void resetDataAll()
    { 
        targetpagination = null;
        currentLine = new Line();
        currentTarget = new Target();
        itemLine  = new ArrayList<LineDataDetail>();
        tableLine  = new ArrayList<Line>();
        itemTargetData  = new ArrayList<TargetData>();
        targeGetTable   = new ListDataModel();
        lineGetTable    = new ListDataModel();
        num  = 0;
        num_line  = 0;
        itemModel = null;
        itemManualNextModel = new ArrayList<TargetMaualNextModel>();
        itemManualPlan  = new ArrayList<TargetManualPlan>();
        idPic  = null;
        linkImage  = null;
        autoManual  =false;
        noneAutoManual = false;
    }
    public void changeToInsert()
    {
//        loadMinute();
//        loadHour();  
        resetDataAll();
        linkImage  = "./images/no_image.jpg";
        noneAutoManual  = true;
        loadModel();
        fc.print("======= Change To Insert ==========="); 
        String link  = "pages/setting/line/createview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
    }
    public void loadModel()
    {
       List<ModelPool> objModel  =  getModelPoolJpaController().findAllModelListBox();
       getItemModel().add(new SelectItem("","----- Select one -----"));
       for(ModelPool objList : objModel)
       {
           getItemModel().add(new SelectItem(objList.getId(),objList.getModelType()+"-"+objList.getModelSeries()+"-"+objList.getModelName()));
       }
    }
    public void changeModelValue(ValueChangeEvent event)
    {
        
        //modeluse  = event.getNewValue().toString();
             
    }
    public void changeToCreate()
    {
        fc.print("======= Change To Create ==========="); 
        if(idPic == null)
        {
           getCurrentLine().setLineId(gemId()); 
        }
        else
        { 
            getCurrentLine().setLineId(idPic);
        }
        getCurrentLine().setDeleted(0);
        getCurrentLine().setDescritpion(currentLine.getDescritpion());
        getCurrentLine().setLineName(currentLine.getLineName());
        getCurrentLine().setStdTime(currentLine.getStdTime());
        getCurrentLine().setStdWorker(currentLine.getStdWorker());
        getCurrentLine().setModelManual(currentLine.getModelManual());
        if(autoManual){
            getCurrentLine().setAutoLine("true");
        }else
        {
            getCurrentLine().setAutoLine("false");
        }
        createLine(); 
        if(autoManual){
            if(!getItemTargetData().isEmpty())
            {
                for(TargetData objList : getItemTargetData())
                {
                    prepareCreateTarget();
                    getCurrentTarget().setId(objList.getIndex());
                    getCurrentTarget().setDateCreate(objList.getDate());
                    getCurrentTarget().setEnd(objList.getEndTime());
                    getCurrentTarget().setTargetQty(objList.getQty());
                    getCurrentTarget().setIdLine(getCurrentLine());
                    getCurrentTarget().setStart(objList.getStartTime());
                    getCurrentTarget().setDeleted(0);
                    getCurrentTarget().setIdModelPool(objList.getIndexModel());
                    
                    createTarget(); 
                }
            }
        }
        if(noneAutoManual)
        {
            if(!getItemManualPlan().isEmpty()){
                for(TargetManualPlan objList :getItemManualPlan())
                {
                    prepareManualPlan();
                    getCurrentManualTarget().setIdManaulTarget(objList.getIndexManualPlan());
                    getCurrentManualTarget().setIdLine(getCurrentLine());
                    getCurrentManualTarget().setDefect(objList.getDefect());
                    getCurrentManualTarget().setDefectRate(objList.getDefectRate());
                    getCurrentManualTarget().setDeleted(0);
                    getCurrentManualTarget().setQty(objList.getQty());
                    getCurrentManualTarget().setResult(objList.getResult());
                    getCurrentManualTarget().setTime(objList.getTime());
                    crateManualPlan();
                }
            }
            if(!getItemManualNextModel().isEmpty()){
                for(TargetMaualNextModel opjList:getItemManualNextModel())
                {
                    prepareManualNexTModel();
                    getCurrentNextModel().setDated(opjList.getDate());
                    getCurrentNextModel().setDeleted(0);
                    getCurrentNextModel().setIdLine(getCurrentLine());
                    getCurrentNextModel().setIdNextModel(opjList.getIndexManualNextModel());
                    getCurrentNextModel().setModel(opjList.getModel());
                    getCurrentNextModel().setQty(opjList.getQuatility());
                    createManualNextModel();
                }
            }
        }
        prepareCreateLine();    
        resetDataAll();
        loadLineAll();
        String link  = "pages/setting/line/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
    }
    public void prepareManualPlan()
    {
        currentManualTarget  = new ManualTarget();
    }
    
    public void prepareManualNexTModel()
    {
       currentNextModel  = new NextModel();
    }
    public void crateManualPlan()
    {
        try
        {
            getManualTargetJpaController().create(getCurrentManualTarget());
        }
        catch(Exception e)
        {
            fc.print("Error not to create manualPlan"+e);
        }
    }
    public void createManualNextModel()
    {
       try
       {
           getNextModelJpaController().create(getCurrentNextModel());
       }
       catch(Exception e)
       {
           fc.print("Error not to create nextPlan"+e);
       }
    }
    
    public void createTarget()
    {
        try
        {
           getTargetJpaController().create(getCurrentTarget());
           //currentTarget  = new Target(); 
        }
        catch(Exception e)
        {
            fc.print("Error not to create target"+e);
        }
    }
    public void changeToCancel()
    {
        fc.print("======= Change To Cancel ===========");
        resetDataAll();
        loadLineAll();
        String link  = "pages/setting/line/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
    }
    public void changeToEdit(String index)
    {
        fc.print("======= Change To Edit ===========");
        
        prepareCreateLine();
        currentLine = getLineJpaController().findLine(index);
        if(getCurrentLine().getAutoLine().equals("true"))
        {
            autoManual  = true;
            noneAutoManual = false;
            loadModel(); 
            loadTargetForLine();
        }
        else
        {
            autoManual = false;
            noneAutoManual = true;
            if(getCurrentLine().getPicManaul()==null)
            {
                 linkImage  = "./images/no_image.jpg";
            }
            else
            {
                 linkImage  = "./images/acmmanaul/"+getCurrentLine().getPicManaul();
            }
            fc.print("==== Not auto ====");
            loadModelpaln();
            loadNextMoel();
        }
        String link  = "pages/setting/line/editview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
    }
    public void loadModelpaln()
    {
        fc.print("=========== Load model plan =========");
       // itemManualPlan  = getManualTargetJpaController().findByLineAndNotDeleted(currentLine);
        List<ManualTarget>  objTarget  = getManualTargetJpaController().findByLineAndNotDeleted(currentLine);
        for(ManualTarget objList : objTarget)
        {
            getItemManualPlan().add(new TargetManualPlan(getCurrentLine().getLineId(),objList.getIdManaulTarget(),objList.getTime(),objList.getQty(), objList.getResult() , objList.getDefect(), objList.getDefectRate()));
        }
    } 
    public void loadNextMoel()
    {   
        fc.print("========= Load next model ============");
        //List<>
        List<NextModel>  objNextModel = getNextModelJpaController().findByLineAndNotDeleted(currentLine);
        for(NextModel objList : objNextModel )
        {
            getItemManualNextModel().add(new TargetMaualNextModel(getCurrentLine().getLineId(),objList.getIdNextModel(),objList.getDated(),objList.getModel(),objList.getQty()));
        }
    }  
    public String formatData(Date  date)
    {
        return new SimpleDateFormat("dd/MM/yyyy").format(date);
    }
    public void loadTargetForLine()
    {
        fc.print("====== Load target for this line. ============");
        List<Target> objTarget = getTargetJpaController().findByIdLine(getCurrentLine());
        num=0;
        for(Target objList:objTarget)
        {  
            fc.print("date create : "+objList.getDateCreate());
             
            Date dateString  = objList.getDateCreate();
//            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE dd/MM/yyyy HH:mm:ss");
            try {
                Date convertedDate = dateString;//dateFormat.parse(dateString); 
                //Date date = new Date();
                //System.out.println(dateFormat.format("date "+date));
                fc.print("Covert date"+convertedDate);
                String endString = formatData(new timeCurrent().getDate());
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy");
                Date endDate  = dateFormat1.parse(endString); 
                if(convertedDate.equals(endDate)||convertedDate.after(endDate))
                {
                    num++; 
                    fc.print("========= Change To Add Target ======"+" Date start "+selectDate);
                    String modelName = objList.getIdModelPool().getModelType()+"-"+objList.getIdModelPool().getModelSeries()+"-"+objList.getIdModelPool().getModelName();
                    getItemTargetData().add(new TargetData(objList.getId(), num, objList.getDateCreate(), objList.getStart(), objList.getEnd(), objList.getTargetQty(),modelName,objList.getIdModelPool()));
                   // getItemTargetData().add(new TargetData(gemId(), num,FrontLineControl.getFormatDate(selectDate),startHourUse+" : "+startMinuteUse,endHourUse+" : "+endMinuteUse,qtyTarget,modelName,objList.getIdModelPool()));
                }
            } catch (ParseException ex) {
                Logger.getLogger(LineControl.class.getName()).log(Level.SEVERE, null, ex);
            } 
        }
        setTargeGetTable();
    }            
    public void changeToSave()
    {
        editLine();
//        if(!getItemTargetData().isEmpty())
//        {
//            for(TargetData objList:getItemTargetData())
//            {
//                prepareCreateTarget();
//                currentTarget = getTargetJpaController().findTarget(objList.getIndex());
//                fc.print("id target  "+getCurrentTarget().getId());
//                if(getCurrentTarget().getId()==null)
//                {
//                    fc.print("======== Add new Target ==========");
//                    getCurrentTarget().setId(objList.getIndex());
//                    getCurrentTarget().setDateCreate(objList.getDate());
//                    getCurrentTarget().setEnd(objList.getEndTime());
//                    getCurrentTarget().setTargetQty(objList.getQty());
//                    getCurrentTarget().setIdLine(getCurrentLine());
//                    getCurrentTarget().setStart(objList.getStartTime());
//                    getCurrentTarget().setDeleted(0);
//                    getCurrentTarget().setIdModelPool(objList.getIndexModel());
//                    createTarget();
//                }
//            } 
//        } 
        
         if(autoManual){
            if(!getItemTargetData().isEmpty())
            {
                
                for(TargetData objList : getItemTargetData())
                { 
                    prepareCreateTarget();
                    currentTarget  = getTargetJpaController().findTarget(objList.getIndex());
                    if(getCurrentTarget().getId()==null){
                        getCurrentTarget().setId(objList.getIndex());
                        getCurrentTarget().setDateCreate(objList.getDate());
                        getCurrentTarget().setEnd(objList.getEndTime());
                        getCurrentTarget().setTargetQty(objList.getQty());
                        getCurrentTarget().setIdLine(getCurrentLine());
                        getCurrentTarget().setStart(objList.getStartTime());
                        getCurrentTarget().setDeleted(0);
                        getCurrentTarget().setIdModelPool(objList.getIndexModel());

                        createTarget();
                   }
                }
            }
        }
        if(noneAutoManual)
        {
            if(!getItemManualPlan().isEmpty()){
                for(TargetManualPlan objList :getItemManualPlan())
                {
                    prepareManualPlan();
                    currentManualTarget  = getManualTargetJpaController().findManualTarget(objList.getIndexManualPlan());
                    fc.print("id : "+getCurrentTarget().getId());
                    if(getCurrentTarget().getId()==null){
                        getCurrentManualTarget().setIdManaulTarget(objList.getIndexManualPlan());
                        getCurrentManualTarget().setIdLine(getCurrentLine());
                        getCurrentManualTarget().setDefect(objList.getDefect());
                        getCurrentManualTarget().setDefectRate(objList.getDefectRate());
                        getCurrentManualTarget().setDeleted(0);
                        getCurrentManualTarget().setQty(objList.getQty());
                        getCurrentManualTarget().setResult(objList.getResult());
                        getCurrentManualTarget().setTime(objList.getTime());
                        crateManualPlan();
                    }
                }
            }
            if(!getItemManualNextModel().isEmpty()){
                for(TargetMaualNextModel opjList:getItemManualNextModel())
                {
                    
                    prepareManualNexTModel();
                    currentNextModel  =  getNextModelJpaController().findNextModel(opjList.getIndexManualNextModel());
                    if(getCurrentNextModel().getIdNextModel()==null){
                        fc.print("==== insert");
                        getCurrentNextModel().setDated(opjList.getDate());
                        getCurrentNextModel().setDeleted(0);
                        getCurrentNextModel().setIdLine(getCurrentLine());
                        getCurrentNextModel().setIdNextModel(opjList.getIndexManualNextModel());
                        getCurrentNextModel().setModel(opjList.getModel());
                        getCurrentNextModel().setQty(opjList.getQuatility());
                        createManualNextModel();
                    }
                    else
                    {
                        fc.print("==== not inset ==");
                    }
                }
            }
        }
        prepareCreateLine();    
        resetDataAll();
        loadLineAll();
        String link  = "pages/setting/line/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
        
        
//        resetDataAll();
//        loadLineAll();
//        fc.print("======= Change To Save ===========");
//        String link  = "pages/setting/line/listview.xhtml";
//        userLogin.getSessionMenuBarBean().setParam(link);
    }
    
    
    //Target 
    public void resetTargetData()
    {
        startHourUse     = "00";
        startMinuteUse   = "00";
        endHourUse       = "00";
        endMinuteUse     = "00";
        qtyTarget  ="";
        selectDate = null;
        modeluse = "";
     }
    public void changeToAddTarget()
    {   
        
        if(checkTime()){
            if(checkQty()){
                if(selectDate != null){
                    if(checkDateTarget(selectDate))
                    {
                    num++;
                    prepareCuurentModel();
                    currentModel  =  getModelPoolJpaController().findModelPool(modeluse);
                    String model  =  getCurrentModel().getModelType()+"-"+getCurrentModel().getModelSeries()+"-"+getCurrentModel().getModelName();
                            
                    fc.print("========= Change To Add Target ======"+" Date start "+selectDate);
                    getItemTargetData().add(new TargetData(gemId(), num,selectDate,startHourUse+" : "+startMinuteUse,endHourUse+" : "+endMinuteUse,qtyTarget,model,getCurrentModel()));
                    setTargeGetTable(); 
                    resetTargetData();
                    }
                    else
                    {
                        fc.print("Check Date");
                    }
                }
                else
                {
                    fc.print("==== Insert Date ========");
                }
            }
        }
    }
    public void prepareCuurentModel()
    {
        currentModel  = new ModelPool();
    }
    public boolean checkTime()
    {
        boolean time = false;
        int startTime  = Integer.parseInt(startHourUse);
        int endTime    = Integer.parseInt(endHourUse); 
        if(startTime < endTime )
        {
           time  = true;
        }
        else
        {
            time = false;
            fc.print("Check time");
        }
        return time;
    }
//    public boolean checkTargetTimeUse()
//    {
//        boolean timeTarget = false;
//        int timeStartHourInput  = Integer.parseInt(startHourUse);
//        int timeStartMinute  = Integer.parseInt(startMinuteUse); 
//        int timeEndHour    = Integer.parseInt(endHourUse);
//        int timeEndMinute  = Integer.parseInt(endMinuteUse);
//        for(TargetData objList:getItemTargetData())
//        {
//            int startttimeItem = Integer.parseInt(objList.getStartTime());
//            int endTimeItem   = Integer.parseInt(objList.getEndTime());
//            if(startttimeItem == timeStartHourInput && )
//            {
//            }
//        }
//        return timeTarget;
//    }
    private String validateQtyTarget  = null;
    public boolean checkQty()
    {
        boolean checkQty  = false;
        if(qtyTarget!=null&&!qtyTarget.trim().isEmpty())
        {
            try  //check qty validate
            {
                int numQty  = Integer.parseInt(qtyTarget);
                fc.print("Num qty  *** :"+numQty);
                checkQty = true;
                validateQtyTarget  = "";
            }
            catch(Exception e)
            {
                checkQty = false;
                validateQtyTarget = "Invalid input";
            }  
        }
        else
        {
            checkQty = false;
            validateQtyTarget = "Invalid input";
        }
        return checkQty;
    }
    
    public boolean checkDateTarget(Date date)
    {
        boolean check = false;
        //Date currentDate = new timeCurrent().getDate();
        String endString = formatData(new timeCurrent().getDate());
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy");
        Date endDate; 
        try {
            endDate = dateFormat1.parse(endString);
            if(date.after(endDate) ||date.equals(endDate))
            {
                check  = true;
            }
        } catch (ParseException ex) {
            Logger.getLogger(LineControl.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return check;
    }
    
    //Start
    public void changeToStartHour(ValueChangeEvent event)
    {
        fc.print("============ Change to Start hour =============");
        startHourUse   = event.getNewValue().toString();     
    
    }
    public void changeToStartMinute(ValueChangeEvent event)
    {
        fc.print("========== Change to Start minute ==============");
        startMinuteUse  = event.getNewValue().toString();
    }
    
    //Stop
    public void changeToEndHour(ValueChangeEvent event)
    {
        fc.print("========= Chamge to End Hour ===================");
        endHourUse  = event.getNewValue().toString();
    }
    public void changeToEndMinute(ValueChangeEvent event)
    {
        fc.print("========= Change to end minute ==================");
        endMinuteUse  = event.getNewValue().toString();
    }
    public void changeToDeleteLine(String index)
    {
        fc.print("id line delete"+index);
        prepareDelLine();
        currentLine  = getLineJpaController().findLine(index);
        getCurrentLine().setDeleted(1);
        editLine();
        prepareDelTarget();
        List<Target> objDel  = getTargetJpaController().findByIdLine(getCurrentLine());
        for(Target objList:objDel)
        {
            objList.setDeleted(1);
        }
        num_line =0; 
        for(LineDataDetail objlist:getItemLine())
        {   num_line++;
            objlist.setNumLine(num_line);
        }
        setLineGetTable();
        resetDataAll();
        loadLineAll();
    }
    public void editLine()
    {
        try
        {
            getLineJpaController().edit(getCurrentLine());
        }
        catch(Exception e)
        {
            fc.print("Error no to edit line");
        }
    }
    public void prepareDelLine()
    {
        currentLine = new Line();
    }
    public void prepareDelTarget()
    {
        currentTarget  = new Target();
    }
    public void changeToDeleteTarget(String index)
    {
        fc.print("========= Change To Delete ======="+index);
        for(TargetData objlist:getItemTargetData())
        {
            if(objlist.getIndex().equals(index)){ 
                getItemTargetData().remove(objlist); 
                currentTarget  = getTargetJpaController().findTarget(index);
                if(getCurrentTarget().getId()!=null)
                {
                    getCurrentTarget().setDeleted(1);
                    editTarget();
                }
                break;
            }        
        }
        resetNum();
    }
    private void editTarget()
    {
        try
        {
            getTargetJpaController().edit(getCurrentTarget());
        }
        catch(Exception e)
        {
            fc.print("Edit delete target");
        }
    }
    public void resetNum()
    {
        prepareDelTarget();
        num = 0;
        for(TargetData objlist:getItemTargetData())
        {
            num++;
            objlist.setNum(num);
        }
        setTargeGetTable();
    } 
    public String gemId()
    {
        String id = UUID.randomUUID().toString();
        return id;
    }
    public void prepareCreateTarget()
    {
        currentTarget = new Target();
    }
    public void prepareCreateLine()
    {
        currentLine  = new Line();
    }
    public void createLine()
    {
        
        try
        {
            getLineJpaController().create(getCurrentLine()); 
        }
        catch(Exception e)
        {
            fc.print("Error not to create line ");
        }
    }
     
    private void loadLineAll() {
         int num = 0;
         tableLine  = getLineJpaController().findAllLineNotDeletd();
         for(Line objList : tableLine)
         {
             num++;
             getItemLine().add(new LineDataDetail(objList.getLineId(), num,objList.getLineName(), objList.getDescritpion()));
         }
         setLineGetTable();
    }
  
    //Hour
    private void loadHour()
    {
        String hour;
        for(int i=0;i<=24;i++)
        {
            if(i<10)
            {
                hour = "0"+Integer.toString(i);
            }
            else{
                hour = Integer.toString(i);
            }
            getStartHour().add(new SelectItem(hour,hour));
            getEndHour().add(new SelectItem(hour,hour));
        }
    } 
    
    //minute
    private void loadMinute()
    {
        String minute;
        for(int i=0;i<=60;i++)
        {
            if(i<10)
            {
                minute  = "0"+Integer.toString(i);
            }
            else
            {
                minute  = Integer.toString(i);
            }
            getStartMinute().add(new SelectItem(minute,minute));
            getEndMinute().add(new SelectItem(minute,minute));
        } 
    }
    //table get target 
    public void previous() {
        getTargetpagination().previousPage();
        setTargeGetTable();
    }

    public void next() {
        getTargetpagination().nextPage();
        setTargeGetTable();
    }

    public int getAllPages() {
        int all = getTargetpagination().getItemsCount();
        int pagesize = getTargetpagination().getPageSize();
        return ((int) all / pagesize) + 1;
    }
    private PaginationHelper targetpagination;
    public PaginationHelper getTargetpagination() {
        if (targetpagination == null) {
            targetpagination = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    return getItemTargetData().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    fc.print("PageFirst : " + getPageFirstItem());
                    fc.print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (getItemTargetData().size() > 0) {
                        listmodel = new ListDataModel(getItemTargetData().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(getItemTargetData());
                    }
                    return listmodel;
                }
            };
        }
        return targetpagination;
    }
    
    //line table
     public void previous_line() {
        getLinepagination().previousPage();
        setLineGetTable();
    }

    public void next_line() {
        getLinepagination().nextPage();
        setLineGetTable();
    }

    public int getAllPages_line() {
        int all = getLinepagination().getItemsCount();
        int pagesize = getLinepagination().getPageSize();
        return ((int) all / pagesize) + 1;
    }
    private PaginationHelper linepagination;
    public PaginationHelper getLinepagination() {
        if (linepagination == null) {
            linepagination = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    return getItemLine().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    fc.print("PageFirst : " + getPageFirstItem());
                    fc.print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (getItemLine().size() > 0) {
                        listmodel = new ListDataModel(getItemLine().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(getItemLine());
                    }
                    return listmodel;
                }
            };
        }
        return linepagination;
    } 
    public void changeAutoTarget(ValueChangeEvent event)
    { 
        if((Boolean)event.getNewValue())
        {
            fc.print("==== Auto manual ====");
             autoManual  =  true;
             noneAutoManual = false;
             loadModel();
        }
        else
        {
            fc.print("==== None auto manual =====");
            noneAutoManual = true;
            autoManual  = false;
             if(getCurrentLine().getPicManaul()==null)
            {
                 linkImage  = "./images/no_image.jpg";
            }
            else
            {
                 linkImage  = "./images/acmmanaul/"+getCurrentLine().getPicManaul();
            }
        }
    }
    public void addManualPlan()
    {
        fc.print("========= Add manual Plan ========");
        getItemManualPlan().add(new TargetManualPlan(null, gemId() ,null, null, null, null, null));
    }
    public void addManualNextModel()
    {
        fc.print("======== Add next model ==========");
        getItemManualNextModel().add(new TargetMaualNextModel(getCurrentLine().getLineId(), gemId(), new Date(), modeluse, qtyTarget));
    }
    public void delManualPlan(String index)
    {
        fc.print("id del : "+index);
        for(TargetManualPlan objList : getItemManualPlan())
        {
            if(objList.getIndexManualPlan().equals(index))
            {
                ManualTarget  objlist2  = getManualTargetJpaController().findManualTarget(index);
                if(!(objlist2.getIdManaulTarget()== null))
                {
                    objlist2.setDeleted(1);
                    try
                    {
                        getManualTargetJpaController().edit(objlist2);
                    }
                    catch(Exception e)
                    {
                        fc.print("===== Edit =====");
                    }
                }
                getItemManualPlan().remove(objList); 
                break;
            }
        }
    } 
   
    public void delManualNextPlan(String index)
    {
        fc.print("id del : "+index);
        for(TargetMaualNextModel objList : getItemManualNextModel())
        {
            if(objList.getIndexManualNextModel().equals(index))
            {
               getItemManualNextModel().remove(objList);
               NextModel objList2 = getNextModelJpaController().findNextModel(index);
               if(!(objList2.getIdNextModel()== null))
               {
                   objList2.setDeleted(1);
                   try
                   {
                       getNextModelJpaController().edit(objList2);
                   }
                   catch(Exception e)
                   {
                       fc.print("===== Error not to edit =====");
                   }
               }
               break;
            }
        }
    } 
    public void uploadImage()
    {
        fc.print("================= upload image =======================");
        closepopup = "";
        showpopup = "window.showModalDialog('pages/setting/line/popuploadpic.xhtml', 'Upload pic', 'dialig');"; 
    }  
    public void listener(FileEntryEvent event) {
        fc.print("====================== listener ===============");
        try {
            FileEntry fileEntry = (FileEntry) event.getSource();
            FileEntryResults results = fileEntry.getResults();
            String partfile = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/images/acmmanaul/");
            for (FileEntryResults.FileInfo fileInfo : results.getFiles()) {
                if (fileInfo.isSaved()) {
                    fc.print("== filename 1 : " + fileInfo.getFileName());
                    File file = fileInfo.getFile();
                    fc.print("=== file name 2 : " + file.getName());
//                    String filename = file.getName();
                    String[] arrname = file.getName().split("\\.");
                    fc.print("= Lenght : " + arrname.length);
                    fc.print(" == filetype : " + arrname[1]);
                    String filename;
                    if(getCurrentLine().getPicManaul() != null){
                        filename = getCurrentLine().getLineId()+ "." + arrname[arrname.length - 1];
                    }
                    else
                    {
                        idPic = gemId();
                        filename = idPic+ "." + arrname[arrname.length - 1];
                    }
                    fc.print("= file name 3 : " + filename);
                    partfile += "/" + filename;
                    if (writFile(file, partfile)) {
                        getCurrentLine().setPicManaul(filename);
                        showpopup = "";
                        closepopup = "window.close();";
                    }
                }
            }
        } catch (Exception e) {
            fc.print("Error0 : " + e);
        }
    }
     private boolean writFile(File file, String filePath) throws FileNotFoundException, IOException {
        fc.print("========== Create file ==================================");
        fc.print("= file parth : " + filePath);
        try {
            File outputFile = new File(filePath);
            FileInputStream in = new FileInputStream(file);
            FileOutputStream out = new FileOutputStream(outputFile);
            int c;
            while ((c = in.read()) != -1) {
                out.write(c);
            }
            file.delete();
            in.close();
            out.close();
            return true;
        } catch (IOException ioe) {
            fc.print("= IO Error3 : " + ioe);
            return false;
        } catch (Exception e) {
            fc.print("= Error2 : " + e);
            return false;
        }
    }
    public List<Line> getTableLine() {
        if(tableLine == null)
        {
            tableLine = new ArrayList<Line>();
        }
        return tableLine;
    }

    public void setTableLine(List<Line> tableLine) {
        this.tableLine = tableLine;
    }

    public List<LineDataDetail> getItemLine() {
        if(itemLine == null)
        {
            itemLine = new ArrayList<LineDataDetail>();
        }    
        return itemLine;
    }

    public void setItemLine(List<LineDataDetail> itemLine) {
        this.itemLine = itemLine;
    }

    public Line getCurrentLine() {
        if(currentLine ==null)
        {
            currentLine = new Line();
        }
        return currentLine;
    }

    public void setCurrentLine(Line currentLine) {
        this.currentLine = currentLine;
    }

    public List<TargetData> getItemTargetData() {
        if(itemTargetData == null)
        {
            itemTargetData  = new ArrayList<TargetData>();
        }
        return itemTargetData;
    }

    public void setItemTargetData(List<TargetData> itemTargetData) {
        this.itemTargetData = itemTargetData;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public Target getCurrentTarget() {
        if(currentTarget == null)
        {
            currentTarget = new Target();
        }
        return currentTarget;
    }

    public void setCurrentTarget(Target currentTarget) {
        this.currentTarget = currentTarget;
    }

    public Date getSelectDate() { 
        return selectDate;
    }

    public void setSelectDate(Date selectDate) {
        this.selectDate = selectDate;
    }

    public List<SelectItem> getEndHour() {
        if(endHour == null)
        {
            endHour  = new ArrayList<SelectItem>();
        }
        return endHour;
    }

    public void setEndHour(List<SelectItem> endHour) {
        this.endHour = endHour;
    }

    public List<SelectItem> getEndMinute() {
        if(endMinute == null)
        {
            endMinute  = new ArrayList<SelectItem>();
        }
        return endMinute;
    }

    public void setEndMinute(List<SelectItem> endMinute) {
        this.endMinute = endMinute;
    }

    public List<SelectItem> getStartHour() {
        if(startHour == null)
        {
            startHour  = new ArrayList<SelectItem>();
        }
        return startHour;
    }

    public void setStartHour(List<SelectItem> startHour) {
        this.startHour = startHour;
    }

    public List<SelectItem> getStartMinute() {
        if(startMinute == null)
        {
            startMinute  = new ArrayList<SelectItem>();
        }
        return startMinute;
    }

    public void setStartMinute(List<SelectItem> startMinute) {
        this.startMinute = startMinute;
    }

    public String getEndHourUse() {
        return endHourUse;
    }

    public void setEndHourUse(String endHourUse) {
        this.endHourUse = endHourUse;
    }

    public String getEndMinuteUse() {
        return endMinuteUse;
    }

    public void setEndMinuteUse(String endMinuteUse) {
        this.endMinuteUse = endMinuteUse;
    }

    public String getStartHourUse() {
        return startHourUse;
    }

    public void setStartHourUse(String startHourUse) {
        this.startHourUse = startHourUse;
    }

    public String getStartMinuteUse() {
        return startMinuteUse;
    }

    public void setStartMinuteUse(String startMinuteUse) {
        this.startMinuteUse = startMinuteUse;
    }

    public String getQtyTarget() {
        return qtyTarget;
    }

    public void setQtyTarget(String qtyTarget) {
        this.qtyTarget = qtyTarget;
    }

    public DataModel getTargeGetTable() {
        return targeGetTable;
    }

    public void setTargeGetTable() { 
        this.targeGetTable = getTargetpagination().createPageDataModel();
    }

    public DataModel getLineGetTable() {
        return lineGetTable;
    }

    public void setLineGetTable() {
        this.lineGetTable = getLinepagination().createPageDataModel();
    }

    public int getNum_line() {
        return num_line;
    }

    public void setNum_line(int num_line) {
        this.num_line = num_line;
    }

    public List<SelectItem> getItemModel() {
        if(itemModel == null)
        {
            itemModel  = new ArrayList<SelectItem>();
        }
        return itemModel;
    }

    public void setItemModel(List<SelectItem> itemModel) {
        this.itemModel = itemModel;
    }

    public String getModeluse() {
        return modeluse;
    }

    public void setModeluse(String modeluse) {
        this.modeluse = modeluse;
    }

    public ModelPool getCurrentModel() {
        if(currentModel == null)
        {
            currentModel  = new ModelPool();
        }
        return currentModel;
    }

    public void setCurrentModel(ModelPool currentModel) {
        this.currentModel = currentModel;
    }

    public boolean isAutoManual() {
        return autoManual;
    }

    public void setAutoManual(boolean autoManual) {
        this.autoManual = autoManual;
    }

    public boolean isNoneAutoManual() {
        return noneAutoManual;
    }

    public void setNoneAutoManual(boolean noneAutoManual) {
        this.noneAutoManual = noneAutoManual;
    }

    public List<TargetMaualNextModel> getItemManualNextModel() {
        if(itemManualNextModel == null)
        {
           itemManualNextModel  = new ArrayList<TargetMaualNextModel>();     
        }
        return itemManualNextModel;
    }

    public void setItemManualNextModel(List<TargetMaualNextModel> itemManualNextModel) {
        this.itemManualNextModel = itemManualNextModel;
    }

    public List<TargetManualPlan> getItemManualPlan() {
        if(itemManualPlan == null)
        {
            itemManualPlan  = new ArrayList<TargetManualPlan>();
        }
        return itemManualPlan;
    }

    public void setItemManualPlan(List<TargetManualPlan> itemManualPlan) {
        this.itemManualPlan = itemManualPlan;
    }

    public ManualTarget getCurrentManualTarget() {
        if(currentManualTarget == null)
        {
            currentManualTarget  = new ManualTarget();
        }
        return currentManualTarget;
    }

    public void setCurrentManualTarget(ManualTarget currentManualTarget) {
        this.currentManualTarget = currentManualTarget;
    }

    public NextModel getCurrentNextModel() {
        if(currentNextModel== null)
        {
            currentNextModel  = new NextModel();
        }
        return currentNextModel;
    }

    public void setCurrentNextModel(NextModel currentNextModel) {
        this.currentNextModel = currentNextModel;
    }

    public String getLinkImage() {
        return linkImage;
    }

    public void setLinkImage(String linkImage) {
        this.linkImage = linkImage;
    }

    public String getClosepopup() {
        return closepopup;
    }

    public void setClosepopup(String closepopup) {
        this.closepopup = closepopup;
    }

    public String getShowpopup() {
        return showpopup;
    }

    public void setShowpopup(String showpopup) {
        this.showpopup = showpopup;
    }

    public String getIdPic() {
        return idPic;
    }

    public void setIdPic(String idPic) {
        this.idPic = idPic;
    }

    public String getValidateQtyTarget() {
        return validateQtyTarget;
    }

    public void setValidateQtyTarget(String validateQtyTarget) {
        this.validateQtyTarget = validateQtyTarget;
    }
    
}
