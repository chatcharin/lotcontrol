/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.master.bean;

import com.appCinfigpage.bean.fc;
import com.appCinfigpage.bean.userLogin;
import com.component.barcodeauto.ProcList;
import com.icesoft.faces.context.effects.JavascriptContext;
import com.master.Data.*;
import com.mysql.jdbc.Connection;
import com.sql.bean.Sql;
import com.tct.barcodegenerate.BarCodeGenerter;
import com.tct.dashboard.ReportBean;
import com.tct.data.*;
import com.tct.data.jpa.*;
import com.tct.data.jsf.util.PaginationHelper;
import com.time.timeCurrent;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.ArrayDataModel;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;
import javax.script.*;
/**
 *
 * Machine User
 *
 * @author pang Development by Chusak laojang Date : Sep 21, 2012, Time :
 * 5:24:28 PM Copy Right 4 Xtreme Co.,Ltd.
 */
public class Master {

    private List<DetailSpecial> objTypeMaster;
    private DetailSpecialJpaController detailSpecialJpaController = null;
    private DetailFieldLeftJpaController detailFieldLeftJpaController = null;
    private DetailFieldRightJpaController detailFieldRightJpaController = null;
    private List<SelectItem> itemTypeSpecial;
    private String itemUse;
    private List<MasterLeftData> itemMasterLeft;
    private List<MasterRightData> itemMasterRight;
    private DetailSpecial currentDetailSpecial;
    private List<SelectItem> itemSelectBox;
    private GroupFieldJpaController groupFieldJpaController = null;
    private Details currentDetails;
    private DetailsJpaController detailsJpaController = null;
    private ModelPoolJpaController modelPoolJpaController = null;
    private MasterDefaultData currentDefaultData;
    private DetailFieldLeftDataJpaController detailFieldLeftDataJpaController = null;
    private DetailFieldRightDateJpaController detailFieldRightDateJpaController = null;
    private DetailFieldLeftData currentFieldLeftData;
    private DetailFieldRightDate currentFieldRightData;
    private LotControlJpaController lotControlJpaController;
    private LotControl currentLotControl;
    private ProcessPoolJpaController processPoolJpaController = null;
    private Md2pcJpaController md2pcJpaController = null;
    private MainDataJpaController mainDataJpaController = null;
    private CurrentProcessJpaController currentProcessJpaController = null;
    private List<MasterListViewData> masterListViewData;
//   barcode list
    private List<MasterLinkImageAllPrint> barcodeImage;
//  data model
    private DataModel masterModel;
    private PaginationHelper worksheetpagination;
    //private List<MasterListViewData>  itenListData; 
    private List<ProcList> currentProcessStatus;
    private List<SelectItem> modelItem;
    private String controlLot;
    private String showModelUse = "false;";
    private String showModelNotUse = "false";
    private Connection conn;
    private String modelStr;
    private String itemStr;
    private String validateModel = null;
    String url = "";
    String urlCheck ="";
    String  urlAll = "";
    public Master() {
        loadMasterWorkSheet();
        findlistViewData();
    }
//   constructor

    private CurrentProcessJpaController getCurrentProcessJpaController() {
        if (currentProcessJpaController == null) {
            currentProcessJpaController = new CurrentProcessJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return currentProcessJpaController;
    }

    private MainDataJpaController getMainDataJpaController() {
        if (mainDataJpaController == null) {
            mainDataJpaController = new MainDataJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mainDataJpaController;
    }

    public Md2pcJpaController getMd2pcJpaController() {
        if (md2pcJpaController == null) {
            md2pcJpaController = new Md2pcJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return md2pcJpaController;
    }

    public ProcessPoolJpaController getProcessPoolJpaController() {
        if (processPoolJpaController == null) {
            processPoolJpaController = new ProcessPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return processPoolJpaController;
    }

    public LotControlJpaController getLotControlJpaController() {
        if (lotControlJpaController == null) {
            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotControlJpaController;
    }

    public DetailFieldLeftDataJpaController getDetailFieldLeftDataJpaController() {
        if (detailFieldLeftDataJpaController == null) {
            detailFieldLeftDataJpaController = new DetailFieldLeftDataJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailFieldLeftDataJpaController;
    }

    public DetailFieldRightDateJpaController getDetailFieldRightDateJpaController() {
        if (detailFieldRightDateJpaController == null) {
            detailFieldRightDateJpaController = new DetailFieldRightDateJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailFieldRightDateJpaController;
    }

    public ModelPoolJpaController getModelPoolJpaController() {
        if (modelPoolJpaController == null) {
            modelPoolJpaController = new ModelPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return modelPoolJpaController;
    }

    public DetailsJpaController getDetailsJpaController() {
        if (detailsJpaController == null) {
            detailsJpaController = new DetailsJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailsJpaController;
    }

    public DetailSpecialJpaController getDetailSpecialJpaController() {
        if (detailSpecialJpaController == null) {
            detailSpecialJpaController = new DetailSpecialJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailSpecialJpaController;
    }

    public DetailFieldLeftJpaController getDetailFieldLeftJpaController() {
        if (detailFieldLeftJpaController == null) {
            detailFieldLeftJpaController = new DetailFieldLeftJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailFieldLeftJpaController;
    }

    public DetailFieldRightJpaController getDetailFieldRightJpaController() {
        if (detailFieldRightJpaController == null) {
            detailFieldRightJpaController = new DetailFieldRightJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailFieldRightJpaController;
    }

    public GroupFieldJpaController getGroupFieldJpaController() {
        if (groupFieldJpaController == null) {
            groupFieldJpaController = new GroupFieldJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return groupFieldJpaController;
    }

    public void changeToInsert() {
        String link = "pages/masterworksheet/createview.xhtml";
        resetData();
        userLogin.getSessionMenuBarBean().setParam(link);
        loadMasterWorkSheet();
        loadDataProductType();
        loadModel();
    }

    public String gemId() {
        String id = UUID.randomUUID().toString();
        return id;
    }
//   product type

    public List<SelectItem> loadDataProductType() {
        List<SelectItem> productType = new ArrayList<SelectItem>();
        productType.add(new SelectItem("null", " ----- Select one ----- "));
        for (String objList : getModelPoolJpaController().findModelPoolGroupByGroup()) {
            productType.add(new SelectItem(objList, objList));
        }
        return productType;
    }
//  product  code name

    public List<SelectItem> loadProductCodeName() {
        List<SelectItem> productCodeName = new ArrayList<SelectItem>();
        productCodeName.add(new SelectItem("null", " ----- Select one ----- "));
        productCodeName.add(new SelectItem("Prototype", "Prototype"));
        productCodeName.add(new SelectItem("New_product_introduction", "New product introduction"));
        productCodeName.add(new SelectItem("Special_built_request", "Special built request"));
        productCodeName.add(new SelectItem("Mass_production", "Mass production"));
        return productCodeName;
    }
// product code

    public void productCodeValue(ValueChangeEvent event) {
        String type = event.getNewValue().toString();
        if (type.equals("null")) {
            currentDefaultData.setProductCode("");
        }
        if (type.equals("Prototype")) {
            currentDefaultData.setProductCode("P");
        }
        if (type.equals("New_product_introduction")) {
            currentDefaultData.setProductCode("NPI");
        }
        if (type.equals("Special_built_request")) {
            currentDefaultData.setProductCode("SBR");
        }
        if (type.equals("Mass_production")) {
            currentDefaultData.setProductCode("M");
        }
    }
//   model 

    public void loadModel() {
        getModelItem().add(new SelectItem("null", " ----- Select one ----- "));
        for (ModelPool objList : getModelPoolJpaController().findModelPoolGroupByName()) {
            getModelItem().add(new SelectItem(objList.getId(), objList.getModelType() + "-" + objList.getModelSeries() + "-" + objList.getModelName()));
        }
    }
//series

    public void changeValueModel(ValueChangeEvent event) {
        String index = event.getNewValue().toString();
        fc.print("=== index ====== : " + index);
        if (!index.equals("null")) {
            currentDefaultData.setSeriee(getModelPoolJpaController().findModelPool(index).getModelSeries());
            currentDefaultData.setModel(index);
            loadProcessStatus();
        }
    }
//   process status 

    public void loadProcessStatus() {
        fc.print("item use ****:" + itemUse);
        currentProcessStatus = new ArrayList<ProcList>();
        int i = 1;
        fc.print("model id =  : " + getCurrentDefaultData().getModel());
        ModelPool modelPool = getModelPoolJpaController().findModelPool(getCurrentDefaultData().getModel());
        fc.print("modelpool id" + modelPool.getId());
        if (modelPool.getId() != null) {
            List<Md2pc> objMd2pc = getMd2pcJpaController().findBymodelPoolId(modelPool);
            if (!objMd2pc.isEmpty()) {
                for (Md2pc objList : objMd2pc) {
                    fc.print("process type id *** : " + objList.getProcessPoolIdProc().getType());
                    if (objList.getProcessPoolIdProc().getType().equals(itemUse)) { //**** check type model is only.
                        getCurrentProcessStatus().add(new ProcList(i, objList.getProcessPoolIdProc().getProcName(), "", "", "", "", "", "", "", null, "", "", "", "", "", ""));
                        i++;
                    }
                }
                validateModel = null;//*** have process 
            } else {

                validateModel = "This model not have process."; //*** not have 
            }
            fc.print("validate Model ***** :"+validateModel);  
        }
    }
//   Plant

    public List<SelectItem> loadPlant() {
        List<SelectItem> plant = new ArrayList<SelectItem>();
        plant.add(new SelectItem("null", "--- Select one --"));
        plant.add(new SelectItem("Tokyo coil", "Tokyo coil"));
        plant.add(new SelectItem("Farsonic", "Farsonic"));
        return plant;
    }
//   order type

    public List<SelectItem> loadOrderType() {
        List<SelectItem> orderType = new ArrayList<SelectItem>();
        orderType.add(new SelectItem("null", "--- Select one ---"));
        orderType.add(new SelectItem("Frame", "Frame"));
        orderType.add(new SelectItem("Roll", "Roll"));
        return orderType;
    }
    //check  left qty

    public boolean checkLeftQty() {
        boolean check = false;
        if (leftqty == 0) {
            check = true;
        }
        return check;
    }
//   validate lot control

    public String validateLeft() {
        String msg = "Check left qty";
        if (checkLeftQty()) {
            msg = "";
        }
        return msg;
    }

    public void changeToCreate() {

        fc.print("======== change to create master =======");
        fc.print("order no.  ==  : " + getCurrentDefaultData().getOrderNo());
        //master default create in details
        if (validateModel == null) {
            if (checkLeftQty()) {
                String idDetails = gemId();
                getCurrentDetails().setIdDetail(idDetails);
                getCurrentDetails().setDetailBarcode("");
                getCurrentDetails().setLotcontrol(getCurrentDefaultData().getLotControlQty());
                getCurrentDetails().setModel(getCurrentDefaultData().getModel());
                getCurrentDetails().setModelseries(getCurrentDefaultData().getSeriee());
                getCurrentDetails().setShift("");
                getCurrentDetails().setNumOnmonth(0);
                getCurrentDetails().setUserCreate(userLogin.getSessionUser().getUserName());
                getCurrentDetails().setUserApprove("");
                getCurrentDetails().setDateCreate(new timeCurrent().getDate());
                getCurrentDetails().setDetailType("");
                getCurrentDetails().setDetailName("");
                getCurrentDetails().setLine("");
                getCurrentDetails().setDeleted(0);
//        getCurrentDetails().setApproveStatus(0);
                getCurrentDetails().setOrderNumber(getCurrentDefaultData().getOrderNo());
                getCurrentDetails().setPdQty(getCurrentDefaultData().getProductQty());
                getCurrentDetails().setPdQtyByLot(getCurrentDefaultData().getProductQty());
                getCurrentDetails().setRemark(getCurrentDefaultData().getRemarks());
                getCurrentDetails().setPdCodeName(getCurrentDefaultData().getProductCodeName());
                getCurrentDetails().setPdCodeType(getCurrentDefaultData().getProductType());
                getCurrentDetails().setPlant(getCurrentDefaultData().getPlant());
                getCurrentDetails().setOrderType(getCurrentDefaultData().getOrderType());
                getCurrentDetails().setInjectionDate(new timeCurrent().getDate());
                getCurrentDetails().setPlatingDate(new timeCurrent().getDate());
                getCurrentDetails().setInvBroze("null");
                getCurrentDetails().setInvBobin("null");
                getCurrentDetails().setAssyDate(new timeCurrent().getDate());
                getCurrentDetails().setPdCode(getCurrentDefaultData().getProductCode());
                getCurrentDetails().setDetailName(itemUse);
                createDetails();
                //master data left
                if (!getItemMasterLeft().isEmpty()) {
                    for (MasterLeftData objList : getItemMasterLeft()) {
                        prepareDetialFileLeftData();
                        getCurrentFieldLeftData().setIdDetailSpecailData(gemId());
                        getCurrentFieldLeftData().setIdDetailFieldLeft(objList.getIndexAttribute());
                        getCurrentFieldLeftData().setDataDetial(objList.getData());
                        getCurrentFieldLeftData().setIdDetails(idDetails);
                        createDetailFieldLeftData();
                    }
                }
                //master data right
                if (!getItemMasterRight().isEmpty()) {
                    for (MasterRightData objList : getItemMasterRight()) {
                        prepareDetailFieldRightData();
                        getCurrentFieldRightData().setIdFieldRihgtData(gemId());
                        getCurrentFieldRightData().setDataRight(objList.getData());
                        getCurrentFieldRightData().setIdDetails(idDetails);
                        getCurrentFieldRightData().setIdDetailRight(objList.getIndexAttribute());
                        createDetailFieldRightData();
                    }
                } else {
                }
//        barcode path file
                String partfile = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/images/barcode/");
                fc.print(" PartFile : " + partfile);
                int lotNum = 1;
                //lot control get model barcode ,etc 
                for (int i = 0; i < Integer.parseInt(getCurrentDefaultData().getLotControlQty()); i++) {
                    prepareLotControl();
                    lotNum++;
                    fc.print("type worksheet ***** :"+itemUse);
                    int maxnum = getBarcodeGenerater().getMaxnumberOfLotcontrolType(itemUse) + 1;
//                    int maxnum = getBarcodeGenerater().getMaxnumberOfLotcontrol() + 1;
                     
//          format barcode:  line-product code-lotnum-year-month
                    String barcode = lineBarcode(getCurrentDetailSpecial().getLineCode()) + "-" + productCodeBarcode(getCurrentDefaultData().getProductCode()) + "-" + maxnum + "-" + new SimpleDateFormat("yyyy").format(new Date()) + "-" + new SimpleDateFormat("MM").format(new Date());
                    fc.print("===== barcode ======== : " + barcode);
                    getCurrentLotControl().setId(gemId());
                    getCurrentLotControl().setDetailsIdDetail(getCurrentDetails());
                    getCurrentLotControl().setBarcode(barcode);
                    getCurrentLotControl().setLotNumber(Integer.toString(lotNum));
                    getCurrentLotControl().setMd2pcIdModel("");
                    getCurrentLotControl().setMainDataId("");
                    getCurrentLotControl().setModelPoolId(getModelPoolJpaController().findModelPool(currentDefaultData.getModel()));
                    getCurrentLotControl().setDeleted(0);
                    getCurrentLotControl().setStatusApprove(0);
                    getCurrentLotControl().setUserApprove("");
                    getCurrentLotControl().setWipQty(0);
                    getCurrentLotControl().setQtyOutput(0);

                    if (genBarCode(barcode, partfile)) {
                        createLotControl();
                        //getBarcodelist().add(new blBarcode(getCurrentLotControl().getId(), barcode, "./images/barcode/", true));
                        fc.print("= Barcode : " + barcode);
                        getBarcodeGenerater().updateMaxnumberOfLotcontrol(maxnum);
                        getBarcodeImage().add(new MasterLinkImageAllPrint(gemId(), barcode, true));
                    }
                }
                for (MasterLeftData objListL : getItemMasterLeft()) {
                    fc.print("data : " + objListL.getData());
                }
                //resetData();
                changToCreateLot();
                itemStr = getDetailSpecialJpaController().findDetailSpecial(itemUse).getName();
                itemUse = getDetailSpecialJpaController().findDetailSpecial(itemUse).getId();
                ModelPool objModel = getModelPoolJpaController().findModelPool(getCurrentDefaultData().getModel());
                String models = objModel.getModelType() + "-" + objModel.getModelSeries() + "-" + objModel.getModelName();
                currentDefaultData.setModel(models);
                itemMasterLeft = new ArrayList<MasterLeftData>();
                itemMasterRight = new ArrayList<MasterRightData>();
                loadDataSpecialLeftPrint(getDetailSpecialJpaController().findDetailSpecial(getCurrentDetails().getDetailName()).getId());
                loadDataSpecialRightPrint();
                String link = "pages/masterworksheet/printallview.xhtml";
                userLogin.getSessionMenuBarBean().setParam(link);
                urlCheck  = "window.close();";
                print_check = false;
                print_all =false;
            }
        }
    }
//   barcode

    private boolean genBarCode(String code, String filePath) {
        fc.print("===================== genbarcode file ===========================");
        try {
            getBarcodeGenerater().createBarcode(code, filePath + "/1d/");
            getBarcodeGenerater().createQrCode(code, filePath + "/2d/");
            fc.print("= GenCode : Complete");
            return true;
        } catch (Exception e) {
            fc.print("= Error can not genbarcode." + e.toString());
            return false;
        }
    }
//   format barcode 
//   line is running number of worksheet  

    public String lineBarcode(String lineno) {
        return lineno;
    }

    public String productCodeBarcode(String productCode) {
        return productCode;
    }
//    public String lotNoBarcode(int numLot)
//    {
//         
//        return numberLot;
//    }

    public String yearBarcode() {
        String yearNow = "";
        return yearNow;
    }

    public String monthBarcode() {
        String monthNow = "";
        return monthNow;
    }
//   generate barcode
    private BarCodeGenerter barCodeGenerter;

    private BarCodeGenerter getBarcodeGenerater() {
        if (barCodeGenerter == null) {
            barCodeGenerter = new BarCodeGenerter();
        }
        return barCodeGenerter;
    }
//   prepare lot control

    public void prepareLotControl() {
        currentLotControl = new LotControl();
    }
//    create lot control

    public void createLotControl() {
        try {
            getLotControlJpaController().create(getCurrentLotControl());
        } catch (Exception e) {
            fc.print("====== Error not to  insert data : " + e);
        }
    }
//   prepare right daat

    public void prepareDetailFieldRightData() {
        currentFieldRightData = new DetailFieldRightDate();
    }
// create detail right data

    public void createDetailFieldRightData() {
        try {
            getDetailFieldRightDateJpaController().create(getCurrentFieldRightData());
        } catch (Exception e) {
            fc.print("=== Error not to insert data" + e);
        }
    }
//prepare right data

    public void prepareDetialFileLeftData() {
        currentFieldLeftData = new DetailFieldLeftData();
    }
    //   createDetailFieldData  

    public void createDetailFieldLeftData() {
        try {
            getDetailFieldLeftDataJpaController().create(getCurrentFieldLeftData());
        } catch (Exception e) {
            fc.print("======= Error not to insert data" + e);
        }
    }

    public void createDetails() {
        try {
            getDetailsJpaController().create(getCurrentDetails());
        } catch (Exception e) {
            fc.print("========= Error not to insert detials =======");
        }
    }

    public void changeToCancel() {
        String link = "pages/masterworksheet/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
        resetData();
        conTrollotResetData();
        loadMasterWorkSheet();
        findlistViewData();
    }

    private void loadMasterWorkSheet() {
        objTypeMaster = getDetailSpecialJpaController().findAlltNotDelete();
        getItemTypeSpecial().add(new SelectItem("null", " ----- Select One ----- "));
        for (DetailSpecial objList : objTypeMaster) {
            getItemTypeSpecial().add(new SelectItem(objList.getId(), objList.getName()));
        }
    }

    public void changeValueItem(ValueChangeEvent event) {
        resetData();
        loadMasterWorkSheet();
        itemUse = (String) event.getNewValue();
        loadModel();
        getCurrentDefaultData().setModel("null");
        fc.print("model id" + getCurrentDefaultData().getModel());
        controlLot = "false";
        if (!itemUse.equals("null")) {
            //
            String check = getDetailSpecialJpaController().findDetailSpecial(itemUse).getControlLot();
            if (check.equals("true")) {
                controlLot = "true";
            } else {
                controlLot = "false";
            }
            loadDataDetail();
//            currentDefaultData.setProductCodeName("null");
//            currentDefaultData.setModel("null");
        }
    }

    private void loadDataDetail() {
        fc.print("======== Load data detail ===========");
        loadDetailLeft();
        loadDetailRight();

    }

    private void loadDetailLeft() {
        fc.print("========= load detail left table ============");
        prepareDetailSpecial();
        currentDetailSpecial = getDetailSpecialJpaController().findById(itemUse);
        List<DetailFieldLeft> objLeft = getDetailFieldLeftJpaController().findByDetialSpecial(currentDetailSpecial);
        for (DetailFieldLeft objList : objLeft) {
            boolean typeinput = typeValue(objList.getIdFieldName().getType());
            boolean typeSelect = typeSelect(objList.getIdFieldName().getType());
            if (typeSelect) {
                loadItemForSelectBox(objList.getIdFieldName());
            }
            getItemMasterLeft().add(new MasterLeftData(getCurrentDetailSpecial(), objList, objList.getIdFieldName().getName(), null, typeinput, typeSelect, itemSelectBox));
        }
    }

    private boolean typeValue(String type) {
        fc.print("type " + type);
        boolean check = false;
        if (type.equals("inputbox")) {
            check = true;
        }
        return check;
    }

    private boolean typeSelect(String type) {
        fc.print("type " + type);
        boolean check = false;
        if (type.equals("selectbox")) {
            check = true;
        }
        return check;
    }

    private void loadItemForSelectBox(FieldName idFileName) {
        fc.print("========= item list ========");
        itemSelectBox = new ArrayList<SelectItem>();
        List<GroupField> objGroub = getGroupFieldJpaController().findIdFieldName(idFileName);
        getItemSelectBox().add(new SelectItem("", " ----- Select one ----- "));
        for (GroupField objList : objGroub) {
            getItemSelectBox().add(new SelectItem(objList.getId(), objList.getDataText()));
        }
//        for()
//        {
//        
//        }
    }

    public void changeToPrintCancel() {
        fc.print("====== changeToPrintCancel =======");
        String link = "pages/masterworksheet/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
        resetData();
        conTrollotResetData();
        findlistViewData();
    }

    private void prepareDetailSpecial() {
        currentDetailSpecial = new DetailSpecial();
    }

    private void loadDetailRight() {
        List<DetailFieldRight> objRight = getDetailFieldRightJpaController().findByDetialSpecial(currentDetailSpecial);
        for (DetailFieldRight objList : objRight) {
            boolean typeinput = typeValue(objList.getIdFieldName().getType());
            boolean typeSelect = typeSelect(objList.getIdFieldName().getType());
            if (typeSelect) {
                loadItemForSelectBox(objList.getIdFieldName());
            }
            getItemMasterRight().add(new MasterRightData(getCurrentDetailSpecial(), objList, objList.getIdFieldName().getName(), null, typeinput, typeSelect, itemSelectBox));
        }
    }
//    list view data

    public void findlistViewData() {
        List<LotControl> objLot = getLotControlJpaController().findAllNotDeleted();
        if (!objLot.isEmpty()) {
            for (LotControl objList : objLot) {
                String process = "";
                String status = "";
                String orderNumber = objList.getDetailsIdDetail().getOrderNumber();
                String model = objList.getModelPoolId().getModelType() + "-" + objList.getModelPoolId().getModelSeries() + "-" + objList.getModelPoolId().getModelName();
                if (!objList.getMainDataId().isEmpty()) {
                    MainData objList2 = getMainDataJpaController().findMainData(objList.getMainDataId());
                    process = objList2.getIdProc().getProcName();
                    status = getCurrentProcessJpaController().findCurrentProcess(objList2.getCurrentNumber()).getStatus();
                }
                getMasterListViewData().add(new MasterListViewData(objList.getId(), objList.getBarcode(), model, objList.getModelPoolId().getModelSeries(), objList.getDetailsIdDetail().getDateCreate().toString(), process, status, orderNumber));
            }
            setMasterModel();
        }
    }
//    print worsheet

    public void changeToPrintWorksheet(String idPrint) {
        print_status=false;
        url = "window.close();";
        fc.print("======= Change to print worksheet ========");
        String link = "pages/masterworksheet/printview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
//        resetData();
//        String  barcodePrint = getMasterListViewData().get(getMasterModel().getRowIndex()).getBarcode();
        currentLotControl = getLotControlJpaController().findProcessNameAndStatus(idPrint);
        currentDetails = currentLotControl.getDetailsIdDetail();
        currentDefaultData = new MasterDefaultData(getCurrentDetails().getIdDetail(), getCurrentDetails().getOrderNumber(), getCurrentDetails().getPdCodeType(), getCurrentDetails().getPdCodeName(), getCurrentDetails().getPdCode(), getCurrentLotControl().getModelPoolId().getModelType() + "-" + getCurrentLotControl().getModelPoolId().getModelSeries() + "-" + getCurrentLotControl().getModelPoolId().getModelName(), getCurrentLotControl().getModelPoolId().getModelSeries(), getCurrentDetails().getPdQty(), getCurrentDetails().getLotcontrol(), getCurrentDetails().getInvBroze(), getCurrentDetails().getInvBobin(), getCurrentDetails().getPlant(), getCurrentDetails().getPlatingDate(), getCurrentDetails().getInjectionDate(), getCurrentDetails().getAssyDate(), getCurrentDetails().getOrderType(), getCurrentDetails().getRemark());
        itemStr = getDetailSpecialJpaController().findDetailSpecial(getCurrentDetails().getDetailName()).getName();
        itemUse = getDetailSpecialJpaController().findDetailSpecial(getCurrentDetails().getDetailName()).getId();
        if (currentDefaultData.getOrderType().equals("Frame")) {
            currentDefaultData.setOrderType("Frame");
        }
        if (currentDefaultData.getOrderType().equals("Roll")) {
            currentDefaultData.setOrderType("Roll");
        }
        loadMasterWorkSheet();
        loadDataSpecialLeftPrint(getDetailSpecialJpaController().findDetailSpecial(getCurrentDetails().getDetailName()).getId());
        loadDataSpecialRightPrint();

    }
//    detial left print 

    public void loadDataSpecialLeftPrint(String idDetailSpecial) {
        currentDetailSpecial = getDetailSpecialJpaController().findById(idDetailSpecial);
        List<DetailFieldLeft> objLeft = getDetailFieldLeftJpaController().findByDetialSpecial(currentDetailSpecial);

        for (DetailFieldLeft objList : objLeft) {
            String data = null;
            boolean typeinput = typeValue(objList.getIdFieldName().getType());
            boolean typeSelect = typeSelect(objList.getIdFieldName().getType());
            DetailFieldLeftData dataLeft = getDetailFieldLeftDataJpaController().findDetailFieldLeftData(objList, currentDetails.getIdDetail());
            if (!dataLeft.getDataDetial().isEmpty()) {

                if (typeSelect) {
                    loadItemForSelectBox(objList.getIdFieldName());
                    GroupField dataGrioup = getGroupFieldJpaController().findGroupField(dataLeft.getDataDetial());
                    data = dataGrioup.getDataText();
                    fc.print("======== data ======= : " + data);
                } else {
                    data = dataLeft.getDataDetial();
                }
                getItemMasterLeft().add(new MasterLeftData(getCurrentDetailSpecial(), objList, objList.getIdFieldName().getName(), data, typeinput, typeSelect, itemSelectBox));
            } else {
                getItemMasterLeft().add(new MasterLeftData(getCurrentDetailSpecial(), objList, objList.getIdFieldName().getName(), null, typeinput, typeSelect, itemSelectBox));
            }
        }
    }

    public void loadDataSpecialRightPrint() {
        List<DetailFieldRight> objRight = getDetailFieldRightJpaController().findByDetialSpecial(currentDetailSpecial);
        for (DetailFieldRight objList : objRight) {
            String data = null;
            DetailFieldRightDate dateRight = getDetailFieldRightDateJpaController().findDetailFieldLeftData(objList, currentDetails.getIdDetail());
            boolean typeinput = typeValue(objList.getIdFieldName().getType());
            boolean typeSelect = typeSelect(objList.getIdFieldName().getType());
            if (!dateRight.getDataRight().isEmpty()) {
                if (typeSelect) {
                    loadItemForSelectBox(objList.getIdFieldName());
                    GroupField dataGrioup = getGroupFieldJpaController().findGroupField(dateRight.getDataRight());
                    data = dataGrioup.getDataText();
                } else {
                    data = dateRight.getDataRight();
                }
            }
            getItemMasterRight().add(new MasterRightData(getCurrentDetailSpecial(), objList, objList.getIdFieldName().getName(), data, typeinput, typeSelect, itemSelectBox));
        }
    }
//    detial right print

//    save work for edit
    public void chanetToSave() {
        fc.print("======== change To save master worksheet ===");
        String link = "pages/masterworksheet/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
        resetData();

    }
//    edit master

    public void changeToEdit(String idEdit) throws SQLException {
        fc.print("======== change To edit master worksheet ===");
        String link = "pages/masterworksheet/editview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
        //resetData();
//        String  barcodeEdit = getMasterListViewData().get(getMasterModel().getRowIndex()).getBarcode();
        currentLotControl = getLotControlJpaController().findProcessNameAndStatus(idEdit);
        currentDetails = currentLotControl.getDetailsIdDetail();

        //process model running
        currentDefaultData = new MasterDefaultData(getCurrentDetails().getIdDetail(), getCurrentDetails().getOrderNumber(), getCurrentDetails().getPdCodeType(), getCurrentDetails().getPdCodeName(), getCurrentDetails().getPdCode(), getCurrentLotControl().getModelPoolId().getId(), getCurrentLotControl().getModelPoolId().getModelSeries(), getCurrentDetails().getPdQty(), getCurrentDetails().getLotcontrol(), getCurrentDetails().getInvBroze(), getCurrentDetails().getInvBobin(), getCurrentDetails().getPlant(), getCurrentDetails().getPlatingDate(), getCurrentDetails().getInjectionDate(), getCurrentDetails().getAssyDate(), getCurrentDetails().getOrderType(), getCurrentDetails().getRemark());
        conn = Sql.getConnection("mysql"); //connection SQL
        showModelNotUse = "true";  //set default 
        showModelUse = "false"; //set default
        if (!checkDetails().isEmpty()) {
            for (String objList : checkDetails()) {
                if (currentLotControl.getDetailsIdDetail().getIdDetail().equals(objList)) {
                    showModelNotUse = "false";
                    showModelUse = "true";
                    fc.print("****** show model  use ******* : " + showModelNotUse + "** show module use :" + showModelUse);
                    String model = currentLotControl.getModelPoolId().getModelType() + "-" + currentLotControl.getModelPoolId().getModelSeries() + "-" + currentLotControl.getModelPoolId().getModelName();
                    //getCurrentDefaultData().setModel(model);
                    modelStr = model; //setmodel str
                    break;
                }
            }
        } else {
            showModelNotUse = "true";
            showModelUse = "false";
            fc.print("****** show model Not use ******* : " + showModelNotUse + "** show module use :" + showModelUse);
        }
        itemStr = getDetailSpecialJpaController().findDetailSpecial(getCurrentDetails().getDetailName()).getName();
        itemUse = getDetailSpecialJpaController().findDetailSpecial(getCurrentDetails().getDetailName()).getId();
        loadMasterWorkSheet();
        loadDataSpecialLeft(getDetailSpecialJpaController().findDetailSpecial(getCurrentDetails().getDetailName()).getId());
        loadDataSpecialRight();
        loadDefaultModel();
        loadProcessStatus();
        loadModel();
    }

    public List<String> checkDetails() throws SQLException {
        List<String> lockModel = new ArrayList<String>();
        String sql = Sql.generateSqlCheckDetails();
//        zConnection conn = Sql.getConnection("mysql"); 
        Statement stm = conn.createStatement();
        if (stm.execute(sql)) {
            ResultSet result = stm.executeQuery(sql);
            while (result.next()) {
                lockModel.add(result.getObject(1).toString());
            }
        }
        return lockModel;
    }

    public void loadDefaultModel() {
        fc.print("======== change to load model default ========");

    }
//    load data left

    public void loadDataSpecialLeft(String idSpecial) {
        currentDetailSpecial = getDetailSpecialJpaController().findById(idSpecial);
        List<DetailFieldLeft> objLeft = getDetailFieldLeftJpaController().findByDetialSpecial(currentDetailSpecial);
        if (!objLeft.isEmpty()) {
            for (DetailFieldLeft objList : objLeft) {
                String data = null;
                DetailFieldLeftData dataLeft = getDetailFieldLeftDataJpaController().findDetailFieldLeftData(objList, currentDetails.getIdDetail());
                if (!dataLeft.getIdDetailSpecailData().isEmpty()) {
                    boolean typeinput = typeValue(objList.getIdFieldName().getType());
                    boolean typeSelect = typeSelect(objList.getIdFieldName().getType());
                    if (typeSelect) {
                        loadItemForSelectBox(objList.getIdFieldName());
                        GroupField dataGrioup = getGroupFieldJpaController().findGroupField(dataLeft.getDataDetial());
                        if (dataGrioup == null) {
                            data = dataGrioup.getId();
                        } else {
                            data = "null";
                        }
                        fc.print("======== data ======= : " + data);
                    } else {
                        data = dataLeft.getDataDetial();
                    }
                    getItemMasterLeft().add(new MasterLeftData(getCurrentDetailSpecial(), objList, objList.getIdFieldName().getName(), data, typeinput, typeSelect, itemSelectBox));
                }
            }
        }

    }
//    load data right

    public void loadDataSpecialRight() {
        List<DetailFieldRight> objRight = getDetailFieldRightJpaController().findByDetialSpecial(currentDetailSpecial);
        if (!objRight.isEmpty()) {
            for (DetailFieldRight objList : objRight) {
                String data = null;
                DetailFieldRightDate dateRight = getDetailFieldRightDateJpaController().findDetailFieldLeftData(objList, currentDetails.getIdDetail());
                if (!dateRight.getIdFieldRihgtData().isEmpty()) {
                    boolean typeinput = typeValue(objList.getIdFieldName().getType());
                    boolean typeSelect = typeSelect(objList.getIdFieldName().getType());
                    if (typeSelect) {
                        loadItemForSelectBox(objList.getIdFieldName());
                        GroupField dataGrioup = getGroupFieldJpaController().findGroupField(dateRight.getDataRight());
                        if (dataGrioup != null) {
                            data = dataGrioup.getId();
                        } else {
                            data = "null";
                        }
                    } else {
                        data = dateRight.getDataRight();
                    }
                    getItemMasterRight().add(new MasterRightData(getCurrentDetailSpecial(), objList, objList.getIdFieldName().getName(), data, typeinput, typeSelect, itemSelectBox));
                }
            }
        }
    }
//    date model

    public void previous() {
        getWorksheetpagination().previousPage();
        setMasterModel();
    }

    public void next() {
        getWorksheetpagination().nextPage();
        setMasterModel();
    }

    public int getAllPages() {
        int all = getWorksheetpagination().getItemsCount();
        int pagesize = getWorksheetpagination().getPageSize();
        return ((int) all / pagesize) + 1;
    }

    public PaginationHelper getWorksheetpagination() {
        if (worksheetpagination == null) {
            worksheetpagination = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    //fc.print("size of data : "+getMasterListViewData().size());
                    return getMasterListViewData().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    fc.print("PageFirst : " + getPageFirstItem());
                    fc.print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (getMasterListViewData().size() > 0) {
                        listmodel = new ListDataModel(getMasterListViewData().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(getMasterListViewData());
                    }
                    return listmodel;
                }
            };
        }
        return worksheetpagination;
    }
//    delete master worksheet

    public void changeToDelete(String idDel) {
        fc.print("====== Change to delete ========");
//        String  barcodeDel = getMasterListViewData().get(getMasterModel().getRowIndex()).getBarcode();
        currentLotControl = getLotControlJpaController().findProcessNameAndStatus(idDel);
        currentLotControl.setDeleted(1);
        deleteLotControl();
        resetData();
        findlistViewData();
    }
//    save

    public void changeToSave() {
        if (validateModel == null) { //*** check model.
            fc.print("======= Change To Save ==========");
            getCurrentDetails().setDetailBarcode("");
            getCurrentDetails().setLotcontrol(getCurrentDefaultData().getLotControlQty());
            getCurrentDetails().setModel(getCurrentDefaultData().getModel());
            getCurrentDetails().setModelseries(getCurrentDefaultData().getSeriee());
            getCurrentDetails().setShift("");
            getCurrentDetails().setNumOnmonth(0);
            getCurrentDetails().setUserCreate(userLogin.getSessionUser().getUserName());
            getCurrentDetails().setUserApprove("");
            getCurrentDetails().setDateCreate(new timeCurrent().getDate());
            getCurrentDetails().setDetailType("");
            getCurrentDetails().setLine("");
            getCurrentDetails().setDeleted(0);
//        getCurrentDetails().setApproveStatus(0);
            getCurrentDetails().setOrderNumber(getCurrentDefaultData().getOrderNo());
            getCurrentDetails().setPdQty(getCurrentDefaultData().getProductQty());
            getCurrentDetails().setPdQtyByLot(getCurrentDefaultData().getProductQty());
            getCurrentDetails().setRemark(getCurrentDefaultData().getRemarks());
            getCurrentDetails().setPdCodeName(getCurrentDefaultData().getProductCodeName());
            getCurrentDetails().setPdCodeType(getCurrentDefaultData().getProductType());
            getCurrentDetails().setPlant(getCurrentDefaultData().getPlant());
            getCurrentDetails().setOrderType(getCurrentDefaultData().getOrderType());
            getCurrentDetails().setInjectionDate(new timeCurrent().getDate());
            getCurrentDetails().setPlatingDate(new timeCurrent().getDate());
            getCurrentDetails().setInvBroze(getCurrentDefaultData().getInvBobbin());
            getCurrentDetails().setInvBobin(getCurrentDefaultData().getInvBronze());
            getCurrentDetails().setAssyDate(getCurrentDefaultData().getAssyDate());
            getCurrentDetails().setPdCode(getCurrentDefaultData().getProductCode());
            editDetails();

//        lotcontrol
            // List<LotControl> objListLot = getLotControlJpaController().findByDetailsId(getCurrentDetails());
            for (LotControl objlistLotControl : getLotControlJpaController().findByDetailsId(getCurrentDetails())) {
                fc.print("==== lotcontrol :" + objlistLotControl);
                objlistLotControl.setModelPoolId(getModelPoolJpaController().findModelPool(getCurrentDefaultData().getModel()));
                try {
                    getLotControlJpaController().edit(objlistLotControl);
                } catch (Exception e) {
                    fc.print("==== not to edit model  in lotcontrol ========" + e);
                }
            }
            //left data
            if (!getItemMasterLeft().isEmpty()) {
                for (MasterLeftData objList : getItemMasterLeft()) {
                    prepareDetialFileLeftData();
                    currentFieldLeftData = getDetailFieldLeftDataJpaController().findDetailFieldLeftData(objList.getIndexAttribute(), currentDetails.getIdDetail());
                    getCurrentFieldLeftData().setDataDetial(objList.getData());
                    editLeftData();
                }
            }
            //master data right
            if (!getItemMasterRight().isEmpty()) {
                for (MasterRightData objList : getItemMasterRight()) {
                    prepareDetailFieldRightData();
                    currentFieldRightData = getDetailFieldRightDateJpaController().findDetailFieldLeftData(objList.getIndexAttribute(), currentDetails.getIdDetail());
                    getCurrentFieldRightData().setDataRight(objList.getData());
                    editRightData();
                }
            }
            resetData();
            findlistViewData();
            String link = "pages/masterworksheet/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        }
    }
    //edit left data

    public void editLeftData() {
        try {
            getDetailFieldLeftDataJpaController().edit(currentFieldLeftData);
        } catch (Exception e) {
            fc.print("==== Error not to edit left data ====== :" + e);
        }
    }
    //edit right data

    public void editRightData() {
        try {
            getDetailFieldRightDateJpaController().edit(currentFieldRightData);
        } catch (Exception e) {
            fc.print("==== Error not to edit right data ===== :" + e);
        }
    }

    public void editDetails() {
        try {
            getDetailsJpaController().edit(currentDetails);
        } catch (Exception e) {
            fc.print("Error not to update data");
        }
    }

    public void deleteLotControl() {
        try {
            getLotControlJpaController().edit(getCurrentLotControl());
        } catch (Exception e) {
            fc.print("==== Error not to delete lot control ====");
        }
    }
//print check report
    boolean print_all = false;
    public void printAll() {
        if (!print_all) {
            fc.print("====== print report  all ========");
            fc.print("Barcode for print id :");
            fc.print("============ print report ============");
            String sql = "SELECT "
                    + "lot_control.`barcode` AS lot_control_barcode,"
                    + "process_pool.`proc_name` AS process_pool_proc_name,"
                    + "model_pool.`model_series` AS model_pool_model_series,"
                    + "CONCAT(model_pool.`model_type`,'-',model_pool.`model_group`,'-',model_pool.`model_name`) AS model_pool_model_name,"
                    + "details.`order_number` AS details_order_number,"
                    + "details.`pd_qty` AS details_pd_qty,"
                    + "details.`inv_bobin` AS details_inv_bobin,"
                    + "details.`inv_broze` AS details_inv_broze,"
                    + "details.`injection_date` AS details_injection_date,"
                    + "details.`order_type` AS details_order_type,"
                    + "details.`plant` AS details_plant,"
                    + "details.`pd_code_type` AS details_pd_code_type,"
                    + "details.`pd_code_name` AS details_pd_code_name,"
                    + "details.`plating_date` AS details_plating_date,"
                    + "details.`assy_date` AS details_assy_date "
                    + "FROM "
                    + "`md2pc` md2pc INNER JOIN `process_pool` process_pool ON md2pc.`process_pool_id_proc` = process_pool.`id_proc` "
                    + "INNER JOIN `model_pool` model_pool ON md2pc.`model_pool_id` = model_pool.`id` "
                    + "INNER JOIN `lot_control` lot_control ON model_pool.`id` = lot_control.`model_pool_id` "
                    + "INNER JOIN `details` details ON lot_control.`details_id_detail` = details.`id_detail` "
                    + "WHERE "
                    //+"lot_control.`barcode` in '"+getCurrentLotControl().getBarcode()+"';";
                    + "lot_control.`barcode` in (";
            for (MasterLinkImageAllPrint objList : barcodeImage) {

                sql += "'" + objList.getBarcode() + "',";
            }
            sql += "'') and md2pc.`deleted` = 0 and process_pool.`type` ='" + itemUse + "' ORDER BY lot_control.barcode,md2pc.`sq_process` ASC ;";
            fc.print(sql);

            if (new ReportBean().pdfSelect("/report/reportAllPrintWorkSheet.jasper", "/report/all_" + getDetailSpecialJpaController().findDetailSpecial(getCurrentDetails().getDetailName()).getName() + ".pdf", sql)) {
                System.out.println("complete");
                print_all =true;
                /*
                 * url ="window.open(\"http://www.w3schools.com\");";
                 * top.preview_html(
                 *
                 */
                urlAll = "function printAllPdf(){ alert('Print');"
                        + " window.open('./report/all_" + getDetailSpecialJpaController().findDetailSpecial(getCurrentDetails().getDetailName()).getName() + ".pdf'); } printAllPdf();";
            }
        }
        else
        {
            urlAll = urlAll+" ";
        }
    }
//print all report    
    boolean print_check   = false;
    public void printForCheck() {
        if (!print_check) {
            fc.print("====== print report check  true========");

            String sql = "SELECT "
                    + "lot_control.`barcode` AS lot_control_barcode,"
                    + "process_pool.`proc_name` AS process_pool_proc_name,"
                    + "model_pool.`model_series` AS model_pool_model_series,"
                    + "CONCAT(model_pool.`model_type`,'-',model_pool.`model_group`,'-',model_pool.`model_name`) AS model_pool_model_name,"
                    + "details.`order_number` AS details_order_number,"
                    + "details.`pd_qty` AS details_pd_qty,"
                    + "details.`inv_bobin` AS details_inv_bobin,"
                    + "details.`inv_broze` AS details_inv_broze,"
                    + "details.`injection_date` AS details_injection_date,"
                    + "details.`order_type` AS details_order_type,"
                    + "details.`plant` AS details_plant,"
                    + "details.`pd_code_type` AS details_pd_code_type,"
                    + "details.`pd_code_name` AS details_pd_code_name,"
                    + "details.`plating_date` AS details_plating_date,"
                    + "details.`assy_date` AS details_assy_date "
                    + "FROM "
                    + "`md2pc` md2pc INNER JOIN `process_pool` process_pool ON md2pc.`process_pool_id_proc` = process_pool.`id_proc` "
                    + "INNER JOIN `model_pool` model_pool ON md2pc.`model_pool_id` = model_pool.`id` "
                    + "INNER JOIN `lot_control` lot_control ON model_pool.`id` = lot_control.`model_pool_id` "
                    + "INNER JOIN `details` details ON lot_control.`details_id_detail` = details.`id_detail` "
                    + "WHERE "
                    //+"lot_control.`barcode` in '"+getCurrentLotControl().getBarcode()+"';";
                    + "lot_control.`barcode` in (";
            for (MasterLinkImageAllPrint objList : barcodeImage) {
                if (objList.isPrint()) {
                    sql += "'" + objList.getBarcode() + "',";
                }
            }
            sql += "'') and md2pc.`deleted` = 0 and process_pool.`type` ='" + itemUse + "' ORDER BY lot_control.barcode,md2pc.`sq_process` ASC ;";
            fc.print(sql);
            if (!barcodeImage.isEmpty()) {
                if (new ReportBean().pdfSelect("/report/reportAllPrintWorkSheet.jasper", "/report/check_" + getDetailSpecialJpaController().findDetailSpecial(getCurrentDetails().getDetailName()).getName() + ".pdf", sql)) {
                    System.out.println("complete");
                    print_check  = true;
                    /*
                     * url ="window.open(\"http://www.w3schools.com\");";
                     * top.preview_html(
                     *
                     */
                    urlCheck = "function printCheckPdf(){ alert('Print');"
                            + " window.open('./report/check_" + getDetailSpecialJpaController().findDetailSpecial(getCurrentDetails().getDetailName()).getName() + ".pdf'); } printCheckPdf();";
                }
            }
        }
        else{
             urlCheck=urlCheck+" ";
        }
    }
//    print report worksheet
    boolean print_status=false;
    public void printFrontlineSelection() {
        
        if(!print_status){
        fc.print("============ print report ============");
        String sql = "SELECT "
                + "lot_control.`barcode` AS lot_control_barcode,"
                + "process_pool.`proc_name` AS process_pool_proc_name,"
                + "model_pool.`model_series` AS model_pool_model_series,"
                + "CONCAT(model_pool.`model_type`,'-',model_pool.`model_group`,'-',model_pool.`model_name`) AS model_pool_model_name,"
                + "details.`order_number` AS details_order_number,"
                + "details.`pd_qty` AS details_pd_qty,"
                + "details.`inv_bobin` AS details_inv_bobin,"
                + "details.`inv_broze` AS details_inv_broze,"
                + "details.`injection_date` AS details_injection_date,"
                + "details.`order_type` AS details_order_type,"
                + "details.`plant` AS details_plant,"
                + "details.`pd_code_type` AS details_pd_code_type,"
                + "details.`pd_code_name` AS details_pd_code_name,"
                + "details.`plating_date` AS details_plating_date,"
                + "details.`assy_date` AS details_assy_date "
                + "FROM "
                + "`md2pc` md2pc INNER JOIN `process_pool` process_pool ON md2pc.`process_pool_id_proc` = process_pool.`id_proc` "
                + "INNER JOIN `model_pool` model_pool ON md2pc.`model_pool_id` = model_pool.`id` "
                + "INNER JOIN `lot_control` lot_control ON model_pool.`id` = lot_control.`model_pool_id` "
                + "INNER JOIN `details` details ON lot_control.`details_id_detail` = details.`id_detail` "
                + "WHERE "
                + "lot_control.`barcode` = '" + getCurrentLotControl().getBarcode() + "' and md2pc.`deleted` = 0 and process_pool.`type` ='" + itemUse + "' ORDER BY md2pc.`sq_process` ASC;";
        if (new ReportBean().pdfSelect("/report/reportAllWorkSheet.jasper", "/report/" + getDetailSpecialJpaController().findDetailSpecial(getCurrentDetails().getDetailName()).getName() + ".pdf", sql)) {
            System.out.println("complete");
            print_status=true;
            /*
             * url ="window.open(\"http://www.w3schools.com\");";
             * top.preview_html(
             *
             */
            fc.print("************* Report priview ***************");
            
            url = "function printpdf(){ alert('Print');"
                    + " window.open('./report/" + getDetailSpecialJpaController().findDetailSpecial(getCurrentDetails().getDetailName()).getName() + ".pdf'); }  printpdf();";
        }
        }else {
            url=url+" ";
        }
    }
//    changet to view

    public void changeToViewDetail(String idView) {
        fc.print("======== change to view ===========");
        String link = "pages/masterworksheet/detailview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
//        resetData();
//        String  barcodePrint = getMasterListViewData().get(getMasterModel().getRowIndex()).getBarcode();
        currentLotControl = getLotControlJpaController().findProcessNameAndStatus(idView);
        currentDetails = currentLotControl.getDetailsIdDetail();
        currentDefaultData = new MasterDefaultData(getCurrentDetails().getIdDetail(), getCurrentDetails().getOrderNumber(), getCurrentDetails().getPdCodeType(), getCurrentDetails().getPdCodeName(), getCurrentDetails().getPdCode(), getCurrentLotControl().getModelPoolId().getModelType() + "-" + getCurrentLotControl().getModelPoolId().getModelSeries() + "-" + getCurrentLotControl().getModelPoolId().getModelName(), getCurrentLotControl().getModelPoolId().getModelSeries(), getCurrentDetails().getPdQty(), getCurrentDetails().getLotcontrol(), getCurrentDetails().getInvBroze(), getCurrentDetails().getInvBobin(), getCurrentDetails().getPlant(), getCurrentDetails().getPlatingDate(), getCurrentDetails().getInjectionDate(), getCurrentDetails().getAssyDate(), getCurrentDetails().getOrderType(), getCurrentDetails().getRemark());
        itemUse = getDetailSpecialJpaController().findDetailSpecial(getCurrentDetails().getDetailName()).getName();
//        if(currentDefaultData.getOrderType().equals("F"))
//        {
//            currentDefaultData.setOrderType("Frame");
//        }
//        if(currentDefaultData.getOrderType().equals("R"))
//        {
//            currentDefaultData.setOrderType("Roll");
//        }
        loadMasterWorkSheet();
        loadDataSpecialLeftPrint(getDetailSpecialJpaController().findDetailSpecial(getCurrentDetails().getDetailName()).getId());
        loadDataSpecialRightPrint();
    }

    private void resetData() {
        worksheetpagination = null;
        controlLot = "false";
        modelItem = new ArrayList<SelectItem>();
        currentProcessStatus = new ArrayList<ProcList>();
        barcodeImage = new ArrayList<MasterLinkImageAllPrint>();
        itemUse = "null";
        itemTypeSpecial = new ArrayList<SelectItem>();
        itemMasterLeft = new ArrayList<MasterLeftData>();
        itemMasterRight = new ArrayList<MasterRightData>();
        currentDetails = new Details();
        currentFieldRightData = new DetailFieldRightDate();
        currentFieldLeftData = new DetailFieldLeftData();
        masterListViewData = new ArrayList<MasterListViewData>();
        masterModel = new ArrayDataModel();
        currentDefaultData = new MasterDefaultData(null, null, null, "null", null, "null", null, null, null, null, null, null, null, null, null, null, null);
        //itemTypeSpecial   = new ArrayList<SelectItem>();
    }

    public List<DetailSpecial> getObjTypeMaster() {
        if (objTypeMaster == null) {
            objTypeMaster = new ArrayList<DetailSpecial>();
        }
        return objTypeMaster;
    }

    public void setObjTypeMaster(List<DetailSpecial> objTypeMaster) {
        this.objTypeMaster = objTypeMaster;
    }

    public List<SelectItem> getItemTypeSpecial() {
        if (itemTypeSpecial == null) {
            itemTypeSpecial = new ArrayList<SelectItem>();
        }
        return itemTypeSpecial;
    }

    public void setItemTypeSpecial(List<SelectItem> itemTypeSpecial) {
        this.itemTypeSpecial = itemTypeSpecial;
    }

    public String getItemUse() {
        return itemUse;
    }

    public void setItemUse(String itemUse) {
        this.itemUse = itemUse;
    }

    public List<MasterLeftData> getItemMasterLeft() {
        if (itemMasterLeft == null) {
            itemMasterLeft = new ArrayList<MasterLeftData>();
        }
        return itemMasterLeft;
    }

    public void setItemMasterLeft(List<MasterLeftData> itemMasterLeft) {
        this.itemMasterLeft = itemMasterLeft;
    }

    public List<MasterRightData> getItemMasterRight() {
        if (itemMasterRight == null) {
            itemMasterRight = new ArrayList<MasterRightData>();
        }
        return itemMasterRight;
    }

    public void setItemMasterRight(List<MasterRightData> itemMasterRight) {
        this.itemMasterRight = itemMasterRight;
    }

    public DetailSpecial getCurrentDetailSpecial() {
        return currentDetailSpecial;
    }

    public void setCurrentDetailSpecial(DetailSpecial currentDetailSpecial) {
        this.currentDetailSpecial = currentDetailSpecial;
    }

    public List<SelectItem> getItemSelectBox() {
        if (itemSelectBox == null) {
            itemSelectBox = new ArrayList<SelectItem>();
        }
        return itemSelectBox;
    }

    public void setItemSelectBox(List<SelectItem> itemSelectBox) {
        this.itemSelectBox = itemSelectBox;
    }

    public Details getCurrentDetails() {
        return currentDetails;
    }

    public void setCurrentDetails(Details currentDetails) {
        this.currentDetails = currentDetails;
    }

    public MasterDefaultData getCurrentDefaultData() {
        if (currentDefaultData == null) {
            currentDefaultData = new MasterDefaultData(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        }
        return currentDefaultData;
    }

    public void setCurrentDefaultData(MasterDefaultData currentDefaultData) {
        this.currentDefaultData = currentDefaultData;
    }

    public DetailFieldLeftData getCurrentFieldLeftData() {
        if (currentFieldLeftData == null) {
            currentFieldLeftData = new DetailFieldLeftData();
        }
        return currentFieldLeftData;
    }

    public void setCurrentFieldLeftData(DetailFieldLeftData currentFieldLeftData) {
        this.currentFieldLeftData = currentFieldLeftData;
    }

    public DetailFieldRightDate getCurrentFieldRightData() {
        if (currentFieldRightData == null) {
            currentFieldRightData = new DetailFieldRightDate();
        }
        return currentFieldRightData;
    }

    public void setCurrentFieldRightData(DetailFieldRightDate currentFieldRightData) {
        this.currentFieldRightData = currentFieldRightData;
    }

    public LotControl getCurrentLotControl() {
        if (currentLotControl == null) {
            currentLotControl = new LotControl();
        }
        return currentLotControl;
    }

    public void setCurrentLotControl(LotControl currentLotControl) {
        this.currentLotControl = currentLotControl;
    }

    public DataModel getMasterModel() {
        return masterModel;
    }

    public void setMasterModel() {
        this.masterModel = getWorksheetpagination().createPageDataModel();
    }

    public List<MasterListViewData> getMasterListViewData() {
        if (masterListViewData == null) {
            masterListViewData = new ArrayList<MasterListViewData>();
        }
        return masterListViewData;
    }

    public void setMasterListViewData(List<MasterListViewData> masterListViewData) {
        this.masterListViewData = masterListViewData;
    }

    public List<MasterLinkImageAllPrint> getBarcodeImage() {
        if (barcodeImage == null) {
            barcodeImage = new ArrayList<MasterLinkImageAllPrint>();
        }
        return barcodeImage;
    }

    public void setBarcodeImage(List<MasterLinkImageAllPrint> barcodeImage) {
        this.barcodeImage = barcodeImage;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<ProcList> getCurrentProcessStatus() {
        if (currentProcessStatus == null) {
            currentProcessStatus = new ArrayList<ProcList>();
        }
        return currentProcessStatus;
    }

    public void setCurrentProcessStatus(List<ProcList> currentProcessStatus) {
        this.currentProcessStatus = currentProcessStatus;
    }
    //=================================
    private String barcodeinput;
    private String barcode;
    private String model;
    // test //
    private int pdqty;
    private int lotqty = 1;
    //======//
    private int qtyoutput;
    private int qtywip;
    private int leftqty;
    private List<BarcodeSelected> barcodeSelecteds;
    private LotControl lotControl;
    private String validateTypeWorkSheet = null;

    private void conTrollotResetData() {
        pdqty = 0;
        barcode = "";
        model = "";
        barcodeinput = "";
        qtyoutput = 0;
        qtywip = 0;
        leftqty = 0;
        lotqty = 1;
        lotControl = null;
        barcodeSelecteds = null;
    }
    private Front2backMaster front2backMaster;

    private Front2backMaster getFront2backMaster() {
        if (front2backMaster == null) {
            front2backMaster = new Front2backMaster(UUID.randomUUID().toString());
        }
        return front2backMaster;
    }
//    private LotControlJpaController lotControlJpaController;
//    private LotControlJpaController getLotControlJpaController(){
//        if(lotControlJpaController == null){
//            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
//        }
//        return lotControlJpaController;
//    }
    private Front2backMasterJpaController front2backMasterJpaController;

    private Front2backMasterJpaController getFront2backMasterJpaController() {
        if (front2backMasterJpaController == null) {
            front2backMasterJpaController = new Front2backMasterJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return front2backMasterJpaController;
    }

//    private DetailsJpaController detailsJpaController;
//    private DetailsJpaController getDetailsJpaController(){
//        if(detailsJpaController == null){
//            detailsJpaController = new DetailsJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
//        }
//        return detailsJpaController;
//    }
    public void changToCreateLot() {
        fc.print("=========== chang to create lot ==============================");
        try {
            LotControl lotcontrol;
            Users u = userLogin.getSessionUser();
            if (getBarcodeSelecteds().size() > 0) {

//                Details details = getCurrentDetails();
//                details.setDateCreate(new Date());
//                details.setDeleted(0);
//                details.setDetailName("test");
//                details.setDetailType("test");
//                details.setLine("");
//                details.setLotcontrol("");
//                details.setModel("");
//                details.setModelseries("");
//                details.setUserCreate(u.getId());
//                details.setUserApprove(u.getId());
//                details.setShift("a");
//                details.setRemark("");
//                details.setPdQtyByLot("");
//                details.setPdQty("");
//                details.setOrderNumber("");
//                details.setNumOnmonth(0);
//                
//                fc.print(details.toString());
//                
//                getDetailsJpaController().create(details);

                for (BarcodeSelected barcodeSelected : getBarcodeSelecteds()) {
                    lotcontrol = barcodeSelected.getLotControl();
                    getLotControlJpaController().edit(lotcontrol);
                    fc.print(lotcontrol.getBarcode());
                    getFront2backMaster().setDateCreate(new Date());
                    getFront2backMaster().setDeleted(0);
                    getFront2backMaster().setDetailsId(getCurrentDetails()); // details object.
                    getFront2backMaster().setLotcontrolId(lotcontrol);  // lotcontrol object.
                    getFront2backMaster().setUserCreate(u.getId());
                    getFront2backMaster().setPdQty(barcodeSelected.getQty() + "");

                    getFront2backMasterJpaController().create(getFront2backMaster());
                    front2backMaster = null;
                    fc.print("Create lot complete");
                }
            }
            conTrollotResetData();
        } catch (Exception e) {
            fc.print("Error : " + e);
        }
    }

    public void changTodelLot(int j) {
        fc.print("=========== changtodel lot : " + j + " ===========================");
        try {
            BarcodeSelected barcodeSelected = null;
            for (BarcodeSelected barcodeSelected1 : getBarcodeSelecteds()) {
                if (barcodeSelected1.getIndex() == j) {
                    getBarcodeSelecteds().remove(barcodeSelected1);
                    barcodeSelected = barcodeSelected1;
                    break;
                }
            }
            fc.print("Remove barcode : " + barcodeSelected.getBarcode());
        } catch (Exception e) {
            fc.print("Error : " + e);
        }
    }

    public void changTodelLotByEdit(int j) {
        fc.print("=========== changtodel lot : " + j + " ===========================");
        try {
            BarcodeSelected barcodeSelected = null;
            for (BarcodeSelected barcodeSelected1 : getBarcodeSelecteds()) {
                if (barcodeSelected1.getIndex() == j) {
                    getBarcodeSelecteds().remove(barcodeSelected1);
                    barcodeSelected = barcodeSelected1;
                    lotControl = barcodeSelected.getLotControl();
                    int qtylot = lotControl.getWipQty();
                    int resulm = qtylot + barcodeSelected.getQty();
                    lotControl.setWipQty(resulm);
                    getLotControlJpaController().edit(lotControl);
                    break;
                }
            }
            fc.print("Remove barcode : " + barcodeSelected.getBarcode());
        } catch (Exception e) {
            fc.print("Error : " + e);
        }
    }
    private int i = 0;

    public void changToSelectedLot() {
        fc.print("=========== select lot " + lotControl.getBarcode() + " ===========");
        try {
            int qty = 0;
            int pdqtys = Integer.parseInt(getCurrentDefaultData().getProductQty()) * Integer.parseInt(getCurrentDefaultData().getLotControlQty());
            fc.print("pdqtys : " + pdqtys);
            if (getBarcodeSelecteds().isEmpty()) {
                if (lotControl.getWipQty() >= pdqtys) {
                    int result = lotControl.getWipQty() - pdqtys;
                    qty = pdqtys;
                    lotControl.setWipQty(result);
                    leftqty = 0;
                } else {
                    leftqty = pdqtys - lotControl.getWipQty();
                    qty = lotControl.getWipQty();
                    lotControl.setWipQty(0);
                }
            } else {
                if (lotControl.getWipQty() >= leftqty && leftqty >= 0) {
                    int result = lotControl.getWipQty() - leftqty;
                    lotControl.setWipQty(result);
                    qty = leftqty;
                    leftqty = 0;
                } else {
                    leftqty = leftqty - lotControl.getWipQty();
                    qty = lotControl.getWipQty();
                    lotControl.setWipQty(0);
                }
            }
            qtywip = lotControl.getWipQty();
            fc.print("QTY : " + qty);
            fc.print("QTY wip : " + qtywip);
            fc.print("QTY left : " + leftqty);
            if (qty > 0) {
                getBarcodeSelecteds().add(new BarcodeSelected(i++, barcode, model, qty, lotControl));
            } else {
                fc.print("Does not have pdqty.");
            }
        } catch (Exception e) {
            fc.print("Error : " + e);
        }
    }

    private void changTobarcodeInput(String barcodeinString) {
        fc.print("=========== barcode chang value ==============================");
        if (validateBarcodeControl()) {
            try {
                barcodeinput = barcodeinString;
                fc.print("batcode new : " + barcodeinput);
                lotControl = getLotControlJpaController().findProcessNameAndStatus(barcodeinput);
                if (lotControl != null && checkDoubly(lotControl)) {
                    ModelPool modelPool = lotControl.getModelPoolId();
                    model = "" + modelPool.getModelGroup() + "-" + modelPool.getModelSeries() + "-" + modelPool.getModelName();
                    fc.print("Model : " + model);
                    barcode = lotControl.getBarcode();
                    fc.print("Barcode : " + barcode);
                    qtyoutput = lotControl.getQtyOutput();
                    fc.print("qtyOutput : " + qtyoutput);
                    qtywip = lotControl.getWipQty();
                    fc.print("qtyWip : " + qtywip);
                    barcodeinput = null;
                } else {
                    fc.print("Error : You don't have barcode '" + barcodeinput + "' in lotcontrol table.");
                    barcodeinput = null;
                }
            } catch (Exception e) {
                fc.print("= Error : " + e);
            }
        }
    }
    private MasterSearch currentSearch;

//    search by barcode
    public void searchByBarcode() {
        fc.print("====== Search  by  barcode =========="); 
        List<LotControl> objLot = getLotControlJpaController().searchByBaroce(currentSearch.getBarcode());
        if (!objLot.isEmpty()) {
            for (LotControl objList : objLot) {
                MainData objManData = getMainDataJpaController().findMainData(objList.getMainDataId());
                String process = "";
                String status = "";
                String orderNumber = objList.getDetailsIdDetail().getOrderNumber();
                if (objManData != null) {
                    process = objManData.getIdProc().getProcName();
                    status = getCurrentProcessJpaController().findCurrentProcess(objManData.getCurrentNumber()).getStatus();
                }
                String model = objList.getModelPoolId().getModelType() + "-" + objList.getModelPoolId().getModelSeries() + "-" + objList.getModelPoolId().getModelName();
                getMasterListViewData().add(new MasterListViewData(objList.getId(), objList.getBarcode(), model, objList.getModelPoolId().getModelSeries(), objList.getDetailsIdDetail().getDateCreate().toString(), process, status, orderNumber));

            }
            setMasterModel();
        }
    }
//    search by process

    public void searchByProcess() {
        fc.print("====== Search by process ==========");
        List<ProcessPool> objPool = getProcessPoolJpaController().findProcessPoolByName(getCurrentSearch().getProcess());
        if (!objPool.isEmpty()) {
            for (ProcessPool objList : objPool) {
                List<MainData> objMainData = getMainDataJpaController().findMainDataByProcess(objList);
                if (!objMainData.isEmpty()) {
                    for (MainData objList2 : objMainData) {
                        LotControl objList3 = objList2.getLotControlId();
                        String orderNum = objList3.getDetailsIdDetail().getOrderNumber();
                        String model = objList3.getModelPoolId().getModelType() + "-" + objList3.getModelPoolId().getModelSeries() + "-" + objList3.getModelPoolId().getModelName();
                        String status = getCurrentProcessJpaController().findCurrentProcess(objList2.getCurrentNumber()).getStatus();
                        getMasterListViewData().add(new MasterListViewData(objList3.getId(), objList3.getBarcode(), model, objList3.getModelPoolId().getModelSeries(), objList3.getDetailsIdDetail().getDateCreate().toString(), objList2.getIdProc().getProcName(), status, orderNum));
                    }
                }
            }
            setMasterModel();
        }
        // List<MainData> objprocess = getMainDataJpaController().f
    }
//    search by  worksheet type

    public void searchByOrderNum() {
        fc.print("====== search by order number ===== : " + currentSearch.getOrderNo());
        List<Details> objDetails = getDetailsJpaController().findByOrderNum(currentSearch.getOrderNo());
        if (!objDetails.isEmpty()) {
            for (Details objList : objDetails) {
                List<LotControl> objList2 = getLotControlJpaController().findByDetailsIdOrderbyLotnumber(objList);
                if (!objList2.isEmpty()) {
                    for (LotControl objList3 : objList2) {
                        ModelPool oblModelPool = getModelPoolJpaController().findModelPool(objList3.getModelPoolId().getId());
                        String model = oblModelPool.getModelGroup() + "-" + oblModelPool.getModelSeries() + "-" + oblModelPool.getModelName();
                        String series = oblModelPool.getModelSeries();
                        String process = "";
                        String status = "";
                        String orderNum = objList3.getDetailsIdDetail().getOrderNumber();
                        if (!objList3.getMainDataId().isEmpty() && objList3.getMainDataId() != null) {
                            MainData objMainData = getMainDataJpaController().findMainData(objList3.getMainDataId());
                            if (!objMainData.getId().isEmpty() && objMainData.getId() != null) {
                                process = objMainData.getIdProc().getProcName();
                                status = getCurrentProcessJpaController().findCurrentProcess(objMainData.getCurrentNumber()).getStatus();
                            }
                        }
                        String orderNumber = objList3.getDetailsIdDetail().getOrderNumber();

                        String barcode = objList3.getBarcode();
                        getMasterListViewData().add(new MasterListViewData(objList3.getId(), barcode, model, series, objList3.getDetailsIdDetail().getDateCreate().toString(), process, status, orderNum));
                    }
                }
            }
        }
        setMasterModel();
    }

    public void searchCompear(){
       
    }
    
    
    public void searchByModel() {

//        List<Details> detailslist = getDetailsJpaController().findDetailsByModelAndDetailNameAuto(modelSearch);
        List<ModelPool> modelPoolslist = getModelPoolJpaController().findByModelname(currentSearch.getModel());
        System.out.println("Model : name : "+currentSearch.getModel());
        if (modelPoolslist.isEmpty()) {
            setMasterModel();
        } else {
            for (ModelPool modelpool : modelPoolslist) {
                List<LotControl> lotControllist = (List<LotControl>) modelpool.getLotControlCollection();
                for (LotControl objList1 : lotControllist) {
                    try {
                        if (objList1.getDeleted() == 0) {
//                        currentdetails = lotControl.getDetailsIdDetail();
//                        if (currentdetails.getDetailName().equals("line_assembly") && currentdetails.getDeleted() == 0) {
                            String series = objList1.getModelPoolId().getModelSeries();
                            String model = objList1.getModelPoolId().getModelType() + "-" + objList1.getModelPoolId().getModelSeries() + "-" + objList1.getModelPoolId().getModelName();
                            String process = "";
                            String status = "";
                            String orderNumber = objList1.getDetailsIdDetail().getOrderNumber();
                            if (objList1.getMainDataId() != null) {

                                MainData mainData = getMainDataJpaController().findMainData(objList1.getMainDataId());
                                ProcessPool processPool = mainData.getIdProc();
                                CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(mainData.getCurrentNumber());
                                status = currentProcess.getStatus();
                                process = mainData.getIdProc().getProcName();
                                getMasterListViewData().add(new MasterListViewData(objList1.getId(), objList1.getBarcode(), model, series, objList1.getDetailsIdDetail().getDateCreate().toString(), process, status, orderNumber));
//                             print("= Detail : " + currentdetails.getDetailName() + " :: Barcode : " + lotControl.getBarcode());
                            } else {
                                getMasterListViewData().add(new MasterListViewData(objList1.getId(), objList1.getBarcode(), model, series, objList1.getDetailsIdDetail().getDateCreate().toString(), process, status, orderNumber));
//                                print("Null");
//                            }
                            }
                        }
                    } catch (Exception e) {
                        fc.print("= Error : " + e);
                    }
                }
            }
        }
        setMasterModel();
    }
//    search by series

    public void searchByseries() {
        List<ModelPool> modelPoolslist = getModelPoolJpaController().findModelPoolBySeries(getCurrentSearch().getSeries());
        if (modelPoolslist.isEmpty()) {
            setMasterModel();
        }
        for (ModelPool modelpool : modelPoolslist) {
            List<LotControl> lotControllist = (List<LotControl>) modelpool.getLotControlCollection();
            for (LotControl objList1 : lotControllist) {
                try {
                    if (objList1.getDeleted() == 0) {
//                        currentdetails = lotControl.getDetailsIdDetail();
//                        if (currentdetails.getDetailName().equals("line_assembly") && currentdetails.getDeleted() == 0) {
                        String series = objList1.getModelPoolId().getModelSeries();
                        String model = objList1.getModelPoolId().getModelType() + "-" + objList1.getModelPoolId().getModelSeries() + "-" + objList1.getModelPoolId().getModelName();
                        String process = "";
                        String status = "";
                        String orderNumber = objList1.getDetailsIdDetail().getOrderNumber();
                        if (objList1.getMainDataId() != null) {

                            MainData mainData = getMainDataJpaController().findMainData(objList1.getMainDataId());
                            ProcessPool processPool = mainData.getIdProc();
                            CurrentProcess currentProcess = getCurrentProcessJpaController().findCurrentProcess(mainData.getCurrentNumber());
                            status = currentProcess.getStatus();
                            process = mainData.getIdProc().getProcName();
                            getMasterListViewData().add(new MasterListViewData(objList1.getId(), objList1.getBarcode(), model, series, objList1.getDetailsIdDetail().getDateCreate().toString(), process, status, orderNumber));
//                             print("= Detail : " + currentdetails.getDetailName() + " :: Barcode : " + lotControl.getBarcode());
                        } else {
                            getMasterListViewData().add(new MasterListViewData(objList1.getId(), objList1.getBarcode(), model, series, objList1.getDetailsIdDetail().getDateCreate().toString(), process, status, orderNumber));
//                                print("Null");
//                            }
                        }
                    }
                } catch (Exception e) {
                    fc.print("= Error : " + e);
                }
            }
        }
        setMasterModel();
    }

    public void changeToSearch() {
        resetData();
        fc.print("======== Chage to search===========");
        if (currentSearch.getBarcode() != null && !currentSearch.getBarcode().isEmpty()) {
            searchByBarcode();
        } else if (currentSearch.getProcess() != null && !currentSearch.getProcess().isEmpty()) {
            searchByProcess();
        } else if (currentSearch.getOrderNo() != null && !currentSearch.getOrderNo().isEmpty()) {
            searchByOrderNum();
        } else if (currentSearch.getModel() != null && !currentSearch.getModel().isEmpty()) {
            searchByModel();
        } else if (currentSearch.getSeries() != null && !currentSearch.getSeries().isEmpty()) {
            searchByseries();
        } else {
            findlistViewData();
        }
        loadMasterWorkSheet();
        resetSearch();
    }

    public void resetSearch() {
        currentSearch.setIndex(gemId());
        currentSearch.setBarcode(null);
        currentSearch.setModel(null);
        currentSearch.setProcess(null);
        currentSearch.setSeries(null);
        currentSearch.setWorkSheetType("null");
        currentSearch.setOrderNo(null);
    }

    private boolean checkDoubly(LotControl lotControl) {
        for (BarcodeSelected barcodeSelected : getBarcodeSelecteds()) {
            if (barcodeSelected.getLotControl().equals(lotControl)) {
                return false;
            }
        }
        return true;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getQtyoutput() {
        return qtyoutput;
    }

    public void setQtyoutput(int qtyoutput) {
        this.qtyoutput = qtyoutput;
    }

    public int getQtywip() {
        return qtywip;
    }

    public void setQtywip(int qtywip) {
        this.qtywip = qtywip;
    }

    public String getBarcodeinput() {
        return barcodeinput;
    }
    //validate barcode

    public boolean validateBarcodeControl() {
        boolean check = false;
        String typeWorkSheet = "";
        String typeWorkSheet2 = "";
        if (itemUse.equals("null")) {
            typeWorkSheet = "";
            typeWorkSheet2 = "";
        } else {
            typeWorkSheet = getDetailSpecialJpaController().findDetailSpecial(itemUse).getInputType();
            typeWorkSheet2 = getDetailSpecialJpaController().findDetailSpecial(typeWorkSheet).getLineCode();
        }
        if (getBarcodeinput() != null) {
            String typeBarcode[] = barcodeinput.split("-");
            if (typeWorkSheet2.equals(typeBarcode[0])) {
                validateTypeWorkSheet = "";
                fc.print(" equal ============== ");
                check = true;
            } else {
                validateTypeWorkSheet = "Check type Work Sheet";
                fc.print("not  equal ============== ");
                barcodeinput = null;
                check = false;
            }
        }
        return check;
    }

    public void setBarcodeinput(String barcodeinput) {
        this.barcodeinput = barcodeinput;
        if (barcodeinput != null && !barcodeinput.isEmpty()) {
            changTobarcodeInput(barcodeinput);
        }
    }

    public LotControl getLotControl() {
        return lotControl;
    }

    public List<BarcodeSelected> getBarcodeSelecteds() {
        if (barcodeSelecteds == null) {
            barcodeSelecteds = new ArrayList<BarcodeSelected>();
        }
        return barcodeSelecteds;
    }

    public int getLeftqty() {
        return leftqty;
    }

    public void setLeftqty(int leftqty) {
        this.leftqty = leftqty;
    }

// test //
    public int getPdqty() {
        return pdqty;
    }

    public void setPdqty(int pdqty) {
        this.pdqty = pdqty;
    }

    public int getLotqty() {
        return lotqty;
    }

    public void setLotqty(int lotqty) {
        this.lotqty = lotqty;
    }

    public BarCodeGenerter getBarCodeGenerter() {
        return barCodeGenerter;
    }

    public void setBarCodeGenerter(BarCodeGenerter barCodeGenerter) {
        this.barCodeGenerter = barCodeGenerter;
    }

    public MasterSearch getCurrentSearch() {
        if (currentSearch == null) {
            currentSearch = new MasterSearch(null, null, null, "null", null, null, null);
        }
        return currentSearch;
    }

    public void setCurrentSearch(MasterSearch currentSearch) {
        this.currentSearch = currentSearch;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public String getValidateTypeWorkSheet() {
        return validateTypeWorkSheet;
    }

    public void setValidateTypeWorkSheet(String validateTypeWorkSheet) {
        this.validateTypeWorkSheet = validateTypeWorkSheet;
    }

    public List<SelectItem> getModelItem() {
        if (modelItem == null) {
            modelItem = new ArrayList<SelectItem>();
        }
        return modelItem;
    }

    public void setModelItem(List<SelectItem> modelItem) {
        this.modelItem = modelItem;
    }

    public String getControlLot() {
        return controlLot;
    }

    public void setControlLot(String controlLot) {
        this.controlLot = controlLot;
    }

    public String getShowModelNotUse() {
        return showModelNotUse;
    }

    public void setShowModelNotUse(String showModelNotUse) {
        this.showModelNotUse = showModelNotUse;
    }

    public String getShowModelUse() {
        return showModelUse;
    }

    public void setShowModelUse(String showModelUse) {
        this.showModelUse = showModelUse;
    }

    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    public String getModelStr() {
        return modelStr;
    }

    public void setModelStr(String modelStr) {
        this.modelStr = modelStr;
    }

    public String getItemStr() {
        return itemStr;
    }

    public void setItemStr(String itemStr) {
        this.itemStr = itemStr;
    }

    public String getValidateModel() {
        return validateModel;
    }

    public void setValidateModel(String validateModel) {
        this.validateModel = validateModel;
    }

    public String getUrlAll() {
        return urlAll;
    }

    public void setUrlAll(String urlAll) {
        this.urlAll = urlAll;
    }

    public String getUrlCheck() {
        return urlCheck;
    }

    public void setUrlCheck(String urlCheck) {
        this.urlCheck = urlCheck;
    } 
}
