/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.master.bean;

import com.tct.data.LotControl;

/**
 *
 * @author TheBoyCs
 */
public class BarcodeSelected {
    private int index;
    private String barcode;
    private String model;
    private int qty;
    private LotControl lotControl;

    public BarcodeSelected(int index,String barcode, String model, int qty, LotControl lotControl) {
        this.index = index;
        this.barcode = barcode;
        this.model = model;
        this.qty = qty;
        this.lotControl = lotControl;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public LotControl getLotControl() {
        return lotControl;
    }

    public void setLotControl(LotControl lotControl) {
        this.lotControl = lotControl;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
    
}
