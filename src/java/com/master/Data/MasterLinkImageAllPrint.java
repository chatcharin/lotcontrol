/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.master.Data;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Oct 15, 2012, Time : 1:57:23 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class MasterLinkImageAllPrint {
    private String index;
    private String barcode;
    private boolean print;

    public MasterLinkImageAllPrint(String index, String barcode, boolean print) {
        this.index = index;
        this.barcode = barcode;
        this.print = print;
    } 
    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public boolean isPrint() {
        return print;
    }

    public void setPrint(boolean print) {
        this.print = print;
    } 
}
