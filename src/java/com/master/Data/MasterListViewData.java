/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.master.Data;

import java.util.Date;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Oct 13, 2012, Time : 1:37:02 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class MasterListViewData {
    private String index;
    private String barcode;
    private String model;
    private String series;
    private String dateCreate;
    private String process;
    private String status;
    private String orderNumber;

    public MasterListViewData(String index, String barcode, String model, String series, String dateCreate, String process, String status,String orderNumber) {
        this.index = index;
        this.barcode = barcode;
        this.model = model;
        this.series = series;
        this.dateCreate = dateCreate;
        this.process = process;
        this.status = status;
        this.orderNumber = orderNumber;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate = dateCreate;
    } 
    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    } 
}
