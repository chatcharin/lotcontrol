/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.master.Data;

import java.util.Date;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Oct 12, 2012, Time : 2:09:07 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class MasterDefaultData {
    private String index;
    private String orderNo;
    private String productType;
    private String productCodeName;
    private String productCode;
    private String model;
    private String seriee;
    private String productQty;
    private String lotControlQty;
    private String invBronze;
    private String invBobbin;
    private String plant;
    private Date plantingDate;
    private Date injectDate;
    private Date assyDate;
    private String orderType;
    private String remarks;

    public MasterDefaultData(String index, String orderNo, String productType, String productCodeName, String productCode, String model, String seriee, String productQty, String lotControlQty, String invBronze, String invBobbin, String plant, Date plantingDate, Date injectDate, Date assyDate, String orderType, String remarks) {
        this.index = index;
        this.orderNo = orderNo;
        this.productType = productType;
        this.productCodeName = productCodeName;
        this.productCode = productCode;
        this.model = model;
        this.seriee = seriee;
        this.productQty = productQty;
        this.lotControlQty = lotControlQty;
        this.invBronze = invBronze;
        this.invBobbin = invBobbin;
        this.plant = plant;
        this.plantingDate = plantingDate;
        this.injectDate = injectDate;
        this.assyDate = assyDate;
        this.orderType = orderType;
        this.remarks = remarks;
    }

    public Date getAssyDate() {
        return assyDate;
    }

    public void setAssyDate(Date assyDate) {
        this.assyDate = assyDate;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public Date getInjectDate() {
        return injectDate;
    }

    public void setInjectDate(Date injectDate) {
        this.injectDate = injectDate;
    }

    public String getInvBobbin() {
        return invBobbin;
    }

    public void setInvBobbin(String invBobbin) {
        this.invBobbin = invBobbin;
    }

    public String getInvBronze() {
        return invBronze;
    }

    public void setInvBronze(String invBronze) {
        this.invBronze = invBronze;
    }

    public String getLotControlQty() {
        return lotControlQty;
    }

    public void setLotControlQty(String lotControlQty) {
        this.lotControlQty = lotControlQty;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getPlant() {
        return plant;
    }

    public void setPlant(String plant) {
        this.plant = plant;
    }

    public Date getPlantingDate() {
        return plantingDate;
    }

    public void setPlantingDate(Date plantingDate) {
        this.plantingDate = plantingDate;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductCodeName() {
        return productCodeName;
    }

    public void setProductCodeName(String productCodeName) {
        this.productCodeName = productCodeName;
    }

    public String getProductQty() {
        return productQty;
    }

    public void setProductQty(String productQty) {
        this.productQty = productQty;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSeriee() {
        return seriee;
    }

    public void setSeriee(String seriee) {
        this.seriee = seriee;
    } 
}
    
