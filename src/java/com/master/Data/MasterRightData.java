/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.master.Data;

import com.tct.data.DetailFieldRight;
import com.tct.data.DetailSpecial;
import java.util.List;
import javax.faces.model.SelectItem;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 26, 2012, Time : 3:39:11 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class MasterRightData {
    private DetailSpecial indexMasterDataDetail;
    private DetailFieldRight indexAttribute;
    private String name;
    private String data;
    private boolean typeInput;
    private boolean typeSelect;
    private List<SelectItem> itemSlectBox;

    public MasterRightData(DetailSpecial indexMasterDataDetail, DetailFieldRight indexAttribute, String name, String data, boolean typeInput, boolean typeSelect, List<SelectItem> itemSlectBox) {
        this.indexMasterDataDetail = indexMasterDataDetail;
        this.indexAttribute = indexAttribute;
        this.name = name;
        this.data = data;
        this.typeInput = typeInput;
        this.typeSelect = typeSelect;
        this.itemSlectBox = itemSlectBox;
    }

    public List<SelectItem> getItemSlectBox() {
        return itemSlectBox;
    }

    public void setItemSlectBox(List<SelectItem> itemSlectBox) {
        this.itemSlectBox = itemSlectBox;
    }

    public boolean isTypeInput() {
        return typeInput;
    }

    public void setTypeInput(boolean typeInput) {
        this.typeInput = typeInput;
    }

    public boolean isTypeSelect() {
        return typeSelect;
    }

    public void setTypeSelect(boolean typeSelect) {
        this.typeSelect = typeSelect;
    } 
    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public DetailFieldRight getIndexAttribute() {
        return indexAttribute;
    }

    public void setIndexAttribute(DetailFieldRight indexAttribute) {
        this.indexAttribute = indexAttribute;
    }

    public DetailSpecial getIndexMasterDataDetail() {
        return indexMasterDataDetail;
    }

    public void setIndexMasterDataDetail(DetailSpecial indexMasterDataDetail) {
        this.indexMasterDataDetail = indexMasterDataDetail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    } 
}
