/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.master.Data;

/**
 *
 * Machine User
 *
 * @author pang Development by Chusak laojang Date : Oct 17, 2012, Time :
 * 9:40:04 AM Copy Right 4 Xtreme Co.,Ltd.
 */
public class MasterSearch {

    private String index;
    private String barcode;
    private String process;
    private String workSheetType;
    private String Model;
    private String Series;
    private String orderNo;

    public MasterSearch(String index, String barcode, String process, String workSheetType, String Model, String Series, String orderNo) {
        this.index = index;
        this.barcode = barcode;
        this.process = process;
        this.workSheetType = workSheetType;
        this.Model = Model;
        this.Series = Series;
        this.orderNo = orderNo;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String Model) {
        this.Model = Model;
    }

    public String getSeries() {
        return Series;
    }

    public void setSeries(String Series) {
        this.Series = Series;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getWorkSheetType() {
        return workSheetType;
    }

    public void setWorkSheetType(String workSheetType) {
        this.workSheetType = workSheetType;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
