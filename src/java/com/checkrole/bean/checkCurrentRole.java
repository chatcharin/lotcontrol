/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.checkrole.bean;

import com.tct.data.Role;
import com.tct.data.RoleUser;
import com.tct.data.Users;
import com.tct.data.jpa.RoleJpaController;
import com.tct.data.jpa.RoleUserJpaController;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.persistence.Persistence;

/**
 *
 * @author The_Boy_Cs
 */
public class checkCurrentRole {

    private Object jparoleusercontroller;
    private Object jpaRoleController;

    private Users getSessionUser() {
        FacesContext context = FacesContext.getCurrentInstance();
        Users currentuser = (Users) context.getExternalContext().getSessionMap().get("currentUser");
        return currentuser;
    }

    private RoleUserJpaController getRoleUserJpaController() {
        if (jparoleusercontroller == null) {
            jparoleusercontroller = new RoleUserJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return (RoleUserJpaController) jparoleusercontroller;
    }

    private RoleJpaController getRoleJpaController() {
        if (jpaRoleController == null) {
            jpaRoleController = new RoleJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return (RoleJpaController) jpaRoleController;
    }

    public boolean getAddUserRole() {
        Role role = checkUserrole();
        if (role != null && role.getAddUserRole().equals(1)) {
            return true;
        } else {
            return false;
        }
    }

    // public boolean 
    public Role checkUserrole() {
        Users users = getSessionUser();
        if(users==null){
            return null;
        }
//        List roleusers = getRoleUserJpaController().findRoleUserByUser(users);
//        Iterator itr = roleusers.iterator();
        RoleUser roleuser = null;
        Role role = new Role();
        Role tmprole = null;
        //List roles = new ArrayList();
//        while (itr.hasNext()) {
//            roleuser = (RoleUser) itr.next();
//            tmprole = getRoleJpaController().findRole(roleuser.getRoleId().getIdRole());
//            //roles.add(tmprole);
//            if (tmprole.getAddUserRole() != 0) {
//                role.setAddUserRole(tmprole.getAddUserRole());
//            }
//            if (tmprole.getApproveAssembly() != 0) {
//                role.setApproveAssembly(tmprole.getApproveAssembly());
//            }
//            if (tmprole.getApproveAssy() != 0) {
//                role.setApproveAssy(tmprole.getApproveAssy());
//            }
//            if (tmprole.getApproveAuto() != 0) {
//                role.setApproveAuto(tmprole.getApproveAuto());
//            }
//            if (tmprole.getApprovePcb01() != 0) {
//                role.setApprovePcb01(tmprole.getApprovePcb01());
//            }
//            if (tmprole.getApprovePcb02() != 0) {
//                role.setApprovePcb02(tmprole.getApprovePcb02());
//            }
//            if (tmprole.getApprovePcb03() != 0) {
//                role.setApproveRefu03(tmprole.getApprovePcb03());
//            }
//            if (tmprole.getCreateAssembly() != 0) {
//                role.setCreateAssembly(tmprole.getCreateAssembly());
//            }
//            if (tmprole.getCreateAssy() != 0) {
//                role.setCreateAssy(tmprole.getCreateAssy());
//            }
//            if (tmprole.getCreateAuto() != 0) {
//                role.setCreateAuto(tmprole.getCreateAuto());
//            }
//            if (tmprole.getCreateFl() != 0) {
//                role.setCreateFl(tmprole.getCreateFl());
//            }
//            if (tmprole.getCreateMc() != 0) {
//                role.setCreateMc(tmprole.getCreateMc());
//            }
//            if (tmprole.getCreatePcb01() != 0) {
//                role.setCreatePcb01(tmprole.getCreatePcb01());
//            }
//            if (tmprole.getCreatePcb02() != 0) {
//                role.setCreatePcb02(tmprole.getCreatePcb02());
//            }
//            if (tmprole.getCreatePcb03() != 0) {
//                role.setCreatePcb03(tmprole.getCreatePcb03());
//            }
//            if (tmprole.getCreateRefu01() != 0) {
//                role.setCreateRefu01(tmprole.getCreateRefu01());
//            }
//            if (tmprole.getCreateRefu02() != 0) {
//                role.setCreateRefu02(tmprole.getCreateRefu02());
//            }
//            if (tmprole.getCreateRefu03() != 0) {
//                role.setCreateRefu03(tmprole.getCreateRefu03());
//            }
//            if (tmprole.getCreateUser() != 0) {
//                role.setCreateUser(tmprole.getCreateUser());
//            }
//            if (tmprole.getDataProcess() != 0) {
//                role.setDataProcess(tmprole.getDataProcess());
//            }
//            if (tmprole.getDeleteAssembly() != 0) {
//                role.setDeleteAssembly(tmprole.getDeleteAssembly());
//            }
//            if (tmprole.getDeleteAssy() != 0) {
//                role.setDeleteAssy(tmprole.getDeleteAssy());
//            }
//            if (tmprole.getDeleteAuto() != 0) {
//                role.setDeleteAssy(tmprole.getDeleteAssy());
//            }
//            if (tmprole.getDeleteFl() != 0) {
//                role.setDeleteFl(tmprole.getDeleteFl());
//            }
//            if (tmprole.getDeletePcb01() != 0) {
//                role.setDeletePcb01(tmprole.getDeletePcb01());
//            }
//            if (tmprole.getDeletePcb02() != 0) {
//                role.setDeletePcb02(tmprole.getDeletePcb02());
//            }
//            if (tmprole.getDeletePcb03() != 0) {
//                role.setDeleteRefu03(tmprole.getDeleteRefu03());
//            }
//            if (tmprole.getDetailAssembly() != 0) {
//                role.setDetailAssembly(tmprole.getDetailAssembly());
//            }
//            if (tmprole.getDetailAssy() != 0) {
//                role.setDetailAssy(tmprole.getDetailAssy());
//            }
//            if (tmprole.getDetailAuto() != 0) {
//                role.setDetailAuto(tmprole.getDetailAuto());
//            }
//            if (tmprole.getDetailFrontLine() != 0) {
//                role.setDetailFrontLine(tmprole.getDetailFrontLine());
//            }
//            if (tmprole.getDetailPcb01() != 0) {
//                role.setDetailPcb01(tmprole.getDetailPcb01());
//            }
//            if (tmprole.getDetailPcb02() != 0) {
//                role.setDetailPcb02(tmprole.getDetailPcb02());
//            }
//            if (tmprole.getDetailPcb03() != 0) {
//                role.setDetailPcb03(tmprole.getDetailPcb03());
//            }
//            if (tmprole.getDetailRefu01() != 0) {
//                role.setDetailRefu01(tmprole.getDetailRefu01());
//            }
//            if (tmprole.getDetailRefu02() != 0) {
//                role.setDetailRefu02(tmprole.getDetailRefu02());
//            }
//            if (tmprole.getDetailRefu03() != 0) {
//                role.setDetailRefu03(tmprole.getDetailRefu03());
//            }
//            if (tmprole.getEditAssembly() != 0) {
//                role.setEditAssembly(tmprole.getEditAssembly());
//            }
//            if (tmprole.getEditAssy() != 0) {
//                role.setEditAssy(tmprole.getEditAssy());
//            }
//            if (tmprole.getEditAuto() != 0) {
//                role.setEditAuto(tmprole.getEditAuto());
//            }
//            if (tmprole.getEditFl() != 0) {
//                role.setEditFl(tmprole.getEditFl());
//            }
//            if (tmprole.getEditModel() != 0) {
//                role.setEditModel(tmprole.getEditModel());
//            }
//            if (tmprole.getEditNg() != 0) {
//                role.setEditNg(tmprole.getEditNg());
//            }
//            if (tmprole.getEditPcb01() != 0) {
//                role.setEditPcb01(tmprole.getEditPcb01());
//            }
//            if (tmprole.getEditPcb02() != 0) {
//                role.setEditPcb02(tmprole.getEditPcb02());
//            }
//            if (tmprole.getEditPcb03() != 0) {
//                role.setEditPcb03(tmprole.getEditPcb03());
//            }
//            if (tmprole.getEditRefu01() != 0) {
//                role.setEditRefu01(tmprole.getEditRefu01());
//            }
//            if (tmprole.getEditRefu02() != 0) {
//                role.setEditRefu02(tmprole.getEditRefu02());
//            }
//            if (tmprole.getEditRefu03() != 0) {
//                role.setEditRefu03(tmprole.getEditRefu03());
//            }
//            if (tmprole.getListAssembly() != 0) {
//                role.setListAssembly(tmprole.getListAssembly());
//            }
//            if (tmprole.getListAssy() != 0) {
//                role.setListAssy(tmprole.getListAssy());
//            }
//            if (tmprole.getListAuto() != 0) {
//                role.setListAuto(tmprole.getListAuto());
//            }
//            if (tmprole.getListFl() != 0) {
//                role.setListFl(tmprole.getListFl());
//            }
//            if (tmprole.getListPcb01() != 0) {
//                role.setListPcb01(tmprole.getListPcb01());
//            }
//            if (tmprole.getListPcb02() != 0) {
//                role.setListPcb02(tmprole.getListPcb02());
//            }
//            if (tmprole.getListPcb03() != 0) {
//                role.setListPcb03(tmprole.getListPcb03());
//            }
//            if (tmprole.getListRefu01() != 0) {
//                role.setListRefu01(tmprole.getListRefu01());
//            }
//            if (tmprole.getListRefu02() != 0) {
//                role.setListRefu02(tmprole.getListRefu02());
//            }
//            if (tmprole.getListRefu03() != 0) {
//                role.setListRefu03(tmprole.getListRefu03());
//            }
//            if (tmprole.getMcPref() != 0) {
//                role.setMcPref(tmprole.getMcPref());
//            }
//            if (tmprole.getNewProcess() != 0) {
//                role.setNewProcess(tmprole.getNewProcess());
//            }
//            if (tmprole.getSeeAppconf() != 0) {
//                role.setSeeAppconf(tmprole.getSeeAppconf());
//            }
//            if (tmprole.getSeeBl() != 0) {
//                role.setSeeBl(tmprole.getSeeBl());
//            }
//            if (tmprole.getSeeFl() != 0) {
//                role.setSeeFl(tmprole.getSeeFl());
//            }
//            if (tmprole.getSeeMc() != 0) {
//                role.setSeeMc(tmprole.getSeeMc());
//            }
//            if (tmprole.getSeePcb() != 0) {
//                role.setSeePcb(tmprole.getSeePcb());
//            }
//            if (tmprole.getSeeRefu() != 0) {
//                role.setSeeRefu(tmprole.getSeeRefu());
//            }
//            if (tmprole.getSeeReport() != 0) {
//                role.setSeeReport(tmprole.getSeeReport());
//            }
//            if (tmprole.getSeeSetting() != 0) {
//                role.setSeeSetting(tmprole.getSeeSetting());
//            }
//            if (tmprole.getSelectBar() != 0) {
//                role.setSelectBar(tmprole.getSelectBar());
//            }
//            if (tmprole.getUserConf() != 0) {
//                role.setUserConf(tmprole.getUserConf());
//            }
//            if (role.getRoleName() == null) {
//                role.setRoleName(tmprole.getRoleName());
//            }
//            if (role.getIdRole() == null) {
//                role.setIdRole(tmprole.getIdRole());
//            }
//        }
        return role;
    }
}
