/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.checkrole.bean;
 
import com.appCinfigpage.bean.userLogin;
import com.tct.data.RoleUser;
import com.tct.data.Users;
import com.tct.data.jpa.RoleUserJpaController;
import java.util.Collection;
import javax.persistence.Persistence;
/**
 *
 * @author pang
 */
public class CheckRoleUser {  
  RoleUserJpaController  roleJpaController;
       private RoleUserJpaController getRoleUserJpaController() {
      
        if (roleJpaController == null) {
            roleJpaController = new RoleUserJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return roleJpaController;
    }

    // Find Role by User id 
    public Collection<RoleUser> getRoleUser(Users u){
        // User jpa find Role
      return  getRoleUserJpaController().findByUserId(u);
    }
    
    public  boolean check(String type)
    {  
//        FacesContext context = FacesContext.getCurrentInstance();
//        Users currentuser = (Users) context.getExternalContext().getSessionMap().get("currentUser");
        Users currentuser = userLogin.getSessionUser();
        Collection<RoleUser> roleUsers = getRoleUser(currentuser);
        for(RoleUser roleUser:roleUsers){
           if(checkRole(roleUser,type)==1)
               return true;
        }
    
        return false;
    }

    public int checkRole(RoleUser roleUser,String type){
       //===== fl
        if(type.equals("see_fl"))
         return roleUser.getRoleId().getSeeFl().intValue();
        if(type.equals("create_fl"))
         return roleUser.getRoleId().getCreateFl().intValue(); 
        if(type.equals("list_fl"))
         return roleUser.getRoleId().getListFl().intValue();
        if(type.equals("delete_fl"))
         return roleUser.getRoleId().getDeleteFl().intValue();
        if(type.equals("edit_fl"))
         return roleUser.getRoleId().getEditFl().intValue();
       
        //Detial font line  
        if(type.equals("detail_front_line"))
         return roleUser.getRoleId().getDetailFrontLine().intValue();
        
        //sell bl
        if(type.equals("see_bl"))
         return roleUser.getRoleId().getSeeBl().intValue();
        
        //assy
        if(type.equals("create_assy"))
         return roleUser.getRoleId().getCreateAssy().intValue();
        if(type.equals("list_assy"))
         return roleUser.getRoleId().getListAssy().intValue();
        if(type.equals("delete_assy"))
         return roleUser.getRoleId().getDeleteAssy().intValue();
        if(type.equals("edit_assy"))
         return roleUser.getRoleId().getEditAssy().intValue();
        if(type.equals("approve_assy"))
         return roleUser.getRoleId().getApproveAssy().intValue();
        if(type.equals("detail_assy"))
         return roleUser.getRoleId().getDeleteAssy().intValue();
        
        //auto
        if(type.equals("create_auto"))
         return roleUser.getRoleId().getCreateAuto().intValue();
        if(type.equals("list_auto"))
         return roleUser.getRoleId().getListAuto().intValue();
        if(type.equals("delete_auto"))
         return roleUser.getRoleId().getDeleteAuto().intValue();
        if(type.equals("edit_auto"))
         return roleUser.getRoleId().getEditAuto().intValue();
        if(type.equals("approve_auto"))
         return roleUser.getRoleId().getApproveAuto().intValue();
        if(type.equals("detail_auto"))
         return roleUser.getRoleId().getDetailAuto().intValue();
        
        //assembly
        if(type.equals("create_assembly"))
         return roleUser.getRoleId().getCreateAssembly().intValue();
        if(type.equals("list_assembly"))
         return roleUser.getRoleId().getListAssembly().intValue();
        if(type.equals("delete_assembly"))
         return roleUser.getRoleId().getDeleteAssembly().intValue();
        if(type.equals("edit_assembly"))
         return roleUser.getRoleId().getEditAssembly().intValue();
        if(type.equals("approve_assembly"))
         return roleUser.getRoleId().getApproveAssembly().intValue();
        if(type.equals("detail_assembly"))
         return roleUser.getRoleId().getDetailAssembly().intValue();
        
        //see_pcb
        if(type.equals("see_pcb"))
         return roleUser.getRoleId().getSeePcb().intValue();
       
        //pcb01
        if(type.equals("create_pcb01"))
         return roleUser.getRoleId().getCreatePcb01().intValue();
        if(type.equals("list_pcb01"))
         return roleUser.getRoleId().getListPcb01().intValue();
        if(type.equals("delete_pcb01"))
         return roleUser.getRoleId().getDeletePcb01().intValue();
        if(type.equals("edit_pcb01"))
         return roleUser.getRoleId().getEditPcb01().intValue();
        if(type.equals("approve_pcb01"))
         return roleUser.getRoleId().getApprovePcb01().intValue();
        if(type.equals("detail_pcb01"))
         return roleUser.getRoleId().getDeletePcb01().intValue();
        
        //pcb02
        if(type.equals("create_pcb02"))
         return roleUser.getRoleId().getCreatePcb02().intValue();
        if(type.equals("list_pcb02"))
         return roleUser.getRoleId().getListPcb02().intValue();
        if(type.equals("delete_pcb02"))
         return roleUser.getRoleId().getDeletePcb02().intValue();
        if(type.equals("edit_pcb02"))
         return roleUser.getRoleId().getEditPcb02().intValue();
        if(type.equals("approve_pcb02"))
         return roleUser.getRoleId().getApprovePcb02().intValue();
        if(type.equals("detail_pcb02"))
         return roleUser.getRoleId().getDetailPcb02().intValue();
        
        //pcb03
        if(type.equals("create_pcb03"))
         return roleUser.getRoleId().getCreatePcb03().intValue();
        if(type.equals("list_pcb03"))
         return roleUser.getRoleId().getListPcb03().intValue();
        if(type.equals("delete_pcb03"))
         return roleUser.getRoleId().getDeletePcb03().intValue();
        if(type.equals("edit_pcb03"))
         return roleUser.getRoleId().getEditPcb03().intValue();
        if(type.equals("approve_pcb03"))
         return roleUser.getRoleId().getApprovePcb03().intValue();
        if(type.equals("detail_pcb03"))
         return roleUser.getRoleId().getDetailPcb03().intValue();
        
        //see_refu
         if(type.equals("see_refu"))
         return roleUser.getRoleId().getSeeRefu().intValue();
        
         // refu01
        if(type.equals("create_refu01"))
         return roleUser.getRoleId().getCreateRefu01().intValue();
        if(type.equals("list_refu01"))
         return roleUser.getRoleId().getListRefu01().intValue();
        if(type.equals("delete_refu01"))
         return roleUser.getRoleId().getDeleteRefu01().intValue();
        if(type.equals("edit_refu01"))
         return roleUser.getRoleId().getEditRefu01().intValue();
        if(type.equals("approve_refu01"))
         return roleUser.getRoleId().getApproveRefu01().intValue();
        if(type.equals("detail_refu01"))
         return roleUser.getRoleId().getDetailRefu01().intValue();
        
        //refu02
        if(type.equals("create_refu02"))
         return roleUser.getRoleId().getCreateRefu02().intValue();
        if(type.equals("list_refu02"))
         return roleUser.getRoleId().getListRefu02().intValue();
        if(type.equals("delete_refu02"))
         return roleUser.getRoleId().getDeleteRefu02().intValue();
        if(type.equals("edit_refu02"))
         return roleUser.getRoleId().getEditRefu02().intValue();
        if(type.equals("approve_refu02"))
         return roleUser.getRoleId().getApproveRefu02().intValue();
        if(type.equals("detail_refu02"))
         return roleUser.getRoleId().getDetailRefu02().intValue();
        
        //refu03
        if(type.equals("create_refu03"))
         return roleUser.getRoleId().getCreateRefu03().intValue();
        if(type.equals("list_refu03"))
         return roleUser.getRoleId().getListRefu03().intValue();
        if(type.equals("delete_refu03"))
         return roleUser.getRoleId().getDeleteRefu03().intValue();
        if(type.equals("edit_refu03"))
         return roleUser.getRoleId().getEditRefu03().intValue();
        if(type.equals("approve_refu03"))
         return roleUser.getRoleId().getApproveRefu03().intValue();
        if(type.equals("detail_refu03"))
         return roleUser.getRoleId().getDetailRefu03().intValue();
        
        //see_report
        if(type.equals("see_report"))
         return roleUser.getRoleId().getSeeReport().intValue();
        
        //see_setting
        if(type.equals("see_setting"))
         return roleUser.getRoleId().getSeeSetting().intValue();
        
        //edit_model
        if(type.equals("edit_model"))
         return roleUser.getRoleId().getEditModel().intValue();
        
        //new_process
         if(type.equals("new_process"))
         return roleUser.getRoleId().getNewProcess().intValue();
        
         //data_process
         if(type.equals("data_process"))
          return roleUser.getRoleId().getDataProcess().intValue();
        
         //edit_ng
          if(type.equals("edit_ng"))
           return roleUser.getRoleId().getEditNg().intValue();
          
         //select_bar
         if(type.equals("select_bar"))
           return roleUser.getRoleId().getSelectBar().intValue(); 
         
         //see_mc
         if(type.equals("see_mc"))
           return roleUser.getRoleId().getSeeMc().intValue(); 
         
         //create_mc
         if(type.equals("create_mc"))
           return roleUser.getRoleId().getCreateMc().intValue(); 
         
         //mc_pref
         if(type.equals("mc_pref"))
           return roleUser.getRoleId().getMcPref().intValue(); 
         
         //see_appconf
         if(type.equals("see_appconf"))
           return roleUser.getRoleId().getSeeAppconf().intValue(); 
         
         //create_user
         if(type.equals("create_user"))
           return roleUser.getRoleId().getCreateUser().intValue(); 
         
         //user_conf
         if(type.equals("user_conf"))
           return roleUser.getRoleId().getUserConf().intValue(); 
         
         //add_user_role
         if(type.equals("add_user_role"))
           return roleUser.getRoleId().getAddUserRole().intValue(); 
        
         
         
//       if(type.equals("adduser"))
//         return roleUser.getRoleId().getAddUserRole().intValue();
       return 0;
    }
}
