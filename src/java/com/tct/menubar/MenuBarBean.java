/*
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * "The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations under
 * the License.
 *
 * The Original Code is ICEfaces 1.5 open source software code, released
 * November 5, 2006. The Initial Developer of the Original Code is ICEsoft
 * Technologies Canada, Corp. Portions created by ICEsoft are Copyright (C)
 * 2004-2006 ICEsoft Technologies Canada, Corp. All Rights Reserved.
 *
 * Contributor(s): _____________________.
 *
 * Alternatively, the contents of this file may be used under the terms of
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"
 * License), in which case the provisions of the LGPL License are
 * applicable instead of those above. If you wish to allow use of your
 * version of this file only under the terms of the LGPL License and not to
 * allow others to use your version of this file under the MPL, indicate
 * your decision by deleting the provisions above and replace them with
 * the notice and other provisions required by the LGPL License. If you do
 * not delete the provisions above, a recipient may use your version of
 * this file under either the MPL or the LGPL License."
 *
 */

package com.tct.menubar;

import com.master.bean.Master;
import com.report.quality.bean.Ng;
import com.report.quality.bean.NgRepair;
import com.report.quality.bean.Yield;
import java.sql.SQLException;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


/**
 * <p>The MenuBarBean class determines which menu item fired the ActionEvent and
 * stores the modified id information in a String. MenuBarBean also controls the
 * orientation of the Menu Bar.</p>
 */
public class MenuBarBean {
	
    // records the param value for the menu item which fired the event
    private String param="report/slideshowkpi/default.xhtml"; 
     

    // orientation of the menubar ("horizontal" or "vertical")
    private String orientation = "horizontal";

    /**
     * Get the param value for the menu item which fired the event.
     *
     * @return the param value
     */
    private boolean visible = false;
    
    public boolean isVisible() { return visible; }
    
    //----
    private String dataTest;
    
    
    
    public void setVisible(boolean visible) { this.visible = visible; }
    
    public void closePopup() {
        visible = false; 
    }
    //private String typePopup; 
    
    public void openPopup(/*String typePopup*/) {
    	//this.typePopup = typePopup; 
    	//System.out.println(typePopup); 
    	visible = true;
    }  
    
  
   
    public String getParam() {
        return param;
    }

    /**
     * Set the param value.
     */
    public void setParam(String param) {
        
        //System.out.println(param); 
        this.param = param;
    }

    /**
     * Identify the ID of the element that fired the event and return it in a
     * form suitable for display.
     *
     * @param e the event that fired the listener
     */
    public void listener(ActionEvent e) throws SQLException {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Map params = facesContext.getExternalContext().getRequestParameterMap();
        String myParam = (String) params.get("myParam");
         System.out.println(myParam );  
        if (myParam != null && myParam.length() > 0) { 
        	//System.out.println(myParam); 
                if(myParam.equals("Home"))
        	{
//        		myParam = "report/index.xhtml";
                        myParam = "report/slideshowkpi/default.xhtml";
        	}
//        	else if(myParam.equals("frontLine"))
//        	{
//        		myParam = "pages/moduleFontLine/createWorksheet.xhtml";
//        	}
        	else if(myParam.equals("frontline"))
        	{
        		myParam = "pages/moduleFontLine/listview.xhtml";
        	}
        	else if(myParam.equals("createAssyWorksheet"))
        	{
        		myParam = "pages/moduleBackLine/createAssyWorksheet.xhtml";
        	}
        	else if(myParam.equals("assyWoeksheetList"))
        	{
        		myParam = "pages/moduleBackLine/assy/listview.xhtml";
        	}
        	else if(myParam.equals("createAutoWorksheet"))
        	{
        		myParam = "pages/moduleBackLine/auto/createview.xhtml";
        	}
        	else if(myParam.equals("autoworksheetList"))
        	{
        		myParam = "pages/moduleBackLine/auto/listview.xhtml";
        	}
        	else if(myParam.equals("createLineworksheet"))
        	{
        		myParam = "pages/moduleBackLine/assembly/createview.xhtml";
        	}
        	else if(myParam.equals("lineWorksheetList"))
        	{
        		myParam = "pages/moduleBackLine/assembly/listview.xhtml";
        	}
        	else if(myParam.equals("rChart"))
        	{
        		myParam = "pages/report/rChart.xhtml";
        	}
        	else if(myParam.equals("reportList"))
        	{
        		myParam = "pages/report/reportList.xhtml";
        	}
        	else if(myParam.equals("editModelStep"))
        	{
        		myParam = "pages/setting/editModelStep.xhtml";
        	}
                else if(myParam.equals("model")){
                    myParam = "pages/setting/model/listview.xhtml";
                }
        	else if(myParam.equals("processed"))
        	{
        		myParam = "pages/setting/processed/listview.xhtml";
        	}
        	else if(myParam.equals("dataProcess"))
        	{
        		myParam = "pages/setting/dataProcess.xhtml";
        	}
        	else if(myParam.equals("editng"))
        	{
//        		myParam = "pages/setting/editNG.xhtml";
                        //@pang
                        myParam = "pages/setting/editng/listview.xhtml";
        	}
        	else if(myParam.equals("selectBarcodeType"))
        	{	
        		myParam = "pages/setting/selectBarcodeType.xhtml";
        	}
        	else if(myParam.equals("mc"))
        	{ 
        		myParam = "pages/machine/listview.xhtml";
        	}
        	else if(myParam.equals("mcPerformance"))
        	{
        		myParam = "pages/machine/mcPerformance.xhtml";
        	}
        	else if(myParam.equals("users"))
        	{
        		//System.out.println("Sucess");
        		myParam = "pages/appConfig/users/listview.xhtml";
        		
        	}
        	else if(myParam.equals("userConfig"))
        	{
        		//System.out.println("Sucess");
        		myParam = "pages/appConfig/userConfig.xhtml";
        		
        	}
        	else if(myParam.equals("printworksheet")){
        		myParam = "pages/moduleFontLine/printworksheet.xhtml";
        	}else if(myParam.equals("packing")){
        		myParam = "pages/moduleBackLine/packing/listview.xhtml";
//        	}else if(myParam.equals("createpacking")){
//        		myParam = "pages/moduleBackLine/createPacking.xhtml";
        	}else if(myParam.equals("config_process")){
        		myParam = "pages/setting/config_process.xhtml";
        	}else if(myParam.equals("refu")){
        		myParam = "pages/moduleBackLine/Refu.xhtml";
        	}else if(myParam.equals("refulist")){
        		myParam = "pages/moduleBackLine/RefuLists.xhtml";
        	}else if(myParam.equals("pcb")){
        		myParam = "pages/moduleBackLine/pcb.xhtml";
        	}else if(myParam.equals("pcblist")){
        		myParam = "pages/moduleBackLine/pcbList.xhtml";
                }else if(myParam.equals("frontlinelistview")){
                    myParam = "pages/moduleFontLine/worksheet.xhtml";
        	}else if(myParam.equals("newcreate")){
                    myParam = "pages/moduleBackLine/newcreate/listview.xhtml"; 
                }
                //@pang
                else if(myParam.equals("attribute"))
                {
                   myParam  = "pages/setting/attribute/listview.xhtml";
                }
                else if(myParam.equals("MasterDetail"))
                {
                   myParam  = "pages/setting/masterdetail/listview.xhtml";
                } 
                else if(myParam.equals("MasterWorksheet"))
                {
                   myParam  = "pages/masterworksheet/listview.xhtml";  
                }
                else if(myParam.equals("Line"))
                {
                   myParam  = "pages/setting/line/listview.xhtml";  
                }
                else if(myParam.equals("Radar"))
                {
                   myParam = "pages/setting/radar/detailview.xhtml";
                }
                else if(myParam.equals("KPIPicture"))
                {
                  myParam = "pages/setting/kpipicture/listview.xhtml";
                }
                else if(myParam.equals("headerinput")){
                    myParam = "pages/setting/inputbyheader/listview.xhtml";
                }
                else if(myParam.equals("settingtineonslide")){
                    myParam = "pages/setting/inputbyheader/settimetorefresh.xhtml";
                }
                else if(myParam.equals("nghistory")){
                    myParam = "pages/report/gethistory/nghistory.xhtml";
                }else if(myParam.equals("stdtimesetting")){
                    myParam = "pages/setting/stdtime/stdtimesetting.xhtml";
                }else if(myParam.equals("historytacking")){
                    myParam = "pages/report/historytacking/tacking.xhtml";
                }else if(myParam.equals("oeereport")){
                    myParam = "pages/report/oeereport/oeereport.xhtml";
                }
                else if(myParam.equals("reportsetting"))
                {
                    myParam = "pages/setting/reportsetting/listview.xhtml";
                }
                else if(myParam.equals("Yield"))
                {
                    myParam = "pages/report/quality/yield/yield.xhtml";
                    
//                    getYield().bufferDefaultDataYied();
//                    getYield().bufferDefaultDataYied();  //****** function load default yield chart.
                }
                else if(myParam.equals("NG"))
                {
                    myParam = "pages/report/quality/ng/ng.xhtml";
                    //**@pang  
                    getNg().loadDefaultNgDataChart(); // ***** fuction load default ng chart..
                }
                else if(myParam.equals("NG_repair"))
                {
                    myParam = "pages/report/quality/ngRepair/ngRepair.xhtml";
//                    getNgRepair().loadDefaultBufferData(); //***** function load default ng repair.. 
                }
                else if(myParam.equals("ngdefect"))
                {
                    myParam = "report/totaldefect/defecttotalng_1.xhtml";
                }
                else if(myParam.equals("stdTimeSetting"))
                {
                    myParam ="pages/setting/stdtimesetting/listview.xhtml";
                }
                else if(myParam.equals("stdTime"))
                {
                    myParam = "pages/report/stdsetting/listview.xhtml";
                }
                else if(myParam.equals("oeechart"))
                {
                    myParam ="pages/report/indicatoroee/listview.xhtml";
                }
                else if(myParam.equals("oeechartmc"))
                {
                    myParam ="pages/report/indicatoroee/listview2.xhtml";
                }
                else if(myParam.equals("mcdowntime"))
                {
                    myParam ="pages/report/mcdowntime/listview.xhtml" ;
                }
                else if(myParam.equals("historydowntime"))
                {
                    myParam ="pages/report/historydowntime/listview.xhtml";
                }
                else{
        		//System.out.println("Sucess");
//        		myParam = "pages/appConfig/role.xhtml";
        		myParam = "pages/appConfig/role/listview.xhtml";
                    //home/pang/NetBeansProjects/tct_project_netbean/web/pages/appConfig/role/listview.xhtml
        	}
                
            setParam(myParam); 
        } else  {
        	myParam = "pages/moduleFontLine/createWorksheet.xhtml";
           // setParam("not defined");
        	setParam(myParam);
        }
        
    }
    public Ng getNg()
    {
        FacesContext context = FacesContext.getCurrentInstance();
        return (Ng) context.getExternalContext().getSessionMap().get("qualityNgControl"); 
    }
    public Yield getYield()
    {
        FacesContext context = FacesContext.getCurrentInstance();
        return (Yield) context.getExternalContext().getSessionMap().get("qualityYieldControl"); 
    }
    public NgRepair getNgRepair()
    {
        FacesContext context = FacesContext.getCurrentInstance();
        return (NgRepair) context.getExternalContext().getSessionMap().get("qualityNgRepairControl"); 
    } 
    /**
     * Get the orientation of the menu ("horizontal" or "vertical")
     *
     * @return the orientation of the menu
     */
    public String getOrientation() {
        return orientation;
    }

    /**
     * Set the orientation of the menu ("horizontal" or "vertical").
     *
     * @param orientation the new orientation of the menu
     */
    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }

	/**
	 * @return the dataTest
	 */
	public String getDataTest() {
		return dataTest;
	}

	/**
	 * @param dataTest the dataTest to set
	 */
	public void setDataTest(String dataTest) {
		this.dataTest = dataTest;
		visible = false; 
	}

	/**
	 * @return the urlFile
	 */
       
}