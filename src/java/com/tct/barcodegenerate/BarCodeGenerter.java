/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.barcodegenerate;

import com.appCinfigpage.bean.fc;
import com.google.zxing.*;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;
import com.tct.data.Countbarcodeid;
import com.tct.data.LotControl;
import com.tct.data.jpa.CountbarcodeidJpaController;
import com.tct.data.jpa.LotControlJpaController;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import javax.persistence.Persistence;

/**
 *
 * @author The_Boy_Cs
 */
public class BarCodeGenerter {

    private final String CHARSET = "UTF-8";
    private final int SIZE = 250;
    private final int HEIGHT = 50;
    private FileOutputStream outputstream;

    public String createQrCode(String value, String filePath) throws FileNotFoundException, WriterException, IOException {
        String data;
        String path;
        Charset charset = Charset.forName(CHARSET);
        CharsetEncoder encoder = charset.newEncoder();
        byte[] b = null;
        try {
            // Convert a string to UTF-8 bytes in a ByteBuffer
            ByteBuffer bbuf = encoder.encode(CharBuffer.wrap(value));
            b = bbuf.array();
        } catch (CharacterCodingException e) {
            System.out.println(e.getMessage());
        }

        data = new String(b, CHARSET);
        Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>(2);
        hints.put(EncodeHintType.CHARACTER_SET, CHARSET);
        path = filePath + value + ".png";
        writFile(new MultiFormatWriter().encode(data, BarcodeFormat.QR_CODE, SIZE, SIZE, hints), path);

        return filePath;
    }
    private Countbarcodeid countbarcodeid;

    public int getMaxnumberOfLotcontrol() {
        print("================= Get Maxnumber on date in lotcontrol ===========");
        countbarcodeid = getCountbarcodeidJpaController().findCountbarcodeidByMaxnum();
        SimpleDateFormat dbdate = new SimpleDateFormat("yyyyMM");
        int dbd = 0;
        if (countbarcodeid != null) {
            dbd = Integer.parseInt(dbdate.format(countbarcodeid.getDatecreate()));
        }
        print("= DB date : " + dbd);
        int nowd = Integer.parseInt(dbdate.format(new Date()));
        print("= Now date : " + nowd);
        int tmp = 10000;
        if (dbd == nowd) {
            tmp = Integer.parseInt(countbarcodeid.getBarcodenum());
        } else if (dbd < nowd) {
            countbarcodeid = new Countbarcodeid();
            countbarcodeid.setBarcodenum(tmp + "");
            countbarcodeid.setDatecreate(new Date());
            getCountbarcodeidJpaController().create(countbarcodeid);
        }
        print("= TMP : " + tmp);
        return tmp;
    }
     public int getMaxnumberOfLotcontrolType(String type) {
        print("================= Get Maxnumber on date in lotcontrol =========== : "+type);
        countbarcodeid  = new Countbarcodeid();
        countbarcodeid = getCountbarcodeidJpaController().findCountbarcodeidByMaxnumType(type); 
        if(countbarcodeid == null)
        {
            //*** create new type worksheet
            fc.print("******** Type not null ******** ");
            int tmp = 10000;
            countbarcodeid = new Countbarcodeid();
            countbarcodeid.setBarcodenum(tmp + "");
            countbarcodeid.setDatecreate(new Date());
            countbarcodeid.setTypeWorksheetId(type);
            getCountbarcodeidJpaController().create(countbarcodeid);
            return tmp;
        }
        else
        {
            fc.print("******** Have ******** ");
            SimpleDateFormat dbdate = new SimpleDateFormat("yyyyMM");
            int dbd = 0;
            if (countbarcodeid != null) { 
                dbd = Integer.parseInt(dbdate.format(countbarcodeid.getDatecreate()));
            }
            print("= DB date : " + dbd);
            int nowd = Integer.parseInt(dbdate.format(new Date()));
            print("= Now date : " + nowd);
            int tmp = 10000;
            if (dbd == nowd) {
                tmp = Integer.parseInt(countbarcodeid.getBarcodenum());
                fc.print("***** tpm ******");
            } else if (dbd < nowd) {
                countbarcodeid = new Countbarcodeid();
                countbarcodeid.setBarcodenum(tmp + "");
                countbarcodeid.setDatecreate(new Date());
                countbarcodeid.setTypeWorksheetId(type);
                getCountbarcodeidJpaController().create(countbarcodeid);
                fc.print("tpm2");
            }
            print("= TMP : " + tmp);
            return tmp;
        }
    }


    public void updateMaxnumberOfLotcontrol(int number) {
        print("=============== Update Maxnumber : " + number + " ===================");
        try {
            countbarcodeid.setBarcodenum("" + number);
            getCountbarcodeidJpaController().edit(countbarcodeid);
        } catch (NonexistentEntityException nex) {
            print("= Error : " + nex);
        } catch (Exception e) {
            print("= Error2 : " + e);
        }
    }
    

    private void print(String str) {
        System.out.println(str);
    }
    private CountbarcodeidJpaController countbarcodeidJpaController;

    private CountbarcodeidJpaController getCountbarcodeidJpaController() {
        if (countbarcodeidJpaController == null) {
            countbarcodeidJpaController = new CountbarcodeidJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return countbarcodeidJpaController;
    }

    public String UseNumberToString(long i) {
        String code = "";
        String num = "" + i;
        if (num.length() == 1) {
            code = "00" + i;
        } else if (num.length() == 2) {
            code = "0" + i;
        } else {
            code = "" + i;
        }
        return code;
    }

    public String createBarcode(String value, String filePath) {
        String path;
        try {
            path = filePath + value + ".png";
            writFile(new Code128Writer().encode(value, BarcodeFormat.CODE_128, SIZE, HEIGHT), path);
        } catch (WriterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "welcomeICEfaces";
    }

    private void writFile(BitMatrix bitMatrix, String filePath) throws FileNotFoundException, IOException {
        print("========== Create file barcode ==================================");
        print("= file parth : " + filePath);
        outputstream = new FileOutputStream(new File(filePath));
        MatrixToImageWriter.writeToStream(bitMatrix, "png", outputstream);
        outputstream.close();
    }
}
