/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Oct 23, 2012, Time : 9:09:27 AM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
@Entity
@Table(name = "send_problem", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SendProblem.findAll", query = "SELECT s FROM SendProblem s"),
    @NamedQuery(name = "SendProblem.findByIdSendProblem", query = "SELECT s FROM SendProblem s WHERE s.idSendProblem = :idSendProblem"),
    @NamedQuery(name = "SendProblem.findByDated", query = "SELECT s FROM SendProblem s WHERE s.dated = :dated"),
    @NamedQuery(name = "SendProblem.findByResult", query = "SELECT s FROM SendProblem s WHERE s.result = :result"),
    @NamedQuery(name = "SendProblem.findByStatus", query = "SELECT s FROM SendProblem s WHERE s.status = :status"),
    @NamedQuery(name = "SendProblem.findByTargetOverCheck", query = "SELECT s FROM SendProblem s WHERE s.targetOverCheck = :targetOverCheck"),
    @NamedQuery(name = "SendProblem.findByTargetOverComment", query = "SELECT s FROM SendProblem s WHERE s.targetOverComment = :targetOverComment")})
public class SendProblem implements Serializable {
    @Basic(optional = false)
    @Column(name = "dated", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dated;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_send_problem", nullable = false, length = 36)
    private String idSendProblem;
    @Basic(optional = false)
    @Column(name = "result", nullable = false, length = 255)
    private String result;
    @Basic(optional = false)
    @Column(name = "status", nullable = false, length = 50)
    private String status;
    @Basic(optional = false)
    @Lob
    @Column(name = "comment_gerneral", nullable = false, length = 65535)
    private String commentGerneral;
    @Basic(optional = false)
    @Lob
    @Column(name = "solution", nullable = false, length = 65535)
    private String solution;
    @Basic(optional = false)
    @Column(name = "target_over_check", nullable = false, length = 20)
    private String targetOverCheck;
    @Basic(optional = false)
    @Column(name = "target_over_comment", nullable = false, length = 255)
    private String targetOverComment;
    @Basic(optional = false)
    @Column(name = "send_email", nullable = false, length = 255)
    private String sendEmail;
    	 

    public SendProblem() {
    }

    public SendProblem(String idSendProblem) {
        this.idSendProblem = idSendProblem;
    }

    public SendProblem(String idSendProblem, Date dated, String result, String status, String commentGerneral, String solution, String targetOverCheck, String targetOverComment) {
        this.idSendProblem = idSendProblem;
        this.dated = dated;
        this.result = result;
        this.status = status;
        this.commentGerneral = commentGerneral;
        this.solution = solution;
        this.targetOverCheck = targetOverCheck;
        this.targetOverComment = targetOverComment;
    }

    public String getIdSendProblem() {
        return idSendProblem;
    }

    public void setIdSendProblem(String idSendProblem) {
        this.idSendProblem = idSendProblem;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCommentGerneral() {
        return commentGerneral;
    }

    public void setCommentGerneral(String commentGerneral) {
        this.commentGerneral = commentGerneral;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public String getTargetOverCheck() {
        return targetOverCheck;
    }

    public void setTargetOverCheck(String targetOverCheck) {
        this.targetOverCheck = targetOverCheck;
    }

    public String getTargetOverComment() {
        return targetOverComment;
    }

    public void setTargetOverComment(String targetOverComment) {
        this.targetOverComment = targetOverComment;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSendProblem != null ? idSendProblem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SendProblem)) {
            return false;
        }
        SendProblem other = (SendProblem) object;
        if ((this.idSendProblem == null && other.idSendProblem != null) || (this.idSendProblem != null && !this.idSendProblem.equals(other.idSendProblem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.SendProblem[ idSendProblem=" + idSendProblem + " ]";
    }

    public Date getDated() {
        return dated;
    }

    public void setDated(Date dated) {
        this.dated = dated;
    }

    public String getSendEmail() {
        return sendEmail;
    }

    public void setSendEmail(String sendEmail) {
        this.sendEmail = sendEmail;
    }
 
}
