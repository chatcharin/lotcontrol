/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 28, 2012, Time : 5:05:44 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
@Entity
@Table(name = "radar_color", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RadarColor.findAll", query = "SELECT r FROM RadarColor r"),
    @NamedQuery(name = "RadarColor.findByIdRadarColor", query = "SELECT r FROM RadarColor r WHERE r.idRadarColor = :idRadarColor"),
    @NamedQuery(name = "RadarColor.findByRandarName", query = "SELECT r FROM RadarColor r WHERE r.randarName = :randarName"),
    @NamedQuery(name = "RadarColor.findByCTarget", query = "SELECT r FROM RadarColor r WHERE r.cTarget = :cTarget"),
    @NamedQuery(name = "RadarColor.findByCActual", query = "SELECT r FROM RadarColor r WHERE r.cActual = :cActual"),
    @NamedQuery(name = "RadarColor.findByCPrecent", query = "SELECT r FROM RadarColor r WHERE r.cPrecent = :cPrecent"),
    @NamedQuery(name = "RadarColor.findByCTargetShow", query = "SELECT r FROM RadarColor r WHERE r.cTargetShow = :cTargetShow"),
    @NamedQuery(name = "RadarColor.findByCActualShow", query = "SELECT r FROM RadarColor r WHERE r.cActualShow = :cActualShow"),
    @NamedQuery(name = "RadarColor.findByCPercentShow", query = "SELECT r FROM RadarColor r WHERE r.cPercentShow = :cPercentShow"),
    @NamedQuery(name = "RadarColor.findByDeleted", query = "SELECT r FROM RadarColor r WHERE r.deleted = :deleted")})
public class RadarColor implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_radar_color", nullable = false, length = 36)
    private String idRadarColor;
    @Basic(optional = false)
    @Column(name = "randar_name", nullable = false, length = 255)
    private String randarName;
    @Basic(optional = false)
    @Column(name = "c_target", nullable = false, length = 255)
    private String cTarget;
    @Basic(optional = false)
    @Column(name = "c_actual", nullable = false, length = 255)
    private String cActual;
    @Basic(optional = false)
    @Column(name = "c_precent", nullable = false, length = 255)
    private String cPrecent;
    @Basic(optional = false)
    @Column(name = "c_target_show", nullable = false, length = 50)
    private String cTargetShow;
    @Basic(optional = false)
    @Column(name = "c_actual_show", nullable = false, length = 50)
    private String cActualShow;
    @Basic(optional = false)
    @Column(name = "c_percent_show", nullable = false, length = 50)
    private String cPercentShow;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idColor")
    private Collection<Radar> radarCollection;

    public RadarColor() {
    }

    public RadarColor(String idRadarColor) {
        this.idRadarColor = idRadarColor;
    }

    public RadarColor(String idRadarColor, String randarName, String cTarget, String cActual, String cPrecent, String cTargetShow, String cActualShow, String cPercentShow, int deleted) {
        this.idRadarColor = idRadarColor;
        this.randarName = randarName;
        this.cTarget = cTarget;
        this.cActual = cActual;
        this.cPrecent = cPrecent;
        this.cTargetShow = cTargetShow;
        this.cActualShow = cActualShow;
        this.cPercentShow = cPercentShow;
        this.deleted = deleted;
    }

    public String getIdRadarColor() {
        return idRadarColor;
    }

    public void setIdRadarColor(String idRadarColor) {
        this.idRadarColor = idRadarColor;
    }

    public String getRandarName() {
        return randarName;
    }

    public void setRandarName(String randarName) {
        this.randarName = randarName;
    }

    public String getCTarget() {
        return cTarget;
    }

    public void setCTarget(String cTarget) {
        this.cTarget = cTarget;
    }

    public String getCActual() {
        return cActual;
    }

    public void setCActual(String cActual) {
        this.cActual = cActual;
    }

    public String getCPrecent() {
        return cPrecent;
    }

    public void setCPrecent(String cPrecent) {
        this.cPrecent = cPrecent;
    }

    public String getCTargetShow() {
        return cTargetShow;
    }

    public void setCTargetShow(String cTargetShow) {
        this.cTargetShow = cTargetShow;
    }

    public String getCActualShow() {
        return cActualShow;
    }

    public void setCActualShow(String cActualShow) {
        this.cActualShow = cActualShow;
    }

    public String getCPercentShow() {
        return cPercentShow;
    }

    public void setCPercentShow(String cPercentShow) {
        this.cPercentShow = cPercentShow;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    @XmlTransient
    public Collection<Radar> getRadarCollection() {
        return radarCollection;
    }

    public void setRadarCollection(Collection<Radar> radarCollection) {
        this.radarCollection = radarCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRadarColor != null ? idRadarColor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RadarColor)) {
            return false;
        }
        RadarColor other = (RadarColor) object;
        if ((this.idRadarColor == null && other.idRadarColor != null) || (this.idRadarColor != null && !this.idRadarColor.equals(other.idRadarColor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.RadarColor[ idRadarColor=" + idRadarColor + " ]";
    }

}
