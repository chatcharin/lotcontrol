/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "pcb01", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pcb01.findAll", query = "SELECT p FROM Pcb01 p"),
    @NamedQuery(name = "Pcb01.findById", query = "SELECT p FROM Pcb01 p WHERE p.id = :id"),
    @NamedQuery(name = "Pcb01.findByStamp", query = "SELECT p FROM Pcb01 p WHERE p.stamp = :stamp"),
    @NamedQuery(name = "Pcb01.findByUser", query = "SELECT p FROM Pcb01 p WHERE p.userCreate = :userCreate"),
     @NamedQuery(name = "Pcb01.findBydetailIdDetail", query = "SELECT p FROM Pcb01 p WHERE p.detailIdDetail = :detailIdDetail"),
    @NamedQuery(name = "Pcb01.findByType", query = "SELECT p FROM Pcb01 p WHERE p.type = :type")})
public class Pcb01 implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Column(name = "stamp", length = 100)
    private String stamp;
    @Basic(optional = false)
    @Column(name = "user_create", nullable = false, length = 36)
    private String userCreate;
    @Column(name = "type", length = 255)
    private String type;
    @JoinColumn(name = "detail_id_detail", referencedColumnName = "id_detail", nullable = false)
    @ManyToOne(optional = false)
    private Details detailIdDetail;

    public Pcb01() {
    }

    public Pcb01(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStamp() {
        return stamp;
    }

    public void setStamp(String stamp) {
        this.stamp = stamp;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Details getDetailIdDetail() {
        return detailIdDetail;
    }

    public void setDetailIdDetail(Details detailIdDetail) {
        this.detailIdDetail = detailIdDetail;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pcb01)) {
            return false;
        }
        Pcb01 other = (Pcb01) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.Pcb01[ id=" + id + " ]";
    }
    
}
