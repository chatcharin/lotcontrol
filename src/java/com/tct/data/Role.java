/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "role", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Role.findAll", query = "SELECT r FROM Role r"),
    @NamedQuery(name = "Role.findByIdRole", query = "SELECT r FROM Role r WHERE r.idRole = :idRole"),
    @NamedQuery(name = "Role.findByRoleName", query = "SELECT r FROM Role r WHERE (r.deleted ='0') and (r.roleName LIKE :roleName) ORDER BY r.roleName ASC"),
    @NamedQuery(name = "Role.findBySeeFl", query = "SELECT r FROM Role r WHERE r.seeFl = :seeFl"),
    @NamedQuery(name = "Role.findByCreateFl", query = "SELECT r FROM Role r WHERE r.createFl = :createFl"),
    @NamedQuery(name = "Role.findByListFl", query = "SELECT r FROM Role r WHERE r.listFl = :listFl"),
    @NamedQuery(name = "Role.findByDeleteFl", query = "SELECT r FROM Role r WHERE r.deleteFl = :deleteFl"),
    @NamedQuery(name = "Role.findByEditFl", query = "SELECT r FROM Role r WHERE r.editFl = :editFl"),
    @NamedQuery(name = "Role.findByDetailFrontLine", query = "SELECT r FROM Role r WHERE r.detailFrontLine = :detailFrontLine"),
    @NamedQuery(name = "Role.findBySeeBl", query = "SELECT r FROM Role r WHERE r.seeBl = :seeBl"),
    @NamedQuery(name = "Role.findByCreateAssy", query = "SELECT r FROM Role r WHERE r.createAssy = :createAssy"),
    @NamedQuery(name = "Role.findByListAssy", query = "SELECT r FROM Role r WHERE r.listAssy = :listAssy"),
    @NamedQuery(name = "Role.findByDeleteAssy", query = "SELECT r FROM Role r WHERE r.deleteAssy = :deleteAssy"),
    @NamedQuery(name = "Role.findByEditAssy", query = "SELECT r FROM Role r WHERE r.editAssy = :editAssy"),
    @NamedQuery(name = "Role.findByApproveAssy", query = "SELECT r FROM Role r WHERE r.approveAssy = :approveAssy"),
    @NamedQuery(name = "Role.findByDetailAssy", query = "SELECT r FROM Role r WHERE r.detailAssy = :detailAssy"),
    @NamedQuery(name = "Role.findByCreateAuto", query = "SELECT r FROM Role r WHERE r.createAuto = :createAuto"),
    @NamedQuery(name = "Role.findByListAuto", query = "SELECT r FROM Role r WHERE r.listAuto = :listAuto"),
    @NamedQuery(name = "Role.findByDeleteAuto", query = "SELECT r FROM Role r WHERE r.deleteAuto = :deleteAuto"),
    @NamedQuery(name = "Role.findByEditAuto", query = "SELECT r FROM Role r WHERE r.editAuto = :editAuto"),
    @NamedQuery(name = "Role.findByApproveAuto", query = "SELECT r FROM Role r WHERE r.approveAuto = :approveAuto"),
    @NamedQuery(name = "Role.findByDetailAuto", query = "SELECT r FROM Role r WHERE r.detailAuto = :detailAuto"),
    @NamedQuery(name = "Role.findByCreateAssembly", query = "SELECT r FROM Role r WHERE r.createAssembly = :createAssembly"),
    @NamedQuery(name = "Role.findByListAssembly", query = "SELECT r FROM Role r WHERE r.listAssembly = :listAssembly"),
    @NamedQuery(name = "Role.findByDeleteAssembly", query = "SELECT r FROM Role r WHERE r.deleteAssembly = :deleteAssembly"),
    @NamedQuery(name = "Role.findByEditAssembly", query = "SELECT r FROM Role r WHERE r.editAssembly = :editAssembly"),
    @NamedQuery(name = "Role.findByApproveAssembly", query = "SELECT r FROM Role r WHERE r.approveAssembly = :approveAssembly"),
    @NamedQuery(name = "Role.findByDetailAssembly", query = "SELECT r FROM Role r WHERE r.detailAssembly = :detailAssembly"),
    @NamedQuery(name = "Role.findBySeePcb", query = "SELECT r FROM Role r WHERE r.seePcb = :seePcb"),
    @NamedQuery(name = "Role.findByCreatePcb01", query = "SELECT r FROM Role r WHERE r.createPcb01 = :createPcb01"),
    @NamedQuery(name = "Role.findByListPcb01", query = "SELECT r FROM Role r WHERE r.listPcb01 = :listPcb01"),
    @NamedQuery(name = "Role.findByDeletePcb01", query = "SELECT r FROM Role r WHERE r.deletePcb01 = :deletePcb01"),
    @NamedQuery(name = "Role.findByEditPcb01", query = "SELECT r FROM Role r WHERE r.editPcb01 = :editPcb01"),
    @NamedQuery(name = "Role.findByApprovePcb01", query = "SELECT r FROM Role r WHERE r.approvePcb01 = :approvePcb01"),
    @NamedQuery(name = "Role.findByDetailPcb01", query = "SELECT r FROM Role r WHERE r.detailPcb01 = :detailPcb01"),
    @NamedQuery(name = "Role.findByCreatePcb02", query = "SELECT r FROM Role r WHERE r.createPcb02 = :createPcb02"),
    @NamedQuery(name = "Role.findByListPcb02", query = "SELECT r FROM Role r WHERE r.listPcb02 = :listPcb02"),
    @NamedQuery(name = "Role.findByDeletePcb02", query = "SELECT r FROM Role r WHERE r.deletePcb02 = :deletePcb02"),
    @NamedQuery(name = "Role.findByEditPcb02", query = "SELECT r FROM Role r WHERE r.editPcb02 = :editPcb02"),
    @NamedQuery(name = "Role.findByApprovePcb02", query = "SELECT r FROM Role r WHERE r.approvePcb02 = :approvePcb02"),
    @NamedQuery(name = "Role.findByDetailPcb02", query = "SELECT r FROM Role r WHERE r.detailPcb02 = :detailPcb02"),
    @NamedQuery(name = "Role.findByCreatePcb03", query = "SELECT r FROM Role r WHERE r.createPcb03 = :createPcb03"),
    @NamedQuery(name = "Role.findByListPcb03", query = "SELECT r FROM Role r WHERE r.listPcb03 = :listPcb03"),
    @NamedQuery(name = "Role.findByDeletePcb03", query = "SELECT r FROM Role r WHERE r.deletePcb03 = :deletePcb03"),
    @NamedQuery(name = "Role.findByEditPcb03", query = "SELECT r FROM Role r WHERE r.editPcb03 = :editPcb03"),
    @NamedQuery(name = "Role.findByApprovePcb03", query = "SELECT r FROM Role r WHERE r.approvePcb03 = :approvePcb03"),
    @NamedQuery(name = "Role.findByDetailPcb03", query = "SELECT r FROM Role r WHERE r.detailPcb03 = :detailPcb03"),
    @NamedQuery(name = "Role.findBySeeRefu", query = "SELECT r FROM Role r WHERE r.seeRefu = :seeRefu"),
    @NamedQuery(name = "Role.findByCreateRefu01", query = "SELECT r FROM Role r WHERE r.createRefu01 = :createRefu01"),
    @NamedQuery(name = "Role.findByListRefu01", query = "SELECT r FROM Role r WHERE r.listRefu01 = :listRefu01"),
    @NamedQuery(name = "Role.findByDeleteRefu01", query = "SELECT r FROM Role r WHERE r.deleteRefu01 = :deleteRefu01"),
    @NamedQuery(name = "Role.findByEditRefu01", query = "SELECT r FROM Role r WHERE r.editRefu01 = :editRefu01"),
    @NamedQuery(name = "Role.findByApproveRefu01", query = "SELECT r FROM Role r WHERE r.approveRefu01 = :approveRefu01"),
    @NamedQuery(name = "Role.findByDetailRefu01", query = "SELECT r FROM Role r WHERE r.detailRefu01 = :detailRefu01"),
    @NamedQuery(name = "Role.findByCreateRefu02", query = "SELECT r FROM Role r WHERE r.createRefu02 = :createRefu02"),
    @NamedQuery(name = "Role.findByListRefu02", query = "SELECT r FROM Role r WHERE r.listRefu02 = :listRefu02"),
    @NamedQuery(name = "Role.findByDeleteRefu02", query = "SELECT r FROM Role r WHERE r.deleteRefu02 = :deleteRefu02"),
    @NamedQuery(name = "Role.findByEditRefu02", query = "SELECT r FROM Role r WHERE r.editRefu02 = :editRefu02"),
    @NamedQuery(name = "Role.findByApproveRefu02", query = "SELECT r FROM Role r WHERE r.approveRefu02 = :approveRefu02"),
    @NamedQuery(name = "Role.findByDetailRefu02", query = "SELECT r FROM Role r WHERE r.detailRefu02 = :detailRefu02"),
    @NamedQuery(name = "Role.findByCreateRefu03", query = "SELECT r FROM Role r WHERE r.createRefu03 = :createRefu03"),
    @NamedQuery(name = "Role.findByListRefu03", query = "SELECT r FROM Role r WHERE r.listRefu03 = :listRefu03"),
    @NamedQuery(name = "Role.findByDeleteRefu03", query = "SELECT r FROM Role r WHERE r.deleteRefu03 = :deleteRefu03"),
    @NamedQuery(name = "Role.findByEditRefu03", query = "SELECT r FROM Role r WHERE r.editRefu03 = :editRefu03"),
    @NamedQuery(name = "Role.findByApproveRefu03", query = "SELECT r FROM Role r WHERE r.approveRefu03 = :approveRefu03"),
    @NamedQuery(name = "Role.findByDetailRefu03", query = "SELECT r FROM Role r WHERE r.detailRefu03 = :detailRefu03"),
    @NamedQuery(name = "Role.findBySeeReport", query = "SELECT r FROM Role r WHERE r.seeReport = :seeReport"),
    @NamedQuery(name = "Role.findBySeeSetting", query = "SELECT r FROM Role r WHERE r.seeSetting = :seeSetting"),
    @NamedQuery(name = "Role.findByEditModel", query = "SELECT r FROM Role r WHERE r.editModel = :editModel"),
    @NamedQuery(name = "Role.findByNewProcess", query = "SELECT r FROM Role r WHERE r.newProcess = :newProcess"),
    @NamedQuery(name = "Role.findByDataProcess", query = "SELECT r FROM Role r WHERE r.dataProcess = :dataProcess"),
    @NamedQuery(name = "Role.findByEditNg", query = "SELECT r FROM Role r WHERE r.editNg = :editNg"),
    @NamedQuery(name = "Role.findBySelectBar", query = "SELECT r FROM Role r WHERE r.selectBar = :selectBar"),
    @NamedQuery(name = "Role.findBySeeMc", query = "SELECT r FROM Role r WHERE r.seeMc = :seeMc"),
    @NamedQuery(name = "Role.findByCreateMc", query = "SELECT r FROM Role r WHERE r.createMc = :createMc"),
    @NamedQuery(name = "Role.findByMcPref", query = "SELECT r FROM Role r WHERE r.mcPref = :mcPref"),
    @NamedQuery(name = "Role.findBySeeAppconf", query = "SELECT r FROM Role r WHERE r.seeAppconf = :seeAppconf"),
    @NamedQuery(name = "Role.findByCreateUser", query = "SELECT r FROM Role r WHERE r.createUser = :createUser"),
    @NamedQuery(name = "Role.findByUserConf", query = "SELECT r FROM Role r WHERE r.userConf = :userConf"),
    @NamedQuery(name = "Role.findAllRolesNotDelete", query = "SELECT r FROM Role r WHERE r.deleted ='0' ORDER by r.roleName ASC"),//@pang
    @NamedQuery(name = "Role.findAllRolesNotIn", query = "SELECT r FROM Role r WHERE (r.deleted ='0') and (r.idRole  NOT in :itemRole)  ORDER by r.roleName ASC"),//@pang
    @NamedQuery(name = "Role.findByAddUserRole", query = "SELECT r FROM Role r WHERE r.addUserRole = :addUserRole")})
public class Role implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_role", nullable = false, length = 36)
    private String idRole;
    @Column(name = "role_name", length = 255)
    private String roleName;
    @Column(name = "see_fl")
    private Integer seeFl;
    @Column(name = "create_fl")
    private Integer createFl;
    @Column(name = "list_fl")
    private Integer listFl;
    @Column(name = "delete_fl")
    private Integer deleteFl;
    @Column(name = "edit_fl")
    private Integer editFl;
    @Column(name = "detail_front_line")
    private Integer detailFrontLine;
    @Column(name = "see_bl")
    private Integer seeBl;
    @Column(name = "create_assy")
    private Integer createAssy;
    @Column(name = "list_assy")
    private Integer listAssy;
    @Column(name = "delete_assy")
    private Integer deleteAssy;
    @Column(name = "edit_assy")
    private Integer editAssy;
    @Column(name = "approve_assy")
    private Integer approveAssy;
    @Column(name = "detail_assy")
    private Integer detailAssy;
    @Column(name = "create_auto")
    private Integer createAuto;
    @Column(name = "list_auto")
    private Integer listAuto;
    @Column(name = "delete_auto")
    private Integer deleteAuto;
    @Column(name = "edit_auto")
    private Integer editAuto;
    @Column(name = "approve_auto")
    private Integer approveAuto;
    @Column(name = "detail_auto")
    private Integer detailAuto;
    @Column(name = "create_assembly")
    private Integer createAssembly;
    @Column(name = "list_assembly")
    private Integer listAssembly;
    @Column(name = "delete_assembly")
    private Integer deleteAssembly;
    @Column(name = "edit_assembly")
    private Integer editAssembly;
    @Column(name = "approve_assembly")
    private Integer approveAssembly;
    @Column(name = "detail_assembly")
    private Integer detailAssembly;
    @Column(name = "see_pcb")
    private Integer seePcb;
    @Column(name = "create_pcb01")
    private Integer createPcb01;
    @Column(name = "list_pcb01")
    private Integer listPcb01;
    @Column(name = "delete_pcb01")
    private Integer deletePcb01;
    @Column(name = "edit_pcb01")
    private Integer editPcb01;
    @Column(name = "approve_pcb01")
    private Integer approvePcb01;
    @Column(name = "detail_pcb01")
    private Integer detailPcb01;
    @Column(name = "create_pcb02")
    private Integer createPcb02;
    @Column(name = "list_pcb02")
    private Integer listPcb02;
    @Column(name = "delete_pcb02")
    private Integer deletePcb02;
    @Column(name = "edit_pcb02")
    private Integer editPcb02;
    @Column(name = "approve_pcb02")
    private Integer approvePcb02;
    @Column(name = "detail_pcb02")
    private Integer detailPcb02;
    @Column(name = "create_pcb03")
    private Integer createPcb03;
    @Column(name = "list_pcb03")
    private Integer listPcb03;
    @Column(name = "delete_pcb03")
    private Integer deletePcb03;
    @Column(name = "edit_pcb03")
    private Integer editPcb03;
    @Column(name = "approve_pcb03")
    private Integer approvePcb03;
    @Column(name = "detail_pcb03")
    private Integer detailPcb03;
    @Column(name = "see_refu")
    private Integer seeRefu;
    @Column(name = "create_refu01")
    private Integer createRefu01;
    @Column(name = "list_refu01")
    private Integer listRefu01;
    @Column(name = "delete_refu01")
    private Integer deleteRefu01;
    @Column(name = "edit_refu01")
    private Integer editRefu01;
    @Column(name = "approve_refu01")
    private Integer approveRefu01;
    @Column(name = "detail_refu01")
    private Integer detailRefu01;
    @Column(name = "create_refu02")
    private Integer createRefu02;
    @Column(name = "list_refu02")
    private Integer listRefu02;
    @Column(name = "delete_refu02")
    private Integer deleteRefu02;
    @Column(name = "edit_refu02")
    private Integer editRefu02;
    @Column(name = "approve_refu02")
    private Integer approveRefu02;
    @Column(name = "detail_refu02")
    private Integer detailRefu02;
    @Column(name = "create_refu03")
    private Integer createRefu03;
    @Column(name = "list_refu03")
    private Integer listRefu03;
    @Column(name = "delete_refu03")
    private Integer deleteRefu03;
    @Column(name = "edit_refu03")
    private Integer editRefu03;
    @Column(name = "approve_refu03")
    private Integer approveRefu03;
    @Column(name = "detail_refu03")
    private Integer detailRefu03;
    @Column(name = "see_report")
    private Integer seeReport;
    @Column(name = "see_setting")
    private Integer seeSetting;
    @Column(name = "edit_model")
    private Integer editModel;
    @Column(name = "new_process")
    private Integer newProcess;
    @Column(name = "data_process")
    private Integer dataProcess;
    @Column(name = "edit_ng")
    private Integer editNg;
    @Column(name = "select_bar")
    private Integer selectBar;
    @Column(name = "see_mc")
    private Integer seeMc;
    @Column(name = "create_mc")
    private Integer createMc;
    @Column(name = "mc_pref")
    private Integer mcPref;
    @Column(name = "see_appconf")
    private Integer seeAppconf;
    @Column(name = "create_user")
    private Integer createUser;
    @Column(name = "user_conf")
    private Integer userConf;
    @Column(name = "add_user_role")
    private Integer addUserRole;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "roleId")
    private Collection<RoleUser> roleUserCollection;
    @Column(name = "descritpion", length = 255) 
    private String desscrition;
    @Column(name = "deleted" , nullable = false)
    private Integer deleted;
    public Role() {
    }

    public Role(String idRole) {
        this.idRole = idRole;
    }

    public String getIdRole() {
        return idRole;
    }

    public void setIdRole(String idRole) {
        this.idRole = idRole;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Integer getSeeFl() {
        return seeFl;
    }

    public void setSeeFl(Integer seeFl) {
        this.seeFl = seeFl;
    }

    public Integer getCreateFl() {
        return createFl;
    }

    public void setCreateFl(Integer createFl) {
        this.createFl = createFl;
    }

    public Integer getListFl() {
        return listFl;
    }

    public void setListFl(Integer listFl) {
        this.listFl = listFl;
    }

    public Integer getDeleteFl() {
        return deleteFl;
    }

    public void setDeleteFl(Integer deleteFl) {
        this.deleteFl = deleteFl;
    }

    public Integer getEditFl() {
        return editFl;
    }

    public void setEditFl(Integer editFl) {
        this.editFl = editFl;
    }

    public Integer getDetailFrontLine() {
        return detailFrontLine;
    }

    public void setDetailFrontLine(Integer detailFrontLine) {
        this.detailFrontLine = detailFrontLine;
    }

    public Integer getSeeBl() {
        return seeBl;
    }

    public void setSeeBl(Integer seeBl) {
        this.seeBl = seeBl;
    }

    public Integer getCreateAssy() {
        return createAssy;
    }

    public void setCreateAssy(Integer createAssy) {
        this.createAssy = createAssy;
    }

    public Integer getListAssy() {
        return listAssy;
    }

    public void setListAssy(Integer listAssy) {
        this.listAssy = listAssy;
    }

    public Integer getDeleteAssy() {
        return deleteAssy;
    }

    public void setDeleteAssy(Integer deleteAssy) {
        this.deleteAssy = deleteAssy;
    }

    public Integer getEditAssy() {
        return editAssy;
    }

    public void setEditAssy(Integer editAssy) {
        this.editAssy = editAssy;
    }

    public Integer getApproveAssy() {
        return approveAssy;
    }

    public void setApproveAssy(Integer approveAssy) {
        this.approveAssy = approveAssy;
    }

    public Integer getDetailAssy() {
        return detailAssy;
    }

    public void setDetailAssy(Integer detailAssy) {
        this.detailAssy = detailAssy;
    }

    public Integer getCreateAuto() {
        return createAuto;
    }

    public void setCreateAuto(Integer createAuto) {
        this.createAuto = createAuto;
    }

    public Integer getListAuto() {
        return listAuto;
    }

    public void setListAuto(Integer listAuto) {
        this.listAuto = listAuto;
    }

    public Integer getDeleteAuto() {
        return deleteAuto;
    }

    public void setDeleteAuto(Integer deleteAuto) {
        this.deleteAuto = deleteAuto;
    }

    public Integer getEditAuto() {
        return editAuto;
    }

    public void setEditAuto(Integer editAuto) {
        this.editAuto = editAuto;
    }

    public Integer getApproveAuto() {
        return approveAuto;
    }

    public void setApproveAuto(Integer approveAuto) {
        this.approveAuto = approveAuto;
    }

    public Integer getDetailAuto() {
        return detailAuto;
    }

    public void setDetailAuto(Integer detailAuto) {
        this.detailAuto = detailAuto;
    }

    public Integer getCreateAssembly() {
        return createAssembly;
    }

    public void setCreateAssembly(Integer createAssembly) {
        this.createAssembly = createAssembly;
    }

    public Integer getListAssembly() {
        return listAssembly;
    }

    public void setListAssembly(Integer listAssembly) {
        this.listAssembly = listAssembly;
    }

    public Integer getDeleteAssembly() {
        return deleteAssembly;
    }

    public void setDeleteAssembly(Integer deleteAssembly) {
        this.deleteAssembly = deleteAssembly;
    }

    public Integer getEditAssembly() {
        return editAssembly;
    }

    public void setEditAssembly(Integer editAssembly) {
        this.editAssembly = editAssembly;
    }

    public Integer getApproveAssembly() {
        return approveAssembly;
    }

    public void setApproveAssembly(Integer approveAssembly) {
        this.approveAssembly = approveAssembly;
    }

    public Integer getDetailAssembly() {
        return detailAssembly;
    }

    public void setDetailAssembly(Integer detailAssembly) {
        this.detailAssembly = detailAssembly;
    }

    public Integer getSeePcb() {
        return seePcb;
    }

    public void setSeePcb(Integer seePcb) {
        this.seePcb = seePcb;
    }

    public Integer getCreatePcb01() {
        return createPcb01;
    }

    public void setCreatePcb01(Integer createPcb01) {
        this.createPcb01 = createPcb01;
    }

    public Integer getListPcb01() {
        return listPcb01;
    }

    public void setListPcb01(Integer listPcb01) {
        this.listPcb01 = listPcb01;
    }

    public Integer getDeletePcb01() {
        return deletePcb01;
    }

    public void setDeletePcb01(Integer deletePcb01) {
        this.deletePcb01 = deletePcb01;
    }

    public Integer getEditPcb01() {
        return editPcb01;
    }

    public void setEditPcb01(Integer editPcb01) {
        this.editPcb01 = editPcb01;
    }

    public Integer getApprovePcb01() {
        return approvePcb01;
    }

    public void setApprovePcb01(Integer approvePcb01) {
        this.approvePcb01 = approvePcb01;
    }

    public Integer getDetailPcb01() {
        return detailPcb01;
    }

    public void setDetailPcb01(Integer detailPcb01) {
        this.detailPcb01 = detailPcb01;
    }

    public Integer getCreatePcb02() {
        return createPcb02;
    }

    public void setCreatePcb02(Integer createPcb02) {
        this.createPcb02 = createPcb02;
    }

    public Integer getListPcb02() {
        return listPcb02;
    }

    public void setListPcb02(Integer listPcb02) {
        this.listPcb02 = listPcb02;
    }

    public Integer getDeletePcb02() {
        return deletePcb02;
    }

    public void setDeletePcb02(Integer deletePcb02) {
        this.deletePcb02 = deletePcb02;
    }

    public Integer getEditPcb02() {
        return editPcb02;
    }

    public void setEditPcb02(Integer editPcb02) {
        this.editPcb02 = editPcb02;
    }

    public Integer getApprovePcb02() {
        return approvePcb02;
    }

    public void setApprovePcb02(Integer approvePcb02) {
        this.approvePcb02 = approvePcb02;
    }

    public Integer getDetailPcb02() {
        return detailPcb02;
    }

    public void setDetailPcb02(Integer detailPcb02) {
        this.detailPcb02 = detailPcb02;
    }

    public Integer getCreatePcb03() {
        return createPcb03;
    }

    public void setCreatePcb03(Integer createPcb03) {
        this.createPcb03 = createPcb03;
    }

    public Integer getListPcb03() {
        return listPcb03;
    }

    public void setListPcb03(Integer listPcb03) {
        this.listPcb03 = listPcb03;
    }

    public Integer getDeletePcb03() {
        return deletePcb03;
    }

    public void setDeletePcb03(Integer deletePcb03) {
        this.deletePcb03 = deletePcb03;
    }

    public Integer getEditPcb03() {
        return editPcb03;
    }

    public void setEditPcb03(Integer editPcb03) {
        this.editPcb03 = editPcb03;
    }

    public Integer getApprovePcb03() {
        return approvePcb03;
    }

    public void setApprovePcb03(Integer approvePcb03) {
        this.approvePcb03 = approvePcb03;
    }

    public Integer getDetailPcb03() {
        return detailPcb03;
    }

    public void setDetailPcb03(Integer detailPcb03) {
        this.detailPcb03 = detailPcb03;
    }

    public Integer getSeeRefu() {
        return seeRefu;
    }

    public void setSeeRefu(Integer seeRefu) {
        this.seeRefu = seeRefu;
    }

    public Integer getCreateRefu01() {
        return createRefu01;
    }

    public void setCreateRefu01(Integer createRefu01) {
        this.createRefu01 = createRefu01;
    }

    public Integer getListRefu01() {
        return listRefu01;
    }

    public void setListRefu01(Integer listRefu01) {
        this.listRefu01 = listRefu01;
    }

    public Integer getDeleteRefu01() {
        return deleteRefu01;
    }

    public void setDeleteRefu01(Integer deleteRefu01) {
        this.deleteRefu01 = deleteRefu01;
    }

    public Integer getEditRefu01() {
        return editRefu01;
    }

    public void setEditRefu01(Integer editRefu01) {
        this.editRefu01 = editRefu01;
    }

    public Integer getApproveRefu01() {
        return approveRefu01;
    }

    public void setApproveRefu01(Integer approveRefu01) {
        this.approveRefu01 = approveRefu01;
    }

    public Integer getDetailRefu01() {
        return detailRefu01;
    }

    public void setDetailRefu01(Integer detailRefu01) {
        this.detailRefu01 = detailRefu01;
    }

    public Integer getCreateRefu02() {
        return createRefu02;
    }

    public void setCreateRefu02(Integer createRefu02) {
        this.createRefu02 = createRefu02;
    }

    public Integer getListRefu02() {
        return listRefu02;
    }

    public void setListRefu02(Integer listRefu02) {
        this.listRefu02 = listRefu02;
    }

    public Integer getDeleteRefu02() {
        return deleteRefu02;
    }

    public void setDeleteRefu02(Integer deleteRefu02) {
        this.deleteRefu02 = deleteRefu02;
    }

    public Integer getEditRefu02() {
        return editRefu02;
    }

    public void setEditRefu02(Integer editRefu02) {
        this.editRefu02 = editRefu02;
    }

    public Integer getApproveRefu02() {
        return approveRefu02;
    }

    public void setApproveRefu02(Integer approveRefu02) {
        this.approveRefu02 = approveRefu02;
    }

    public Integer getDetailRefu02() {
        return detailRefu02;
    }

    public void setDetailRefu02(Integer detailRefu02) {
        this.detailRefu02 = detailRefu02;
    }

    public Integer getCreateRefu03() {
        return createRefu03;
    }

    public void setCreateRefu03(Integer createRefu03) {
        this.createRefu03 = createRefu03;
    }

    public Integer getListRefu03() {
        return listRefu03;
    }

    public void setListRefu03(Integer listRefu03) {
        this.listRefu03 = listRefu03;
    }

    public Integer getDeleteRefu03() {
        return deleteRefu03;
    }

    public void setDeleteRefu03(Integer deleteRefu03) {
        this.deleteRefu03 = deleteRefu03;
    }

    public Integer getEditRefu03() {
        return editRefu03;
    }

    public void setEditRefu03(Integer editRefu03) {
        this.editRefu03 = editRefu03;
    }

    public Integer getApproveRefu03() {
        return approveRefu03;
    }

    public void setApproveRefu03(Integer approveRefu03) {
        this.approveRefu03 = approveRefu03;
    }

    public Integer getDetailRefu03() {
        return detailRefu03;
    }

    public void setDetailRefu03(Integer detailRefu03) {
        this.detailRefu03 = detailRefu03;
    }

    public Integer getSeeReport() {
        return seeReport;
    }

    public void setSeeReport(Integer seeReport) {
        this.seeReport = seeReport;
    }

    public Integer getSeeSetting() {
        return seeSetting;
    }

    public void setSeeSetting(Integer seeSetting) {
        this.seeSetting = seeSetting;
    }

    public Integer getEditModel() {
        return editModel;
    }

    public void setEditModel(Integer editModel) {
        this.editModel = editModel;
    }

    public Integer getNewProcess() {
        return newProcess;
    }

    public void setNewProcess(Integer newProcess) {
        this.newProcess = newProcess;
    }

    public Integer getDataProcess() {
        return dataProcess;
    }

    public void setDataProcess(Integer dataProcess) {
        this.dataProcess = dataProcess;
    }

    public Integer getEditNg() {
        return editNg;
    }

    public void setEditNg(Integer editNg) {
        this.editNg = editNg;
    }

    public Integer getSelectBar() {
        return selectBar;
    }

    public void setSelectBar(Integer selectBar) {
        this.selectBar = selectBar;
    }

    public Integer getSeeMc() {
        return seeMc;
    }

    public void setSeeMc(Integer seeMc) {
        this.seeMc = seeMc;
    }

    public Integer getCreateMc() {
        return createMc;
    }

    public void setCreateMc(Integer createMc) {
        this.createMc = createMc;
    }

    public Integer getMcPref() {
        return mcPref;
    }

    public void setMcPref(Integer mcPref) {
        this.mcPref = mcPref;
    }

    public Integer getSeeAppconf() {
        return seeAppconf;
    }

    public void setSeeAppconf(Integer seeAppconf) {
        this.seeAppconf = seeAppconf;
    }

    public Integer getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Integer createUser) {
        this.createUser = createUser;
    }

    public Integer getUserConf() {
        return userConf;
    }

    public void setUserConf(Integer userConf) {
        this.userConf = userConf;
    }

    public Integer getAddUserRole() {
        return addUserRole;
    }

    public void setAddUserRole(Integer addUserRole) {
        this.addUserRole = addUserRole;
    }

    @XmlTransient
    public Collection<RoleUser> getRoleUserCollection() {
        return roleUserCollection;
    }

    public void setRoleUserCollection(Collection<RoleUser> roleUserCollection) {
        this.roleUserCollection = roleUserCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRole != null ? idRole.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Role)) {
            return false;
        }
        Role other = (Role) object;
        if ((this.idRole == null && other.idRole != null) || (this.idRole != null && !this.idRole.equals(other.idRole))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.Role[ idRole=" + idRole + " ]";
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public String getDesscrition() {
        return desscrition;
    }

    public void setDesscrition(String desscrition) {
        this.desscrition = desscrition;
    }
    
}
