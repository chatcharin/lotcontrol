/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "role_user", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RoleUser.findAll", query = "SELECT r FROM RoleUser r"),
    @NamedQuery(name = "RoleUser.findById", query = "SELECT r FROM RoleUser r WHERE r.id = :id"),
    @NamedQuery(name = "RoleUser.findByCreateDate", query = "SELECT r FROM RoleUser r WHERE r.createDate = :createDate"),
    @NamedQuery(name = "RoleUser.findByDeleted", query = "SELECT r FROM RoleUser r WHERE r.deleted = :deleted"),
    @NamedQuery(name = "RoleUser.findByUserCreate", query = "SELECT r FROM RoleUser r WHERE r.userCreate = :userCreate"),
    @NamedQuery(name = "RoleUser.findByUserId", query = "SELECT r FROM RoleUser r WHERE (r.deleted ='0') and (r.usersId = :usersId)" )})
public class RoleUser implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "deleted")
    private Integer deleted;
    @Basic(optional = false)
    @Column(name = "user_create", nullable = false, length = 36)
    private String userCreate;
    @JoinColumn(name = "role_id", referencedColumnName = "id_role", nullable = false)
    @ManyToOne(optional = false)
    private Role roleId;
    @JoinColumn(name = "users_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Users usersId;

    public RoleUser() {
    }

    public RoleUser(String id) {
        this.id = id;
    }

    public RoleUser(String id, String userCreate) {
        this.id = id;
        this.userCreate = userCreate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public Role getRoleId() {
        return roleId;
    }

    public void setRoleId(Role roleId) {
        this.roleId = roleId;
    }

    public Users getUsersId() {
        return usersId;
    }

    public void setUsersId(Users usersId) {
        this.usersId = usersId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RoleUser)) {
            return false;
        }
        RoleUser other = (RoleUser) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.RoleUser[ id=" + id + " ]";
    }
    
}
