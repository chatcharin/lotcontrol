/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "ng_pool", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NgPool.findAll", query = "SELECT n FROM NgPool n"),
    @NamedQuery(name = "NgPool.findByNotdeleted", query = "SELECT n FROM NgPool n WHERE n.deleted ='0' ORDER BY n.ngName ASC"),
    @NamedQuery(name = "NgPool.findByIdNg", query = "SELECT n FROM NgPool n WHERE n.idNg = :idNg"),
    @NamedQuery(name = "NgPool.findByNgCode", query = "SELECT n FROM NgPool n WHERE n.deleted ='0' and n.ngCode LIKE :ngCode"),
    @NamedQuery(name = "NgPool.findByNgName", query = "SELECT n FROM NgPool n WHERE n.deleted ='0' and n.ngName LIKE :ngName"),
    @NamedQuery(name = "NgPool.findByStatus", query = "SELECT n FROM NgPool n WHERE n.deleted ='0' and n.status = :status"),
    @NamedQuery(name = "NgPool.findByCreateDate", query = "SELECT n FROM NgPool n WHERE n.createDate = :createDate"),
    @NamedQuery(name = "NgPool.findByUserCreate", query = "SELECT n FROM NgPool n WHERE n.userCreate = :userCreate"),
    @NamedQuery(name = "NgPool.findByUserApprove", query = "SELECT n FROM NgPool n WHERE n.userApprove = :userApprove"),
    @NamedQuery(name = "NgPool.findByNgType", query = "SELECT n FROM NgPool n WHERE n.deleted ='0' and n.ngType = :ngType")})
public class NgPool implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_ng", nullable = false, length = 36)
    private String idNg;
    @Column(name = "ng_code", length = 100)
    private String ngCode;
    @Column(name = "ng_name", length = 100)
    private String ngName;
    @Column(name = "status", length = 100)
    private String status;
    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "user_create", nullable = false, length = 36)
    private String userCreate;
    @Column(name = "user_approve", length = 36)
    private String userApprove;
    @Column(name = "ng_type", length = 100)
    private String ngType;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idNg")
    private Collection<Pc2ng> pc2ngCollection;
    @Column(name = "user_modifire ", length = 36)
    private String userModifire;
    @Column(name = "date_modifire ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateModifire;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    
    public NgPool() {
    }

    public NgPool(String idNg) {
        this.idNg = idNg;
    }

    public NgPool(String idNg, String userCreate) {
        this.idNg = idNg;
        this.userCreate = userCreate;
    }

    public String getIdNg() {
        return idNg;
    }

    public void setIdNg(String idNg) {
        this.idNg = idNg;
    }

    public String getNgCode() {
        return ngCode;
    }

    public void setNgCode(String ngCode) {
        this.ngCode = ngCode;
    }

    public String getNgName() {
        return ngName;
    }

    public void setNgName(String ngName) {
        this.ngName = ngName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserApprove() {
        return userApprove;
    }

    public void setUserApprove(String userApprove) {
        this.userApprove = userApprove;
    }

    public String getNgType() {
        return ngType;
    }

    public void setNgType(String ngType) {
        this.ngType = ngType;
    }

    @XmlTransient
    public Collection<Pc2ng> getPc2ngCollection() {
        return pc2ngCollection;
    }

    public void setPc2ngCollection(Collection<Pc2ng> pc2ngCollection) {
        this.pc2ngCollection = pc2ngCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNg != null ? idNg.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NgPool)) {
            return false;
        }
        NgPool other = (NgPool) object;
        if ((this.idNg == null && other.idNg != null) || (this.idNg != null && !this.idNg.equals(other.idNg))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.NgPool[ idNg=" + idNg + " ]";
    }

    public Date getDateModifire() {
        return dateModifire;
    }

    public void setDateModifire(Date dateModifire) {
        this.dateModifire = dateModifire;
    }

    public String getUserModifire() {
        return userModifire;
    }

    public void setUserModifire(String userModifire) {
        this.userModifire = userModifire;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }
    
}
