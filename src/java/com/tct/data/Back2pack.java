/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author The_Boy_Cs
 * 
 * This Assembly to packing
 * 
 */
@Entity
@Table(name = "back2pack", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Back2pack.findAll", query = "SELECT b FROM Back2pack b"),
    @NamedQuery(name = "Back2pack.findByBack2packId", query = "SELECT b FROM Back2pack b WHERE b.back2packId = :back2packId")})
public class Back2pack implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "back2pack_id", nullable = false, length = 36)
    private String back2packId;
    @Basic(optional = false)
    @Column(name = "qty", nullable = false, length = 255)
    private String qty;
    @Column(name = "createdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdate;
    @JoinColumn(name = "lotcontrol_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private LotControl lotcontrolId;
    @JoinColumn(name = "details_id_detail", referencedColumnName = "id_detail", nullable = false)
    @ManyToOne(optional = false)
    private Details detailsIdDetail;

    public Back2pack() {
    }

    public Back2pack(String back2packId) {
        this.back2packId = back2packId;
    }

    public Back2pack(String back2packId, String qty) {
        this.back2packId = back2packId;
        this.qty = qty;
    }

    public String getBack2packId() {
        return back2packId;
    }

    public void setBack2packId(String back2packId) {
        this.back2packId = back2packId;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public LotControl getLotcontrolId() {
        return lotcontrolId;
    }

    public void setLotcontrolId(LotControl lotcontrolId) {
        this.lotcontrolId = lotcontrolId;
    }

    public Details getDetailsIdDetail() {
        return detailsIdDetail;
    }

    public void setDetailsIdDetail(Details detailsIdDetail) {
        this.detailsIdDetail = detailsIdDetail;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (back2packId != null ? back2packId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Back2pack)) {
            return false;
        }
        Back2pack other = (Back2pack) object;
        if ((this.back2packId == null && other.back2packId != null) || (this.back2packId != null && !this.back2packId.equals(other.back2packId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.Back2pack[ back2packId=" + back2packId + " ]";
    }
    
}
