/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * Machine User
 *
 * @author pang Development by Chusak laojang Date : Nov 1, 2012, Time : 2:15:39
 * PM Copy Right 4 Xtreme Co.,Ltd.
 */
@Entity
@Table(name = "std_time_setting", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StdTimeSetting.findAll", query = "SELECT s FROM StdTimeSetting s"),
    @NamedQuery(name = "StdTimeSetting.findByIdStdSetting", query = "SELECT s FROM StdTimeSetting s WHERE s.idStdSetting = :idStdSetting"),
    @NamedQuery(name = "StdTimeSetting.findByHeaderName", query = "SELECT s FROM StdTimeSetting s WHERE s.headerName = :headerName"),
    @NamedQuery(name = "StdTimeSetting.findByHours", query = "SELECT s FROM StdTimeSetting s WHERE s.hours = :hours"),
    @NamedQuery(name = "StdTimeSetting.findByMinute", query = "SELECT s FROM StdTimeSetting s WHERE s.minute = :minute"),
    @NamedQuery(name = "StdTimeSetting.findByDateCreate", query = "SELECT s FROM StdTimeSetting s WHERE s.dateCreate = :dateCreate"),
    @NamedQuery(name = "StdTimeSetting.findByDeleted", query = "SELECT s FROM StdTimeSetting s WHERE s.deleted = :deleted")})
public class StdTimeSetting implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_std_setting", nullable = false, length = 36)
    private String idStdSetting;
    @Basic(optional = false)
    @Column(name = "header_name", nullable = false, length = 255)
    private String headerName;
    @Basic(optional = false)
    @Column(name = "hours", nullable = false, length = 255)
    private String hours;
    @Basic(optional = false)
    @Column(name = "minute", nullable = false, length = 255)
    private String minute;
    @Basic(optional = false)
    @Column(name = "date_create", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSettingName")
    private Collection<StdDataShowInput> stdDataShowInputCollection;

    public StdTimeSetting() {
    }

    public StdTimeSetting(String idStdSetting) {
        this.idStdSetting = idStdSetting;
    }

    public StdTimeSetting(String idStdSetting, String headerName, String hours, String minute, Date dateCreate, int deleted) {
        this.idStdSetting = idStdSetting;
        this.headerName = headerName;
        this.hours = hours;
        this.minute = minute;
        this.dateCreate = dateCreate;
        this.deleted = deleted;
    }

    public String getIdStdSetting() {
        return idStdSetting;
    }

    public void setIdStdSetting(String idStdSetting) {
        this.idStdSetting = idStdSetting;
    }

    public String getHeaderName() {
        return headerName;
    }

    public void setHeaderName(String headerName) {
        this.headerName = headerName;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStdSetting != null ? idStdSetting.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StdTimeSetting)) {
            return false;
        }
        StdTimeSetting other = (StdTimeSetting) object;
        if ((this.idStdSetting == null && other.idStdSetting != null) || (this.idStdSetting != null && !this.idStdSetting.equals(other.idStdSetting))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.StdTimeSetting[ idStdSetting=" + idStdSetting + " ]";
    }

    @XmlTransient
    public Collection<StdDataShowInput> getStdDataShowInputCollection() {
        return stdDataShowInputCollection;
    }

    public void setStdDataShowInputCollection(Collection<StdDataShowInput> stdDataShowInputCollection) {
        this.stdDataShowInputCollection = stdDataShowInputCollection;
    }
}
