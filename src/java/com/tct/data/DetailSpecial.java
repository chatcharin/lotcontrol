/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 19, 2012, Time : 2:57:01 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
@Entity
@Table(name = "detail_special", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailSpecial.findAll", query = "SELECT d FROM DetailSpecial d WHERE d.deleted=0"),
    @NamedQuery(name = "DetailSpecial.findById", query = "SELECT d FROM DetailSpecial d WHERE d.id = :id and d.deleted=0"),
    @NamedQuery(name = "DetailSpecial.findByName", query = "SELECT d FROM DetailSpecial d WHERE d.name = :name and d.deleted=0 "),
    @NamedQuery(name = "DetailSpecial.findByTypename", query = "SELECT d FROM DetailSpecial d WHERE d.typename = :typename"),
    @NamedQuery(name = "DetailSpecial.findByDescrition", query = "SELECT d FROM DetailSpecial d WHERE d.descrition = :descrition and d.deleted=0 "),
    @NamedQuery(name = "DetailSpecial.findByUserCreate", query = "SELECT d FROM DetailSpecial d WHERE d.userCreate = :userCreate and d.deleted=0 "),
    @NamedQuery(name = "DetailSpecial.findByDateCreate", query = "SELECT d FROM DetailSpecial d WHERE d.dateCreate = :dateCreate and d.deleted=0 "),
    @NamedQuery(name = "DetailSpecial.findByDeleted", query = "SELECT d FROM DetailSpecial d WHERE d.deleted = :deleted")})
public class DetailSpecial implements Serializable {
    @Basic(optional =     false)
    @Column(name = "date_create", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Basic(optional = false)
    @Column(name = "name", nullable = false, length = 255)
    private String name;
    @Column(name = "typename", length = 255)
    private String typename;
    @Column(name = "descrition", length = 255)
    private String descrition;
    @Basic(optional = false)
    @Column(name = "user_create", nullable = false, length = 100)
    private String userCreate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDetailSpecail")
    private Collection<DetailFieldLeft> detailFieldLeftCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDetailSpecail")
    private Collection<DetailFieldRight> detailFieldRightCollection; 
    @Column(name = "line_code", length = 255)
    private String lineCode;
    @Column(name = "select_control_lot", length = 20)
    private String controlLot;
    @Column(name = "input_type", length = 255)
    private String inputType;
    
    public DetailSpecial() {
    }

    public DetailSpecial(String id) {
        this.id = id;
    }

    public DetailSpecial(String id, String name, String userCreate, Date dateCreate) {
        this.id = id;
        this.name = name;
        this.userCreate = userCreate;
        this.dateCreate = dateCreate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public String getDescrition() {
        return descrition;
    }

    public void setDescrition(String descrition) {
        this.descrition = descrition;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    @XmlTransient
    public Collection<DetailFieldLeft> getDetailFieldLeftCollection() {
        return detailFieldLeftCollection;
    }

    public void setDetailFieldLeftCollection(Collection<DetailFieldLeft> detailFieldLeftCollection) {
        this.detailFieldLeftCollection = detailFieldLeftCollection;
    }

    @XmlTransient
    public Collection<DetailFieldRight> getDetailFieldRightCollection() {
        return detailFieldRightCollection;
    }

    public void setDetailFieldRightCollection(Collection<DetailFieldRight> detailFieldRightCollection) {
        this.detailFieldRightCollection = detailFieldRightCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailSpecial)) {
            return false;
        }
        DetailSpecial other = (DetailSpecial) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.DetailSpecial[ id=" + id + " ]";
    }

    public String getControlLot() {
        return controlLot;
    }

    public void setControlLot(String controlLot) {
        this.controlLot = controlLot;
    }

    public String getLineCode() {
        return lineCode;
    }

    public void setLineCode(String lineCode) {
        this.lineCode = lineCode;

    }

    
    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    


    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    } 
}
