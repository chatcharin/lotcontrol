/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author tawat
 */
@Entity
@Table(name = "detail_field_left_data", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailFieldLeftData.findAll", query = "SELECT d FROM DetailFieldLeftData d"),
    @NamedQuery(name = "DetailFieldLeftData.findByIdDetailSpecailData", query = "SELECT d FROM DetailFieldLeftData d WHERE d.idDetailSpecailData = :idDetailSpecailData"),
    @NamedQuery(name = "DetailFieldLeftData.findByDataDetial", query = "SELECT d FROM DetailFieldLeftData d WHERE d.dataDetial = :dataDetial"),
    @NamedQuery(name = "DetailFieldLeftData.findByIdDetails", query = "SELECT d FROM DetailFieldLeftData d WHERE d.idDetails = :idDetails")})
public class DetailFieldLeftData implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_detail_specail_data", nullable = false, length = 36)
    private String idDetailSpecailData;
    @Basic(optional = false)
    @Column(name = "data_detial", nullable = false, length = 255)
    private String dataDetial;
    @Basic(optional = false)
    @Column(name = "id_details", nullable = false, length = 36)
    private String idDetails;
    @JoinColumn(name = "id_detail_field_left", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private DetailFieldLeft idDetailFieldLeft;

    public DetailFieldLeftData() {
    }

    public DetailFieldLeftData(String idDetailSpecailData) {
        this.idDetailSpecailData = idDetailSpecailData;
    }

    public DetailFieldLeftData(String idDetailSpecailData, String dataDetial, String idDetails) {
        this.idDetailSpecailData = idDetailSpecailData;
        this.dataDetial = dataDetial;
        this.idDetails = idDetails;
    }

    public String getIdDetailSpecailData() {
        return idDetailSpecailData;
    }

    public void setIdDetailSpecailData(String idDetailSpecailData) {
        this.idDetailSpecailData = idDetailSpecailData;
    }

    public String getDataDetial() {
        return dataDetial;
    }

    public void setDataDetial(String dataDetial) {
        this.dataDetial = dataDetial;
    }

    public String getIdDetails() {
        return idDetails;
    }

    public void setIdDetails(String idDetails) {
        this.idDetails = idDetails;
    }

    public DetailFieldLeft getIdDetailFieldLeft() {
        return idDetailFieldLeft;
    }

    public void setIdDetailFieldLeft(DetailFieldLeft idDetailFieldLeft) {
        this.idDetailFieldLeft = idDetailFieldLeft;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDetailSpecailData != null ? idDetailSpecailData.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailFieldLeftData)) {
            return false;
        }
        DetailFieldLeftData other = (DetailFieldLeftData) object;
        if ((this.idDetailSpecailData == null && other.idDetailSpecailData != null) || (this.idDetailSpecailData != null && !this.idDetailSpecailData.equals(other.idDetailSpecailData))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.DetailFieldLeftData[ idDetailSpecailData=" + idDetailSpecailData + " ]";
    }
    
}
