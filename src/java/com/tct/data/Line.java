/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * Machine User
 *
 * @author pang Development by Chusak laojang Date : Sep 22, 2012, Time :
 * 11:13:22 AM Copy Right 4 Xtreme Co.,Ltd.
 */
@Entity
@Table(name = "line", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Line.findAll", query = "SELECT l FROM Line l"),
    @NamedQuery(name = "Line.findByLineId", query = "SELECT l FROM Line l WHERE l.lineId = :lineId"),
    @NamedQuery(name = "Line.findByLineName", query = "SELECT l FROM Line l WHERE l.lineName = :lineName"),
    @NamedQuery(name = "Line.findByDescritpion", query = "SELECT l FROM Line l WHERE l.descritpion = :descritpion")})
public class Line implements Serializable {
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "line_id", nullable = false, length = 36)
    private String lineId;
    @Column(name = "line_name", length = 255)
    private String lineName;
    @Column(name = "descritpion", length = 255)
    private String descritpion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLine")
    private Collection<Target> targetCollection;
    @Column(name = "std_time", length = 255)
    private String stdTime;
    @Column(name = "std_worker", length = 255)
    private String stdWorker;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLine")
    private Collection<ManualTarget> manualTargetCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLine")
    private Collection<NextModel> nextModelCollection;
    @Column(name = "auto_line", length = 10)
    private String autoLine;
    @Column(name = "model_manual", length = 255)
    private String modelManual;
    @Column(name = "target_defect", length = 255)
    private String targetDefect;
    @Column(name = "time_slide", length = 255)
    private String timeSlide;
    @Lob
    @Column(name = "header_comment", length = 65535)
    private String headerComment;
     @Column(name = "pic_manaul", length = 255)
    private String picManaul; 
     
    public Line() {
    }

    public Line(String lineId) {
        this.lineId = lineId;
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getLineName() {
        return lineName;
    }

    public void setLineName(String lineName) {
        this.lineName = lineName;
    }

    public String getDescritpion() {
        return descritpion;
    }

    public void setDescritpion(String descritpion) {
        this.descritpion = descritpion;
    }

    public String getStdTime() {
        return stdTime;
    }

    public void setStdTime(String stdTime) {
        this.stdTime = stdTime;
    }

    public String getStdWorker() {
        return stdWorker;
    }

    public void setStdWorker(String stdWorker) {
        this.stdWorker = stdWorker;
    }

    public String getHeaderComment() {
        return headerComment;
    }

    public void setHeaderComment(String headerComment) {
        this.headerComment = headerComment;
    }

    @XmlTransient
    public Collection<Target> getTargetCollection() {
        return targetCollection;
    }

    public void setTargetCollection(Collection<Target> targetCollection) {
        this.targetCollection = targetCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lineId != null ? lineId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Line)) {
            return false;
        }
        Line other = (Line) object;
        if ((this.lineId == null && other.lineId != null) || (this.lineId != null && !this.lineId.equals(other.lineId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.Line[ lineId=" + lineId + " ]";
    }

    @XmlTransient
    public Collection<ManualTarget> getManualTargetCollection() {
        return manualTargetCollection;
    }

    public void setManualTargetCollection(Collection<ManualTarget> manualTargetCollection) {
        this.manualTargetCollection = manualTargetCollection;
    }

    @XmlTransient
    public Collection<NextModel> getNextModelCollection() {
        return nextModelCollection;
    }

    public void setNextModelCollection(Collection<NextModel> nextModelCollection) {
        this.nextModelCollection = nextModelCollection;
    }

    public String getAutoLine() {
        return autoLine;
    }

    public void setAutoLine(String autoLine) {
        this.autoLine = autoLine;
    }

    public String getModelManual() {
        return modelManual;
    }

    public void setModelManual(String modelManual) {
        this.modelManual = modelManual;
    }

    public String getTargetDefect() {
        return targetDefect;
    }

    public void setTargetDefect(String targetDefect) {
        this.targetDefect = targetDefect;
    }

    public String getTimeSlide() {
        return timeSlide;
    }

    public void setTimeSlide(String timeSlide) {
        this.timeSlide = timeSlide;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public String getPicManaul() {
        return picManaul;
    }

    public void setPicManaul(String picManaul) {
        this.picManaul = picManaul;
    }
    
}
