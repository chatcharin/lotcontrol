/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author tawat
 */
@Entity
@Table(name = "group_field", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GroupField.findAll", query = "SELECT g FROM GroupField g WHERE g.deleted = 0"),
    @NamedQuery(name = "GroupField.findById", query = "SELECT g FROM GroupField g WHERE g.id = :id and g.deleted =0 "),
    @NamedQuery(name = "GroupField.findByDataText", query = "SELECT g FROM GroupField g WHERE g.dataText = :dataText and g.deleted = 0"),
    @NamedQuery(name = "GroupField.findByIdFieldName", query = "SELECT g FROM GroupField g WHERE g.idFieldName = :idFieldName and g.deleted = 0"),
    @NamedQuery(name = "GroupField.findByDeleted", query = "SELECT g FROM GroupField g WHERE g.deleted = :deleted")})
public class GroupField implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Basic(optional = false)
    @Column(name = "data_text", nullable = false, length = 255)
    private String dataText;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    @JoinColumn(name = "id_field_name", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private FieldName idFieldName;

    public GroupField() {
    }

    public GroupField(String id) {
        this.id = id;
    }

    public GroupField(String id, String dataText, int deleted) {
        this.id = id;
        this.dataText = dataText;
        this.deleted = deleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDataText() {
        return dataText;
    }

    public void setDataText(String dataText) {
        this.dataText = dataText;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public FieldName getIdFieldName() {
        return idFieldName;
    }

    public void setIdFieldName(FieldName idFieldName) {
        this.idFieldName = idFieldName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GroupField)) {
            return false;
        }
        GroupField other = (GroupField) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.GroupField[ id=" + id + " ]";
    }
    
}
