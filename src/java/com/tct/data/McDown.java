/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "mc_down", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "McDown.findAll", query = "SELECT m FROM McDown m"),
    @NamedQuery(name = "McDown.findById", query = "SELECT m FROM McDown m WHERE m.id = :id"),
    @NamedQuery(name = "McDown.findByReson", query = "SELECT m FROM McDown m WHERE m.reson = :reson"),
    @NamedQuery(name = "McDown.findByCurrentProcessId", query = "SELECT m FROM McDown m WHERE m.currentProcessId = :currentProcessId"),
    @NamedQuery(name = "McDown.findByRemark", query = "SELECT m FROM McDown m WHERE m.remark = :remark")})
public class McDown implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Column(name = "reson", length = 255)
    private String reson;
    @Column(name = "remark", length = 255)
    private String remark;
    @JoinColumn(name = "current_process_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private CurrentProcess currentProcessId;
    @JoinColumn(name = "mc_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Mc mcId;
    @Column(name = "ref_downTimeStart", nullable = false, length = 36)
    private String refDownTimeStart;
    
    public McDown() {
    }

    public McDown(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReson() {
        return reson;
    }

    public void setReson(String reson) {
        this.reson = reson;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public CurrentProcess getCurrentProcessId() {
        return currentProcessId;
    }

    public void setCurrentProcessId(CurrentProcess currentProcessId) {
        this.currentProcessId = currentProcessId;
    }

    public Mc getMcId() {
        return mcId;
    }

    public void setMcId(Mc mcId) {
        this.mcId = mcId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof McDown)) {
            return false;
        }
        McDown other = (McDown) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.McDown[ id=" + id + " ]";
    }

    public String getRefDownTimeStart() {
        return refDownTimeStart;
    }

    public void setRefDownTimeStart(String refDownTimeStart) {
        this.refDownTimeStart = refDownTimeStart;
    }
    
}
