/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "pcs_qty", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PcsQty.findAll", query = "SELECT p FROM PcsQty p"),
    @NamedQuery(name = "PcsQty.findById", query = "SELECT p FROM PcsQty p WHERE p.id = :id"),
    @NamedQuery(name = "PcsQty.findByQtyType", query = "SELECT p FROM PcsQty p WHERE p.qtyType = :qtyType"),
    @NamedQuery(name = "PcsQty.findByCurrentProcessId", query = "SELECT p FROM PcsQty p WHERE p.currentProcessId = :currentProcessId"),
//    @NamedQuery(name = "PcsQty.findByQtyType", query = "SELECT p FROM PcsQty p WHERE p.qtyType = :qtyType"),
    @NamedQuery(name = "PcsQty.findByQty", query = "SELECT p FROM PcsQty p WHERE p.qty = :qty")})

public class PcsQty implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 45)
    private String id;
    @Column(name = "qty_type", length = 255)
    private String qtyType;
    @Column(name = "qty", length = 255)
    private String qty;
    @JoinColumn(name = "current_process_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private CurrentProcess currentProcessId;
    @Column(name="wip_qty",length=255)
    private String wipQty;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPcsqty")
    private Collection<QaSampling> qaSamplingCollection;
    public PcsQty() {
    }

    public PcsQty(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQtyType() {
        return qtyType;
    }

    public void setQtyType(String qtyType) {
        this.qtyType = qtyType;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public CurrentProcess getCurrentProcessId() {
        return currentProcessId;
    }

    public void setCurrentProcessId(CurrentProcess currentProcessId) {
        this.currentProcessId = currentProcessId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PcsQty)) {
            return false;
        }
        PcsQty other = (PcsQty) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.PcsQty[ id=" + id + " ]";
    }

    public String getWipQty() {
        return wipQty;
    }

    public void setWipQty(String wipQty) {
        this.wipQty = wipQty;
    }
      @XmlTransient
    public Collection<QaSampling> getQaSamplingCollection() {
        return qaSamplingCollection;
    }

    public void setQaSamplingCollection(Collection<QaSampling> qaSamplingCollection) {
        this.qaSamplingCollection = qaSamplingCollection;
    }
}
