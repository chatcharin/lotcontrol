/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "process_pool", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProcessPool.findAll", query = "SELECT p FROM ProcessPool p"),
    @NamedQuery(name = "ProcessPool.findByNotdeleted",query = "SELECT p FROM ProcessPool p WHERE p.deleted = 0 ORDER BY p.procName ASC"),
    @NamedQuery(name = "ProcessPool.findByIdProc", query = "SELECT p FROM ProcessPool p WHERE p.idProc = :idProc"),
    @NamedQuery(name = "ProcessPool.findByCodeOperation", query = "SELECT p FROM ProcessPool p WHERE p.codeOperation LIKE :codeOperation AND p.deleted = 0 ORDER BY p.createDate ASC"),
    @NamedQuery(name = "ProcessPool.findByProcName", query = "SELECT p FROM ProcessPool p WHERE p.procName LIKE :procName AND p.deleted = 0 ORDER BY p.procName ASC"),
    @NamedQuery(name = "ProcessPool.findByStatus", query = "SELECT p FROM ProcessPool p WHERE p.status LIKE :status AND p.deleted = 0 ORDER BY p.createDate ASC"),
    @NamedQuery(name = "ProcessPool.findByCreateDate", query = "SELECT p FROM ProcessPool p WHERE p.createDate = :createDate"),
    @NamedQuery(name = "ProcessPool.findByUserCreate", query = "SELECT p FROM ProcessPool p WHERE p.userCreate = :userCreate"),
    @NamedQuery(name = "ProcessPool.findByUserApprove", query = "SELECT p FROM ProcessPool p WHERE p.userApprove = :userApprove"),
    @NamedQuery(name = "ProcessPool.findByCostProc", query = "SELECT p FROM ProcessPool p WHERE p.costProc = :costProc"),
    @NamedQuery(name = "ProcessPool.findByMcUse", query = "SELECT p FROM ProcessPool p WHERE (p.mcUse = :mcUse) and (p.deleted = 0)"),
    @NamedQuery(name = "ProcessPool.findByType", query = "SELECT p FROM ProcessPool p WHERE p.type LIKE :type AND p.deleted = 0 ORDER BY p.createDate ASC")})
public class ProcessPool implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_proc", nullable = false, length = 36)
    private String idProc;
    @Column(name = "code_operation", length = 100)
    private String codeOperation;
    @Column(name = "proc_name", length = 100)
    private String procName;
    @Column(name = "status", length = 100)
    private String status;
    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "user_create", nullable = false, length = 36)
    private String userCreate;
    @Column(name = "user_approve", length = 36)
    private String userApprove;
    @Column(name = "cost_proc", length = 100)
    private String costProc;
    @Basic(optional = false)
    @Column(name = "mc_use", nullable = false)
    private int mcUse;
    @Column(name = "type", length = 100)
    private String type;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProc")
    private Collection<Pc2ng> pc2ngCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProc")
    private Collection<Pc2data> pc2dataCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "processPoolIdProc")
    private Collection<Md2pc> md2pcCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProc")
    private Collection<MainData> mainDataCollection;

    public ProcessPool() {
    }

    public ProcessPool(String idProc) {
        this.idProc = idProc;
    }

    public ProcessPool(String idProc, String userCreate) {
        this.idProc = idProc;
        this.userCreate = userCreate;
    }

    public ProcessPool(String idProc, String userCreate, int mcUse) {
        this.idProc = idProc;
        this.userCreate = userCreate;
        this.mcUse = mcUse;
    }

    public String getIdProc() {
        return idProc;
    }

    public void setIdProc(String idProc) {
        this.idProc = idProc;
    }

    public String getCodeOperation() {
        return codeOperation;
    }

    public void setCodeOperation(String codeOperation) {
        this.codeOperation = codeOperation;
    }

    public String getProcName() {
        return procName;
    }

    public void setProcName(String procName) {
        this.procName = procName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserApprove() {
        return userApprove;
    }

    public void setUserApprove(String userApprove) {
        this.userApprove = userApprove;
    }

    public String getCostProc() {
        return costProc;
    }

    public void setCostProc(String costProc) {
        this.costProc = costProc;
    }

    public int getMcUse() {
        return mcUse;
    }

    public void setMcUse(int mcUse) {
        this.mcUse = mcUse;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    @XmlTransient
    public Collection<Pc2ng> getPc2ngCollection() {
        return pc2ngCollection;
    }

    public void setPc2ngCollection(Collection<Pc2ng> pc2ngCollection) {
        this.pc2ngCollection = pc2ngCollection;
    }

    @XmlTransient
    public Collection<Pc2data> getPc2dataCollection() {
        return pc2dataCollection;
    }

    public void setPc2dataCollection(Collection<Pc2data> pc2dataCollection) {
        this.pc2dataCollection = pc2dataCollection;
    }

    @XmlTransient
    public Collection<Md2pc> getMd2pcCollection() {
        return md2pcCollection;
    }

    public void setMd2pcCollection(Collection<Md2pc> md2pcCollection) {
        this.md2pcCollection = md2pcCollection;
    }

    @XmlTransient
    public Collection<MainData> getMainDataCollection() {
        return mainDataCollection;
    }

    public void setMainDataCollection(Collection<MainData> mainDataCollection) {
        this.mainDataCollection = mainDataCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProc != null ? idProc.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProcessPool)) {
            return false;
        }
        ProcessPool other = (ProcessPool) object;
        if ((this.idProc == null && other.idProc != null) || (this.idProc != null && !this.idProc.equals(other.idProc))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.ProcessPool[ idProc=" + idProc + " ]";
    }
}
