/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "detail_fl", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailFl.findAll", query = "SELECT d FROM DetailFl d WHERE d.deleted = 0"),
    @NamedQuery(name = "DetailFl.findById", query = "SELECT d FROM DetailFl d WHERE d.id = :id"),
//    @NamedQuery(name = "DetailFl.findByOrderNo", query = "SELECT d FROM DetailFl d WHERE d.orderNo LIKE :orderNo ORDER BY d.orderNo ASC"),
//    @NamedQuery(name = "DetailFl.findByQtyFrame", query = "SELECT d FROM DetailFl d WHERE d.qtyFrame = :qtyFrame"),
//    @NamedQuery(name = "DetailFl.findByQtyRoll", query = "SELECT d FROM DetailFl d WHERE d.qtyRoll = :qtyRoll"),
    @NamedQuery(name = "DetailFl.findByResinName", query = "SELECT d FROM DetailFl d WHERE d.resinName = :resinName"),
    @NamedQuery(name = "DetailFl.findByResinType", query = "SELECT d FROM DetailFl d WHERE d.resinType = :resinType"),
    @NamedQuery(name = "DetailFl.findByTypeModel", query = "SELECT d FROM DetailFl d WHERE d.typeModel LIKE :typeModel ORDER BY d.typeModel ASC"),
    @NamedQuery(name = "DetailFl.findByInvoicebrozeplat", query = "SELECT d FROM DetailFl d WHERE d.invoicebrozeplat = :invoicebrozeplat"),
    @NamedQuery(name = "DetailFl.findByTypeBb", query = "SELECT d FROM DetailFl d WHERE d.typeBb LIKE :typeBb ORDER BY d.typeBb ASC"),
    @NamedQuery(name = "DetailFl.findByPlat", query = "SELECT d FROM DetailFl d WHERE d.plat = :plat"),
    @NamedQuery(name = "DetailFl.findByDetailsIdDetails", query = "SELECT d FROM DetailFl d WHERE d.detailIdDetail = :detailIdDetail"),
    @NamedQuery(name = "DetailFl.findByProductcodeName", query = "SELECT d FROM DetailFl d WHERE d.productcodeName = :productcodeName"),
    @NamedQuery(name = "DetailFl.findByProductcode", query = "SELECT d FROM DetailFl d WHERE d.productcode = :productcode"),
    @NamedQuery(name = "DetailFl.findByDetailIdDetail", query = "SELECT d FROM DetailFl d WHERE d.detailIdDetail = :detailIdDetail")
})
public class DetailFl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Column(name = "resin_name", length = 100)
    private String resinName;
    @Column(name = "resin_type", length = 100)
    private String resinType;
    @Column(name = "type_model", length = 100)
    private String typeModel;
    @Column(name = "invoicebrozeplat", length = 100)
    private String invoicebrozeplat;
    @Column(name = "type_bb", length = 100)
    private String typeBb;
    @Column(name = "plat", length = 100)
    private String plat;
    @Column(name = "productcode_name", length = 100)
    private String productcodeName;
    @Column(name = "productcode", length = 100)
    private String productcode;
    @Column(name = "product_type", length = 255)
    private String productType;
    @JoinColumn(name = "detail_id_detail", referencedColumnName = "id_detail", nullable = false)
    @ManyToOne(optional = false)
    private Details detailIdDetail;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    @Column(name = "mold_injection", length = 255)
    private String moldInjection;
    @Column(name = "mold_stamping", length = 255)
    private String moldStamping;
    @Column(name = "bobbinname", length = 255)
    private String bobbinname;
    @Column(name = "invoice_resin", length = 255)
    private String invoiceresin;
    @Column(name = "invoice_bobbin", length = 255)
    private String invoiceBobbin;
    @Basic(optional = false)
    @Column(name = "type_f_r", nullable = false, length = 255)
    private String typeFR;

    public DetailFl() {
    }

    public DetailFl(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getResinName() {
        return resinName;
    }

    public void setResinName(String resinName) {
        this.resinName = resinName;
    }

    public String getResinType() {
        return resinType;
    }

    public void setResinType(String resinType) {
        this.resinType = resinType;
    }

    public String getTypeModel() {
        return typeModel;
    }

    public void setTypeModel(String typeModel) {
        this.typeModel = typeModel;
    }

    public String getInvoicebrozeplat() {
        return invoicebrozeplat;
    }

    public void setInvoicebrozeplat(String invoicebrozeplat) {
        this.invoicebrozeplat = invoicebrozeplat;
    }

    public String getTypeBb() {
        return typeBb;
    }

    public void setTypeBb(String typeBb) {
        this.typeBb = typeBb;
    }

    public String getPlat() {
        return plat;
    }

    public void setPlat(String plat) {
        this.plat = plat;
    }

    public String getProductcodeName() {
        return productcodeName;
    }

    public void setProductcodeName(String productcodeName) {
        this.productcodeName = productcodeName;
    }

    public String getProductcode() {
        return productcode;
    }

    public void setProductcode(String productcode) {
        this.productcode = productcode;
    }

    public Details getDetailIdDetail() {
        return detailIdDetail;
    }

    public void setDetailIdDetail(Details detailIdDetail) {
        this.detailIdDetail = detailIdDetail;
    }

    public String getMoldInjection() {
        return moldInjection;
    }

    public void setMoldInjection(String moldInjection) {
        this.moldInjection = moldInjection;
    }

    public String getMoldStamping() {
        return moldStamping;
    }

    public void setMoldStamping(String moldStamping) {
        this.moldStamping = moldStamping;
    }

    public String getBobbinname() {
        return bobbinname;
    }

    public void setBobbinname(String bobbinname) {
        this.bobbinname = bobbinname;
    }

    public String getInvoiceresin() {
        return invoiceresin;
    }

    public void setInvoiceresin(String invoiceresin) {
        this.invoiceresin = invoiceresin;
    }

    public String getInvoiceBobbin() {
        return invoiceBobbin;
    }

    public void setInvoiceBobbin(String invoiceBobbin) {
        this.invoiceBobbin = invoiceBobbin;
    }

    public String getTypeFR() {
        return typeFR;
    }

    public void setTypeFR(String typeFR) {
        this.typeFR = typeFR;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailFl)) {
            return false;
        }
        DetailFl other = (DetailFl) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.DetailFl[ id=" + id + " ]";
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }
}
