/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "countbarcodeid", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Countbarcodeid.findAll", query = "SELECT c FROM Countbarcodeid c"),
    @NamedQuery(name = "Countbarcodeid.findById", query = "SELECT c FROM Countbarcodeid c WHERE c.id = :id"),
    @NamedQuery(name = "Countbarcodeid.findByDatecreate", query = "SELECT c FROM Countbarcodeid c WHERE c.datecreate = :datecreate"),
    @NamedQuery(name = "Countbarcodeid.findByBarcodenum", query = "SELECT c FROM Countbarcodeid c WHERE c.barcodenum = :barcodenum"),
    @NamedQuery(name = "Countbarcodeid.findByMaxId",query="SELECT c FROM Countbarcodeid c WHERE c.id = (SELECT MAX(cc.id) FROM Countbarcodeid cc)")
})
public class Countbarcodeid implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id;
    @Basic(optional = false)
    @Column(name = "datecreate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date datecreate;
    @Basic(optional = false)
    @Column(name = "barcodenum", nullable = false, length = 255)
    private String barcodenum;
    @Column(name = "type_worksheet_id", nullable = false, length = 255)
    private String typeWorksheetId; 
    public Countbarcodeid() {
    }

    public Countbarcodeid(Long id) {
        this.id = id;
    }

    public Countbarcodeid(Long id, Date datecreate, String barcodenum) {
        this.id = id;
        this.datecreate = datecreate;
        this.barcodenum = barcodenum;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDatecreate() {
        return datecreate;
    }

    public void setDatecreate(Date datecreate) {
        this.datecreate = datecreate;
    }

    public String getBarcodenum() {
        return barcodenum;
    }

    public void setBarcodenum(String barcodenum) {
        this.barcodenum = barcodenum;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Countbarcodeid)) {
            return false;
        }
        Countbarcodeid other = (Countbarcodeid) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    } 
    @Override
    public String toString() {
        return "com.tct.data.Countbarcodeid[ id=" + id + " ]";
    }

    public String getTypeWorksheetId() {
        return typeWorksheetId;
    }

    public void setTypeWorksheetId(String typeWorksheetId) {
        this.typeWorksheetId = typeWorksheetId;
    } 
}
