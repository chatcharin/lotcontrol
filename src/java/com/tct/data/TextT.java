/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "text_t", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TextT.findAll", query = "SELECT t FROM TextT t"),
    @NamedQuery(name = "TextT.findByIdKdataT", query = "SELECT t FROM TextT t WHERE t.idKdataT = :idKdataT"),
    @NamedQuery(name = "TextT.findByTextLabel", query = "SELECT t FROM TextT t WHERE t.textLabel = :textLabel"),
    @NamedQuery(name = "TextT.findByCreateDate", query = "SELECT t FROM TextT t WHERE t.createDate = :createDate"),
    @NamedQuery(name = "TextT.findByUserCreate", query = "SELECT t FROM TextT t WHERE t.userCreate = :userCreate"),
    @NamedQuery(name = "TextT.findByStatusText", query = "SELECT t FROM TextT t WHERE t.statusText = :statusText")})
public class TextT implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_kdata_t", nullable = false, length = 36)
    private String idKdataT;
    @Column(name = "text_label", length = 100)
    private String textLabel;
    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "user_create", nullable = false, length = 36)
    private String userCreate;
    @Column(name = "status_text", length = 100)
    private String statusText;

    public TextT() {
    }

    public TextT(String idKdataT) {
        this.idKdataT = idKdataT;
    }

    public TextT(String idKdataT, String userCreate) {
        this.idKdataT = idKdataT;
        this.userCreate = userCreate;
    }

    public String getIdKdataT() {
        return idKdataT;
    }

    public void setIdKdataT(String idKdataT) {
        this.idKdataT = idKdataT;
    }

    public String getTextLabel() {
        return textLabel;
    }

    public void setTextLabel(String textLabel) {
        this.textLabel = textLabel;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idKdataT != null ? idKdataT.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TextT)) {
            return false;
        }
        TextT other = (TextT) object;
        if ((this.idKdataT == null && other.idKdataT != null) || (this.idKdataT != null && !this.idKdataT.equals(other.idKdataT))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.TextT[ idKdataT=" + idKdataT + " ]";
    }
    
}
