/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author tawat
 */
@Entity
@Table(name = "kpi_picture", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "KpiPicture.findAll", query = "SELECT k FROM KpiPicture k"),
    @NamedQuery(name = "KpiPicture.findByIdKpiPicture", query = "SELECT k FROM KpiPicture k WHERE k.idKpiPicture = :idKpiPicture"),
    @NamedQuery(name = "KpiPicture.findByDeleted", query = "SELECT k FROM KpiPicture k WHERE k.deleted = :deleted"),
    @NamedQuery(name = "KpiPicture.findByPictureUrl", query = "SELECT k FROM KpiPicture k WHERE k.pictureUrl = :pictureUrl"),
    @NamedQuery(name = "KpiPicture.findBySequences", query = "SELECT k FROM KpiPicture k WHERE k.sequences = :sequences"),
    @NamedQuery(name = "KpiPicture.findByShowPicture", query = "SELECT k FROM KpiPicture k WHERE k.showPicture = :showPicture"),
    @NamedQuery(name = "KpiPicture.findByTitleKpiPicture", query = "SELECT k FROM KpiPicture k WHERE k.titleKpiPicture = :titleKpiPicture")})
public class KpiPicture implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_kpi_picture", nullable = false, length = 36)
    private String idKpiPicture;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    @Basic(optional = false)
    @Column(name = "picture_url", nullable = false, length = 255)
    private String pictureUrl;
    @Basic(optional = false)
    @Column(name = "sequences", nullable = false)
    private int sequences;
    @Basic(optional = false)
    @Column(name = "show_picture", nullable = false, length = 10)
    private String showPicture;
    @Basic(optional = false)
    @Column(name = "title_kpi_picture", nullable = false, length = 255)
    private String titleKpiPicture;

    public KpiPicture() {
    }

    public KpiPicture(String idKpiPicture) {
        this.idKpiPicture = idKpiPicture;
    }

    public KpiPicture(String idKpiPicture, int deleted, String pictureUrl, int sequences, String showPicture, String titleKpiPicture) {
        this.idKpiPicture = idKpiPicture;
        this.deleted = deleted;
        this.pictureUrl = pictureUrl;
        this.sequences = sequences;
        this.showPicture = showPicture;
        this.titleKpiPicture = titleKpiPicture;
    }

    public String getIdKpiPicture() {
        return idKpiPicture;
    }

    public void setIdKpiPicture(String idKpiPicture) {
        this.idKpiPicture = idKpiPicture;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public int getSequences() {
        return sequences;
    }

    public void setSequences(int sequences) {
        this.sequences = sequences;
    }

    public String getShowPicture() {
        return showPicture;
    }

    public void setShowPicture(String showPicture) {
        this.showPicture = showPicture;
    }

    public String getTitleKpiPicture() {
        return titleKpiPicture;
    }

    public void setTitleKpiPicture(String titleKpiPicture) {
        this.titleKpiPicture = titleKpiPicture;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idKpiPicture != null ? idKpiPicture.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof KpiPicture)) {
            return false;
        }
        KpiPicture other = (KpiPicture) object;
        if ((this.idKpiPicture == null && other.idKpiPicture != null) || (this.idKpiPicture != null && !this.idKpiPicture.equals(other.idKpiPicture))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.KpiPicture[ idKpiPicture=" + idKpiPicture + " ]";
    }
    
}
