/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Tawat Saekue
 */
@Entity
@Table(name = "details", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Details.findAll", query = "SELECT d FROM Details d"),
    @NamedQuery(name = "Details.findByIdDetail", query = "SELECT d FROM Details d WHERE d.idDetail = :idDetail"),
    @NamedQuery(name = "Details.findByDetailBarcode", query = "SELECT d FROM Details d WHERE d.detailBarcode = :detailBarcode"),
    @NamedQuery(name = "Details.findByLotcontrol", query = "SELECT d FROM Details d WHERE d.lotcontrol = :lotcontrol"),
    @NamedQuery(name = "Details.findByModel", query = "SELECT d FROM Details d WHERE d.model = :model"),
    @NamedQuery(name = "Details.findByShift", query = "SELECT d FROM Details d WHERE d.shift = :shift"),
    @NamedQuery(name = "Details.findByNumOnmonth", query = "SELECT d FROM Details d WHERE d.numOnmonth = :numOnmonth"),
    @NamedQuery(name = "Details.findByUserCreate", query = "SELECT d FROM Details d WHERE d.userCreate = :userCreate"),
    @NamedQuery(name = "Details.findByUserApprove", query = "SELECT d FROM Details d WHERE d.userApprove = :userApprove"),
    @NamedQuery(name = "Details.findByDateCreate", query = "SELECT d FROM Details d WHERE d.dateCreate = :dateCreate"),
    @NamedQuery(name = "Details.findByDetailType", query = "SELECT d FROM Details d WHERE d.detailType = :detailType"),
    @NamedQuery(name = "Details.findByDetailName", query = "SELECT d FROM Details d WHERE d.detailName = :detailName AND d.deleted = 0"),
    @NamedQuery(name = "Details.findByModelAndDetailNameAssy", query = "SELECT d FROM Details d WHERE d.model LIKE :model AND d.detailName = :detailName"),
    @NamedQuery(name = "Details.findByPdCodeName", query = "SELECT d FROM Details d WHERE d.pdCodeName = :pdCodeName"),
    @NamedQuery(name = "Details.findByPdCodeType", query = "SELECT d FROM Details d WHERE d.pdCodeType = :pdCodeType"),
    @NamedQuery(name = "Details.findByPlant", query = "SELECT d FROM Details d WHERE d.plant = :plant"),
    @NamedQuery(name = "Details.findByOrderType", query = "SELECT d FROM Details d WHERE d.orderType = :orderType"),
    @NamedQuery(name = "Details.findByInjectionDate", query = "SELECT d FROM Details d WHERE d.injectionDate = :injectionDate"),
    @NamedQuery(name = "Details.findByPlatingDate", query = "SELECT d FROM Details d WHERE d.platingDate = :platingDate"),
    @NamedQuery(name = "Details.findByInvBroze", query = "SELECT d FROM Details d WHERE d.invBroze = :invBroze"),
    @NamedQuery(name = "Details.findByInvBobin", query = "SELECT d FROM Details d WHERE d.invBobin = :invBobin")
})
public class Details implements Serializable {

    @Column(name = "date_create")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_detail", nullable = false, length = 36)
    private String idDetail;
    @Column(name = "detail_barcode", length = 255)
    private String detailBarcode;
    @Column(name = "lotcontrol", length = 255)
    private String lotcontrol;
    @Column(name = "model", length = 255)
    private String model;
    @Column(name = "shift", length = 100)
    private String shift;
    @Column(name = "num_on_month", length = 100)
    private int numOnmonth;
    @Column(name = "user_create", length = 36)
    private String userCreate;
    @Column(name = "user_approve", length = 36)
    private String userApprove;
    @Column(name = "detail_type", length = 255)
    private String detailType;
    @Column(name = "detail_name", length = 255)
    private String detailName;
    @Column(name = "line", length = 255)
    private String line;
    @Column(name = "model_series", length = 255)
    private String modelseries;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    @Column(name = "order_number", length = 255)
    private String orderNumber;
    @Basic(optional = false)
    @Column(name = "pd_qty", nullable = false, length = 255)
    private String pdQty;
    @Basic(optional = false)
    @Column(name = "pd_qty_by_lot", nullable = false, length = 255)
    private String pdQtyByLot;
    @Lob
    @Column(name = "remark", length = 65535)
    private String remark;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detailIdDetail")
    private Collection<Pcb01> pcb01Collection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detailIdDetail")
    private Collection<LineAssembly> lineAssemblyCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detailIdDetail")
    private Collection<DetailAssy> detailAssyCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detailIdDetail")
    private Collection<DetailFl> detailFlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detailIdDetail")
    private Collection<DetailAuto> detailAutoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detailIdDetail")
    private Collection<Refu01> refu01Collection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detailsIdDetail")
    private Collection<LotControl> lotControlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detailsIdDetail")
    private Collection<DetailPacking> detailPackingsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detailsIdDetail")
    private Collection<Back2pack> back2packCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detailsIdDetail")
    private Collection<Front2back> front2backCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detailId")
    private Collection<Assy2auto> assy2autoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detailsIdDetail")
    private Collection<CopperWireInvoice> copperWireInvoiceCollection;
 
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detailsId")
    private Collection<Front2backMaster> front2backMasterCollection;
 
    @Column(name = "pd_code_name", length = 255)
    private String pdCodeName;
    @Column(name = "pd_code_type", length = 255)
    private String pdCodeType;
    @Column(name = "plant", length = 255)
    private String plant;
    @Column(name = "order_type", length = 255)
    private String orderType;
    @Basic(optional = false)
    @Column(name = "injection_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date injectionDate;
    @Basic(optional = false)
    @Column(name = "plating_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date platingDate;
    @Column(name = "inv_broze", length = 255)
    private String invBroze;
    @Column(name = "inv_bobin", length = 255)
    private String invBobin;
    @Column(name = "approve_status", nullable = false)
    private int approveStatus;
    @Column(name = "assy_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date assyDate;
    @Column(name = "pd_code", length = 255)
    private String pdCode;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detailsIdDetail")
    private Collection<DetailPacking> detailPackingCollection;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detailsId")
//    private Collection<Front2backMaster> front2backMasterCollection;
 

    public Details() {
    }

    public Details(String idDetail) {
        this.idDetail = idDetail;
    }

    public String getIdDetail() {
        return idDetail;
    }

    public void setIdDetail(String idDetail) {
        this.idDetail = idDetail;
    }

    public String getDetailBarcode() {
        return detailBarcode;
    }

    public void setDetailBarcode(String detailBarcode) {
        this.detailBarcode = detailBarcode;
    }

    public String getLotcontrol() {
        return lotcontrol;
    }

    public void setLotcontrol(String lotcontrol) {
        this.lotcontrol = lotcontrol;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public int getNumOnmonth() {
        return numOnmonth;
    }

    public void setNumOnmonth(int numOnmonth) {
        this.numOnmonth = numOnmonth;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserApprove() {
        return userApprove;
    }

    public void setUserApprove(String userApprove) {
        this.userApprove = userApprove;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getDetailType() {
        return detailType;
    }

    public void setDetailType(String detailType) {
        this.detailType = detailType;
    }

    public String getDetailName() {
        return detailName;
    }

    public void setDetailName(String detailName) {
        this.detailName = detailName;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getPdQty() {
        return pdQty;
    }

    public void setPdQty(String pdQty) {
        this.pdQty = pdQty;
    }

    public String getPdQtyByLot() {
        return pdQtyByLot;
    }

    public void setPdQtyByLot(String pdQtyByLot) {
        this.pdQtyByLot = pdQtyByLot;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @XmlTransient
    public Collection<Pcb01> getPcb01Collection() {
        return pcb01Collection;
    }

    public void setPcb01Collection(Collection<Pcb01> pcb01Collection) {
        this.pcb01Collection = pcb01Collection;
    }

    @XmlTransient
    public Collection<LineAssembly> getLineAssemblyCollection() {
        return lineAssemblyCollection;
    }

    public void setLineAssemblyCollection(Collection<LineAssembly> lineAssemblyCollection) {
        this.lineAssemblyCollection = lineAssemblyCollection;
    }

    @XmlTransient
    public Collection<DetailAssy> getDetailAssyCollection() {
        return detailAssyCollection;
    }

    public void setDetailAssyCollection(Collection<DetailAssy> detailAssyCollection) {
        this.detailAssyCollection = detailAssyCollection;
    }

    @XmlTransient
    public Collection<DetailFl> getDetailFlCollection() {
        return detailFlCollection;
    }

    public void setDetailFlCollection(Collection<DetailFl> detailFlCollection) {
        this.detailFlCollection = detailFlCollection;
    }

    @XmlTransient
    public Collection<DetailAuto> getDetailAutoCollection() {
        return detailAutoCollection;
    }

    public void setDetailAutoCollection(Collection<DetailAuto> detailAutoCollection) {
        this.detailAutoCollection = detailAutoCollection;
    }

    @XmlTransient
    public Collection<Refu01> getRefu01Collection() {
        return refu01Collection;
    }

    public void setRefu01Collection(Collection<Refu01> refu01Collection) {
        this.refu01Collection = refu01Collection;
    }

    @XmlTransient
    public Collection<LotControl> getLotControlCollection() {
        return lotControlCollection;
    }

    public void setLotControlCollection(Collection<LotControl> lotControlCollection) {
        this.lotControlCollection = lotControlCollection;
    }

    @XmlTransient
    public Collection<DetailPacking> getDetailPackingsCollection() {
        return detailPackingsCollection;
    }

    public void setDetailPackingsCollection(Collection<DetailPacking> detailPackingsCollection) {
        this.detailPackingsCollection = detailPackingsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDetail != null ? idDetail.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Details)) {
            return false;
        }
        Details other = (Details) object;
        if ((this.idDetail == null && other.idDetail != null) || (this.idDetail != null && !this.idDetail.equals(other.idDetail))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.Details[ idDetail=" + idDetail + " ]";
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    @XmlTransient
    public Collection<Back2pack> getBack2packCollection() {
        return back2packCollection;
    }

    public void setBack2packCollection(Collection<Back2pack> back2packCollection) {
        this.back2packCollection = back2packCollection;
    }

    @XmlTransient
    public Collection<Front2back> getFront2backCollection() {
        return front2backCollection;
    }

    @XmlTransient
    public Collection<Front2backMaster> getFront2backMasterCollection() {
        return front2backMasterCollection;
    }

    public void setFront2backMasterCollection(Collection<Front2backMaster> front2backMasterCollection) {
        this.front2backMasterCollection = front2backMasterCollection;
    }

    public void setFront2backCollection(Collection<Front2back> front2backCollection) {
        this.front2backCollection = front2backCollection;
    }

    public String getModelseries() {
        return modelseries;
    }

    public void setModelseries(String modelseries) {
        this.modelseries = modelseries;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    @XmlTransient
    public Collection<Assy2auto> getAssy2autoCollection() {
        return assy2autoCollection;
    }

    public void setAssy2autoCollection(Collection<Assy2auto> assy2autoCollection) {
        this.assy2autoCollection = assy2autoCollection;
    }

    @XmlTransient
    public Collection<CopperWireInvoice> getCopperWireInvoiceCollection() {
        return copperWireInvoiceCollection;
    }

    public void setCopperWireInvoiceCollection(Collection<CopperWireInvoice> copperWireInvoiceCollection) {
        this.copperWireInvoiceCollection = copperWireInvoiceCollection;
    }

    public Date getInjectionDate() {
        return injectionDate;
    }

    public void setInjectionDate(Date injectionDate) {
        this.injectionDate = injectionDate;
    }

    public String getInvBobin() {
        return invBobin;
    }

    public void setInvBobin(String invBobin) {
        this.invBobin = invBobin;
    }

    public String getInvBroze() {
        return invBroze;
    }

    public void setInvBroze(String invBroze) {
        this.invBroze = invBroze;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getPdCodeName() {
        return pdCodeName;
    }

    public void setPdCodeName(String pdCodeName) {
        this.pdCodeName = pdCodeName;
    }

    public String getPdCodeType() {
        return pdCodeType;
    }

    public void setPdCodeType(String pdCodeType) {
        this.pdCodeType = pdCodeType;
    }

    public String getPlant() {
        return plant;
    }

    public void setPlant(String plant) {
        this.plant = plant;
    }

    public Date getPlatingDate() {
        return platingDate;
    }

    public void setPlatingDate(Date platingDate) {
        this.platingDate = platingDate;
    }

    public int getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(int approveStatus) {
        this.approveStatus = approveStatus;
    }

    public Date getAssyDate() {
        return assyDate;
    }

    public void setAssyDate(Date assyDate) {
        this.assyDate = assyDate;
    }

    public String getPdCode() {
        return pdCode;
    }

    public void setPdCode(String pdCode) {
        this.pdCode = pdCode;
    }
    
    @XmlTransient
    public Collection<DetailPacking> getDetailPackingCollection() {
        return detailPackingCollection;
    }

    public void setDetailPackingCollection(Collection<DetailPacking> detailPackingCollection) {
        this.detailPackingCollection = detailPackingCollection;
    }
}
