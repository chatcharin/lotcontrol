/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "mc_move", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "McMove.findAll", query = "SELECT m FROM McMove m"),
    @NamedQuery(name = "McMove.findById", query = "SELECT m FROM McMove m WHERE m.id = :id")})
public class McMove implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @JoinColumn(name = "befor_mc", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Mc beforMc;
    @JoinColumn(name = "current_process_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private CurrentProcess currentProcessId;
    @JoinColumn(name = "aftor_mc", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Mc aftorMc;
    @Column(name = "ref_moveIdCurrent", nullable = false, length = 36)
    private String refMoveIdCurrent;
    
    

    public McMove() {
    }

    public McMove(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Mc getBeforMc() {
        return beforMc;
    }

    public void setBeforMc(Mc beforMc) {
        this.beforMc = beforMc;
    }

    public CurrentProcess getCurrentProcessId() {
        return currentProcessId;
    }

    public void setCurrentProcessId(CurrentProcess currentProcessId) {
        this.currentProcessId = currentProcessId;
    }

    public Mc getAftorMc() {
        return aftorMc;
    }

    public void setAftorMc(Mc aftorMc) {
        this.aftorMc = aftorMc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof McMove)) {
            return false;
        }
        McMove other = (McMove) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.McMove[ id=" + id + " ]";
    }

    public String getRefMoveIdCurrent() {
        return refMoveIdCurrent;
    }

    public void setRefMoveIdCurrent(String refMoveIdCurrent) {
        this.refMoveIdCurrent = refMoveIdCurrent;
    }
    
}
