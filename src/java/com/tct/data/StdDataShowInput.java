/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Nov 2, 2012, Time : 1:45:07 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
@Entity
@Table(name = "std_data_show_input", catalog = "tct_project", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id_std_show_data_input"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StdDataShowInput.findAll", query = "SELECT s FROM StdDataShowInput s"),
    @NamedQuery(name = "StdDataShowInput.findByIdStdShowDataInput", query = "SELECT s FROM StdDataShowInput s WHERE s.idStdShowDataInput = :idStdShowDataInput"),
    @NamedQuery(name = "StdDataShowInput.findByValue", query = "SELECT s FROM StdDataShowInput s WHERE s.value = :value")})
public class StdDataShowInput implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_std_show_data_input", nullable = false, length = 36)
    private String idStdShowDataInput;
    @Basic(optional = false)
    @Column(name = "value", nullable = false, length = 255)
    private String value;
    @JoinColumn(name = "id_show", referencedColumnName = "id_std_show", nullable = false)
    @ManyToOne(optional = false)
    private StdDataShow idShow;
    @JoinColumn(name = "id_setting_name", referencedColumnName = "id_std_setting", nullable = false)
    @ManyToOne(optional = false)
    private StdTimeSetting idSettingName;

    public StdDataShowInput() {
    }

    public StdDataShowInput(String idStdShowDataInput) {
        this.idStdShowDataInput = idStdShowDataInput;
    }

    public StdDataShowInput(String idStdShowDataInput, String value) {
        this.idStdShowDataInput = idStdShowDataInput;
        this.value = value;
    }

    public String getIdStdShowDataInput() {
        return idStdShowDataInput;
    }

    public void setIdStdShowDataInput(String idStdShowDataInput) {
        this.idStdShowDataInput = idStdShowDataInput;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public StdDataShow getIdShow() {
        return idShow;
    }

    public void setIdShow(StdDataShow idShow) {
        this.idShow = idShow;
    }

    public StdTimeSetting getIdSettingName() {
        return idSettingName;
    }

    public void setIdSettingName(StdTimeSetting idSettingName) {
        this.idSettingName = idSettingName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStdShowDataInput != null ? idStdShowDataInput.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StdDataShowInput)) {
            return false;
        }
        StdDataShowInput other = (StdDataShowInput) object;
        if ((this.idStdShowDataInput == null && other.idStdShowDataInput != null) || (this.idStdShowDataInput != null && !this.idStdShowDataInput.equals(other.idStdShowDataInput))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.StdDataShowInput[ idStdShowDataInput=" + idStdShowDataInput + " ]";
    }

}
