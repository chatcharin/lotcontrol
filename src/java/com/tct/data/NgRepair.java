/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "ng_repair", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NgRepair.findAll", query = "SELECT n FROM NgRepair n"),
    @NamedQuery(name = "NgRepair.findByIdNg", query = "SELECT n FROM NgRepair n WHERE n.idNg = :idNg"),
    @NamedQuery(name = "NgRepair.findByName", query = "SELECT n FROM NgRepair n WHERE n.name = :name"),
    @NamedQuery(name = "NgRepair.findByCurrentProcessId", query = "SELECT n FROM NgRepair n WHERE n.currentProcessId = :currentProcessId"),
    @NamedQuery(name = "NgRepair.findByQty", query = "SELECT n FROM NgRepair n WHERE n.qty = :qty")})
public class NgRepair implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_ng", nullable = false, length = 36)
    private String idNg;
    @Column(name = "name", length = 255)
    private String name;
    @Column(name = "qty", length = 255)
    private String qty;
    @JoinColumn(name = "current_process_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private CurrentProcess currentProcessId;

    public NgRepair() {
    }

    public NgRepair(String idNg) {
        this.idNg = idNg;
    }

    public String getIdNg() {
        return idNg;
    }

    public void setIdNg(String idNg) {
        this.idNg = idNg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public CurrentProcess getCurrentProcessId() {
        return currentProcessId;
    }

    public void setCurrentProcessId(CurrentProcess currentProcessId) {
        this.currentProcessId = currentProcessId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNg != null ? idNg.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NgRepair)) {
            return false;
        }
        NgRepair other = (NgRepair) object;
        if ((this.idNg == null && other.idNg != null) || (this.idNg != null && !this.idNg.equals(other.idNg))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.NgRepair[ idNg=" + idNg + " ]";
    }
    
}
