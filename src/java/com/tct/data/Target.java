/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 25, 2012, Time : 1:07:41 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
@Entity
@Table(name = "target", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Target.findAll", query = "SELECT t FROM Target t"),
    @NamedQuery(name = "Target.findById", query = "SELECT t FROM Target t WHERE t.id = :id"),
    @NamedQuery(name = "Target.findByStart", query = "SELECT t FROM Target t WHERE t.start = :start"),
    @NamedQuery(name = "Target.findByEnd", query = "SELECT t FROM Target t WHERE t.end = :end"),
    @NamedQuery(name = "Target.findByTargetQty", query = "SELECT t FROM Target t WHERE t.targetQty = :targetQty"),
    @NamedQuery(name = "Target.findByDateCreate", query = "SELECT t FROM Target t WHERE t.dateCreate = :dateCreate"),
    @NamedQuery(name = "Target.findByDeleted", query = "SELECT t FROM Target t WHERE t.deleted = :deleted")})
public class Target implements Serializable {
    @Basic(optional = false)
    @Column(name = "date_create", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Basic(optional = false)
    @Column(name = "start", nullable = false, length = 10)
    private String start;
    @Basic(optional = false)
    @Column(name = "end", nullable = false, length = 10)
    private String end;
    @Basic(optional = false)
    @Column(name = "target_qty", nullable = false, length = 100)
    private String targetQty;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    @JoinColumn(name = "id_line", referencedColumnName = "line_id", nullable = false)
    @ManyToOne(optional = false)
    private Line idLine;
    @JoinColumn(name = "id_model_pool", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private ModelPool idModelPool;

    public Target() {
    }

    public Target(String id) {
        this.id = id;
    }

    public Target(String id, String start, String end, String targetQty, Date dateCreate, int deleted) {
        this.id = id;
        this.start = start;
        this.end = end;
        this.targetQty = targetQty;
        this.dateCreate = dateCreate;
        this.deleted = deleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getTargetQty() {
        return targetQty;
    }

    public void setTargetQty(String targetQty) {
        this.targetQty = targetQty;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public Line getIdLine() {
        return idLine;
    }

    public void setIdLine(Line idLine) {
        this.idLine = idLine;
    }

    public ModelPool getIdModelPool() {
        return idModelPool;
    }

    public void setIdModelPool(ModelPool idModelPool) {
        this.idModelPool = idModelPool;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Target)) {
            return false;
        }
        Target other = (Target) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.Target[ id=" + id + " ]";
    }

}
