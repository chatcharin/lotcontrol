/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "pc2data", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pc2data.findAll", query = "SELECT p FROM Pc2data p"),
    @NamedQuery(name = "Pc2data.findByPc2dataId", query = "SELECT p FROM Pc2data p WHERE p.pc2dataId = :pc2dataId"),
    @NamedQuery(name = "Pc2data.findByCreateDate", query = "SELECT p FROM Pc2data p WHERE p.createDate = :createDate"),
    @NamedQuery(name = "Pc2data.findByUserCreate", query = "SELECT p FROM Pc2data p WHERE p.userCreate = :userCreate"),
    @NamedQuery(name = "Pc2data.findByUserApprove", query = "SELECT p FROM Pc2data p WHERE p.userApprove = :userApprove"),
    @NamedQuery(name = "Pc2data.findByIdKdataO", query = "SELECT p FROM Pc2data p WHERE p.idKdataO = :idKdataO"),
    @NamedQuery(name = "Pc2data.findByIdKdataT", query = "SELECT p FROM Pc2data p WHERE p.idKdataT = :idKdataT")})
public class Pc2data implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "pc2data_id", nullable = false, length = 36)
    private String pc2dataId;
    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "user_create", nullable = false, length = 36)
    private String userCreate;
    @Column(name = "user_approve", length = 36)
    private String userApprove;
    @Column(name = "id_kdata_o", length = 36)
    private String idKdataO;
    @Column(name = "id_kdata_t", length = 36)
    private String idKdataT;
    @JoinColumn(name = "id_proc", referencedColumnName = "id_proc", nullable = false)
    @ManyToOne(optional = false)
    private ProcessPool idProc;

    public Pc2data() {
    }

    public Pc2data(String pc2dataId) {
        this.pc2dataId = pc2dataId;
    }

    public Pc2data(String pc2dataId, String userCreate) {
        this.pc2dataId = pc2dataId;
        this.userCreate = userCreate;
    }

    public String getPc2dataId() {
        return pc2dataId;
    }

    public void setPc2dataId(String pc2dataId) {
        this.pc2dataId = pc2dataId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserApprove() {
        return userApprove;
    }

    public void setUserApprove(String userApprove) {
        this.userApprove = userApprove;
    }

    public String getIdKdataO() {
        return idKdataO;
    }

    public void setIdKdataO(String idKdataO) {
        this.idKdataO = idKdataO;
    }

    public String getIdKdataT() {
        return idKdataT;
    }

    public void setIdKdataT(String idKdataT) {
        this.idKdataT = idKdataT;
    }

    public ProcessPool getIdProc() {
        return idProc;
    }

    public void setIdProc(ProcessPool idProc) {
        this.idProc = idProc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pc2dataId != null ? pc2dataId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pc2data)) {
            return false;
        }
        Pc2data other = (Pc2data) object;
        if ((this.pc2dataId == null && other.pc2dataId != null) || (this.pc2dataId != null && !this.pc2dataId.equals(other.pc2dataId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.Pc2data[ pc2dataId=" + pc2dataId + " ]";
    }
    
}
