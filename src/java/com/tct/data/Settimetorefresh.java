/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author TheBoyCs
 */
@Entity
@Table(name = "settimetorefresh", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Settimetorefresh.findAll", query = "SELECT s FROM Settimetorefresh s"),
    @NamedQuery(name = "Settimetorefresh.findById", query = "SELECT s FROM Settimetorefresh s WHERE s.id = :id"),
    @NamedQuery(name = "Settimetorefresh.findBySlideAuto", query = "SELECT s FROM Settimetorefresh s WHERE s.slideAuto = :slideAuto"),
    @NamedQuery(name = "Settimetorefresh.findBySlideManual", query = "SELECT s FROM Settimetorefresh s WHERE s.slideManual = :slideManual"),
    @NamedQuery(name = "Settimetorefresh.findBySlideLine", query = "SELECT s FROM Settimetorefresh s WHERE s.slideLine = :slideLine"),
    @NamedQuery(name = "Settimetorefresh.findByRefreshAuto", query = "SELECT s FROM Settimetorefresh s WHERE s.refreshAuto = :refreshAuto"),
    @NamedQuery(name = "Settimetorefresh.findByRefreshManual", query = "SELECT s FROM Settimetorefresh s WHERE s.refreshManual = :refreshManual"),
    @NamedQuery(name = "Settimetorefresh.findByRefreshLine", query = "SELECT s FROM Settimetorefresh s WHERE s.refreshLine = :refreshLine"),
    @NamedQuery(name = "Settimetorefresh.findByDateCreate", query = "SELECT s FROM Settimetorefresh s WHERE s.dateCreate = :dateCreate"),
    @NamedQuery(name = "Settimetorefresh.findByUserCreate", query = "SELECT s FROM Settimetorefresh s WHERE s.userCreate = :userCreate")})
public class Settimetorefresh implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Column(name = "slide_auto")
    private Integer slideAuto;
    @Column(name = "slide_manual")
    private Integer slideManual;
    @Column(name = "slide_line")
    private Integer slideLine;
    @Column(name = "refresh_auto")
    private Integer refreshAuto;
    @Column(name = "refresh_manual")
    private Integer refreshManual;
    @Column(name = "refresh_line")
    private Integer refreshLine;
    @Column(name = "date_create")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    @Column(name = "user_create", length = 36)
    private String userCreate;

    public Settimetorefresh() {
    }

    public Settimetorefresh(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getSlideAuto() {
        return slideAuto;
    }

    public void setSlideAuto(Integer slideAuto) {
        this.slideAuto = slideAuto;
    }

    public Integer getSlideManual() {
        return slideManual;
    }

    public void setSlideManual(Integer slideManual) {
        this.slideManual = slideManual;
    }

    public Integer getSlideLine() {
        return slideLine;
    }

    public void setSlideLine(Integer slideLine) {
        this.slideLine = slideLine;
    }

    public Integer getRefreshAuto() {
        return refreshAuto;
    }

    public void setRefreshAuto(Integer refreshAuto) {
        this.refreshAuto = refreshAuto;
    }

    public Integer getRefreshManual() {
        return refreshManual;
    }

    public void setRefreshManual(Integer refreshManual) {
        this.refreshManual = refreshManual;
    }

    public Integer getRefreshLine() {
        return refreshLine;
    }

    public void setRefreshLine(Integer refreshLine) {
        this.refreshLine = refreshLine;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Settimetorefresh)) {
            return false;
        }
        Settimetorefresh other = (Settimetorefresh) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.Settimetorefresh[ id=" + id + " ]";
    }
    
}
