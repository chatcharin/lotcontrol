/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.ReportSetting;
import com.tct.data.ReportSettingAttribute;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Oct 18, 2012, Time : 6:53:03 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class ReportSettingAttributeJpaController implements Serializable {

    public ReportSettingAttributeJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ReportSettingAttribute reportSettingAttribute) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ReportSetting idReportSetting = reportSettingAttribute.getIdReportSetting();
            if (idReportSetting != null) {
                idReportSetting = em.getReference(idReportSetting.getClass(), idReportSetting.getIdReportSetting());
                reportSettingAttribute.setIdReportSetting(idReportSetting);
            }
            em.persist(reportSettingAttribute);
            if (idReportSetting != null) {
                idReportSetting.getReportSettingAttributeCollection().add(reportSettingAttribute);
                idReportSetting = em.merge(idReportSetting);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findReportSettingAttribute(reportSettingAttribute.getIdReportSettingAtt()) != null) {
                throw new PreexistingEntityException("ReportSettingAttribute " + reportSettingAttribute + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ReportSettingAttribute reportSettingAttribute) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ReportSettingAttribute persistentReportSettingAttribute = em.find(ReportSettingAttribute.class, reportSettingAttribute.getIdReportSettingAtt());
            ReportSetting idReportSettingOld = persistentReportSettingAttribute.getIdReportSetting();
            ReportSetting idReportSettingNew = reportSettingAttribute.getIdReportSetting();
            if (idReportSettingNew != null) {
                idReportSettingNew = em.getReference(idReportSettingNew.getClass(), idReportSettingNew.getIdReportSetting());
                reportSettingAttribute.setIdReportSetting(idReportSettingNew);
            }
            reportSettingAttribute = em.merge(reportSettingAttribute);
            if (idReportSettingOld != null && !idReportSettingOld.equals(idReportSettingNew)) {
                idReportSettingOld.getReportSettingAttributeCollection().remove(reportSettingAttribute);
                idReportSettingOld = em.merge(idReportSettingOld);
            }
            if (idReportSettingNew != null && !idReportSettingNew.equals(idReportSettingOld)) {
                idReportSettingNew.getReportSettingAttributeCollection().add(reportSettingAttribute);
                idReportSettingNew = em.merge(idReportSettingNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = reportSettingAttribute.getIdReportSettingAtt();
                if (findReportSettingAttribute(id) == null) {
                    throw new NonexistentEntityException("The reportSettingAttribute with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ReportSettingAttribute reportSettingAttribute;
            try {
                reportSettingAttribute = em.getReference(ReportSettingAttribute.class, id);
                reportSettingAttribute.getIdReportSettingAtt();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The reportSettingAttribute with id " + id + " no longer exists.", enfe);
            }
            ReportSetting idReportSetting = reportSettingAttribute.getIdReportSetting();
            if (idReportSetting != null) {
                idReportSetting.getReportSettingAttributeCollection().remove(reportSettingAttribute);
                idReportSetting = em.merge(idReportSetting);
            }
            em.remove(reportSettingAttribute);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ReportSettingAttribute> findReportSettingAttributeEntities() {
        return findReportSettingAttributeEntities(true, -1, -1);
    }

    public List<ReportSettingAttribute> findReportSettingAttributeEntities(int maxResults, int firstResult) {
        return findReportSettingAttributeEntities(false, maxResults, firstResult);
    }

    private List<ReportSettingAttribute> findReportSettingAttributeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ReportSettingAttribute.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ReportSettingAttribute findReportSettingAttribute(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ReportSettingAttribute.class, id);
        } 
        catch(Exception e)
        {
            return null;
        }
        finally {
            em.close();
        }
    }

    public int getReportSettingAttributeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ReportSettingAttribute> rt = cq.from(ReportSettingAttribute.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
//    find id report setting
    public List<ReportSettingAttribute>  findReportSettingAttributeNotDelete(ReportSetting idReportSetting)
    {
        EntityManager  em  = getEntityManager();
        try
        {
            Query q  = em.createQuery("SELECT r FROM ReportSettingAttribute r WHERE r.idReportSetting = :idReportSetting").setParameter("idReportSetting", idReportSetting);
            return  q.getResultList();
        }
        finally
        {
            em.close();
        }
    }
    //find by id reportsettin
    public List<ReportSettingAttribute> findByIdReportSetting(ReportSetting idReportSetting)
    {
        EntityManager em = getEntityManager();
        try
        {
            Query q  =  em.createQuery("SELECT r FROM ReportSettingAttribute r  WHERE  r.idReportSetting =:idReportSetting and r.deleted = 0");
            q.setParameter("idReportSetting",idReportSetting);
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }
}
