/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import com.tct.data.Role;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.RoleUser;
import com.tct.data.jpa.exceptions.IllegalOrphanException;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author The_Boy_Cs
 */
public class RoleJpaController implements Serializable {

    public RoleJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Role role) throws PreexistingEntityException, Exception {
        if (role.getRoleUserCollection() == null) {
            role.setRoleUserCollection(new ArrayList<RoleUser>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<RoleUser> attachedRoleUserCollection = new ArrayList<RoleUser>();
            for (RoleUser roleUserCollectionRoleUserToAttach : role.getRoleUserCollection()) {
                roleUserCollectionRoleUserToAttach = em.getReference(roleUserCollectionRoleUserToAttach.getClass(), roleUserCollectionRoleUserToAttach.getId());
                attachedRoleUserCollection.add(roleUserCollectionRoleUserToAttach);
            }
            role.setRoleUserCollection(attachedRoleUserCollection);
            em.persist(role);
            for (RoleUser roleUserCollectionRoleUser : role.getRoleUserCollection()) {
                Role oldRoleIdOfRoleUserCollectionRoleUser = roleUserCollectionRoleUser.getRoleId();
                roleUserCollectionRoleUser.setRoleId(role);
                roleUserCollectionRoleUser = em.merge(roleUserCollectionRoleUser);
                if (oldRoleIdOfRoleUserCollectionRoleUser != null) {
                    oldRoleIdOfRoleUserCollectionRoleUser.getRoleUserCollection().remove(roleUserCollectionRoleUser);
                    oldRoleIdOfRoleUserCollectionRoleUser = em.merge(oldRoleIdOfRoleUserCollectionRoleUser);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findRole(role.getIdRole()) != null) {
                throw new PreexistingEntityException("Role " + role + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Role role) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Role persistentRole = em.find(Role.class, role.getIdRole());
            Collection<RoleUser> roleUserCollectionOld = persistentRole.getRoleUserCollection();
            Collection<RoleUser> roleUserCollectionNew = role.getRoleUserCollection();
            List<String> illegalOrphanMessages = null;
            for (RoleUser roleUserCollectionOldRoleUser : roleUserCollectionOld) {
                if (!roleUserCollectionNew.contains(roleUserCollectionOldRoleUser)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain RoleUser " + roleUserCollectionOldRoleUser + " since its roleId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<RoleUser> attachedRoleUserCollectionNew = new ArrayList<RoleUser>();
            for (RoleUser roleUserCollectionNewRoleUserToAttach : roleUserCollectionNew) {
                roleUserCollectionNewRoleUserToAttach = em.getReference(roleUserCollectionNewRoleUserToAttach.getClass(), roleUserCollectionNewRoleUserToAttach.getId());
                attachedRoleUserCollectionNew.add(roleUserCollectionNewRoleUserToAttach);
            }
            roleUserCollectionNew = attachedRoleUserCollectionNew;
            role.setRoleUserCollection(roleUserCollectionNew);
            role = em.merge(role);
            for (RoleUser roleUserCollectionNewRoleUser : roleUserCollectionNew) {
                if (!roleUserCollectionOld.contains(roleUserCollectionNewRoleUser)) {
                    Role oldRoleIdOfRoleUserCollectionNewRoleUser = roleUserCollectionNewRoleUser.getRoleId();
                    roleUserCollectionNewRoleUser.setRoleId(role);
                    roleUserCollectionNewRoleUser = em.merge(roleUserCollectionNewRoleUser);
                    if (oldRoleIdOfRoleUserCollectionNewRoleUser != null && !oldRoleIdOfRoleUserCollectionNewRoleUser.equals(role)) {
                        oldRoleIdOfRoleUserCollectionNewRoleUser.getRoleUserCollection().remove(roleUserCollectionNewRoleUser);
                        oldRoleIdOfRoleUserCollectionNewRoleUser = em.merge(oldRoleIdOfRoleUserCollectionNewRoleUser);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = role.getIdRole();
                if (findRole(id) == null) {
                    throw new NonexistentEntityException("The role with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Role role;
            try {
                role = em.getReference(Role.class, id);
                role.getIdRole();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The role with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<RoleUser> roleUserCollectionOrphanCheck = role.getRoleUserCollection();
            for (RoleUser roleUserCollectionOrphanCheckRoleUser : roleUserCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Role (" + role + ") cannot be destroyed since the RoleUser " + roleUserCollectionOrphanCheckRoleUser + " in its roleUserCollection field has a non-nullable roleId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(role);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Role> findRoleEntities() {
        return findRoleEntities(true, -1, -1);
    }

    public List<Role> findRoleEntities(int maxResults, int firstResult) {
        return findRoleEntities(false, maxResults, firstResult);
    }

    private List<Role> findRoleEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Role.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Role findRole(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Role.class, id);
        } finally {
            em.close();
        }
    }

    public int getRoleCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Role> rt = cq.from(Role.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    //@pang
     public  List<Role> findRoleNameAllNotDelete() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("Role.findAllRolesNotDelete");
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    //@pang
    public List<Role> findRoleNameSearchNotDelete(String rolesName)
    {
        EntityManager  em  = getEntityManager();
        try
        {
            Query q = em.createNamedQuery("Role.findByRoleName");
            q.setParameter("roleName", rolesName+"%");
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    } 
    public List<Role> findByUserIdNotIn(List<String> a) {
        Query query = null;
        EntityManager em = getEntityManager();
        try{ 
            query = em.createNamedQuery("Role.findAllRolesNotIn").setParameter("itemRole",a);
            return query.getResultList();
        }
        catch(Exception e)
        {
            return null;
        }
        finally{
            em.close();
        }
    }
}
