/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import com.tct.data.DetailFl;
import com.tct.data.DetailPacking;
import com.tct.data.Details;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author The_Boy_Cs
 */
public class DetailPackingJpaController implements Serializable {
    
    private EntityManagerFactory emf = null;

    public DetailPackingJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    public void create(DetailPacking detailPacking) throws PreexistingEntityException, Exception{
        EntityManager em = null;
        try{
            em = getEntityManager();
            em.getTransaction().begin();
            Details detailIdDetail = detailPacking.getDetailsIdDetail();
            if (detailIdDetail != null) {
                detailIdDetail = em.getReference(detailIdDetail.getClass(), detailIdDetail.getIdDetail());
                detailPacking.setDetailsIdDetail(detailIdDetail);
            }
            em.persist(detailPacking);
            if (detailIdDetail != null) {
                detailIdDetail.getDetailPackingsCollection().add(detailPacking);
                detailIdDetail = em.merge(detailIdDetail);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDetailPacking(detailPacking.getId()) != null) {
                throw new PreexistingEntityException("DetailFl " + detailPacking + " already exists.", ex);
            }
            throw ex;
        }finally{
            if (em != null) {
            em.close();
            }
        }
    }
    
    public void edit(DetailPacking detailPacking)throws NonexistentEntityException, Exception{
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailPacking persistentDetailPacking = em.find(DetailPacking.class, detailPacking.getId());
            Details detailIdDetailOld = persistentDetailPacking.getDetailsIdDetail();
            Details detailIdDetailNew = detailPacking.getDetailsIdDetail();
            if (detailIdDetailNew != null) {
                detailIdDetailNew = em.getReference(detailIdDetailNew.getClass(), detailIdDetailNew.getIdDetail());
                detailPacking.setDetailsIdDetail(detailIdDetailNew);
            }
            detailPacking = em.merge(detailPacking);
            if (detailIdDetailOld != null && !detailIdDetailOld.equals(detailIdDetailNew)) {
                detailIdDetailOld.getDetailPackingsCollection().remove(detailPacking);
                detailIdDetailOld = em.merge(detailIdDetailOld);
            }
            if (detailIdDetailNew != null && !detailIdDetailNew.equals(detailIdDetailOld)) {
                detailIdDetailNew.getDetailPackingsCollection().add(detailPacking);
                detailIdDetailNew = em.merge(detailIdDetailNew);
            }
            em.getTransaction().commit();
        }catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = detailPacking.getId();
                if (findDetailPacking(id) == null) {
                    throw new NonexistentEntityException("The detailFl with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }finally{
            if (em != null) {
                em.close();
            }
        }
    }
    
    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailPacking detailPacking;
            try {
                detailPacking = em.getReference(DetailPacking.class, id);
                detailPacking.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The detailFl with id " + id + " no longer exists.", enfe);
            }
            Details detailIdDetail = detailPacking.getDetailsIdDetail();
            if (detailIdDetail != null) {
                detailIdDetail.getDetailPackingsCollection().remove(detailPacking);
                detailIdDetail = em.merge(detailIdDetail);
            }
            em.remove(detailPacking);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
    public List<DetailPacking> findDetailPackingEntities(){
        return findDetailPackingEntities(true, -1, -1);
    }
    public List<DetailPacking> findDetailPackingEntities(int maxResults, int firstResult){
        return findDetailPackingEntities(false, maxResults, firstResult);
    }
    private List<DetailPacking> findDetailPackingEntities(boolean all, int maxResults, int firstResult){
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(DetailPacking.class));
           Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public int getDetailFlCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<DetailPacking> rt = cq.from(DetailPacking.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public DetailPacking findDetailPacking(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DetailPacking.class, id);
        } finally {
            em.close();
        }
    }
    //--find data packing list
    public List<DetailPacking>  findDetialPackingList(Details idDetail){
        EntityManager em = getEntityManager();
        try{
            Query q  = em.createNamedQuery("DetailPacking.findBydetailsIdDetail");
            q.setParameter("detailsIdDetail",idDetail);
            return q.getResultList();
        }
        finally{
            em.close();
        
        }
    }
    
    public List<DetailPacking> findDetailPackingsAll(){
        EntityManager em = getEntityManager();
        try{
            Query query = em.createNamedQuery("DetailPacking.findAll");
            return query.getResultList();
        }finally{
            em.close();
        }
    }
    public DetailPacking  findDetialPackingListSingle(Details idDetail){
        EntityManager em = getEntityManager();
        try{
            Query q  = em.createNamedQuery("DetailPacking.findBydetailsIdDetail");
            q.setParameter("detailsIdDetail",idDetail);
            return (DetailPacking)q.getSingleResult();
        }
        finally{
            em.close();
        
        }
    }
}
