/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.DetailFieldRight;
import com.tct.data.DetailFieldRightDate;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Oct 12, 2012, Time : 3:51:21 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class DetailFieldRightDateJpaController implements Serializable {

    public DetailFieldRightDateJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DetailFieldRightDate detailFieldRightDate) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailFieldRight idDetailRight = detailFieldRightDate.getIdDetailRight();
            if (idDetailRight != null) {
                idDetailRight = em.getReference(idDetailRight.getClass(), idDetailRight.getId());
                detailFieldRightDate.setIdDetailRight(idDetailRight);
            }
            em.persist(detailFieldRightDate);
            if (idDetailRight != null) {
                idDetailRight.getDetailFieldRightDateCollection().add(detailFieldRightDate);
                idDetailRight = em.merge(idDetailRight);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDetailFieldRightDate(detailFieldRightDate.getIdFieldRihgtData()) != null) {
                throw new PreexistingEntityException("DetailFieldRightDate " + detailFieldRightDate + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DetailFieldRightDate detailFieldRightDate) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailFieldRightDate persistentDetailFieldRightDate = em.find(DetailFieldRightDate.class, detailFieldRightDate.getIdFieldRihgtData());
            DetailFieldRight idDetailRightOld = persistentDetailFieldRightDate.getIdDetailRight();
            DetailFieldRight idDetailRightNew = detailFieldRightDate.getIdDetailRight();
            if (idDetailRightNew != null) {
                idDetailRightNew = em.getReference(idDetailRightNew.getClass(), idDetailRightNew.getId());
                detailFieldRightDate.setIdDetailRight(idDetailRightNew);
            }
            detailFieldRightDate = em.merge(detailFieldRightDate);
            if (idDetailRightOld != null && !idDetailRightOld.equals(idDetailRightNew)) {
                idDetailRightOld.getDetailFieldRightDateCollection().remove(detailFieldRightDate);
                idDetailRightOld = em.merge(idDetailRightOld);
            }
            if (idDetailRightNew != null && !idDetailRightNew.equals(idDetailRightOld)) {
                idDetailRightNew.getDetailFieldRightDateCollection().add(detailFieldRightDate);
                idDetailRightNew = em.merge(idDetailRightNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = detailFieldRightDate.getIdFieldRihgtData();
                if (findDetailFieldRightDate(id) == null) {
                    throw new NonexistentEntityException("The detailFieldRightDate with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailFieldRightDate detailFieldRightDate;
            try {
                detailFieldRightDate = em.getReference(DetailFieldRightDate.class, id);
                detailFieldRightDate.getIdFieldRihgtData();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The detailFieldRightDate with id " + id + " no longer exists.", enfe);
            }
            DetailFieldRight idDetailRight = detailFieldRightDate.getIdDetailRight();
            if (idDetailRight != null) {
                idDetailRight.getDetailFieldRightDateCollection().remove(detailFieldRightDate);
                idDetailRight = em.merge(idDetailRight);
            }
            em.remove(detailFieldRightDate);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DetailFieldRightDate> findDetailFieldRightDateEntities() {
        return findDetailFieldRightDateEntities(true, -1, -1);
    }

    public List<DetailFieldRightDate> findDetailFieldRightDateEntities(int maxResults, int firstResult) {
        return findDetailFieldRightDateEntities(false, maxResults, firstResult);
    }

    private List<DetailFieldRightDate> findDetailFieldRightDateEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(DetailFieldRightDate.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DetailFieldRightDate findDetailFieldRightDate(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DetailFieldRightDate.class, id);
        } finally {
            em.close();
        }
    }

    public int getDetailFieldRightDateCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<DetailFieldRightDate> rt = cq.from(DetailFieldRightDate.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
//    detail Right data
      public DetailFieldRightDate findDetailFieldLeftData(DetailFieldRight idDetailRight,String idDetails)
    {
        EntityManager em  = getEntityManager();
        try
        {
            Query q  = em.createQuery("SELECT d FROM  DetailFieldRightDate d   WHERE d.idDetails =:idDetails and d.idDetailRight =:idDetailRight");
            q.setParameter("idDetailRight", idDetailRight);
            q.setParameter("idDetails",idDetails);
            return (DetailFieldRightDate) q.getSingleResult();
        }
        finally
        {
            em.close();
        }
    }
    

}
