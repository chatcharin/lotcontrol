/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.Details;
import com.tct.data.Front2back;
import com.tct.data.LotControl;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author The_Boy_Cs
 */
public class Front2backJpaController implements Serializable {

    public Front2backJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Front2back front2back) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Details detailsIdDetail = front2back.getDetailsIdDetail();
            if (detailsIdDetail != null) {
                detailsIdDetail = em.getReference(detailsIdDetail.getClass(), detailsIdDetail.getIdDetail());
                front2back.setDetailsIdDetail(detailsIdDetail);
            }
            LotControl lotcontrolId = front2back.getLotcontrolId();
            if (lotcontrolId != null) {
                lotcontrolId = em.getReference(lotcontrolId.getClass(), lotcontrolId.getId());
                front2back.setLotcontrolId(lotcontrolId);
            }
            em.persist(front2back);
            if (detailsIdDetail != null) {
                detailsIdDetail.getFront2backCollection().add(front2back);
                detailsIdDetail = em.merge(detailsIdDetail);
            }
            if (lotcontrolId != null) {
                lotcontrolId.getFront2backCollection().add(front2back);
                lotcontrolId = em.merge(lotcontrolId);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findFront2back(front2back.getFront2backId()) != null) {
                throw new PreexistingEntityException("Front2back " + front2back + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Front2back front2back) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Front2back persistentFront2back = em.find(Front2back.class, front2back.getFront2backId());
            Details detailsIdDetailOld = persistentFront2back.getDetailsIdDetail();
            Details detailsIdDetailNew = front2back.getDetailsIdDetail();
            LotControl lotcontrolIdOld = persistentFront2back.getLotcontrolId();
            LotControl lotcontrolIdNew = front2back.getLotcontrolId();
            if (detailsIdDetailNew != null) {
                detailsIdDetailNew = em.getReference(detailsIdDetailNew.getClass(), detailsIdDetailNew.getIdDetail());
                front2back.setDetailsIdDetail(detailsIdDetailNew);
            }
            if (lotcontrolIdNew != null) {
                lotcontrolIdNew = em.getReference(lotcontrolIdNew.getClass(), lotcontrolIdNew.getId());
                front2back.setLotcontrolId(lotcontrolIdNew);
            }
            front2back = em.merge(front2back);
            if (detailsIdDetailOld != null && !detailsIdDetailOld.equals(detailsIdDetailNew)) {
                detailsIdDetailOld.getFront2backCollection().remove(front2back);
                detailsIdDetailOld = em.merge(detailsIdDetailOld);
            }
            if (detailsIdDetailNew != null && !detailsIdDetailNew.equals(detailsIdDetailOld)) {
                detailsIdDetailNew.getFront2backCollection().add(front2back);
                detailsIdDetailNew = em.merge(detailsIdDetailNew);
            }
            if (lotcontrolIdOld != null && !lotcontrolIdOld.equals(lotcontrolIdNew)) {
                lotcontrolIdOld.getFront2backCollection().remove(front2back);
                lotcontrolIdOld = em.merge(lotcontrolIdOld);
            }
            if (lotcontrolIdNew != null && !lotcontrolIdNew.equals(lotcontrolIdOld)) {
                lotcontrolIdNew.getFront2backCollection().add(front2back);
                lotcontrolIdNew = em.merge(lotcontrolIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = front2back.getFront2backId();
                if (findFront2back(id) == null) {
                    throw new NonexistentEntityException("The front2back with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Front2back front2back;
            try {
                front2back = em.getReference(Front2back.class, id);
                front2back.getFront2backId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The front2back with id " + id + " no longer exists.", enfe);
            }
            Details detailsIdDetail = front2back.getDetailsIdDetail();
            if (detailsIdDetail != null) {
                detailsIdDetail.getFront2backCollection().remove(front2back);
                detailsIdDetail = em.merge(detailsIdDetail);
            }
            LotControl lotcontrolId = front2back.getLotcontrolId();
            if (lotcontrolId != null) {
                lotcontrolId.getFront2backCollection().remove(front2back);
                lotcontrolId = em.merge(lotcontrolId);
            }
            em.remove(front2back);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Front2back> findFront2backEntities() {
        return findFront2backEntities(true, -1, -1);
    }

    public List<Front2back> findFront2backEntities(int maxResults, int firstResult) {
        return findFront2backEntities(false, maxResults, firstResult);
    }

    private List<Front2back> findFront2backEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Front2back.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Front2back findFront2back(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Front2back.class, id);
        } finally {
            em.close();
        }
    }

    public int getFront2backCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Front2back> rt = cq.from(Front2back.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
