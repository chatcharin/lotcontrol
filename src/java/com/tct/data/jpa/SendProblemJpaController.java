/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data.jpa;

import com.tct.data.SendProblem;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Oct 23, 2012, Time : 9:12:17 AM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class SendProblemJpaController implements Serializable {

    public SendProblemJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(SendProblem sendProblem) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(sendProblem);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findSendProblem(sendProblem.getIdSendProblem()) != null) {
                throw new PreexistingEntityException("SendProblem " + sendProblem + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(SendProblem sendProblem) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            sendProblem = em.merge(sendProblem);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = sendProblem.getIdSendProblem();
                if (findSendProblem(id) == null) {
                    throw new NonexistentEntityException("The sendProblem with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SendProblem sendProblem;
            try {
                sendProblem = em.getReference(SendProblem.class, id);
                sendProblem.getIdSendProblem();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The sendProblem with id " + id + " no longer exists.", enfe);
            }
            em.remove(sendProblem);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<SendProblem> findSendProblemEntities() {
        return findSendProblemEntities(true, -1, -1);
    }

    public List<SendProblem> findSendProblemEntities(int maxResults, int firstResult) {
        return findSendProblemEntities(false, maxResults, firstResult);
    }

    private List<SendProblem> findSendProblemEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(SendProblem.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public SendProblem findSendProblem(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(SendProblem.class, id);
        } finally {
            em.close();
        }
    }

    public int getSendProblemCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<SendProblem> rt = cq.from(SendProblem.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
//    find all sendProblem  order by date
    public List<SendProblem>  findAllNotDeletedOrderByDate()
    {
        EntityManager em  =getEntityManager();
        try
        {
            Query q  = em.createQuery("SELECT s FROM SendProblem s ORDER BY s.dated ASC");
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }
//    find send email today
    public SendProblem  findSendEmailToday(String dated)
    {
        EntityManager em  = getEntityManager();
        try
        {
            Query q  = em.createQuery("SELECT s FROM SendProblem s WHERE s.dated =:dated");
            q.setParameter("dated",dated);
            return (SendProblem) q.getSingleResult();
        }
        catch(Exception e)
        {
            return  null;
        }
        finally
        {
            em.close();
        }
    }
}
