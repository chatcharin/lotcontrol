/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.CurrentProcess;
import com.tct.data.Mc;
import java.util.ArrayList;
import java.util.Collection;
import com.tct.data.McDown;
import com.tct.data.McMove;
import com.tct.data.jpa.exceptions.IllegalOrphanException;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author The_Boy_Cs
 */
public class McJpaController implements Serializable {

    public McJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Mc mc) throws PreexistingEntityException, Exception {
        if (mc.getCurrentProcessCollection() == null) {
            mc.setCurrentProcessCollection(new ArrayList<CurrentProcess>());
        }
        if (mc.getMcDownCollection() == null) {
            mc.setMcDownCollection(new ArrayList<McDown>());
        }
        if (mc.getMcMoveCollection() == null) {
            mc.setMcMoveCollection(new ArrayList<McMove>());
        }
        if (mc.getMcMoveCollection1() == null) {
            mc.setMcMoveCollection1(new ArrayList<McMove>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<CurrentProcess> attachedCurrentProcessCollection = new ArrayList<CurrentProcess>();
            for (CurrentProcess currentProcessCollectionCurrentProcessToAttach : mc.getCurrentProcessCollection()) {
                currentProcessCollectionCurrentProcessToAttach = em.getReference(currentProcessCollectionCurrentProcessToAttach.getClass(), currentProcessCollectionCurrentProcessToAttach.getId());
                attachedCurrentProcessCollection.add(currentProcessCollectionCurrentProcessToAttach);
            }
            mc.setCurrentProcessCollection(attachedCurrentProcessCollection);
            Collection<McDown> attachedMcDownCollection = new ArrayList<McDown>();
            for (McDown mcDownCollectionMcDownToAttach : mc.getMcDownCollection()) {
                mcDownCollectionMcDownToAttach = em.getReference(mcDownCollectionMcDownToAttach.getClass(), mcDownCollectionMcDownToAttach.getId());
                attachedMcDownCollection.add(mcDownCollectionMcDownToAttach);
            }
            mc.setMcDownCollection(attachedMcDownCollection);
            Collection<McMove> attachedMcMoveCollection = new ArrayList<McMove>();
            for (McMove mcMoveCollectionMcMoveToAttach : mc.getMcMoveCollection()) {
                mcMoveCollectionMcMoveToAttach = em.getReference(mcMoveCollectionMcMoveToAttach.getClass(), mcMoveCollectionMcMoveToAttach.getId());
                attachedMcMoveCollection.add(mcMoveCollectionMcMoveToAttach);
            }
            mc.setMcMoveCollection(attachedMcMoveCollection);
            Collection<McMove> attachedMcMoveCollection1 = new ArrayList<McMove>();
            for (McMove mcMoveCollection1McMoveToAttach : mc.getMcMoveCollection1()) {
                mcMoveCollection1McMoveToAttach = em.getReference(mcMoveCollection1McMoveToAttach.getClass(), mcMoveCollection1McMoveToAttach.getId());
                attachedMcMoveCollection1.add(mcMoveCollection1McMoveToAttach);
            }
            mc.setMcMoveCollection1(attachedMcMoveCollection1);
            em.persist(mc);
            for (CurrentProcess currentProcessCollectionCurrentProcess : mc.getCurrentProcessCollection()) {
                Mc oldMcIdOfCurrentProcessCollectionCurrentProcess = currentProcessCollectionCurrentProcess.getMcId();
                currentProcessCollectionCurrentProcess.setMcId(mc);
                currentProcessCollectionCurrentProcess = em.merge(currentProcessCollectionCurrentProcess);
                if (oldMcIdOfCurrentProcessCollectionCurrentProcess != null) {
                    oldMcIdOfCurrentProcessCollectionCurrentProcess.getCurrentProcessCollection().remove(currentProcessCollectionCurrentProcess);
                    oldMcIdOfCurrentProcessCollectionCurrentProcess = em.merge(oldMcIdOfCurrentProcessCollectionCurrentProcess);
                }
            }
            for (McDown mcDownCollectionMcDown : mc.getMcDownCollection()) {
                Mc oldMcIdOfMcDownCollectionMcDown = mcDownCollectionMcDown.getMcId();
                mcDownCollectionMcDown.setMcId(mc);
                mcDownCollectionMcDown = em.merge(mcDownCollectionMcDown);
                if (oldMcIdOfMcDownCollectionMcDown != null) {
                    oldMcIdOfMcDownCollectionMcDown.getMcDownCollection().remove(mcDownCollectionMcDown);
                    oldMcIdOfMcDownCollectionMcDown = em.merge(oldMcIdOfMcDownCollectionMcDown);
                }
            }
            for (McMove mcMoveCollectionMcMove : mc.getMcMoveCollection()) {
                Mc oldBeforMcOfMcMoveCollectionMcMove = mcMoveCollectionMcMove.getBeforMc();
                mcMoveCollectionMcMove.setBeforMc(mc);
                mcMoveCollectionMcMove = em.merge(mcMoveCollectionMcMove);
                if (oldBeforMcOfMcMoveCollectionMcMove != null) {
                    oldBeforMcOfMcMoveCollectionMcMove.getMcMoveCollection().remove(mcMoveCollectionMcMove);
                    oldBeforMcOfMcMoveCollectionMcMove = em.merge(oldBeforMcOfMcMoveCollectionMcMove);
                }
            }
            for (McMove mcMoveCollection1McMove : mc.getMcMoveCollection1()) {
                Mc oldAftorMcOfMcMoveCollection1McMove = mcMoveCollection1McMove.getAftorMc();
                mcMoveCollection1McMove.setAftorMc(mc);
                mcMoveCollection1McMove = em.merge(mcMoveCollection1McMove);
                if (oldAftorMcOfMcMoveCollection1McMove != null) {
                    oldAftorMcOfMcMoveCollection1McMove.getMcMoveCollection1().remove(mcMoveCollection1McMove);
                    oldAftorMcOfMcMoveCollection1McMove = em.merge(oldAftorMcOfMcMoveCollection1McMove);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findMc(mc.getId()) != null) {
                throw new PreexistingEntityException("Mc " + mc + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Mc mc) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Mc persistentMc = em.find(Mc.class, mc.getId());
            Collection<CurrentProcess> currentProcessCollectionOld = persistentMc.getCurrentProcessCollection();
            Collection<CurrentProcess> currentProcessCollectionNew = mc.getCurrentProcessCollection();
            Collection<McDown> mcDownCollectionOld = persistentMc.getMcDownCollection();
            Collection<McDown> mcDownCollectionNew = mc.getMcDownCollection();
            Collection<McMove> mcMoveCollectionOld = persistentMc.getMcMoveCollection();
            Collection<McMove> mcMoveCollectionNew = mc.getMcMoveCollection();
            Collection<McMove> mcMoveCollection1Old = persistentMc.getMcMoveCollection1();
            Collection<McMove> mcMoveCollection1New = mc.getMcMoveCollection1();
            List<String> illegalOrphanMessages = null;
            for (McDown mcDownCollectionOldMcDown : mcDownCollectionOld) {
                if (!mcDownCollectionNew.contains(mcDownCollectionOldMcDown)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain McDown " + mcDownCollectionOldMcDown + " since its mcId field is not nullable.");
                }
            }
            for (McMove mcMoveCollectionOldMcMove : mcMoveCollectionOld) {
                if (!mcMoveCollectionNew.contains(mcMoveCollectionOldMcMove)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain McMove " + mcMoveCollectionOldMcMove + " since its beforMc field is not nullable.");
                }
            }
            for (McMove mcMoveCollection1OldMcMove : mcMoveCollection1Old) {
                if (!mcMoveCollection1New.contains(mcMoveCollection1OldMcMove)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain McMove " + mcMoveCollection1OldMcMove + " since its aftorMc field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<CurrentProcess> attachedCurrentProcessCollectionNew = new ArrayList<CurrentProcess>();
            for (CurrentProcess currentProcessCollectionNewCurrentProcessToAttach : currentProcessCollectionNew) {
                currentProcessCollectionNewCurrentProcessToAttach = em.getReference(currentProcessCollectionNewCurrentProcessToAttach.getClass(), currentProcessCollectionNewCurrentProcessToAttach.getId());
                attachedCurrentProcessCollectionNew.add(currentProcessCollectionNewCurrentProcessToAttach);
            }
            currentProcessCollectionNew = attachedCurrentProcessCollectionNew;
            mc.setCurrentProcessCollection(currentProcessCollectionNew);
            Collection<McDown> attachedMcDownCollectionNew = new ArrayList<McDown>();
            for (McDown mcDownCollectionNewMcDownToAttach : mcDownCollectionNew) {
                mcDownCollectionNewMcDownToAttach = em.getReference(mcDownCollectionNewMcDownToAttach.getClass(), mcDownCollectionNewMcDownToAttach.getId());
                attachedMcDownCollectionNew.add(mcDownCollectionNewMcDownToAttach);
            }
            mcDownCollectionNew = attachedMcDownCollectionNew;
            mc.setMcDownCollection(mcDownCollectionNew);
            Collection<McMove> attachedMcMoveCollectionNew = new ArrayList<McMove>();
            for (McMove mcMoveCollectionNewMcMoveToAttach : mcMoveCollectionNew) {
                mcMoveCollectionNewMcMoveToAttach = em.getReference(mcMoveCollectionNewMcMoveToAttach.getClass(), mcMoveCollectionNewMcMoveToAttach.getId());
                attachedMcMoveCollectionNew.add(mcMoveCollectionNewMcMoveToAttach);
            }
            mcMoveCollectionNew = attachedMcMoveCollectionNew;
            mc.setMcMoveCollection(mcMoveCollectionNew);
            Collection<McMove> attachedMcMoveCollection1New = new ArrayList<McMove>();
            for (McMove mcMoveCollection1NewMcMoveToAttach : mcMoveCollection1New) {
                mcMoveCollection1NewMcMoveToAttach = em.getReference(mcMoveCollection1NewMcMoveToAttach.getClass(), mcMoveCollection1NewMcMoveToAttach.getId());
                attachedMcMoveCollection1New.add(mcMoveCollection1NewMcMoveToAttach);
            }
            mcMoveCollection1New = attachedMcMoveCollection1New;
            mc.setMcMoveCollection1(mcMoveCollection1New);
            mc = em.merge(mc);
            for (CurrentProcess currentProcessCollectionOldCurrentProcess : currentProcessCollectionOld) {
                if (!currentProcessCollectionNew.contains(currentProcessCollectionOldCurrentProcess)) {
                    currentProcessCollectionOldCurrentProcess.setMcId(null);
                    currentProcessCollectionOldCurrentProcess = em.merge(currentProcessCollectionOldCurrentProcess);
                }
            }
            for (CurrentProcess currentProcessCollectionNewCurrentProcess : currentProcessCollectionNew) {
                if (!currentProcessCollectionOld.contains(currentProcessCollectionNewCurrentProcess)) {
                    Mc oldMcIdOfCurrentProcessCollectionNewCurrentProcess = currentProcessCollectionNewCurrentProcess.getMcId();
                    currentProcessCollectionNewCurrentProcess.setMcId(mc);
                    currentProcessCollectionNewCurrentProcess = em.merge(currentProcessCollectionNewCurrentProcess);
                    if (oldMcIdOfCurrentProcessCollectionNewCurrentProcess != null && !oldMcIdOfCurrentProcessCollectionNewCurrentProcess.equals(mc)) {
                        oldMcIdOfCurrentProcessCollectionNewCurrentProcess.getCurrentProcessCollection().remove(currentProcessCollectionNewCurrentProcess);
                        oldMcIdOfCurrentProcessCollectionNewCurrentProcess = em.merge(oldMcIdOfCurrentProcessCollectionNewCurrentProcess);
                    }
                }
            }
            for (McDown mcDownCollectionNewMcDown : mcDownCollectionNew) {
                if (!mcDownCollectionOld.contains(mcDownCollectionNewMcDown)) {
                    Mc oldMcIdOfMcDownCollectionNewMcDown = mcDownCollectionNewMcDown.getMcId();
                    mcDownCollectionNewMcDown.setMcId(mc);
                    mcDownCollectionNewMcDown = em.merge(mcDownCollectionNewMcDown);
                    if (oldMcIdOfMcDownCollectionNewMcDown != null && !oldMcIdOfMcDownCollectionNewMcDown.equals(mc)) {
                        oldMcIdOfMcDownCollectionNewMcDown.getMcDownCollection().remove(mcDownCollectionNewMcDown);
                        oldMcIdOfMcDownCollectionNewMcDown = em.merge(oldMcIdOfMcDownCollectionNewMcDown);
                    }
                }
            }
            for (McMove mcMoveCollectionNewMcMove : mcMoveCollectionNew) {
                if (!mcMoveCollectionOld.contains(mcMoveCollectionNewMcMove)) {
                    Mc oldBeforMcOfMcMoveCollectionNewMcMove = mcMoveCollectionNewMcMove.getBeforMc();
                    mcMoveCollectionNewMcMove.setBeforMc(mc);
                    mcMoveCollectionNewMcMove = em.merge(mcMoveCollectionNewMcMove);
                    if (oldBeforMcOfMcMoveCollectionNewMcMove != null && !oldBeforMcOfMcMoveCollectionNewMcMove.equals(mc)) {
                        oldBeforMcOfMcMoveCollectionNewMcMove.getMcMoveCollection().remove(mcMoveCollectionNewMcMove);
                        oldBeforMcOfMcMoveCollectionNewMcMove = em.merge(oldBeforMcOfMcMoveCollectionNewMcMove);
                    }
                }
            }
            for (McMove mcMoveCollection1NewMcMove : mcMoveCollection1New) {
                if (!mcMoveCollection1Old.contains(mcMoveCollection1NewMcMove)) {
                    Mc oldAftorMcOfMcMoveCollection1NewMcMove = mcMoveCollection1NewMcMove.getAftorMc();
                    mcMoveCollection1NewMcMove.setAftorMc(mc);
                    mcMoveCollection1NewMcMove = em.merge(mcMoveCollection1NewMcMove);
                    if (oldAftorMcOfMcMoveCollection1NewMcMove != null && !oldAftorMcOfMcMoveCollection1NewMcMove.equals(mc)) {
                        oldAftorMcOfMcMoveCollection1NewMcMove.getMcMoveCollection1().remove(mcMoveCollection1NewMcMove);
                        oldAftorMcOfMcMoveCollection1NewMcMove = em.merge(oldAftorMcOfMcMoveCollection1NewMcMove);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = mc.getId();
                if (findMc(id) == null) {
                    throw new NonexistentEntityException("The mc with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Mc mc;
            try {
                mc = em.getReference(Mc.class, id);
                mc.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The mc with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<McDown> mcDownCollectionOrphanCheck = mc.getMcDownCollection();
            for (McDown mcDownCollectionOrphanCheckMcDown : mcDownCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Mc (" + mc + ") cannot be destroyed since the McDown " + mcDownCollectionOrphanCheckMcDown + " in its mcDownCollection field has a non-nullable mcId field.");
            }
            Collection<McMove> mcMoveCollectionOrphanCheck = mc.getMcMoveCollection();
            for (McMove mcMoveCollectionOrphanCheckMcMove : mcMoveCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Mc (" + mc + ") cannot be destroyed since the McMove " + mcMoveCollectionOrphanCheckMcMove + " in its mcMoveCollection field has a non-nullable beforMc field.");
            }
            Collection<McMove> mcMoveCollection1OrphanCheck = mc.getMcMoveCollection1();
            for (McMove mcMoveCollection1OrphanCheckMcMove : mcMoveCollection1OrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Mc (" + mc + ") cannot be destroyed since the McMove " + mcMoveCollection1OrphanCheckMcMove + " in its mcMoveCollection1 field has a non-nullable aftorMc field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<CurrentProcess> currentProcessCollection = mc.getCurrentProcessCollection();
            for (CurrentProcess currentProcessCollectionCurrentProcess : currentProcessCollection) {
                currentProcessCollectionCurrentProcess.setMcId(null);
                currentProcessCollectionCurrentProcess = em.merge(currentProcessCollectionCurrentProcess);
            }
            em.remove(mc);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Mc> findMcEntities() {
        return findMcEntities(true, -1, -1);
    }

    public List<Mc> findMcEntities(int maxResults, int firstResult) {
        return findMcEntities(false, maxResults, firstResult);
    }

    private List<Mc> findMcEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Mc.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Mc findMc(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Mc.class, id);
        } finally {
            em.close();
        }
    }

    public int getMcCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Mc> rt = cq.from(Mc.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    //---find  mc is good
    public  List<Mc> findListMCGood(String mcStatus){
        EntityManager em  = getEntityManager();
        try{ 
            Query q  = em.createNamedQuery("Mc.findByMcStatus");
            q.setParameter("mcStatus",mcStatus);
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }
    //@pang
    public  List<Mc> findByMcname(String mcNameSearch){
        EntityManager em  = getEntityManager();
        try{ 
            Query q  = em.createNamedQuery("Mc.findByMcName");
            q.setParameter("mcName",mcNameSearch+"%");
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }
    public List<Mc> findAllMcNotDelete()
    {
        EntityManager em  = getEntityManager();
        try{ 
            Query q  = em.createNamedQuery("Mc.findAllNotDelete"); 
            return q.getResultList();
        }
        finally
        {
            em.close();
        } 
    }
    
}
