/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import com.tct.data.*;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import com.tct.data.jpa.exceptions.IllegalOrphanException;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author The_Boy_Cs
 */
public class CurrentProcessJpaController implements Serializable {

    public CurrentProcessJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CurrentProcess currentProcess) throws PreexistingEntityException, Exception {
        if (currentProcess.getNgNormalCollection() == null) {
            currentProcess.setNgNormalCollection(new ArrayList<NgNormal>());
        }
        if (currentProcess.getNgRepairCollection() == null) {
            currentProcess.setNgRepairCollection(new ArrayList<NgRepair>());
        }
        if (currentProcess.getMcDownCollection() == null) {
            currentProcess.setMcDownCollection(new ArrayList<McDown>());
        }
        if (currentProcess.getHoldCollection() == null) {
            currentProcess.setHoldCollection(new ArrayList<Hold>());
        }
        if (currentProcess.getPcsQtyCollection() == null) {
            currentProcess.setPcsQtyCollection(new ArrayList<PcsQty>());
        }
        if (currentProcess.getMcMoveCollection() == null) {
            currentProcess.setMcMoveCollection(new ArrayList<McMove>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MainData mainDataId = currentProcess.getMainDataId();
            if (mainDataId != null) {
                mainDataId = em.getReference(mainDataId.getClass(), mainDataId.getId());
                currentProcess.setMainDataId(mainDataId);
            }
            Mc mcId = currentProcess.getMcId();
            if (mcId != null) {
                mcId = em.getReference(mcId.getClass(), mcId.getId());
                currentProcess.setMcId(mcId);
            }
            Collection<NgNormal> attachedNgNormalCollection = new ArrayList<NgNormal>();
            for (NgNormal ngNormalCollectionNgNormalToAttach : currentProcess.getNgNormalCollection()) {
                ngNormalCollectionNgNormalToAttach = em.getReference(ngNormalCollectionNgNormalToAttach.getClass(), ngNormalCollectionNgNormalToAttach.getIdNg());
                attachedNgNormalCollection.add(ngNormalCollectionNgNormalToAttach);
            }
            currentProcess.setNgNormalCollection(attachedNgNormalCollection);
            Collection<NgRepair> attachedNgRepairCollection = new ArrayList<NgRepair>();
            for (NgRepair ngRepairCollectionNgRepairToAttach : currentProcess.getNgRepairCollection()) {
                ngRepairCollectionNgRepairToAttach = em.getReference(ngRepairCollectionNgRepairToAttach.getClass(), ngRepairCollectionNgRepairToAttach.getIdNg());
                attachedNgRepairCollection.add(ngRepairCollectionNgRepairToAttach);
            }
            currentProcess.setNgRepairCollection(attachedNgRepairCollection);
            Collection<McDown> attachedMcDownCollection = new ArrayList<McDown>();
            for (McDown mcDownCollectionMcDownToAttach : currentProcess.getMcDownCollection()) {
                mcDownCollectionMcDownToAttach = em.getReference(mcDownCollectionMcDownToAttach.getClass(), mcDownCollectionMcDownToAttach.getId());
                attachedMcDownCollection.add(mcDownCollectionMcDownToAttach);
            }
            currentProcess.setMcDownCollection(attachedMcDownCollection);
            Collection<Hold> attachedHoldCollection = new ArrayList<Hold>();
            for (Hold holdCollectionHoldToAttach : currentProcess.getHoldCollection()) {
                holdCollectionHoldToAttach = em.getReference(holdCollectionHoldToAttach.getClass(), holdCollectionHoldToAttach.getId());
                attachedHoldCollection.add(holdCollectionHoldToAttach);
            }
            currentProcess.setHoldCollection(attachedHoldCollection);
            Collection<PcsQty> attachedPcsQtyCollection = new ArrayList<PcsQty>();
            for (PcsQty pcsQtyCollectionPcsQtyToAttach : currentProcess.getPcsQtyCollection()) {
                pcsQtyCollectionPcsQtyToAttach = em.getReference(pcsQtyCollectionPcsQtyToAttach.getClass(), pcsQtyCollectionPcsQtyToAttach.getId());
                attachedPcsQtyCollection.add(pcsQtyCollectionPcsQtyToAttach);
            }
            currentProcess.setPcsQtyCollection(attachedPcsQtyCollection);
            Collection<McMove> attachedMcMoveCollection = new ArrayList<McMove>();
            for (McMove mcMoveCollectionMcMoveToAttach : currentProcess.getMcMoveCollection()) {
                mcMoveCollectionMcMoveToAttach = em.getReference(mcMoveCollectionMcMoveToAttach.getClass(), mcMoveCollectionMcMoveToAttach.getId());
                attachedMcMoveCollection.add(mcMoveCollectionMcMoveToAttach);
            }
            currentProcess.setMcMoveCollection(attachedMcMoveCollection);
            em.persist(currentProcess);
            if (mainDataId != null) {
                mainDataId.getCurrentProcessCollection().add(currentProcess);
                mainDataId = em.merge(mainDataId);
            }
            if (mcId != null) {
                mcId.getCurrentProcessCollection().add(currentProcess);
                mcId = em.merge(mcId);
            }
            for (NgNormal ngNormalCollectionNgNormal : currentProcess.getNgNormalCollection()) {
                CurrentProcess oldCurrentProcessIdOfNgNormalCollectionNgNormal = ngNormalCollectionNgNormal.getCurrentProcessId();
                ngNormalCollectionNgNormal.setCurrentProcessId(currentProcess);
                ngNormalCollectionNgNormal = em.merge(ngNormalCollectionNgNormal);
                if (oldCurrentProcessIdOfNgNormalCollectionNgNormal != null) {
                    oldCurrentProcessIdOfNgNormalCollectionNgNormal.getNgNormalCollection().remove(ngNormalCollectionNgNormal);
                    oldCurrentProcessIdOfNgNormalCollectionNgNormal = em.merge(oldCurrentProcessIdOfNgNormalCollectionNgNormal);
                }
            }
            for (NgRepair ngRepairCollectionNgRepair : currentProcess.getNgRepairCollection()) {
                CurrentProcess oldCurrentProcessIdOfNgRepairCollectionNgRepair = ngRepairCollectionNgRepair.getCurrentProcessId();
                ngRepairCollectionNgRepair.setCurrentProcessId(currentProcess);
                ngRepairCollectionNgRepair = em.merge(ngRepairCollectionNgRepair);
                if (oldCurrentProcessIdOfNgRepairCollectionNgRepair != null) {
                    oldCurrentProcessIdOfNgRepairCollectionNgRepair.getNgRepairCollection().remove(ngRepairCollectionNgRepair);
                    oldCurrentProcessIdOfNgRepairCollectionNgRepair = em.merge(oldCurrentProcessIdOfNgRepairCollectionNgRepair);
                }
            }
            for (McDown mcDownCollectionMcDown : currentProcess.getMcDownCollection()) {
                CurrentProcess oldCurrentProcessIdOfMcDownCollectionMcDown = mcDownCollectionMcDown.getCurrentProcessId();
                mcDownCollectionMcDown.setCurrentProcessId(currentProcess);
                mcDownCollectionMcDown = em.merge(mcDownCollectionMcDown);
                if (oldCurrentProcessIdOfMcDownCollectionMcDown != null) {
                    oldCurrentProcessIdOfMcDownCollectionMcDown.getMcDownCollection().remove(mcDownCollectionMcDown);
                    oldCurrentProcessIdOfMcDownCollectionMcDown = em.merge(oldCurrentProcessIdOfMcDownCollectionMcDown);
                }
            }
            for (Hold holdCollectionHold : currentProcess.getHoldCollection()) {
                CurrentProcess oldCurrentProcessIdOfHoldCollectionHold = holdCollectionHold.getCurrentProcessId();
                holdCollectionHold.setCurrentProcessId(currentProcess);
                holdCollectionHold = em.merge(holdCollectionHold);
                if (oldCurrentProcessIdOfHoldCollectionHold != null) {
                    oldCurrentProcessIdOfHoldCollectionHold.getHoldCollection().remove(holdCollectionHold);
                    oldCurrentProcessIdOfHoldCollectionHold = em.merge(oldCurrentProcessIdOfHoldCollectionHold);
                }
            }
            for (PcsQty pcsQtyCollectionPcsQty : currentProcess.getPcsQtyCollection()) {
                CurrentProcess oldCurrentProcessIdOfPcsQtyCollectionPcsQty = pcsQtyCollectionPcsQty.getCurrentProcessId();
                pcsQtyCollectionPcsQty.setCurrentProcessId(currentProcess);
                pcsQtyCollectionPcsQty = em.merge(pcsQtyCollectionPcsQty);
                if (oldCurrentProcessIdOfPcsQtyCollectionPcsQty != null) {
                    oldCurrentProcessIdOfPcsQtyCollectionPcsQty.getPcsQtyCollection().remove(pcsQtyCollectionPcsQty);
                    oldCurrentProcessIdOfPcsQtyCollectionPcsQty = em.merge(oldCurrentProcessIdOfPcsQtyCollectionPcsQty);
                }
            }
            for (McMove mcMoveCollectionMcMove : currentProcess.getMcMoveCollection()) {
                CurrentProcess oldCurrentProcessIdOfMcMoveCollectionMcMove = mcMoveCollectionMcMove.getCurrentProcessId();
                mcMoveCollectionMcMove.setCurrentProcessId(currentProcess);
                mcMoveCollectionMcMove = em.merge(mcMoveCollectionMcMove);
                if (oldCurrentProcessIdOfMcMoveCollectionMcMove != null) {
                    oldCurrentProcessIdOfMcMoveCollectionMcMove.getMcMoveCollection().remove(mcMoveCollectionMcMove);
                    oldCurrentProcessIdOfMcMoveCollectionMcMove = em.merge(oldCurrentProcessIdOfMcMoveCollectionMcMove);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCurrentProcess(currentProcess.getId()) != null) {
                throw new PreexistingEntityException("CurrentProcess " + currentProcess + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CurrentProcess currentProcess) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CurrentProcess persistentCurrentProcess = em.find(CurrentProcess.class, currentProcess.getId());
            MainData mainDataIdOld = persistentCurrentProcess.getMainDataId();
            MainData mainDataIdNew = currentProcess.getMainDataId();
            Mc mcIdOld = persistentCurrentProcess.getMcId();
            Mc mcIdNew = currentProcess.getMcId();
            Collection<NgNormal> ngNormalCollectionOld = persistentCurrentProcess.getNgNormalCollection();
            Collection<NgNormal> ngNormalCollectionNew = currentProcess.getNgNormalCollection();
            Collection<NgRepair> ngRepairCollectionOld = persistentCurrentProcess.getNgRepairCollection();
            Collection<NgRepair> ngRepairCollectionNew = currentProcess.getNgRepairCollection();
            Collection<McDown> mcDownCollectionOld = persistentCurrentProcess.getMcDownCollection();
            Collection<McDown> mcDownCollectionNew = currentProcess.getMcDownCollection();
            Collection<Hold> holdCollectionOld = persistentCurrentProcess.getHoldCollection();
            Collection<Hold> holdCollectionNew = currentProcess.getHoldCollection();
            Collection<PcsQty> pcsQtyCollectionOld = persistentCurrentProcess.getPcsQtyCollection();
            Collection<PcsQty> pcsQtyCollectionNew = currentProcess.getPcsQtyCollection();
            Collection<McMove> mcMoveCollectionOld = persistentCurrentProcess.getMcMoveCollection();
            Collection<McMove> mcMoveCollectionNew = currentProcess.getMcMoveCollection();
            List<String> illegalOrphanMessages = null;
            for (NgNormal ngNormalCollectionOldNgNormal : ngNormalCollectionOld) {
                if (!ngNormalCollectionNew.contains(ngNormalCollectionOldNgNormal)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain NgNormal " + ngNormalCollectionOldNgNormal + " since its currentProcessId field is not nullable.");
                }
            }
            for (NgRepair ngRepairCollectionOldNgRepair : ngRepairCollectionOld) {
                if (!ngRepairCollectionNew.contains(ngRepairCollectionOldNgRepair)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain NgRepair " + ngRepairCollectionOldNgRepair + " since its currentProcessId field is not nullable.");
                }
            }
            for (McDown mcDownCollectionOldMcDown : mcDownCollectionOld) {
                if (!mcDownCollectionNew.contains(mcDownCollectionOldMcDown)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain McDown " + mcDownCollectionOldMcDown + " since its currentProcessId field is not nullable.");
                }
            }
            for (Hold holdCollectionOldHold : holdCollectionOld) {
                if (!holdCollectionNew.contains(holdCollectionOldHold)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Hold " + holdCollectionOldHold + " since its currentProcessId field is not nullable.");
                }
            }
            for (PcsQty pcsQtyCollectionOldPcsQty : pcsQtyCollectionOld) {
                if (!pcsQtyCollectionNew.contains(pcsQtyCollectionOldPcsQty)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain PcsQty " + pcsQtyCollectionOldPcsQty + " since its currentProcessId field is not nullable.");
                }
            }
            for (McMove mcMoveCollectionOldMcMove : mcMoveCollectionOld) {
                if (!mcMoveCollectionNew.contains(mcMoveCollectionOldMcMove)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain McMove " + mcMoveCollectionOldMcMove + " since its currentProcessId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (mainDataIdNew != null) {
                mainDataIdNew = em.getReference(mainDataIdNew.getClass(), mainDataIdNew.getId());
                currentProcess.setMainDataId(mainDataIdNew);
            }
            if (mcIdNew != null) {
                mcIdNew = em.getReference(mcIdNew.getClass(), mcIdNew.getId());
                currentProcess.setMcId(mcIdNew);
            }
            Collection<NgNormal> attachedNgNormalCollectionNew = new ArrayList<NgNormal>();
            for (NgNormal ngNormalCollectionNewNgNormalToAttach : ngNormalCollectionNew) {
                ngNormalCollectionNewNgNormalToAttach = em.getReference(ngNormalCollectionNewNgNormalToAttach.getClass(), ngNormalCollectionNewNgNormalToAttach.getIdNg());
                attachedNgNormalCollectionNew.add(ngNormalCollectionNewNgNormalToAttach);
            }
            ngNormalCollectionNew = attachedNgNormalCollectionNew;
            currentProcess.setNgNormalCollection(ngNormalCollectionNew);
            Collection<NgRepair> attachedNgRepairCollectionNew = new ArrayList<NgRepair>();
            for (NgRepair ngRepairCollectionNewNgRepairToAttach : ngRepairCollectionNew) {
                ngRepairCollectionNewNgRepairToAttach = em.getReference(ngRepairCollectionNewNgRepairToAttach.getClass(), ngRepairCollectionNewNgRepairToAttach.getIdNg());
                attachedNgRepairCollectionNew.add(ngRepairCollectionNewNgRepairToAttach);
            }
            ngRepairCollectionNew = attachedNgRepairCollectionNew;
            currentProcess.setNgRepairCollection(ngRepairCollectionNew);
            Collection<McDown> attachedMcDownCollectionNew = new ArrayList<McDown>();
            for (McDown mcDownCollectionNewMcDownToAttach : mcDownCollectionNew) {
                mcDownCollectionNewMcDownToAttach = em.getReference(mcDownCollectionNewMcDownToAttach.getClass(), mcDownCollectionNewMcDownToAttach.getId());
                attachedMcDownCollectionNew.add(mcDownCollectionNewMcDownToAttach);
            }
            mcDownCollectionNew = attachedMcDownCollectionNew;
            currentProcess.setMcDownCollection(mcDownCollectionNew);
            Collection<Hold> attachedHoldCollectionNew = new ArrayList<Hold>();
            for (Hold holdCollectionNewHoldToAttach : holdCollectionNew) {
                holdCollectionNewHoldToAttach = em.getReference(holdCollectionNewHoldToAttach.getClass(), holdCollectionNewHoldToAttach.getId());
                attachedHoldCollectionNew.add(holdCollectionNewHoldToAttach);
            }
            holdCollectionNew = attachedHoldCollectionNew;
            currentProcess.setHoldCollection(holdCollectionNew);
            Collection<PcsQty> attachedPcsQtyCollectionNew = new ArrayList<PcsQty>();
            for (PcsQty pcsQtyCollectionNewPcsQtyToAttach : pcsQtyCollectionNew) {
                pcsQtyCollectionNewPcsQtyToAttach = em.getReference(pcsQtyCollectionNewPcsQtyToAttach.getClass(), pcsQtyCollectionNewPcsQtyToAttach.getId());
                attachedPcsQtyCollectionNew.add(pcsQtyCollectionNewPcsQtyToAttach);
            }
            pcsQtyCollectionNew = attachedPcsQtyCollectionNew;
            currentProcess.setPcsQtyCollection(pcsQtyCollectionNew);
            Collection<McMove> attachedMcMoveCollectionNew = new ArrayList<McMove>();
            for (McMove mcMoveCollectionNewMcMoveToAttach : mcMoveCollectionNew) {
                mcMoveCollectionNewMcMoveToAttach = em.getReference(mcMoveCollectionNewMcMoveToAttach.getClass(), mcMoveCollectionNewMcMoveToAttach.getId());
                attachedMcMoveCollectionNew.add(mcMoveCollectionNewMcMoveToAttach);
            }
            mcMoveCollectionNew = attachedMcMoveCollectionNew;
            currentProcess.setMcMoveCollection(mcMoveCollectionNew);
            currentProcess = em.merge(currentProcess);
            if (mainDataIdOld != null && !mainDataIdOld.equals(mainDataIdNew)) {
                mainDataIdOld.getCurrentProcessCollection().remove(currentProcess);
                mainDataIdOld = em.merge(mainDataIdOld);
            }
            if (mainDataIdNew != null && !mainDataIdNew.equals(mainDataIdOld)) {
                mainDataIdNew.getCurrentProcessCollection().add(currentProcess);
                mainDataIdNew = em.merge(mainDataIdNew);
            }
            if (mcIdOld != null && !mcIdOld.equals(mcIdNew)) {
                mcIdOld.getCurrentProcessCollection().remove(currentProcess);
                mcIdOld = em.merge(mcIdOld);
            }
            if (mcIdNew != null && !mcIdNew.equals(mcIdOld)) {
                mcIdNew.getCurrentProcessCollection().add(currentProcess);
                mcIdNew = em.merge(mcIdNew);
            }
            for (NgNormal ngNormalCollectionNewNgNormal : ngNormalCollectionNew) {
                if (!ngNormalCollectionOld.contains(ngNormalCollectionNewNgNormal)) {
                    CurrentProcess oldCurrentProcessIdOfNgNormalCollectionNewNgNormal = ngNormalCollectionNewNgNormal.getCurrentProcessId();
                    ngNormalCollectionNewNgNormal.setCurrentProcessId(currentProcess);
                    ngNormalCollectionNewNgNormal = em.merge(ngNormalCollectionNewNgNormal);
                    if (oldCurrentProcessIdOfNgNormalCollectionNewNgNormal != null && !oldCurrentProcessIdOfNgNormalCollectionNewNgNormal.equals(currentProcess)) {
                        oldCurrentProcessIdOfNgNormalCollectionNewNgNormal.getNgNormalCollection().remove(ngNormalCollectionNewNgNormal);
                        oldCurrentProcessIdOfNgNormalCollectionNewNgNormal = em.merge(oldCurrentProcessIdOfNgNormalCollectionNewNgNormal);
                    }
                }
            }
            for (NgRepair ngRepairCollectionNewNgRepair : ngRepairCollectionNew) {
                if (!ngRepairCollectionOld.contains(ngRepairCollectionNewNgRepair)) {
                    CurrentProcess oldCurrentProcessIdOfNgRepairCollectionNewNgRepair = ngRepairCollectionNewNgRepair.getCurrentProcessId();
                    ngRepairCollectionNewNgRepair.setCurrentProcessId(currentProcess);
                    ngRepairCollectionNewNgRepair = em.merge(ngRepairCollectionNewNgRepair);
                    if (oldCurrentProcessIdOfNgRepairCollectionNewNgRepair != null && !oldCurrentProcessIdOfNgRepairCollectionNewNgRepair.equals(currentProcess)) {
                        oldCurrentProcessIdOfNgRepairCollectionNewNgRepair.getNgRepairCollection().remove(ngRepairCollectionNewNgRepair);
                        oldCurrentProcessIdOfNgRepairCollectionNewNgRepair = em.merge(oldCurrentProcessIdOfNgRepairCollectionNewNgRepair);
                    }
                }
            }
            for (McDown mcDownCollectionNewMcDown : mcDownCollectionNew) {
                if (!mcDownCollectionOld.contains(mcDownCollectionNewMcDown)) {
                    CurrentProcess oldCurrentProcessIdOfMcDownCollectionNewMcDown = mcDownCollectionNewMcDown.getCurrentProcessId();
                    mcDownCollectionNewMcDown.setCurrentProcessId(currentProcess);
                    mcDownCollectionNewMcDown = em.merge(mcDownCollectionNewMcDown);
                    if (oldCurrentProcessIdOfMcDownCollectionNewMcDown != null && !oldCurrentProcessIdOfMcDownCollectionNewMcDown.equals(currentProcess)) {
                        oldCurrentProcessIdOfMcDownCollectionNewMcDown.getMcDownCollection().remove(mcDownCollectionNewMcDown);
                        oldCurrentProcessIdOfMcDownCollectionNewMcDown = em.merge(oldCurrentProcessIdOfMcDownCollectionNewMcDown);
                    }
                }
            }
            for (Hold holdCollectionNewHold : holdCollectionNew) {
                if (!holdCollectionOld.contains(holdCollectionNewHold)) {
                    CurrentProcess oldCurrentProcessIdOfHoldCollectionNewHold = holdCollectionNewHold.getCurrentProcessId();
                    holdCollectionNewHold.setCurrentProcessId(currentProcess);
                    holdCollectionNewHold = em.merge(holdCollectionNewHold);
                    if (oldCurrentProcessIdOfHoldCollectionNewHold != null && !oldCurrentProcessIdOfHoldCollectionNewHold.equals(currentProcess)) {
                        oldCurrentProcessIdOfHoldCollectionNewHold.getHoldCollection().remove(holdCollectionNewHold);
                        oldCurrentProcessIdOfHoldCollectionNewHold = em.merge(oldCurrentProcessIdOfHoldCollectionNewHold);
                    }
                }
            }
            for (PcsQty pcsQtyCollectionNewPcsQty : pcsQtyCollectionNew) {
                if (!pcsQtyCollectionOld.contains(pcsQtyCollectionNewPcsQty)) {
                    CurrentProcess oldCurrentProcessIdOfPcsQtyCollectionNewPcsQty = pcsQtyCollectionNewPcsQty.getCurrentProcessId();
                    pcsQtyCollectionNewPcsQty.setCurrentProcessId(currentProcess);
                    pcsQtyCollectionNewPcsQty = em.merge(pcsQtyCollectionNewPcsQty);
                    if (oldCurrentProcessIdOfPcsQtyCollectionNewPcsQty != null && !oldCurrentProcessIdOfPcsQtyCollectionNewPcsQty.equals(currentProcess)) {
                        oldCurrentProcessIdOfPcsQtyCollectionNewPcsQty.getPcsQtyCollection().remove(pcsQtyCollectionNewPcsQty);
                        oldCurrentProcessIdOfPcsQtyCollectionNewPcsQty = em.merge(oldCurrentProcessIdOfPcsQtyCollectionNewPcsQty);
                    }
                }
            }
            for (McMove mcMoveCollectionNewMcMove : mcMoveCollectionNew) {
                if (!mcMoveCollectionOld.contains(mcMoveCollectionNewMcMove)) {
                    CurrentProcess oldCurrentProcessIdOfMcMoveCollectionNewMcMove = mcMoveCollectionNewMcMove.getCurrentProcessId();
                    mcMoveCollectionNewMcMove.setCurrentProcessId(currentProcess);
                    mcMoveCollectionNewMcMove = em.merge(mcMoveCollectionNewMcMove);
                    if (oldCurrentProcessIdOfMcMoveCollectionNewMcMove != null && !oldCurrentProcessIdOfMcMoveCollectionNewMcMove.equals(currentProcess)) {
                        oldCurrentProcessIdOfMcMoveCollectionNewMcMove.getMcMoveCollection().remove(mcMoveCollectionNewMcMove);
                        oldCurrentProcessIdOfMcMoveCollectionNewMcMove = em.merge(oldCurrentProcessIdOfMcMoveCollectionNewMcMove);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = currentProcess.getId();
                if (findCurrentProcess(id) == null) {
                    throw new NonexistentEntityException("The currentProcess with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CurrentProcess currentProcess;
            try {
                currentProcess = em.getReference(CurrentProcess.class, id);
                currentProcess.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The currentProcess with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<NgNormal> ngNormalCollectionOrphanCheck = currentProcess.getNgNormalCollection();
            for (NgNormal ngNormalCollectionOrphanCheckNgNormal : ngNormalCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This CurrentProcess (" + currentProcess + ") cannot be destroyed since the NgNormal " + ngNormalCollectionOrphanCheckNgNormal + " in its ngNormalCollection field has a non-nullable currentProcessId field.");
            }
            Collection<NgRepair> ngRepairCollectionOrphanCheck = currentProcess.getNgRepairCollection();
            for (NgRepair ngRepairCollectionOrphanCheckNgRepair : ngRepairCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This CurrentProcess (" + currentProcess + ") cannot be destroyed since the NgRepair " + ngRepairCollectionOrphanCheckNgRepair + " in its ngRepairCollection field has a non-nullable currentProcessId field.");
            }
            Collection<McDown> mcDownCollectionOrphanCheck = currentProcess.getMcDownCollection();
            for (McDown mcDownCollectionOrphanCheckMcDown : mcDownCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This CurrentProcess (" + currentProcess + ") cannot be destroyed since the McDown " + mcDownCollectionOrphanCheckMcDown + " in its mcDownCollection field has a non-nullable currentProcessId field.");
            }
            Collection<Hold> holdCollectionOrphanCheck = currentProcess.getHoldCollection();
            for (Hold holdCollectionOrphanCheckHold : holdCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This CurrentProcess (" + currentProcess + ") cannot be destroyed since the Hold " + holdCollectionOrphanCheckHold + " in its holdCollection field has a non-nullable currentProcessId field.");
            }
            Collection<PcsQty> pcsQtyCollectionOrphanCheck = currentProcess.getPcsQtyCollection();
            for (PcsQty pcsQtyCollectionOrphanCheckPcsQty : pcsQtyCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This CurrentProcess (" + currentProcess + ") cannot be destroyed since the PcsQty " + pcsQtyCollectionOrphanCheckPcsQty + " in its pcsQtyCollection field has a non-nullable currentProcessId field.");
            }
            Collection<McMove> mcMoveCollectionOrphanCheck = currentProcess.getMcMoveCollection();
            for (McMove mcMoveCollectionOrphanCheckMcMove : mcMoveCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This CurrentProcess (" + currentProcess + ") cannot be destroyed since the McMove " + mcMoveCollectionOrphanCheckMcMove + " in its mcMoveCollection field has a non-nullable currentProcessId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            MainData mainDataId = currentProcess.getMainDataId();
            if (mainDataId != null) {
                mainDataId.getCurrentProcessCollection().remove(currentProcess);
                mainDataId = em.merge(mainDataId);
            }
            Mc mcId = currentProcess.getMcId();
            if (mcId != null) {
                mcId.getCurrentProcessCollection().remove(currentProcess);
                mcId = em.merge(mcId);
            }
            em.remove(currentProcess);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CurrentProcess> findCurrentProcessEntities() {
        return findCurrentProcessEntities(true, -1, -1);
    }

    public List<CurrentProcess> findCurrentProcessEntities(int maxResults, int firstResult) {
        return findCurrentProcessEntities(false, maxResults, firstResult);
    }

    private List<CurrentProcess> findCurrentProcessEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CurrentProcess.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CurrentProcess findCurrentProcess(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CurrentProcess.class, id);
        } catch (Exception e) {
            return null;
        } finally {
            em.close();
        }
    }
    //---find main id

    public List<MainData> findMainDataId() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("SELECT c FROM MainData c", MainData.class);
            return q.getResultList();
        } finally {
            em.close();
        }

    }
    //---find staus 

    public List<CurrentProcessJpaController> findStatusCurrent(String idCurrent) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("CurrentProcess.findAll");
            return q.getResultList();
        } finally {
            em.close();
        }

    }

    public int getCurrentProcessCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CurrentProcess> rt = cq.from(CurrentProcess.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    //--find mainDataId

    public List<CurrentProcess> findCurrentStatus(MainData idMainData) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("CurrentProcess.findBymainDataId");
            q.setParameter("mainDataId", idMainData);
            return q.getResultList();
        } finally {
            em.close();
        }

    }
    //--find mainid  

    public List<CurrentProcess> findCurrentStatusAndOpen(MainData idMainData, String status) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("CurrentProcess.findBymainDataIdAndStatus");
            q.setParameter("mainDataId", idMainData);
            q.setParameter("status", status);
            return q.getResultList();
        } finally {
            em.close();
        }

    }
    //@pang

    public CurrentProcess findCurrenIdFinished(MainData idMainData) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("SELECT c FROM CurrentProcess c WHERE c.mainDataId =:idMainData and c.status ='Finished'");
            q.setParameter("idMainData", idMainData);
            return (CurrentProcess) q.getSingleResult();
        } finally {
            em.close();
        }
    }

    public CurrentProcess findCuurentDowntime(MainData mainDataId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("SELECT  c FROM CurrentProcess c  WHERE c.mainDataId =:idMainData and c.status ='Downtime' and c.currentProc = '1' ");
            q.setParameter("idMainData", mainDataId);
            return (CurrentProcess) q.getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
            em.close();
        }
    }

    public List<CurrentProcess> findCurrentProcessByTime(Date times) {
        EntityManager em = getEntityManager();
        try {
            String sql = "SELECT c FROM CurrentProcess c WHERE c.timeCurrent > :timeCurrent";
            Query query = em.createQuery(sql);
            query.setParameter("timeCurrent", times);
            return query.getResultList();
        } finally {
            em.close();
        }
    }

    public List<CurrentProcess> findCurrenntProcessByFristtimeAndLasttime(Date fristdate, Date lastdate) {
        EntityManager em = getEntityManager();
        try {
            String sql = "SELECT c FROM CurrentProcess c WHERE c.timeCurrent > :fristdate AND c.timeCurrent < :lastdate ORDER BY c.timeCurrent ASC";
            Query query = em.createQuery(sql);
            query.setParameter("fristdate", fristdate);
            query.setParameter("lastdate", lastdate);
            return query.getResultList();
        } finally {
            em.clear();
        }
    }

    public List<CurrentProcess> findCurrrentProcessByLine(String lineid) {
        EntityManager em = getEntityManager();
        try {
            String sql = "SELECT c FROM CurrentProcess c WHERE c.line = :line";
            Query query = em.createQuery(sql);
            query.setParameter("line", lineid);
            return query.getResultList();
        } finally {
            em.close();
        }
    }

    public List<CurrentProcess> findCurrentProcessByLine(Mc mc) {
        EntityManager em = getEntityManager();
        try {
            String sql = "SELECT c FROM CurrentProcess c WHERE c.mcId = :mcId";
            Query query = em.createQuery(sql);
            query.setParameter("mcId", mc);
            return query.getResultList();
        } finally {
            em.close();
        }
    }
    
    public List<CurrentProcess> findCurrentProcessByDateAndMc(Mc mc,Date fDate,Date lDate){
        EntityManager em = getEntityManager();
        try{
            String sql = "SELECT c FROM CurrentProcess c WHERE c.mcId = :mcId AND c.timeCurrent > :fristdate AND c.timeCurrent < :lastdate ORDER BY c.timeCurrent ASC";
            Query query = em.createQuery(sql);
            query.setParameter("mcId", mc);
            query.setParameter("fristdate", fDate);
            query.setParameter("lastdate", lDate);
            return query.getResultList();
        }finally{
            em.close();
        }
    }
    //find first process open time
    public CurrentProcess findFirstProcessOpen(MainData idMaindata)
    {
        EntityManager  em  =  getEntityManager();
        try
        {
            Query q  = em.createQuery("SELECT c From CurrentProcess c WHERE c.mainDataId=:idMaindata and c.firstOpen = 1");
            q.setParameter("idMaindata",idMaindata);
            return (CurrentProcess) q.getSingleResult();
        }
        finally
        {
            em.close();
        }
        
    }
}
