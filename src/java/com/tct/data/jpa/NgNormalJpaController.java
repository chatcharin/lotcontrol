/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.CurrentProcess;
import com.tct.data.NgNormal;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author The_Boy_Cs
 */
public class NgNormalJpaController implements Serializable {

    public NgNormalJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(NgNormal ngNormal) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CurrentProcess currentProcessId = ngNormal.getCurrentProcessId();
            if (currentProcessId != null) {
                currentProcessId = em.getReference(currentProcessId.getClass(), currentProcessId.getId());
                ngNormal.setCurrentProcessId(currentProcessId);
            }
            em.persist(ngNormal);
            if (currentProcessId != null) {
                currentProcessId.getNgNormalCollection().add(ngNormal);
                currentProcessId = em.merge(currentProcessId);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findNgNormal(ngNormal.getIdNg()) != null) {
                throw new PreexistingEntityException("NgNormal " + ngNormal + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(NgNormal ngNormal) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            NgNormal persistentNgNormal = em.find(NgNormal.class, ngNormal.getIdNg());
            CurrentProcess currentProcessIdOld = persistentNgNormal.getCurrentProcessId();
            CurrentProcess currentProcessIdNew = ngNormal.getCurrentProcessId();
            if (currentProcessIdNew != null) {
                currentProcessIdNew = em.getReference(currentProcessIdNew.getClass(), currentProcessIdNew.getId());
                ngNormal.setCurrentProcessId(currentProcessIdNew);
            }
            ngNormal = em.merge(ngNormal);
            if (currentProcessIdOld != null && !currentProcessIdOld.equals(currentProcessIdNew)) {
                currentProcessIdOld.getNgNormalCollection().remove(ngNormal);
                currentProcessIdOld = em.merge(currentProcessIdOld);
            }
            if (currentProcessIdNew != null && !currentProcessIdNew.equals(currentProcessIdOld)) {
                currentProcessIdNew.getNgNormalCollection().add(ngNormal);
                currentProcessIdNew = em.merge(currentProcessIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = ngNormal.getIdNg();
                if (findNgNormal(id) == null) {
                    throw new NonexistentEntityException("The ngNormal with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            NgNormal ngNormal;
            try {
                ngNormal = em.getReference(NgNormal.class, id);
                ngNormal.getIdNg();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ngNormal with id " + id + " no longer exists.", enfe);
            }
            CurrentProcess currentProcessId = ngNormal.getCurrentProcessId();
            if (currentProcessId != null) {
                currentProcessId.getNgNormalCollection().remove(ngNormal);
                currentProcessId = em.merge(currentProcessId);
            }
            em.remove(ngNormal);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<NgNormal> findNgNormalEntities() {
        return findNgNormalEntities(true, -1, -1);
    }

    public List<NgNormal> findNgNormalEntities(int maxResults, int firstResult) {
        return findNgNormalEntities(false, maxResults, firstResult);
    }

    private List<NgNormal> findNgNormalEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(NgNormal.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public NgNormal findNgNormal(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(NgNormal.class, id);
        } finally {
            em.close();
        }
    }

    public int getNgNormalCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<NgNormal> rt = cq.from(NgNormal.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    //============ Find  Ng mormal

    public List<NgNormal> findSumNg(CurrentProcess idCurrentProcess) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("NgNormal.findByCurrentProcessId");
            q.setParameter("currentProcessId", idCurrentProcess);
            return q.getResultList();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        } finally {
            em.close();
        }
    }

    public List<NgNormal> findAll() {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("NgNormal.findAll");
            return query.getResultList();
        } finally {
            em.close();
        }
    }

    public List<NgNormal> findByName(String name) {
        EntityManager em = getEntityManager();
        try {
            String sql = "SELECT n FROM NgNormal n WHERE n.name LIKE :name";
            Query query = em.createQuery(sql);
            query.setParameter("name", name + "%");
            return query.getResultList();
        } finally {
            em.clear();
        }
    }
}
