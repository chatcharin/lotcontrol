/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import com.tct.data.*;
import com.tct.data.jpa.exceptions.IllegalOrphanException;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author The_Boy_Cs
 */
public class DetailsJpaController implements Serializable {

    public DetailsJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Details details) throws PreexistingEntityException, Exception {
        if (details.getCopperWireInvoiceCollection() == null) {
            details.setCopperWireInvoiceCollection(new ArrayList<CopperWireInvoice>());
        }
        if (details.getDetailAssyCollection() == null) {
            details.setDetailAssyCollection(new ArrayList<DetailAssy>());
        }
        if (details.getFront2backMasterCollection() == null) {
            details.setFront2backMasterCollection(new ArrayList<Front2backMaster>());
        }
        if (details.getDetailAutoCollection() == null) {
            details.setDetailAutoCollection(new ArrayList<DetailAuto>());
        }
        if (details.getDetailPackingCollection() == null) {
            details.setDetailPackingCollection(new ArrayList<DetailPacking>());
        }
        if (details.getAssy2autoCollection() == null) {
            details.setAssy2autoCollection(new ArrayList<Assy2auto>());
        }
        if (details.getLineAssemblyCollection() == null) {
            details.setLineAssemblyCollection(new ArrayList<LineAssembly>());
        }
        if (details.getDetailFlCollection() == null) {
            details.setDetailFlCollection(new ArrayList<DetailFl>());
        }
        if (details.getFront2backCollection() == null) {
            details.setFront2backCollection(new ArrayList<Front2back>());
        }
        if (details.getPcb01Collection() == null) {
            details.setPcb01Collection(new ArrayList<Pcb01>());
        }
        if (details.getBack2packCollection() == null) {
            details.setBack2packCollection(new ArrayList<Back2pack>());
        }
        if (details.getRefu01Collection() == null) {
            details.setRefu01Collection(new ArrayList<Refu01>());
        }
        if (details.getLotControlCollection() == null) {
            details.setLotControlCollection(new ArrayList<LotControl>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<CopperWireInvoice> attachedCopperWireInvoiceCollection = new ArrayList<CopperWireInvoice>();
            for (CopperWireInvoice copperWireInvoiceCollectionCopperWireInvoiceToAttach : details.getCopperWireInvoiceCollection()) {
                copperWireInvoiceCollectionCopperWireInvoiceToAttach = em.getReference(copperWireInvoiceCollectionCopperWireInvoiceToAttach.getClass(), copperWireInvoiceCollectionCopperWireInvoiceToAttach.getId());
                attachedCopperWireInvoiceCollection.add(copperWireInvoiceCollectionCopperWireInvoiceToAttach);
            }
            details.setCopperWireInvoiceCollection(attachedCopperWireInvoiceCollection);
            Collection<DetailAssy> attachedDetailAssyCollection = new ArrayList<DetailAssy>();
            for (DetailAssy detailAssyCollectionDetailAssyToAttach : details.getDetailAssyCollection()) {
                detailAssyCollectionDetailAssyToAttach = em.getReference(detailAssyCollectionDetailAssyToAttach.getClass(), detailAssyCollectionDetailAssyToAttach.getId());
                attachedDetailAssyCollection.add(detailAssyCollectionDetailAssyToAttach);
            }
            details.setDetailAssyCollection(attachedDetailAssyCollection);
            Collection<Front2backMaster> attachedFront2backMasterCollection = new ArrayList<Front2backMaster>();
            for (Front2backMaster front2backMasterCollectionFront2backMasterToAttach : details.getFront2backMasterCollection()) {
                front2backMasterCollectionFront2backMasterToAttach = em.getReference(front2backMasterCollectionFront2backMasterToAttach.getClass(), front2backMasterCollectionFront2backMasterToAttach.getId());
                attachedFront2backMasterCollection.add(front2backMasterCollectionFront2backMasterToAttach);
            }
            details.setFront2backMasterCollection(attachedFront2backMasterCollection);
            Collection<DetailAuto> attachedDetailAutoCollection = new ArrayList<DetailAuto>();
            for (DetailAuto detailAutoCollectionDetailAutoToAttach : details.getDetailAutoCollection()) {
                detailAutoCollectionDetailAutoToAttach = em.getReference(detailAutoCollectionDetailAutoToAttach.getClass(), detailAutoCollectionDetailAutoToAttach.getId());
                attachedDetailAutoCollection.add(detailAutoCollectionDetailAutoToAttach);
            }
            details.setDetailAutoCollection(attachedDetailAutoCollection);
            Collection<DetailPacking> attachedDetailPackingCollection = new ArrayList<DetailPacking>();
            for (DetailPacking detailPackingCollectionDetailPackingToAttach : details.getDetailPackingCollection()) {
                detailPackingCollectionDetailPackingToAttach = em.getReference(detailPackingCollectionDetailPackingToAttach.getClass(), detailPackingCollectionDetailPackingToAttach.getId());
                attachedDetailPackingCollection.add(detailPackingCollectionDetailPackingToAttach);
            }
            details.setDetailPackingCollection(attachedDetailPackingCollection);
            Collection<Assy2auto> attachedAssy2autoCollection = new ArrayList<Assy2auto>();
            for (Assy2auto assy2autoCollectionAssy2autoToAttach : details.getAssy2autoCollection()) {
                assy2autoCollectionAssy2autoToAttach = em.getReference(assy2autoCollectionAssy2autoToAttach.getClass(), assy2autoCollectionAssy2autoToAttach.getId());
                attachedAssy2autoCollection.add(assy2autoCollectionAssy2autoToAttach);
            }
            details.setAssy2autoCollection(attachedAssy2autoCollection);
            Collection<LineAssembly> attachedLineAssemblyCollection = new ArrayList<LineAssembly>();
            for (LineAssembly lineAssemblyCollectionLineAssemblyToAttach : details.getLineAssemblyCollection()) {
                lineAssemblyCollectionLineAssemblyToAttach = em.getReference(lineAssemblyCollectionLineAssemblyToAttach.getClass(), lineAssemblyCollectionLineAssemblyToAttach.getId());
                attachedLineAssemblyCollection.add(lineAssemblyCollectionLineAssemblyToAttach);
            }
            details.setLineAssemblyCollection(attachedLineAssemblyCollection);
            Collection<DetailFl> attachedDetailFlCollection = new ArrayList<DetailFl>();
            for (DetailFl detailFlCollectionDetailFlToAttach : details.getDetailFlCollection()) {
                detailFlCollectionDetailFlToAttach = em.getReference(detailFlCollectionDetailFlToAttach.getClass(), detailFlCollectionDetailFlToAttach.getId());
                attachedDetailFlCollection.add(detailFlCollectionDetailFlToAttach);
            }
            details.setDetailFlCollection(attachedDetailFlCollection);
            Collection<Front2back> attachedFront2backCollection = new ArrayList<Front2back>();
            for (Front2back front2backCollectionFront2backToAttach : details.getFront2backCollection()) {
                front2backCollectionFront2backToAttach = em.getReference(front2backCollectionFront2backToAttach.getClass(), front2backCollectionFront2backToAttach.getFront2backId());
                attachedFront2backCollection.add(front2backCollectionFront2backToAttach);
            }
            details.setFront2backCollection(attachedFront2backCollection);
            Collection<Pcb01> attachedPcb01Collection = new ArrayList<Pcb01>();
            for (Pcb01 pcb01CollectionPcb01ToAttach : details.getPcb01Collection()) {
                pcb01CollectionPcb01ToAttach = em.getReference(pcb01CollectionPcb01ToAttach.getClass(), pcb01CollectionPcb01ToAttach.getId());
                attachedPcb01Collection.add(pcb01CollectionPcb01ToAttach);
            }
            details.setPcb01Collection(attachedPcb01Collection);
            Collection<Back2pack> attachedBack2packCollection = new ArrayList<Back2pack>();
            for (Back2pack back2packCollectionBack2packToAttach : details.getBack2packCollection()) {
                back2packCollectionBack2packToAttach = em.getReference(back2packCollectionBack2packToAttach.getClass(), back2packCollectionBack2packToAttach.getBack2packId());
                attachedBack2packCollection.add(back2packCollectionBack2packToAttach);
            }
            details.setBack2packCollection(attachedBack2packCollection);
            Collection<Refu01> attachedRefu01Collection = new ArrayList<Refu01>();
            for (Refu01 refu01CollectionRefu01ToAttach : details.getRefu01Collection()) {
                refu01CollectionRefu01ToAttach = em.getReference(refu01CollectionRefu01ToAttach.getClass(), refu01CollectionRefu01ToAttach.getId());
                attachedRefu01Collection.add(refu01CollectionRefu01ToAttach);
            }
            details.setRefu01Collection(attachedRefu01Collection);
            Collection<LotControl> attachedLotControlCollection = new ArrayList<LotControl>();
            for (LotControl lotControlCollectionLotControlToAttach : details.getLotControlCollection()) {
                lotControlCollectionLotControlToAttach = em.getReference(lotControlCollectionLotControlToAttach.getClass(), lotControlCollectionLotControlToAttach.getId());
                attachedLotControlCollection.add(lotControlCollectionLotControlToAttach);
            }
            details.setLotControlCollection(attachedLotControlCollection);
            em.persist(details);
            for (CopperWireInvoice copperWireInvoiceCollectionCopperWireInvoice : details.getCopperWireInvoiceCollection()) {
                Details oldDetailsIdDetailOfCopperWireInvoiceCollectionCopperWireInvoice = copperWireInvoiceCollectionCopperWireInvoice.getDetailsIdDetail();
                copperWireInvoiceCollectionCopperWireInvoice.setDetailsIdDetail(details);
                copperWireInvoiceCollectionCopperWireInvoice = em.merge(copperWireInvoiceCollectionCopperWireInvoice);
                if (oldDetailsIdDetailOfCopperWireInvoiceCollectionCopperWireInvoice != null) {
                    oldDetailsIdDetailOfCopperWireInvoiceCollectionCopperWireInvoice.getCopperWireInvoiceCollection().remove(copperWireInvoiceCollectionCopperWireInvoice);
                    oldDetailsIdDetailOfCopperWireInvoiceCollectionCopperWireInvoice = em.merge(oldDetailsIdDetailOfCopperWireInvoiceCollectionCopperWireInvoice);
                }
            }
            for (DetailAssy detailAssyCollectionDetailAssy : details.getDetailAssyCollection()) {
                Details oldDetailIdDetailOfDetailAssyCollectionDetailAssy = detailAssyCollectionDetailAssy.getDetailIdDetail();
                detailAssyCollectionDetailAssy.setDetailIdDetail(details);
                detailAssyCollectionDetailAssy = em.merge(detailAssyCollectionDetailAssy);
                if (oldDetailIdDetailOfDetailAssyCollectionDetailAssy != null) {
                    oldDetailIdDetailOfDetailAssyCollectionDetailAssy.getDetailAssyCollection().remove(detailAssyCollectionDetailAssy);
                    oldDetailIdDetailOfDetailAssyCollectionDetailAssy = em.merge(oldDetailIdDetailOfDetailAssyCollectionDetailAssy);
                }
            }
            for (Front2backMaster front2backMasterCollectionFront2backMaster : details.getFront2backMasterCollection()) {
                Details oldDetailsIdOfFront2backMasterCollectionFront2backMaster = front2backMasterCollectionFront2backMaster.getDetailsId();
                front2backMasterCollectionFront2backMaster.setDetailsId(details);
                front2backMasterCollectionFront2backMaster = em.merge(front2backMasterCollectionFront2backMaster);
                if (oldDetailsIdOfFront2backMasterCollectionFront2backMaster != null) {
                    oldDetailsIdOfFront2backMasterCollectionFront2backMaster.getFront2backMasterCollection().remove(front2backMasterCollectionFront2backMaster);
                    oldDetailsIdOfFront2backMasterCollectionFront2backMaster = em.merge(oldDetailsIdOfFront2backMasterCollectionFront2backMaster);
                }
            }
            for (DetailAuto detailAutoCollectionDetailAuto : details.getDetailAutoCollection()) {
                Details oldDetailIdDetailOfDetailAutoCollectionDetailAuto = detailAutoCollectionDetailAuto.getDetailIdDetail();
                detailAutoCollectionDetailAuto.setDetailIdDetail(details);
                detailAutoCollectionDetailAuto = em.merge(detailAutoCollectionDetailAuto);
                if (oldDetailIdDetailOfDetailAutoCollectionDetailAuto != null) {
                    oldDetailIdDetailOfDetailAutoCollectionDetailAuto.getDetailAutoCollection().remove(detailAutoCollectionDetailAuto);
                    oldDetailIdDetailOfDetailAutoCollectionDetailAuto = em.merge(oldDetailIdDetailOfDetailAutoCollectionDetailAuto);
                }
            }
            for (DetailPacking detailPackingCollectionDetailPacking : details.getDetailPackingCollection()) {
                Details oldDetailsIdDetailOfDetailPackingCollectionDetailPacking = detailPackingCollectionDetailPacking.getDetailsIdDetail();
                detailPackingCollectionDetailPacking.setDetailsIdDetail(details);
                detailPackingCollectionDetailPacking = em.merge(detailPackingCollectionDetailPacking);
                if (oldDetailsIdDetailOfDetailPackingCollectionDetailPacking != null) {
                    oldDetailsIdDetailOfDetailPackingCollectionDetailPacking.getDetailPackingCollection().remove(detailPackingCollectionDetailPacking);
                    oldDetailsIdDetailOfDetailPackingCollectionDetailPacking = em.merge(oldDetailsIdDetailOfDetailPackingCollectionDetailPacking);
                }
            }
            for (Assy2auto assy2autoCollectionAssy2auto : details.getAssy2autoCollection()) {
                Details oldDetailIdOfAssy2autoCollectionAssy2auto = assy2autoCollectionAssy2auto.getDetailId();
                assy2autoCollectionAssy2auto.setDetailId(details);
                assy2autoCollectionAssy2auto = em.merge(assy2autoCollectionAssy2auto);
                if (oldDetailIdOfAssy2autoCollectionAssy2auto != null) {
                    oldDetailIdOfAssy2autoCollectionAssy2auto.getAssy2autoCollection().remove(assy2autoCollectionAssy2auto);
                    oldDetailIdOfAssy2autoCollectionAssy2auto = em.merge(oldDetailIdOfAssy2autoCollectionAssy2auto);
                }
            }
            for (LineAssembly lineAssemblyCollectionLineAssembly : details.getLineAssemblyCollection()) {
                Details oldDetailIdDetailOfLineAssemblyCollectionLineAssembly = lineAssemblyCollectionLineAssembly.getDetailIdDetail();
                lineAssemblyCollectionLineAssembly.setDetailIdDetail(details);
                lineAssemblyCollectionLineAssembly = em.merge(lineAssemblyCollectionLineAssembly);
                if (oldDetailIdDetailOfLineAssemblyCollectionLineAssembly != null) {
                    oldDetailIdDetailOfLineAssemblyCollectionLineAssembly.getLineAssemblyCollection().remove(lineAssemblyCollectionLineAssembly);
                    oldDetailIdDetailOfLineAssemblyCollectionLineAssembly = em.merge(oldDetailIdDetailOfLineAssemblyCollectionLineAssembly);
                }
            }
            for (DetailFl detailFlCollectionDetailFl : details.getDetailFlCollection()) {
                Details oldDetailIdDetailOfDetailFlCollectionDetailFl = detailFlCollectionDetailFl.getDetailIdDetail();
                detailFlCollectionDetailFl.setDetailIdDetail(details);
                detailFlCollectionDetailFl = em.merge(detailFlCollectionDetailFl);
                if (oldDetailIdDetailOfDetailFlCollectionDetailFl != null) {
                    oldDetailIdDetailOfDetailFlCollectionDetailFl.getDetailFlCollection().remove(detailFlCollectionDetailFl);
                    oldDetailIdDetailOfDetailFlCollectionDetailFl = em.merge(oldDetailIdDetailOfDetailFlCollectionDetailFl);
                }
            }
            for (Front2back front2backCollectionFront2back : details.getFront2backCollection()) {
                Details oldDetailsIdDetailOfFront2backCollectionFront2back = front2backCollectionFront2back.getDetailsIdDetail();
                front2backCollectionFront2back.setDetailsIdDetail(details);
                front2backCollectionFront2back = em.merge(front2backCollectionFront2back);
                if (oldDetailsIdDetailOfFront2backCollectionFront2back != null) {
                    oldDetailsIdDetailOfFront2backCollectionFront2back.getFront2backCollection().remove(front2backCollectionFront2back);
                    oldDetailsIdDetailOfFront2backCollectionFront2back = em.merge(oldDetailsIdDetailOfFront2backCollectionFront2back);
                }
            }
            for (Pcb01 pcb01CollectionPcb01 : details.getPcb01Collection()) {
                Details oldDetailIdDetailOfPcb01CollectionPcb01 = pcb01CollectionPcb01.getDetailIdDetail();
                pcb01CollectionPcb01.setDetailIdDetail(details);
                pcb01CollectionPcb01 = em.merge(pcb01CollectionPcb01);
                if (oldDetailIdDetailOfPcb01CollectionPcb01 != null) {
                    oldDetailIdDetailOfPcb01CollectionPcb01.getPcb01Collection().remove(pcb01CollectionPcb01);
                    oldDetailIdDetailOfPcb01CollectionPcb01 = em.merge(oldDetailIdDetailOfPcb01CollectionPcb01);
                }
            }
            for (Back2pack back2packCollectionBack2pack : details.getBack2packCollection()) {
                Details oldDetailsIdDetailOfBack2packCollectionBack2pack = back2packCollectionBack2pack.getDetailsIdDetail();
                back2packCollectionBack2pack.setDetailsIdDetail(details);
                back2packCollectionBack2pack = em.merge(back2packCollectionBack2pack);
                if (oldDetailsIdDetailOfBack2packCollectionBack2pack != null) {
                    oldDetailsIdDetailOfBack2packCollectionBack2pack.getBack2packCollection().remove(back2packCollectionBack2pack);
                    oldDetailsIdDetailOfBack2packCollectionBack2pack = em.merge(oldDetailsIdDetailOfBack2packCollectionBack2pack);
                }
            }
            for (Refu01 refu01CollectionRefu01 : details.getRefu01Collection()) {
                Details oldDetailIdDetailOfRefu01CollectionRefu01 = refu01CollectionRefu01.getDetailIdDetail();
                refu01CollectionRefu01.setDetailIdDetail(details);
                refu01CollectionRefu01 = em.merge(refu01CollectionRefu01);
                if (oldDetailIdDetailOfRefu01CollectionRefu01 != null) {
                    oldDetailIdDetailOfRefu01CollectionRefu01.getRefu01Collection().remove(refu01CollectionRefu01);
                    oldDetailIdDetailOfRefu01CollectionRefu01 = em.merge(oldDetailIdDetailOfRefu01CollectionRefu01);
                }
            }
            for (LotControl lotControlCollectionLotControl : details.getLotControlCollection()) {
                Details oldDetailsIdDetailOfLotControlCollectionLotControl = lotControlCollectionLotControl.getDetailsIdDetail();
                lotControlCollectionLotControl.setDetailsIdDetail(details);
                lotControlCollectionLotControl = em.merge(lotControlCollectionLotControl);
                if (oldDetailsIdDetailOfLotControlCollectionLotControl != null) {
                    oldDetailsIdDetailOfLotControlCollectionLotControl.getLotControlCollection().remove(lotControlCollectionLotControl);
                    oldDetailsIdDetailOfLotControlCollectionLotControl = em.merge(oldDetailsIdDetailOfLotControlCollectionLotControl);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDetails(details.getIdDetail()) != null) {
                throw new PreexistingEntityException("Details " + details + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Details details) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Details persistentDetails = em.find(Details.class, details.getIdDetail());
            Collection<CopperWireInvoice> copperWireInvoiceCollectionOld = persistentDetails.getCopperWireInvoiceCollection();
            Collection<CopperWireInvoice> copperWireInvoiceCollectionNew = details.getCopperWireInvoiceCollection();
            Collection<DetailAssy> detailAssyCollectionOld = persistentDetails.getDetailAssyCollection();
            Collection<DetailAssy> detailAssyCollectionNew = details.getDetailAssyCollection();
            Collection<Front2backMaster> front2backMasterCollectionOld = persistentDetails.getFront2backMasterCollection();
            Collection<Front2backMaster> front2backMasterCollectionNew = details.getFront2backMasterCollection();
            Collection<DetailAuto> detailAutoCollectionOld = persistentDetails.getDetailAutoCollection();
            Collection<DetailAuto> detailAutoCollectionNew = details.getDetailAutoCollection();
            Collection<DetailPacking> detailPackingCollectionOld = persistentDetails.getDetailPackingCollection();
            Collection<DetailPacking> detailPackingCollectionNew = details.getDetailPackingCollection();
            Collection<Assy2auto> assy2autoCollectionOld = persistentDetails.getAssy2autoCollection();
            Collection<Assy2auto> assy2autoCollectionNew = details.getAssy2autoCollection();
            Collection<LineAssembly> lineAssemblyCollectionOld = persistentDetails.getLineAssemblyCollection();
            Collection<LineAssembly> lineAssemblyCollectionNew = details.getLineAssemblyCollection();
            Collection<DetailFl> detailFlCollectionOld = persistentDetails.getDetailFlCollection();
            Collection<DetailFl> detailFlCollectionNew = details.getDetailFlCollection();
            Collection<Front2back> front2backCollectionOld = persistentDetails.getFront2backCollection();
            Collection<Front2back> front2backCollectionNew = details.getFront2backCollection();
            Collection<Pcb01> pcb01CollectionOld = persistentDetails.getPcb01Collection();
            Collection<Pcb01> pcb01CollectionNew = details.getPcb01Collection();
            Collection<Back2pack> back2packCollectionOld = persistentDetails.getBack2packCollection();
            Collection<Back2pack> back2packCollectionNew = details.getBack2packCollection();
            Collection<Refu01> refu01CollectionOld = persistentDetails.getRefu01Collection();
            Collection<Refu01> refu01CollectionNew = details.getRefu01Collection();
            Collection<LotControl> lotControlCollectionOld = persistentDetails.getLotControlCollection();
            Collection<LotControl> lotControlCollectionNew = details.getLotControlCollection();
            List<String> illegalOrphanMessages = null;
            for (CopperWireInvoice copperWireInvoiceCollectionOldCopperWireInvoice : copperWireInvoiceCollectionOld) {
                if (!copperWireInvoiceCollectionNew.contains(copperWireInvoiceCollectionOldCopperWireInvoice)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain CopperWireInvoice " + copperWireInvoiceCollectionOldCopperWireInvoice + " since its detailsIdDetail field is not nullable.");
                }
            }
            for (DetailAssy detailAssyCollectionOldDetailAssy : detailAssyCollectionOld) {
                if (!detailAssyCollectionNew.contains(detailAssyCollectionOldDetailAssy)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain DetailAssy " + detailAssyCollectionOldDetailAssy + " since its detailIdDetail field is not nullable.");
                }
            }
            for (Front2backMaster front2backMasterCollectionOldFront2backMaster : front2backMasterCollectionOld) {
                if (!front2backMasterCollectionNew.contains(front2backMasterCollectionOldFront2backMaster)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Front2backMaster " + front2backMasterCollectionOldFront2backMaster + " since its detailsId field is not nullable.");
                }
            }
            for (DetailAuto detailAutoCollectionOldDetailAuto : detailAutoCollectionOld) {
                if (!detailAutoCollectionNew.contains(detailAutoCollectionOldDetailAuto)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain DetailAuto " + detailAutoCollectionOldDetailAuto + " since its detailIdDetail field is not nullable.");
                }
            }
            for (DetailPacking detailPackingCollectionOldDetailPacking : detailPackingCollectionOld) {
                if (!detailPackingCollectionNew.contains(detailPackingCollectionOldDetailPacking)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain DetailPacking " + detailPackingCollectionOldDetailPacking + " since its detailsIdDetail field is not nullable.");
                }
            }
            for (Assy2auto assy2autoCollectionOldAssy2auto : assy2autoCollectionOld) {
                if (!assy2autoCollectionNew.contains(assy2autoCollectionOldAssy2auto)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Assy2auto " + assy2autoCollectionOldAssy2auto + " since its detailId field is not nullable.");
                }
            }
            for (LineAssembly lineAssemblyCollectionOldLineAssembly : lineAssemblyCollectionOld) {
                if (!lineAssemblyCollectionNew.contains(lineAssemblyCollectionOldLineAssembly)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain LineAssembly " + lineAssemblyCollectionOldLineAssembly + " since its detailIdDetail field is not nullable.");
                }
            }
            for (DetailFl detailFlCollectionOldDetailFl : detailFlCollectionOld) {
                if (!detailFlCollectionNew.contains(detailFlCollectionOldDetailFl)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain DetailFl " + detailFlCollectionOldDetailFl + " since its detailIdDetail field is not nullable.");
                }
            }
            for (Front2back front2backCollectionOldFront2back : front2backCollectionOld) {
                if (!front2backCollectionNew.contains(front2backCollectionOldFront2back)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Front2back " + front2backCollectionOldFront2back + " since its detailsIdDetail field is not nullable.");
                }
            }
            for (Pcb01 pcb01CollectionOldPcb01 : pcb01CollectionOld) {
                if (!pcb01CollectionNew.contains(pcb01CollectionOldPcb01)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Pcb01 " + pcb01CollectionOldPcb01 + " since its detailIdDetail field is not nullable.");
                }
            }
            for (Back2pack back2packCollectionOldBack2pack : back2packCollectionOld) {
                if (!back2packCollectionNew.contains(back2packCollectionOldBack2pack)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Back2pack " + back2packCollectionOldBack2pack + " since its detailsIdDetail field is not nullable.");
                }
            }
            for (Refu01 refu01CollectionOldRefu01 : refu01CollectionOld) {
                if (!refu01CollectionNew.contains(refu01CollectionOldRefu01)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Refu01 " + refu01CollectionOldRefu01 + " since its detailIdDetail field is not nullable.");
                }
            }
            for (LotControl lotControlCollectionOldLotControl : lotControlCollectionOld) {
                if (!lotControlCollectionNew.contains(lotControlCollectionOldLotControl)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain LotControl " + lotControlCollectionOldLotControl + " since its detailsIdDetail field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<CopperWireInvoice> attachedCopperWireInvoiceCollectionNew = new ArrayList<CopperWireInvoice>();
            for (CopperWireInvoice copperWireInvoiceCollectionNewCopperWireInvoiceToAttach : copperWireInvoiceCollectionNew) {
                copperWireInvoiceCollectionNewCopperWireInvoiceToAttach = em.getReference(copperWireInvoiceCollectionNewCopperWireInvoiceToAttach.getClass(), copperWireInvoiceCollectionNewCopperWireInvoiceToAttach.getId());
                attachedCopperWireInvoiceCollectionNew.add(copperWireInvoiceCollectionNewCopperWireInvoiceToAttach);
            }
            copperWireInvoiceCollectionNew = attachedCopperWireInvoiceCollectionNew;
            details.setCopperWireInvoiceCollection(copperWireInvoiceCollectionNew);
            Collection<DetailAssy> attachedDetailAssyCollectionNew = new ArrayList<DetailAssy>();
            for (DetailAssy detailAssyCollectionNewDetailAssyToAttach : detailAssyCollectionNew) {
                detailAssyCollectionNewDetailAssyToAttach = em.getReference(detailAssyCollectionNewDetailAssyToAttach.getClass(), detailAssyCollectionNewDetailAssyToAttach.getId());
                attachedDetailAssyCollectionNew.add(detailAssyCollectionNewDetailAssyToAttach);
            }
            detailAssyCollectionNew = attachedDetailAssyCollectionNew;
            details.setDetailAssyCollection(detailAssyCollectionNew);
            Collection<Front2backMaster> attachedFront2backMasterCollectionNew = new ArrayList<Front2backMaster>();
            for (Front2backMaster front2backMasterCollectionNewFront2backMasterToAttach : front2backMasterCollectionNew) {
                front2backMasterCollectionNewFront2backMasterToAttach = em.getReference(front2backMasterCollectionNewFront2backMasterToAttach.getClass(), front2backMasterCollectionNewFront2backMasterToAttach.getId());
                attachedFront2backMasterCollectionNew.add(front2backMasterCollectionNewFront2backMasterToAttach);
            }
            front2backMasterCollectionNew = attachedFront2backMasterCollectionNew;
            details.setFront2backMasterCollection(front2backMasterCollectionNew);
            Collection<DetailAuto> attachedDetailAutoCollectionNew = new ArrayList<DetailAuto>();
            for (DetailAuto detailAutoCollectionNewDetailAutoToAttach : detailAutoCollectionNew) {
                detailAutoCollectionNewDetailAutoToAttach = em.getReference(detailAutoCollectionNewDetailAutoToAttach.getClass(), detailAutoCollectionNewDetailAutoToAttach.getId());
                attachedDetailAutoCollectionNew.add(detailAutoCollectionNewDetailAutoToAttach);
            }
            detailAutoCollectionNew = attachedDetailAutoCollectionNew;
            details.setDetailAutoCollection(detailAutoCollectionNew);
            Collection<DetailPacking> attachedDetailPackingCollectionNew = new ArrayList<DetailPacking>();
            for (DetailPacking detailPackingCollectionNewDetailPackingToAttach : detailPackingCollectionNew) {
                detailPackingCollectionNewDetailPackingToAttach = em.getReference(detailPackingCollectionNewDetailPackingToAttach.getClass(), detailPackingCollectionNewDetailPackingToAttach.getId());
                attachedDetailPackingCollectionNew.add(detailPackingCollectionNewDetailPackingToAttach);
            }
            detailPackingCollectionNew = attachedDetailPackingCollectionNew;
            details.setDetailPackingCollection(detailPackingCollectionNew);
            Collection<Assy2auto> attachedAssy2autoCollectionNew = new ArrayList<Assy2auto>();
            for (Assy2auto assy2autoCollectionNewAssy2autoToAttach : assy2autoCollectionNew) {
                assy2autoCollectionNewAssy2autoToAttach = em.getReference(assy2autoCollectionNewAssy2autoToAttach.getClass(), assy2autoCollectionNewAssy2autoToAttach.getId());
                attachedAssy2autoCollectionNew.add(assy2autoCollectionNewAssy2autoToAttach);
            }
            assy2autoCollectionNew = attachedAssy2autoCollectionNew;
            details.setAssy2autoCollection(assy2autoCollectionNew);
            Collection<LineAssembly> attachedLineAssemblyCollectionNew = new ArrayList<LineAssembly>();
            for (LineAssembly lineAssemblyCollectionNewLineAssemblyToAttach : lineAssemblyCollectionNew) {
                lineAssemblyCollectionNewLineAssemblyToAttach = em.getReference(lineAssemblyCollectionNewLineAssemblyToAttach.getClass(), lineAssemblyCollectionNewLineAssemblyToAttach.getId());
                attachedLineAssemblyCollectionNew.add(lineAssemblyCollectionNewLineAssemblyToAttach);
            }
            lineAssemblyCollectionNew = attachedLineAssemblyCollectionNew;
            details.setLineAssemblyCollection(lineAssemblyCollectionNew);
            Collection<DetailFl> attachedDetailFlCollectionNew = new ArrayList<DetailFl>();
            for (DetailFl detailFlCollectionNewDetailFlToAttach : detailFlCollectionNew) {
                detailFlCollectionNewDetailFlToAttach = em.getReference(detailFlCollectionNewDetailFlToAttach.getClass(), detailFlCollectionNewDetailFlToAttach.getId());
                attachedDetailFlCollectionNew.add(detailFlCollectionNewDetailFlToAttach);
            }
            detailFlCollectionNew = attachedDetailFlCollectionNew;
            details.setDetailFlCollection(detailFlCollectionNew);
            Collection<Front2back> attachedFront2backCollectionNew = new ArrayList<Front2back>();
            for (Front2back front2backCollectionNewFront2backToAttach : front2backCollectionNew) {
                front2backCollectionNewFront2backToAttach = em.getReference(front2backCollectionNewFront2backToAttach.getClass(), front2backCollectionNewFront2backToAttach.getFront2backId());
                attachedFront2backCollectionNew.add(front2backCollectionNewFront2backToAttach);
            }
            front2backCollectionNew = attachedFront2backCollectionNew;
            details.setFront2backCollection(front2backCollectionNew);
            Collection<Pcb01> attachedPcb01CollectionNew = new ArrayList<Pcb01>();
            for (Pcb01 pcb01CollectionNewPcb01ToAttach : pcb01CollectionNew) {
                pcb01CollectionNewPcb01ToAttach = em.getReference(pcb01CollectionNewPcb01ToAttach.getClass(), pcb01CollectionNewPcb01ToAttach.getId());
                attachedPcb01CollectionNew.add(pcb01CollectionNewPcb01ToAttach);
            }
            pcb01CollectionNew = attachedPcb01CollectionNew;
            details.setPcb01Collection(pcb01CollectionNew);
            Collection<Back2pack> attachedBack2packCollectionNew = new ArrayList<Back2pack>();
            for (Back2pack back2packCollectionNewBack2packToAttach : back2packCollectionNew) {
                back2packCollectionNewBack2packToAttach = em.getReference(back2packCollectionNewBack2packToAttach.getClass(), back2packCollectionNewBack2packToAttach.getBack2packId());
                attachedBack2packCollectionNew.add(back2packCollectionNewBack2packToAttach);
            }
            back2packCollectionNew = attachedBack2packCollectionNew;
            details.setBack2packCollection(back2packCollectionNew);
            Collection<Refu01> attachedRefu01CollectionNew = new ArrayList<Refu01>();
            for (Refu01 refu01CollectionNewRefu01ToAttach : refu01CollectionNew) {
                refu01CollectionNewRefu01ToAttach = em.getReference(refu01CollectionNewRefu01ToAttach.getClass(), refu01CollectionNewRefu01ToAttach.getId());
                attachedRefu01CollectionNew.add(refu01CollectionNewRefu01ToAttach);
            }
            refu01CollectionNew = attachedRefu01CollectionNew;
            details.setRefu01Collection(refu01CollectionNew);
            Collection<LotControl> attachedLotControlCollectionNew = new ArrayList<LotControl>();
            for (LotControl lotControlCollectionNewLotControlToAttach : lotControlCollectionNew) {
                lotControlCollectionNewLotControlToAttach = em.getReference(lotControlCollectionNewLotControlToAttach.getClass(), lotControlCollectionNewLotControlToAttach.getId());
                attachedLotControlCollectionNew.add(lotControlCollectionNewLotControlToAttach);
            }
            lotControlCollectionNew = attachedLotControlCollectionNew;
            details.setLotControlCollection(lotControlCollectionNew);
            details = em.merge(details);
            for (CopperWireInvoice copperWireInvoiceCollectionNewCopperWireInvoice : copperWireInvoiceCollectionNew) {
                if (!copperWireInvoiceCollectionOld.contains(copperWireInvoiceCollectionNewCopperWireInvoice)) {
                    Details oldDetailsIdDetailOfCopperWireInvoiceCollectionNewCopperWireInvoice = copperWireInvoiceCollectionNewCopperWireInvoice.getDetailsIdDetail();
                    copperWireInvoiceCollectionNewCopperWireInvoice.setDetailsIdDetail(details);
                    copperWireInvoiceCollectionNewCopperWireInvoice = em.merge(copperWireInvoiceCollectionNewCopperWireInvoice);
                    if (oldDetailsIdDetailOfCopperWireInvoiceCollectionNewCopperWireInvoice != null && !oldDetailsIdDetailOfCopperWireInvoiceCollectionNewCopperWireInvoice.equals(details)) {
                        oldDetailsIdDetailOfCopperWireInvoiceCollectionNewCopperWireInvoice.getCopperWireInvoiceCollection().remove(copperWireInvoiceCollectionNewCopperWireInvoice);
                        oldDetailsIdDetailOfCopperWireInvoiceCollectionNewCopperWireInvoice = em.merge(oldDetailsIdDetailOfCopperWireInvoiceCollectionNewCopperWireInvoice);
                    }
                }
            }
            for (DetailAssy detailAssyCollectionNewDetailAssy : detailAssyCollectionNew) {
                if (!detailAssyCollectionOld.contains(detailAssyCollectionNewDetailAssy)) {
                    Details oldDetailIdDetailOfDetailAssyCollectionNewDetailAssy = detailAssyCollectionNewDetailAssy.getDetailIdDetail();
                    detailAssyCollectionNewDetailAssy.setDetailIdDetail(details);
                    detailAssyCollectionNewDetailAssy = em.merge(detailAssyCollectionNewDetailAssy);
                    if (oldDetailIdDetailOfDetailAssyCollectionNewDetailAssy != null && !oldDetailIdDetailOfDetailAssyCollectionNewDetailAssy.equals(details)) {
                        oldDetailIdDetailOfDetailAssyCollectionNewDetailAssy.getDetailAssyCollection().remove(detailAssyCollectionNewDetailAssy);
                        oldDetailIdDetailOfDetailAssyCollectionNewDetailAssy = em.merge(oldDetailIdDetailOfDetailAssyCollectionNewDetailAssy);
                    }
                }
            }
            for (Front2backMaster front2backMasterCollectionNewFront2backMaster : front2backMasterCollectionNew) {
                if (!front2backMasterCollectionOld.contains(front2backMasterCollectionNewFront2backMaster)) {
                    Details oldDetailsIdOfFront2backMasterCollectionNewFront2backMaster = front2backMasterCollectionNewFront2backMaster.getDetailsId();
                    front2backMasterCollectionNewFront2backMaster.setDetailsId(details);
                    front2backMasterCollectionNewFront2backMaster = em.merge(front2backMasterCollectionNewFront2backMaster);
                    if (oldDetailsIdOfFront2backMasterCollectionNewFront2backMaster != null && !oldDetailsIdOfFront2backMasterCollectionNewFront2backMaster.equals(details)) {
                        oldDetailsIdOfFront2backMasterCollectionNewFront2backMaster.getFront2backMasterCollection().remove(front2backMasterCollectionNewFront2backMaster);
                        oldDetailsIdOfFront2backMasterCollectionNewFront2backMaster = em.merge(oldDetailsIdOfFront2backMasterCollectionNewFront2backMaster);
                    }
                }
            }
            for (DetailAuto detailAutoCollectionNewDetailAuto : detailAutoCollectionNew) {
                if (!detailAutoCollectionOld.contains(detailAutoCollectionNewDetailAuto)) {
                    Details oldDetailIdDetailOfDetailAutoCollectionNewDetailAuto = detailAutoCollectionNewDetailAuto.getDetailIdDetail();
                    detailAutoCollectionNewDetailAuto.setDetailIdDetail(details);
                    detailAutoCollectionNewDetailAuto = em.merge(detailAutoCollectionNewDetailAuto);
                    if (oldDetailIdDetailOfDetailAutoCollectionNewDetailAuto != null && !oldDetailIdDetailOfDetailAutoCollectionNewDetailAuto.equals(details)) {
                        oldDetailIdDetailOfDetailAutoCollectionNewDetailAuto.getDetailAutoCollection().remove(detailAutoCollectionNewDetailAuto);
                        oldDetailIdDetailOfDetailAutoCollectionNewDetailAuto = em.merge(oldDetailIdDetailOfDetailAutoCollectionNewDetailAuto);
                    }
                }
            }
            for (DetailPacking detailPackingCollectionNewDetailPacking : detailPackingCollectionNew) {
                if (!detailPackingCollectionOld.contains(detailPackingCollectionNewDetailPacking)) {
                    Details oldDetailsIdDetailOfDetailPackingCollectionNewDetailPacking = detailPackingCollectionNewDetailPacking.getDetailsIdDetail();
                    detailPackingCollectionNewDetailPacking.setDetailsIdDetail(details);
                    detailPackingCollectionNewDetailPacking = em.merge(detailPackingCollectionNewDetailPacking);
                    if (oldDetailsIdDetailOfDetailPackingCollectionNewDetailPacking != null && !oldDetailsIdDetailOfDetailPackingCollectionNewDetailPacking.equals(details)) {
                        oldDetailsIdDetailOfDetailPackingCollectionNewDetailPacking.getDetailPackingCollection().remove(detailPackingCollectionNewDetailPacking);
                        oldDetailsIdDetailOfDetailPackingCollectionNewDetailPacking = em.merge(oldDetailsIdDetailOfDetailPackingCollectionNewDetailPacking);
                    }
                }
            }
            for (Assy2auto assy2autoCollectionNewAssy2auto : assy2autoCollectionNew) {
                if (!assy2autoCollectionOld.contains(assy2autoCollectionNewAssy2auto)) {
                    Details oldDetailIdOfAssy2autoCollectionNewAssy2auto = assy2autoCollectionNewAssy2auto.getDetailId();
                    assy2autoCollectionNewAssy2auto.setDetailId(details);
                    assy2autoCollectionNewAssy2auto = em.merge(assy2autoCollectionNewAssy2auto);
                    if (oldDetailIdOfAssy2autoCollectionNewAssy2auto != null && !oldDetailIdOfAssy2autoCollectionNewAssy2auto.equals(details)) {
                        oldDetailIdOfAssy2autoCollectionNewAssy2auto.getAssy2autoCollection().remove(assy2autoCollectionNewAssy2auto);
                        oldDetailIdOfAssy2autoCollectionNewAssy2auto = em.merge(oldDetailIdOfAssy2autoCollectionNewAssy2auto);
                    }
                }
            }
            for (LineAssembly lineAssemblyCollectionNewLineAssembly : lineAssemblyCollectionNew) {
                if (!lineAssemblyCollectionOld.contains(lineAssemblyCollectionNewLineAssembly)) {
                    Details oldDetailIdDetailOfLineAssemblyCollectionNewLineAssembly = lineAssemblyCollectionNewLineAssembly.getDetailIdDetail();
                    lineAssemblyCollectionNewLineAssembly.setDetailIdDetail(details);
                    lineAssemblyCollectionNewLineAssembly = em.merge(lineAssemblyCollectionNewLineAssembly);
                    if (oldDetailIdDetailOfLineAssemblyCollectionNewLineAssembly != null && !oldDetailIdDetailOfLineAssemblyCollectionNewLineAssembly.equals(details)) {
                        oldDetailIdDetailOfLineAssemblyCollectionNewLineAssembly.getLineAssemblyCollection().remove(lineAssemblyCollectionNewLineAssembly);
                        oldDetailIdDetailOfLineAssemblyCollectionNewLineAssembly = em.merge(oldDetailIdDetailOfLineAssemblyCollectionNewLineAssembly);
                    }
                }
            }
            for (DetailFl detailFlCollectionNewDetailFl : detailFlCollectionNew) {
                if (!detailFlCollectionOld.contains(detailFlCollectionNewDetailFl)) {
                    Details oldDetailIdDetailOfDetailFlCollectionNewDetailFl = detailFlCollectionNewDetailFl.getDetailIdDetail();
                    detailFlCollectionNewDetailFl.setDetailIdDetail(details);
                    detailFlCollectionNewDetailFl = em.merge(detailFlCollectionNewDetailFl);
                    if (oldDetailIdDetailOfDetailFlCollectionNewDetailFl != null && !oldDetailIdDetailOfDetailFlCollectionNewDetailFl.equals(details)) {
                        oldDetailIdDetailOfDetailFlCollectionNewDetailFl.getDetailFlCollection().remove(detailFlCollectionNewDetailFl);
                        oldDetailIdDetailOfDetailFlCollectionNewDetailFl = em.merge(oldDetailIdDetailOfDetailFlCollectionNewDetailFl);
                    }
                }
            }
            for (Front2back front2backCollectionNewFront2back : front2backCollectionNew) {
                if (!front2backCollectionOld.contains(front2backCollectionNewFront2back)) {
                    Details oldDetailsIdDetailOfFront2backCollectionNewFront2back = front2backCollectionNewFront2back.getDetailsIdDetail();
                    front2backCollectionNewFront2back.setDetailsIdDetail(details);
                    front2backCollectionNewFront2back = em.merge(front2backCollectionNewFront2back);
                    if (oldDetailsIdDetailOfFront2backCollectionNewFront2back != null && !oldDetailsIdDetailOfFront2backCollectionNewFront2back.equals(details)) {
                        oldDetailsIdDetailOfFront2backCollectionNewFront2back.getFront2backCollection().remove(front2backCollectionNewFront2back);
                        oldDetailsIdDetailOfFront2backCollectionNewFront2back = em.merge(oldDetailsIdDetailOfFront2backCollectionNewFront2back);
                    }
                }
            }
            for (Pcb01 pcb01CollectionNewPcb01 : pcb01CollectionNew) {
                if (!pcb01CollectionOld.contains(pcb01CollectionNewPcb01)) {
                    Details oldDetailIdDetailOfPcb01CollectionNewPcb01 = pcb01CollectionNewPcb01.getDetailIdDetail();
                    pcb01CollectionNewPcb01.setDetailIdDetail(details);
                    pcb01CollectionNewPcb01 = em.merge(pcb01CollectionNewPcb01);
                    if (oldDetailIdDetailOfPcb01CollectionNewPcb01 != null && !oldDetailIdDetailOfPcb01CollectionNewPcb01.equals(details)) {
                        oldDetailIdDetailOfPcb01CollectionNewPcb01.getPcb01Collection().remove(pcb01CollectionNewPcb01);
                        oldDetailIdDetailOfPcb01CollectionNewPcb01 = em.merge(oldDetailIdDetailOfPcb01CollectionNewPcb01);
                    }
                }
            }
            for (Back2pack back2packCollectionNewBack2pack : back2packCollectionNew) {
                if (!back2packCollectionOld.contains(back2packCollectionNewBack2pack)) {
                    Details oldDetailsIdDetailOfBack2packCollectionNewBack2pack = back2packCollectionNewBack2pack.getDetailsIdDetail();
                    back2packCollectionNewBack2pack.setDetailsIdDetail(details);
                    back2packCollectionNewBack2pack = em.merge(back2packCollectionNewBack2pack);
                    if (oldDetailsIdDetailOfBack2packCollectionNewBack2pack != null && !oldDetailsIdDetailOfBack2packCollectionNewBack2pack.equals(details)) {
                        oldDetailsIdDetailOfBack2packCollectionNewBack2pack.getBack2packCollection().remove(back2packCollectionNewBack2pack);
                        oldDetailsIdDetailOfBack2packCollectionNewBack2pack = em.merge(oldDetailsIdDetailOfBack2packCollectionNewBack2pack);
                    }
                }
            }
            for (Refu01 refu01CollectionNewRefu01 : refu01CollectionNew) {
                if (!refu01CollectionOld.contains(refu01CollectionNewRefu01)) {
                    Details oldDetailIdDetailOfRefu01CollectionNewRefu01 = refu01CollectionNewRefu01.getDetailIdDetail();
                    refu01CollectionNewRefu01.setDetailIdDetail(details);
                    refu01CollectionNewRefu01 = em.merge(refu01CollectionNewRefu01);
                    if (oldDetailIdDetailOfRefu01CollectionNewRefu01 != null && !oldDetailIdDetailOfRefu01CollectionNewRefu01.equals(details)) {
                        oldDetailIdDetailOfRefu01CollectionNewRefu01.getRefu01Collection().remove(refu01CollectionNewRefu01);
                        oldDetailIdDetailOfRefu01CollectionNewRefu01 = em.merge(oldDetailIdDetailOfRefu01CollectionNewRefu01);
                    }
                }
            }
            for (LotControl lotControlCollectionNewLotControl : lotControlCollectionNew) {
                if (!lotControlCollectionOld.contains(lotControlCollectionNewLotControl)) {
                    Details oldDetailsIdDetailOfLotControlCollectionNewLotControl = lotControlCollectionNewLotControl.getDetailsIdDetail();
                    lotControlCollectionNewLotControl.setDetailsIdDetail(details);
                    lotControlCollectionNewLotControl = em.merge(lotControlCollectionNewLotControl);
                    if (oldDetailsIdDetailOfLotControlCollectionNewLotControl != null && !oldDetailsIdDetailOfLotControlCollectionNewLotControl.equals(details)) {
                        oldDetailsIdDetailOfLotControlCollectionNewLotControl.getLotControlCollection().remove(lotControlCollectionNewLotControl);
                        oldDetailsIdDetailOfLotControlCollectionNewLotControl = em.merge(oldDetailsIdDetailOfLotControlCollectionNewLotControl);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = details.getIdDetail();
                if (findDetails(id) == null) {
                    throw new NonexistentEntityException("The details with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Details details;
            try {
                details = em.getReference(Details.class, id);
                details.getIdDetail();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The details with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<CopperWireInvoice> copperWireInvoiceCollectionOrphanCheck = details.getCopperWireInvoiceCollection();
            for (CopperWireInvoice copperWireInvoiceCollectionOrphanCheckCopperWireInvoice : copperWireInvoiceCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Details (" + details + ") cannot be destroyed since the CopperWireInvoice " + copperWireInvoiceCollectionOrphanCheckCopperWireInvoice + " in its copperWireInvoiceCollection field has a non-nullable detailsIdDetail field.");
            }
            Collection<DetailAssy> detailAssyCollectionOrphanCheck = details.getDetailAssyCollection();
            for (DetailAssy detailAssyCollectionOrphanCheckDetailAssy : detailAssyCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Details (" + details + ") cannot be destroyed since the DetailAssy " + detailAssyCollectionOrphanCheckDetailAssy + " in its detailAssyCollection field has a non-nullable detailIdDetail field.");
            }
            Collection<Front2backMaster> front2backMasterCollectionOrphanCheck = details.getFront2backMasterCollection();
            for (Front2backMaster front2backMasterCollectionOrphanCheckFront2backMaster : front2backMasterCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Details (" + details + ") cannot be destroyed since the Front2backMaster " + front2backMasterCollectionOrphanCheckFront2backMaster + " in its front2backMasterCollection field has a non-nullable detailsId field.");
            }
            Collection<DetailAuto> detailAutoCollectionOrphanCheck = details.getDetailAutoCollection();
            for (DetailAuto detailAutoCollectionOrphanCheckDetailAuto : detailAutoCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Details (" + details + ") cannot be destroyed since the DetailAuto " + detailAutoCollectionOrphanCheckDetailAuto + " in its detailAutoCollection field has a non-nullable detailIdDetail field.");
            }
            Collection<DetailPacking> detailPackingCollectionOrphanCheck = details.getDetailPackingCollection();
            for (DetailPacking detailPackingCollectionOrphanCheckDetailPacking : detailPackingCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Details (" + details + ") cannot be destroyed since the DetailPacking " + detailPackingCollectionOrphanCheckDetailPacking + " in its detailPackingCollection field has a non-nullable detailsIdDetail field.");
            }
            Collection<Assy2auto> assy2autoCollectionOrphanCheck = details.getAssy2autoCollection();
            for (Assy2auto assy2autoCollectionOrphanCheckAssy2auto : assy2autoCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Details (" + details + ") cannot be destroyed since the Assy2auto " + assy2autoCollectionOrphanCheckAssy2auto + " in its assy2autoCollection field has a non-nullable detailId field.");
            }
            Collection<LineAssembly> lineAssemblyCollectionOrphanCheck = details.getLineAssemblyCollection();
            for (LineAssembly lineAssemblyCollectionOrphanCheckLineAssembly : lineAssemblyCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Details (" + details + ") cannot be destroyed since the LineAssembly " + lineAssemblyCollectionOrphanCheckLineAssembly + " in its lineAssemblyCollection field has a non-nullable detailIdDetail field.");
            }
            Collection<DetailFl> detailFlCollectionOrphanCheck = details.getDetailFlCollection();
            for (DetailFl detailFlCollectionOrphanCheckDetailFl : detailFlCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Details (" + details + ") cannot be destroyed since the DetailFl " + detailFlCollectionOrphanCheckDetailFl + " in its detailFlCollection field has a non-nullable detailIdDetail field.");
            }
            Collection<Front2back> front2backCollectionOrphanCheck = details.getFront2backCollection();
            for (Front2back front2backCollectionOrphanCheckFront2back : front2backCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Details (" + details + ") cannot be destroyed since the Front2back " + front2backCollectionOrphanCheckFront2back + " in its front2backCollection field has a non-nullable detailsIdDetail field.");
            }
            Collection<Pcb01> pcb01CollectionOrphanCheck = details.getPcb01Collection();
            for (Pcb01 pcb01CollectionOrphanCheckPcb01 : pcb01CollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Details (" + details + ") cannot be destroyed since the Pcb01 " + pcb01CollectionOrphanCheckPcb01 + " in its pcb01Collection field has a non-nullable detailIdDetail field.");
            }
            Collection<Back2pack> back2packCollectionOrphanCheck = details.getBack2packCollection();
            for (Back2pack back2packCollectionOrphanCheckBack2pack : back2packCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Details (" + details + ") cannot be destroyed since the Back2pack " + back2packCollectionOrphanCheckBack2pack + " in its back2packCollection field has a non-nullable detailsIdDetail field.");
            }
            Collection<Refu01> refu01CollectionOrphanCheck = details.getRefu01Collection();
            for (Refu01 refu01CollectionOrphanCheckRefu01 : refu01CollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Details (" + details + ") cannot be destroyed since the Refu01 " + refu01CollectionOrphanCheckRefu01 + " in its refu01Collection field has a non-nullable detailIdDetail field.");
            }
            Collection<LotControl> lotControlCollectionOrphanCheck = details.getLotControlCollection();
            for (LotControl lotControlCollectionOrphanCheckLotControl : lotControlCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Details (" + details + ") cannot be destroyed since the LotControl " + lotControlCollectionOrphanCheckLotControl + " in its lotControlCollection field has a non-nullable detailsIdDetail field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(details);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Details> findDetailsEntities() {
        return findDetailsEntities(true, -1, -1);
    }

    public List<Details> findDetailsEntities(int maxResults, int firstResult) {
        return findDetailsEntities(false, maxResults, firstResult);
    }

    private List<Details> findDetailsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Details.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Details findDetails(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Details.class, id);
        } finally {
            em.close();
        }
    }

    public List<Details> findByMonthAndYear(Date date) {
        String year = new SimpleDateFormat("yyyy-MM-dd").format(date);
        System.err.println("Year : " + year);
        String month = new SimpleDateFormat("MM").format(date);
        String sql = "SELECT d FROM Details d WHERE months(d.dateCreate) = :MdateCreate";// AND d.dateCreate = :YdateCreate";
        System.err.println("Month : " + month);
        EntityManager em = getEntityManager();
        try {
            Query query = em.createQuery(sql);

//            Query query = em.createNamedQuery("Details.findByYearAndMonth");
            query.setParameter("MdateCreate", month);
//            query.setParameter("YdateCreate", "{d '"+year+"'}");
            return query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            em.close();
        }
//        return new ArrayList<Details>();
    }

    public int getDetailsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Details> rt = cq.from(Details.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<Details> findDetailsByModelAndDetailNameAssy(String model) {
        EntityManager em = getEntityManager();
        Query query;
        try {
            query = em.createNamedQuery("Details.findByModelAndDetailNameAssy");
            query.setParameter("model", model + "%");
            query.setParameter("detailName", "detail_assy");
            return query.getResultList();
        } finally {
            em.close();
        }
    }

    public List<Details> findDetailsByModelAndDetailNameAuto(String model) {
        EntityManager em = getEntityManager();
        Query query;
        try {
            query = em.createNamedQuery("Details.findByModelAndDetailNameAssy");
            query.setParameter("model", model + "%");
            query.setParameter("detailName", "detail_auto");
            return query.getResultList();
        } finally {
            em.close();
        }
    }

    public List<Details> findDetailsByModelAndDetailNamePacking(String model) {
        EntityManager em = getEntityManager();
        Query query;
        try {
            query = em.createNamedQuery("Details.findByModelAndDetailNameAssy");
            query.setParameter("model", model + "%");
            query.setParameter("detailName", "detail_packing");
            return query.getResultList();
        } finally {
            em.close();
        }
    }

    public List<Details> findDetailsByDetailName(String detailname) {
        EntityManager em = getEntityManager();
        Query query;
        try {
            query = em.createNamedQuery("Details.findByDetailName");
            query.setParameter("detailName", detailname);
            return query.getResultList();
        } finally {
            em.close();
        }
    }

    public List<Details> findDetailsByOrdernumber(String ordernumber) {
        EntityManager em = getEntityManager();
        try {
            String sql = "SELECT d FROM Details d WHERE d.orderNumber LIKE :orderNumber ORDER BY d.dateCreate ASC";
            Query query = em.createQuery(sql);
            query.setParameter("orderNumber", ordernumber + "%");
            return query.getResultList();
        } finally {
            em.close();
        }
    }
//    find by order num  
    public List<Details>  findByOrderNum(String orderNumber)
    {
        EntityManager  em  = getEntityManager();
        try
        {
            Query q  = em.createQuery("SELECT d FROM Details d WHERE d.deleted = 0 and d.orderNumber =:orderNumber").setParameter("orderNumber",orderNumber);
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    
    }
}
