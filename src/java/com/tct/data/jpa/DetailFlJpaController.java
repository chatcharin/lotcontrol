/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import com.tct.data.DetailFl;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.Details;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author The_Boy_Cs
 */
public class DetailFlJpaController implements Serializable {

    public DetailFlJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DetailFl detailFl) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Details detailIdDetail = detailFl.getDetailIdDetail();
            if (detailIdDetail != null) {
                detailIdDetail = em.getReference(detailIdDetail.getClass(), detailIdDetail.getIdDetail());
                detailFl.setDetailIdDetail(detailIdDetail);
            }
            em.persist(detailFl);
            if (detailIdDetail != null) {
                detailIdDetail.getDetailFlCollection().add(detailFl);
                detailIdDetail = em.merge(detailIdDetail);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDetailFl(detailFl.getId()) != null) {
                throw new PreexistingEntityException("DetailFl " + detailFl + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DetailFl detailFl) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailFl persistentDetailFl = em.find(DetailFl.class, detailFl.getId());
            Details detailIdDetailOld = persistentDetailFl.getDetailIdDetail();
            Details detailIdDetailNew = detailFl.getDetailIdDetail();
            if (detailIdDetailNew != null) {
                detailIdDetailNew = em.getReference(detailIdDetailNew.getClass(), detailIdDetailNew.getIdDetail());
                detailFl.setDetailIdDetail(detailIdDetailNew);
            }
            detailFl = em.merge(detailFl);
            if (detailIdDetailOld != null && !detailIdDetailOld.equals(detailIdDetailNew)) {
                detailIdDetailOld.getDetailFlCollection().remove(detailFl);
                detailIdDetailOld = em.merge(detailIdDetailOld);
            }
            if (detailIdDetailNew != null && !detailIdDetailNew.equals(detailIdDetailOld)) {
                detailIdDetailNew.getDetailFlCollection().add(detailFl);
                detailIdDetailNew = em.merge(detailIdDetailNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = detailFl.getId();
                if (findDetailFl(id) == null) {
                    throw new NonexistentEntityException("The detailFl with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailFl detailFl;
            try {
                detailFl = em.getReference(DetailFl.class, id);
                detailFl.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The detailFl with id " + id + " no longer exists.", enfe);
            }
            Details detailIdDetail = detailFl.getDetailIdDetail();
            if (detailIdDetail != null) {
                detailIdDetail.getDetailFlCollection().remove(detailFl);
                detailIdDetail = em.merge(detailIdDetail);
            }
            em.remove(detailFl);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DetailFl> findDetailFlEntities() {
        return findDetailFlEntities(true, -1, -1);
    }

    public List<DetailFl> findDetailFlEntities(int maxResults, int firstResult) {
        return findDetailFlEntities(false, maxResults, firstResult);
    }

    private List<DetailFl> findDetailFlEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(DetailFl.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } catch (Exception e) {
            System.out.println("test --------" + e.getMessage());
            return null;
        } finally {
            em.close();
        }
    }

    public DetailFl findDetailFl(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DetailFl.class, id);
        } finally {
            em.close();
        }
    }
    //---find  data detaile fl

    public List<DetailFl> findDataListDetailFl(Details idDetials) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("DetailFl.findByDetailsIdDetails");
            q.setParameter("detailIdDetail", idDetials);
            return q.getResultList();
        } finally {
            em.close();

        }

    }

    public int getDetailFlCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<DetailFl> rt = cq.from(DetailFl.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<DetailFl> findDetailFlByOrderNumber(String ordernumber) {
        EntityManager em = getEntityManager();
        Query query;
        try {
            query = em.createNamedQuery("DetailFl.findByOrderNo");
            query.setParameter("orderNo", ordernumber + "%");
            return query.getResultList();
        } finally {
            em.close();
        }
    }

    public List<DetailFl> findDetailFlByTypeBobbin(String typebobbin) {
        EntityManager em = getEntityManager();
        Query query;
        try {
            query = em.createNamedQuery("DetailFl.findByTypeBb");
            query.setParameter("typeBb", typebobbin + "%");
            return query.getResultList();
        } finally {
            em.close();
        }
    }

    public List<DetailFl> findDetailFlByModel(String model) {
        EntityManager em = getEntityManager();
        Query query;
        try {
            query = em.createNamedQuery("DetailFl.findByTypeModel");
            query.setParameter("typeModel", model + "%");
            return query.getResultList();
        } finally {
            em.close();
        }
    }

    public DetailFl findDetailFlByDetails(Details detail) {
        EntityManager em = getEntityManager();
        Query query;
        try {
            query = em.createNamedQuery("DetailFl.findByDetailIdDetail", DetailFl.class);
            query.setParameter("detailIdDetail", detail);
            return (DetailFl) query.getSingleResult();
        } finally {
            em.close();
        }
    }

    public DetailFl findDataListDetailFlSingle(Details idDetials) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("DetailFl.findByDetailsIdDetails");
            q.setParameter("detailIdDetail", idDetials);
            return (DetailFl) q.getSingleResult();
        } finally {
            em.close();

        }
    }

    public List<DetailFl> findAll() {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("DetailFl.findAll");
            return query.getResultList();
        } finally {
            em.close();
        }
    }
}
