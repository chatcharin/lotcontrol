/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import com.tct.data.Details;
import com.tct.data.LineAssembly;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author The_Boy_Cs
 */
public class LineAssemblyJpaController implements Serializable {

    public LineAssemblyJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(LineAssembly lineAssembly) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Details detailIdDetail = lineAssembly.getDetailIdDetail();
            if (detailIdDetail != null) {
                detailIdDetail = em.getReference(detailIdDetail.getClass(), detailIdDetail.getIdDetail());
                lineAssembly.setDetailIdDetail(detailIdDetail);
            }
            em.persist(lineAssembly);
            if (detailIdDetail != null) {
                detailIdDetail.getLineAssemblyCollection().add(lineAssembly);
                detailIdDetail = em.merge(detailIdDetail);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findLineAssembly(lineAssembly.getId()) != null) {
                throw new PreexistingEntityException("LineAssembly " + lineAssembly + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(LineAssembly lineAssembly) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            LineAssembly persistentLineAssembly = em.find(LineAssembly.class, lineAssembly.getId());
            Details detailIdDetailOld = persistentLineAssembly.getDetailIdDetail();
            Details detailIdDetailNew = lineAssembly.getDetailIdDetail();
            if (detailIdDetailNew != null) {
                detailIdDetailNew = em.getReference(detailIdDetailNew.getClass(), detailIdDetailNew.getIdDetail());
                lineAssembly.setDetailIdDetail(detailIdDetailNew);
            }
            lineAssembly = em.merge(lineAssembly);
            if (detailIdDetailOld != null && !detailIdDetailOld.equals(detailIdDetailNew)) {
                detailIdDetailOld.getLineAssemblyCollection().remove(lineAssembly);
                detailIdDetailOld = em.merge(detailIdDetailOld);
            }
            if (detailIdDetailNew != null && !detailIdDetailNew.equals(detailIdDetailOld)) {
                detailIdDetailNew.getLineAssemblyCollection().add(lineAssembly);
                detailIdDetailNew = em.merge(detailIdDetailNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = lineAssembly.getId();
                if (findLineAssembly(id) == null) {
                    throw new NonexistentEntityException("The lineAssembly with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            LineAssembly lineAssembly;
            try {
                lineAssembly = em.getReference(LineAssembly.class, id);
                lineAssembly.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The lineAssembly with id " + id + " no longer exists.", enfe);
            }
            Details detailIdDetail = lineAssembly.getDetailIdDetail();
            if (detailIdDetail != null) {
                detailIdDetail.getLineAssemblyCollection().remove(lineAssembly);
                detailIdDetail = em.merge(detailIdDetail);
            }
            em.remove(lineAssembly);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<LineAssembly> findLineAssemblyEntities() {
        return findLineAssemblyEntities(true, -1, -1);
    }

    public List<LineAssembly> findLineAssemblyEntities(int maxResults, int firstResult) {
        return findLineAssemblyEntities(false, maxResults, firstResult);
    }

    private List<LineAssembly> findLineAssemblyEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(LineAssembly.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public LineAssembly findLineAssembly(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(LineAssembly.class, id);
        } finally {
            em.close();
        }
    }

    public int getLineAssemblyCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<LineAssembly> rt = cq.from(LineAssembly.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    public List<LineAssembly>  findLineAssemblyList(Details  idDetails){
        EntityManager  em  =getEntityManager();
        try{
            Query q  =  em.createNamedQuery("LineAssembly.findBydetailIdDetail");
            q.setParameter("detailIdDetail", idDetails);
            return q.getResultList();
        }
        finally{
            em.close();
        }
    }
    
    public List<LineAssembly> findLineAssemblysAll(){
        EntityManager em = getEntityManager();
        Query query ;
        try{
            query = em.createNamedQuery("LineAssembly.findAll");
            return query.getResultList();
        }finally{
            em.close();
        }
    }
    public LineAssembly  findLineAssemblyListSingle(Details  idDetails){
        EntityManager  em  =getEntityManager();
        try{
            Query q  =  em.createNamedQuery("LineAssembly.findBydetailIdDetail");
            q.setParameter("detailIdDetail", idDetails);
            return (LineAssembly)q.getSingleResult();
        }
        finally{
            em.close();
        }
    }
}
