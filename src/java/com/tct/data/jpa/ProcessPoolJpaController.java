/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import com.tct.data.*;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import com.tct.data.jpa.exceptions.IllegalOrphanException;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author The_Boy_Cs
 */
public class ProcessPoolJpaController implements Serializable {

    public ProcessPoolJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ProcessPool processPool) throws PreexistingEntityException, Exception {
        if (processPool.getPc2ngCollection() == null) {
            processPool.setPc2ngCollection(new ArrayList<Pc2ng>());
        }
        if (processPool.getPc2dataCollection() == null) {
            processPool.setPc2dataCollection(new ArrayList<Pc2data>());
        }
        if (processPool.getMd2pcCollection() == null) {
            processPool.setMd2pcCollection(new ArrayList<Md2pc>());
        }
        if (processPool.getMainDataCollection() == null) {
            processPool.setMainDataCollection(new ArrayList<MainData>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Pc2ng> attachedPc2ngCollection = new ArrayList<Pc2ng>();
            for (Pc2ng pc2ngCollectionPc2ngToAttach : processPool.getPc2ngCollection()) {
                pc2ngCollectionPc2ngToAttach = em.getReference(pc2ngCollectionPc2ngToAttach.getClass(), pc2ngCollectionPc2ngToAttach.getPc2ngId());
                attachedPc2ngCollection.add(pc2ngCollectionPc2ngToAttach);
            }
            processPool.setPc2ngCollection(attachedPc2ngCollection);
            Collection<Pc2data> attachedPc2dataCollection = new ArrayList<Pc2data>();
            for (Pc2data pc2dataCollectionPc2dataToAttach : processPool.getPc2dataCollection()) {
                pc2dataCollectionPc2dataToAttach = em.getReference(pc2dataCollectionPc2dataToAttach.getClass(), pc2dataCollectionPc2dataToAttach.getPc2dataId());
                attachedPc2dataCollection.add(pc2dataCollectionPc2dataToAttach);
            }
            processPool.setPc2dataCollection(attachedPc2dataCollection);
            Collection<Md2pc> attachedMd2pcCollection = new ArrayList<Md2pc>();
            for (Md2pc md2pcCollectionMd2pcToAttach : processPool.getMd2pcCollection()) {
                md2pcCollectionMd2pcToAttach = em.getReference(md2pcCollectionMd2pcToAttach.getClass(), md2pcCollectionMd2pcToAttach.getMd2pcId());
                attachedMd2pcCollection.add(md2pcCollectionMd2pcToAttach);
            }
            processPool.setMd2pcCollection(attachedMd2pcCollection);
            Collection<MainData> attachedMainDataCollection = new ArrayList<MainData>();
            for (MainData mainDataCollectionMainDataToAttach : processPool.getMainDataCollection()) {
                mainDataCollectionMainDataToAttach = em.getReference(mainDataCollectionMainDataToAttach.getClass(), mainDataCollectionMainDataToAttach.getId());
                attachedMainDataCollection.add(mainDataCollectionMainDataToAttach);
            }
            processPool.setMainDataCollection(attachedMainDataCollection);
            em.persist(processPool);
            for (Pc2ng pc2ngCollectionPc2ng : processPool.getPc2ngCollection()) {
                ProcessPool oldIdProcOfPc2ngCollectionPc2ng = pc2ngCollectionPc2ng.getIdProc();
                pc2ngCollectionPc2ng.setIdProc(processPool);
                pc2ngCollectionPc2ng = em.merge(pc2ngCollectionPc2ng);
                if (oldIdProcOfPc2ngCollectionPc2ng != null) {
                    oldIdProcOfPc2ngCollectionPc2ng.getPc2ngCollection().remove(pc2ngCollectionPc2ng);
                    oldIdProcOfPc2ngCollectionPc2ng = em.merge(oldIdProcOfPc2ngCollectionPc2ng);
                }
            }
            for (Pc2data pc2dataCollectionPc2data : processPool.getPc2dataCollection()) {
                ProcessPool oldIdProcOfPc2dataCollectionPc2data = pc2dataCollectionPc2data.getIdProc();
                pc2dataCollectionPc2data.setIdProc(processPool);
                pc2dataCollectionPc2data = em.merge(pc2dataCollectionPc2data);
                if (oldIdProcOfPc2dataCollectionPc2data != null) {
                    oldIdProcOfPc2dataCollectionPc2data.getPc2dataCollection().remove(pc2dataCollectionPc2data);
                    oldIdProcOfPc2dataCollectionPc2data = em.merge(oldIdProcOfPc2dataCollectionPc2data);
                }
            }
            for (Md2pc md2pcCollectionMd2pc : processPool.getMd2pcCollection()) {
                ProcessPool oldProcessPoolIdProcOfMd2pcCollectionMd2pc = md2pcCollectionMd2pc.getProcessPoolIdProc();
                md2pcCollectionMd2pc.setProcessPoolIdProc(processPool);
                md2pcCollectionMd2pc = em.merge(md2pcCollectionMd2pc);
                if (oldProcessPoolIdProcOfMd2pcCollectionMd2pc != null) {
                    oldProcessPoolIdProcOfMd2pcCollectionMd2pc.getMd2pcCollection().remove(md2pcCollectionMd2pc);
                    oldProcessPoolIdProcOfMd2pcCollectionMd2pc = em.merge(oldProcessPoolIdProcOfMd2pcCollectionMd2pc);
                }
            }
            for (MainData mainDataCollectionMainData : processPool.getMainDataCollection()) {
                ProcessPool oldIdProcOfMainDataCollectionMainData = mainDataCollectionMainData.getIdProc();
                mainDataCollectionMainData.setIdProc(processPool);
                mainDataCollectionMainData = em.merge(mainDataCollectionMainData);
                if (oldIdProcOfMainDataCollectionMainData != null) {
                    oldIdProcOfMainDataCollectionMainData.getMainDataCollection().remove(mainDataCollectionMainData);
                    oldIdProcOfMainDataCollectionMainData = em.merge(oldIdProcOfMainDataCollectionMainData);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findProcessPool(processPool.getIdProc()) != null) {
                throw new PreexistingEntityException("ProcessPool " + processPool + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ProcessPool processPool) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ProcessPool persistentProcessPool = em.find(ProcessPool.class, processPool.getIdProc());
            Collection<Pc2ng> pc2ngCollectionOld = persistentProcessPool.getPc2ngCollection();
            Collection<Pc2ng> pc2ngCollectionNew = processPool.getPc2ngCollection();
            Collection<Pc2data> pc2dataCollectionOld = persistentProcessPool.getPc2dataCollection();
            Collection<Pc2data> pc2dataCollectionNew = processPool.getPc2dataCollection();
            Collection<Md2pc> md2pcCollectionOld = persistentProcessPool.getMd2pcCollection();
            Collection<Md2pc> md2pcCollectionNew = processPool.getMd2pcCollection();
            Collection<MainData> mainDataCollectionOld = persistentProcessPool.getMainDataCollection();
            Collection<MainData> mainDataCollectionNew = processPool.getMainDataCollection();
            List<String> illegalOrphanMessages = null;
            for (Pc2ng pc2ngCollectionOldPc2ng : pc2ngCollectionOld) {
                if (!pc2ngCollectionNew.contains(pc2ngCollectionOldPc2ng)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Pc2ng " + pc2ngCollectionOldPc2ng + " since its idProc field is not nullable.");
                }
            }
            for (Pc2data pc2dataCollectionOldPc2data : pc2dataCollectionOld) {
                if (!pc2dataCollectionNew.contains(pc2dataCollectionOldPc2data)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Pc2data " + pc2dataCollectionOldPc2data + " since its idProc field is not nullable.");
                }
            }
            for (Md2pc md2pcCollectionOldMd2pc : md2pcCollectionOld) {
                if (!md2pcCollectionNew.contains(md2pcCollectionOldMd2pc)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Md2pc " + md2pcCollectionOldMd2pc + " since its processPoolIdProc field is not nullable.");
                }
            }
            for (MainData mainDataCollectionOldMainData : mainDataCollectionOld) {
                if (!mainDataCollectionNew.contains(mainDataCollectionOldMainData)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain MainData " + mainDataCollectionOldMainData + " since its idProc field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Pc2ng> attachedPc2ngCollectionNew = new ArrayList<Pc2ng>();
            for (Pc2ng pc2ngCollectionNewPc2ngToAttach : pc2ngCollectionNew) {
                pc2ngCollectionNewPc2ngToAttach = em.getReference(pc2ngCollectionNewPc2ngToAttach.getClass(), pc2ngCollectionNewPc2ngToAttach.getPc2ngId());
                attachedPc2ngCollectionNew.add(pc2ngCollectionNewPc2ngToAttach);
            }
            pc2ngCollectionNew = attachedPc2ngCollectionNew;
            processPool.setPc2ngCollection(pc2ngCollectionNew);
            Collection<Pc2data> attachedPc2dataCollectionNew = new ArrayList<Pc2data>();
            for (Pc2data pc2dataCollectionNewPc2dataToAttach : pc2dataCollectionNew) {
                pc2dataCollectionNewPc2dataToAttach = em.getReference(pc2dataCollectionNewPc2dataToAttach.getClass(), pc2dataCollectionNewPc2dataToAttach.getPc2dataId());
                attachedPc2dataCollectionNew.add(pc2dataCollectionNewPc2dataToAttach);
            }
            pc2dataCollectionNew = attachedPc2dataCollectionNew;
            processPool.setPc2dataCollection(pc2dataCollectionNew);
            Collection<Md2pc> attachedMd2pcCollectionNew = new ArrayList<Md2pc>();
            for (Md2pc md2pcCollectionNewMd2pcToAttach : md2pcCollectionNew) {
                md2pcCollectionNewMd2pcToAttach = em.getReference(md2pcCollectionNewMd2pcToAttach.getClass(), md2pcCollectionNewMd2pcToAttach.getMd2pcId());
                attachedMd2pcCollectionNew.add(md2pcCollectionNewMd2pcToAttach);
            }
            md2pcCollectionNew = attachedMd2pcCollectionNew;
            processPool.setMd2pcCollection(md2pcCollectionNew);
            Collection<MainData> attachedMainDataCollectionNew = new ArrayList<MainData>();
            for (MainData mainDataCollectionNewMainDataToAttach : mainDataCollectionNew) {
                mainDataCollectionNewMainDataToAttach = em.getReference(mainDataCollectionNewMainDataToAttach.getClass(), mainDataCollectionNewMainDataToAttach.getId());
                attachedMainDataCollectionNew.add(mainDataCollectionNewMainDataToAttach);
            }
            mainDataCollectionNew = attachedMainDataCollectionNew;
            processPool.setMainDataCollection(mainDataCollectionNew);
            processPool = em.merge(processPool);
            for (Pc2ng pc2ngCollectionNewPc2ng : pc2ngCollectionNew) {
                if (!pc2ngCollectionOld.contains(pc2ngCollectionNewPc2ng)) {
                    ProcessPool oldIdProcOfPc2ngCollectionNewPc2ng = pc2ngCollectionNewPc2ng.getIdProc();
                    pc2ngCollectionNewPc2ng.setIdProc(processPool);
                    pc2ngCollectionNewPc2ng = em.merge(pc2ngCollectionNewPc2ng);
                    if (oldIdProcOfPc2ngCollectionNewPc2ng != null && !oldIdProcOfPc2ngCollectionNewPc2ng.equals(processPool)) {
                        oldIdProcOfPc2ngCollectionNewPc2ng.getPc2ngCollection().remove(pc2ngCollectionNewPc2ng);
                        oldIdProcOfPc2ngCollectionNewPc2ng = em.merge(oldIdProcOfPc2ngCollectionNewPc2ng);
                    }
                }
            }
            for (Pc2data pc2dataCollectionNewPc2data : pc2dataCollectionNew) {
                if (!pc2dataCollectionOld.contains(pc2dataCollectionNewPc2data)) {
                    ProcessPool oldIdProcOfPc2dataCollectionNewPc2data = pc2dataCollectionNewPc2data.getIdProc();
                    pc2dataCollectionNewPc2data.setIdProc(processPool);
                    pc2dataCollectionNewPc2data = em.merge(pc2dataCollectionNewPc2data);
                    if (oldIdProcOfPc2dataCollectionNewPc2data != null && !oldIdProcOfPc2dataCollectionNewPc2data.equals(processPool)) {
                        oldIdProcOfPc2dataCollectionNewPc2data.getPc2dataCollection().remove(pc2dataCollectionNewPc2data);
                        oldIdProcOfPc2dataCollectionNewPc2data = em.merge(oldIdProcOfPc2dataCollectionNewPc2data);
                    }
                }
            }
            for (Md2pc md2pcCollectionNewMd2pc : md2pcCollectionNew) {
                if (!md2pcCollectionOld.contains(md2pcCollectionNewMd2pc)) {
                    ProcessPool oldProcessPoolIdProcOfMd2pcCollectionNewMd2pc = md2pcCollectionNewMd2pc.getProcessPoolIdProc();
                    md2pcCollectionNewMd2pc.setProcessPoolIdProc(processPool);
                    md2pcCollectionNewMd2pc = em.merge(md2pcCollectionNewMd2pc);
                    if (oldProcessPoolIdProcOfMd2pcCollectionNewMd2pc != null && !oldProcessPoolIdProcOfMd2pcCollectionNewMd2pc.equals(processPool)) {
                        oldProcessPoolIdProcOfMd2pcCollectionNewMd2pc.getMd2pcCollection().remove(md2pcCollectionNewMd2pc);
                        oldProcessPoolIdProcOfMd2pcCollectionNewMd2pc = em.merge(oldProcessPoolIdProcOfMd2pcCollectionNewMd2pc);
                    }
                }
            }
            for (MainData mainDataCollectionNewMainData : mainDataCollectionNew) {
                if (!mainDataCollectionOld.contains(mainDataCollectionNewMainData)) {
                    ProcessPool oldIdProcOfMainDataCollectionNewMainData = mainDataCollectionNewMainData.getIdProc();
                    mainDataCollectionNewMainData.setIdProc(processPool);
                    mainDataCollectionNewMainData = em.merge(mainDataCollectionNewMainData);
                    if (oldIdProcOfMainDataCollectionNewMainData != null && !oldIdProcOfMainDataCollectionNewMainData.equals(processPool)) {
                        oldIdProcOfMainDataCollectionNewMainData.getMainDataCollection().remove(mainDataCollectionNewMainData);
                        oldIdProcOfMainDataCollectionNewMainData = em.merge(oldIdProcOfMainDataCollectionNewMainData);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = processPool.getIdProc();
                if (findProcessPool(id) == null) {
                    throw new NonexistentEntityException("The processPool with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ProcessPool processPool;
            try {
                processPool = em.getReference(ProcessPool.class, id);
                processPool.getIdProc();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The processPool with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Pc2ng> pc2ngCollectionOrphanCheck = processPool.getPc2ngCollection();
            for (Pc2ng pc2ngCollectionOrphanCheckPc2ng : pc2ngCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This ProcessPool (" + processPool + ") cannot be destroyed since the Pc2ng " + pc2ngCollectionOrphanCheckPc2ng + " in its pc2ngCollection field has a non-nullable idProc field.");
            }
            Collection<Pc2data> pc2dataCollectionOrphanCheck = processPool.getPc2dataCollection();
            for (Pc2data pc2dataCollectionOrphanCheckPc2data : pc2dataCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This ProcessPool (" + processPool + ") cannot be destroyed since the Pc2data " + pc2dataCollectionOrphanCheckPc2data + " in its pc2dataCollection field has a non-nullable idProc field.");
            }
            Collection<Md2pc> md2pcCollectionOrphanCheck = processPool.getMd2pcCollection();
            for (Md2pc md2pcCollectionOrphanCheckMd2pc : md2pcCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This ProcessPool (" + processPool + ") cannot be destroyed since the Md2pc " + md2pcCollectionOrphanCheckMd2pc + " in its md2pcCollection field has a non-nullable processPoolIdProc field.");
            }
            Collection<MainData> mainDataCollectionOrphanCheck = processPool.getMainDataCollection();
            for (MainData mainDataCollectionOrphanCheckMainData : mainDataCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This ProcessPool (" + processPool + ") cannot be destroyed since the MainData " + mainDataCollectionOrphanCheckMainData + " in its mainDataCollection field has a non-nullable idProc field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(processPool);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ProcessPool> findProcessPoolEntities() {
        return findProcessPoolEntities(true, -1, -1);
    }

    public List<ProcessPool> findProcessPoolEntities(int maxResults, int firstResult) {
        return findProcessPoolEntities(false, maxResults, firstResult);
    }

    private List<ProcessPool> findProcessPoolEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ProcessPool.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ProcessPool findProcessPool(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ProcessPool.class, id);
        } finally {
            em.close();
        }
    }

    public int getProcessPoolCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ProcessPool> rt = cq.from(ProcessPool.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<ProcessPool> findProcessPoolByName(String name) {
        EntityManager em = getEntityManager();
        Query query;
        try {
            query = em.createNamedQuery("ProcessPool.findByProcName");
            query.setParameter("procName", name + "%");
            return query.getResultList();
        } finally {
            em.close();
        }
    }

    public List<ProcessPool> findProcessPoolByNotdeleted() {
        EntityManager em = getEntityManager();
        Query query;
        try {
            query = em.createNamedQuery("ProcessPool.findByNotdeleted");
            return query.getResultList();
        } finally {
            em.close();
        }
    }
    
    public List<ProcessPool> findProcessPoolByCodeOperation(String codeoperation){
        EntityManager em = getEntityManager();
        try{
            Query query = em.createNamedQuery("ProcessPool.findByCodeOperation");
            query.setParameter("codeOperation", codeoperation+"%");
            return query.getResultList();
        }finally{
            em.close();
        }
    }
    
    public List<ProcessPool> findProcessPoolByStatus(String status){
        EntityManager em = getEntityManager();
        try{
            Query query = em.createNamedQuery("ProcessPool.findByStatus");
            query.setParameter("status", status+"%");
            return query.getResultList();
        }finally{
            em.close();
        }
    }
    
    public List<ProcessPool> findProcessPoolByType(String type){
        EntityManager em = getEntityManager();
        try{
            Query query = em.createNamedQuery("ProcessPool.findByType");
            query.setParameter("type", type+"%");
            return query.getResultList();
        }finally{
            em.close();
        }
    }
    public List<ProcessPool> findProcessUseMc()
    {
        EntityManager  em = getEntityManager();
        try
        {
            Query q = em.createNamedQuery("ProcessPool.findByMcUse").setParameter("mcUse",1);
            return q.getResultList();
        }
        finally
        {
            em.close();;
        }
    }
    //@pang find nameprocess alll asc not deleted
      public List<ProcessPool> findProcessPoolByNameNotDeteltedAsc() {
        EntityManager em = getEntityManager();
        Query query;
        try {
            query = em.createQuery("SELECT p FROM ProcessPool p WHERE p.deleted =0 ORDER BY p.procName ASC");
            return query.getResultList();
        } finally {
            em.close();
        }
    }
}
