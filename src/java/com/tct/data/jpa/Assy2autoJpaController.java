/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import com.tct.data.Assy2auto;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.LotControl;
import com.tct.data.Details;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author The_Boy_Cs
 */
public class Assy2autoJpaController implements Serializable {

    public Assy2autoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Assy2auto assy2auto) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            LotControl lotcontrolId = assy2auto.getLotcontrolId();
            if (lotcontrolId != null) {
                lotcontrolId = em.getReference(lotcontrolId.getClass(), lotcontrolId.getId());
                assy2auto.setLotcontrolId(lotcontrolId);
            }
            Details detailId = assy2auto.getDetailId();
            if (detailId != null) {
                detailId = em.getReference(detailId.getClass(), detailId.getIdDetail());
                assy2auto.setDetailId(detailId);
            }
            em.persist(assy2auto);
            if (lotcontrolId != null) {
                lotcontrolId.getAssy2autoCollection().add(assy2auto);
                lotcontrolId = em.merge(lotcontrolId);
            }
            if (detailId != null) {
                detailId.getAssy2autoCollection().add(assy2auto);
                detailId = em.merge(detailId);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findAssy2auto(assy2auto.getId()) != null) {
                throw new PreexistingEntityException("Assy2auto " + assy2auto + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Assy2auto assy2auto) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Assy2auto persistentAssy2auto = em.find(Assy2auto.class, assy2auto.getId());
            LotControl lotcontrolIdOld = persistentAssy2auto.getLotcontrolId();
            LotControl lotcontrolIdNew = assy2auto.getLotcontrolId();
            Details detailIdOld = persistentAssy2auto.getDetailId();
            Details detailIdNew = assy2auto.getDetailId();
            if (lotcontrolIdNew != null) {
                lotcontrolIdNew = em.getReference(lotcontrolIdNew.getClass(), lotcontrolIdNew.getId());
                assy2auto.setLotcontrolId(lotcontrolIdNew);
            }
            if (detailIdNew != null) {
                detailIdNew = em.getReference(detailIdNew.getClass(), detailIdNew.getIdDetail());
                assy2auto.setDetailId(detailIdNew);
            }
            assy2auto = em.merge(assy2auto);
            if (lotcontrolIdOld != null && !lotcontrolIdOld.equals(lotcontrolIdNew)) {
                lotcontrolIdOld.getAssy2autoCollection().remove(assy2auto);
                lotcontrolIdOld = em.merge(lotcontrolIdOld);
            }
            if (lotcontrolIdNew != null && !lotcontrolIdNew.equals(lotcontrolIdOld)) {
                lotcontrolIdNew.getAssy2autoCollection().add(assy2auto);
                lotcontrolIdNew = em.merge(lotcontrolIdNew);
            }
            if (detailIdOld != null && !detailIdOld.equals(detailIdNew)) {
                detailIdOld.getAssy2autoCollection().remove(assy2auto);
                detailIdOld = em.merge(detailIdOld);
            }
            if (detailIdNew != null && !detailIdNew.equals(detailIdOld)) {
                detailIdNew.getAssy2autoCollection().add(assy2auto);
                detailIdNew = em.merge(detailIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = assy2auto.getId();
                if (findAssy2auto(id) == null) {
                    throw new NonexistentEntityException("The assy2auto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Assy2auto assy2auto;
            try {
                assy2auto = em.getReference(Assy2auto.class, id);
                assy2auto.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The assy2auto with id " + id + " no longer exists.", enfe);
            }
            LotControl lotcontrolId = assy2auto.getLotcontrolId();
            if (lotcontrolId != null) {
                lotcontrolId.getAssy2autoCollection().remove(assy2auto);
                lotcontrolId = em.merge(lotcontrolId);
            }
            Details detailId = assy2auto.getDetailId();
            if (detailId != null) {
                detailId.getAssy2autoCollection().remove(assy2auto);
                detailId = em.merge(detailId);
            }
            em.remove(assy2auto);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Assy2auto> findAssy2autoEntities() {
        return findAssy2autoEntities(true, -1, -1);
    }

    public List<Assy2auto> findAssy2autoEntities(int maxResults, int firstResult) {
        return findAssy2autoEntities(false, maxResults, firstResult);
    }

    private List<Assy2auto> findAssy2autoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Assy2auto.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Assy2auto findAssy2auto(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Assy2auto.class, id);
        } finally {
            em.close();
        }
    }

    public int getAssy2autoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Assy2auto> rt = cq.from(Assy2auto.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
