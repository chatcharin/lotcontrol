/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import com.tct.data.NgPool;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.Pc2ng;
import com.tct.data.jpa.exceptions.IllegalOrphanException;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author The_Boy_Cs
 */
public class NgPoolJpaController implements Serializable {

    public NgPoolJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(NgPool ngPool) throws PreexistingEntityException, Exception {
        if (ngPool.getPc2ngCollection() == null) {
            ngPool.setPc2ngCollection(new ArrayList<Pc2ng>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Pc2ng> attachedPc2ngCollection = new ArrayList<Pc2ng>();
            for (Pc2ng pc2ngCollectionPc2ngToAttach : ngPool.getPc2ngCollection()) {
                pc2ngCollectionPc2ngToAttach = em.getReference(pc2ngCollectionPc2ngToAttach.getClass(), pc2ngCollectionPc2ngToAttach.getPc2ngId());
                attachedPc2ngCollection.add(pc2ngCollectionPc2ngToAttach);
            }
            ngPool.setPc2ngCollection(attachedPc2ngCollection);
            em.persist(ngPool);
            for (Pc2ng pc2ngCollectionPc2ng : ngPool.getPc2ngCollection()) {
                NgPool oldIdNgOfPc2ngCollectionPc2ng = pc2ngCollectionPc2ng.getIdNg();
                pc2ngCollectionPc2ng.setIdNg(ngPool);
                pc2ngCollectionPc2ng = em.merge(pc2ngCollectionPc2ng);
                if (oldIdNgOfPc2ngCollectionPc2ng != null) {
                    oldIdNgOfPc2ngCollectionPc2ng.getPc2ngCollection().remove(pc2ngCollectionPc2ng);
                    oldIdNgOfPc2ngCollectionPc2ng = em.merge(oldIdNgOfPc2ngCollectionPc2ng);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findNgPool(ngPool.getIdNg()) != null) {
                throw new PreexistingEntityException("NgPool " + ngPool + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(NgPool ngPool) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            NgPool persistentNgPool = em.find(NgPool.class, ngPool.getIdNg());
            Collection<Pc2ng> pc2ngCollectionOld = persistentNgPool.getPc2ngCollection();
            Collection<Pc2ng> pc2ngCollectionNew = ngPool.getPc2ngCollection();
            List<String> illegalOrphanMessages = null;
            for (Pc2ng pc2ngCollectionOldPc2ng : pc2ngCollectionOld) {
                if (!pc2ngCollectionNew.contains(pc2ngCollectionOldPc2ng)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Pc2ng " + pc2ngCollectionOldPc2ng + " since its idNg field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Pc2ng> attachedPc2ngCollectionNew = new ArrayList<Pc2ng>();
            for (Pc2ng pc2ngCollectionNewPc2ngToAttach : pc2ngCollectionNew) {
                pc2ngCollectionNewPc2ngToAttach = em.getReference(pc2ngCollectionNewPc2ngToAttach.getClass(), pc2ngCollectionNewPc2ngToAttach.getPc2ngId());
                attachedPc2ngCollectionNew.add(pc2ngCollectionNewPc2ngToAttach);
            }
            pc2ngCollectionNew = attachedPc2ngCollectionNew;
            ngPool.setPc2ngCollection(pc2ngCollectionNew);
            ngPool = em.merge(ngPool);
            for (Pc2ng pc2ngCollectionNewPc2ng : pc2ngCollectionNew) {
                if (!pc2ngCollectionOld.contains(pc2ngCollectionNewPc2ng)) {
                    NgPool oldIdNgOfPc2ngCollectionNewPc2ng = pc2ngCollectionNewPc2ng.getIdNg();
                    pc2ngCollectionNewPc2ng.setIdNg(ngPool);
                    pc2ngCollectionNewPc2ng = em.merge(pc2ngCollectionNewPc2ng);
                    if (oldIdNgOfPc2ngCollectionNewPc2ng != null && !oldIdNgOfPc2ngCollectionNewPc2ng.equals(ngPool)) {
                        oldIdNgOfPc2ngCollectionNewPc2ng.getPc2ngCollection().remove(pc2ngCollectionNewPc2ng);
                        oldIdNgOfPc2ngCollectionNewPc2ng = em.merge(oldIdNgOfPc2ngCollectionNewPc2ng);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = ngPool.getIdNg();
                if (findNgPool(id) == null) {
                    throw new NonexistentEntityException("The ngPool with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            NgPool ngPool;
            try {
                ngPool = em.getReference(NgPool.class, id);
                ngPool.getIdNg();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ngPool with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Pc2ng> pc2ngCollectionOrphanCheck = ngPool.getPc2ngCollection();
            for (Pc2ng pc2ngCollectionOrphanCheckPc2ng : pc2ngCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This NgPool (" + ngPool + ") cannot be destroyed since the Pc2ng " + pc2ngCollectionOrphanCheckPc2ng + " in its pc2ngCollection field has a non-nullable idNg field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(ngPool);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<NgPool> findNgPoolEntities() {
        return findNgPoolEntities(true, -1, -1);
    }

    public List<NgPool> findNgPoolEntities(int maxResults, int firstResult) {
        return findNgPoolEntities(false, maxResults, firstResult);
    }

    private List<NgPool> findNgPoolEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(NgPool.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public NgPool findNgPool(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(NgPool.class, id);
        } finally {
            em.close();
        }
    }
    //---find 
    public List<NgPool> findNgName() {
        EntityManager em = getEntityManager();
        try {
            Query q  = em.createNamedQuery("NgPool.findAll");
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public int getNgPoolCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<NgPool> rt = cq.from(NgPool.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    public NgPool findByNgName(String ngName)
    {
        EntityManager em  = getEntityManager();
        try
        {
            Query q  = em.createNamedQuery("NgPool.findByNgName");
            q.setParameter("ngName", ngName);
            return  (NgPool) q.getSingleResult();
        }
        catch(Exception e)
        {
            return null; 
        }
        finally
        {
            em.close();
        } 
    } 
    
     public List<NgPool> findByNotdeleted()
    {
        EntityManager em  = getEntityManager();
        try
        {
            Query q  = em.createNamedQuery("NgPool.findByNotdeleted");
            return    q.getResultList();
        }
        catch(Exception e)
        {
            return null; 
        }
        finally
        {
            em.close();
        } 
    } 
 
    public List<NgPool>  findByNgNameList(String ngName)
    {
       EntityManager em  = getEntityManager();
        try
        {
            Query q  = em.createNamedQuery("NgPool.findByNgName");
            q.setParameter("ngName",ngName+"%");
            return    q.getResultList();
        }
        catch(Exception e)
        {
            return null; 
        }
        finally
        {
            em.close();
        } 
    }
    public List<NgPool>  findByNgCode(String ngCode)
    {
        EntityManager em  = getEntityManager();
        try
        {
            Query q  = em.createNamedQuery("NgPool.findByNgCode");
            q.setParameter("ngCode",ngCode+"%");
            return    q.getResultList();
        }
        catch(Exception e)
        {
            return null; 
        }
        finally
        {
            em.close();
        } 
    }
    public List<NgPool> findByNgType(String ngType)
    {
        EntityManager em  = getEntityManager();
        try
        {
            Query q  = em.createNamedQuery("NgPool.findByNgType");
            q.setParameter("ngType",ngType);
            return    q.getResultList();
        }
        catch(Exception e)
        {
            return null; 
        }
        finally
        {
            em.close();
        } 
    }
    public List<NgPool> findByNgStatus(String ngStatus)
    {
        EntityManager em  = getEntityManager();
        try
        {
            Query q  = em.createNamedQuery("NgPool.findByStatus");
            q.setParameter("status",ngStatus);
            return    q.getResultList();
        }
        catch(Exception e)
        {
            return null; 
        }
        finally
        {
            em.close();
        } 
    }  
}