/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data.jpa;

import com.tct.data.ReportSetting;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.ReportSettingAttribute;
import com.tct.data.jpa.exceptions.IllegalOrphanException;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Oct 18, 2012, Time : 6:53:03 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class ReportSettingJpaController implements Serializable {

    public ReportSettingJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ReportSetting reportSetting) throws PreexistingEntityException, Exception {
        if (reportSetting.getReportSettingAttributeCollection() == null) {
            reportSetting.setReportSettingAttributeCollection(new ArrayList<ReportSettingAttribute>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<ReportSettingAttribute> attachedReportSettingAttributeCollection = new ArrayList<ReportSettingAttribute>();
            for (ReportSettingAttribute reportSettingAttributeCollectionReportSettingAttributeToAttach : reportSetting.getReportSettingAttributeCollection()) {
                reportSettingAttributeCollectionReportSettingAttributeToAttach = em.getReference(reportSettingAttributeCollectionReportSettingAttributeToAttach.getClass(), reportSettingAttributeCollectionReportSettingAttributeToAttach.getIdReportSettingAtt());
                attachedReportSettingAttributeCollection.add(reportSettingAttributeCollectionReportSettingAttributeToAttach);
            }
            reportSetting.setReportSettingAttributeCollection(attachedReportSettingAttributeCollection);
            em.persist(reportSetting);
            for (ReportSettingAttribute reportSettingAttributeCollectionReportSettingAttribute : reportSetting.getReportSettingAttributeCollection()) {
                ReportSetting oldIdReportSettingOfReportSettingAttributeCollectionReportSettingAttribute = reportSettingAttributeCollectionReportSettingAttribute.getIdReportSetting();
                reportSettingAttributeCollectionReportSettingAttribute.setIdReportSetting(reportSetting);
                reportSettingAttributeCollectionReportSettingAttribute = em.merge(reportSettingAttributeCollectionReportSettingAttribute);
                if (oldIdReportSettingOfReportSettingAttributeCollectionReportSettingAttribute != null) {
                    oldIdReportSettingOfReportSettingAttributeCollectionReportSettingAttribute.getReportSettingAttributeCollection().remove(reportSettingAttributeCollectionReportSettingAttribute);
                    oldIdReportSettingOfReportSettingAttributeCollectionReportSettingAttribute = em.merge(oldIdReportSettingOfReportSettingAttributeCollectionReportSettingAttribute);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findReportSetting(reportSetting.getIdReportSetting()) != null) {
                throw new PreexistingEntityException("ReportSetting " + reportSetting + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ReportSetting reportSetting) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ReportSetting persistentReportSetting = em.find(ReportSetting.class, reportSetting.getIdReportSetting());
            Collection<ReportSettingAttribute> reportSettingAttributeCollectionOld = persistentReportSetting.getReportSettingAttributeCollection();
            Collection<ReportSettingAttribute> reportSettingAttributeCollectionNew = reportSetting.getReportSettingAttributeCollection();
            List<String> illegalOrphanMessages = null;
            for (ReportSettingAttribute reportSettingAttributeCollectionOldReportSettingAttribute : reportSettingAttributeCollectionOld) {
                if (!reportSettingAttributeCollectionNew.contains(reportSettingAttributeCollectionOldReportSettingAttribute)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ReportSettingAttribute " + reportSettingAttributeCollectionOldReportSettingAttribute + " since its idReportSetting field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<ReportSettingAttribute> attachedReportSettingAttributeCollectionNew = new ArrayList<ReportSettingAttribute>();
            for (ReportSettingAttribute reportSettingAttributeCollectionNewReportSettingAttributeToAttach : reportSettingAttributeCollectionNew) {
                reportSettingAttributeCollectionNewReportSettingAttributeToAttach = em.getReference(reportSettingAttributeCollectionNewReportSettingAttributeToAttach.getClass(), reportSettingAttributeCollectionNewReportSettingAttributeToAttach.getIdReportSettingAtt());
                attachedReportSettingAttributeCollectionNew.add(reportSettingAttributeCollectionNewReportSettingAttributeToAttach);
            }
            reportSettingAttributeCollectionNew = attachedReportSettingAttributeCollectionNew;
            reportSetting.setReportSettingAttributeCollection(reportSettingAttributeCollectionNew);
            reportSetting = em.merge(reportSetting);
            for (ReportSettingAttribute reportSettingAttributeCollectionNewReportSettingAttribute : reportSettingAttributeCollectionNew) {
                if (!reportSettingAttributeCollectionOld.contains(reportSettingAttributeCollectionNewReportSettingAttribute)) {
                    ReportSetting oldIdReportSettingOfReportSettingAttributeCollectionNewReportSettingAttribute = reportSettingAttributeCollectionNewReportSettingAttribute.getIdReportSetting();
                    reportSettingAttributeCollectionNewReportSettingAttribute.setIdReportSetting(reportSetting);
                    reportSettingAttributeCollectionNewReportSettingAttribute = em.merge(reportSettingAttributeCollectionNewReportSettingAttribute);
                    if (oldIdReportSettingOfReportSettingAttributeCollectionNewReportSettingAttribute != null && !oldIdReportSettingOfReportSettingAttributeCollectionNewReportSettingAttribute.equals(reportSetting)) {
                        oldIdReportSettingOfReportSettingAttributeCollectionNewReportSettingAttribute.getReportSettingAttributeCollection().remove(reportSettingAttributeCollectionNewReportSettingAttribute);
                        oldIdReportSettingOfReportSettingAttributeCollectionNewReportSettingAttribute = em.merge(oldIdReportSettingOfReportSettingAttributeCollectionNewReportSettingAttribute);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = reportSetting.getIdReportSetting();
                if (findReportSetting(id) == null) {
                    throw new NonexistentEntityException("The reportSetting with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ReportSetting reportSetting;
            try {
                reportSetting = em.getReference(ReportSetting.class, id);
                reportSetting.getIdReportSetting();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The reportSetting with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<ReportSettingAttribute> reportSettingAttributeCollectionOrphanCheck = reportSetting.getReportSettingAttributeCollection();
            for (ReportSettingAttribute reportSettingAttributeCollectionOrphanCheckReportSettingAttribute : reportSettingAttributeCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This ReportSetting (" + reportSetting + ") cannot be destroyed since the ReportSettingAttribute " + reportSettingAttributeCollectionOrphanCheckReportSettingAttribute + " in its reportSettingAttributeCollection field has a non-nullable idReportSetting field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(reportSetting);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ReportSetting> findReportSettingEntities() {
        return findReportSettingEntities(true, -1, -1);
    }

    public List<ReportSetting> findReportSettingEntities(int maxResults, int firstResult) {
        return findReportSettingEntities(false, maxResults, firstResult);
    }

    private List<ReportSetting> findReportSettingEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ReportSetting.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ReportSetting findReportSetting(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ReportSetting.class, id);
        } finally {
            em.close();
        }
    }

    public int getReportSettingCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ReportSetting> rt = cq.from(ReportSetting.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
//    findallNotDeleted
    public List<ReportSetting> findAllNotDeleted()
    {
        EntityManager em = getEntityManager();
        try
        {
            Query  q = em.createQuery("SELECT r FROM ReportSetting  r WHERE  r.deleted = 0");
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }
    //find name report Setting
    public ReportSetting  findNameReportSettingNotDeleted(String nameSetting)
    {
        EntityManager em  = getEntityManager();
        try
        {
            Query q  = em.createQuery("SELECT r FROM ReportSetting r WHERE r.nameSetting =:nameSetting");
            q.setParameter("nameSetting",nameSetting);
            return (ReportSetting)  q.getSingleResult();
        }
        finally
        {
            em.close();
        }
    }
}
