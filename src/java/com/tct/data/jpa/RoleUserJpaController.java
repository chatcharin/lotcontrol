/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import com.appCinfigpage.bean.fc;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.Role;
import com.tct.data.RoleUser;
import com.tct.data.Users;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author The_Boy_Cs
 */
public class RoleUserJpaController implements Serializable {

    public RoleUserJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(RoleUser roleUser) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Role roleId = roleUser.getRoleId();
            if (roleId != null) {
                roleId = em.getReference(roleId.getClass(), roleId.getIdRole());
                roleUser.setRoleId(roleId);
            }
            Users usersId = roleUser.getUsersId();
            if (usersId != null) {
                usersId = em.getReference(usersId.getClass(), usersId.getId());
                roleUser.setUsersId(usersId);
            }
            em.persist(roleUser);
            if (roleId != null) {
                roleId.getRoleUserCollection().add(roleUser);
                roleId = em.merge(roleId);
            }
            if (usersId != null) {
                usersId.getRoleUserCollection().add(roleUser);
                usersId = em.merge(usersId);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findRoleUser(roleUser.getId()) != null) {
                throw new PreexistingEntityException("RoleUser " + roleUser + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(RoleUser roleUser) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            RoleUser persistentRoleUser = em.find(RoleUser.class, roleUser.getId());
            Role roleIdOld = persistentRoleUser.getRoleId();
            Role roleIdNew = roleUser.getRoleId();
            Users usersIdOld = persistentRoleUser.getUsersId();
            Users usersIdNew = roleUser.getUsersId();
            if (roleIdNew != null) {
                roleIdNew = em.getReference(roleIdNew.getClass(), roleIdNew.getIdRole());
                roleUser.setRoleId(roleIdNew);
            }
            if (usersIdNew != null) {
                usersIdNew = em.getReference(usersIdNew.getClass(), usersIdNew.getId());
                roleUser.setUsersId(usersIdNew);
            }
            roleUser = em.merge(roleUser);
            if (roleIdOld != null && !roleIdOld.equals(roleIdNew)) {
                roleIdOld.getRoleUserCollection().remove(roleUser);
                roleIdOld = em.merge(roleIdOld);
            }
            if (roleIdNew != null && !roleIdNew.equals(roleIdOld)) {
                roleIdNew.getRoleUserCollection().add(roleUser);
                roleIdNew = em.merge(roleIdNew);
            }
            if (usersIdOld != null && !usersIdOld.equals(usersIdNew)) {
                usersIdOld.getRoleUserCollection().remove(roleUser);
                usersIdOld = em.merge(usersIdOld);
            }
            if (usersIdNew != null && !usersIdNew.equals(usersIdOld)) {
                usersIdNew.getRoleUserCollection().add(roleUser);
                usersIdNew = em.merge(usersIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = roleUser.getId();
                if (findRoleUser(id) == null) {
                    throw new NonexistentEntityException("The roleUser with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
    public void destroyByRoleUser(RoleUser roleuser) throws NonexistentEntityException{
        destroy(roleuser.getId());
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            RoleUser roleUser;
            try {
                roleUser = em.getReference(RoleUser.class, id);
                roleUser.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The roleUser with id " + id + " no longer exists.", enfe);
            }
            Role roleId = roleUser.getRoleId();
            if (roleId != null) {
                roleId.getRoleUserCollection().remove(roleUser);
                roleId = em.merge(roleId);
            }
            Users usersId = roleUser.getUsersId();
            if (usersId != null) {
                usersId.getRoleUserCollection().remove(roleUser);
                usersId = em.merge(usersId);
            }
            em.remove(roleUser);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<RoleUser> findRoleUserEntities() {
        return findRoleUserEntities(true, -1, -1);
    }

    public List<RoleUser> findRoleUserEntities(int maxResults, int firstResult) {
        return findRoleUserEntities(false, maxResults, firstResult);
    }

    private List<RoleUser> findRoleUserEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(RoleUser.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public RoleUser findRoleUser(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(RoleUser.class, id);
        } 
        catch(Exception e)
        {
            fc.print("Null"+e);
            return  null;
        }
        finally {
            em.close();
        }
    }
    
    public List<RoleUser> findByRoleName(String rolename){
        Query query = null;
        EntityManager em = getEntityManager();
        try{
            System.out.println("RoleName : "+rolename);
            query = em.createNamedQuery("RoleUser.fildByRoleName").setParameter("roleName", "%"+rolename+"%");
            return query.getResultList();
        }finally{
            em.close();
        }
    }
    
    public List<RoleUser> findByUserId(Users user){
        
        Query query = null;
        EntityManager em = getEntityManager();
        try{
            System.out.println("UserId : "+user.getId());
            query = em.createNamedQuery("RoleUser.findByUserId");
            query.setParameter("usersId", user);
            return query.getResultList();
        }finally{
            em.close();
        }
    }

    public int getRoleUserCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<RoleUser> rt = cq.from(RoleUser.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    } 
}
