/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import com.tct.data.Back2pack;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.Details;
import com.tct.data.LotControl;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author The_Boy_Cs
 */
public class Back2packJpaController implements Serializable {

    public Back2packJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Back2pack back2pack) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Details detailsIdDetail = back2pack.getDetailsIdDetail();
            if (detailsIdDetail != null) {
                detailsIdDetail = em.getReference(detailsIdDetail.getClass(), detailsIdDetail.getIdDetail());
                back2pack.setDetailsIdDetail(detailsIdDetail);
            }
            LotControl lotcontrolId = back2pack.getLotcontrolId();
            if (lotcontrolId != null) {
                lotcontrolId = em.getReference(lotcontrolId.getClass(), lotcontrolId.getId());
                back2pack.setLotcontrolId(lotcontrolId);
            }
            em.persist(back2pack);
            if (detailsIdDetail != null) {
                detailsIdDetail.getBack2packCollection().add(back2pack);
                detailsIdDetail = em.merge(detailsIdDetail);
            }
            if (lotcontrolId != null) {
                lotcontrolId.getBack2packCollection().add(back2pack);
                lotcontrolId = em.merge(lotcontrolId);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findBack2pack(back2pack.getBack2packId()) != null) {
                throw new PreexistingEntityException("Back2pack " + back2pack + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Back2pack back2pack) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Back2pack persistentBack2pack = em.find(Back2pack.class, back2pack.getBack2packId());
            Details detailsIdDetailOld = persistentBack2pack.getDetailsIdDetail();
            Details detailsIdDetailNew = back2pack.getDetailsIdDetail();
            LotControl lotcontrolIdOld = persistentBack2pack.getLotcontrolId();
            LotControl lotcontrolIdNew = back2pack.getLotcontrolId();
            if (detailsIdDetailNew != null) {
                detailsIdDetailNew = em.getReference(detailsIdDetailNew.getClass(), detailsIdDetailNew.getIdDetail());
                back2pack.setDetailsIdDetail(detailsIdDetailNew);
            }
            if (lotcontrolIdNew != null) {
                lotcontrolIdNew = em.getReference(lotcontrolIdNew.getClass(), lotcontrolIdNew.getId());
                back2pack.setLotcontrolId(lotcontrolIdNew);
            }
            back2pack = em.merge(back2pack);
            if (detailsIdDetailOld != null && !detailsIdDetailOld.equals(detailsIdDetailNew)) {
                detailsIdDetailOld.getBack2packCollection().remove(back2pack);
                detailsIdDetailOld = em.merge(detailsIdDetailOld);
            }
            if (detailsIdDetailNew != null && !detailsIdDetailNew.equals(detailsIdDetailOld)) {
                detailsIdDetailNew.getBack2packCollection().add(back2pack);
                detailsIdDetailNew = em.merge(detailsIdDetailNew);
            }
            if (lotcontrolIdOld != null && !lotcontrolIdOld.equals(lotcontrolIdNew)) {
                lotcontrolIdOld.getBack2packCollection().remove(back2pack);
                lotcontrolIdOld = em.merge(lotcontrolIdOld);
            }
            if (lotcontrolIdNew != null && !lotcontrolIdNew.equals(lotcontrolIdOld)) {
                lotcontrolIdNew.getBack2packCollection().add(back2pack);
                lotcontrolIdNew = em.merge(lotcontrolIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = back2pack.getBack2packId();
                if (findBack2pack(id) == null) {
                    throw new NonexistentEntityException("The back2pack with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Back2pack back2pack;
            try {
                back2pack = em.getReference(Back2pack.class, id);
                back2pack.getBack2packId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The back2pack with id " + id + " no longer exists.", enfe);
            }
            Details detailsIdDetail = back2pack.getDetailsIdDetail();
            if (detailsIdDetail != null) {
                detailsIdDetail.getBack2packCollection().remove(back2pack);
                detailsIdDetail = em.merge(detailsIdDetail);
            }
            LotControl lotcontrolId = back2pack.getLotcontrolId();
            if (lotcontrolId != null) {
                lotcontrolId.getBack2packCollection().remove(back2pack);
                lotcontrolId = em.merge(lotcontrolId);
            }
            em.remove(back2pack);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Back2pack> findBack2packEntities() {
        return findBack2packEntities(true, -1, -1);
    }

    public List<Back2pack> findBack2packEntities(int maxResults, int firstResult) {
        return findBack2packEntities(false, maxResults, firstResult);
    }

    private List<Back2pack> findBack2packEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Back2pack.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Back2pack findBack2pack(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Back2pack.class, id);
        } finally {
            em.close();
        }
    }

    public int getBack2packCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Back2pack> rt = cq.from(Back2pack.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
