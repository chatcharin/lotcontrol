/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import com.tct.data.Countbarcodeid;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author The_Boy_Cs
 */
public class CountbarcodeidJpaController implements Serializable {

    public CountbarcodeidJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Countbarcodeid countbarcodeid) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(countbarcodeid);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Countbarcodeid countbarcodeid) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            countbarcodeid = em.merge(countbarcodeid);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = countbarcodeid.getId();
                if (findCountbarcodeid(id) == null) {
                    throw new NonexistentEntityException("The countbarcodeid with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Countbarcodeid countbarcodeid;
            try {
                countbarcodeid = em.getReference(Countbarcodeid.class, id);
                countbarcodeid.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The countbarcodeid with id " + id + " no longer exists.", enfe);
            }
            em.remove(countbarcodeid);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Countbarcodeid> findCountbarcodeidEntities() {
        return findCountbarcodeidEntities(true, -1, -1);
    }

    public List<Countbarcodeid> findCountbarcodeidEntities(int maxResults, int firstResult) {
        return findCountbarcodeidEntities(false, maxResults, firstResult);
    }

    private List<Countbarcodeid> findCountbarcodeidEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Countbarcodeid.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Countbarcodeid findCountbarcodeid(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Countbarcodeid.class, id);
        } finally {
            em.close();
        }
    }

    public int getCountbarcodeidCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Countbarcodeid> rt = cq.from(Countbarcodeid.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    public Countbarcodeid findCountbarcodeidByMaxnum(){
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("Countbarcodeid.findByMaxId");
            return (Countbarcodeid) query.getSingleResult();
        }catch(Exception e){
            System.out.println("= Error : "+e);
            return null;
        }finally{
            em.close();
        }
    }
    public Countbarcodeid findCountbarcodeidByMaxnumType(String typeWorksheetId){
        EntityManager em = getEntityManager();
        try {
            Query query = em.createQuery("SELECT c FROM Countbarcodeid c WHERE c.id = (SELECT MAX(cc.id) FROM Countbarcodeid cc WHERE cc.typeWorksheetId =:typeWorksheetId)");
            query.setParameter("typeWorksheetId",typeWorksheetId);
            return (Countbarcodeid) query.getSingleResult();
        }catch(Exception e){
            System.out.println("= Error : "+e);
            return null;
        }finally{
            em.close();
        }
    }
}
