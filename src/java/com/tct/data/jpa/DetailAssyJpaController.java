/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import com.tct.data.DetailAssy;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.Details;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author The_Boy_Cs
 */
public class DetailAssyJpaController implements Serializable {

    public DetailAssyJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DetailAssy detailAssy) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Details detailIdDetail = detailAssy.getDetailIdDetail();
            if (detailIdDetail != null) {
                detailIdDetail = em.getReference(detailIdDetail.getClass(), detailIdDetail.getIdDetail());
                detailAssy.setDetailIdDetail(detailIdDetail);
            }
            em.persist(detailAssy);
            if (detailIdDetail != null) {
                detailIdDetail.getDetailAssyCollection().add(detailAssy);
                detailIdDetail = em.merge(detailIdDetail);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDetailAssy(detailAssy.getId()) != null) {
                throw new PreexistingEntityException("DetailAssy " + detailAssy + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DetailAssy detailAssy) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailAssy persistentDetailAssy = em.find(DetailAssy.class, detailAssy.getId());
            Details detailIdDetailOld = persistentDetailAssy.getDetailIdDetail();
            Details detailIdDetailNew = detailAssy.getDetailIdDetail();
            if (detailIdDetailNew != null) {
                detailIdDetailNew = em.getReference(detailIdDetailNew.getClass(), detailIdDetailNew.getIdDetail());
                detailAssy.setDetailIdDetail(detailIdDetailNew);
            }
            detailAssy = em.merge(detailAssy);
            if (detailIdDetailOld != null && !detailIdDetailOld.equals(detailIdDetailNew)) {
                detailIdDetailOld.getDetailAssyCollection().remove(detailAssy);
                detailIdDetailOld = em.merge(detailIdDetailOld);
            }
            if (detailIdDetailNew != null && !detailIdDetailNew.equals(detailIdDetailOld)) {
                detailIdDetailNew.getDetailAssyCollection().add(detailAssy);
                detailIdDetailNew = em.merge(detailIdDetailNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = detailAssy.getId();
                if (findDetailAssy(id) == null) {
                    throw new NonexistentEntityException("The detailAssy with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailAssy detailAssy;
            try {
                detailAssy = em.getReference(DetailAssy.class, id);
                detailAssy.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The detailAssy with id " + id + " no longer exists.", enfe);
            }
            Details detailIdDetail = detailAssy.getDetailIdDetail();
            if (detailIdDetail != null) {
                detailIdDetail.getDetailAssyCollection().remove(detailAssy);
                detailIdDetail = em.merge(detailIdDetail);
            }
            em.remove(detailAssy);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DetailAssy> findDetailAssyEntities() {
        return findDetailAssyEntities(true, -1, -1);
    }

    public List<DetailAssy> findDetailAssyEntities(int maxResults, int firstResult) {
        return findDetailAssyEntities(false, maxResults, firstResult);
    }

    private List<DetailAssy> findDetailAssyEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(DetailAssy.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DetailAssy findDetailAssy(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DetailAssy.class, id);
        } finally {
            em.close();
        }
    }

    public int getDetailAssyCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<DetailAssy> rt = cq.from(DetailAssy.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    //--find  assy data
    public List<DetailAssy>  findDataDetailAssyList(Details idDetails){
        EntityManager  em = getEntityManager();
        try{
            Query  q  = em.createNamedQuery("DetailAssy.findBydetailIdDetail");
            q.setParameter("detailIdDetail",idDetails);
            return q.getResultList(); 
        }
        finally{
            em.close();
        }
    
    }

    public DetailAssy findDetailFlByDetails(Details detail){
        EntityManager em = getEntityManager();
        Query query;
        try {
            query = em.createNamedQuery("DetailAssy.findBydetailIdDetail");
            query.setParameter("detailIdDetail", detail);
            return (DetailAssy) query.getSingleResult();
        }finally{
            em.close();
        }
    }
    public DetailAssy  findDataDetailAssyListSingle(Details idDetails){
        EntityManager  em = getEntityManager();
        try{
            Query  q  = em.createNamedQuery("DetailAssy.findBydetailIdDetail");
            q.setParameter("detailIdDetail",idDetails);
            return (DetailAssy)q.getSingleResult(); 
        }
        finally{
            em.close();
        }
    
    }
}
