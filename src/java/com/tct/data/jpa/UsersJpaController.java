/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.RoleUser;
import com.tct.data.Users;
import com.tct.data.jpa.exceptions.IllegalOrphanException;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author The_Boy_Cs
 */
public class UsersJpaController implements Serializable {

    public UsersJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Users users) throws PreexistingEntityException, Exception {
        if (users.getRoleUserCollection() == null) {
            users.setRoleUserCollection(new ArrayList<RoleUser>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<RoleUser> attachedRoleUserCollection = new ArrayList<RoleUser>();
            for (RoleUser roleUserCollectionRoleUserToAttach : users.getRoleUserCollection()) {
                roleUserCollectionRoleUserToAttach = em.getReference(roleUserCollectionRoleUserToAttach.getClass(), roleUserCollectionRoleUserToAttach.getId());
                attachedRoleUserCollection.add(roleUserCollectionRoleUserToAttach);
            }
            users.setRoleUserCollection(attachedRoleUserCollection);
            em.persist(users);
            for (RoleUser roleUserCollectionRoleUser : users.getRoleUserCollection()) {
                Users oldUsersIdOfRoleUserCollectionRoleUser = roleUserCollectionRoleUser.getUsersId();
                roleUserCollectionRoleUser.setUsersId(users);
                roleUserCollectionRoleUser = em.merge(roleUserCollectionRoleUser);
                if (oldUsersIdOfRoleUserCollectionRoleUser != null) {
                    oldUsersIdOfRoleUserCollectionRoleUser.getRoleUserCollection().remove(roleUserCollectionRoleUser);
                    oldUsersIdOfRoleUserCollectionRoleUser = em.merge(oldUsersIdOfRoleUserCollectionRoleUser);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findUsers(users.getId()) != null) {
                throw new PreexistingEntityException("Users " + users + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Users users) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users persistentUsers = em.find(Users.class, users.getId());
            Collection<RoleUser> roleUserCollectionOld = persistentUsers.getRoleUserCollection();
            Collection<RoleUser> roleUserCollectionNew = users.getRoleUserCollection();
            List<String> illegalOrphanMessages = null;
            for (RoleUser roleUserCollectionOldRoleUser : roleUserCollectionOld) {
                if (!roleUserCollectionNew.contains(roleUserCollectionOldRoleUser)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain RoleUser " + roleUserCollectionOldRoleUser + " since its usersId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<RoleUser> attachedRoleUserCollectionNew = new ArrayList<RoleUser>();
            for (RoleUser roleUserCollectionNewRoleUserToAttach : roleUserCollectionNew) {
                roleUserCollectionNewRoleUserToAttach = em.getReference(roleUserCollectionNewRoleUserToAttach.getClass(), roleUserCollectionNewRoleUserToAttach.getId());
                attachedRoleUserCollectionNew.add(roleUserCollectionNewRoleUserToAttach);
            }
            roleUserCollectionNew = attachedRoleUserCollectionNew;
            users.setRoleUserCollection(roleUserCollectionNew);
            users = em.merge(users);
            for (RoleUser roleUserCollectionNewRoleUser : roleUserCollectionNew) {
                if (!roleUserCollectionOld.contains(roleUserCollectionNewRoleUser)) {
                    Users oldUsersIdOfRoleUserCollectionNewRoleUser = roleUserCollectionNewRoleUser.getUsersId();
                    roleUserCollectionNewRoleUser.setUsersId(users);
                    roleUserCollectionNewRoleUser = em.merge(roleUserCollectionNewRoleUser);
                    if (oldUsersIdOfRoleUserCollectionNewRoleUser != null && !oldUsersIdOfRoleUserCollectionNewRoleUser.equals(users)) {
                        oldUsersIdOfRoleUserCollectionNewRoleUser.getRoleUserCollection().remove(roleUserCollectionNewRoleUser);
                        oldUsersIdOfRoleUserCollectionNewRoleUser = em.merge(oldUsersIdOfRoleUserCollectionNewRoleUser);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = users.getId();
                if (findUsers(id) == null) {
                    throw new NonexistentEntityException("The users with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users users;
            try {
                users = em.getReference(Users.class, id);
                users.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The users with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<RoleUser> roleUserCollectionOrphanCheck = users.getRoleUserCollection();
            for (RoleUser roleUserCollectionOrphanCheckRoleUser : roleUserCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Users (" + users + ") cannot be destroyed since the RoleUser " + roleUserCollectionOrphanCheckRoleUser + " in its roleUserCollection field has a non-nullable usersId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(users);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Users> findUsersEntities() {
        return findUsersEntities(true, -1, -1);
    }

    public List<Users> findUsersEntities(int maxResults, int firstResult) {
        return findUsersEntities(false, maxResults, firstResult);
    }

    private List<Users> findUsersEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Users.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Users findUsers(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Users.class, id);
        } finally {
            em.close();
        }
    }
    
    public List<Users> findAll(){
        Query query = null;
        List<Users> user = null;
        EntityManager em = getEntityManager();
        try{
            query = em.createNamedQuery("Users.findAll");
            if(query.getResultList().size()>0){
                user = query.getResultList();
            }
        }catch(Exception e){
            System.out.println("Error : "+e.toString());
        }finally{
            em.close();
        }
        return user;
    }

    public List<Users> findByName(String name) {
        Query query = null;
        List<Users> user = null;
        EntityManager em = getEntityManager();
        try {
//            String sql = "SELECT u FROM Users u WHERE u.firstName LIKE :firstName";
            query = em.createNamedQuery("Users.findByFirstName").setParameter("firstName", "%"+name+"%");
            
            if(query.getResultList().size()>0){
                user = query.getResultList();
            }
        } catch (Exception e) {
            System.out.println("Error 2 :" + e.toString());
        }
        return user;
    }

    public Users findByUsername(String username) {
        Query query = null;
        Users user = null;
        EntityManager em = getEntityManager();
        try {
//            String sql = "SELECT u FROM Users u WHERE u.userName = :userName";
            query = em.createNamedQuery("Users.findByUserName").setParameter("userName", "%"+username+"%");
                       if (query.getResultList().size() >= 1) {
                user = (Users) query.getResultList().get(0);
            }else {
                System.out.println(" Not data");
            }
        } catch (Exception e) {
            System.out.println("Error 2 :" + e.toString());
        } finally {
            em.close();
        }
        return user;
    }

    public int getUsersCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Users> rt = cq.from(Users.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    //@pang
     public List<Users> findByUsernameList(String username) {
        Query query = null;
        Users user = null;
        EntityManager em = getEntityManager();
        try {
//            String sql = "SELECT u FROM Users u WHERE u.userName = :userName";
            query = em.createNamedQuery("Users.findByUserName").setParameter("userName", "%"+username+"%");
            if (query.getResultList().size() == 1) {
                user = (Users) query.getSingleResult();
            }
            return  query.getResultList();
        } catch (Exception e) {
            System.out.println("Error 2 :" + e.toString());
            return null;
        } finally {
            em.close();
        } 
         
    }
    public List<Users> findByFisrtNameList(String fisrtName)
    {
         Query query = null;
        Users user = null;
        EntityManager em = getEntityManager();
        try {
//            String sql = "SELECT u FROM Users u WHERE u.userName = :userName";
            query = em.createNamedQuery("Users.findByFirstName").setParameter("firstName", "%"+fisrtName+"%");
            if (query.getResultList().size() == 1) {
                user = (Users) query.getSingleResult();
            }
            return  query.getResultList();
        } catch (Exception e) {
            System.out.println("Error 2 :" + e.toString());
            return null;
        } finally {
            em.close();
        } 
    } 
//    public List<Users> findByPositionList(String position)
//    {
//        
//        Query query = null;
//        Users user = null;
//        EntityManager em = getEntityManager();
//        try {
////            String sql = "SELECT u FROM Users u WHERE u.userName = :userName";
//            query = em.createNamedQuery("Users.findByFirstName").setParameter("firstName", "%"+position+"%");
//            if (query.getResultList().size() == 1) {
//                user = (Users) query.getSingleResult();
//            }
//            return  query.getResultList();
//        } catch (Exception e) {
//            System.out.println("Error 2 :" + e.toString());
//            return null;
//        } finally {
//            em.close();
//        } 
//    }
}
