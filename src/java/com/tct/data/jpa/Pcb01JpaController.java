/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.Details;
import com.tct.data.Pcb01;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author The_Boy_Cs
 */
public class Pcb01JpaController implements Serializable {

    public Pcb01JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Pcb01 pcb01) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Details detailIdDetail = pcb01.getDetailIdDetail();
            if (detailIdDetail != null) {
                detailIdDetail = em.getReference(detailIdDetail.getClass(), detailIdDetail.getIdDetail());
                pcb01.setDetailIdDetail(detailIdDetail);
            }
            em.persist(pcb01);
            if (detailIdDetail != null) {
                detailIdDetail.getPcb01Collection().add(pcb01);
                detailIdDetail = em.merge(detailIdDetail);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPcb01(pcb01.getId()) != null) {
                throw new PreexistingEntityException("Pcb01 " + pcb01 + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Pcb01 pcb01) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pcb01 persistentPcb01 = em.find(Pcb01.class, pcb01.getId());
            Details detailIdDetailOld = persistentPcb01.getDetailIdDetail();
            Details detailIdDetailNew = pcb01.getDetailIdDetail();
            if (detailIdDetailNew != null) {
                detailIdDetailNew = em.getReference(detailIdDetailNew.getClass(), detailIdDetailNew.getIdDetail());
                pcb01.setDetailIdDetail(detailIdDetailNew);
            }
            pcb01 = em.merge(pcb01);
            if (detailIdDetailOld != null && !detailIdDetailOld.equals(detailIdDetailNew)) {
                detailIdDetailOld.getPcb01Collection().remove(pcb01);
                detailIdDetailOld = em.merge(detailIdDetailOld);
            }
            if (detailIdDetailNew != null && !detailIdDetailNew.equals(detailIdDetailOld)) {
                detailIdDetailNew.getPcb01Collection().add(pcb01);
                detailIdDetailNew = em.merge(detailIdDetailNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = pcb01.getId();
                if (findPcb01(id) == null) {
                    throw new NonexistentEntityException("The pcb01 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pcb01 pcb01;
            try {
                pcb01 = em.getReference(Pcb01.class, id);
                pcb01.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The pcb01 with id " + id + " no longer exists.", enfe);
            }
            Details detailIdDetail = pcb01.getDetailIdDetail();
            if (detailIdDetail != null) {
                detailIdDetail.getPcb01Collection().remove(pcb01);
                detailIdDetail = em.merge(detailIdDetail);
            }
            em.remove(pcb01);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Pcb01> findPcb01Entities() {
        return findPcb01Entities(true, -1, -1);
    }

    public List<Pcb01> findPcb01Entities(int maxResults, int firstResult) {
        return findPcb01Entities(false, maxResults, firstResult);
    }

    private List<Pcb01> findPcb01Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Pcb01.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Pcb01 findPcb01(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Pcb01.class, id);
        } finally {
            em.close();
        }
    }

    public int getPcb01Count() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Pcb01> rt = cq.from(Pcb01.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    //--find list packing data
    public List<Pcb01>  findPcb01ListData(Details idDetail){
        EntityManager em = getEntityManager();
        try{
            Query q  = em.createNamedQuery("Pcb01.findBydetailIdDetail");
            q.setParameter("detailIdDetail",idDetail);
            return q.getResultList();
        }
        finally{
            em.close();
        }
    }
    public Pcb01  findPcb01ListDataSingle(Details idDetail){
        EntityManager em = getEntityManager();
        try{
            Query q  = em.createNamedQuery("Pcb01.findBydetailIdDetail");
            q.setParameter("detailIdDetail",idDetail);
            return (Pcb01)q.getSingleResult();
        }
        finally{
            em.close();
        }
    }
}
