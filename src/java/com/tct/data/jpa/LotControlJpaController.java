/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data.jpa;

import com.tct.data.*;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import com.tct.data.jpa.exceptions.IllegalOrphanException;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 22, 2012, Time : 11:14:33 AM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class LotControlJpaController implements Serializable {

      public LotControlJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(LotControl lotControl) throws PreexistingEntityException, Exception {
        if (lotControl.getFront2backMasterCollection() == null) {
            lotControl.setFront2backMasterCollection(new ArrayList<Front2backMaster>());
        }
        if (lotControl.getAssy2autoCollection() == null) {
            lotControl.setAssy2autoCollection(new ArrayList<Assy2auto>());
        }
        if (lotControl.getFront2backCollection() == null) {
            lotControl.setFront2backCollection(new ArrayList<Front2back>());
        }
        if (lotControl.getBack2packCollection() == null) {
            lotControl.setBack2packCollection(new ArrayList<Back2pack>());
        }
        if (lotControl.getMainDataCollection() == null) {
            lotControl.setMainDataCollection(new ArrayList<MainData>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ModelPool modelPoolId = lotControl.getModelPoolId();
            if (modelPoolId != null) {
                modelPoolId = em.getReference(modelPoolId.getClass(), modelPoolId.getId());
                lotControl.setModelPoolId(modelPoolId);
            }
            Details detailsIdDetail = lotControl.getDetailsIdDetail();
            if (detailsIdDetail != null) {
                detailsIdDetail = em.getReference(detailsIdDetail.getClass(), detailsIdDetail.getIdDetail());
                lotControl.setDetailsIdDetail(detailsIdDetail);
            }
            Collection<Front2backMaster> attachedFront2backMasterCollection = new ArrayList<Front2backMaster>();
            for (Front2backMaster front2backMasterCollectionFront2backMasterToAttach : lotControl.getFront2backMasterCollection()) {
                front2backMasterCollectionFront2backMasterToAttach = em.getReference(front2backMasterCollectionFront2backMasterToAttach.getClass(), front2backMasterCollectionFront2backMasterToAttach.getId());
                attachedFront2backMasterCollection.add(front2backMasterCollectionFront2backMasterToAttach);
            }
            lotControl.setFront2backMasterCollection(attachedFront2backMasterCollection);
            Collection<Assy2auto> attachedAssy2autoCollection = new ArrayList<Assy2auto>();
            for (Assy2auto assy2autoCollectionAssy2autoToAttach : lotControl.getAssy2autoCollection()) {
                assy2autoCollectionAssy2autoToAttach = em.getReference(assy2autoCollectionAssy2autoToAttach.getClass(), assy2autoCollectionAssy2autoToAttach.getId());
                attachedAssy2autoCollection.add(assy2autoCollectionAssy2autoToAttach);
            }
            lotControl.setAssy2autoCollection(attachedAssy2autoCollection);
            Collection<Front2back> attachedFront2backCollection = new ArrayList<Front2back>();
            for (Front2back front2backCollectionFront2backToAttach : lotControl.getFront2backCollection()) {
                front2backCollectionFront2backToAttach = em.getReference(front2backCollectionFront2backToAttach.getClass(), front2backCollectionFront2backToAttach.getFront2backId());
                attachedFront2backCollection.add(front2backCollectionFront2backToAttach);
            }
            lotControl.setFront2backCollection(attachedFront2backCollection);
            Collection<Back2pack> attachedBack2packCollection = new ArrayList<Back2pack>();
            for (Back2pack back2packCollectionBack2packToAttach : lotControl.getBack2packCollection()) {
                back2packCollectionBack2packToAttach = em.getReference(back2packCollectionBack2packToAttach.getClass(), back2packCollectionBack2packToAttach.getBack2packId());
                attachedBack2packCollection.add(back2packCollectionBack2packToAttach);
            }
            lotControl.setBack2packCollection(attachedBack2packCollection);
            Collection<MainData> attachedMainDataCollection = new ArrayList<MainData>();
            for (MainData mainDataCollectionMainDataToAttach : lotControl.getMainDataCollection()) {
                mainDataCollectionMainDataToAttach = em.getReference(mainDataCollectionMainDataToAttach.getClass(), mainDataCollectionMainDataToAttach.getId());
                attachedMainDataCollection.add(mainDataCollectionMainDataToAttach);
            }
            lotControl.setMainDataCollection(attachedMainDataCollection);
            em.persist(lotControl);
            if (modelPoolId != null) {
                modelPoolId.getLotControlCollection().add(lotControl);
                modelPoolId = em.merge(modelPoolId);
            }
            if (detailsIdDetail != null) {
                detailsIdDetail.getLotControlCollection().add(lotControl);
                detailsIdDetail = em.merge(detailsIdDetail);
            }
            for (Front2backMaster front2backMasterCollectionFront2backMaster : lotControl.getFront2backMasterCollection()) {
                LotControl oldLotcontrolIdOfFront2backMasterCollectionFront2backMaster = front2backMasterCollectionFront2backMaster.getLotcontrolId();
                front2backMasterCollectionFront2backMaster.setLotcontrolId(lotControl);
                front2backMasterCollectionFront2backMaster = em.merge(front2backMasterCollectionFront2backMaster);
                if (oldLotcontrolIdOfFront2backMasterCollectionFront2backMaster != null) {
                    oldLotcontrolIdOfFront2backMasterCollectionFront2backMaster.getFront2backMasterCollection().remove(front2backMasterCollectionFront2backMaster);
                    oldLotcontrolIdOfFront2backMasterCollectionFront2backMaster = em.merge(oldLotcontrolIdOfFront2backMasterCollectionFront2backMaster);
                }
            }
            for (Assy2auto assy2autoCollectionAssy2auto : lotControl.getAssy2autoCollection()) {
                LotControl oldLotcontrolIdOfAssy2autoCollectionAssy2auto = assy2autoCollectionAssy2auto.getLotcontrolId();
                assy2autoCollectionAssy2auto.setLotcontrolId(lotControl);
                assy2autoCollectionAssy2auto = em.merge(assy2autoCollectionAssy2auto);
                if (oldLotcontrolIdOfAssy2autoCollectionAssy2auto != null) {
                    oldLotcontrolIdOfAssy2autoCollectionAssy2auto.getAssy2autoCollection().remove(assy2autoCollectionAssy2auto);
                    oldLotcontrolIdOfAssy2autoCollectionAssy2auto = em.merge(oldLotcontrolIdOfAssy2autoCollectionAssy2auto);
                }
            }
            for (Front2back front2backCollectionFront2back : lotControl.getFront2backCollection()) {
                LotControl oldLotcontrolIdOfFront2backCollectionFront2back = front2backCollectionFront2back.getLotcontrolId();
                front2backCollectionFront2back.setLotcontrolId(lotControl);
                front2backCollectionFront2back = em.merge(front2backCollectionFront2back);
                if (oldLotcontrolIdOfFront2backCollectionFront2back != null) {
                    oldLotcontrolIdOfFront2backCollectionFront2back.getFront2backCollection().remove(front2backCollectionFront2back);
                    oldLotcontrolIdOfFront2backCollectionFront2back = em.merge(oldLotcontrolIdOfFront2backCollectionFront2back);
                }
            }
            for (Back2pack back2packCollectionBack2pack : lotControl.getBack2packCollection()) {
                LotControl oldLotcontrolIdOfBack2packCollectionBack2pack = back2packCollectionBack2pack.getLotcontrolId();
                back2packCollectionBack2pack.setLotcontrolId(lotControl);
                back2packCollectionBack2pack = em.merge(back2packCollectionBack2pack);
                if (oldLotcontrolIdOfBack2packCollectionBack2pack != null) {
                    oldLotcontrolIdOfBack2packCollectionBack2pack.getBack2packCollection().remove(back2packCollectionBack2pack);
                    oldLotcontrolIdOfBack2packCollectionBack2pack = em.merge(oldLotcontrolIdOfBack2packCollectionBack2pack);
                }
            }
            for (MainData mainDataCollectionMainData : lotControl.getMainDataCollection()) {
                LotControl oldLotControlIdOfMainDataCollectionMainData = mainDataCollectionMainData.getLotControlId();
                mainDataCollectionMainData.setLotControlId(lotControl);
                mainDataCollectionMainData = em.merge(mainDataCollectionMainData);
                if (oldLotControlIdOfMainDataCollectionMainData != null) {
                    oldLotControlIdOfMainDataCollectionMainData.getMainDataCollection().remove(mainDataCollectionMainData);
                    oldLotControlIdOfMainDataCollectionMainData = em.merge(oldLotControlIdOfMainDataCollectionMainData);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findLotControl(lotControl.getId()) != null) {
                throw new PreexistingEntityException("LotControl " + lotControl + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(LotControl lotControl) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            LotControl persistentLotControl = em.find(LotControl.class, lotControl.getId());
            ModelPool modelPoolIdOld = persistentLotControl.getModelPoolId();
            ModelPool modelPoolIdNew = lotControl.getModelPoolId();
            Details detailsIdDetailOld = persistentLotControl.getDetailsIdDetail();
            Details detailsIdDetailNew = lotControl.getDetailsIdDetail();
            Collection<Front2backMaster> front2backMasterCollectionOld = persistentLotControl.getFront2backMasterCollection();
            Collection<Front2backMaster> front2backMasterCollectionNew = lotControl.getFront2backMasterCollection();
            Collection<Assy2auto> assy2autoCollectionOld = persistentLotControl.getAssy2autoCollection();
            Collection<Assy2auto> assy2autoCollectionNew = lotControl.getAssy2autoCollection();
            Collection<Front2back> front2backCollectionOld = persistentLotControl.getFront2backCollection();
            Collection<Front2back> front2backCollectionNew = lotControl.getFront2backCollection();
            Collection<Back2pack> back2packCollectionOld = persistentLotControl.getBack2packCollection();
            Collection<Back2pack> back2packCollectionNew = lotControl.getBack2packCollection();
            Collection<MainData> mainDataCollectionOld = persistentLotControl.getMainDataCollection();
            Collection<MainData> mainDataCollectionNew = lotControl.getMainDataCollection();
            List<String> illegalOrphanMessages = null;
            for (Front2backMaster front2backMasterCollectionOldFront2backMaster : front2backMasterCollectionOld) {
                if (!front2backMasterCollectionNew.contains(front2backMasterCollectionOldFront2backMaster)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Front2backMaster " + front2backMasterCollectionOldFront2backMaster + " since its lotcontrolId field is not nullable.");
                }
            }
            for (Assy2auto assy2autoCollectionOldAssy2auto : assy2autoCollectionOld) {
                if (!assy2autoCollectionNew.contains(assy2autoCollectionOldAssy2auto)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Assy2auto " + assy2autoCollectionOldAssy2auto + " since its lotcontrolId field is not nullable.");
                }
            }
            for (Front2back front2backCollectionOldFront2back : front2backCollectionOld) {
                if (!front2backCollectionNew.contains(front2backCollectionOldFront2back)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Front2back " + front2backCollectionOldFront2back + " since its lotcontrolId field is not nullable.");
                }
            }
            for (Back2pack back2packCollectionOldBack2pack : back2packCollectionOld) {
                if (!back2packCollectionNew.contains(back2packCollectionOldBack2pack)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Back2pack " + back2packCollectionOldBack2pack + " since its lotcontrolId field is not nullable.");
                }
            }
            for (MainData mainDataCollectionOldMainData : mainDataCollectionOld) {
                if (!mainDataCollectionNew.contains(mainDataCollectionOldMainData)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain MainData " + mainDataCollectionOldMainData + " since its lotControlId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (modelPoolIdNew != null) {
                modelPoolIdNew = em.getReference(modelPoolIdNew.getClass(), modelPoolIdNew.getId());
                lotControl.setModelPoolId(modelPoolIdNew);
            }
            if (detailsIdDetailNew != null) {
                detailsIdDetailNew = em.getReference(detailsIdDetailNew.getClass(), detailsIdDetailNew.getIdDetail());
                lotControl.setDetailsIdDetail(detailsIdDetailNew);
            }
            Collection<Front2backMaster> attachedFront2backMasterCollectionNew = new ArrayList<Front2backMaster>();
            for (Front2backMaster front2backMasterCollectionNewFront2backMasterToAttach : front2backMasterCollectionNew) {
                front2backMasterCollectionNewFront2backMasterToAttach = em.getReference(front2backMasterCollectionNewFront2backMasterToAttach.getClass(), front2backMasterCollectionNewFront2backMasterToAttach.getId());
                attachedFront2backMasterCollectionNew.add(front2backMasterCollectionNewFront2backMasterToAttach);
            }
            front2backMasterCollectionNew = attachedFront2backMasterCollectionNew;
            lotControl.setFront2backMasterCollection(front2backMasterCollectionNew);
            Collection<Assy2auto> attachedAssy2autoCollectionNew = new ArrayList<Assy2auto>();
            for (Assy2auto assy2autoCollectionNewAssy2autoToAttach : assy2autoCollectionNew) {
                assy2autoCollectionNewAssy2autoToAttach = em.getReference(assy2autoCollectionNewAssy2autoToAttach.getClass(), assy2autoCollectionNewAssy2autoToAttach.getId());
                attachedAssy2autoCollectionNew.add(assy2autoCollectionNewAssy2autoToAttach);
            }
            assy2autoCollectionNew = attachedAssy2autoCollectionNew;
            lotControl.setAssy2autoCollection(assy2autoCollectionNew);
            Collection<Front2back> attachedFront2backCollectionNew = new ArrayList<Front2back>();
            for (Front2back front2backCollectionNewFront2backToAttach : front2backCollectionNew) {
                front2backCollectionNewFront2backToAttach = em.getReference(front2backCollectionNewFront2backToAttach.getClass(), front2backCollectionNewFront2backToAttach.getFront2backId());
                attachedFront2backCollectionNew.add(front2backCollectionNewFront2backToAttach);
            }
            front2backCollectionNew = attachedFront2backCollectionNew;
            lotControl.setFront2backCollection(front2backCollectionNew);
            Collection<Back2pack> attachedBack2packCollectionNew = new ArrayList<Back2pack>();
            for (Back2pack back2packCollectionNewBack2packToAttach : back2packCollectionNew) {
                back2packCollectionNewBack2packToAttach = em.getReference(back2packCollectionNewBack2packToAttach.getClass(), back2packCollectionNewBack2packToAttach.getBack2packId());
                attachedBack2packCollectionNew.add(back2packCollectionNewBack2packToAttach);
            }
            back2packCollectionNew = attachedBack2packCollectionNew;
            lotControl.setBack2packCollection(back2packCollectionNew);
            Collection<MainData> attachedMainDataCollectionNew = new ArrayList<MainData>();
            for (MainData mainDataCollectionNewMainDataToAttach : mainDataCollectionNew) {
                mainDataCollectionNewMainDataToAttach = em.getReference(mainDataCollectionNewMainDataToAttach.getClass(), mainDataCollectionNewMainDataToAttach.getId());
                attachedMainDataCollectionNew.add(mainDataCollectionNewMainDataToAttach);
            }
            mainDataCollectionNew = attachedMainDataCollectionNew;
            lotControl.setMainDataCollection(mainDataCollectionNew);
            lotControl = em.merge(lotControl);
            if (modelPoolIdOld != null && !modelPoolIdOld.equals(modelPoolIdNew)) {
                modelPoolIdOld.getLotControlCollection().remove(lotControl);
                modelPoolIdOld = em.merge(modelPoolIdOld);
            }
            if (modelPoolIdNew != null && !modelPoolIdNew.equals(modelPoolIdOld)) {
                modelPoolIdNew.getLotControlCollection().add(lotControl);
                modelPoolIdNew = em.merge(modelPoolIdNew);
            }
            if (detailsIdDetailOld != null && !detailsIdDetailOld.equals(detailsIdDetailNew)) {
                detailsIdDetailOld.getLotControlCollection().remove(lotControl);
                detailsIdDetailOld = em.merge(detailsIdDetailOld);
            }
            if (detailsIdDetailNew != null && !detailsIdDetailNew.equals(detailsIdDetailOld)) {
                detailsIdDetailNew.getLotControlCollection().add(lotControl);
                detailsIdDetailNew = em.merge(detailsIdDetailNew);
            }
            for (Front2backMaster front2backMasterCollectionNewFront2backMaster : front2backMasterCollectionNew) {
                if (!front2backMasterCollectionOld.contains(front2backMasterCollectionNewFront2backMaster)) {
                    LotControl oldLotcontrolIdOfFront2backMasterCollectionNewFront2backMaster = front2backMasterCollectionNewFront2backMaster.getLotcontrolId();
                    front2backMasterCollectionNewFront2backMaster.setLotcontrolId(lotControl);
                    front2backMasterCollectionNewFront2backMaster = em.merge(front2backMasterCollectionNewFront2backMaster);
                    if (oldLotcontrolIdOfFront2backMasterCollectionNewFront2backMaster != null && !oldLotcontrolIdOfFront2backMasterCollectionNewFront2backMaster.equals(lotControl)) {
                        oldLotcontrolIdOfFront2backMasterCollectionNewFront2backMaster.getFront2backMasterCollection().remove(front2backMasterCollectionNewFront2backMaster);
                        oldLotcontrolIdOfFront2backMasterCollectionNewFront2backMaster = em.merge(oldLotcontrolIdOfFront2backMasterCollectionNewFront2backMaster);
                    }
                }
            }
            for (Assy2auto assy2autoCollectionNewAssy2auto : assy2autoCollectionNew) {
                if (!assy2autoCollectionOld.contains(assy2autoCollectionNewAssy2auto)) {
                    LotControl oldLotcontrolIdOfAssy2autoCollectionNewAssy2auto = assy2autoCollectionNewAssy2auto.getLotcontrolId();
                    assy2autoCollectionNewAssy2auto.setLotcontrolId(lotControl);
                    assy2autoCollectionNewAssy2auto = em.merge(assy2autoCollectionNewAssy2auto);
                    if (oldLotcontrolIdOfAssy2autoCollectionNewAssy2auto != null && !oldLotcontrolIdOfAssy2autoCollectionNewAssy2auto.equals(lotControl)) {
                        oldLotcontrolIdOfAssy2autoCollectionNewAssy2auto.getAssy2autoCollection().remove(assy2autoCollectionNewAssy2auto);
                        oldLotcontrolIdOfAssy2autoCollectionNewAssy2auto = em.merge(oldLotcontrolIdOfAssy2autoCollectionNewAssy2auto);
                    }
                }
            }
            for (Front2back front2backCollectionNewFront2back : front2backCollectionNew) {
                if (!front2backCollectionOld.contains(front2backCollectionNewFront2back)) {
                    LotControl oldLotcontrolIdOfFront2backCollectionNewFront2back = front2backCollectionNewFront2back.getLotcontrolId();
                    front2backCollectionNewFront2back.setLotcontrolId(lotControl);
                    front2backCollectionNewFront2back = em.merge(front2backCollectionNewFront2back);
                    if (oldLotcontrolIdOfFront2backCollectionNewFront2back != null && !oldLotcontrolIdOfFront2backCollectionNewFront2back.equals(lotControl)) {
                        oldLotcontrolIdOfFront2backCollectionNewFront2back.getFront2backCollection().remove(front2backCollectionNewFront2back);
                        oldLotcontrolIdOfFront2backCollectionNewFront2back = em.merge(oldLotcontrolIdOfFront2backCollectionNewFront2back);
                    }
                }
            }
            for (Back2pack back2packCollectionNewBack2pack : back2packCollectionNew) {
                if (!back2packCollectionOld.contains(back2packCollectionNewBack2pack)) {
                    LotControl oldLotcontrolIdOfBack2packCollectionNewBack2pack = back2packCollectionNewBack2pack.getLotcontrolId();
                    back2packCollectionNewBack2pack.setLotcontrolId(lotControl);
                    back2packCollectionNewBack2pack = em.merge(back2packCollectionNewBack2pack);
                    if (oldLotcontrolIdOfBack2packCollectionNewBack2pack != null && !oldLotcontrolIdOfBack2packCollectionNewBack2pack.equals(lotControl)) {
                        oldLotcontrolIdOfBack2packCollectionNewBack2pack.getBack2packCollection().remove(back2packCollectionNewBack2pack);
                        oldLotcontrolIdOfBack2packCollectionNewBack2pack = em.merge(oldLotcontrolIdOfBack2packCollectionNewBack2pack);
                    }
                }
            }
            for (MainData mainDataCollectionNewMainData : mainDataCollectionNew) {
                if (!mainDataCollectionOld.contains(mainDataCollectionNewMainData)) {
                    LotControl oldLotControlIdOfMainDataCollectionNewMainData = mainDataCollectionNewMainData.getLotControlId();
                    mainDataCollectionNewMainData.setLotControlId(lotControl);
                    mainDataCollectionNewMainData = em.merge(mainDataCollectionNewMainData);
                    if (oldLotControlIdOfMainDataCollectionNewMainData != null && !oldLotControlIdOfMainDataCollectionNewMainData.equals(lotControl)) {
                        oldLotControlIdOfMainDataCollectionNewMainData.getMainDataCollection().remove(mainDataCollectionNewMainData);
                        oldLotControlIdOfMainDataCollectionNewMainData = em.merge(oldLotControlIdOfMainDataCollectionNewMainData);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = lotControl.getId();
                if (findLotControl(id) == null) {
                    throw new NonexistentEntityException("The lotControl with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            LotControl lotControl;
            try {
                lotControl = em.getReference(LotControl.class, id);
                lotControl.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The lotControl with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Front2backMaster> front2backMasterCollectionOrphanCheck = lotControl.getFront2backMasterCollection();
            for (Front2backMaster front2backMasterCollectionOrphanCheckFront2backMaster : front2backMasterCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This LotControl (" + lotControl + ") cannot be destroyed since the Front2backMaster " + front2backMasterCollectionOrphanCheckFront2backMaster + " in its front2backMasterCollection field has a non-nullable lotcontrolId field.");
            }
            Collection<Assy2auto> assy2autoCollectionOrphanCheck = lotControl.getAssy2autoCollection();
            for (Assy2auto assy2autoCollectionOrphanCheckAssy2auto : assy2autoCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This LotControl (" + lotControl + ") cannot be destroyed since the Assy2auto " + assy2autoCollectionOrphanCheckAssy2auto + " in its assy2autoCollection field has a non-nullable lotcontrolId field.");
            }
            Collection<Front2back> front2backCollectionOrphanCheck = lotControl.getFront2backCollection();
            for (Front2back front2backCollectionOrphanCheckFront2back : front2backCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This LotControl (" + lotControl + ") cannot be destroyed since the Front2back " + front2backCollectionOrphanCheckFront2back + " in its front2backCollection field has a non-nullable lotcontrolId field.");
            }
            Collection<Back2pack> back2packCollectionOrphanCheck = lotControl.getBack2packCollection();
            for (Back2pack back2packCollectionOrphanCheckBack2pack : back2packCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This LotControl (" + lotControl + ") cannot be destroyed since the Back2pack " + back2packCollectionOrphanCheckBack2pack + " in its back2packCollection field has a non-nullable lotcontrolId field.");
            }
            Collection<MainData> mainDataCollectionOrphanCheck = lotControl.getMainDataCollection();
            for (MainData mainDataCollectionOrphanCheckMainData : mainDataCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This LotControl (" + lotControl + ") cannot be destroyed since the MainData " + mainDataCollectionOrphanCheckMainData + " in its mainDataCollection field has a non-nullable lotControlId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            ModelPool modelPoolId = lotControl.getModelPoolId();
            if (modelPoolId != null) {
                modelPoolId.getLotControlCollection().remove(lotControl);
                modelPoolId = em.merge(modelPoolId);
            }
            Details detailsIdDetail = lotControl.getDetailsIdDetail();
            if (detailsIdDetail != null) {
                detailsIdDetail.getLotControlCollection().remove(lotControl);
                detailsIdDetail = em.merge(detailsIdDetail);
            }
            em.remove(lotControl);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<LotControl> findLotControlEntities() {
        return findLotControlEntities(true, -1, -1);
    }

    public List<LotControl> findLotControlEntities(int maxResults, int firstResult) {
        return findLotControlEntities(false, maxResults, firstResult);
    }

    private List<LotControl> findLotControlEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(LotControl.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }  
    public LotControl findLotControl(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(LotControl.class, id);
        } finally {
            em.close();
        }
    }
    
    //------------ find by detail id --------------
    public List<LotControl> findByDetailsId(Details detail){
        EntityManager em = getEntityManager();
        try{
            Query query = em.createNamedQuery("LotControl.findByDetailsId");
            query.setParameter("detailsIdDetail", detail);
            return query.getResultList();
        }finally{
            em.close();
        }
    }
     //------------ find by detail id order by lot Number--------------
    public List<LotControl> findByDetailsIdOrderbyLotnumber(Details detail){
        EntityManager em = getEntityManager();
        try{
            Query query = em.createNamedQuery("LotControl.findByDetailsOrderNumber");
            query.setParameter("detailsIdDetail", detail);
            return query.getResultList();
        }finally{
            em.close();
        }
    }
    //----------- find  barcode -------------------
   // public  List<LotControl> findProcessNameAndStatus(String barcode) {
 public  LotControl findProcessNameAndStatus(String barcode) {
        EntityManager em = getEntityManager();
        try { 
            Query  q   = em.createNamedQuery("LotControl.findByBarcode");  
            q.setParameter("barcode",barcode);  
            //return   q.getResultList();   
            return (LotControl) q.getSingleResult();
        } 
        catch(Exception e)
        {
            return null;
        }
        finally {
            em.close();
        }
    } 
    //----find   idLotControl  from   main_data
    public  List<MainData>  findMain(LotControl idLogControl,String mainDataId){
       EntityManager  em = getEntityManager();
        try {
            Query q  = em.createNamedQuery("MainData.findByLotControlIdAndMainDataId");
            q.setParameter("lotControlId",idLogControl);
            q.setParameter("id",mainDataId);
            return q.getResultList();
        } 
        finally{
            em.close();
        } 
    }
    //---find  model 
    public ModelPool findModel(String modelPoolId)
    {
          EntityManager em = getEntityManager();
          try
          {
              Query q = em.createQuery("SELECT m FROM ModelPool m WHERE m.id ="+"'"+modelPoolId+"'",ModelPool.class);
              return (ModelPool)q.getSingleResult();
          }
          finally{
              em.close();
          }
    }
    //---find md2pc
    public List<ModelPool>  findProcessIdAndSequnce(String modelPoolId){
        EntityManager em  = getEntityManager();
        try {
            Query q  = em.createQuery("SELECT c FROM ModelPool c ,Md2pc d WHERE (c.id = d.modelPoolId) and (c.modelPoolId='"+modelPoolId+"')",ModelPool.class);
            return q.getResultList();
        } 
        finally
        {
            em.close();
        }  
    } 
    public int getLotControlCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<LotControl> rt = cq.from(LotControl.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<LotControl> findLotcontrolByBarcode(String barcode){
        EntityManager em = getEntityManager();
        Query query;
        try {
            query = em.createNamedQuery("LotControl.findByBarcodeSearch");
            query.setParameter("barcode", barcode+"%");
//            query.setParameter("deleted", 1);
            return query.getResultList();
        }finally{
            em.close();
        }
    } 
    
    public LotControl findLotcontrolByMaindataId(String maindataid){
        EntityManager em = getEntityManager();
        Query query;
        try {
            query = em.createNamedQuery("LotControl.findByMainDataId");
            query.setParameter("mainDataId", maindataid);
            return  (LotControl) query.getSingleResult();
        }finally{
            em.close();
        }
    }
    
    public LotControl findLotcontrolByMaxnumber(){
        EntityManager em = getEntityManager();
        Query query;
        try {
            query = em.createNamedQuery(null);
            return (LotControl) query.getSingleResult();
        }finally{
            em.close();
        }
    }
//    find lot all not deleted
    public List<LotControl> findAllNotDeleted()
    {
        EntityManager em  = getEntityManager();
        try
        {   
            Query q = em.createQuery("SELECT l FROM LotControl l WHERE l.deleted = 0");
            return  q.getResultList();
        }
        finally
        {
            em.close();
        }
    }
//    search by barcode
    public List<LotControl>  searchByBaroce(String barcode)
    {
        EntityManager em = getEntityManager();
        try
        {
            Query q  = em.createQuery("SELECT l FROM LotControl l WHERE   l.barcode like :barcode ORDER BY l.barcode asc").setParameter("barcode",barcode+"%");
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }  
//    search by model
    public List<LotControl> searchByModel(ModelPool idModelPool)
    {
        EntityManager  em  =  getEntityManager();
        try
        {
            Query q = em.createQuery("SELECT l FROM LotControl l WHERE   l.modelPoolId =:idModelPool").setParameter("idModelPool",idModelPool);
            return   q.getResultList() ;
        }
        catch(Exception e)
        {
            return null;
        }
        finally
        {
            em.close();
        }
        
    }
//    search by series
    public List<LotControl> searchBySeries()
    {
        EntityManager  em  = getEntityManager();
        try
        {
            return  null;
        }
        finally
        {
            em.close();
        }
    } 
    
}