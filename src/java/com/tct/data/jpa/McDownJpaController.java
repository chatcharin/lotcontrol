/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.CurrentProcess;
import com.tct.data.Mc;
import com.tct.data.McDown;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author The_Boy_Cs
 */
public class McDownJpaController implements Serializable {

    public McDownJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(McDown mcDown) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CurrentProcess currentProcessId = mcDown.getCurrentProcessId();
            if (currentProcessId != null) {
                currentProcessId = em.getReference(currentProcessId.getClass(), currentProcessId.getId());
                mcDown.setCurrentProcessId(currentProcessId);
            }
            Mc mcId = mcDown.getMcId();
            if (mcId != null) {
                mcId = em.getReference(mcId.getClass(), mcId.getId());
                mcDown.setMcId(mcId);
            }
            em.persist(mcDown);
            if (currentProcessId != null) {
                currentProcessId.getMcDownCollection().add(mcDown);
                currentProcessId = em.merge(currentProcessId);
            }
            if (mcId != null) {
                mcId.getMcDownCollection().add(mcDown);
                mcId = em.merge(mcId);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findMcDown(mcDown.getId()) != null) {
                throw new PreexistingEntityException("McDown " + mcDown + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(McDown mcDown) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            McDown persistentMcDown = em.find(McDown.class, mcDown.getId());
            CurrentProcess currentProcessIdOld = persistentMcDown.getCurrentProcessId();
            CurrentProcess currentProcessIdNew = mcDown.getCurrentProcessId();
            Mc mcIdOld = persistentMcDown.getMcId();
            Mc mcIdNew = mcDown.getMcId();
            if (currentProcessIdNew != null) {
                currentProcessIdNew = em.getReference(currentProcessIdNew.getClass(), currentProcessIdNew.getId());
                mcDown.setCurrentProcessId(currentProcessIdNew);
            }
            if (mcIdNew != null) {
                mcIdNew = em.getReference(mcIdNew.getClass(), mcIdNew.getId());
                mcDown.setMcId(mcIdNew);
            }
            mcDown = em.merge(mcDown);
            if (currentProcessIdOld != null && !currentProcessIdOld.equals(currentProcessIdNew)) {
                currentProcessIdOld.getMcDownCollection().remove(mcDown);
                currentProcessIdOld = em.merge(currentProcessIdOld);
            }
            if (currentProcessIdNew != null && !currentProcessIdNew.equals(currentProcessIdOld)) {
                currentProcessIdNew.getMcDownCollection().add(mcDown);
                currentProcessIdNew = em.merge(currentProcessIdNew);
            }
            if (mcIdOld != null && !mcIdOld.equals(mcIdNew)) {
                mcIdOld.getMcDownCollection().remove(mcDown);
                mcIdOld = em.merge(mcIdOld);
            }
            if (mcIdNew != null && !mcIdNew.equals(mcIdOld)) {
                mcIdNew.getMcDownCollection().add(mcDown);
                mcIdNew = em.merge(mcIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = mcDown.getId();
                if (findMcDown(id) == null) {
                    throw new NonexistentEntityException("The mcDown with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            McDown mcDown;
            try {
                mcDown = em.getReference(McDown.class, id);
                mcDown.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The mcDown with id " + id + " no longer exists.", enfe);
            }
            CurrentProcess currentProcessId = mcDown.getCurrentProcessId();
            if (currentProcessId != null) {
                currentProcessId.getMcDownCollection().remove(mcDown);
                currentProcessId = em.merge(currentProcessId);
            }
            Mc mcId = mcDown.getMcId();
            if (mcId != null) {
                mcId.getMcDownCollection().remove(mcDown);
                mcId = em.merge(mcId);
            }
            em.remove(mcDown);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<McDown> findMcDownEntities() {
        return findMcDownEntities(true, -1, -1);
    }

    public List<McDown> findMcDownEntities(int maxResults, int firstResult) {
        return findMcDownEntities(false, maxResults, firstResult);
    }

    private List<McDown> findMcDownEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(McDown.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public McDown findMcDown(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(McDown.class, id);
        } finally {
            em.close();
        }
    }

    public int getMcDownCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<McDown> rt = cq.from(McDown.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    //=======find currentProcessId
    public McDown findByCurrentProcessId(CurrentProcess currentProcessId)
    {
        EntityManager  em = getEntityManager();
        try
        {
            Query q  = em.createNamedQuery("McDown.findByCurrentProcessId");
            q.setParameter("currentProcessId",currentProcessId);
            return (McDown) q.getSingleResult();
        }
        catch(Exception e)
        {
            System.out.println(e);
            return null;
        }
        finally{
            em.close();;
        }
    }
    public McDown findCheckDowntime(CurrentProcess idCurrentProcess)
    {
        EntityManager em  = getEntityManager();
        try
        {
            Query q = em.createQuery("SELECT m FROM McDown  m WHERE m.currentProcessId =:idCurrentProcess").setParameter("idCurrentProcess",idCurrentProcess);
            return (McDown)q.getSingleResult();
        }
        catch(Exception e)
        {
            return  null;
        }
        finally
        {
            em.close();
        }
    
    }

    public McDown findMcDownByrefDownTimeStart(String currentprocessid) {
        EntityManager em = getEntityManager();
        try{
            String sql = "SELECT m FROM McDown m WHERE m.refDownTimeStart = :refDownTimeStart";
            Query query = em.createQuery(sql);
            query.setParameter("refDownTimeStart", currentprocessid);
            return (McDown) query.getSingleResult();
        }finally{
            em.close();
        }
    }
    
    
}
