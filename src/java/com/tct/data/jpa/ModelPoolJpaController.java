/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import com.tct.data.LotControl;
import com.tct.data.Md2pc;
import com.tct.data.ModelPool;
import com.tct.data.Target;
import com.tct.data.jpa.exceptions.IllegalOrphanException;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author The_Boy_Cs
 */
public class ModelPoolJpaController implements Serializable {

   public ModelPoolJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ModelPool modelPool) throws PreexistingEntityException, Exception {
        if (modelPool.getMd2pcCollection() == null) {
            modelPool.setMd2pcCollection(new ArrayList<Md2pc>());
        }
        if (modelPool.getTargetCollection() == null) {
            modelPool.setTargetCollection(new ArrayList<Target>());
        }
        if (modelPool.getLotControlCollection() == null) {
            modelPool.setLotControlCollection(new ArrayList<LotControl>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Md2pc> attachedMd2pcCollection = new ArrayList<Md2pc>();
            for (Md2pc md2pcCollectionMd2pcToAttach : modelPool.getMd2pcCollection()) {
                md2pcCollectionMd2pcToAttach = em.getReference(md2pcCollectionMd2pcToAttach.getClass(), md2pcCollectionMd2pcToAttach.getMd2pcId());
                attachedMd2pcCollection.add(md2pcCollectionMd2pcToAttach);
            }
            modelPool.setMd2pcCollection(attachedMd2pcCollection);
            Collection<Target> attachedTargetCollection = new ArrayList<Target>();
            for (Target targetCollectionTargetToAttach : modelPool.getTargetCollection()) {
                targetCollectionTargetToAttach = em.getReference(targetCollectionTargetToAttach.getClass(), targetCollectionTargetToAttach.getId());
                attachedTargetCollection.add(targetCollectionTargetToAttach);
            }
            modelPool.setTargetCollection(attachedTargetCollection);
            Collection<LotControl> attachedLotControlCollection = new ArrayList<LotControl>();
            for (LotControl lotControlCollectionLotControlToAttach : modelPool.getLotControlCollection()) {
                lotControlCollectionLotControlToAttach = em.getReference(lotControlCollectionLotControlToAttach.getClass(), lotControlCollectionLotControlToAttach.getId());
                attachedLotControlCollection.add(lotControlCollectionLotControlToAttach);
            }
            modelPool.setLotControlCollection(attachedLotControlCollection);
            em.persist(modelPool);
            for (Md2pc md2pcCollectionMd2pc : modelPool.getMd2pcCollection()) {
                ModelPool oldModelPoolIdOfMd2pcCollectionMd2pc = md2pcCollectionMd2pc.getModelPoolId();
                md2pcCollectionMd2pc.setModelPoolId(modelPool);
                md2pcCollectionMd2pc = em.merge(md2pcCollectionMd2pc);
                if (oldModelPoolIdOfMd2pcCollectionMd2pc != null) {
                    oldModelPoolIdOfMd2pcCollectionMd2pc.getMd2pcCollection().remove(md2pcCollectionMd2pc);
                    oldModelPoolIdOfMd2pcCollectionMd2pc = em.merge(oldModelPoolIdOfMd2pcCollectionMd2pc);
                }
            }
            for (Target targetCollectionTarget : modelPool.getTargetCollection()) {
                ModelPool oldIdModelPoolOfTargetCollectionTarget = targetCollectionTarget.getIdModelPool();
                targetCollectionTarget.setIdModelPool(modelPool);
                targetCollectionTarget = em.merge(targetCollectionTarget);
                if (oldIdModelPoolOfTargetCollectionTarget != null) {
                    oldIdModelPoolOfTargetCollectionTarget.getTargetCollection().remove(targetCollectionTarget);
                    oldIdModelPoolOfTargetCollectionTarget = em.merge(oldIdModelPoolOfTargetCollectionTarget);
                }
            }
            for (LotControl lotControlCollectionLotControl : modelPool.getLotControlCollection()) {
                ModelPool oldModelPoolIdOfLotControlCollectionLotControl = lotControlCollectionLotControl.getModelPoolId();
                lotControlCollectionLotControl.setModelPoolId(modelPool);
                lotControlCollectionLotControl = em.merge(lotControlCollectionLotControl);
                if (oldModelPoolIdOfLotControlCollectionLotControl != null) {
                    oldModelPoolIdOfLotControlCollectionLotControl.getLotControlCollection().remove(lotControlCollectionLotControl);
                    oldModelPoolIdOfLotControlCollectionLotControl = em.merge(oldModelPoolIdOfLotControlCollectionLotControl);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findModelPool(modelPool.getId()) != null) {
                throw new PreexistingEntityException("ModelPool " + modelPool + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ModelPool modelPool) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ModelPool persistentModelPool = em.find(ModelPool.class, modelPool.getId());
            Collection<Md2pc> md2pcCollectionOld = persistentModelPool.getMd2pcCollection();
            Collection<Md2pc> md2pcCollectionNew = modelPool.getMd2pcCollection();
            Collection<Target> targetCollectionOld = persistentModelPool.getTargetCollection();
            Collection<Target> targetCollectionNew = modelPool.getTargetCollection();
            Collection<LotControl> lotControlCollectionOld = persistentModelPool.getLotControlCollection();
            Collection<LotControl> lotControlCollectionNew = modelPool.getLotControlCollection();
            List<String> illegalOrphanMessages = null;
            for (Md2pc md2pcCollectionOldMd2pc : md2pcCollectionOld) {
                if (!md2pcCollectionNew.contains(md2pcCollectionOldMd2pc)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Md2pc " + md2pcCollectionOldMd2pc + " since its modelPoolId field is not nullable.");
                }
            }
            for (Target targetCollectionOldTarget : targetCollectionOld) {
                if (!targetCollectionNew.contains(targetCollectionOldTarget)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Target " + targetCollectionOldTarget + " since its idModelPool field is not nullable.");
                }
            }
            for (LotControl lotControlCollectionOldLotControl : lotControlCollectionOld) {
                if (!lotControlCollectionNew.contains(lotControlCollectionOldLotControl)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain LotControl " + lotControlCollectionOldLotControl + " since its modelPoolId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Md2pc> attachedMd2pcCollectionNew = new ArrayList<Md2pc>();
            for (Md2pc md2pcCollectionNewMd2pcToAttach : md2pcCollectionNew) {
                md2pcCollectionNewMd2pcToAttach = em.getReference(md2pcCollectionNewMd2pcToAttach.getClass(), md2pcCollectionNewMd2pcToAttach.getMd2pcId());
                attachedMd2pcCollectionNew.add(md2pcCollectionNewMd2pcToAttach);
            }
            md2pcCollectionNew = attachedMd2pcCollectionNew;
            modelPool.setMd2pcCollection(md2pcCollectionNew);
            Collection<Target> attachedTargetCollectionNew = new ArrayList<Target>();
            for (Target targetCollectionNewTargetToAttach : targetCollectionNew) {
                targetCollectionNewTargetToAttach = em.getReference(targetCollectionNewTargetToAttach.getClass(), targetCollectionNewTargetToAttach.getId());
                attachedTargetCollectionNew.add(targetCollectionNewTargetToAttach);
            }
            targetCollectionNew = attachedTargetCollectionNew;
            modelPool.setTargetCollection(targetCollectionNew);
            Collection<LotControl> attachedLotControlCollectionNew = new ArrayList<LotControl>();
            for (LotControl lotControlCollectionNewLotControlToAttach : lotControlCollectionNew) {
                lotControlCollectionNewLotControlToAttach = em.getReference(lotControlCollectionNewLotControlToAttach.getClass(), lotControlCollectionNewLotControlToAttach.getId());
                attachedLotControlCollectionNew.add(lotControlCollectionNewLotControlToAttach);
            }
            lotControlCollectionNew = attachedLotControlCollectionNew;
            modelPool.setLotControlCollection(lotControlCollectionNew);
            modelPool = em.merge(modelPool);
            for (Md2pc md2pcCollectionNewMd2pc : md2pcCollectionNew) {
                if (!md2pcCollectionOld.contains(md2pcCollectionNewMd2pc)) {
                    ModelPool oldModelPoolIdOfMd2pcCollectionNewMd2pc = md2pcCollectionNewMd2pc.getModelPoolId();
                    md2pcCollectionNewMd2pc.setModelPoolId(modelPool);
                    md2pcCollectionNewMd2pc = em.merge(md2pcCollectionNewMd2pc);
                    if (oldModelPoolIdOfMd2pcCollectionNewMd2pc != null && !oldModelPoolIdOfMd2pcCollectionNewMd2pc.equals(modelPool)) {
                        oldModelPoolIdOfMd2pcCollectionNewMd2pc.getMd2pcCollection().remove(md2pcCollectionNewMd2pc);
                        oldModelPoolIdOfMd2pcCollectionNewMd2pc = em.merge(oldModelPoolIdOfMd2pcCollectionNewMd2pc);
                    }
                }
            }
            for (Target targetCollectionNewTarget : targetCollectionNew) {
                if (!targetCollectionOld.contains(targetCollectionNewTarget)) {
                    ModelPool oldIdModelPoolOfTargetCollectionNewTarget = targetCollectionNewTarget.getIdModelPool();
                    targetCollectionNewTarget.setIdModelPool(modelPool);
                    targetCollectionNewTarget = em.merge(targetCollectionNewTarget);
                    if (oldIdModelPoolOfTargetCollectionNewTarget != null && !oldIdModelPoolOfTargetCollectionNewTarget.equals(modelPool)) {
                        oldIdModelPoolOfTargetCollectionNewTarget.getTargetCollection().remove(targetCollectionNewTarget);
                        oldIdModelPoolOfTargetCollectionNewTarget = em.merge(oldIdModelPoolOfTargetCollectionNewTarget);
                    }
                }
            }
            for (LotControl lotControlCollectionNewLotControl : lotControlCollectionNew) {
                if (!lotControlCollectionOld.contains(lotControlCollectionNewLotControl)) {
                    ModelPool oldModelPoolIdOfLotControlCollectionNewLotControl = lotControlCollectionNewLotControl.getModelPoolId();
                    lotControlCollectionNewLotControl.setModelPoolId(modelPool);
                    lotControlCollectionNewLotControl = em.merge(lotControlCollectionNewLotControl);
                    if (oldModelPoolIdOfLotControlCollectionNewLotControl != null && !oldModelPoolIdOfLotControlCollectionNewLotControl.equals(modelPool)) {
                        oldModelPoolIdOfLotControlCollectionNewLotControl.getLotControlCollection().remove(lotControlCollectionNewLotControl);
                        oldModelPoolIdOfLotControlCollectionNewLotControl = em.merge(oldModelPoolIdOfLotControlCollectionNewLotControl);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = modelPool.getId();
                if (findModelPool(id) == null) {
                    throw new NonexistentEntityException("The modelPool with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ModelPool modelPool;
            try {
                modelPool = em.getReference(ModelPool.class, id);
                modelPool.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The modelPool with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Md2pc> md2pcCollectionOrphanCheck = modelPool.getMd2pcCollection();
            for (Md2pc md2pcCollectionOrphanCheckMd2pc : md2pcCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This ModelPool (" + modelPool + ") cannot be destroyed since the Md2pc " + md2pcCollectionOrphanCheckMd2pc + " in its md2pcCollection field has a non-nullable modelPoolId field.");
            }
            Collection<Target> targetCollectionOrphanCheck = modelPool.getTargetCollection();
            for (Target targetCollectionOrphanCheckTarget : targetCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This ModelPool (" + modelPool + ") cannot be destroyed since the Target " + targetCollectionOrphanCheckTarget + " in its targetCollection field has a non-nullable idModelPool field.");
            }
            Collection<LotControl> lotControlCollectionOrphanCheck = modelPool.getLotControlCollection();
            for (LotControl lotControlCollectionOrphanCheckLotControl : lotControlCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This ModelPool (" + modelPool + ") cannot be destroyed since the LotControl " + lotControlCollectionOrphanCheckLotControl + " in its lotControlCollection field has a non-nullable modelPoolId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(modelPool);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ModelPool> findModelPoolEntities() {
        return findModelPoolEntities(true, -1, -1);
    }

    public List<ModelPool> findModelPoolEntities(int maxResults, int firstResult) {
        return findModelPoolEntities(false, maxResults, firstResult);
    }

    private List<ModelPool> findModelPoolEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ModelPool.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ModelPool findModelPool(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ModelPool.class, id);
        } 
        finally {
            em.close();
        }
    }

    public int getModelPoolCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ModelPool> rt = cq.from(ModelPool.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<ModelPool> findByModelname(String modelname) {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("ModelPool.findByModelName");
            query.setParameter("modelName", modelname + "%");
            return query.getResultList();
        } finally {
            em.close();
        }
    }

    public List<ModelPool> findModelPoolGroupByName() {
        EntityManager em = getEntityManager();
        Query query;
        try {
//            String sql = "SELECT distinct m.modelName FROM ModelPool m order by m.modelName asc";
//                    query = em.createQuery(sql);
            query = em.createNamedQuery("ModelPool.findGroupByName");
            return query.getResultList();
        } finally {
            em.close();
        }
    }

    public List<String> findModelPoolGroupBySeries() {
        EntityManager em = getEntityManager();
        Query query;
        try {
//            String sql = "SELECT distinct m.modelName FROM ModelPool m order by m.modelName asc";
//                    query = em.createQuery(sql);
            query = em.createNamedQuery("ModelPool.findGroupBySeries");
            return query.getResultList();
        } finally {
            em.close();
        }
    }

    public ModelPool findModelPoolByNameAndSeries(String name, String series) {
        EntityManager em = getEntityManager();
        Query query;
        try {
            query = em.createNamedQuery("ModelPool.findByNameAndSeries");
            query.setParameter("modelName", name);
            query.setParameter("modelSeries", series);
            return (ModelPool) query.getSingleResult();
        } catch (Exception e) {
            System.out.println(" Error : " + e);
            return null;
        } finally {
            em.close();
        }
    }

    public List<ModelPool> findModelPoolBySeries(String modelseries) {
        EntityManager em = getEntityManager();
        Query query;
        try {
            query = em.createNamedQuery("ModelPool.findByModelSeries");
            query.setParameter("modelSeries", modelseries);
            return query.getResultList();
        } finally {
            em.close();
        }
    }

    public List<ModelPool> findModelPoolByNotDeleted() {
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("ModelPool.findByNotDeleted");
            return query.getResultList();
        } finally {
            em.close();
        }
    }

    //@pang
    public List<ModelPool> findModelPoolByStatus(String status)
    {
        EntityManager em  = getEntityManager();
        try
        {
            Query q = em.createNamedQuery("ModelPool.findByStatus").setParameter("status",status);
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }
    public List<ModelPool> findModelPoolByGroup(String modelGroup)
    {
        EntityManager em  = getEntityManager();
        try
        {
            Query q  = em.createNamedQuery("ModelPool.findByModelGroup").setParameter("modelGroup", modelGroup);
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }
    public List<ModelPool> findModelPoolByModelType(String modelType)
    {
        EntityManager em  = getEntityManager();
        try
        {
            Query q = em.createNamedQuery("ModelPool.findByModelType").setParameter("modelType", modelType);
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    
    }
    
    public List<ModelPool> findModelPoolByTypeFlBl(String type){
        EntityManager em = getEntityManager();
        try{
            Query query = em.createNamedQuery("ModelPool.findByTypeFlBl");
            query.setParameter("typeFlbl", type);
            return query.getResultList();
        }finally{
            em.close();
        }
    }
    
    public List<String> findModelPoolGroupByGroup(){
        EntityManager em = getEntityManager();
        try{
            Query query = em.createNamedQuery("ModelPool.findGroupByGroup");
            return query.getResultList();
        }finally{
            em.close();
        }
    }
    public List<ModelPool> findAllModelListBox()
    {
        EntityManager em  = getEntityManager();
        try
        {
            Query q  = em.createQuery("SELECT m  FROM ModelPool m WHERE m.deleted = 0 and m.status ='Active' ORDER BY m.modelType,m.modelSeries,m.modelName ASC");
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    } 
    public List<ModelPool> findByGroupModelNotDeleted()
    {
        EntityManager em  = getEntityManager();
        try
        {
            Query q  = em.createQuery("SELECT m FROM ModelPool m WHERE m.deleted = 0 GROUP BY m.modelGroup order by m.modelGroup asc");
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }
}
