/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.KpiPicture;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;

/**
 *
 * @author panglao
 */
public class KpiPictureJpaController implements Serializable {

    public KpiPictureJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(KpiPicture kpiPicture) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(kpiPicture);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findKpiPicture(kpiPicture.getIdKpiPicture()) != null) {
                throw new PreexistingEntityException("KpiPicture " + kpiPicture + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(KpiPicture kpiPicture) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            kpiPicture = em.merge(kpiPicture);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = kpiPicture.getIdKpiPicture();
                if (findKpiPicture(id) == null) {
                    throw new NonexistentEntityException("The kpiPicture with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            KpiPicture kpiPicture;
            try {
                kpiPicture = em.getReference(KpiPicture.class, id);
                kpiPicture.getIdKpiPicture();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The kpiPicture with id " + id + " no longer exists.", enfe);
            }
            em.remove(kpiPicture);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<KpiPicture> findKpiPictureEntities() {
        return findKpiPictureEntities(true, -1, -1);
    }

    public List<KpiPicture> findKpiPictureEntities(int maxResults, int firstResult) {
        return findKpiPictureEntities(false, maxResults, firstResult);
    }

    private List<KpiPicture> findKpiPictureEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(KpiPicture.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public KpiPicture findKpiPicture(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(KpiPicture.class, id);
        } finally {
            em.close();
        }
    }

    public int getKpiPictureCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<KpiPicture> rt = cq.from(KpiPicture.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    public List<KpiPicture> findAllNotDelete()
    {
        EntityManager  em  = getEntityManager();
        try
        {
 
            Query q  = em.createQuery("SELECT k FROM KpiPicture k WHERE k.deleted = 0 order by k.sequences asc");  
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }
}
