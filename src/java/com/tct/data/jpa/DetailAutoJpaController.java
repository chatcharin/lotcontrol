/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import com.tct.data.DetailAuto;
import com.tct.data.Details;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author The_Boy_Cs
 */
public class DetailAutoJpaController implements Serializable {

    public DetailAutoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DetailAuto detailAuto) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Details detailIdDetail = detailAuto.getDetailIdDetail();
            if (detailIdDetail != null) {
                detailIdDetail = em.getReference(detailIdDetail.getClass(), detailIdDetail.getIdDetail());
                detailAuto.setDetailIdDetail(detailIdDetail);
            }
            em.persist(detailAuto);
            if (detailIdDetail != null) {
                detailIdDetail.getDetailAutoCollection().add(detailAuto);
                detailIdDetail = em.merge(detailIdDetail);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDetailAuto(detailAuto.getId()) != null) {
                throw new PreexistingEntityException("DetailAuto " + detailAuto + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DetailAuto detailAuto) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailAuto persistentDetailAuto = em.find(DetailAuto.class, detailAuto.getId());
            Details detailIdDetailOld = persistentDetailAuto.getDetailIdDetail();
            Details detailIdDetailNew = detailAuto.getDetailIdDetail();
            if (detailIdDetailNew != null) {
                detailIdDetailNew = em.getReference(detailIdDetailNew.getClass(), detailIdDetailNew.getIdDetail());
                detailAuto.setDetailIdDetail(detailIdDetailNew);
            }
            detailAuto = em.merge(detailAuto);
            if (detailIdDetailOld != null && !detailIdDetailOld.equals(detailIdDetailNew)) {
                detailIdDetailOld.getDetailAutoCollection().remove(detailAuto);
                detailIdDetailOld = em.merge(detailIdDetailOld);
            }
            if (detailIdDetailNew != null && !detailIdDetailNew.equals(detailIdDetailOld)) {
                detailIdDetailNew.getDetailAutoCollection().add(detailAuto);
                detailIdDetailNew = em.merge(detailIdDetailNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = detailAuto.getId();
                if (findDetailAuto(id) == null) {
                    throw new NonexistentEntityException("The detailAuto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailAuto detailAuto;
            try {
                detailAuto = em.getReference(DetailAuto.class, id);
                detailAuto.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The detailAuto with id " + id + " no longer exists.", enfe);
            }
            Details detailIdDetail = detailAuto.getDetailIdDetail();
            if (detailIdDetail != null) {
                detailIdDetail.getDetailAutoCollection().remove(detailAuto);
                detailIdDetail = em.merge(detailIdDetail);
            }
            em.remove(detailAuto);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DetailAuto> findDetailAutoEntities() {
        return findDetailAutoEntities(true, -1, -1);
    }

    public List<DetailAuto> findDetailAutoEntities(int maxResults, int firstResult) {
        return findDetailAutoEntities(false, maxResults, firstResult);
    }

    private List<DetailAuto> findDetailAutoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(DetailAuto.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DetailAuto findDetailAuto(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DetailAuto.class, id);
        } finally {
            em.close();
        }
    }

    public int getDetailAutoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<DetailAuto> rt = cq.from(DetailAuto.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    //--find  autoList
    public List<DetailAuto>   findDataDetailList(Details  idDetails){
        EntityManager  em  = getEntityManager();
        try{
            Query q  = em.createNamedQuery("DetailAuto.findBydetailIdDetail");
            q.setParameter("detailIdDetail",idDetails);
            return q.getResultList();
        }
        finally{
            em.close();
        }
    
    }
     public DetailAuto   findDataDetailListSingle(Details  idDetails){
        EntityManager  em  = getEntityManager();
        try{
            Query q  = em.createNamedQuery("DetailAuto.findBydetailIdDetail");
            q.setParameter("detailIdDetail",idDetails);
            return (DetailAuto)q.getSingleResult();
        }
        finally{
            em.close();
        }
    
    }
    
}
