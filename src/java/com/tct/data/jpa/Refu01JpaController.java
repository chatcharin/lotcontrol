/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.Details;
import com.tct.data.Refu01;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author The_Boy_Cs
 */
public class Refu01JpaController implements Serializable {

    public Refu01JpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Refu01 refu01) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Details detailIdDetail = refu01.getDetailIdDetail();
            if (detailIdDetail != null) {
                detailIdDetail = em.getReference(detailIdDetail.getClass(), detailIdDetail.getIdDetail());
                refu01.setDetailIdDetail(detailIdDetail);
            }
            em.persist(refu01);
            if (detailIdDetail != null) {
                detailIdDetail.getRefu01Collection().add(refu01);
                detailIdDetail = em.merge(detailIdDetail);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findRefu01(refu01.getId()) != null) {
                throw new PreexistingEntityException("Refu01 " + refu01 + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Refu01 refu01) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Refu01 persistentRefu01 = em.find(Refu01.class, refu01.getId());
            Details detailIdDetailOld = persistentRefu01.getDetailIdDetail();
            Details detailIdDetailNew = refu01.getDetailIdDetail();
            if (detailIdDetailNew != null) {
                detailIdDetailNew = em.getReference(detailIdDetailNew.getClass(), detailIdDetailNew.getIdDetail());
                refu01.setDetailIdDetail(detailIdDetailNew);
            }
            refu01 = em.merge(refu01);
            if (detailIdDetailOld != null && !detailIdDetailOld.equals(detailIdDetailNew)) {
                detailIdDetailOld.getRefu01Collection().remove(refu01);
                detailIdDetailOld = em.merge(detailIdDetailOld);
            }
            if (detailIdDetailNew != null && !detailIdDetailNew.equals(detailIdDetailOld)) {
                detailIdDetailNew.getRefu01Collection().add(refu01);
                detailIdDetailNew = em.merge(detailIdDetailNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = refu01.getId();
                if (findRefu01(id) == null) {
                    throw new NonexistentEntityException("The refu01 with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Refu01 refu01;
            try {
                refu01 = em.getReference(Refu01.class, id);
                refu01.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The refu01 with id " + id + " no longer exists.", enfe);
            }
            Details detailIdDetail = refu01.getDetailIdDetail();
            if (detailIdDetail != null) {
                detailIdDetail.getRefu01Collection().remove(refu01);
                detailIdDetail = em.merge(detailIdDetail);
            }
            em.remove(refu01);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Refu01> findRefu01Entities() {
        return findRefu01Entities(true, -1, -1);
    }

    public List<Refu01> findRefu01Entities(int maxResults, int firstResult) {
        return findRefu01Entities(false, maxResults, firstResult);
    }

    private List<Refu01> findRefu01Entities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Refu01.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Refu01 findRefu01(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Refu01.class, id);
        } finally {
            em.close();
        }
    }

    public int getRefu01Count() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Refu01> rt = cq.from(Refu01.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    //--find list data refu01
    public List<Refu01>  findDataRefu01List(Details idDetail){
        EntityManager  em = getEntityManager();
        try{
            Query q  = em.createNamedQuery("Refu01.findBydetailIdDetail");
            q.setParameter("detailIdDetail",idDetail);
            return q.getResultList();
        }
        finally{
            em.close();
        }
    
    }
    public Refu01  findDataRefu01ListSingle(Details idDetail){
        EntityManager  em = getEntityManager();
        try{
            Query q  = em.createNamedQuery("Refu01.findBydetailIdDetail");
            q.setParameter("detailIdDetail",idDetail);
            return (Refu01)q.getSingleResult();
        }
        finally{
            em.close();
        }
    
    }
}
