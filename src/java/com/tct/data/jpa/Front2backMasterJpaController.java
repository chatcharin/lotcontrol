/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import com.tct.data.Details;
import com.tct.data.Front2backMaster;
import com.tct.data.LotControl;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author TheBoyCs
 */
public class Front2backMasterJpaController implements Serializable {

    public Front2backMasterJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Front2backMaster front2backMaster) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Details detailsId = front2backMaster.getDetailsId();
            if (detailsId != null) {
                detailsId = em.getReference(detailsId.getClass(), detailsId.getIdDetail());
                front2backMaster.setDetailsId(detailsId);
            }
            LotControl lotcontrolId = front2backMaster.getLotcontrolId();
            if (lotcontrolId != null) {
                lotcontrolId = em.getReference(lotcontrolId.getClass(), lotcontrolId.getId());
                front2backMaster.setLotcontrolId(lotcontrolId);
            }
            em.persist(front2backMaster);
            if (detailsId != null) {
                detailsId.getFront2backMasterCollection().add(front2backMaster);
                detailsId = em.merge(detailsId);
            }
            if (lotcontrolId != null) {
                lotcontrolId.getFront2backMasterCollection().add(front2backMaster);
                lotcontrolId = em.merge(lotcontrolId);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findFront2backMaster(front2backMaster.getId()) != null) {
                throw new PreexistingEntityException("Front2backMaster " + front2backMaster + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Front2backMaster front2backMaster) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Front2backMaster persistentFront2backMaster = em.find(Front2backMaster.class, front2backMaster.getId());
            Details detailsIdOld = persistentFront2backMaster.getDetailsId();
            Details detailsIdNew = front2backMaster.getDetailsId();
            LotControl lotcontrolIdOld = persistentFront2backMaster.getLotcontrolId();
            LotControl lotcontrolIdNew = front2backMaster.getLotcontrolId();
            if (detailsIdNew != null) {
                detailsIdNew = em.getReference(detailsIdNew.getClass(), detailsIdNew.getIdDetail());
                front2backMaster.setDetailsId(detailsIdNew);
            }
            if (lotcontrolIdNew != null) {
                lotcontrolIdNew = em.getReference(lotcontrolIdNew.getClass(), lotcontrolIdNew.getId());
                front2backMaster.setLotcontrolId(lotcontrolIdNew);
            }
            front2backMaster = em.merge(front2backMaster);
            if (detailsIdOld != null && !detailsIdOld.equals(detailsIdNew)) {
                detailsIdOld.getFront2backMasterCollection().remove(front2backMaster);
                detailsIdOld = em.merge(detailsIdOld);
            }
            if (detailsIdNew != null && !detailsIdNew.equals(detailsIdOld)) {
                detailsIdNew.getFront2backMasterCollection().add(front2backMaster);
                detailsIdNew = em.merge(detailsIdNew);
            }
            if (lotcontrolIdOld != null && !lotcontrolIdOld.equals(lotcontrolIdNew)) {
                lotcontrolIdOld.getFront2backMasterCollection().remove(front2backMaster);
                lotcontrolIdOld = em.merge(lotcontrolIdOld);
            }
            if (lotcontrolIdNew != null && !lotcontrolIdNew.equals(lotcontrolIdOld)) {
                lotcontrolIdNew.getFront2backMasterCollection().add(front2backMaster);
                lotcontrolIdNew = em.merge(lotcontrolIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = front2backMaster.getId();
                if (findFront2backMaster(id) == null) {
                    throw new NonexistentEntityException("The front2backMaster with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Front2backMaster front2backMaster;
            try {
                front2backMaster = em.getReference(Front2backMaster.class, id);
                front2backMaster.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The front2backMaster with id " + id + " no longer exists.", enfe);
            }
            Details detailsId = front2backMaster.getDetailsId();
            if (detailsId != null) {
                detailsId.getFront2backMasterCollection().remove(front2backMaster);
                detailsId = em.merge(detailsId);
            }
            LotControl lotcontrolId = front2backMaster.getLotcontrolId();
            if (lotcontrolId != null) {
                lotcontrolId.getFront2backMasterCollection().remove(front2backMaster);
                lotcontrolId = em.merge(lotcontrolId);
            }
            em.remove(front2backMaster);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Front2backMaster> findFront2backMasterEntities() {
        return findFront2backMasterEntities(true, -1, -1);
    }

    public List<Front2backMaster> findFront2backMasterEntities(int maxResults, int firstResult) {
        return findFront2backMasterEntities(false, maxResults, firstResult);
    }

    private List<Front2backMaster> findFront2backMasterEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Front2backMaster.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Front2backMaster findFront2backMaster(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Front2backMaster.class, id);
        } finally {
            em.close();
        }
    }

    public int getFront2backMasterCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Front2backMaster> rt = cq.from(Front2backMaster.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
