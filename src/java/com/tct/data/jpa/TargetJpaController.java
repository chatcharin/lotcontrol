/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import com.appCinfigpage.bean.fc;
import com.icesoft.faces.component.dataexporter.DataExporter;
import com.tct.data.Line;
import com.tct.data.ModelPool;
import com.tct.data.Target;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * Machine User
 *
 * @author pang Development by Chusak laojang Date : Sep 24, 2012, Time :
 * 11:13:40 AM Copy Right 4 Xtreme Co.,Ltd.
 */
public class TargetJpaController implements Serializable {

    public TargetJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Target target) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Line idLine = target.getIdLine();
            if (idLine != null) {
                idLine = em.getReference(idLine.getClass(), idLine.getLineId());
                target.setIdLine(idLine);
            }
            ModelPool idModelPool = target.getIdModelPool();
            if (idModelPool != null) {
                idModelPool = em.getReference(idModelPool.getClass(), idModelPool.getId());
                target.setIdModelPool(idModelPool);
            }
            em.persist(target);
            if (idLine != null) {
                idLine.getTargetCollection().add(target);
                idLine = em.merge(idLine);
            }
            if (idModelPool != null) {
                idModelPool.getTargetCollection().add(target);
                idModelPool = em.merge(idModelPool);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTarget(target.getId()) != null) {
                throw new PreexistingEntityException("Target " + target + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Target target) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Target persistentTarget = em.find(Target.class, target.getId());
            Line idLineOld = persistentTarget.getIdLine();
            Line idLineNew = target.getIdLine();
            ModelPool idModelPoolOld = persistentTarget.getIdModelPool();
            ModelPool idModelPoolNew = target.getIdModelPool();
            if (idLineNew != null) {
                idLineNew = em.getReference(idLineNew.getClass(), idLineNew.getLineId());
                target.setIdLine(idLineNew);
            }
            if (idModelPoolNew != null) {
                idModelPoolNew = em.getReference(idModelPoolNew.getClass(), idModelPoolNew.getId());
                target.setIdModelPool(idModelPoolNew);
            }
            target = em.merge(target);
            if (idLineOld != null && !idLineOld.equals(idLineNew)) {
                idLineOld.getTargetCollection().remove(target);
                idLineOld = em.merge(idLineOld);
            }
            if (idLineNew != null && !idLineNew.equals(idLineOld)) {
                idLineNew.getTargetCollection().add(target);
                idLineNew = em.merge(idLineNew);
            }
            if (idModelPoolOld != null && !idModelPoolOld.equals(idModelPoolNew)) {
                idModelPoolOld.getTargetCollection().remove(target);
                idModelPoolOld = em.merge(idModelPoolOld);
            }
            if (idModelPoolNew != null && !idModelPoolNew.equals(idModelPoolOld)) {
                idModelPoolNew.getTargetCollection().add(target);
                idModelPoolNew = em.merge(idModelPoolNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = target.getId();
                if (findTarget(id) == null) {
                    throw new NonexistentEntityException("The target with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Target target;
            try {
                target = em.getReference(Target.class, id);
                target.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The target with id " + id + " no longer exists.", enfe);
            }
            Line idLine = target.getIdLine();
            if (idLine != null) {
                idLine.getTargetCollection().remove(target);
                idLine = em.merge(idLine);
            }
            ModelPool idModelPool = target.getIdModelPool();
            if (idModelPool != null) {
                idModelPool.getTargetCollection().remove(target);
                idModelPool = em.merge(idModelPool);
            }
            em.remove(target);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Target> findTargetEntities() {
        return findTargetEntities(true, -1, -1);
    }

    public List<Target> findTargetEntities(int maxResults, int firstResult) {
        return findTargetEntities(false, maxResults, firstResult);
    }

    private List<Target> findTargetEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Target.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Target findTarget(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Target.class, id);
        } finally {
            em.close();
        }
    }

    public int getTargetCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Target> rt = cq.from(Target.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<Target> findByIdLine(Line idLine) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("SELECT t FROM Target t WHERE t.idLine=:idLine AND t.deleted = 0 ORDER BY t.dateCreate ASC ").setParameter("idLine", idLine);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<Target> findByIdLineAndCurrentDate(Line line) {
        EntityManager em = getEntityManager();
        List<Target> tmptargets = new ArrayList<Target>();
        List<Target> targets;
        try {
            String sql = "SELECT t FROM Target t WHERE t.idLine=:idLine and t.deleted = 0 ORDER BY t.start ASC";
            Query query = em.createQuery(sql);
            query.setParameter("idLine", line);
            targets = query.getResultList();
            for (Target target : targets) {
                SimpleDateFormat simpledate = new SimpleDateFormat("dd/MM/yyyy");
                if (simpledate.format(target.getDateCreate()).equals(simpledate.format(new Date()))) {
                    tmptargets.add(target);
                }
            }
            return tmptargets;
        } finally {
            em.close();
        }
    }
    
    public List<Target> findAllByNotDeleted(Line line){
        EntityManager em = getEntityManager();
        try{
            String sql = "SELECT t FROM Target t WHERE t.idLine=:idLine AND t.deleted = 0 ORDER BY t.dateCreate ASC";
            Query query = em.createQuery(sql);
            query.setParameter("idLine", line);
            return query.getResultList();
        }finally{
            em.close();
        }
    }
}
