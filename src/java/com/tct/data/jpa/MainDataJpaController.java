/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.ProcessPool;
import com.tct.data.CurrentProcess;
import com.tct.data.LotControl;
import com.tct.data.MainData;
import com.tct.data.jpa.exceptions.IllegalOrphanException;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import com.tct.data.LotControl;
/**
 *
 * @author The_Boy_Cs
 */
public class MainDataJpaController implements Serializable {

    public MainDataJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(MainData mainData) throws PreexistingEntityException, Exception {
        if (mainData.getCurrentProcessCollection() == null) {
            mainData.setCurrentProcessCollection(new ArrayList<CurrentProcess>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ProcessPool idProc = mainData.getIdProc();
            if (idProc != null) {
                idProc = em.getReference(idProc.getClass(), idProc.getIdProc());
                mainData.setIdProc(idProc);
            }
            Collection<CurrentProcess> attachedCurrentProcessCollection = new ArrayList<CurrentProcess>();
            for (CurrentProcess currentProcessCollectionCurrentProcessToAttach : mainData.getCurrentProcessCollection()) {
                currentProcessCollectionCurrentProcessToAttach = em.getReference(currentProcessCollectionCurrentProcessToAttach.getClass(), currentProcessCollectionCurrentProcessToAttach.getId());
                attachedCurrentProcessCollection.add(currentProcessCollectionCurrentProcessToAttach);
            }
            mainData.setCurrentProcessCollection(attachedCurrentProcessCollection);
            em.persist(mainData);
            if (idProc != null) {
                idProc.getMainDataCollection().add(mainData);
                idProc = em.merge(idProc);
            }
            for (CurrentProcess currentProcessCollectionCurrentProcess : mainData.getCurrentProcessCollection()) {
                MainData oldMainDataIdOfCurrentProcessCollectionCurrentProcess = currentProcessCollectionCurrentProcess.getMainDataId();
                currentProcessCollectionCurrentProcess.setMainDataId(mainData);
                currentProcessCollectionCurrentProcess = em.merge(currentProcessCollectionCurrentProcess);
                if (oldMainDataIdOfCurrentProcessCollectionCurrentProcess != null) {
                    oldMainDataIdOfCurrentProcessCollectionCurrentProcess.getCurrentProcessCollection().remove(currentProcessCollectionCurrentProcess);
                    oldMainDataIdOfCurrentProcessCollectionCurrentProcess = em.merge(oldMainDataIdOfCurrentProcessCollectionCurrentProcess);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findMainData(mainData.getId()) != null) {
                throw new PreexistingEntityException("MainData " + mainData + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(MainData mainData) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MainData persistentMainData = em.find(MainData.class, mainData.getId());
            ProcessPool idProcOld = persistentMainData.getIdProc();
            ProcessPool idProcNew = mainData.getIdProc();
            Collection<CurrentProcess> currentProcessCollectionOld = persistentMainData.getCurrentProcessCollection();
            Collection<CurrentProcess> currentProcessCollectionNew = mainData.getCurrentProcessCollection();
            List<String> illegalOrphanMessages = null;
            for (CurrentProcess currentProcessCollectionOldCurrentProcess : currentProcessCollectionOld) {
                if (!currentProcessCollectionNew.contains(currentProcessCollectionOldCurrentProcess)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain CurrentProcess " + currentProcessCollectionOldCurrentProcess + " since its mainDataId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idProcNew != null) {
                idProcNew = em.getReference(idProcNew.getClass(), idProcNew.getIdProc());
                mainData.setIdProc(idProcNew);
            }
            Collection<CurrentProcess> attachedCurrentProcessCollectionNew = new ArrayList<CurrentProcess>();
            for (CurrentProcess currentProcessCollectionNewCurrentProcessToAttach : currentProcessCollectionNew) {
                currentProcessCollectionNewCurrentProcessToAttach = em.getReference(currentProcessCollectionNewCurrentProcessToAttach.getClass(), currentProcessCollectionNewCurrentProcessToAttach.getId());
                attachedCurrentProcessCollectionNew.add(currentProcessCollectionNewCurrentProcessToAttach);
            }
            currentProcessCollectionNew = attachedCurrentProcessCollectionNew;
            mainData.setCurrentProcessCollection(currentProcessCollectionNew);
            mainData = em.merge(mainData);
            if (idProcOld != null && !idProcOld.equals(idProcNew)) {
                idProcOld.getMainDataCollection().remove(mainData);
                idProcOld = em.merge(idProcOld);
            }
            if (idProcNew != null && !idProcNew.equals(idProcOld)) {
                idProcNew.getMainDataCollection().add(mainData);
                idProcNew = em.merge(idProcNew);
            }
            for (CurrentProcess currentProcessCollectionNewCurrentProcess : currentProcessCollectionNew) {
                if (!currentProcessCollectionOld.contains(currentProcessCollectionNewCurrentProcess)) {
                    MainData oldMainDataIdOfCurrentProcessCollectionNewCurrentProcess = currentProcessCollectionNewCurrentProcess.getMainDataId();
                    currentProcessCollectionNewCurrentProcess.setMainDataId(mainData);
                    currentProcessCollectionNewCurrentProcess = em.merge(currentProcessCollectionNewCurrentProcess);
                    if (oldMainDataIdOfCurrentProcessCollectionNewCurrentProcess != null && !oldMainDataIdOfCurrentProcessCollectionNewCurrentProcess.equals(mainData)) {
                        oldMainDataIdOfCurrentProcessCollectionNewCurrentProcess.getCurrentProcessCollection().remove(currentProcessCollectionNewCurrentProcess);
                        oldMainDataIdOfCurrentProcessCollectionNewCurrentProcess = em.merge(oldMainDataIdOfCurrentProcessCollectionNewCurrentProcess);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = mainData.getId();
                if (findMainData(id) == null) {
                    throw new NonexistentEntityException("The mainData with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MainData mainData;
            try {
                mainData = em.getReference(MainData.class, id);
                mainData.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The mainData with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<CurrentProcess> currentProcessCollectionOrphanCheck = mainData.getCurrentProcessCollection();
            for (CurrentProcess currentProcessCollectionOrphanCheckCurrentProcess : currentProcessCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This MainData (" + mainData + ") cannot be destroyed since the CurrentProcess " + currentProcessCollectionOrphanCheckCurrentProcess + " in its currentProcessCollection field has a non-nullable mainDataId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            ProcessPool idProc = mainData.getIdProc();
            if (idProc != null) {
                idProc.getMainDataCollection().remove(mainData);
                idProc = em.merge(idProc);
            }
            em.remove(mainData);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<MainData> findMainDataEntities() {
        return findMainDataEntities(true, -1, -1);
    }

    public List<MainData> findMainDataEntities(int maxResults, int firstResult) {
        return findMainDataEntities(false, maxResults, firstResult);
    }

    private List<MainData> findMainDataEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(MainData.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public MainData findMainData(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(MainData.class, id);
        } finally {
            em.close();
        }
    }

    public int getMainDataCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<MainData> rt = cq.from(MainData.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    //--find  maindata  lot control 

    public List<MainData> findMaindataLotcontrolId() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("MainData.findByLotControlId");
            //q.setParameter("", q);
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public MainData findMainDataByLotControl(LotControl lotControl){ /// tawat findBylotcontrol object
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("MainData.findByLotControlId");
            query.setParameter("lotControlId", lotControl);
            return (MainData) query.getSingleResult(); 
        }
        catch(Exception e)
        {
            return null;
        } 
        finally{
            em.close();
        }
        
    }

    public List<MainData> findMainDataByProcess(ProcessPool pcpool) {
        EntityManager em = getEntityManager();
        Query query = null;
        try {
            query = em.createNamedQuery("MainData.findByIdProc");
            query.setParameter("idProc", pcpool);
            return query.getResultList();
        } finally {
            em.close(); 
        }
    } 
    //--find grop  idLotControl
    public List<MainData>  findGropProcessLotControlId(LotControl idLotControl){
        EntityManager em =  getEntityManager();
        try{
            Query q = em.createNamedQuery("MainData.findByLotControlId");
            q.setParameter("lotControlId",idLotControl);
            return q.getResultList();
        }
        finally{
            em.close(); 
        }  
    
    }
    
    public MainData findMainDataByProcessAndLotControl(ProcessPool pcpool,LotControl lotControl){
        EntityManager em = getEntityManager();
        Query query;
        try {
            query = em.createNamedQuery("MainData.findByIdProcAndLotControlId");
            query.setParameter("idProc", pcpool);
            query.setParameter("lotControlId", lotControl);
            return (MainData) query.getSingleResult();
        }catch(Exception e){
            System.out.println(e);
            return null;
        }
        
        finally{
            em.close();
        }
    }
}
