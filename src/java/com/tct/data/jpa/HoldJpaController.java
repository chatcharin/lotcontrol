/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.CurrentProcess;
import com.tct.data.Hold;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author The_Boy_Cs
 */
public class HoldJpaController implements Serializable {

    public HoldJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Hold hold) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CurrentProcess currentProcessId = hold.getCurrentProcessId();
            if (currentProcessId != null) {
                currentProcessId = em.getReference(currentProcessId.getClass(), currentProcessId.getId());
                hold.setCurrentProcessId(currentProcessId);
            }
            em.persist(hold);
            if (currentProcessId != null) {
                currentProcessId.getHoldCollection().add(hold);
                currentProcessId = em.merge(currentProcessId);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findHold(hold.getId()) != null) {
                throw new PreexistingEntityException("Hold " + hold + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Hold hold) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Hold persistentHold = em.find(Hold.class, hold.getId());
            CurrentProcess currentProcessIdOld = persistentHold.getCurrentProcessId();
            CurrentProcess currentProcessIdNew = hold.getCurrentProcessId();
            if (currentProcessIdNew != null) {
                currentProcessIdNew = em.getReference(currentProcessIdNew.getClass(), currentProcessIdNew.getId());
                hold.setCurrentProcessId(currentProcessIdNew);
            }
            hold = em.merge(hold);
            if (currentProcessIdOld != null && !currentProcessIdOld.equals(currentProcessIdNew)) {
                currentProcessIdOld.getHoldCollection().remove(hold);
                currentProcessIdOld = em.merge(currentProcessIdOld);
            }
            if (currentProcessIdNew != null && !currentProcessIdNew.equals(currentProcessIdOld)) {
                currentProcessIdNew.getHoldCollection().add(hold);
                currentProcessIdNew = em.merge(currentProcessIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = hold.getId();
                if (findHold(id) == null) {
                    throw new NonexistentEntityException("The hold with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Hold hold;
            try {
                hold = em.getReference(Hold.class, id);
                hold.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The hold with id " + id + " no longer exists.", enfe);
            }
            CurrentProcess currentProcessId = hold.getCurrentProcessId();
            if (currentProcessId != null) {
                currentProcessId.getHoldCollection().remove(hold);
                currentProcessId = em.merge(currentProcessId);
            }
            em.remove(hold);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Hold> findHoldEntities() {
        return findHoldEntities(true, -1, -1);
    }

    public List<Hold> findHoldEntities(int maxResults, int firstResult) {
        return findHoldEntities(false, maxResults, firstResult);
    }

    private List<Hold> findHoldEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Hold.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Hold findHold(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Hold.class, id);
        } finally {
            em.close();
        }
    }

    public int getHoldCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Hold> rt = cq.from(Hold.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    //========find id current 
    public Hold  findHoldByIdCurrentProcess(CurrentProcess idCurrentProcess)
    {
        EntityManager em  =  getEntityManager();
        try
        {
            Query  q  = em.createNamedQuery("Hold.findByCurrentProcessId");
            q.setParameter("currentProcessId",idCurrentProcess);
            return (Hold)q.getSingleResult();
        }
        catch(Exception e)
        {
            return null;
        }
        finally
        {
            em.close();;
        }
                
    
    }
    //@pang
    public  Hold  findCheckHold(CurrentProcess idCurrent)
    {
        EntityManager em = getEntityManager();
        try
        {
            Query q  = em.createQuery("SELECT h FROM Hold h WHERE h.currentProcessId =:idCurrent").setParameter("idCurrent", idCurrent);
            return (Hold) q.getSingleResult();
        }
        catch(Exception e)
        {
            return null;
        }
        finally
        {
            em.close();
        }
    }
    
}
