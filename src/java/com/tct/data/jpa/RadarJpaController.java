/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data.jpa;

import com.tct.data.Radar;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.RadarColor;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 28, 2012, Time : 6:30:39 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class RadarJpaController implements Serializable {

    public RadarJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Radar radar) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            RadarColor idColor = radar.getIdColor();
            if (idColor != null) {
                idColor = em.getReference(idColor.getClass(), idColor.getIdRadarColor());
                radar.setIdColor(idColor);
            }
            em.persist(radar);
            if (idColor != null) {
                idColor.getRadarCollection().add(radar);
                idColor = em.merge(idColor);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findRadar(radar.getIdRadar()) != null) {
                throw new PreexistingEntityException("Radar " + radar + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Radar radar) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Radar persistentRadar = em.find(Radar.class, radar.getIdRadar());
            RadarColor idColorOld = persistentRadar.getIdColor();
            RadarColor idColorNew = radar.getIdColor();
            if (idColorNew != null) {
                idColorNew = em.getReference(idColorNew.getClass(), idColorNew.getIdRadarColor());
                radar.setIdColor(idColorNew);
            }
            radar = em.merge(radar);
            if (idColorOld != null && !idColorOld.equals(idColorNew)) {
                idColorOld.getRadarCollection().remove(radar);
                idColorOld = em.merge(idColorOld);
            }
            if (idColorNew != null && !idColorNew.equals(idColorOld)) {
                idColorNew.getRadarCollection().add(radar);
                idColorNew = em.merge(idColorNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = radar.getIdRadar();
                if (findRadar(id) == null) {
                    throw new NonexistentEntityException("The radar with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Radar radar;
            try {
                radar = em.getReference(Radar.class, id);
                radar.getIdRadar();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The radar with id " + id + " no longer exists.", enfe);
            }
            RadarColor idColor = radar.getIdColor();
            if (idColor != null) {
                idColor.getRadarCollection().remove(radar);
                idColor = em.merge(idColor);
            }
            em.remove(radar);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Radar> findRadarEntities() {
        return findRadarEntities(true, -1, -1);
    }

    public List<Radar> findRadarEntities(int maxResults, int firstResult) {
        return findRadarEntities(false, maxResults, firstResult);
    }

    private List<Radar> findRadarEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Radar.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Radar findRadar(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Radar.class, id);
        } finally {
            em.close();
        }
    }

    public int getRadarCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Radar> rt = cq.from(Radar.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    public List<Radar> findByIdColor(RadarColor idRadarColor) {
        EntityManager em = getEntityManager();
        try {
             Query q = em.createQuery("SELECT r FROM Radar r WHERE r.idColor =:idColor and r.deleted = 0 ORDER BY r.lineNo ASC").setParameter("idColor",idRadarColor);
             return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    public List<Radar> findAllAndNotDeleted(){
        EntityManager em = getEntityManager();
        try{
            String sql = "SELECT r FROM Radar r WHERE r.deleted = 0";
            Query query = em.createQuery(sql);
            return query.getResultList();
        }finally{
            em.close();
        }
    }

}
