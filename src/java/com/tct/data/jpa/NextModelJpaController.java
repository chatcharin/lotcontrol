/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.Line;
import com.tct.data.NextModel;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * Machine User
 *
 * @author pang Development by Chusak laojang Date : Sep 25, 2012, Time :
 * 7:00:30 PM Copy Right 4 Xtreme Co.,Ltd.
 */
public class NextModelJpaController implements Serializable {

    public NextModelJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(NextModel nextModel) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Line idLine = nextModel.getIdLine();
            if (idLine != null) {
                idLine = em.getReference(idLine.getClass(), idLine.getLineId());
                nextModel.setIdLine(idLine);
            }
            em.persist(nextModel);
            if (idLine != null) {
                idLine.getNextModelCollection().add(nextModel);
                idLine = em.merge(idLine);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findNextModel(nextModel.getIdNextModel()) != null) {
                throw new PreexistingEntityException("NextModel " + nextModel + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(NextModel nextModel) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            NextModel persistentNextModel = em.find(NextModel.class, nextModel.getIdNextModel());
            Line idLineOld = persistentNextModel.getIdLine();
            Line idLineNew = nextModel.getIdLine();
            if (idLineNew != null) {
                idLineNew = em.getReference(idLineNew.getClass(), idLineNew.getLineId());
                nextModel.setIdLine(idLineNew);
            }
            nextModel = em.merge(nextModel);
            if (idLineOld != null && !idLineOld.equals(idLineNew)) {
                idLineOld.getNextModelCollection().remove(nextModel);
                idLineOld = em.merge(idLineOld);
            }
            if (idLineNew != null && !idLineNew.equals(idLineOld)) {
                idLineNew.getNextModelCollection().add(nextModel);
                idLineNew = em.merge(idLineNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = nextModel.getIdNextModel();
                if (findNextModel(id) == null) {
                    throw new NonexistentEntityException("The nextModel with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            NextModel nextModel;
            try {
                nextModel = em.getReference(NextModel.class, id);
                nextModel.getIdNextModel();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The nextModel with id " + id + " no longer exists.", enfe);
            }
            Line idLine = nextModel.getIdLine();
            if (idLine != null) {
                idLine.getNextModelCollection().remove(nextModel);
                idLine = em.merge(idLine);
            }
            em.remove(nextModel);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<NextModel> findNextModelEntities() {
        return findNextModelEntities(true, -1, -1);
    }

    public List<NextModel> findNextModelEntities(int maxResults, int firstResult) {
        return findNextModelEntities(false, maxResults, firstResult);
    }

    private List<NextModel> findNextModelEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(NextModel.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public NextModel findNextModel(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(NextModel.class, id);
        } finally {
            em.close();
        }
    }

    public int getNextModelCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<NextModel> rt = cq.from(NextModel.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<NextModel> findByLineAndNotDeleted(Line line) {
        EntityManager em = getEntityManager();
        try {
            String sql = "SELECT n FROM NextModel n WHERE n.idLine = :idLine AND n.deleted = 0";
            Query query = em.createQuery(sql);
            query.setParameter("idLine", line);
            return query.getResultList();
        } finally {
            em.close();
        }
    } 
}
