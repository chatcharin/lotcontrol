/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.Radar;
import com.tct.data.RadarColor;
import com.tct.data.jpa.exceptions.IllegalOrphanException;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 28, 2012, Time : 5:06:33 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class RadarColorJpaController implements Serializable {

    public RadarColorJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(RadarColor radarColor) throws PreexistingEntityException, Exception {
        if (radarColor.getRadarCollection() == null) {
            radarColor.setRadarCollection(new ArrayList<Radar>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Radar> attachedRadarCollection = new ArrayList<Radar>();
            for (Radar radarCollectionRadarToAttach : radarColor.getRadarCollection()) {
                radarCollectionRadarToAttach = em.getReference(radarCollectionRadarToAttach.getClass(), radarCollectionRadarToAttach.getIdRadar());
                attachedRadarCollection.add(radarCollectionRadarToAttach);
            }
            radarColor.setRadarCollection(attachedRadarCollection);
            em.persist(radarColor);
            for (Radar radarCollectionRadar : radarColor.getRadarCollection()) {
                RadarColor oldIdColorOfRadarCollectionRadar = radarCollectionRadar.getIdColor();
                radarCollectionRadar.setIdColor(radarColor);
                radarCollectionRadar = em.merge(radarCollectionRadar);
                if (oldIdColorOfRadarCollectionRadar != null) {
                    oldIdColorOfRadarCollectionRadar.getRadarCollection().remove(radarCollectionRadar);
                    oldIdColorOfRadarCollectionRadar = em.merge(oldIdColorOfRadarCollectionRadar);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findRadarColor(radarColor.getIdRadarColor()) != null) {
                throw new PreexistingEntityException("RadarColor " + radarColor + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(RadarColor radarColor) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            RadarColor persistentRadarColor = em.find(RadarColor.class, radarColor.getIdRadarColor());
            Collection<Radar> radarCollectionOld = persistentRadarColor.getRadarCollection();
            Collection<Radar> radarCollectionNew = radarColor.getRadarCollection();
            List<String> illegalOrphanMessages = null;
            for (Radar radarCollectionOldRadar : radarCollectionOld) {
                if (!radarCollectionNew.contains(radarCollectionOldRadar)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Radar " + radarCollectionOldRadar + " since its idColor field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Radar> attachedRadarCollectionNew = new ArrayList<Radar>();
            for (Radar radarCollectionNewRadarToAttach : radarCollectionNew) {
                radarCollectionNewRadarToAttach = em.getReference(radarCollectionNewRadarToAttach.getClass(), radarCollectionNewRadarToAttach.getIdRadar());
                attachedRadarCollectionNew.add(radarCollectionNewRadarToAttach);
            }
            radarCollectionNew = attachedRadarCollectionNew;
            radarColor.setRadarCollection(radarCollectionNew);
            radarColor = em.merge(radarColor);
            for (Radar radarCollectionNewRadar : radarCollectionNew) {
                if (!radarCollectionOld.contains(radarCollectionNewRadar)) {
                    RadarColor oldIdColorOfRadarCollectionNewRadar = radarCollectionNewRadar.getIdColor();
                    radarCollectionNewRadar.setIdColor(radarColor);
                    radarCollectionNewRadar = em.merge(radarCollectionNewRadar);
                    if (oldIdColorOfRadarCollectionNewRadar != null && !oldIdColorOfRadarCollectionNewRadar.equals(radarColor)) {
                        oldIdColorOfRadarCollectionNewRadar.getRadarCollection().remove(radarCollectionNewRadar);
                        oldIdColorOfRadarCollectionNewRadar = em.merge(oldIdColorOfRadarCollectionNewRadar);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = radarColor.getIdRadarColor();
                if (findRadarColor(id) == null) {
                    throw new NonexistentEntityException("The radarColor with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            RadarColor radarColor;
            try {
                radarColor = em.getReference(RadarColor.class, id);
                radarColor.getIdRadarColor();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The radarColor with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Radar> radarCollectionOrphanCheck = radarColor.getRadarCollection();
            for (Radar radarCollectionOrphanCheckRadar : radarCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This RadarColor (" + radarColor + ") cannot be destroyed since the Radar " + radarCollectionOrphanCheckRadar + " in its radarCollection field has a non-nullable idColor field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(radarColor);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<RadarColor> findRadarColorEntities() {
        return findRadarColorEntities(true, -1, -1);
    }

    public List<RadarColor> findRadarColorEntities(int maxResults, int firstResult) {
        return findRadarColorEntities(false, maxResults, firstResult);
    }

    private List<RadarColor> findRadarColorEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(RadarColor.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public RadarColor findRadarColor(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(RadarColor.class, id);
        } finally {
            em.close();
        }
    }

    public int getRadarColorCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<RadarColor> rt = cq.from(RadarColor.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    public RadarColor findAllOne()
    {
        EntityManager em = getEntityManager();
        try
        {
            Query q  = em.createQuery("SELECT r FROM RadarColor r WHERE r.deleted = 0");
            return (RadarColor) q.getSingleResult();
        }
        catch(Exception e)
        {
            return null;
        }
        finally
        {
            em.close();
        }
    }

}
