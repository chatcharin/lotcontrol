/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.ProcessPool;
import com.tct.data.NgPool;
import com.tct.data.Pc2ng;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author The_Boy_Cs
 */
public class Pc2ngJpaController implements Serializable {

    public Pc2ngJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    public List<Pc2ng> findByIdProc(ProcessPool proc){
        EntityManager em = getEntityManager();
        try {
            Query query = em.createQuery("select p from Pc2ng p where p.idProc : idProc");
            query.setParameter("idProc", proc);
            return query.getResultList();
        } finally {
            em.close();
        }
    }

    public void create(Pc2ng pc2ng) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ProcessPool idProc = pc2ng.getIdProc();
            if (idProc != null) {
                idProc = em.getReference(idProc.getClass(), idProc.getIdProc());
                pc2ng.setIdProc(idProc);
            }
            NgPool idNg = pc2ng.getIdNg();
            if (idNg != null) {
                idNg = em.getReference(idNg.getClass(), idNg.getIdNg());
                pc2ng.setIdNg(idNg);
            }
            em.persist(pc2ng);
            if (idProc != null) {
                idProc.getPc2ngCollection().add(pc2ng);
                idProc = em.merge(idProc);
            }
            if (idNg != null) {
                idNg.getPc2ngCollection().add(pc2ng);
                idNg = em.merge(idNg);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPc2ng(pc2ng.getPc2ngId()) != null) {
                throw new PreexistingEntityException("Pc2ng " + pc2ng + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Pc2ng pc2ng) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pc2ng persistentPc2ng = em.find(Pc2ng.class, pc2ng.getPc2ngId());
            ProcessPool idProcOld = persistentPc2ng.getIdProc();
            ProcessPool idProcNew = pc2ng.getIdProc();
            NgPool idNgOld = persistentPc2ng.getIdNg();
            NgPool idNgNew = pc2ng.getIdNg();
            if (idProcNew != null) {
                idProcNew = em.getReference(idProcNew.getClass(), idProcNew.getIdProc());
                pc2ng.setIdProc(idProcNew);
            }
            if (idNgNew != null) {
                idNgNew = em.getReference(idNgNew.getClass(), idNgNew.getIdNg());
                pc2ng.setIdNg(idNgNew);
            }
            pc2ng = em.merge(pc2ng);
            if (idProcOld != null && !idProcOld.equals(idProcNew)) {
                idProcOld.getPc2ngCollection().remove(pc2ng);
                idProcOld = em.merge(idProcOld);
            }
            if (idProcNew != null && !idProcNew.equals(idProcOld)) {
                idProcNew.getPc2ngCollection().add(pc2ng);
                idProcNew = em.merge(idProcNew);
            }
            if (idNgOld != null && !idNgOld.equals(idNgNew)) {
                idNgOld.getPc2ngCollection().remove(pc2ng);
                idNgOld = em.merge(idNgOld);
            }
            if (idNgNew != null && !idNgNew.equals(idNgOld)) {
                idNgNew.getPc2ngCollection().add(pc2ng);
                idNgNew = em.merge(idNgNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = pc2ng.getPc2ngId();
                if (findPc2ng(id) == null) {
                    throw new NonexistentEntityException("The pc2ng with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pc2ng pc2ng;
            try {
                pc2ng = em.getReference(Pc2ng.class, id);
                pc2ng.getPc2ngId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The pc2ng with id " + id + " no longer exists.", enfe);
            }
            ProcessPool idProc = pc2ng.getIdProc();
            if (idProc != null) {
                idProc.getPc2ngCollection().remove(pc2ng);
                idProc = em.merge(idProc);
            }
            NgPool idNg = pc2ng.getIdNg();
            if (idNg != null) {
                idNg.getPc2ngCollection().remove(pc2ng);
                idNg = em.merge(idNg);
            }
            em.remove(pc2ng);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Pc2ng> findPc2ngEntities() {
        return findPc2ngEntities(true, -1, -1);
    }

    public List<Pc2ng> findPc2ngEntities(int maxResults, int firstResult) {
        return findPc2ngEntities(false, maxResults, firstResult);
    }

    private List<Pc2ng> findPc2ngEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Pc2ng.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Pc2ng findPc2ng(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Pc2ng.class, id);
        } finally {
            em.close();
        }
    }
   //---find   id_n_pool
   public  List<Pc2ng> findIdNgPool(ProcessPool id){
       EntityManager em  =  getEntityManager();
       try{
           Query q  = em.createQuery("select c from Pc2ng c where c.idProc = :idProc"); 
           q.setParameter("idProc", id);
           return  q.getResultList();
       }
       finally
       {
           em.close();
       }    
   
   
   }
    
    public int getPc2ngCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Pc2ng> rt = cq.from(Pc2ng.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public Pc2ng findPc2ngByngPool(NgPool ngPool,ProcessPool processPool){
        EntityManager em = getEntityManager();
        try{
            Query query = em.createNamedQuery("Pc2ng.findByNgPoolAndProcessPool");
            query.setParameter("idNg", ngPool);
            query.setParameter("", processPool);
            return (Pc2ng) query.getSingleResult();
        }finally{
            em.close();
        }
    }
    
}
