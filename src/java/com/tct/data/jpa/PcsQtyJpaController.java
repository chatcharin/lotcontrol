/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.CurrentProcess;
import com.tct.data.PcsQty;
import com.tct.data.QaSampling;
import com.tct.data.jpa.exceptions.IllegalOrphanException;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author The_Boy_Cs
 */
public class PcsQtyJpaController implements Serializable {

    public PcsQtyJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(PcsQty pcsQty) throws PreexistingEntityException, Exception {
        if (pcsQty.getQaSamplingCollection() == null) {
            pcsQty.setQaSamplingCollection(new ArrayList<QaSampling>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CurrentProcess currentProcessId = pcsQty.getCurrentProcessId();
            if (currentProcessId != null) {
                currentProcessId = em.getReference(currentProcessId.getClass(), currentProcessId.getId());
                pcsQty.setCurrentProcessId(currentProcessId);
            }
            Collection<QaSampling> attachedQaSamplingCollection = new ArrayList<QaSampling>();
            for (QaSampling qaSamplingCollectionQaSamplingToAttach : pcsQty.getQaSamplingCollection()) {
                qaSamplingCollectionQaSamplingToAttach = em.getReference(qaSamplingCollectionQaSamplingToAttach.getClass(), qaSamplingCollectionQaSamplingToAttach.getIdSampling());
                attachedQaSamplingCollection.add(qaSamplingCollectionQaSamplingToAttach);
            }
            pcsQty.setQaSamplingCollection(attachedQaSamplingCollection);
            em.persist(pcsQty);
            if (currentProcessId != null) {
                currentProcessId.getPcsQtyCollection().add(pcsQty);
                currentProcessId = em.merge(currentProcessId);
            }
            for (QaSampling qaSamplingCollectionQaSampling : pcsQty.getQaSamplingCollection()) {
                PcsQty oldIdPcsqtyOfQaSamplingCollectionQaSampling = qaSamplingCollectionQaSampling.getIdPcsqty();
                qaSamplingCollectionQaSampling.setIdPcsqty(pcsQty);
                qaSamplingCollectionQaSampling = em.merge(qaSamplingCollectionQaSampling);
                if (oldIdPcsqtyOfQaSamplingCollectionQaSampling != null) {
                    oldIdPcsqtyOfQaSamplingCollectionQaSampling.getQaSamplingCollection().remove(qaSamplingCollectionQaSampling);
                    oldIdPcsqtyOfQaSamplingCollectionQaSampling = em.merge(oldIdPcsqtyOfQaSamplingCollectionQaSampling);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPcsQty(pcsQty.getId()) != null) {
                throw new PreexistingEntityException("PcsQty " + pcsQty + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PcsQty pcsQty) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PcsQty persistentPcsQty = em.find(PcsQty.class, pcsQty.getId());
            CurrentProcess currentProcessIdOld = persistentPcsQty.getCurrentProcessId();
            CurrentProcess currentProcessIdNew = pcsQty.getCurrentProcessId();
            Collection<QaSampling> qaSamplingCollectionOld = persistentPcsQty.getQaSamplingCollection();
            Collection<QaSampling> qaSamplingCollectionNew = pcsQty.getQaSamplingCollection();
            List<String> illegalOrphanMessages = null;
            for (QaSampling qaSamplingCollectionOldQaSampling : qaSamplingCollectionOld) {
                if (!qaSamplingCollectionNew.contains(qaSamplingCollectionOldQaSampling)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain QaSampling " + qaSamplingCollectionOldQaSampling + " since its idPcsqty field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (currentProcessIdNew != null) {
                currentProcessIdNew = em.getReference(currentProcessIdNew.getClass(), currentProcessIdNew.getId());
                pcsQty.setCurrentProcessId(currentProcessIdNew);
            }
            Collection<QaSampling> attachedQaSamplingCollectionNew = new ArrayList<QaSampling>();
            for (QaSampling qaSamplingCollectionNewQaSamplingToAttach : qaSamplingCollectionNew) {
                qaSamplingCollectionNewQaSamplingToAttach = em.getReference(qaSamplingCollectionNewQaSamplingToAttach.getClass(), qaSamplingCollectionNewQaSamplingToAttach.getIdSampling());
                attachedQaSamplingCollectionNew.add(qaSamplingCollectionNewQaSamplingToAttach);
            }
            qaSamplingCollectionNew = attachedQaSamplingCollectionNew;
            pcsQty.setQaSamplingCollection(qaSamplingCollectionNew);
            pcsQty = em.merge(pcsQty);
            if (currentProcessIdOld != null && !currentProcessIdOld.equals(currentProcessIdNew)) {
                currentProcessIdOld.getPcsQtyCollection().remove(pcsQty);
                currentProcessIdOld = em.merge(currentProcessIdOld);
            }
            if (currentProcessIdNew != null && !currentProcessIdNew.equals(currentProcessIdOld)) {
                currentProcessIdNew.getPcsQtyCollection().add(pcsQty);
                currentProcessIdNew = em.merge(currentProcessIdNew);
            }
            for (QaSampling qaSamplingCollectionNewQaSampling : qaSamplingCollectionNew) {
                if (!qaSamplingCollectionOld.contains(qaSamplingCollectionNewQaSampling)) {
                    PcsQty oldIdPcsqtyOfQaSamplingCollectionNewQaSampling = qaSamplingCollectionNewQaSampling.getIdPcsqty();
                    qaSamplingCollectionNewQaSampling.setIdPcsqty(pcsQty);
                    qaSamplingCollectionNewQaSampling = em.merge(qaSamplingCollectionNewQaSampling);
                    if (oldIdPcsqtyOfQaSamplingCollectionNewQaSampling != null && !oldIdPcsqtyOfQaSamplingCollectionNewQaSampling.equals(pcsQty)) {
                        oldIdPcsqtyOfQaSamplingCollectionNewQaSampling.getQaSamplingCollection().remove(qaSamplingCollectionNewQaSampling);
                        oldIdPcsqtyOfQaSamplingCollectionNewQaSampling = em.merge(oldIdPcsqtyOfQaSamplingCollectionNewQaSampling);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = pcsQty.getId();
                if (findPcsQty(id) == null) {
                    throw new NonexistentEntityException("The pcsQty with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PcsQty pcsQty;
            try {
                pcsQty = em.getReference(PcsQty.class, id);
                pcsQty.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The pcsQty with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<QaSampling> qaSamplingCollectionOrphanCheck = pcsQty.getQaSamplingCollection();
            for (QaSampling qaSamplingCollectionOrphanCheckQaSampling : qaSamplingCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This PcsQty (" + pcsQty + ") cannot be destroyed since the QaSampling " + qaSamplingCollectionOrphanCheckQaSampling + " in its qaSamplingCollection field has a non-nullable idPcsqty field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            CurrentProcess currentProcessId = pcsQty.getCurrentProcessId();
            if (currentProcessId != null) {
                currentProcessId.getPcsQtyCollection().remove(pcsQty);
                currentProcessId = em.merge(currentProcessId);
            }
            em.remove(pcsQty);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PcsQty> findPcsQtyEntities() {
        return findPcsQtyEntities(true, -1, -1);
    }

    public List<PcsQty> findPcsQtyEntities(int maxResults, int firstResult) {
        return findPcsQtyEntities(false, maxResults, firstResult);
    }

    private List<PcsQty> findPcsQtyEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(PcsQty.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PcsQty findPcsQty(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(PcsQty.class, id);
        } finally {
            em.close();
        }
    }

    public int getPcsQtyCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<PcsQty> rt = cq.from(PcsQty.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    
    public List<PcsQty> findPcsQtyByType(){
        EntityManager em = getEntityManager();
        try {
            Query query = em.createNamedQuery("PcsQty.findByQtyType");
            query.setParameter("qtyType", "output");
            return query.getResultList();
        }finally{
            em.close();
        }
    }
    //============= find current id
   public PcsQty findPcsQtyCurrentProcessId(CurrentProcess idCurentProcess){
        EntityManager em = getEntityManager();
        Query query;
        try {
            System.out.println(idCurentProcess.getId());
            query = em.createNamedQuery("PcsQty.findByCurrentProcessId");
            query.setParameter("currentProcessId", idCurentProcess); 
            return (PcsQty) query.getSingleResult();
        }catch(Exception e){
            System.out.println(e);
            return null;
        }
        
        finally{
            em.close();
        }
    }
   public PcsQty findByIdCurrentProcessAndStatusClose(CurrentProcess idCurrentProcess)
   {
       EntityManager em  = getEntityManager();
       try
       {
           Query q  = em.createQuery("SELECT p FROM PcsQty p  WHERE p.currentProcessId=:idCurrentProcess and p.qtyType='out'");
           q.setParameter("idCurrentProcess", idCurrentProcess);
           return (PcsQty) q.getSingleResult();
       }
       finally
       {
           em.close();;
       }
   }
    
}
