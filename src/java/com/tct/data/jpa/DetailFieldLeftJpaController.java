/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data.jpa;

//import com.sun.xml.internal.messaging.saaj.soap.ver1_1.Detail1_1Impl;
import com.tct.data.DetailFieldLeft;
import com.tct.data.DetailFieldLeftData;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.DetailSpecial;
import com.tct.data.FieldName;
import com.tct.data.jpa.exceptions.IllegalOrphanException;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 19, 2012, Time : 3:00:28 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class DetailFieldLeftJpaController implements Serializable {

     public DetailFieldLeftJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DetailFieldLeft detailFieldLeft) throws PreexistingEntityException, Exception {
        if (detailFieldLeft.getDetailFieldLeftDataCollection() == null) {
            detailFieldLeft.setDetailFieldLeftDataCollection(new ArrayList<DetailFieldLeftData>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            FieldName idFieldName = detailFieldLeft.getIdFieldName();
            if (idFieldName != null) {
                idFieldName = em.getReference(idFieldName.getClass(), idFieldName.getId());
                detailFieldLeft.setIdFieldName(idFieldName);
            }
            DetailSpecial idDetailSpecail = detailFieldLeft.getIdDetailSpecail();
            if (idDetailSpecail != null) {
                idDetailSpecail = em.getReference(idDetailSpecail.getClass(), idDetailSpecail.getId());
                detailFieldLeft.setIdDetailSpecail(idDetailSpecail);
            }
            Collection<DetailFieldLeftData> attachedDetailFieldLeftDataCollection = new ArrayList<DetailFieldLeftData>();
            for (DetailFieldLeftData detailFieldLeftDataCollectionDetailFieldLeftDataToAttach : detailFieldLeft.getDetailFieldLeftDataCollection()) {
                detailFieldLeftDataCollectionDetailFieldLeftDataToAttach = em.getReference(detailFieldLeftDataCollectionDetailFieldLeftDataToAttach.getClass(), detailFieldLeftDataCollectionDetailFieldLeftDataToAttach.getIdDetailSpecailData());
                attachedDetailFieldLeftDataCollection.add(detailFieldLeftDataCollectionDetailFieldLeftDataToAttach);
            }
            detailFieldLeft.setDetailFieldLeftDataCollection(attachedDetailFieldLeftDataCollection);
            em.persist(detailFieldLeft);
            if (idFieldName != null) {
                idFieldName.getDetailFieldLeftCollection().add(detailFieldLeft);
                idFieldName = em.merge(idFieldName);
            }
            if (idDetailSpecail != null) {
                idDetailSpecail.getDetailFieldLeftCollection().add(detailFieldLeft);
                idDetailSpecail = em.merge(idDetailSpecail);
            }
            for (DetailFieldLeftData detailFieldLeftDataCollectionDetailFieldLeftData : detailFieldLeft.getDetailFieldLeftDataCollection()) {
                DetailFieldLeft oldIdDetailFieldLeftOfDetailFieldLeftDataCollectionDetailFieldLeftData = detailFieldLeftDataCollectionDetailFieldLeftData.getIdDetailFieldLeft();
                detailFieldLeftDataCollectionDetailFieldLeftData.setIdDetailFieldLeft(detailFieldLeft);
                detailFieldLeftDataCollectionDetailFieldLeftData = em.merge(detailFieldLeftDataCollectionDetailFieldLeftData);
                if (oldIdDetailFieldLeftOfDetailFieldLeftDataCollectionDetailFieldLeftData != null) {
                    oldIdDetailFieldLeftOfDetailFieldLeftDataCollectionDetailFieldLeftData.getDetailFieldLeftDataCollection().remove(detailFieldLeftDataCollectionDetailFieldLeftData);
                    oldIdDetailFieldLeftOfDetailFieldLeftDataCollectionDetailFieldLeftData = em.merge(oldIdDetailFieldLeftOfDetailFieldLeftDataCollectionDetailFieldLeftData);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDetailFieldLeft(detailFieldLeft.getId()) != null) {
                throw new PreexistingEntityException("DetailFieldLeft " + detailFieldLeft + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DetailFieldLeft detailFieldLeft) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailFieldLeft persistentDetailFieldLeft = em.find(DetailFieldLeft.class, detailFieldLeft.getId());
            FieldName idFieldNameOld = persistentDetailFieldLeft.getIdFieldName();
            FieldName idFieldNameNew = detailFieldLeft.getIdFieldName();
            DetailSpecial idDetailSpecailOld = persistentDetailFieldLeft.getIdDetailSpecail();
            DetailSpecial idDetailSpecailNew = detailFieldLeft.getIdDetailSpecail();
            Collection<DetailFieldLeftData> detailFieldLeftDataCollectionOld = persistentDetailFieldLeft.getDetailFieldLeftDataCollection();
            Collection<DetailFieldLeftData> detailFieldLeftDataCollectionNew = detailFieldLeft.getDetailFieldLeftDataCollection();
            List<String> illegalOrphanMessages = null;
            for (DetailFieldLeftData detailFieldLeftDataCollectionOldDetailFieldLeftData : detailFieldLeftDataCollectionOld) {
                if (!detailFieldLeftDataCollectionNew.contains(detailFieldLeftDataCollectionOldDetailFieldLeftData)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain DetailFieldLeftData " + detailFieldLeftDataCollectionOldDetailFieldLeftData + " since its idDetailFieldLeft field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idFieldNameNew != null) {
                idFieldNameNew = em.getReference(idFieldNameNew.getClass(), idFieldNameNew.getId());
                detailFieldLeft.setIdFieldName(idFieldNameNew);
            }
            if (idDetailSpecailNew != null) {
                idDetailSpecailNew = em.getReference(idDetailSpecailNew.getClass(), idDetailSpecailNew.getId());
                detailFieldLeft.setIdDetailSpecail(idDetailSpecailNew);
            }
            Collection<DetailFieldLeftData> attachedDetailFieldLeftDataCollectionNew = new ArrayList<DetailFieldLeftData>();
            for (DetailFieldLeftData detailFieldLeftDataCollectionNewDetailFieldLeftDataToAttach : detailFieldLeftDataCollectionNew) {
                detailFieldLeftDataCollectionNewDetailFieldLeftDataToAttach = em.getReference(detailFieldLeftDataCollectionNewDetailFieldLeftDataToAttach.getClass(), detailFieldLeftDataCollectionNewDetailFieldLeftDataToAttach.getIdDetailSpecailData());
                attachedDetailFieldLeftDataCollectionNew.add(detailFieldLeftDataCollectionNewDetailFieldLeftDataToAttach);
            }
            detailFieldLeftDataCollectionNew = attachedDetailFieldLeftDataCollectionNew;
            detailFieldLeft.setDetailFieldLeftDataCollection(detailFieldLeftDataCollectionNew);
            detailFieldLeft = em.merge(detailFieldLeft);
            if (idFieldNameOld != null && !idFieldNameOld.equals(idFieldNameNew)) {
                idFieldNameOld.getDetailFieldLeftCollection().remove(detailFieldLeft);
                idFieldNameOld = em.merge(idFieldNameOld);
            }
            if (idFieldNameNew != null && !idFieldNameNew.equals(idFieldNameOld)) {
                idFieldNameNew.getDetailFieldLeftCollection().add(detailFieldLeft);
                idFieldNameNew = em.merge(idFieldNameNew);
            }
            if (idDetailSpecailOld != null && !idDetailSpecailOld.equals(idDetailSpecailNew)) {
                idDetailSpecailOld.getDetailFieldLeftCollection().remove(detailFieldLeft);
                idDetailSpecailOld = em.merge(idDetailSpecailOld);
            }
            if (idDetailSpecailNew != null && !idDetailSpecailNew.equals(idDetailSpecailOld)) {
                idDetailSpecailNew.getDetailFieldLeftCollection().add(detailFieldLeft);
                idDetailSpecailNew = em.merge(idDetailSpecailNew);
            }
            for (DetailFieldLeftData detailFieldLeftDataCollectionNewDetailFieldLeftData : detailFieldLeftDataCollectionNew) {
                if (!detailFieldLeftDataCollectionOld.contains(detailFieldLeftDataCollectionNewDetailFieldLeftData)) {
                    DetailFieldLeft oldIdDetailFieldLeftOfDetailFieldLeftDataCollectionNewDetailFieldLeftData = detailFieldLeftDataCollectionNewDetailFieldLeftData.getIdDetailFieldLeft();
                    detailFieldLeftDataCollectionNewDetailFieldLeftData.setIdDetailFieldLeft(detailFieldLeft);
                    detailFieldLeftDataCollectionNewDetailFieldLeftData = em.merge(detailFieldLeftDataCollectionNewDetailFieldLeftData);
                    if (oldIdDetailFieldLeftOfDetailFieldLeftDataCollectionNewDetailFieldLeftData != null && !oldIdDetailFieldLeftOfDetailFieldLeftDataCollectionNewDetailFieldLeftData.equals(detailFieldLeft)) {
                        oldIdDetailFieldLeftOfDetailFieldLeftDataCollectionNewDetailFieldLeftData.getDetailFieldLeftDataCollection().remove(detailFieldLeftDataCollectionNewDetailFieldLeftData);
                        oldIdDetailFieldLeftOfDetailFieldLeftDataCollectionNewDetailFieldLeftData = em.merge(oldIdDetailFieldLeftOfDetailFieldLeftDataCollectionNewDetailFieldLeftData);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = detailFieldLeft.getId();
                if (findDetailFieldLeft(id) == null) {
                    throw new NonexistentEntityException("The detailFieldLeft with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailFieldLeft detailFieldLeft;
            try {
                detailFieldLeft = em.getReference(DetailFieldLeft.class, id);
                detailFieldLeft.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The detailFieldLeft with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<DetailFieldLeftData> detailFieldLeftDataCollectionOrphanCheck = detailFieldLeft.getDetailFieldLeftDataCollection();
            for (DetailFieldLeftData detailFieldLeftDataCollectionOrphanCheckDetailFieldLeftData : detailFieldLeftDataCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This DetailFieldLeft (" + detailFieldLeft + ") cannot be destroyed since the DetailFieldLeftData " + detailFieldLeftDataCollectionOrphanCheckDetailFieldLeftData + " in its detailFieldLeftDataCollection field has a non-nullable idDetailFieldLeft field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            FieldName idFieldName = detailFieldLeft.getIdFieldName();
            if (idFieldName != null) {
                idFieldName.getDetailFieldLeftCollection().remove(detailFieldLeft);
                idFieldName = em.merge(idFieldName);
            }
            DetailSpecial idDetailSpecail = detailFieldLeft.getIdDetailSpecail();
            if (idDetailSpecail != null) {
                idDetailSpecail.getDetailFieldLeftCollection().remove(detailFieldLeft);
                idDetailSpecail = em.merge(idDetailSpecail);
            }
            em.remove(detailFieldLeft);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DetailFieldLeft> findDetailFieldLeftEntities() {
        return findDetailFieldLeftEntities(true, -1, -1);
    }

    public List<DetailFieldLeft> findDetailFieldLeftEntities(int maxResults, int firstResult) {
        return findDetailFieldLeftEntities(false, maxResults, firstResult);
    }

    private List<DetailFieldLeft> findDetailFieldLeftEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(DetailFieldLeft.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DetailFieldLeft findDetailFieldLeft(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DetailFieldLeft.class, id);
        } finally {
            em.close();
        }
    }

    public int getDetailFieldLeftCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<DetailFieldLeft> rt = cq.from(DetailFieldLeft.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    public List<DetailFieldLeft> findByDetialSpecial(DetailSpecial detailSpecial)
    {
        EntityManager em  = getEntityManager();
        try
        {
            Query q = em.createQuery("SELECT d FROM DetailFieldLeft d WHERE d.deleted = 0 and d.idDetailSpecail=:idDetailSpecail").setParameter("idDetailSpecail",detailSpecial);
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }

}
