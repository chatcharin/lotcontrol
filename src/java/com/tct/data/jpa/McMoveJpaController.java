/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.Mc;
import com.tct.data.CurrentProcess;
import com.tct.data.McMove;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author The_Boy_Cs
 */
public class McMoveJpaController implements Serializable {

    public McMoveJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(McMove mcMove) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Mc beforMc = mcMove.getBeforMc();
            if (beforMc != null) {
                beforMc = em.getReference(beforMc.getClass(), beforMc.getId());
                mcMove.setBeforMc(beforMc);
            }
            CurrentProcess currentProcessId = mcMove.getCurrentProcessId();
            if (currentProcessId != null) {
                currentProcessId = em.getReference(currentProcessId.getClass(), currentProcessId.getId());
                mcMove.setCurrentProcessId(currentProcessId);
            }
            Mc aftorMc = mcMove.getAftorMc();
            if (aftorMc != null) {
                aftorMc = em.getReference(aftorMc.getClass(), aftorMc.getId());
                mcMove.setAftorMc(aftorMc);
            }
            em.persist(mcMove);
            if (beforMc != null) {
                beforMc.getMcMoveCollection().add(mcMove);
                beforMc = em.merge(beforMc);
            }
            if (currentProcessId != null) {
                currentProcessId.getMcMoveCollection().add(mcMove);
                currentProcessId = em.merge(currentProcessId);
            }
            if (aftorMc != null) {
                aftorMc.getMcMoveCollection().add(mcMove);
                aftorMc = em.merge(aftorMc);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findMcMove(mcMove.getId()) != null) {
                throw new PreexistingEntityException("McMove " + mcMove + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(McMove mcMove) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            McMove persistentMcMove = em.find(McMove.class, mcMove.getId());
            Mc beforMcOld = persistentMcMove.getBeforMc();
            Mc beforMcNew = mcMove.getBeforMc();
            CurrentProcess currentProcessIdOld = persistentMcMove.getCurrentProcessId();
            CurrentProcess currentProcessIdNew = mcMove.getCurrentProcessId();
            Mc aftorMcOld = persistentMcMove.getAftorMc();
            Mc aftorMcNew = mcMove.getAftorMc();
            if (beforMcNew != null) {
                beforMcNew = em.getReference(beforMcNew.getClass(), beforMcNew.getId());
                mcMove.setBeforMc(beforMcNew);
            }
            if (currentProcessIdNew != null) {
                currentProcessIdNew = em.getReference(currentProcessIdNew.getClass(), currentProcessIdNew.getId());
                mcMove.setCurrentProcessId(currentProcessIdNew);
            }
            if (aftorMcNew != null) {
                aftorMcNew = em.getReference(aftorMcNew.getClass(), aftorMcNew.getId());
                mcMove.setAftorMc(aftorMcNew);
            }
            mcMove = em.merge(mcMove);
            if (beforMcOld != null && !beforMcOld.equals(beforMcNew)) {
                beforMcOld.getMcMoveCollection().remove(mcMove);
                beforMcOld = em.merge(beforMcOld);
            }
            if (beforMcNew != null && !beforMcNew.equals(beforMcOld)) {
                beforMcNew.getMcMoveCollection().add(mcMove);
                beforMcNew = em.merge(beforMcNew);
            }
            if (currentProcessIdOld != null && !currentProcessIdOld.equals(currentProcessIdNew)) {
                currentProcessIdOld.getMcMoveCollection().remove(mcMove);
                currentProcessIdOld = em.merge(currentProcessIdOld);
            }
            if (currentProcessIdNew != null && !currentProcessIdNew.equals(currentProcessIdOld)) {
                currentProcessIdNew.getMcMoveCollection().add(mcMove);
                currentProcessIdNew = em.merge(currentProcessIdNew);
            }
            if (aftorMcOld != null && !aftorMcOld.equals(aftorMcNew)) {
                aftorMcOld.getMcMoveCollection().remove(mcMove);
                aftorMcOld = em.merge(aftorMcOld);
            }
            if (aftorMcNew != null && !aftorMcNew.equals(aftorMcOld)) {
                aftorMcNew.getMcMoveCollection().add(mcMove);
                aftorMcNew = em.merge(aftorMcNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = mcMove.getId();
                if (findMcMove(id) == null) {
                    throw new NonexistentEntityException("The mcMove with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            McMove mcMove;
            try {
                mcMove = em.getReference(McMove.class, id);
                mcMove.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The mcMove with id " + id + " no longer exists.", enfe);
            }
            Mc beforMc = mcMove.getBeforMc();
            if (beforMc != null) {
                beforMc.getMcMoveCollection().remove(mcMove);
                beforMc = em.merge(beforMc);
            }
            CurrentProcess currentProcessId = mcMove.getCurrentProcessId();
            if (currentProcessId != null) {
                currentProcessId.getMcMoveCollection().remove(mcMove);
                currentProcessId = em.merge(currentProcessId);
            }
            Mc aftorMc = mcMove.getAftorMc();
            if (aftorMc != null) {
                aftorMc.getMcMoveCollection().remove(mcMove);
                aftorMc = em.merge(aftorMc);
            }
            em.remove(mcMove);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<McMove> findMcMoveEntities() {
        return findMcMoveEntities(true, -1, -1);
    }

    public List<McMove> findMcMoveEntities(int maxResults, int firstResult) {
        return findMcMoveEntities(false, maxResults, firstResult);
    }

    private List<McMove> findMcMoveEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(McMove.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public McMove findMcMove(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(McMove.class, id);
        } finally {
            em.close();
        }
    }

    public int getMcMoveCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<McMove> rt = cq.from(McMove.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    //@pang
    public McMove  fingCheckMcMove(CurrentProcess idCurrent)
    {
        EntityManager em = getEntityManager();
        try
        {
            Query q  = em.createQuery("SELECT m FROM McMove m WHERE m.CurrentProcessId =:idCurrent");
            return (McMove) q.getSingleResult();
        }
        catch(Exception e)
        {
            return null;
        }
        finally
        {
            em.close();
        }
                
    
    }
}
