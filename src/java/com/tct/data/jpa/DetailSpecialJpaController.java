/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.DetailFieldLeft;
import java.util.ArrayList;
import java.util.Collection;
import com.tct.data.DetailFieldRight;
import com.tct.data.DetailSpecial;
import com.tct.data.jpa.exceptions.IllegalOrphanException;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 19, 2012, Time : 3:00:28 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class DetailSpecialJpaController implements Serializable {

    public DetailSpecialJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DetailSpecial detailSpecial) throws PreexistingEntityException, Exception {
        if (detailSpecial.getDetailFieldLeftCollection() == null) {
            detailSpecial.setDetailFieldLeftCollection(new ArrayList<DetailFieldLeft>());
        }
        if (detailSpecial.getDetailFieldRightCollection() == null) {
            detailSpecial.setDetailFieldRightCollection(new ArrayList<DetailFieldRight>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<DetailFieldLeft> attachedDetailFieldLeftCollection = new ArrayList<DetailFieldLeft>();
            for (DetailFieldLeft detailFieldLeftCollectionDetailFieldLeftToAttach : detailSpecial.getDetailFieldLeftCollection()) {
                detailFieldLeftCollectionDetailFieldLeftToAttach = em.getReference(detailFieldLeftCollectionDetailFieldLeftToAttach.getClass(), detailFieldLeftCollectionDetailFieldLeftToAttach.getId());
                attachedDetailFieldLeftCollection.add(detailFieldLeftCollectionDetailFieldLeftToAttach);
            }
            detailSpecial.setDetailFieldLeftCollection(attachedDetailFieldLeftCollection);
            Collection<DetailFieldRight> attachedDetailFieldRightCollection = new ArrayList<DetailFieldRight>();
            for (DetailFieldRight detailFieldRightCollectionDetailFieldRightToAttach : detailSpecial.getDetailFieldRightCollection()) {
                detailFieldRightCollectionDetailFieldRightToAttach = em.getReference(detailFieldRightCollectionDetailFieldRightToAttach.getClass(), detailFieldRightCollectionDetailFieldRightToAttach.getId());
                attachedDetailFieldRightCollection.add(detailFieldRightCollectionDetailFieldRightToAttach);
            }
            detailSpecial.setDetailFieldRightCollection(attachedDetailFieldRightCollection);
            em.persist(detailSpecial);
            for (DetailFieldLeft detailFieldLeftCollectionDetailFieldLeft : detailSpecial.getDetailFieldLeftCollection()) {
                DetailSpecial oldIdDetailSpecailOfDetailFieldLeftCollectionDetailFieldLeft = detailFieldLeftCollectionDetailFieldLeft.getIdDetailSpecail();
                detailFieldLeftCollectionDetailFieldLeft.setIdDetailSpecail(detailSpecial);
                detailFieldLeftCollectionDetailFieldLeft = em.merge(detailFieldLeftCollectionDetailFieldLeft);
                if (oldIdDetailSpecailOfDetailFieldLeftCollectionDetailFieldLeft != null) {
                    oldIdDetailSpecailOfDetailFieldLeftCollectionDetailFieldLeft.getDetailFieldLeftCollection().remove(detailFieldLeftCollectionDetailFieldLeft);
                    oldIdDetailSpecailOfDetailFieldLeftCollectionDetailFieldLeft = em.merge(oldIdDetailSpecailOfDetailFieldLeftCollectionDetailFieldLeft);
                }
            }
            for (DetailFieldRight detailFieldRightCollectionDetailFieldRight : detailSpecial.getDetailFieldRightCollection()) {
                DetailSpecial oldIdDetailSpecailOfDetailFieldRightCollectionDetailFieldRight = detailFieldRightCollectionDetailFieldRight.getIdDetailSpecail();
                detailFieldRightCollectionDetailFieldRight.setIdDetailSpecail(detailSpecial);
                detailFieldRightCollectionDetailFieldRight = em.merge(detailFieldRightCollectionDetailFieldRight);
                if (oldIdDetailSpecailOfDetailFieldRightCollectionDetailFieldRight != null) {
                    oldIdDetailSpecailOfDetailFieldRightCollectionDetailFieldRight.getDetailFieldRightCollection().remove(detailFieldRightCollectionDetailFieldRight);
                    oldIdDetailSpecailOfDetailFieldRightCollectionDetailFieldRight = em.merge(oldIdDetailSpecailOfDetailFieldRightCollectionDetailFieldRight);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDetailSpecial(detailSpecial.getId()) != null) {
                throw new PreexistingEntityException("DetailSpecial " + detailSpecial + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DetailSpecial detailSpecial) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailSpecial persistentDetailSpecial = em.find(DetailSpecial.class, detailSpecial.getId());
            Collection<DetailFieldLeft> detailFieldLeftCollectionOld = persistentDetailSpecial.getDetailFieldLeftCollection();
            Collection<DetailFieldLeft> detailFieldLeftCollectionNew = detailSpecial.getDetailFieldLeftCollection();
            Collection<DetailFieldRight> detailFieldRightCollectionOld = persistentDetailSpecial.getDetailFieldRightCollection();
            Collection<DetailFieldRight> detailFieldRightCollectionNew = detailSpecial.getDetailFieldRightCollection();
            List<String> illegalOrphanMessages = null;
            for (DetailFieldLeft detailFieldLeftCollectionOldDetailFieldLeft : detailFieldLeftCollectionOld) {
                if (!detailFieldLeftCollectionNew.contains(detailFieldLeftCollectionOldDetailFieldLeft)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain DetailFieldLeft " + detailFieldLeftCollectionOldDetailFieldLeft + " since its idDetailSpecail field is not nullable.");
                }
            }
            for (DetailFieldRight detailFieldRightCollectionOldDetailFieldRight : detailFieldRightCollectionOld) {
                if (!detailFieldRightCollectionNew.contains(detailFieldRightCollectionOldDetailFieldRight)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain DetailFieldRight " + detailFieldRightCollectionOldDetailFieldRight + " since its idDetailSpecail field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<DetailFieldLeft> attachedDetailFieldLeftCollectionNew = new ArrayList<DetailFieldLeft>();
            for (DetailFieldLeft detailFieldLeftCollectionNewDetailFieldLeftToAttach : detailFieldLeftCollectionNew) {
                detailFieldLeftCollectionNewDetailFieldLeftToAttach = em.getReference(detailFieldLeftCollectionNewDetailFieldLeftToAttach.getClass(), detailFieldLeftCollectionNewDetailFieldLeftToAttach.getId());
                attachedDetailFieldLeftCollectionNew.add(detailFieldLeftCollectionNewDetailFieldLeftToAttach);
            }
            detailFieldLeftCollectionNew = attachedDetailFieldLeftCollectionNew;
            detailSpecial.setDetailFieldLeftCollection(detailFieldLeftCollectionNew);
            Collection<DetailFieldRight> attachedDetailFieldRightCollectionNew = new ArrayList<DetailFieldRight>();
            for (DetailFieldRight detailFieldRightCollectionNewDetailFieldRightToAttach : detailFieldRightCollectionNew) {
                detailFieldRightCollectionNewDetailFieldRightToAttach = em.getReference(detailFieldRightCollectionNewDetailFieldRightToAttach.getClass(), detailFieldRightCollectionNewDetailFieldRightToAttach.getId());
                attachedDetailFieldRightCollectionNew.add(detailFieldRightCollectionNewDetailFieldRightToAttach);
            }
            detailFieldRightCollectionNew = attachedDetailFieldRightCollectionNew;
            detailSpecial.setDetailFieldRightCollection(detailFieldRightCollectionNew);
            detailSpecial = em.merge(detailSpecial);
            for (DetailFieldLeft detailFieldLeftCollectionNewDetailFieldLeft : detailFieldLeftCollectionNew) {
                if (!detailFieldLeftCollectionOld.contains(detailFieldLeftCollectionNewDetailFieldLeft)) {
                    DetailSpecial oldIdDetailSpecailOfDetailFieldLeftCollectionNewDetailFieldLeft = detailFieldLeftCollectionNewDetailFieldLeft.getIdDetailSpecail();
                    detailFieldLeftCollectionNewDetailFieldLeft.setIdDetailSpecail(detailSpecial);
                    detailFieldLeftCollectionNewDetailFieldLeft = em.merge(detailFieldLeftCollectionNewDetailFieldLeft);
                    if (oldIdDetailSpecailOfDetailFieldLeftCollectionNewDetailFieldLeft != null && !oldIdDetailSpecailOfDetailFieldLeftCollectionNewDetailFieldLeft.equals(detailSpecial)) {
                        oldIdDetailSpecailOfDetailFieldLeftCollectionNewDetailFieldLeft.getDetailFieldLeftCollection().remove(detailFieldLeftCollectionNewDetailFieldLeft);
                        oldIdDetailSpecailOfDetailFieldLeftCollectionNewDetailFieldLeft = em.merge(oldIdDetailSpecailOfDetailFieldLeftCollectionNewDetailFieldLeft);
                    }
                }
            }
            for (DetailFieldRight detailFieldRightCollectionNewDetailFieldRight : detailFieldRightCollectionNew) {
                if (!detailFieldRightCollectionOld.contains(detailFieldRightCollectionNewDetailFieldRight)) {
                    DetailSpecial oldIdDetailSpecailOfDetailFieldRightCollectionNewDetailFieldRight = detailFieldRightCollectionNewDetailFieldRight.getIdDetailSpecail();
                    detailFieldRightCollectionNewDetailFieldRight.setIdDetailSpecail(detailSpecial);
                    detailFieldRightCollectionNewDetailFieldRight = em.merge(detailFieldRightCollectionNewDetailFieldRight);
                    if (oldIdDetailSpecailOfDetailFieldRightCollectionNewDetailFieldRight != null && !oldIdDetailSpecailOfDetailFieldRightCollectionNewDetailFieldRight.equals(detailSpecial)) {
                        oldIdDetailSpecailOfDetailFieldRightCollectionNewDetailFieldRight.getDetailFieldRightCollection().remove(detailFieldRightCollectionNewDetailFieldRight);
                        oldIdDetailSpecailOfDetailFieldRightCollectionNewDetailFieldRight = em.merge(oldIdDetailSpecailOfDetailFieldRightCollectionNewDetailFieldRight);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = detailSpecial.getId();
                if (findDetailSpecial(id) == null) {
                    throw new NonexistentEntityException("The detailSpecial with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailSpecial detailSpecial;
            try {
                detailSpecial = em.getReference(DetailSpecial.class, id);
                detailSpecial.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The detailSpecial with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<DetailFieldLeft> detailFieldLeftCollectionOrphanCheck = detailSpecial.getDetailFieldLeftCollection();
            for (DetailFieldLeft detailFieldLeftCollectionOrphanCheckDetailFieldLeft : detailFieldLeftCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This DetailSpecial (" + detailSpecial + ") cannot be destroyed since the DetailFieldLeft " + detailFieldLeftCollectionOrphanCheckDetailFieldLeft + " in its detailFieldLeftCollection field has a non-nullable idDetailSpecail field.");
            }
            Collection<DetailFieldRight> detailFieldRightCollectionOrphanCheck = detailSpecial.getDetailFieldRightCollection();
            for (DetailFieldRight detailFieldRightCollectionOrphanCheckDetailFieldRight : detailFieldRightCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This DetailSpecial (" + detailSpecial + ") cannot be destroyed since the DetailFieldRight " + detailFieldRightCollectionOrphanCheckDetailFieldRight + " in its detailFieldRightCollection field has a non-nullable idDetailSpecail field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(detailSpecial);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DetailSpecial> findDetailSpecialEntities() {
        return findDetailSpecialEntities(true, -1, -1);
    }

    public List<DetailSpecial> findDetailSpecialEntities(int maxResults, int firstResult) {
        return findDetailSpecialEntities(false, maxResults, firstResult);
    }

    private List<DetailSpecial> findDetailSpecialEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(DetailSpecial.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DetailSpecial findDetailSpecial(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DetailSpecial.class, id);
        } finally {
            em.close();
        }
    }

    public int getDetailSpecialCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<DetailSpecial> rt = cq.from(DetailSpecial.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    public List<DetailSpecial> findByNameNotDelete(String name)
    {
        EntityManager em  = getEntityManager();
        try
        {
            Query q = em.createQuery("SELECT d FROM DetailSpecial d WHERE ( d.name LIKE :name) and (d.deleted=0)").setParameter("name", name+"%");
            return  q.getResultList();
        }
        finally{
            em.close();
        }
    }
     public List<DetailSpecial> findAlltNotDelete()
    {
        EntityManager em  = getEntityManager();
        try
        {
            Query q = em.createQuery("SELECT d FROM DetailSpecial d WHERE  d.deleted=0 ORDER BY d.name asc");
            return  q.getResultList();
        }
        finally{
            em.close();
        }
    }
     public  DetailSpecial  findById(String idDel)
    {
        EntityManager em  = getEntityManager();
        try
        {
            Query q = em.createQuery("SELECT d FROM DetailSpecial d WHERE  (d.id=:id)and (d.deleted=0)").setParameter("id", idDel);
            return  (DetailSpecial)q.getSingleResult();
        }
        finally{
            em.close();
        }
    }
//    find by name detail special 
     public DetailSpecial searchDetailSpecail(String nameDetailSpecial)
     {
         EntityManager em = getEntityManager();
         try
         {
             Query q  = em.createQuery("SELECT d  FROM DetaiSpecial d WHERE d.name =:nameDetailSpecial and (d.deleted=0)").setParameter("nameDetailSpecial",nameDetailSpecial);
             return (DetailSpecial) q.getSingleResult();
         }
         finally
         {
             em.close();
         }
     }
}
