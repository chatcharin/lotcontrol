/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data.jpa;

import com.appCinfigpage.bean.fc;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.FieldName;
import com.tct.data.GroupField;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 17, 2012, Time : 6:56:28 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class GroupFieldJpaController implements Serializable {

    public GroupFieldJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(GroupField groupField) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            FieldName idFieldName = groupField.getIdFieldName();
            if (idFieldName != null) {
                idFieldName = em.getReference(idFieldName.getClass(), idFieldName.getId());
                groupField.setIdFieldName(idFieldName);
            }
            em.persist(groupField);
            if (idFieldName != null) {
                idFieldName.getGroupFieldCollection().add(groupField);
                idFieldName = em.merge(idFieldName);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findGroupField(groupField.getId()) != null) {
                throw new PreexistingEntityException("GroupField " + groupField + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(GroupField groupField) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            GroupField persistentGroupField = em.find(GroupField.class, groupField.getId());
            FieldName idFieldNameOld = persistentGroupField.getIdFieldName();
            FieldName idFieldNameNew = groupField.getIdFieldName();
            if (idFieldNameNew != null) {
                idFieldNameNew = em.getReference(idFieldNameNew.getClass(), idFieldNameNew.getId());
                groupField.setIdFieldName(idFieldNameNew);
            }
            groupField = em.merge(groupField);
            if (idFieldNameOld != null && !idFieldNameOld.equals(idFieldNameNew)) {
                idFieldNameOld.getGroupFieldCollection().remove(groupField);
                idFieldNameOld = em.merge(idFieldNameOld);
            }
            if (idFieldNameNew != null && !idFieldNameNew.equals(idFieldNameOld)) {
                idFieldNameNew.getGroupFieldCollection().add(groupField);
                idFieldNameNew = em.merge(idFieldNameNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = groupField.getId();
                if (findGroupField(id) == null) {
                    throw new NonexistentEntityException("The groupField with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            GroupField groupField;
            try {
                groupField = em.getReference(GroupField.class, id);
                groupField.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The groupField with id " + id + " no longer exists.", enfe);
            }
            FieldName idFieldName = groupField.getIdFieldName();
            if (idFieldName != null) {
                idFieldName.getGroupFieldCollection().remove(groupField);
                idFieldName = em.merge(idFieldName);
            }
            em.remove(groupField);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<GroupField> findGroupFieldEntities() {
        return findGroupFieldEntities(true, -1, -1);
    }

    public List<GroupField> findGroupFieldEntities(int maxResults, int firstResult) {
        return findGroupFieldEntities(false, maxResults, firstResult);
    }

    private List<GroupField> findGroupFieldEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(GroupField.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public GroupField findGroupField(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(GroupField.class, id);
        } 
        catch(Exception e)
        {
            fc.print("======== Null =========");
            return  null;
        }
        finally {
            em.close();
        }
    }

    public int getGroupFieldCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<GroupField> rt = cq.from(GroupField.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    public List<GroupField> findIdFieldName(FieldName idFieldName)
    {
        EntityManager em  = getEntityManager();
        try
        {
            Query q = em.createNamedQuery("GroupField.findByIdFieldName").setParameter("idFieldName", idFieldName);
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }
     public GroupField findIdFieldGroubId(String idGroup)
    {
        EntityManager em  = getEntityManager();
        try
        {
            Query q = em.createNamedQuery("GroupField.findById").setParameter("id", idGroup);
            return (GroupField)q.getSingleResult();
        }
        catch(Exception e)
        {
            return null;
        }
        finally
        {
            em.close();;
        }
    }
}
