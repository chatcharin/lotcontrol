/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data.jpa;

import com.tct.data.Line;
import com.tct.data.ManualTarget;
import com.tct.data.NextModel;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.Target;
import com.tct.data.jpa.exceptions.IllegalOrphanException;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 22, 2012, Time : 11:14:33 AM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class LineJpaController implements Serializable {

    public LineJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Line line) throws PreexistingEntityException, Exception {
        if (line.getManualTargetCollection() == null) {
            line.setManualTargetCollection(new ArrayList<ManualTarget>());
        }
        if (line.getNextModelCollection() == null) {
            line.setNextModelCollection(new ArrayList<NextModel>());
        }
        if (line.getTargetCollection() == null) {
            line.setTargetCollection(new ArrayList<Target>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<ManualTarget> attachedManualTargetCollection = new ArrayList<ManualTarget>();
            for (ManualTarget manualTargetCollectionManualTargetToAttach : line.getManualTargetCollection()) {
                manualTargetCollectionManualTargetToAttach = em.getReference(manualTargetCollectionManualTargetToAttach.getClass(), manualTargetCollectionManualTargetToAttach.getIdManaulTarget());
                attachedManualTargetCollection.add(manualTargetCollectionManualTargetToAttach);
            }
            line.setManualTargetCollection(attachedManualTargetCollection);
            Collection<NextModel> attachedNextModelCollection = new ArrayList<NextModel>();
            for (NextModel nextModelCollectionNextModelToAttach : line.getNextModelCollection()) {
                nextModelCollectionNextModelToAttach = em.getReference(nextModelCollectionNextModelToAttach.getClass(), nextModelCollectionNextModelToAttach.getIdNextModel());
                attachedNextModelCollection.add(nextModelCollectionNextModelToAttach);
            }
            line.setNextModelCollection(attachedNextModelCollection);
            Collection<Target> attachedTargetCollection = new ArrayList<Target>();
            for (Target targetCollectionTargetToAttach : line.getTargetCollection()) {
                targetCollectionTargetToAttach = em.getReference(targetCollectionTargetToAttach.getClass(), targetCollectionTargetToAttach.getId());
                attachedTargetCollection.add(targetCollectionTargetToAttach);
            }
            line.setTargetCollection(attachedTargetCollection);
            em.persist(line);
            for (ManualTarget manualTargetCollectionManualTarget : line.getManualTargetCollection()) {
                Line oldIdLineOfManualTargetCollectionManualTarget = manualTargetCollectionManualTarget.getIdLine();
                manualTargetCollectionManualTarget.setIdLine(line);
                manualTargetCollectionManualTarget = em.merge(manualTargetCollectionManualTarget);
                if (oldIdLineOfManualTargetCollectionManualTarget != null) {
                    oldIdLineOfManualTargetCollectionManualTarget.getManualTargetCollection().remove(manualTargetCollectionManualTarget);
                    oldIdLineOfManualTargetCollectionManualTarget = em.merge(oldIdLineOfManualTargetCollectionManualTarget);
                }
            }
            for (NextModel nextModelCollectionNextModel : line.getNextModelCollection()) {
                Line oldIdLineOfNextModelCollectionNextModel = nextModelCollectionNextModel.getIdLine();
                nextModelCollectionNextModel.setIdLine(line);
                nextModelCollectionNextModel = em.merge(nextModelCollectionNextModel);
                if (oldIdLineOfNextModelCollectionNextModel != null) {
                    oldIdLineOfNextModelCollectionNextModel.getNextModelCollection().remove(nextModelCollectionNextModel);
                    oldIdLineOfNextModelCollectionNextModel = em.merge(oldIdLineOfNextModelCollectionNextModel);
                }
            }
            for (Target targetCollectionTarget : line.getTargetCollection()) {
                Line oldIdLineOfTargetCollectionTarget = targetCollectionTarget.getIdLine();
                targetCollectionTarget.setIdLine(line);
                targetCollectionTarget = em.merge(targetCollectionTarget);
                if (oldIdLineOfTargetCollectionTarget != null) {
                    oldIdLineOfTargetCollectionTarget.getTargetCollection().remove(targetCollectionTarget);
                    oldIdLineOfTargetCollectionTarget = em.merge(oldIdLineOfTargetCollectionTarget);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findLine(line.getLineId()) != null) {
                throw new PreexistingEntityException("Line " + line + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Line line) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Line persistentLine = em.find(Line.class, line.getLineId());
            Collection<ManualTarget> manualTargetCollectionOld = persistentLine.getManualTargetCollection();
            Collection<ManualTarget> manualTargetCollectionNew = line.getManualTargetCollection();
            Collection<NextModel> nextModelCollectionOld = persistentLine.getNextModelCollection();
            Collection<NextModel> nextModelCollectionNew = line.getNextModelCollection();
            Collection<Target> targetCollectionOld = persistentLine.getTargetCollection();
            Collection<Target> targetCollectionNew = line.getTargetCollection();
            List<String> illegalOrphanMessages = null;
            for (ManualTarget manualTargetCollectionOldManualTarget : manualTargetCollectionOld) {
                if (!manualTargetCollectionNew.contains(manualTargetCollectionOldManualTarget)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ManualTarget " + manualTargetCollectionOldManualTarget + " since its idLine field is not nullable.");
                }
            }
            for (NextModel nextModelCollectionOldNextModel : nextModelCollectionOld) {
                if (!nextModelCollectionNew.contains(nextModelCollectionOldNextModel)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain NextModel " + nextModelCollectionOldNextModel + " since its idLine field is not nullable.");
                }
            }
            for (Target targetCollectionOldTarget : targetCollectionOld) {
                if (!targetCollectionNew.contains(targetCollectionOldTarget)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Target " + targetCollectionOldTarget + " since its idLine field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<ManualTarget> attachedManualTargetCollectionNew = new ArrayList<ManualTarget>();
            for (ManualTarget manualTargetCollectionNewManualTargetToAttach : manualTargetCollectionNew) {
                manualTargetCollectionNewManualTargetToAttach = em.getReference(manualTargetCollectionNewManualTargetToAttach.getClass(), manualTargetCollectionNewManualTargetToAttach.getIdManaulTarget());
                attachedManualTargetCollectionNew.add(manualTargetCollectionNewManualTargetToAttach);
            }
            manualTargetCollectionNew = attachedManualTargetCollectionNew;
            line.setManualTargetCollection(manualTargetCollectionNew);
            Collection<NextModel> attachedNextModelCollectionNew = new ArrayList<NextModel>();
            for (NextModel nextModelCollectionNewNextModelToAttach : nextModelCollectionNew) {
                nextModelCollectionNewNextModelToAttach = em.getReference(nextModelCollectionNewNextModelToAttach.getClass(), nextModelCollectionNewNextModelToAttach.getIdNextModel());
                attachedNextModelCollectionNew.add(nextModelCollectionNewNextModelToAttach);
            }
            nextModelCollectionNew = attachedNextModelCollectionNew;
            line.setNextModelCollection(nextModelCollectionNew);
            Collection<Target> attachedTargetCollectionNew = new ArrayList<Target>();
            for (Target targetCollectionNewTargetToAttach : targetCollectionNew) {
                targetCollectionNewTargetToAttach = em.getReference(targetCollectionNewTargetToAttach.getClass(), targetCollectionNewTargetToAttach.getId());
                attachedTargetCollectionNew.add(targetCollectionNewTargetToAttach);
            }
            targetCollectionNew = attachedTargetCollectionNew;
            line.setTargetCollection(targetCollectionNew);
            line = em.merge(line);
            for (ManualTarget manualTargetCollectionNewManualTarget : manualTargetCollectionNew) {
                if (!manualTargetCollectionOld.contains(manualTargetCollectionNewManualTarget)) {
                    Line oldIdLineOfManualTargetCollectionNewManualTarget = manualTargetCollectionNewManualTarget.getIdLine();
                    manualTargetCollectionNewManualTarget.setIdLine(line);
                    manualTargetCollectionNewManualTarget = em.merge(manualTargetCollectionNewManualTarget);
                    if (oldIdLineOfManualTargetCollectionNewManualTarget != null && !oldIdLineOfManualTargetCollectionNewManualTarget.equals(line)) {
                        oldIdLineOfManualTargetCollectionNewManualTarget.getManualTargetCollection().remove(manualTargetCollectionNewManualTarget);
                        oldIdLineOfManualTargetCollectionNewManualTarget = em.merge(oldIdLineOfManualTargetCollectionNewManualTarget);
                    }
                }
            }
            for (NextModel nextModelCollectionNewNextModel : nextModelCollectionNew) {
                if (!nextModelCollectionOld.contains(nextModelCollectionNewNextModel)) {
                    Line oldIdLineOfNextModelCollectionNewNextModel = nextModelCollectionNewNextModel.getIdLine();
                    nextModelCollectionNewNextModel.setIdLine(line);
                    nextModelCollectionNewNextModel = em.merge(nextModelCollectionNewNextModel);
                    if (oldIdLineOfNextModelCollectionNewNextModel != null && !oldIdLineOfNextModelCollectionNewNextModel.equals(line)) {
                        oldIdLineOfNextModelCollectionNewNextModel.getNextModelCollection().remove(nextModelCollectionNewNextModel);
                        oldIdLineOfNextModelCollectionNewNextModel = em.merge(oldIdLineOfNextModelCollectionNewNextModel);
                    }
                }
            }
            for (Target targetCollectionNewTarget : targetCollectionNew) {
                if (!targetCollectionOld.contains(targetCollectionNewTarget)) {
                    Line oldIdLineOfTargetCollectionNewTarget = targetCollectionNewTarget.getIdLine();
                    targetCollectionNewTarget.setIdLine(line);
                    targetCollectionNewTarget = em.merge(targetCollectionNewTarget);
                    if (oldIdLineOfTargetCollectionNewTarget != null && !oldIdLineOfTargetCollectionNewTarget.equals(line)) {
                        oldIdLineOfTargetCollectionNewTarget.getTargetCollection().remove(targetCollectionNewTarget);
                        oldIdLineOfTargetCollectionNewTarget = em.merge(oldIdLineOfTargetCollectionNewTarget);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = line.getLineId();
                if (findLine(id) == null) {
                    throw new NonexistentEntityException("The line with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Line line;
            try {
                line = em.getReference(Line.class, id);
                line.getLineId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The line with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<ManualTarget> manualTargetCollectionOrphanCheck = line.getManualTargetCollection();
            for (ManualTarget manualTargetCollectionOrphanCheckManualTarget : manualTargetCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Line (" + line + ") cannot be destroyed since the ManualTarget " + manualTargetCollectionOrphanCheckManualTarget + " in its manualTargetCollection field has a non-nullable idLine field.");
            }
            Collection<NextModel> nextModelCollectionOrphanCheck = line.getNextModelCollection();
            for (NextModel nextModelCollectionOrphanCheckNextModel : nextModelCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Line (" + line + ") cannot be destroyed since the NextModel " + nextModelCollectionOrphanCheckNextModel + " in its nextModelCollection field has a non-nullable idLine field.");
            }
            Collection<Target> targetCollectionOrphanCheck = line.getTargetCollection();
            for (Target targetCollectionOrphanCheckTarget : targetCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Line (" + line + ") cannot be destroyed since the Target " + targetCollectionOrphanCheckTarget + " in its targetCollection field has a non-nullable idLine field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(line);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Line> findLineEntities() {
        return findLineEntities(true, -1, -1);
    }

    public List<Line> findLineEntities(int maxResults, int firstResult) {
        return findLineEntities(false, maxResults, firstResult);
    }

    private List<Line> findLineEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Line.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Line findLine(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Line.class, id);
        } finally {
            em.close();
        }
    }

    public int getLineCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Line> rt = cq.from(Line.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    public List<Line> findAllLineNotDeletd()
    {
        EntityManager em = getEntityManager();
        try
        {
            Query q  = em.createQuery("SELECT l FROM Line l WHERE l.deleted = 0 ORDER BY l.lineName ASC ");
            return q.getResultList();
        }
        finally
        {
            em.close();  
        }
    }

}
