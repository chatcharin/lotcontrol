/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data.jpa;

import com.tct.data.StdDataShow;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.StdDataShowInput;
import com.tct.data.jpa.exceptions.IllegalOrphanException;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Nov 2, 2012, Time : 1:45:59 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class StdDataShowJpaController implements Serializable {

    public StdDataShowJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(StdDataShow stdDataShow) throws PreexistingEntityException, Exception {
        if (stdDataShow.getStdDataShowInputCollection() == null) {
            stdDataShow.setStdDataShowInputCollection(new ArrayList<StdDataShowInput>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<StdDataShowInput> attachedStdDataShowInputCollection = new ArrayList<StdDataShowInput>();
            for (StdDataShowInput stdDataShowInputCollectionStdDataShowInputToAttach : stdDataShow.getStdDataShowInputCollection()) {
                stdDataShowInputCollectionStdDataShowInputToAttach = em.getReference(stdDataShowInputCollectionStdDataShowInputToAttach.getClass(), stdDataShowInputCollectionStdDataShowInputToAttach.getIdStdShowDataInput());
                attachedStdDataShowInputCollection.add(stdDataShowInputCollectionStdDataShowInputToAttach);
            }
            stdDataShow.setStdDataShowInputCollection(attachedStdDataShowInputCollection);
            em.persist(stdDataShow);
            for (StdDataShowInput stdDataShowInputCollectionStdDataShowInput : stdDataShow.getStdDataShowInputCollection()) {
                StdDataShow oldIdShowOfStdDataShowInputCollectionStdDataShowInput = stdDataShowInputCollectionStdDataShowInput.getIdShow();
                stdDataShowInputCollectionStdDataShowInput.setIdShow(stdDataShow);
                stdDataShowInputCollectionStdDataShowInput = em.merge(stdDataShowInputCollectionStdDataShowInput);
                if (oldIdShowOfStdDataShowInputCollectionStdDataShowInput != null) {
                    oldIdShowOfStdDataShowInputCollectionStdDataShowInput.getStdDataShowInputCollection().remove(stdDataShowInputCollectionStdDataShowInput);
                    oldIdShowOfStdDataShowInputCollectionStdDataShowInput = em.merge(oldIdShowOfStdDataShowInputCollectionStdDataShowInput);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findStdDataShow(stdDataShow.getIdStdShow()) != null) {
                throw new PreexistingEntityException("StdDataShow " + stdDataShow + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(StdDataShow stdDataShow) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StdDataShow persistentStdDataShow = em.find(StdDataShow.class, stdDataShow.getIdStdShow());
            Collection<StdDataShowInput> stdDataShowInputCollectionOld = persistentStdDataShow.getStdDataShowInputCollection();
            Collection<StdDataShowInput> stdDataShowInputCollectionNew = stdDataShow.getStdDataShowInputCollection();
            List<String> illegalOrphanMessages = null;
            for (StdDataShowInput stdDataShowInputCollectionOldStdDataShowInput : stdDataShowInputCollectionOld) {
                if (!stdDataShowInputCollectionNew.contains(stdDataShowInputCollectionOldStdDataShowInput)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain StdDataShowInput " + stdDataShowInputCollectionOldStdDataShowInput + " since its idShow field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<StdDataShowInput> attachedStdDataShowInputCollectionNew = new ArrayList<StdDataShowInput>();
            for (StdDataShowInput stdDataShowInputCollectionNewStdDataShowInputToAttach : stdDataShowInputCollectionNew) {
                stdDataShowInputCollectionNewStdDataShowInputToAttach = em.getReference(stdDataShowInputCollectionNewStdDataShowInputToAttach.getClass(), stdDataShowInputCollectionNewStdDataShowInputToAttach.getIdStdShowDataInput());
                attachedStdDataShowInputCollectionNew.add(stdDataShowInputCollectionNewStdDataShowInputToAttach);
            }
            stdDataShowInputCollectionNew = attachedStdDataShowInputCollectionNew;
            stdDataShow.setStdDataShowInputCollection(stdDataShowInputCollectionNew);
            stdDataShow = em.merge(stdDataShow);
            for (StdDataShowInput stdDataShowInputCollectionNewStdDataShowInput : stdDataShowInputCollectionNew) {
                if (!stdDataShowInputCollectionOld.contains(stdDataShowInputCollectionNewStdDataShowInput)) {
                    StdDataShow oldIdShowOfStdDataShowInputCollectionNewStdDataShowInput = stdDataShowInputCollectionNewStdDataShowInput.getIdShow();
                    stdDataShowInputCollectionNewStdDataShowInput.setIdShow(stdDataShow);
                    stdDataShowInputCollectionNewStdDataShowInput = em.merge(stdDataShowInputCollectionNewStdDataShowInput);
                    if (oldIdShowOfStdDataShowInputCollectionNewStdDataShowInput != null && !oldIdShowOfStdDataShowInputCollectionNewStdDataShowInput.equals(stdDataShow)) {
                        oldIdShowOfStdDataShowInputCollectionNewStdDataShowInput.getStdDataShowInputCollection().remove(stdDataShowInputCollectionNewStdDataShowInput);
                        oldIdShowOfStdDataShowInputCollectionNewStdDataShowInput = em.merge(oldIdShowOfStdDataShowInputCollectionNewStdDataShowInput);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = stdDataShow.getIdStdShow();
                if (findStdDataShow(id) == null) {
                    throw new NonexistentEntityException("The stdDataShow with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StdDataShow stdDataShow;
            try {
                stdDataShow = em.getReference(StdDataShow.class, id);
                stdDataShow.getIdStdShow();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The stdDataShow with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<StdDataShowInput> stdDataShowInputCollectionOrphanCheck = stdDataShow.getStdDataShowInputCollection();
            for (StdDataShowInput stdDataShowInputCollectionOrphanCheckStdDataShowInput : stdDataShowInputCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This StdDataShow (" + stdDataShow + ") cannot be destroyed since the StdDataShowInput " + stdDataShowInputCollectionOrphanCheckStdDataShowInput + " in its stdDataShowInputCollection field has a non-nullable idShow field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(stdDataShow);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<StdDataShow> findStdDataShowEntities() {
        return findStdDataShowEntities(true, -1, -1);
    }

    public List<StdDataShow> findStdDataShowEntities(int maxResults, int firstResult) {
        return findStdDataShowEntities(false, maxResults, firstResult);
    }

    private List<StdDataShow> findStdDataShowEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(StdDataShow.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public StdDataShow findStdDataShow(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(StdDataShow.class, id);
        } 
        catch(Exception e)
        {
            System.out.println(e);
            return  null;
        }
        finally {
            em.close();
        }
    }

    public int getStdDataShowCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<StdDataShow> rt = cq.from(StdDataShow.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    public StdDataShow  findCurrentProcessId(String idCurrentProc)
    {
        EntityManager em = getEntityManager();
        try
        {
            Query q = em.createQuery("SELECT s FROM StdDataShow s WHERE s.idCurrentProc =:idCurrentProc");
            q.setParameter("idCurrentProc",idCurrentProc);
            return  (StdDataShow) q.getSingleResult();
        }
        catch(Exception e)
        {  
            System.out.println("error find "+e);
            return null;
        }
        finally
        {
            em.close();
        }
    }
    
}
