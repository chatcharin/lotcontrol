/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import com.tct.data.CopperWireInvoice;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.Details;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author The_Boy_Cs
 */
public class CopperWireInvoiceJpaController implements Serializable {

    public CopperWireInvoiceJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CopperWireInvoice copperWireInvoice) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Details detailsIdDetail = copperWireInvoice.getDetailsIdDetail();
            if (detailsIdDetail != null) {
                detailsIdDetail = em.getReference(detailsIdDetail.getClass(), detailsIdDetail.getIdDetail());
                copperWireInvoice.setDetailsIdDetail(detailsIdDetail);
            }
            em.persist(copperWireInvoice);
            if (detailsIdDetail != null) {
                detailsIdDetail.getCopperWireInvoiceCollection().add(copperWireInvoice);
                detailsIdDetail = em.merge(detailsIdDetail);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCopperWireInvoice(copperWireInvoice.getId()) != null) {
                throw new PreexistingEntityException("CopperWireInvoice " + copperWireInvoice + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CopperWireInvoice copperWireInvoice) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CopperWireInvoice persistentCopperWireInvoice = em.find(CopperWireInvoice.class, copperWireInvoice.getId());
            Details detailsIdDetailOld = persistentCopperWireInvoice.getDetailsIdDetail();
            Details detailsIdDetailNew = copperWireInvoice.getDetailsIdDetail();
            if (detailsIdDetailNew != null) {
                detailsIdDetailNew = em.getReference(detailsIdDetailNew.getClass(), detailsIdDetailNew.getIdDetail());
                copperWireInvoice.setDetailsIdDetail(detailsIdDetailNew);
            }
            copperWireInvoice = em.merge(copperWireInvoice);
            if (detailsIdDetailOld != null && !detailsIdDetailOld.equals(detailsIdDetailNew)) {
                detailsIdDetailOld.getCopperWireInvoiceCollection().remove(copperWireInvoice);
                detailsIdDetailOld = em.merge(detailsIdDetailOld);
            }
            if (detailsIdDetailNew != null && !detailsIdDetailNew.equals(detailsIdDetailOld)) {
                detailsIdDetailNew.getCopperWireInvoiceCollection().add(copperWireInvoice);
                detailsIdDetailNew = em.merge(detailsIdDetailNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = copperWireInvoice.getId();
                if (findCopperWireInvoice(id) == null) {
                    throw new NonexistentEntityException("The copperWireInvoice with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CopperWireInvoice copperWireInvoice;
            try {
                copperWireInvoice = em.getReference(CopperWireInvoice.class, id);
                copperWireInvoice.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The copperWireInvoice with id " + id + " no longer exists.", enfe);
            }
            Details detailsIdDetail = copperWireInvoice.getDetailsIdDetail();
            if (detailsIdDetail != null) {
                detailsIdDetail.getCopperWireInvoiceCollection().remove(copperWireInvoice);
                detailsIdDetail = em.merge(detailsIdDetail);
            }
            em.remove(copperWireInvoice);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CopperWireInvoice> findCopperWireInvoiceEntities() {
        return findCopperWireInvoiceEntities(true, -1, -1);
    }

    public List<CopperWireInvoice> findCopperWireInvoiceEntities(int maxResults, int firstResult) {
        return findCopperWireInvoiceEntities(false, maxResults, firstResult);
    }

    private List<CopperWireInvoice> findCopperWireInvoiceEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CopperWireInvoice.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CopperWireInvoice findCopperWireInvoice(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CopperWireInvoice.class, id);
        } finally {
            em.close();
        }
    }

    public int getCopperWireInvoiceCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CopperWireInvoice> rt = cq.from(CopperWireInvoice.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<CopperWireInvoice> findCopperWireInvoicesByDetails(Details details){
        EntityManager em = getEntityManager();
        try{
            Query query = em.createNamedQuery("CopperWireInvoice.detailsIdDetail");
            query.setParameter("detailsIdDetail", details);
            return query.getResultList();
        }finally{
            em.close();
        }
    }
}
