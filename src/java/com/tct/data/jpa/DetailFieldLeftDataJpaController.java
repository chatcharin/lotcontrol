/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.DetailFieldLeft;
import com.tct.data.DetailFieldLeftData;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Oct 12, 2012, Time : 3:51:21 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class DetailFieldLeftDataJpaController implements Serializable {

    public DetailFieldLeftDataJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DetailFieldLeftData detailFieldLeftData) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailFieldLeft idDetailFieldLeft = detailFieldLeftData.getIdDetailFieldLeft();
            if (idDetailFieldLeft != null) {
                idDetailFieldLeft = em.getReference(idDetailFieldLeft.getClass(), idDetailFieldLeft.getId());
                detailFieldLeftData.setIdDetailFieldLeft(idDetailFieldLeft);
            }
            em.persist(detailFieldLeftData);
            if (idDetailFieldLeft != null) {
                idDetailFieldLeft.getDetailFieldLeftDataCollection().add(detailFieldLeftData);
                idDetailFieldLeft = em.merge(idDetailFieldLeft);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDetailFieldLeftData(detailFieldLeftData.getIdDetailSpecailData()) != null) {
                throw new PreexistingEntityException("DetailFieldLeftData " + detailFieldLeftData + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DetailFieldLeftData detailFieldLeftData) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailFieldLeftData persistentDetailFieldLeftData = em.find(DetailFieldLeftData.class, detailFieldLeftData.getIdDetailSpecailData());
            DetailFieldLeft idDetailFieldLeftOld = persistentDetailFieldLeftData.getIdDetailFieldLeft();
            DetailFieldLeft idDetailFieldLeftNew = detailFieldLeftData.getIdDetailFieldLeft();
            if (idDetailFieldLeftNew != null) {
                idDetailFieldLeftNew = em.getReference(idDetailFieldLeftNew.getClass(), idDetailFieldLeftNew.getId());
                detailFieldLeftData.setIdDetailFieldLeft(idDetailFieldLeftNew);
            }
            detailFieldLeftData = em.merge(detailFieldLeftData);
            if (idDetailFieldLeftOld != null && !idDetailFieldLeftOld.equals(idDetailFieldLeftNew)) {
                idDetailFieldLeftOld.getDetailFieldLeftDataCollection().remove(detailFieldLeftData);
                idDetailFieldLeftOld = em.merge(idDetailFieldLeftOld);
            }
            if (idDetailFieldLeftNew != null && !idDetailFieldLeftNew.equals(idDetailFieldLeftOld)) {
                idDetailFieldLeftNew.getDetailFieldLeftDataCollection().add(detailFieldLeftData);
                idDetailFieldLeftNew = em.merge(idDetailFieldLeftNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = detailFieldLeftData.getIdDetailSpecailData();
                if (findDetailFieldLeftData(id) == null) {
                    throw new NonexistentEntityException("The detailFieldLeftData with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailFieldLeftData detailFieldLeftData;
            try {
                detailFieldLeftData = em.getReference(DetailFieldLeftData.class, id);
                detailFieldLeftData.getIdDetailSpecailData();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The detailFieldLeftData with id " + id + " no longer exists.", enfe);
            }
            DetailFieldLeft idDetailFieldLeft = detailFieldLeftData.getIdDetailFieldLeft();
            if (idDetailFieldLeft != null) {
                idDetailFieldLeft.getDetailFieldLeftDataCollection().remove(detailFieldLeftData);
                idDetailFieldLeft = em.merge(idDetailFieldLeft);
            }
            em.remove(detailFieldLeftData);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DetailFieldLeftData> findDetailFieldLeftDataEntities() {
        return findDetailFieldLeftDataEntities(true, -1, -1);
    }

    public List<DetailFieldLeftData> findDetailFieldLeftDataEntities(int maxResults, int firstResult) {
        return findDetailFieldLeftDataEntities(false, maxResults, firstResult);
    }

    private List<DetailFieldLeftData> findDetailFieldLeftDataEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(DetailFieldLeftData.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DetailFieldLeftData findDetailFieldLeftData(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DetailFieldLeftData.class, id);
        } finally {
            em.close();
        }
    }

    public int getDetailFieldLeftDataCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<DetailFieldLeftData> rt = cq.from(DetailFieldLeftData.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
//    find detail field left
    public DetailFieldLeftData findDetailFieldLeftData(DetailFieldLeft idFieldLeft,String idDetails)
    {
        EntityManager em  = getEntityManager();
        try
        {
            Query q  = em.createQuery("SELECT d FROM  DetailFieldLeftData d   WHERE d.idDetails =:idDetails and d.idDetailFieldLeft =:idFieldLeft");
            q.setParameter("idFieldLeft", idFieldLeft);
            q.setParameter("idDetails",idDetails);
            return (DetailFieldLeftData) q.getSingleResult();
        }
        finally
        {
            em.close();
        }
    }
}
