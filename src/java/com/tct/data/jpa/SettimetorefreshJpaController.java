/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import com.tct.data.Settimetorefresh;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author TheBoyCs
 */
public class SettimetorefreshJpaController implements Serializable {

    public SettimetorefreshJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Settimetorefresh settimetorefresh) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(settimetorefresh);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findSettimetorefresh(settimetorefresh.getId()) != null) {
                throw new PreexistingEntityException("Settimetorefresh " + settimetorefresh + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Settimetorefresh settimetorefresh) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            settimetorefresh = em.merge(settimetorefresh);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = settimetorefresh.getId();
                if (findSettimetorefresh(id) == null) {
                    throw new NonexistentEntityException("The settimetorefresh with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Settimetorefresh settimetorefresh;
            try {
                settimetorefresh = em.getReference(Settimetorefresh.class, id);
                settimetorefresh.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The settimetorefresh with id " + id + " no longer exists.", enfe);
            }
            em.remove(settimetorefresh);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Settimetorefresh> findSettimetorefreshEntities() {
        return findSettimetorefreshEntities(true, -1, -1);
    }

    public List<Settimetorefresh> findSettimetorefreshEntities(int maxResults, int firstResult) {
        return findSettimetorefreshEntities(false, maxResults, firstResult);
    }

    private List<Settimetorefresh> findSettimetorefreshEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Settimetorefresh.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Settimetorefresh findSettimetorefresh(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Settimetorefresh.class, id);
        } finally {
            em.close();
        }
    }

    public int getSettimetorefreshCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Settimetorefresh> rt = cq.from(Settimetorefresh.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<Settimetorefresh> findSettimetorefreshAllAndFrist(){
        EntityManager em = getEntityManager();
        try{
            String sql = "SELECT s FROM Settimetorefresh s ORDER BY s.dateCreate DESC";
            Query query = em.createQuery(sql);
            return query.getResultList();
        }finally{
            em.close();
        }
    }
}
