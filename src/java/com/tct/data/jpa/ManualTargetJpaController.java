/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.Line;
import com.tct.data.ManualTarget;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * Machine User
 *
 * @author pang Development by Chusak laojang Date : Sep 25, 2012, Time :
 * 7:00:30 PM Copy Right 4 Xtreme Co.,Ltd.
 */
public class ManualTargetJpaController implements Serializable {

    public ManualTargetJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ManualTarget manualTarget) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Line idLine = manualTarget.getIdLine();
            if (idLine != null) {
                idLine = em.getReference(idLine.getClass(), idLine.getLineId());
                manualTarget.setIdLine(idLine);
            }
            em.persist(manualTarget);
            if (idLine != null) {
                idLine.getManualTargetCollection().add(manualTarget);
                idLine = em.merge(idLine);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findManualTarget(manualTarget.getIdManaulTarget()) != null) {
                throw new PreexistingEntityException("ManualTarget " + manualTarget + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ManualTarget manualTarget) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ManualTarget persistentManualTarget = em.find(ManualTarget.class, manualTarget.getIdManaulTarget());
            Line idLineOld = persistentManualTarget.getIdLine();
            Line idLineNew = manualTarget.getIdLine();
            if (idLineNew != null) {
                idLineNew = em.getReference(idLineNew.getClass(), idLineNew.getLineId());
                manualTarget.setIdLine(idLineNew);
            }
            manualTarget = em.merge(manualTarget);
            if (idLineOld != null && !idLineOld.equals(idLineNew)) {
                idLineOld.getManualTargetCollection().remove(manualTarget);
                idLineOld = em.merge(idLineOld);
            }
            if (idLineNew != null && !idLineNew.equals(idLineOld)) {
                idLineNew.getManualTargetCollection().add(manualTarget);
                idLineNew = em.merge(idLineNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = manualTarget.getIdManaulTarget();
                if (findManualTarget(id) == null) {
                    throw new NonexistentEntityException("The manualTarget with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ManualTarget manualTarget;
            try {
                manualTarget = em.getReference(ManualTarget.class, id);
                manualTarget.getIdManaulTarget();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The manualTarget with id " + id + " no longer exists.", enfe);
            }
            Line idLine = manualTarget.getIdLine();
            if (idLine != null) {
                idLine.getManualTargetCollection().remove(manualTarget);
                idLine = em.merge(idLine);
            }
            em.remove(manualTarget);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ManualTarget> findManualTargetEntities() {
        return findManualTargetEntities(true, -1, -1);
    }

    public List<ManualTarget> findManualTargetEntities(int maxResults, int firstResult) {
        return findManualTargetEntities(false, maxResults, firstResult);
    }

    private List<ManualTarget> findManualTargetEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ManualTarget.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ManualTarget findManualTarget(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ManualTarget.class, id);
        } finally {
            em.close();
        }
    }

    public int getManualTargetCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ManualTarget> rt = cq.from(ManualTarget.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<ManualTarget> findByLineAndNotDeleted(Line line) {
        EntityManager em = getEntityManager();
        try {
            String sql = "SELECT m FROM ManualTarget m WHERE m.idLine = :idLine AND m.deleted = 0";
//            String sql = "SELECT m FROM ManualTarget m WHERE m.idLine = :idLine AND m.deleted = 0";
            Query query = em.createQuery(sql);
            query.setParameter("idLine", line);
            return query.getResultList();
        } finally {
            em.close();
        }
    }
}
