/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.CurrentProcess;
import com.tct.data.NgRepair;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author The_Boy_Cs
 */
public class NgRepairJpaController implements Serializable {

    public NgRepairJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(NgRepair ngRepair) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CurrentProcess currentProcessId = ngRepair.getCurrentProcessId();
            if (currentProcessId != null) {
                currentProcessId = em.getReference(currentProcessId.getClass(), currentProcessId.getId());
                ngRepair.setCurrentProcessId(currentProcessId);
            }
            em.persist(ngRepair);
            if (currentProcessId != null) {
                currentProcessId.getNgRepairCollection().add(ngRepair);
                currentProcessId = em.merge(currentProcessId);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findNgRepair(ngRepair.getIdNg()) != null) {
                throw new PreexistingEntityException("NgRepair " + ngRepair + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(NgRepair ngRepair) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            NgRepair persistentNgRepair = em.find(NgRepair.class, ngRepair.getIdNg());
            CurrentProcess currentProcessIdOld = persistentNgRepair.getCurrentProcessId();
            CurrentProcess currentProcessIdNew = ngRepair.getCurrentProcessId();
            if (currentProcessIdNew != null) {
                currentProcessIdNew = em.getReference(currentProcessIdNew.getClass(), currentProcessIdNew.getId());
                ngRepair.setCurrentProcessId(currentProcessIdNew);
            }
            ngRepair = em.merge(ngRepair);
            if (currentProcessIdOld != null && !currentProcessIdOld.equals(currentProcessIdNew)) {
                currentProcessIdOld.getNgRepairCollection().remove(ngRepair);
                currentProcessIdOld = em.merge(currentProcessIdOld);
            }
            if (currentProcessIdNew != null && !currentProcessIdNew.equals(currentProcessIdOld)) {
                currentProcessIdNew.getNgRepairCollection().add(ngRepair);
                currentProcessIdNew = em.merge(currentProcessIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = ngRepair.getIdNg();
                if (findNgRepair(id) == null) {
                    throw new NonexistentEntityException("The ngRepair with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            NgRepair ngRepair;
            try {
                ngRepair = em.getReference(NgRepair.class, id);
                ngRepair.getIdNg();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ngRepair with id " + id + " no longer exists.", enfe);
            }
            CurrentProcess currentProcessId = ngRepair.getCurrentProcessId();
            if (currentProcessId != null) {
                currentProcessId.getNgRepairCollection().remove(ngRepair);
                currentProcessId = em.merge(currentProcessId);
            }
            em.remove(ngRepair);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<NgRepair> findNgRepairEntities() {
        return findNgRepairEntities(true, -1, -1);
    }

    public List<NgRepair> findNgRepairEntities(int maxResults, int firstResult) {
        return findNgRepairEntities(false, maxResults, firstResult);
    }

    private List<NgRepair> findNgRepairEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(NgRepair.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public NgRepair findNgRepair(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(NgRepair.class, id);
        } finally {
            em.close();
        }
    }

    public int getNgRepairCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<NgRepair> rt = cq.from(NgRepair.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    //==== findsum ng repair

    public List<NgRepair> findSumNgRepair(CurrentProcess idCurrentProcess) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("NgRepair.findByCurrentProcessId");
            q.setParameter("currentProcessId", idCurrentProcess);
            return q.getResultList();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        } finally {
            em.close();
        }
    }

    public List<NgRepair> findByName(String name) {
        EntityManager em = getEntityManager();
        try {
            String sql = "SELECT n FROM NgRepair n WHERE n.name LIKE :name";
            Query query = em.createQuery(sql);
            query.setParameter("name", name + "%");
            return query.getResultList();
        } finally {
            em.close();
        }
    }
}
