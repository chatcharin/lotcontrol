/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data.jpa;

import com.tct.data.StdTimeSetting;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;


/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Nov 1, 2012, Time : 2:16:18 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class StdTimeSettingJpaController implements Serializable {

    public StdTimeSettingJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }

    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(StdTimeSetting stdTimeSetting) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(stdTimeSetting);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findStdTimeSetting(stdTimeSetting.getIdStdSetting()) != null) {
                throw new PreexistingEntityException("StdTimeSetting " + stdTimeSetting + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(StdTimeSetting stdTimeSetting) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            stdTimeSetting = em.merge(stdTimeSetting);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = stdTimeSetting.getIdStdSetting();
                if (findStdTimeSetting(id) == null) {
                    throw new NonexistentEntityException("The stdTimeSetting with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StdTimeSetting stdTimeSetting;
            try {
                stdTimeSetting = em.getReference(StdTimeSetting.class, id);
                stdTimeSetting.getIdStdSetting();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The stdTimeSetting with id " + id + " no longer exists.", enfe);
            }
            em.remove(stdTimeSetting);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<StdTimeSetting> findStdTimeSettingEntities() {
        return findStdTimeSettingEntities(true, -1, -1);
    }

    public List<StdTimeSetting> findStdTimeSettingEntities(int maxResults, int firstResult) {
        return findStdTimeSettingEntities(false, maxResults, firstResult);
    }

    private List<StdTimeSetting> findStdTimeSettingEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(StdTimeSetting.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public StdTimeSetting findStdTimeSetting(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(StdTimeSetting.class, id);
        } finally {
            em.close();
        }
    }

    public int getStdTimeSettingCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<StdTimeSetting> rt = cq.from(StdTimeSetting.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    //**** find all
    public List<StdTimeSetting> findAllNotDelete()
    {
        EntityManager  em  = getEntityManager();
        try
        {
            Query q = em.createQuery("SELECT s FROM StdTimeSetting s WHERE s.deleted =0");
            return q.getResultList();
        }
        finally
        {
            em.close();;
        }
    }
    //Test 
    public StdTimeSetting findIdForEdit(String idStdSetting)
    {
        EntityManager  em  = getEntityManager();
        try
        {
            Query q  = em.createQuery("SELECT s FROM StdTimeSetting s  WHERE s.idStdSetting =:idStdSetting");
            q.setParameter("idStdSetting", idStdSetting);
            return (StdTimeSetting) q.getSingleResult();
        }
        catch(Exception e)
        {
            return  null;
        }
        finally
        {
        em.close();
        }
    
    }
    

}
