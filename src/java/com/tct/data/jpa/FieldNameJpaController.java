/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data.jpa;

import com.appCinfigpage.bean.fc;
import com.tct.data.FieldName;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.GroupField;
import com.tct.data.jpa.exceptions.IllegalOrphanException;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 17, 2012, Time : 6:56:28 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class FieldNameJpaController implements Serializable {

    public FieldNameJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(FieldName fieldName) throws PreexistingEntityException, Exception {
        if (fieldName.getGroupFieldCollection() == null) {
            fieldName.setGroupFieldCollection(new ArrayList<GroupField>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<GroupField> attachedGroupFieldCollection = new ArrayList<GroupField>();
            for (GroupField groupFieldCollectionGroupFieldToAttach : fieldName.getGroupFieldCollection()) {
                groupFieldCollectionGroupFieldToAttach = em.getReference(groupFieldCollectionGroupFieldToAttach.getClass(), groupFieldCollectionGroupFieldToAttach.getId());
                attachedGroupFieldCollection.add(groupFieldCollectionGroupFieldToAttach);
            }
            fieldName.setGroupFieldCollection(attachedGroupFieldCollection);
            em.persist(fieldName);
            for (GroupField groupFieldCollectionGroupField : fieldName.getGroupFieldCollection()) {
                FieldName oldIdFieldNameOfGroupFieldCollectionGroupField = groupFieldCollectionGroupField.getIdFieldName();
                groupFieldCollectionGroupField.setIdFieldName(fieldName);
                groupFieldCollectionGroupField = em.merge(groupFieldCollectionGroupField);
                if (oldIdFieldNameOfGroupFieldCollectionGroupField != null) {
                    oldIdFieldNameOfGroupFieldCollectionGroupField.getGroupFieldCollection().remove(groupFieldCollectionGroupField);
                    oldIdFieldNameOfGroupFieldCollectionGroupField = em.merge(oldIdFieldNameOfGroupFieldCollectionGroupField);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findFieldName(fieldName.getId()) != null) {
                throw new PreexistingEntityException("FieldName " + fieldName + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(FieldName fieldName) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            FieldName persistentFieldName = em.find(FieldName.class, fieldName.getId());
            Collection<GroupField> groupFieldCollectionOld = persistentFieldName.getGroupFieldCollection();
            Collection<GroupField> groupFieldCollectionNew = fieldName.getGroupFieldCollection();
            List<String> illegalOrphanMessages = null;
            for (GroupField groupFieldCollectionOldGroupField : groupFieldCollectionOld) {
                if (!groupFieldCollectionNew.contains(groupFieldCollectionOldGroupField)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain GroupField " + groupFieldCollectionOldGroupField + " since its idFieldName field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<GroupField> attachedGroupFieldCollectionNew = new ArrayList<GroupField>();
            for (GroupField groupFieldCollectionNewGroupFieldToAttach : groupFieldCollectionNew) {
                groupFieldCollectionNewGroupFieldToAttach = em.getReference(groupFieldCollectionNewGroupFieldToAttach.getClass(), groupFieldCollectionNewGroupFieldToAttach.getId());
                attachedGroupFieldCollectionNew.add(groupFieldCollectionNewGroupFieldToAttach);
            }
            groupFieldCollectionNew = attachedGroupFieldCollectionNew;
            fieldName.setGroupFieldCollection(groupFieldCollectionNew);
            fieldName = em.merge(fieldName);
            for (GroupField groupFieldCollectionNewGroupField : groupFieldCollectionNew) {
                if (!groupFieldCollectionOld.contains(groupFieldCollectionNewGroupField)) {
                    FieldName oldIdFieldNameOfGroupFieldCollectionNewGroupField = groupFieldCollectionNewGroupField.getIdFieldName();
                    groupFieldCollectionNewGroupField.setIdFieldName(fieldName);
                    groupFieldCollectionNewGroupField = em.merge(groupFieldCollectionNewGroupField);
                    if (oldIdFieldNameOfGroupFieldCollectionNewGroupField != null && !oldIdFieldNameOfGroupFieldCollectionNewGroupField.equals(fieldName)) {
                        oldIdFieldNameOfGroupFieldCollectionNewGroupField.getGroupFieldCollection().remove(groupFieldCollectionNewGroupField);
                        oldIdFieldNameOfGroupFieldCollectionNewGroupField = em.merge(oldIdFieldNameOfGroupFieldCollectionNewGroupField);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = fieldName.getId();
                if (findFieldName(id) == null) {
                    throw new NonexistentEntityException("The fieldName with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            FieldName fieldName;
            try {
                fieldName = em.getReference(FieldName.class, id);
                fieldName.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The fieldName with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<GroupField> groupFieldCollectionOrphanCheck = fieldName.getGroupFieldCollection();
            for (GroupField groupFieldCollectionOrphanCheckGroupField : groupFieldCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This FieldName (" + fieldName + ") cannot be destroyed since the GroupField " + groupFieldCollectionOrphanCheckGroupField + " in its groupFieldCollection field has a non-nullable idFieldName field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(fieldName);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<FieldName> findFieldNameEntities() {
        return findFieldNameEntities(true, -1, -1);
    }

    public List<FieldName> findFieldNameEntities(int maxResults, int firstResult) {
        return findFieldNameEntities(false, maxResults, firstResult);
    }

    private List<FieldName> findFieldNameEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(FieldName.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public FieldName findFieldName(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(FieldName.class, id);
        } finally {
            em.close();
        }
    }

    public int getFieldNameCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<FieldName> rt = cq.from(FieldName.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    } 
    public List<FieldName> findAllNotDeleted()
    {
        EntityManager  em  = getEntityManager();
        try
        {
            Query q  = em.createNamedQuery("FieldName.findAll");
            return q.getResultList();
        }
        finally{
            em.close();;
        }
    }
     public List<FieldName> findByType(String type)
    {
        EntityManager  em  = getEntityManager();
        try
        {
            Query q  = em.createNamedQuery("FieldName.findByType").setParameter("type",type);
            return q.getResultList();
        }
        finally{
            em.close();;
        }
    }
      public List<FieldName> findByFieldName(String name)
    {
        EntityManager  em  = getEntityManager();
        try
        {
            Query q  = em.createNamedQuery("FieldName.findByName").setParameter("name",name+"%");
            return q.getResultList();
        }
        finally{
            em.close();;
        }
    } 
    public List<FieldName>   findByAllFieldNotInAndNotDelete(List<String> fildName)
    {
       
        EntityManager em   = getEntityManager();
        try
        {
            Query q  =  em.createNamedQuery("FieldName.findByIdNotIn");
            q.setParameter("id", fildName);
            return  q.getResultList();
        }
        finally
        {
            em.close();
        }
    } 
    public FieldName  findByIdFieldName(String id)
    {
       System.out.println("id to find : "+id); 
       EntityManager em   = getEntityManager();
        try
        {
            Query q  =  em.createNamedQuery("FieldName.findById");
            q.setParameter("id", id);
            return  (FieldName) q.getSingleResult();
        }
        finally
        {
            em.close();
        } 
    
    }
}
