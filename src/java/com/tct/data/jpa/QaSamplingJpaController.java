/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.PcsQty;
import com.tct.data.QaSampling;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Oct 3, 2012, Time : 5:40:16 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class QaSamplingJpaController implements Serializable {

    public QaSamplingJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(QaSampling qaSampling) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PcsQty idPcsqty = qaSampling.getIdPcsqty();
            if (idPcsqty != null) {
                idPcsqty = em.getReference(idPcsqty.getClass(), idPcsqty.getId());
                qaSampling.setIdPcsqty(idPcsqty);
            }
            em.persist(qaSampling);
            if (idPcsqty != null) {
                idPcsqty.getQaSamplingCollection().add(qaSampling);
                idPcsqty = em.merge(idPcsqty);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findQaSampling(qaSampling.getIdSampling()) != null) {
                throw new PreexistingEntityException("QaSampling " + qaSampling + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(QaSampling qaSampling) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            QaSampling persistentQaSampling = em.find(QaSampling.class, qaSampling.getIdSampling());
            PcsQty idPcsqtyOld = persistentQaSampling.getIdPcsqty();
            PcsQty idPcsqtyNew = qaSampling.getIdPcsqty();
            if (idPcsqtyNew != null) {
                idPcsqtyNew = em.getReference(idPcsqtyNew.getClass(), idPcsqtyNew.getId());
                qaSampling.setIdPcsqty(idPcsqtyNew);
            }
            qaSampling = em.merge(qaSampling);
            if (idPcsqtyOld != null && !idPcsqtyOld.equals(idPcsqtyNew)) {
                idPcsqtyOld.getQaSamplingCollection().remove(qaSampling);
                idPcsqtyOld = em.merge(idPcsqtyOld);
            }
            if (idPcsqtyNew != null && !idPcsqtyNew.equals(idPcsqtyOld)) {
                idPcsqtyNew.getQaSamplingCollection().add(qaSampling);
                idPcsqtyNew = em.merge(idPcsqtyNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = qaSampling.getIdSampling();
                if (findQaSampling(id) == null) {
                    throw new NonexistentEntityException("The qaSampling with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            QaSampling qaSampling;
            try {
                qaSampling = em.getReference(QaSampling.class, id);
                qaSampling.getIdSampling();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The qaSampling with id " + id + " no longer exists.", enfe);
            }
            PcsQty idPcsqty = qaSampling.getIdPcsqty();
            if (idPcsqty != null) {
                idPcsqty.getQaSamplingCollection().remove(qaSampling);
                idPcsqty = em.merge(idPcsqty);
            }
            em.remove(qaSampling);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<QaSampling> findQaSamplingEntities() {
        return findQaSamplingEntities(true, -1, -1);
    }

    public List<QaSampling> findQaSamplingEntities(int maxResults, int firstResult) {
        return findQaSamplingEntities(false, maxResults, firstResult);
    }

    private List<QaSampling> findQaSamplingEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(QaSampling.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public QaSampling findQaSampling(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(QaSampling.class, id);
        } finally {
            em.close();
        }
    }

    public int getQaSamplingCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<QaSampling> rt = cq.from(QaSampling.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
