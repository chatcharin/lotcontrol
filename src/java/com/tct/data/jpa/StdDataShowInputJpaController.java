/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.StdDataShow;
import com.tct.data.StdDataShowInput;
import com.tct.data.StdTimeSetting;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Nov 2, 2012, Time : 1:45:59 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class StdDataShowInputJpaController implements Serializable {

    public StdDataShowInputJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(StdDataShowInput stdDataShowInput) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StdDataShow idShow = stdDataShowInput.getIdShow();
            if (idShow != null) {
                idShow = em.getReference(idShow.getClass(), idShow.getIdStdShow());
                stdDataShowInput.setIdShow(idShow);
            }
            StdTimeSetting idSettingName = stdDataShowInput.getIdSettingName();
            if (idSettingName != null) {
                idSettingName = em.getReference(idSettingName.getClass(), idSettingName.getIdStdSetting());
                stdDataShowInput.setIdSettingName(idSettingName);
            }
            em.persist(stdDataShowInput);
            if (idShow != null) {
                idShow.getStdDataShowInputCollection().add(stdDataShowInput);
                idShow = em.merge(idShow);
            }
            if (idSettingName != null) {
                idSettingName.getStdDataShowInputCollection().add(stdDataShowInput);
                idSettingName = em.merge(idSettingName);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findStdDataShowInput(stdDataShowInput.getIdStdShowDataInput()) != null) {
                throw new PreexistingEntityException("StdDataShowInput " + stdDataShowInput + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(StdDataShowInput stdDataShowInput) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StdDataShowInput persistentStdDataShowInput = em.find(StdDataShowInput.class, stdDataShowInput.getIdStdShowDataInput());
            StdDataShow idShowOld = persistentStdDataShowInput.getIdShow();
            StdDataShow idShowNew = stdDataShowInput.getIdShow();
            StdTimeSetting idSettingNameOld = persistentStdDataShowInput.getIdSettingName();
            StdTimeSetting idSettingNameNew = stdDataShowInput.getIdSettingName();
            if (idShowNew != null) {
                idShowNew = em.getReference(idShowNew.getClass(), idShowNew.getIdStdShow());
                stdDataShowInput.setIdShow(idShowNew);
            }
            if (idSettingNameNew != null) {
                idSettingNameNew = em.getReference(idSettingNameNew.getClass(), idSettingNameNew.getIdStdSetting());
                stdDataShowInput.setIdSettingName(idSettingNameNew);
            }
            stdDataShowInput = em.merge(stdDataShowInput);
            if (idShowOld != null && !idShowOld.equals(idShowNew)) {
                idShowOld.getStdDataShowInputCollection().remove(stdDataShowInput);
                idShowOld = em.merge(idShowOld);
            }
            if (idShowNew != null && !idShowNew.equals(idShowOld)) {
                idShowNew.getStdDataShowInputCollection().add(stdDataShowInput);
                idShowNew = em.merge(idShowNew);
            }
            if (idSettingNameOld != null && !idSettingNameOld.equals(idSettingNameNew)) {
                idSettingNameOld.getStdDataShowInputCollection().remove(stdDataShowInput);
                idSettingNameOld = em.merge(idSettingNameOld);
            }
            if (idSettingNameNew != null && !idSettingNameNew.equals(idSettingNameOld)) {
                idSettingNameNew.getStdDataShowInputCollection().add(stdDataShowInput);
                idSettingNameNew = em.merge(idSettingNameNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = stdDataShowInput.getIdStdShowDataInput();
                if (findStdDataShowInput(id) == null) {
                    throw new NonexistentEntityException("The stdDataShowInput with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            StdDataShowInput stdDataShowInput;
            try {
                stdDataShowInput = em.getReference(StdDataShowInput.class, id);
                stdDataShowInput.getIdStdShowDataInput();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The stdDataShowInput with id " + id + " no longer exists.", enfe);
            }
            StdDataShow idShow = stdDataShowInput.getIdShow();
            if (idShow != null) {
                idShow.getStdDataShowInputCollection().remove(stdDataShowInput);
                idShow = em.merge(idShow);
            }
            StdTimeSetting idSettingName = stdDataShowInput.getIdSettingName();
            if (idSettingName != null) {
                idSettingName.getStdDataShowInputCollection().remove(stdDataShowInput);
                idSettingName = em.merge(idSettingName);
            }
            em.remove(stdDataShowInput);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<StdDataShowInput> findStdDataShowInputEntities() {
        return findStdDataShowInputEntities(true, -1, -1);
    }

    public List<StdDataShowInput> findStdDataShowInputEntities(int maxResults, int firstResult) {
        return findStdDataShowInputEntities(false, maxResults, firstResult);
    }

    private List<StdDataShowInput> findStdDataShowInputEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(StdDataShowInput.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public StdDataShowInput findStdDataShowInput(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(StdDataShowInput.class, id);
        } finally {
            em.close();
        }
    }

    public int getStdDataShowInputCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<StdDataShowInput> rt = cq.from(StdDataShowInput.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    //**** find data process && setting 
    public StdDataShowInput findDataProcessAndSetting(StdDataShow idStdShow,StdTimeSetting idStdTimeSetting)
    {
        EntityManager  em = getEntityManager();
        try
        {
            Query q  = em.createQuery("SELECT s FROM StdDataShowInput s WHERE s.idShow =:idStdShow and s.idSettingName =:idStdTimeSetting");
            q.setParameter("idStdShow", idStdShow);
            q.setParameter("idStdTimeSetting",idStdTimeSetting);
            return  (StdDataShowInput) q.getSingleResult();
        }
        catch(Exception e)
        {
            System.out.println("Error "+e);
            return null;
        }
        finally
        {
            em.close();
        }
    } 
    //find stdsetting 
    public List<StdDataShowInput>  findStdSetting(StdTimeSetting idSettingName)
    {
        EntityManager  em  = getEntityManager();
        try
        {
            Query q = em.createQuery("SELECT s from StdDataShowInput s WHERE s.idSettingName =:idSettingName");
            q.setParameter("idSettingName",idSettingName);
            return  q.getResultList();
        }
        finally
        {
            em.close();
        }
    }
}
