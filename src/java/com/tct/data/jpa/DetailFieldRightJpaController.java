/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data.jpa;

import com.tct.data.DetailFieldRight;
import com.tct.data.DetailFieldRightDate;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.DetailSpecial;
import com.tct.data.FieldName;
import com.tct.data.jpa.exceptions.IllegalOrphanException;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 19, 2012, Time : 3:00:28 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class DetailFieldRightJpaController implements Serializable {

    public DetailFieldRightJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DetailFieldRight detailFieldRight) throws PreexistingEntityException, Exception {
        if (detailFieldRight.getDetailFieldRightDateCollection() == null) {
            detailFieldRight.setDetailFieldRightDateCollection(new ArrayList<DetailFieldRightDate>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            FieldName idFieldName = detailFieldRight.getIdFieldName();
            if (idFieldName != null) {
                idFieldName = em.getReference(idFieldName.getClass(), idFieldName.getId());
                detailFieldRight.setIdFieldName(idFieldName);
            }
            DetailSpecial idDetailSpecail = detailFieldRight.getIdDetailSpecail();
            if (idDetailSpecail != null) {
                idDetailSpecail = em.getReference(idDetailSpecail.getClass(), idDetailSpecail.getId());
                detailFieldRight.setIdDetailSpecail(idDetailSpecail);
            }
            Collection<DetailFieldRightDate> attachedDetailFieldRightDateCollection = new ArrayList<DetailFieldRightDate>();
            for (DetailFieldRightDate detailFieldRightDateCollectionDetailFieldRightDateToAttach : detailFieldRight.getDetailFieldRightDateCollection()) {
                detailFieldRightDateCollectionDetailFieldRightDateToAttach = em.getReference(detailFieldRightDateCollectionDetailFieldRightDateToAttach.getClass(), detailFieldRightDateCollectionDetailFieldRightDateToAttach.getIdFieldRihgtData());
                attachedDetailFieldRightDateCollection.add(detailFieldRightDateCollectionDetailFieldRightDateToAttach);
            }
            detailFieldRight.setDetailFieldRightDateCollection(attachedDetailFieldRightDateCollection);
            em.persist(detailFieldRight);
            if (idFieldName != null) {
                idFieldName.getDetailFieldRightCollection().add(detailFieldRight);
                idFieldName = em.merge(idFieldName);
            }
            if (idDetailSpecail != null) {
                idDetailSpecail.getDetailFieldRightCollection().add(detailFieldRight);
                idDetailSpecail = em.merge(idDetailSpecail);
            }
            for (DetailFieldRightDate detailFieldRightDateCollectionDetailFieldRightDate : detailFieldRight.getDetailFieldRightDateCollection()) {
                DetailFieldRight oldIdDetailRightOfDetailFieldRightDateCollectionDetailFieldRightDate = detailFieldRightDateCollectionDetailFieldRightDate.getIdDetailRight();
                detailFieldRightDateCollectionDetailFieldRightDate.setIdDetailRight(detailFieldRight);
                detailFieldRightDateCollectionDetailFieldRightDate = em.merge(detailFieldRightDateCollectionDetailFieldRightDate);
                if (oldIdDetailRightOfDetailFieldRightDateCollectionDetailFieldRightDate != null) {
                    oldIdDetailRightOfDetailFieldRightDateCollectionDetailFieldRightDate.getDetailFieldRightDateCollection().remove(detailFieldRightDateCollectionDetailFieldRightDate);
                    oldIdDetailRightOfDetailFieldRightDateCollectionDetailFieldRightDate = em.merge(oldIdDetailRightOfDetailFieldRightDateCollectionDetailFieldRightDate);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDetailFieldRight(detailFieldRight.getId()) != null) {
                throw new PreexistingEntityException("DetailFieldRight " + detailFieldRight + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DetailFieldRight detailFieldRight) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailFieldRight persistentDetailFieldRight = em.find(DetailFieldRight.class, detailFieldRight.getId());
            FieldName idFieldNameOld = persistentDetailFieldRight.getIdFieldName();
            FieldName idFieldNameNew = detailFieldRight.getIdFieldName();
            DetailSpecial idDetailSpecailOld = persistentDetailFieldRight.getIdDetailSpecail();
            DetailSpecial idDetailSpecailNew = detailFieldRight.getIdDetailSpecail();
            Collection<DetailFieldRightDate> detailFieldRightDateCollectionOld = persistentDetailFieldRight.getDetailFieldRightDateCollection();
            Collection<DetailFieldRightDate> detailFieldRightDateCollectionNew = detailFieldRight.getDetailFieldRightDateCollection();
            List<String> illegalOrphanMessages = null;
            for (DetailFieldRightDate detailFieldRightDateCollectionOldDetailFieldRightDate : detailFieldRightDateCollectionOld) {
                if (!detailFieldRightDateCollectionNew.contains(detailFieldRightDateCollectionOldDetailFieldRightDate)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain DetailFieldRightDate " + detailFieldRightDateCollectionOldDetailFieldRightDate + " since its idDetailRight field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idFieldNameNew != null) {
                idFieldNameNew = em.getReference(idFieldNameNew.getClass(), idFieldNameNew.getId());
                detailFieldRight.setIdFieldName(idFieldNameNew);
            }
            if (idDetailSpecailNew != null) {
                idDetailSpecailNew = em.getReference(idDetailSpecailNew.getClass(), idDetailSpecailNew.getId());
                detailFieldRight.setIdDetailSpecail(idDetailSpecailNew);
            }
            Collection<DetailFieldRightDate> attachedDetailFieldRightDateCollectionNew = new ArrayList<DetailFieldRightDate>();
            for (DetailFieldRightDate detailFieldRightDateCollectionNewDetailFieldRightDateToAttach : detailFieldRightDateCollectionNew) {
                detailFieldRightDateCollectionNewDetailFieldRightDateToAttach = em.getReference(detailFieldRightDateCollectionNewDetailFieldRightDateToAttach.getClass(), detailFieldRightDateCollectionNewDetailFieldRightDateToAttach.getIdFieldRihgtData());
                attachedDetailFieldRightDateCollectionNew.add(detailFieldRightDateCollectionNewDetailFieldRightDateToAttach);
            }
            detailFieldRightDateCollectionNew = attachedDetailFieldRightDateCollectionNew;
            detailFieldRight.setDetailFieldRightDateCollection(detailFieldRightDateCollectionNew);
            detailFieldRight = em.merge(detailFieldRight);
            if (idFieldNameOld != null && !idFieldNameOld.equals(idFieldNameNew)) {
                idFieldNameOld.getDetailFieldRightCollection().remove(detailFieldRight);
                idFieldNameOld = em.merge(idFieldNameOld);
            }
            if (idFieldNameNew != null && !idFieldNameNew.equals(idFieldNameOld)) {
                idFieldNameNew.getDetailFieldRightCollection().add(detailFieldRight);
                idFieldNameNew = em.merge(idFieldNameNew);
            }
            if (idDetailSpecailOld != null && !idDetailSpecailOld.equals(idDetailSpecailNew)) {
                idDetailSpecailOld.getDetailFieldRightCollection().remove(detailFieldRight);
                idDetailSpecailOld = em.merge(idDetailSpecailOld);
            }
            if (idDetailSpecailNew != null && !idDetailSpecailNew.equals(idDetailSpecailOld)) {
                idDetailSpecailNew.getDetailFieldRightCollection().add(detailFieldRight);
                idDetailSpecailNew = em.merge(idDetailSpecailNew);
            }
            for (DetailFieldRightDate detailFieldRightDateCollectionNewDetailFieldRightDate : detailFieldRightDateCollectionNew) {
                if (!detailFieldRightDateCollectionOld.contains(detailFieldRightDateCollectionNewDetailFieldRightDate)) {
                    DetailFieldRight oldIdDetailRightOfDetailFieldRightDateCollectionNewDetailFieldRightDate = detailFieldRightDateCollectionNewDetailFieldRightDate.getIdDetailRight();
                    detailFieldRightDateCollectionNewDetailFieldRightDate.setIdDetailRight(detailFieldRight);
                    detailFieldRightDateCollectionNewDetailFieldRightDate = em.merge(detailFieldRightDateCollectionNewDetailFieldRightDate);
                    if (oldIdDetailRightOfDetailFieldRightDateCollectionNewDetailFieldRightDate != null && !oldIdDetailRightOfDetailFieldRightDateCollectionNewDetailFieldRightDate.equals(detailFieldRight)) {
                        oldIdDetailRightOfDetailFieldRightDateCollectionNewDetailFieldRightDate.getDetailFieldRightDateCollection().remove(detailFieldRightDateCollectionNewDetailFieldRightDate);
                        oldIdDetailRightOfDetailFieldRightDateCollectionNewDetailFieldRightDate = em.merge(oldIdDetailRightOfDetailFieldRightDateCollectionNewDetailFieldRightDate);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = detailFieldRight.getId();
                if (findDetailFieldRight(id) == null) {
                    throw new NonexistentEntityException("The detailFieldRight with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetailFieldRight detailFieldRight;
            try {
                detailFieldRight = em.getReference(DetailFieldRight.class, id);
                detailFieldRight.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The detailFieldRight with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<DetailFieldRightDate> detailFieldRightDateCollectionOrphanCheck = detailFieldRight.getDetailFieldRightDateCollection();
            for (DetailFieldRightDate detailFieldRightDateCollectionOrphanCheckDetailFieldRightDate : detailFieldRightDateCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This DetailFieldRight (" + detailFieldRight + ") cannot be destroyed since the DetailFieldRightDate " + detailFieldRightDateCollectionOrphanCheckDetailFieldRightDate + " in its detailFieldRightDateCollection field has a non-nullable idDetailRight field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            FieldName idFieldName = detailFieldRight.getIdFieldName();
            if (idFieldName != null) {
                idFieldName.getDetailFieldRightCollection().remove(detailFieldRight);
                idFieldName = em.merge(idFieldName);
            }
            DetailSpecial idDetailSpecail = detailFieldRight.getIdDetailSpecail();
            if (idDetailSpecail != null) {
                idDetailSpecail.getDetailFieldRightCollection().remove(detailFieldRight);
                idDetailSpecail = em.merge(idDetailSpecail);
            }
            em.remove(detailFieldRight);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DetailFieldRight> findDetailFieldRightEntities() {
        return findDetailFieldRightEntities(true, -1, -1);
    }

    public List<DetailFieldRight> findDetailFieldRightEntities(int maxResults, int firstResult) {
        return findDetailFieldRightEntities(false, maxResults, firstResult);
    }

    private List<DetailFieldRight> findDetailFieldRightEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(DetailFieldRight.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DetailFieldRight findDetailFieldRight(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DetailFieldRight.class, id);
        } finally {
            em.close();
        }
    }

    public int getDetailFieldRightCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<DetailFieldRight> rt = cq.from(DetailFieldRight.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    public List<DetailFieldRight> findByDetialSpecial(DetailSpecial detailSpecial)
    {
        EntityManager em  = getEntityManager();
        try
        {
            Query q = em.createQuery("SELECT d FROM DetailFieldRight d WHERE d.deleted = 0 and d.idDetailSpecail=:idDetailSpecail").setParameter("idDetailSpecail",detailSpecial);
            return q.getResultList();
        }
        finally
        {
            em.close();
        }
    }
}
