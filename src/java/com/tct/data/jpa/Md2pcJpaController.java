/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data.jpa;

import com.tct.data.Md2pc;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.tct.data.ProcessPool;
import com.tct.data.ModelPool;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author The_Boy_Cs
 */
public class Md2pcJpaController implements Serializable {

    public Md2pcJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Md2pc md2pc) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ProcessPool processPoolIdProc = md2pc.getProcessPoolIdProc();
            if (processPoolIdProc != null) {
                processPoolIdProc = em.getReference(processPoolIdProc.getClass(), processPoolIdProc.getIdProc());
                md2pc.setProcessPoolIdProc(processPoolIdProc);
            }
            ModelPool modelPoolId = md2pc.getModelPoolId();
            if (modelPoolId != null) {
                modelPoolId = em.getReference(modelPoolId.getClass(), modelPoolId.getId());
                md2pc.setModelPoolId(modelPoolId);
            }
            em.persist(md2pc);
            if (processPoolIdProc != null) {
                processPoolIdProc.getMd2pcCollection().add(md2pc);
                processPoolIdProc = em.merge(processPoolIdProc);
            }
            if (modelPoolId != null) {
                modelPoolId.getMd2pcCollection().add(md2pc);
                modelPoolId = em.merge(modelPoolId);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findMd2pc(md2pc.getMd2pcId()) != null) {
                throw new PreexistingEntityException("Md2pc " + md2pc + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Md2pc md2pc) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Md2pc persistentMd2pc = em.find(Md2pc.class, md2pc.getMd2pcId());
            ProcessPool processPoolIdProcOld = persistentMd2pc.getProcessPoolIdProc();
            ProcessPool processPoolIdProcNew = md2pc.getProcessPoolIdProc();
            ModelPool modelPoolIdOld = persistentMd2pc.getModelPoolId();
            ModelPool modelPoolIdNew = md2pc.getModelPoolId();
            if (processPoolIdProcNew != null) {
                processPoolIdProcNew = em.getReference(processPoolIdProcNew.getClass(), processPoolIdProcNew.getIdProc());
                md2pc.setProcessPoolIdProc(processPoolIdProcNew);
            }
            if (modelPoolIdNew != null) {
                modelPoolIdNew = em.getReference(modelPoolIdNew.getClass(), modelPoolIdNew.getId());
                md2pc.setModelPoolId(modelPoolIdNew);
            }
            md2pc = em.merge(md2pc);
            if (processPoolIdProcOld != null && !processPoolIdProcOld.equals(processPoolIdProcNew)) {
                processPoolIdProcOld.getMd2pcCollection().remove(md2pc);
                processPoolIdProcOld = em.merge(processPoolIdProcOld);
            }
            if (processPoolIdProcNew != null && !processPoolIdProcNew.equals(processPoolIdProcOld)) {
                processPoolIdProcNew.getMd2pcCollection().add(md2pc);
                processPoolIdProcNew = em.merge(processPoolIdProcNew);
            }
            if (modelPoolIdOld != null && !modelPoolIdOld.equals(modelPoolIdNew)) {
                modelPoolIdOld.getMd2pcCollection().remove(md2pc);
                modelPoolIdOld = em.merge(modelPoolIdOld);
            }
            if (modelPoolIdNew != null && !modelPoolIdNew.equals(modelPoolIdOld)) {
                modelPoolIdNew.getMd2pcCollection().add(md2pc);
                modelPoolIdNew = em.merge(modelPoolIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = md2pc.getMd2pcId();
                if (findMd2pc(id) == null) {
                    throw new NonexistentEntityException("The md2pc with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Md2pc md2pc;
            try {
                md2pc = em.getReference(Md2pc.class, id);
                md2pc.getMd2pcId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The md2pc with id " + id + " no longer exists.", enfe);
            }
            ProcessPool processPoolIdProc = md2pc.getProcessPoolIdProc();
            if (processPoolIdProc != null) {
                processPoolIdProc.getMd2pcCollection().remove(md2pc);
                processPoolIdProc = em.merge(processPoolIdProc);
            }
            ModelPool modelPoolId = md2pc.getModelPoolId();
            if (modelPoolId != null) {
                modelPoolId.getMd2pcCollection().remove(md2pc);
                modelPoolId = em.merge(modelPoolId);
            }
            em.remove(md2pc);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Md2pc> findMd2pcEntities() {
        return findMd2pcEntities(true, -1, -1);
    }

    public List<Md2pc> findMd2pcEntities(int maxResults, int firstResult) {
        return findMd2pcEntities(false, maxResults, firstResult);
    }

    private List<Md2pc> findMd2pcEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Md2pc.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
/*
    public Md2pc findMd2pc(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Md2pc.class, id);
        } finally {
            em.close();
        }
    }*/    
     //------find md2pc
    public List<Md2pc>  findMd2pc(String modelPoolId){
        EntityManager em   = getEntityManager();
        try{
             //Query q  =  em.createQuery("SELECT  a FROM  Md2pc a WHERE  a.modelPoolId='"+modelPoolId+"'",Md2pc.class); 
             Query q  =  em.createQuery("SELECT  a FROM  Md2pc a WHERE a.sqProcess = '1' and d.deleted = 0 ",Md2pc.class);  
             return q.getResultList();
        }
        finally{
            em.close();
        }
        
    } 
    //--find neet sq_process
    public Md2pc  findSqProcess(ProcessPool processPoolId,ModelPool  modelPoolId){
        EntityManager  em  = getEntityManager();
        try{
            Query q  = em.createNamedQuery("Md2pc.findBySqProcess");
            q.setParameter("modelPoolId", modelPoolId);
            q.setParameter("processPoolIdProc",processPoolId);
            return (Md2pc)q.getSingleResult();
        }
        catch(Exception e)
        {
            return null;
        }    
        finally{
            em.close();
        }
    
    }
    //--find check sq_process
    public Md2pc checkSqProcess(ModelPool  modelPoolId,int sqProcess){
        EntityManager  em  = getEntityManager();
        try{
            Query q  = em.createNamedQuery("Md2pc.checkSqProcess");
            q.setParameter("modelPoolId", modelPoolId);
            q.setParameter("sqProcess",sqProcess);
            return (Md2pc)q.getSingleResult();
        }
        catch(Exception e)
        {
            return null;
        }
        finally{
            em.close();
        }
    
    }
    
    
    
    
    public int getMd2pcCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Md2pc> rt = cq.from(Md2pc.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<Md2pc> findBymodelPoolId(ModelPool modelpool){
        EntityManager em = getEntityManager();
        Query query = null;
        try {
            query = em.createNamedQuery("Md2pc.findBymodelPoolId");
            query.setParameter("modelPoolId", modelpool);
            return query.getResultList();
        }finally{
            em.close();
        }
    }
    
    public List<Md2pc> findByProcess(ProcessPool pcpool){
        EntityManager em = getEntityManager();
        Query query = null;
        try {
            query = em.createNamedQuery("Md2pc.findByProcessPoolIdProc");
            query.setParameter("processPoolIdProc", pcpool);
            return query.getResultList();
        }finally{
            em.close();
        }
    } 
    public Md2pc  findByMd2pcId(String md2pcId)
    {
        EntityManager em  = getEntityManager();
        try
        {
            Query q  = em.createNamedQuery("Md2pc.findByMd2pcId");
            q.setParameter("md2pcId", md2pcId);
            return (Md2pc) q.getSingleResult();
        }
        catch(Exception e)
        {
            System.out.println(e);
            return null;
        }
        finally
        {
            em.close();
        }
    }  
}
