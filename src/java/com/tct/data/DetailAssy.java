/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "detail_assy", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailAssy.findAll", query = "SELECT d FROM DetailAssy d"),
    @NamedQuery(name = "DetailAssy.findById", query = "SELECT d FROM DetailAssy d WHERE d.id = :id"),
    @NamedQuery(name = "DetailAssy.findByUser", query = "SELECT d FROM DetailAssy d WHERE d.userCreate = :userCreate"),
    @NamedQuery(name = "DetailAssy.findByInjectionDate", query = "SELECT d FROM DetailAssy d WHERE d.injectionDate = :injectionDate"),
    @NamedQuery(name = "DetailAssy.findByPlatingDate", query = "SELECT d FROM DetailAssy d WHERE d.platingDate = :platingDate"),
    @NamedQuery(name = "DetailAssy.findByDrcoreLotNo", query = "SELECT d FROM DetailAssy d WHERE d.drcoreLotNo = :drcoreLotNo"),
    @NamedQuery(name = "DetailAssy.findByInvoiceDrcore", query = "SELECT d FROM DetailAssy d WHERE d.invoiceDrcore = :invoiceDrcore"),
    @NamedQuery(name = "DetailAssy.findByAssyDate", query = "SELECT d FROM DetailAssy d WHERE d.assyDate = :assyDate"),
    @NamedQuery(name = "DetailAssy.findByRollNo", query = "SELECT d FROM DetailAssy d WHERE d.rollNo = :rollNo"),
    @NamedQuery(name = "DetailAssy.findByPdType", query = "SELECT d FROM DetailAssy d WHERE d.pdType = :pdType"),
    @NamedQuery(name = "DetailAssy.findByPdCodename", query = "SELECT d FROM DetailAssy d WHERE d.pdCodename = :pdCodename"),
    @NamedQuery(name = "DetailAssy.findByPdCode", query = "SELECT d FROM DetailAssy d WHERE d.pdCode = :pdCode"),
    @NamedQuery(name = "DetailAssy.findByProductQty", query = "SELECT d FROM DetailAssy d WHERE d.productQty = :productQty"),
    @NamedQuery(name = "DetailAssy.findBydetailIdDetail", query = "SELECT d FROM DetailAssy d WHERE d.detailIdDetail = :detailIdDetail"),
    @NamedQuery(name = "DetailAssy.findByNumOnDate", query = "SELECT d FROM DetailAssy d WHERE d.numOnDate = :numOnDate")})
public class DetailAssy implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Column(name = "user_create", length = 36)
    private String userCreate;
    @Column(name = "injection_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date injectionDate;
    @Column(name = "plating_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date platingDate;
    @Column(name = "drcore_lot_no", length = 100)
    private String drcoreLotNo;
    @Basic(optional = false)
    @Column(name = "invoice_drcore", nullable = false, length = 255)
    private String invoiceDrcore;
    @Column(name = "assy_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date assyDate;
    @Column(name = "roll_no", length = 100)
    private String rollNo;
    @Column(name = "pd_type", length = 255)
    private String pdType;
    @Column(name = "pd_codename", length = 255)
    private String pdCodename;
    @Column(name = "pd_code", length = 255)
    private String pdCode;
    @Column(name = "product_qty")
    private Integer productQty;
    @Column(name = "num_on_date")
    private Integer numOnDate;
    @Basic(optional = false)
    @Column(name = "stamp", nullable = false, length = 255)
    private String stamp;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    @JoinColumn(name = "detail_id_detail", referencedColumnName = "id_detail", nullable = false)
    @ManyToOne(optional = false)
    private Details detailIdDetail;

    public DetailAssy() {
    }

    public DetailAssy(String id) {
        this.id = id;
    }

    public DetailAssy(String id, String invoiceDrcore, String stamp, int deleted) {
        this.id = id;
        this.invoiceDrcore = invoiceDrcore;
        this.stamp = stamp;
        this.deleted = deleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public Date getInjectionDate() {
        return injectionDate;
    }

    public void setInjectionDate(Date injectionDate) {
        this.injectionDate = injectionDate;
    }

    public Date getPlatingDate() {
        return platingDate;
    }

    public void setPlatingDate(Date platingDate) {
        this.platingDate = platingDate;
    }

    public String getDrcoreLotNo() {
        return drcoreLotNo;
    }

    public void setDrcoreLotNo(String drcoreLotNo) {
        this.drcoreLotNo = drcoreLotNo;
    }

    public String getInvoiceDrcore() {
        return invoiceDrcore;
    }

    public void setInvoiceDrcore(String invoiceDrcore) {
        this.invoiceDrcore = invoiceDrcore;
    }

    public Date getAssyDate() {
        return assyDate;
    }

    public void setAssyDate(Date assyDate) {
        this.assyDate = assyDate;
    }

    public String getRollNo() {
        return rollNo;
    }

    public void setRollNo(String rollNo) {
        this.rollNo = rollNo;
    }

    public String getPdType() {
        return pdType;
    }

    public void setPdType(String pdType) {
        this.pdType = pdType;
    }

    public String getPdCodename() {
        return pdCodename;
    }

    public void setPdCodename(String pdCodename) {
        this.pdCodename = pdCodename;
    }

    public String getPdCode() {
        return pdCode;
    }

    public void setPdCode(String pdCode) {
        this.pdCode = pdCode;
    }

    public Integer getProductQty() {
        return productQty;
    }

    public void setProductQty(Integer productQty) {
        this.productQty = productQty;
    }

    public Integer getNumOnDate() {
        return numOnDate;
    }

    public void setNumOnDate(Integer numOnDate) {
        this.numOnDate = numOnDate;
    }

    public String getStamp() {
        return stamp;
    }

    public void setStamp(String stamp) {
        this.stamp = stamp;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public Details getDetailIdDetail() {
        return detailIdDetail;
    }

    public void setDetailIdDetail(Details detailIdDetail) {
        this.detailIdDetail = detailIdDetail;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailAssy)) {
            return false;
        }
        DetailAssy other = (DetailAssy) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.DetailAssy[ id=" + id + " ]";
    }
    
}
