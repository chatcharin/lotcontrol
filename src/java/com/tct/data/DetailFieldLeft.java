/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 19, 2012, Time : 2:57:01 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
@Entity
@Table(name = "detail_field_left", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailFieldLeft.findAll", query = "SELECT d FROM DetailFieldLeft d"),
    @NamedQuery(name = "DetailFieldLeft.findById", query = "SELECT d FROM DetailFieldLeft d WHERE d.id = :id"),
    @NamedQuery(name = "DetailFieldLeft.findByRequest", query = "SELECT d FROM DetailFieldLeft d WHERE d.request = :request"),
    @NamedQuery(name = "DetailFieldLeft.findByReport", query = "SELECT d FROM DetailFieldLeft d WHERE d.report = :report"),
    @NamedQuery(name = "DetailFieldLeft.findByDeleted", query = "SELECT d FROM DetailFieldLeft d WHERE d.deleted = :deleted")})
public class DetailFieldLeft implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Column(name = "request")
    private Integer request;
    @Column(name = "report")
    private Integer report;
    @Column(name = "deleted")
    private Integer deleted;
    @JoinColumn(name = "id_detail_specail", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private DetailSpecial idDetailSpecail;
    @JoinColumn(name = "id_field_name", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private FieldName idFieldName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDetailFieldLeft")
    private Collection<DetailFieldLeftData> detailFieldLeftDataCollection;
    public DetailFieldLeft() {
    }

    public DetailFieldLeft(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getRequest() {
        return request;
    }

    public void setRequest(Integer request) {
        this.request = request;
    }

    public Integer getReport() {
        return report;
    }

    public void setReport(Integer report) {
        this.report = report;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public DetailSpecial getIdDetailSpecail() {
        return idDetailSpecail;
    }

    public void setIdDetailSpecail(DetailSpecial idDetailSpecail) {
        this.idDetailSpecail = idDetailSpecail;
    }

    public FieldName getIdFieldName() {
        return idFieldName;
    }

    public void setIdFieldName(FieldName idFieldName) {
        this.idFieldName = idFieldName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailFieldLeft)) {
            return false;
        }
        DetailFieldLeft other = (DetailFieldLeft) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.DetailFieldLeft[ id=" + id + " ]";
    }
    @XmlTransient
    public Collection<DetailFieldLeftData> getDetailFieldLeftDataCollection() {
        return detailFieldLeftDataCollection;
    }

    public void setDetailFieldLeftDataCollection(Collection<DetailFieldLeftData> detailFieldLeftDataCollection) {
        this.detailFieldLeftDataCollection = detailFieldLeftDataCollection;
    }
}
