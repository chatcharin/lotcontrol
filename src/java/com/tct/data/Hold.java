/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "hold", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Hold.findAll", query = "SELECT h FROM Hold h"),
    @NamedQuery(name = "Hold.findById", query = "SELECT h FROM Hold h WHERE h.id = :id"),
    @NamedQuery(name = "Hold.findByCurrentProcessId", query = "SELECT h FROM Hold h WHERE h.currentProcessId = :currentProcessId"),
    @NamedQuery(name = "Hold.findByStatus", query = "SELECT h FROM Hold h WHERE h.status = :status"),
    @NamedQuery(name = "Hold.findByQty", query = "SELECT h FROM Hold h WHERE h.qty = :qty")})
public class Hold implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Basic(optional = false)
    @Column(name = "status", nullable = false, length = 100)
    private String status;
    @Column(name = "qty", length = 255)
    private String qty;
    @JoinColumn(name = "current_process_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private CurrentProcess currentProcessId;
    @Column(name = "ref_idHoldStart", nullable = false, length = 36)
    private String refIdHoldStart;
	
    public Hold() {
    }

    public Hold(String id) {
        this.id = id;
    }

    public Hold(String id, String status) {
        this.id = id;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public CurrentProcess getCurrentProcessId() {
        return currentProcessId;
    }

    public void setCurrentProcessId(CurrentProcess currentProcessId) {
        this.currentProcessId = currentProcessId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hold)) {
            return false;
        }
        Hold other = (Hold) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    } 
    @Override
    public String toString() {
        return "com.tct.data.Hold[ id=" + id + " ]";
    }

    public String getRefIdHoldStart() {
        return refIdHoldStart;
    }

    public void setRefIdHoldStart(String refIdHoldStart) {
        this.refIdHoldStart = refIdHoldStart;
    } 
}
