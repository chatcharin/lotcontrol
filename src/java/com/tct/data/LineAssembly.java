/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "line_assembly", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LineAssembly.findAll", query = "SELECT l FROM LineAssembly l WHERE l.deleted = 0"),
    @NamedQuery(name = "LineAssembly.findById", query = "SELECT l FROM LineAssembly l WHERE l.id = :id"),
    @NamedQuery(name = "LineAssembly.findByUser", query = "SELECT l FROM LineAssembly l WHERE l.userCreate = :userCreate"),
    @NamedQuery(name = "LineAssembly.findByRemark", query = "SELECT l FROM LineAssembly l WHERE l.remark = :remark"),
    @NamedQuery(name = "LineAssembly.findByStamp", query = "SELECT l FROM LineAssembly l WHERE l.stamp = :stamp"),
    @NamedQuery(name = "LineAssembly.findByDetailproductreport", query = "SELECT l FROM LineAssembly l WHERE l.detailproductreport = :detailproductreport"),
    @NamedQuery(name = "LineAssembly.findByPotcore", query = "SELECT l FROM LineAssembly l WHERE l.detailIdDetail = :detailIdDetail"),
    @NamedQuery(name = "LineAssembly.findBydetailIdDetail", query = "SELECT l FROM LineAssembly l WHERE l.detailIdDetail = :detailIdDetail"),
    @NamedQuery(name = "LineAssembly.findByLotcontrolauto", query = "SELECT l FROM LineAssembly l WHERE l.lotcontrolauto = :lotcontrolauto")})
public class LineAssembly implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Column(name = "user_create", length = 36)
    private String userCreate;
    @Column(name = "remark", length = 255)
    private String remark;
    @Column(name = "stamp", length = 100)
    private String stamp;
    @Column(name = "detailproductreport", length = 255)
    private String detailproductreport;
    @Column(name = "potcore", length = 100)
    private String potcore;
    @Column(name = "invoice_potcore", length = 255)
    private String invoicePotcore;
    @Column(name = "solder", length = 255)
    private String solder;
    @Column(name = "invoice_solder", length = 255)
    private String invoiceSolder;
    @Column(name = "ink", length = 255)
    private String ink;
    @Column(name = "invoice_ink", length = 255)
    private String invoiceInk;
    @Column(name = "bond", length = 255)
    private String bond;
    @Column(name = "invoice_bond", length = 255)
    private String invoiceBond;
    @Column(name = "lotcontrolauto", length = 100)
    private String lotcontrolauto;
    @Basic(optional = false)
    @Column(name = "pd_code_name", nullable = false, length = 255)
    private String pdCodeName;
    @Basic(optional = false)
    @Column(name = "pd_code", nullable = false, length = 255)
    private String pdCode;
    @Basic(optional = false)
    @Column(name = "pd_type", nullable = false, length = 255)
    private String pdType;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    @JoinColumn(name = "detail_id_detail", referencedColumnName = "id_detail", nullable = false)
    @ManyToOne(optional = false)
    private Details detailIdDetail;

    public LineAssembly() {
    }

    public LineAssembly(String id) {
        this.id = id;
    }

    public LineAssembly(String id, String pdCodeName, String pdCode, String pdType, int deleted) {
        this.id = id;
        this.pdCodeName = pdCodeName;
        this.pdCode = pdCode;
        this.pdType = pdType;
        this.deleted = deleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStamp() {
        return stamp;
    }

    public void setStamp(String stamp) {
        this.stamp = stamp;
    }

    public String getDetailproductreport() {
        return detailproductreport;
    }

    public void setDetailproductreport(String detailproductreport) {
        this.detailproductreport = detailproductreport;
    }

    public String getPotcore() {
        return potcore;
    }

    public void setPotcore(String potcore) {
        this.potcore = potcore;
    }

    public String getInvoicePotcore() {
        return invoicePotcore;
    }

    public void setInvoicePotcore(String invoicePotcore) {
        this.invoicePotcore = invoicePotcore;
    }

    public String getSolder() {
        return solder;
    }

    public void setSolder(String solder) {
        this.solder = solder;
    }

    public String getInvoiceSolder() {
        return invoiceSolder;
    }

    public void setInvoiceSolder(String invoiceSolder) {
        this.invoiceSolder = invoiceSolder;
    }

    public String getInk() {
        return ink;
    }

    public void setInk(String ink) {
        this.ink = ink;
    }

    public String getInvoiceInk() {
        return invoiceInk;
    }

    public void setInvoiceInk(String invoiceInk) {
        this.invoiceInk = invoiceInk;
    }

    public String getBond() {
        return bond;
    }

    public void setBond(String bond) {
        this.bond = bond;
    }

    public String getInvoiceBond() {
        return invoiceBond;
    }

    public void setInvoiceBond(String invoiceBond) {
        this.invoiceBond = invoiceBond;
    }

    public String getLotcontrolauto() {
        return lotcontrolauto;
    }

    public void setLotcontrolauto(String lotcontrolauto) {
        this.lotcontrolauto = lotcontrolauto;
    }

    public String getPdCodeName() {
        return pdCodeName;
    }

    public void setPdCodeName(String pdCodeName) {
        this.pdCodeName = pdCodeName;
    }

    public String getPdCode() {
        return pdCode;
    }

    public void setPdCode(String pdCode) {
        this.pdCode = pdCode;
    }

    public String getPdType() {
        return pdType;
    }

    public void setPdType(String pdType) {
        this.pdType = pdType;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public Details getDetailIdDetail() {
        return detailIdDetail;
    }

    public void setDetailIdDetail(Details detailIdDetail) {
        this.detailIdDetail = detailIdDetail;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LineAssembly)) {
            return false;
        }
        LineAssembly other = (LineAssembly) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.LineAssembly[ id=" + id + " ]";
    }
}
