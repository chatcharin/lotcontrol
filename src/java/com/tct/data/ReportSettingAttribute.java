/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Oct 18, 2012, Time : 6:52:02 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
@Entity
@Table(name = "report_setting_attribute", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReportSettingAttribute.findAll", query = "SELECT r FROM ReportSettingAttribute r"),
    @NamedQuery(name = "ReportSettingAttribute.findByIdReportSettingAtt", query = "SELECT r FROM ReportSettingAttribute r WHERE r.idReportSettingAtt = :idReportSettingAtt"),
    @NamedQuery(name = "ReportSettingAttribute.findByNameReportAtt", query = "SELECT r FROM ReportSettingAttribute r WHERE r.nameReportAtt = :nameReportAtt"),
    @NamedQuery(name = "ReportSettingAttribute.findByValueReportAtt", query = "SELECT r FROM ReportSettingAttribute r WHERE r.valueReportAtt = :valueReportAtt"),
    @NamedQuery(name = "ReportSettingAttribute.findByDeleted", query = "SELECT r FROM ReportSettingAttribute r WHERE r.deleted = :deleted")})
public class ReportSettingAttribute implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_report_setting_att", nullable = false, length = 36)
    private String idReportSettingAtt;
    @Basic(optional = false)
    @Column(name = "name_report_att", nullable = false, length = 255)
    private String nameReportAtt;
    @Basic(optional = false)
    @Column(name = "value_report_att", nullable = false, length = 255)
    private String valueReportAtt;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    @JoinColumn(name = "id_report_setting", referencedColumnName = "id_report_setting", nullable = false)
    @ManyToOne(optional = false)
    private ReportSetting idReportSetting;

    public ReportSettingAttribute() {
    }

    public ReportSettingAttribute(String idReportSettingAtt) {
        this.idReportSettingAtt = idReportSettingAtt;
    }

    public ReportSettingAttribute(String idReportSettingAtt, String nameReportAtt, String valueReportAtt, int deleted) {
        this.idReportSettingAtt = idReportSettingAtt;
        this.nameReportAtt = nameReportAtt;
        this.valueReportAtt = valueReportAtt;
        this.deleted = deleted;
    }

    public String getIdReportSettingAtt() {
        return idReportSettingAtt;
    }

    public void setIdReportSettingAtt(String idReportSettingAtt) {
        this.idReportSettingAtt = idReportSettingAtt;
    }

    public String getNameReportAtt() {
        return nameReportAtt;
    }

    public void setNameReportAtt(String nameReportAtt) {
        this.nameReportAtt = nameReportAtt;
    }

    public String getValueReportAtt() {
        return valueReportAtt;
    }

    public void setValueReportAtt(String valueReportAtt) {
        this.valueReportAtt = valueReportAtt;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public ReportSetting getIdReportSetting() {
        return idReportSetting;
    }

    public void setIdReportSetting(ReportSetting idReportSetting) {
        this.idReportSetting = idReportSetting;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idReportSettingAtt != null ? idReportSettingAtt.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReportSettingAttribute)) {
            return false;
        }
        ReportSettingAttribute other = (ReportSettingAttribute) object;
        if ((this.idReportSettingAtt == null && other.idReportSettingAtt != null) || (this.idReportSettingAtt != null && !this.idReportSettingAtt.equals(other.idReportSettingAtt))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.ReportSettingAttribute[ idReportSettingAtt=" + idReportSettingAtt + " ]";
    }

}
