/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 17, 2012, Time : 6:53:48 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
@Entity
@Table(name = "field_name", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FieldName.findAll", query = "SELECT f FROM FieldName f WHERE f.deleted = 0"),
    @NamedQuery(name = "FieldName.findById", query = "SELECT f FROM FieldName f WHERE (f.deleted = 0) and (f.id = :id)"),
    @NamedQuery(name = "FieldName.findByIdNotIn", query = "SELECT f FROM FieldName f WHERE (f.id NOT  in :id) and (f.deleted = 0 )"),
    @NamedQuery(name = "FieldName.findByName", query = "SELECT f FROM FieldName f WHERE (f.deleted = 0) and (f.name LIKE :name)"),
    @NamedQuery(name = "FieldName.findByType", query = "SELECT f FROM FieldName f WHERE (f.deleted = 0) and (f.type = :type)")})
public class FieldName implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Basic(optional = false)
    @Column(name = "name", nullable = false, length = 255)
    private String name;
    @Basic(optional = false)
    @Column(name = "type", nullable = false,length = 36)
    private String type;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idFieldName")
    private Collection<GroupField> groupFieldCollection;
    @Column(name = "detail", nullable = false, length = 255)
    private String detail;
    @Column(name = "request", nullable = false)
    private int request;
    @Column(name = "use_field_name", nullable = false)
    private int useFieldName;
    @Column(name = "deleted", nullable = false)
    private int deleted;
     @OneToMany(cascade = CascadeType.ALL, mappedBy = "idFieldName")
    private Collection<DetailFieldLeft> detailFieldLeftCollection;
     @OneToMany(cascade = CascadeType.ALL, mappedBy = "idFieldName")
    private Collection<DetailFieldRight> detailFieldRightCollection; 
    public FieldName() {
    }

    public FieldName(String id) {
        this.id = id;
    }

    public FieldName(String id, String name, String type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    } 
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

   

    @XmlTransient
    public Collection<GroupField> getGroupFieldCollection() {
        return groupFieldCollection;
    }

    public void setGroupFieldCollection(Collection<GroupField> groupFieldCollection) {
        this.groupFieldCollection = groupFieldCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FieldName)) {
            return false;
        }
        FieldName other = (FieldName) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.FieldName[ id=" + id + " ]";
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public int getRequest() {
        return request;
    }

    public void setRequest(int request) {
        this.request = request;
    }

    public int getUseFieldName() {
        return useFieldName;
    }

    public void setUseFieldName(int useFieldName) {
        this.useFieldName = useFieldName;
    }
     @XmlTransient
    public Collection<DetailFieldLeft> getDetailFieldLeftCollection() {
        return detailFieldLeftCollection;
    }

    public void setDetailFieldLeftCollection(Collection<DetailFieldLeft> detailFieldLeftCollection) {
        this.detailFieldLeftCollection = detailFieldLeftCollection;
    }
    @XmlTransient
    public Collection<DetailFieldRight> getDetailFieldRightCollection() {
        return detailFieldRightCollection;
    }

    public void setDetailFieldRightCollection(Collection<DetailFieldRight> detailFieldRightCollection) {
        this.detailFieldRightCollection = detailFieldRightCollection;
    }
}
