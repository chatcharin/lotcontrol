/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "option_t", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OptionT.findAll", query = "SELECT o FROM OptionT o"),
    @NamedQuery(name = "OptionT.findByIdKdataO", query = "SELECT o FROM OptionT o WHERE o.idKdataO = :idKdataO"),
    @NamedQuery(name = "OptionT.findByOptionName", query = "SELECT o FROM OptionT o WHERE o.optionName = :optionName"),
    @NamedQuery(name = "OptionT.findByCreateDate", query = "SELECT o FROM OptionT o WHERE o.createDate = :createDate"),
    @NamedQuery(name = "OptionT.findByUserCreate", query = "SELECT o FROM OptionT o WHERE o.userCreate = :userCreate"),
    @NamedQuery(name = "OptionT.findByStatusOption", query = "SELECT o FROM OptionT o WHERE o.statusOption = :statusOption"),
    @NamedQuery(name = "OptionT.findByOptionList", query = "SELECT o FROM OptionT o WHERE o.optionList = :optionList")})
public class OptionT implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_kdata_o", nullable = false, length = 36)
    private String idKdataO;
    @Column(name = "option_name", length = 100)
    private String optionName;
    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "user_create", nullable = false, length = 36)
    private String userCreate;
    @Column(name = "status_option", length = 100)
    private String statusOption;
    @Column(name = "option_list", length = 100)
    private String optionList;

    public OptionT() {
    }

    public OptionT(String idKdataO) {
        this.idKdataO = idKdataO;
    }

    public OptionT(String idKdataO, String userCreate) {
        this.idKdataO = idKdataO;
        this.userCreate = userCreate;
    }

    public String getIdKdataO() {
        return idKdataO;
    }

    public void setIdKdataO(String idKdataO) {
        this.idKdataO = idKdataO;
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getStatusOption() {
        return statusOption;
    }

    public void setStatusOption(String statusOption) {
        this.statusOption = statusOption;
    }

    public String getOptionList() {
        return optionList;
    }

    public void setOptionList(String optionList) {
        this.optionList = optionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idKdataO != null ? idKdataO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OptionT)) {
            return false;
        }
        OptionT other = (OptionT) object;
        if ((this.idKdataO == null && other.idKdataO != null) || (this.idKdataO != null && !this.idKdataO.equals(other.idKdataO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.OptionT[ idKdataO=" + idKdataO + " ]";
    }
    
}
