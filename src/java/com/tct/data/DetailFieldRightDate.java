/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author tawat
 */
@Entity
@Table(name = "detail_field_right_date", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailFieldRightDate.findAll", query = "SELECT d FROM DetailFieldRightDate d"),
    @NamedQuery(name = "DetailFieldRightDate.findByIdFieldRihgtData", query = "SELECT d FROM DetailFieldRightDate d WHERE d.idFieldRihgtData = :idFieldRihgtData"),
    @NamedQuery(name = "DetailFieldRightDate.findByDataRight", query = "SELECT d FROM DetailFieldRightDate d WHERE d.dataRight = :dataRight"),
    @NamedQuery(name = "DetailFieldRightDate.findByIdDetails", query = "SELECT d FROM DetailFieldRightDate d WHERE d.idDetails = :idDetails")})
public class DetailFieldRightDate implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_field_rihgt_data", nullable = false, length = 36)
    private String idFieldRihgtData;
    @Basic(optional = false)
    @Column(name = "data_right", nullable = false, length = 255)
    private String dataRight;
    @Basic(optional = false)
    @Column(name = "id_details", nullable = false, length = 36)
    private String idDetails;
    @JoinColumn(name = "id_detail_right", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private DetailFieldRight idDetailRight;

    public DetailFieldRightDate() {
    }

    public DetailFieldRightDate(String idFieldRihgtData) {
        this.idFieldRihgtData = idFieldRihgtData;
    }

    public DetailFieldRightDate(String idFieldRihgtData, String dataRight, String idDetails) {
        this.idFieldRihgtData = idFieldRihgtData;
        this.dataRight = dataRight;
        this.idDetails = idDetails;
    }

    public String getIdFieldRihgtData() {
        return idFieldRihgtData;
    }

    public void setIdFieldRihgtData(String idFieldRihgtData) {
        this.idFieldRihgtData = idFieldRihgtData;
    }

    public String getDataRight() {
        return dataRight;
    }

    public void setDataRight(String dataRight) {
        this.dataRight = dataRight;
    }

    public String getIdDetails() {
        return idDetails;
    }

    public void setIdDetails(String idDetails) {
        this.idDetails = idDetails;
    }

    public DetailFieldRight getIdDetailRight() {
        return idDetailRight;
    }

    public void setIdDetailRight(DetailFieldRight idDetailRight) {
        this.idDetailRight = idDetailRight;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFieldRihgtData != null ? idFieldRihgtData.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailFieldRightDate)) {
            return false;
        }
        DetailFieldRightDate other = (DetailFieldRightDate) object;
        if ((this.idFieldRihgtData == null && other.idFieldRihgtData != null) || (this.idFieldRihgtData != null && !this.idFieldRihgtData.equals(other.idFieldRihgtData))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.DetailFieldRightDate[ idFieldRihgtData=" + idFieldRihgtData + " ]";
    }
    
}
