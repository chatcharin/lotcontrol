/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Nov 2, 2012, Time : 1:45:07 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
@Entity
@Table(name = "std_data_show", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StdDataShow.findAll", query = "SELECT s FROM StdDataShow s"),
    @NamedQuery(name = "StdDataShow.findByIdStdShow", query = "SELECT s FROM StdDataShow s WHERE s.idStdShow = :idStdShow"),
    @NamedQuery(name = "StdDataShow.findByIdCurrentProc", query = "SELECT s FROM StdDataShow s WHERE s.idCurrentProc = :idCurrentProc"),
    @NamedQuery(name = "StdDataShow.findByTotalWorktime", query = "SELECT s FROM StdDataShow s WHERE s.totalWorktime = :totalWorktime"),
    @NamedQuery(name = "StdDataShow.findByStandardTime", query = "SELECT s FROM StdDataShow s WHERE s.standardTime = :standardTime")})
public class StdDataShow implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_std_show", nullable = false, length = 36)
    private String idStdShow;
    @Basic(optional = false)
    @Column(name = "id_current_proc", nullable = false,length = 36)
    private String idCurrentProc;
    @Basic(optional = false)
    @Column(name = "total_worktime", nullable = false, length = 255)
    private String totalWorktime;
    @Basic(optional = false)
    @Column(name = "standard_time", nullable = false, length = 255)
    private String standardTime;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idShow")
    private Collection<StdDataShowInput> stdDataShowInputCollection;
    @Column(name = "plan", nullable = false, length = 255)
    private String plan;   
    public StdDataShow() {
    }

    public StdDataShow(String idStdShow) {
        this.idStdShow = idStdShow;
    }

    public StdDataShow(String idStdShow,String idCurrentProc, String totalWorktime, String standardTime) {
        this.idStdShow = idStdShow;
        this.idCurrentProc = idCurrentProc;
        this.totalWorktime = totalWorktime;
        this.standardTime = standardTime;
    }

    public String getIdStdShow() {
        return idStdShow;
    }

    public void setIdStdShow(String idStdShow) {
        this.idStdShow = idStdShow;
    }

    public String getIdCurrentProc() {
        return idCurrentProc;
    }

    public void setIdCurrentProc(String idCurrentProc) {
        this.idCurrentProc = idCurrentProc;
    }

    public String getTotalWorktime() {
        return totalWorktime;
    }

    public void setTotalWorktime(String totalWorktime) {
        this.totalWorktime = totalWorktime;
    }

    public String getStandardTime() {
        return standardTime;
    }

    public void setStandardTime(String standardTime) {
        this.standardTime = standardTime;
    }

    @XmlTransient
    public Collection<StdDataShowInput> getStdDataShowInputCollection() {
        return stdDataShowInputCollection;
    }

    public void setStdDataShowInputCollection(Collection<StdDataShowInput> stdDataShowInputCollection) {
        this.stdDataShowInputCollection = stdDataShowInputCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStdShow != null ? idStdShow.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StdDataShow)) {
            return false;
        }
        StdDataShow other = (StdDataShow) object;
        if ((this.idStdShow == null && other.idStdShow != null) || (this.idStdShow != null && !this.idStdShow.equals(other.idStdShow))) {
            return false;
        }
        return true;
    } 
    @Override
    public String toString() {
        return "com.tct.data.StdDataShow[ idStdShow=" + idStdShow + " ]";
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    } 
}
