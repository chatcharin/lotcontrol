/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Oct 3, 2012, Time : 5:39:11 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
@Entity
@Table(name = "qa_sampling", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "QaSampling.findAll", query = "SELECT q FROM QaSampling q"),
    @NamedQuery(name = "QaSampling.findByIdSampling", query = "SELECT q FROM QaSampling q WHERE q.idSampling = :idSampling"),
    @NamedQuery(name = "QaSampling.findBySamplingQty", query = "SELECT q FROM QaSampling q WHERE q.samplingQty = :samplingQty"),
    @NamedQuery(name = "QaSampling.findByDateSampling", query = "SELECT q FROM QaSampling q WHERE q.dateSampling = :dateSampling"),
    @NamedQuery(name = "QaSampling.findByUserCreate", query = "SELECT q FROM QaSampling q WHERE q.userCreate = :userCreate"),
    @NamedQuery(name = "QaSampling.findByDeleted", query = "SELECT q FROM QaSampling q WHERE q.deleted = :deleted")})
public class QaSampling implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_sampling", nullable = false, length = 36)
    private String idSampling;
    @Basic(optional = false)
    @Column(name = "sampling_qty", nullable = false, length = 255)
    private String samplingQty;
    @Basic(optional = false)
    @Column(name = "date_sampling", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateSampling;
    @Basic(optional = false)
    @Column(name = "user_create", nullable = false, length = 255)
    private String userCreate;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    @JoinColumn(name = "id_pcsqty", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private PcsQty idPcsqty;

    public QaSampling() {
    }

    public QaSampling(String idSampling) {
        this.idSampling = idSampling;
    }

    public QaSampling(String idSampling, String samplingQty, Date dateSampling, String userCreate, int deleted) {
        this.idSampling = idSampling;
        this.samplingQty = samplingQty;
        this.dateSampling = dateSampling;
        this.userCreate = userCreate;
        this.deleted = deleted;
    }

    public String getIdSampling() {
        return idSampling;
    }

    public void setIdSampling(String idSampling) {
        this.idSampling = idSampling;
    }

    public String getSamplingQty() {
        return samplingQty;
    }

    public void setSamplingQty(String samplingQty) {
        this.samplingQty = samplingQty;
    }

    public Date getDateSampling() {
        return dateSampling;
    }

    public void setDateSampling(Date dateSampling) {
        this.dateSampling = dateSampling;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public PcsQty getIdPcsqty() {
        return idPcsqty;
    }

    public void setIdPcsqty(PcsQty idPcsqty) {
        this.idPcsqty = idPcsqty;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSampling != null ? idSampling.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof QaSampling)) {
            return false;
        }
        QaSampling other = (QaSampling) object;
        if ((this.idSampling == null && other.idSampling != null) || (this.idSampling != null && !this.idSampling.equals(other.idSampling))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.QaSampling[ idSampling=" + idSampling + " ]";
    }

}
