/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "pc2ng", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pc2ng.findAll", query = "SELECT p FROM Pc2ng p"),
    @NamedQuery(name = "Pc2ng.findByPc2ngId", query = "SELECT p FROM Pc2ng p WHERE p.pc2ngId = :pc2ngId"),
    @NamedQuery(name = "Pc2ng.findByCreateDate", query = "SELECT p FROM Pc2ng p WHERE p.createDate = :createDate"),
    @NamedQuery(name = "Pc2ng.findByUserCreate", query = "SELECT p FROM Pc2ng p WHERE p.userCreate = :userCreate"),
    @NamedQuery(name = "Pc2ng.findByUserApprove", query = "SELECT p FROM Pc2ng p WHERE p.userApprove = :userApprove"),
    @NamedQuery(name="Pc2ng.findByNgPool",query="SELECT p FROM Pc2ng p WHERE p.idNg =:idNg"),
    @NamedQuery(name="Pc2ng.findByNgPoolAndProcessPool",query="SELECT p FROM Pc2ng p WHERE p.idNg =:idNg AND p.idProc =:idProc AND p.deleted = 0")
})
public class Pc2ng implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "pc2ng_id", nullable = false, length = 36)
    private String pc2ngId;
    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "user_create", nullable = false, length = 36)
    private String userCreate;
    @Column(name = "user_approve", length = 36)
    private String userApprove;
    @JoinColumn(name = "id_proc", referencedColumnName = "id_proc", nullable = false)
    @ManyToOne(optional = false)
    private ProcessPool idProc;
    @JoinColumn(name = "id_ng", referencedColumnName = "id_ng", nullable = false)
    @ManyToOne(optional = false)
    private NgPool idNg;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    
    public Pc2ng() {
    }

    public Pc2ng(String pc2ngId) {
        this.pc2ngId = pc2ngId;
    }

    public Pc2ng(String pc2ngId, String userCreate) {
        this.pc2ngId = pc2ngId;
        this.userCreate = userCreate;
    }

    public String getPc2ngId() {
        return pc2ngId;
    }

    public void setPc2ngId(String pc2ngId) {
        this.pc2ngId = pc2ngId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserApprove() {
        return userApprove;
    }

    public void setUserApprove(String userApprove) {
        this.userApprove = userApprove;
    }

    public ProcessPool getIdProc() {
        return idProc;
    }

    public void setIdProc(ProcessPool idProc) {
        this.idProc = idProc;
    }

    public NgPool getIdNg() {
        return idNg;
    }

    public void setIdNg(NgPool idNg) {
        this.idNg = idNg;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pc2ngId != null ? pc2ngId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pc2ng)) {
            return false;
        }
        Pc2ng other = (Pc2ng) object;
        if ((this.pc2ngId == null && other.pc2ngId != null) || (this.pc2ngId != null && !this.pc2ngId.equals(other.pc2ngId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.Pc2ng[ pc2ngId=" + pc2ngId + " ]";
    }
    
}
