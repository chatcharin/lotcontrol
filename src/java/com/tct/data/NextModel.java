/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * Machine User
 *
 * @author pang Development by Chusak laojang Date : Sep 25, 2012, Time :
 * 6:59:33 PM Copy Right 4 Xtreme Co.,Ltd.
 */
@Entity
@Table(name = "next_model", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NextModel.findAll", query = "SELECT n FROM NextModel n"),
    @NamedQuery(name = "NextModel.findByIdNextModel", query = "SELECT n FROM NextModel n WHERE n.idNextModel = :idNextModel"),
    @NamedQuery(name = "NextModel.findByDate", query = "SELECT n FROM NextModel n WHERE n.dated = :dated"),
    @NamedQuery(name = "NextModel.findByModel", query = "SELECT n FROM NextModel n WHERE n.model = :model"),
    @NamedQuery(name = "NextModel.findByQty", query = "SELECT n FROM NextModel n WHERE n.qty = :qty"),
    @NamedQuery(name = "NextModel.findByDeleted", query = "SELECT n FROM NextModel n WHERE n.deleted = :deleted")})
public class NextModel implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_next_model", nullable = false, length = 36)
    private String idNextModel;
    @Column(name = "dated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dated;
    @Basic(optional = false)
    @Column(name = "model", nullable = false, length = 255)
    private String model;
    @Basic(optional = false)
    @Column(name = "qty", nullable = false, length = 255)
    private String qty;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    @JoinColumn(name = "id_line", referencedColumnName = "line_id", nullable = false)
    @ManyToOne(optional = false)
    private Line idLine;

    public NextModel() {
    }

    public NextModel(String idNextModel) {
        this.idNextModel = idNextModel;
    }

    public NextModel(String idNextModel, Date dated, String model, String qty, int deleted) {
        this.idNextModel = idNextModel;
        this.dated = dated;
        this.model = model;
        this.qty = qty;
        this.deleted = deleted;
    }

    public String getIdNextModel() {
        return idNextModel;
    }

    public void setIdNextModel(String idNextModel) {
        this.idNextModel = idNextModel;
    }

    public Date getDated() {
        return dated;
    }

    public void setDated(Date dated) {
        this.dated = dated;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public Line getIdLine() {
        return idLine;
    }

    public void setIdLine(Line idLine) {
        this.idLine = idLine;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNextModel != null ? idNextModel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NextModel)) {
            return false;
        }
        NextModel other = (NextModel) object;
        if ((this.idNextModel == null && other.idNextModel != null) || (this.idNextModel != null && !this.idNextModel.equals(other.idNextModel))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.NextModel[ idNextModel=" + idNextModel + " ]";
    }
}
