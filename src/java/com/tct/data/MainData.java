/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;  

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "main_data", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    //@NamedQuery(name = "MainData.findAll", query = "SELECT m FROM MainData m"),
    @NamedQuery(name = "MainData.findById", query = "SELECT m FROM MainData m WHERE m.id = :id"),
    @NamedQuery(name = "MainData.findByLotNumber", query = "SELECT m FROM MainData m WHERE m.lotNumber = :lotNumber"),

    @NamedQuery(name = "MainData.findByCurrentNumber", query = "SELECT m FROM MainData m WHERE m.currentNumber = :currentNumber"),
    @NamedQuery(name="MainData.findByIdProc",query="SELECT m FROM MainData m WHERE m.idProc = :idProc"),

    @NamedQuery(name = "MainData.findByLotControlId", query = "SELECT m FROM MainData m WHERE m.lotControlId = :lotControlId"),

     @NamedQuery(name = "MainData.findByLotControlIdAndMainDataId", query = "SELECT m FROM MainData m WHERE (m.lotControlId = :lotControlId) and (m.id =:id)"),
    @NamedQuery(name = "MainData.findByCurrentNumber", query = "SELECT m FROM MainData m WHERE m.currentNumber = :currentNumber"),
     @NamedQuery(name = "MainData.findByIdProcAndLotControlId", query = "SELECT m FROM MainData m WHERE (m.idProc = :idProc) and (m.lotControlId = :lotControlId)")
})


public class MainData implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Column(name = "lot_number", length = 255)
    private String lotNumber;
    @Column(name = "current_number", length = 36)
    private String currentNumber;
    @JoinColumn(name = "lot_control_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private LotControl lotControlId;
    @JoinColumn(name = "id_proc", referencedColumnName = "id_proc", nullable = false)
    @ManyToOne(optional = false)
    private ProcessPool idProc;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mainDataId")
    private Collection<CurrentProcess> currentProcessCollection;
    @Column(name = "remark", length = 255)
    private String remark;

    public MainData() {
    }

    public MainData(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLotNumber() {
        return lotNumber;
    }

    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }

    public String getCurrentNumber() {
        return currentNumber;
    }

    public void setCurrentNumber(String currentNumber) {
        this.currentNumber = currentNumber;
    }

    public LotControl getLotControlId() {
        return lotControlId;
    }

    public void setLotControlId(LotControl lotControlId) {
        this.lotControlId = lotControlId;
    }

    public ProcessPool getIdProc() {
        return idProc;
    }

    public void setIdProc(ProcessPool idProc) {
        this.idProc = idProc;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @XmlTransient
    public Collection<CurrentProcess> getCurrentProcessCollection() {
        return currentProcessCollection;
    }

    public void setCurrentProcessCollection(Collection<CurrentProcess> currentProcessCollection) {
        this.currentProcessCollection = currentProcessCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MainData)) {
            return false;
        }
        MainData other = (MainData) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.MainData[ id=" + id + " ]";
    }
    
}
