/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "refu01", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Refu01.findAll", query = "SELECT r FROM Refu01 r"),
    @NamedQuery(name = "Refu01.findById", query = "SELECT r FROM Refu01 r WHERE r.id = :id"),
    @NamedQuery(name = "Refu01.findByL2", query = "SELECT r FROM Refu01 r WHERE r.l2 = :l2"),
    @NamedQuery(name = "Refu01.findBydetailIdDetail", query = "SELECT r FROM Refu01 r WHERE r.detailIdDetail = :detailIdDetail"),
    @NamedQuery(name = "Refu01.findByL1", query = "SELECT r FROM Refu01 r WHERE r.l1 = :l1")})
public class Refu01 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Column(name = "l2", length = 100)
    private String l2;
    @Column(name = "l1", length = 100)
    private String l1;
    @Column(name = "stamp", length = 255)
    private String stamp;
    @JoinColumn(name = "detail_id_detail", referencedColumnName = "id_detail", nullable = false)
    @ManyToOne(optional = false)
    private Details detailIdDetail;
    @Column(name = "type", length = 255)
    private String type;

    public Refu01() {
    }

    public Refu01(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getL2() {
        return l2;
    }

    public void setL2(String l2) {
        this.l2 = l2;
    }

    public String getL1() {
        return l1;
    }

    public void setL1(String l1) {
        this.l1 = l1;
    }

    public Details getDetailIdDetail() {
        return detailIdDetail;
    }

    public void setDetailIdDetail(Details detailIdDetail) {
        this.detailIdDetail = detailIdDetail;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Refu01)) {
            return false;
        }
        Refu01 other = (Refu01) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.Refu01[ id=" + id + " ]";
    }

    public String getStamp() {
        return stamp;
    }

    public void setStamp(String stamp) {
        this.stamp = stamp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}
