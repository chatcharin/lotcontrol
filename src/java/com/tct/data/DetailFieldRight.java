/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 19, 2012, Time : 2:57:01 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
@Entity
@Table(name = "detail_field_right", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailFieldRight.findAll", query = "SELECT d FROM DetailFieldRight d"),
    @NamedQuery(name = "DetailFieldRight.findById", query = "SELECT d FROM DetailFieldRight d WHERE d.id = :id"),
    @NamedQuery(name = "DetailFieldRight.findByRequest", query = "SELECT d FROM DetailFieldRight d WHERE d.request = :request"),
    @NamedQuery(name = "DetailFieldRight.findByReport", query = "SELECT d FROM DetailFieldRight d WHERE d.report = :report"),
    @NamedQuery(name = "DetailFieldRight.findByDeleted", query = "SELECT d FROM DetailFieldRight d WHERE d.deleted = :deleted")})
public class DetailFieldRight implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Basic(optional = false)
    @Column(name = "request", nullable = false)
    private int request;
    @Basic(optional = false)
    @Column(name = "report", nullable = false)
    private int report;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    @JoinColumn(name = "id_detail_specail", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private DetailSpecial idDetailSpecail;
    @JoinColumn(name = "id_field_name", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private FieldName idFieldName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDetailRight")
    private Collection<DetailFieldRightDate> detailFieldRightDateCollection;
    public DetailFieldRight() {
    }

    public DetailFieldRight(String id) {
        this.id = id;
    }

    public DetailFieldRight(String id, int request, int report, int deleted) {
        this.id = id;
        this.request = request;
        this.report = report;
        this.deleted = deleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getRequest() {
        return request;
    }

    public void setRequest(int request) {
        this.request = request;
    }

    public int getReport() {
        return report;
    }

    public void setReport(int report) {
        this.report = report;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public DetailSpecial getIdDetailSpecail() {
        return idDetailSpecail;
    }

    public void setIdDetailSpecail(DetailSpecial idDetailSpecail) {
        this.idDetailSpecail = idDetailSpecail;
    }

    public FieldName getIdFieldName() {
        return idFieldName;
    }

    public void setIdFieldName(FieldName idFieldName) {
        this.idFieldName = idFieldName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailFieldRight)) {
            return false;
        }
        DetailFieldRight other = (DetailFieldRight) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.DetailFieldRight[ id=" + id + " ]";
    }
    @XmlTransient
    public Collection<DetailFieldRightDate> getDetailFieldRightDateCollection() {
        return detailFieldRightDateCollection;
    }

    public void setDetailFieldRightDateCollection(Collection<DetailFieldRightDate> detailFieldRightDateCollection) {
        this.detailFieldRightDateCollection = detailFieldRightDateCollection;
    }
}
