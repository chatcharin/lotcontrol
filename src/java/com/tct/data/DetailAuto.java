/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "detail_auto", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailAuto.findAll", query = "SELECT d FROM DetailAuto d"),
    @NamedQuery(name = "DetailAuto.findById", query = "SELECT d FROM DetailAuto d WHERE d.id = :id"),
    @NamedQuery(name = "DetailAuto.findByUser", query = "SELECT d FROM DetailAuto d WHERE d.userCreate = :userCreate"),
    @NamedQuery(name = "DetailAuto.findByRemark", query = "SELECT d FROM DetailAuto d WHERE d.remark = :remark"),
    @NamedQuery(name = "DetailAuto.findBydetailIdDetail", query = "SELECT d FROM DetailAuto d WHERE d.detailIdDetail = :detailIdDetail"),
    @NamedQuery(name = "DetailAuto.findByLotcontrolassy", query = "SELECT d FROM DetailAuto d WHERE d.lotcontrolassy = :lotcontrolassy")})
public class DetailAuto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 45)
    private String id;
    @Column(name = "user_create", length = 36)
    private String userCreate;
    @Column(name = "remark", length = 255)
    private String remark;
    @Column(name = "lotcontrolassy", length = 100)
    private String lotcontrolassy;
    @Basic(optional = false)
    @Column(name = "stamp", nullable = false, length = 255)
    private String stamp;
    @Basic(optional = false)
    @Column(name = "dr_core", nullable = false, length = 255)
    private String drCore;
    @Column(name = "pastcode", length = 255)
    private String pastcode;
    @Column(name = "invoice_copper_wire", length = 255)
    private String invoiceCopperWire;
    @Basic(optional = false)
    @Column(name = "product_code_name", nullable = false, length = 255)
    private String productCodeName;
    @Basic(optional = false)
    @Column(name = "product_code", nullable = false, length = 255)
    private String productCode;
    @Basic(optional = false)
    @Column(name = "product_type", nullable = false, length = 255)
    private String productType;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    @JoinColumn(name = "detail_id_detail", referencedColumnName = "id_detail", nullable = false)
    @ManyToOne(optional = false)
    private Details detailIdDetail;

    public DetailAuto() {
    }

    public DetailAuto(String id) {
        this.id = id;
    }

    public DetailAuto(String id, String stamp, String drCore, String productCodeName, String productCode, String productType, int deleted) {
        this.id = id;
        this.stamp = stamp;
        this.drCore = drCore;
        this.productCodeName = productCodeName;
        this.productCode = productCode;
        this.productType = productType;
        this.deleted = deleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getLotcontrolassy() {
        return lotcontrolassy;
    }

    public void setLotcontrolassy(String lotcontrolassy) {
        this.lotcontrolassy = lotcontrolassy;
    }

    public String getStamp() {
        return stamp;
    }

    public void setStamp(String stamp) {
        this.stamp = stamp;
    }

    public String getDrCore() {
        return drCore;
    }

    public void setDrCore(String drCore) {
        this.drCore = drCore;
    }

    public String getPastcode() {
        return pastcode;
    }

    public void setPastcode(String pastcode) {
        this.pastcode = pastcode;
    }

    public String getInvoiceCopperWire() {
        return invoiceCopperWire;
    }

    public void setInvoiceCopperWire(String invoiceCopperWire) {
        this.invoiceCopperWire = invoiceCopperWire;
    }

    public String getProductCodeName() {
        return productCodeName;
    }

    public void setProductCodeName(String productCodeName) {
        this.productCodeName = productCodeName;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public Details getDetailIdDetail() {
        return detailIdDetail;
    }

    public void setDetailIdDetail(Details detailIdDetail) {
        this.detailIdDetail = detailIdDetail;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailAuto)) {
            return false;
        }
        DetailAuto other = (DetailAuto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.DetailAuto[ id=" + id + " ]";
    }
    
}
