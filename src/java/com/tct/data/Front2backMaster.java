/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author TheBoyCs
 */
@Entity
@Table(name = "front2back_master", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Front2backMaster.findAll", query = "SELECT f FROM Front2backMaster f"),
    @NamedQuery(name = "Front2backMaster.findById", query = "SELECT f FROM Front2backMaster f WHERE f.id = :id"),
    @NamedQuery(name = "Front2backMaster.findByPdQty", query = "SELECT f FROM Front2backMaster f WHERE f.pdQty = :pdQty"),
    @NamedQuery(name = "Front2backMaster.findByDeleted", query = "SELECT f FROM Front2backMaster f WHERE f.deleted = :deleted"),
    @NamedQuery(name = "Front2backMaster.findByDateCreate", query = "SELECT f FROM Front2backMaster f WHERE f.dateCreate = :dateCreate"),
    @NamedQuery(name = "Front2backMaster.findByUserCreate", query = "SELECT f FROM Front2backMaster f WHERE f.userCreate = :userCreate")})
public class Front2backMaster implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Basic(optional = false)
    @Column(name = "pd_qty", nullable = false, length = 255)
    private String pdQty;
    @Column(name = "deleted")
    private Integer deleted;
    @Column(name = "date_create")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    @Column(name = "user_create", length = 36)
    private String userCreate;
    @JoinColumn(name = "details_id", referencedColumnName = "id_detail", nullable = false)
    @ManyToOne(optional = false)
    private Details detailsId;
    @JoinColumn(name = "lotcontrol_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private LotControl lotcontrolId;

    public Front2backMaster() {
    }

    public Front2backMaster(String id) {
        this.id = id;
    }

    public Front2backMaster(String id, String pdQty) {
        this.id = id;
        this.pdQty = pdQty;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPdQty() {
        return pdQty;
    }

    public void setPdQty(String pdQty) {
        this.pdQty = pdQty;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public Details getDetailsId() {
        return detailsId;
    }

    public void setDetailsId(Details detailsId) {
        this.detailsId = detailsId;
    }

    public LotControl getLotcontrolId() {
        return lotcontrolId;
    }

    public void setLotcontrolId(LotControl lotcontrolId) {
        this.lotcontrolId = lotcontrolId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Front2backMaster)) {
            return false;
        }
        Front2backMaster other = (Front2backMaster) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.Front2backMaster[ id=" + id + " ]";
    }
    
}
