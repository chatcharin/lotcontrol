/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.data;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Oct 18, 2012, Time : 6:52:02 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
@Entity
@Table(name = "report_setting", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReportSetting.findAll", query = "SELECT r FROM ReportSetting r"),
    @NamedQuery(name = "ReportSetting.findByIdReportSetting", query = "SELECT r FROM ReportSetting r WHERE r.idReportSetting = :idReportSetting"),
    @NamedQuery(name = "ReportSetting.findByNameSetting", query = "SELECT r FROM ReportSetting r WHERE r.nameSetting = :nameSetting"),
    @NamedQuery(name = "ReportSetting.findByDescriptionSetting", query = "SELECT r FROM ReportSetting r WHERE r.descriptionSetting = :descriptionSetting"),
    @NamedQuery(name = "ReportSetting.findByDeleted", query = "SELECT r FROM ReportSetting r WHERE r.deleted = :deleted")})
public class ReportSetting implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_report_setting", nullable = false, length = 36)
    private String idReportSetting;
    @Basic(optional = false)
    @Column(name = "name_setting", nullable = false, length = 255)
    private String nameSetting;
    @Basic(optional = false)
    @Column(name = "description_setting", nullable = false, length = 255)
    private String descriptionSetting;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idReportSetting")
    private Collection<ReportSettingAttribute> reportSettingAttributeCollection;

    public ReportSetting() {
    }

    public ReportSetting(String idReportSetting) {
        this.idReportSetting = idReportSetting;
    }

    public ReportSetting(String idReportSetting, String nameSetting, String descriptionSetting, int deleted) {
        this.idReportSetting = idReportSetting;
        this.nameSetting = nameSetting;
        this.descriptionSetting = descriptionSetting;
        this.deleted = deleted;
    }

    public String getIdReportSetting() {
        return idReportSetting;
    }

    public void setIdReportSetting(String idReportSetting) {
        this.idReportSetting = idReportSetting;
    }

    public String getNameSetting() {
        return nameSetting;
    }

    public void setNameSetting(String nameSetting) {
        this.nameSetting = nameSetting;
    }

    public String getDescriptionSetting() {
        return descriptionSetting;
    }

    public void setDescriptionSetting(String descriptionSetting) {
        this.descriptionSetting = descriptionSetting;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    @XmlTransient
    public Collection<ReportSettingAttribute> getReportSettingAttributeCollection() {
        return reportSettingAttributeCollection;
    }

    public void setReportSettingAttributeCollection(Collection<ReportSettingAttribute> reportSettingAttributeCollection) {
        this.reportSettingAttributeCollection = reportSettingAttributeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idReportSetting != null ? idReportSetting.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReportSetting)) {
            return false;
        }
        ReportSetting other = (ReportSetting) object;
        if ((this.idReportSetting == null && other.idReportSetting != null) || (this.idReportSetting != null && !this.idReportSetting.equals(other.idReportSetting))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.ReportSetting[ idReportSetting=" + idReportSetting + " ]";
    }

}
