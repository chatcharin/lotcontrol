/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "detail_packing", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailPacking.findAll", query = "SELECT d FROM DetailPacking d"),
    @NamedQuery(name = "DetailPacking.findById", query = "SELECT d FROM DetailPacking d WHERE d.id = :id"),
    @NamedQuery(name = "DetailPacking.findByStamp", query = "SELECT d FROM DetailPacking d WHERE d.stamp = :stamp"),
    @NamedQuery(name = "DetailPacking.findByPdCodeName", query = "SELECT d FROM DetailPacking d WHERE d.pdCodeName = :pdCodeName"),
    @NamedQuery(name = "DetailPacking.findByPdCode", query = "SELECT d FROM DetailPacking d WHERE d.pdCode = :pdCode"),
    @NamedQuery(name = "DetailPacking.findBydetailsIdDetail", query = "SELECT d FROM DetailPacking d WHERE d.detailsIdDetail = :detailsIdDetail"),
    @NamedQuery(name = "DetailPacking.findByPdType", query = "SELECT d FROM DetailPacking d WHERE d.pdType = :pdType")})
public class DetailPacking implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Basic(optional = false)
    @Column(name = "stamp", nullable = false, length = 255)
    private String stamp;
    @Basic(optional = false)
    @Column(name = "pd_code_name", nullable = false, length = 255)
    private String pdCodeName;
    @Basic(optional = false)
    @Column(name = "pd_code", nullable = false, length = 255)
    private String pdCode;
    @Basic(optional = false)
    @Column(name = "pd_type", nullable = false, length = 255)
    private String pdType;
    @JoinColumn(name = "details_id_detail", referencedColumnName = "id_detail", nullable = false)
    @ManyToOne(optional = false)
    private Details detailsIdDetail;
    @Basic(optional = false)
    @Column(name="deleted",nullable = false)
    private int deleted;

    public DetailPacking() {
    }

    public DetailPacking(String id) {
        this.id = id;
    }

    public DetailPacking(String id, String stamp, String pdCodeName, String pdCode, String pdType) {
        this.id = id;
        this.stamp = stamp;
        this.pdCodeName = pdCodeName;
        this.pdCode = pdCode;
        this.pdType = pdType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStamp() {
        return stamp;
    }

    public void setStamp(String stamp) {
        this.stamp = stamp;
    }

    public String getPdCodeName() {
        return pdCodeName;
    }

    public void setPdCodeName(String pdCodeName) {
        this.pdCodeName = pdCodeName;
    }

    public String getPdCode() {
        return pdCode;
    }

    public void setPdCode(String pdCode) {
        this.pdCode = pdCode;
    }

    public String getPdType() {
        return pdType;
    }

    public void setPdType(String pdType) {
        this.pdType = pdType;
    }

    public Details getDetailsIdDetail() {
        return detailsIdDetail;
    }

    public void setDetailsIdDetail(Details detailsIdDetail) {
        this.detailsIdDetail = detailsIdDetail;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailPacking)) {
            return false;
        }
        DetailPacking other = (DetailPacking) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.DetailPacking[ id=" + id + " ]";
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }
    
}
