/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "model_pool", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ModelPool.findAll", query = "SELECT m FROM ModelPool m WHERE m.deleted = 0"),
    @NamedQuery(name = "ModelPool.findById", query = "SELECT m FROM ModelPool m WHERE (m.deleted = 0) and (m.id = :id)"),
    @NamedQuery(name = "ModelPool.findByModelGroup", query = "SELECT m FROM ModelPool m WHERE (m.deleted = 0) and (m.modelGroup = :modelGroup)"),
    @NamedQuery(name = "ModelPool.findByModelType", query = "SELECT m FROM ModelPool m WHERE (m.deleted = 0) and (m.modelType = :modelType)"),
    @NamedQuery(name = "ModelPool.findByModelSeries", query = "SELECT m FROM ModelPool m WHERE (m.deleted = 0) and (m.modelSeries = :modelSeries)"),
    @NamedQuery(name = "ModelPool.findByModelName", query = "SELECT m FROM ModelPool m WHERE (m.deleted = 0) and (m.modelName LIKE :modelName) "),
    @NamedQuery(name = "ModelPool.findByCreateDate", query = "SELECT m FROM ModelPool m WHERE (m.deleted = 0) and (m.createDate = :createDate)"),
    @NamedQuery(name = "ModelPool.findByUserCreate", query = "SELECT m FROM ModelPool m WHERE (m.deleted = 0) and (m.userCreate = :userCreate)"),
    @NamedQuery(name = "ModelPool.findByUserApprove", query = "SELECT m FROM ModelPool m WHERE (m.deleted = 0) and (m.userApprove = :userApprove)"),
    @NamedQuery(name = "ModelPool.findByPlat", query = "SELECT m FROM ModelPool m WHERE (m.deleted = 0) and (m.plat = :plat)"),
    @NamedQuery(name = "ModelPool.findGroupByName", query = "SELECT m FROM ModelPool m WHERE m.deleted = 0 GROUP BY m.modelName order by m.modelName asc"),
    @NamedQuery(name = "ModelPool.findByNameAndSeries", query = "SELECT m FROM ModelPool m WHERE (m.deleted = 0) and (m.modelName =:modelName) AND (m.modelSeries =:modelSeries)"),
    @NamedQuery(name = "ModelPool.findGroupBySeries", query = "SELECT distinct m.modelSeries FROM ModelPool m WHERE m.deleted = 0 order by m.modelSeries asc"),
    @NamedQuery(name = "ModelPool.findByNotDeleted", query = "SELECT m FROM ModelPool m WHERE m.deleted = 0"),
    @NamedQuery(name = "ModelPool.findGroupByGroup", query = "SELECT DISTINCT m.modelGroup FROM ModelPool m WHERE m.deleted = 0 ORDER BY m.modelGroup ASC"),
    @NamedQuery(name = "ModelPool.findByStatus", query = "SELECT m FROM ModelPool m WHERE (m.deleted = 0) and (m.status =:status)"),
    @NamedQuery(name = "ModelPool.findByTypeFlBl", query = "SELECT m FROM ModelPool m WHERE m.typeFlbl = :typeFlbl AND m.deleted = 0")
})
public class ModelPool implements Serializable {

    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Column(name = "model_group", length = 100)
    private String modelGroup;
    @Column(name = "model_type", length = 100)
    private String modelType;
    @Column(name = "model_series", length = 100)
    private String modelSeries;
    @Column(name = "model_name", length = 100)
    private String modelName;
    @Basic(optional = false)
    @Column(name = "user_create", nullable = false, length = 36)
    private String userCreate;
    @Column(name = "user_approve", length = 36)
    private String userApprove;
    @Column(name = "plat", length = 100)
    private String plat;
    @Column(name = "cost_model", length = 100)
    private String costModel;
    @Column(name = "status", nullable = false, length = 255)
    private String status;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    @Column(name = "type_fl_bl", nullable = false, length = 255)
    private String typeFlbl;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "modelPoolId")
    private Collection<Md2pc> md2pcCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "modelPoolId")
    private Collection<LotControl> lotControlCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idModelPool")
    private Collection<Target> targetCollection;
    @Column(name = "std_time", nullable = false, length = 255)
    private String stdTime;
    @Column(name = "price_yen", nullable = false, length = 255)
    private String priceYen;
    @Column(name = "repair_std_time", nullable = false, length = 255)
    private String repairStdTime; 
    public ModelPool() {
    }

    public ModelPool(String id) {
        this.id = id;
    }

    public ModelPool(String id, String userCreate) {
        this.id = id;
        this.userCreate = userCreate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModelGroup() {
        return modelGroup;
    }

    public void setModelGroup(String modelGroup) {
        this.modelGroup = modelGroup;
    }

    public String getModelType() {
        return modelType;
    }

    public void setModelType(String modelType) {
        this.modelType = modelType;
    }

    public String getModelSeries() {
        return modelSeries;
    }

    public void setModelSeries(String modelSeries) {
        this.modelSeries = modelSeries;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserApprove() {
        return userApprove;
    }

    public void setUserApprove(String userApprove) {
        this.userApprove = userApprove;
    }

    public String getPlat() {
        return plat;
    }

    public void setPlat(String plat) {
        this.plat = plat;
    }

    public String getCostModel() {
        return costModel;
    }

    public void setCostModel(String costModel) {
        this.costModel = costModel;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTypeFlbl() {
        return typeFlbl;
    }

    public void setTypeFlbl(String typeFlbl) {
        this.typeFlbl = typeFlbl;
    }

    @XmlTransient
    public Collection<Md2pc> getMd2pcCollection() {
        return md2pcCollection;
    }

    public void setMd2pcCollection(Collection<Md2pc> md2pcCollection) {
        this.md2pcCollection = md2pcCollection;
    }

    @XmlTransient
    public Collection<LotControl> getLotControlCollection() {
        return lotControlCollection;
    }

    public void setLotControlCollection(Collection<LotControl> lotControlCollection) {
        this.lotControlCollection = lotControlCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ModelPool)) {
            return false;
        }
        ModelPool other = (ModelPool) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.ModelPool[ id=" + id + " ]";
    }

    @XmlTransient
    public Collection<Target> getTargetCollection() {
        return targetCollection;
    }

    public void setTargetCollection(Collection<Target> targetCollection) {
        this.targetCollection = targetCollection;
    }

    public String getPriceYen() {
        return priceYen;
    }

    public void setPriceYen(String priceYen) {
        this.priceYen = priceYen;
    }

    public String getRepairStdTime() {
        return repairStdTime;
    }

    public void setRepairStdTime(String repairStdTime) {
        this.repairStdTime = repairStdTime;
    }

    public String getStdTime() {
        return stdTime;
    }

    public void setStdTime(String stdTime) {
        this.stdTime = stdTime;
    } 
}
