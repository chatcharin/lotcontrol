/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "lot_control", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LotControl.findAll", query = "SELECT l FROM LotControl l"),
    @NamedQuery(name = "LotControl.findById", query = "SELECT l FROM LotControl l WHERE l.id = :id"),
    @NamedQuery(name = "LotControl.findByBarcode", query = "SELECT l FROM LotControl l WHERE l.barcode = :barcode AND l.deleted=0"),
    @NamedQuery(name = "LotControl.findByLotNumber", query = "SELECT l FROM LotControl l WHERE l.lotNumber = :lotNumber AND l.deleted = 0"),
    @NamedQuery(name = "LotControl.findByMd2pcIdModel", query = "SELECT l FROM LotControl l WHERE l.md2pcIdModel = :md2pcIdModel"),
    @NamedQuery(name = "LotControl.findByMainDataId", query = "SELECT l FROM LotControl l WHERE l.mainDataId = :mainDataId"),
    @NamedQuery(name = "LotControl.findByDetailsId", query = "SELECT l FROM LotControl l WHERE l.detailsIdDetail = :detailsIdDetail AND l.deleted=0 ORDER BY l.barcode ASC"),
    @NamedQuery(name = "LotControl.findByBarcodeSearch", query = "SELECT l FROM LotControl l WHERE l.barcode LIKE :barcode AND l.deleted=0 ORDER BY l.barcode ASC"),
    @NamedQuery(name = "LotControl.findByDetailsOrderNumber", query = "SELECT l FROM LotControl l WHERE l.detailsIdDetail = :detailsIdDetail order by l.lotNumber ASC")
})
public class LotControl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Column(name = "barcode", length = 255)
    private String barcode;
    @Column(name = "lot_number", length = 255)
    private String lotNumber;
    @Basic(optional = false)
    @Column(name = "md2pc_id_model", length = 36)
    private String md2pcIdModel;
    @Column(name = "main_data_id", length = 36)
    private String mainDataId;
    @JoinColumn(name = "model_pool_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private ModelPool modelPoolId;
    @JoinColumn(name = "details_id_detail", referencedColumnName = "id_detail", nullable = false)
    @ManyToOne(optional = false)
    private Details detailsIdDetail;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lotcontrolId")
    private Collection<Front2back> front2backCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lotcontrolId")
    private Collection<Back2pack> back2packCollection;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    @Basic(optional = false)
    @Column(name = "status_approve", nullable = false)
    private int statusApprove;
    @Column(name = "user_approve", length = 36)
    private String userApprove;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lotcontrolId")
    private Collection<Assy2auto> assy2autoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lotControlId")
    private Collection<MainData> mainDataCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lotcontrolId")
    private Collection<Front2backMaster> front2backMasterCollection;
    @Basic(optional = false)
    @Column(name = "wip_qty", nullable = false)
    private int wipQty;
    @Basic(optional = false)
    @Column(name = "qty_output", nullable = false)
    private int qtyOutput;

    public LotControl() {
    }

    public LotControl(String id) {
        this.id = id;
    }

    public LotControl(String id, String md2pcIdModel) {
        this.id = id;
        this.md2pcIdModel = md2pcIdModel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getLotNumber() {
        return lotNumber;
    }

    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }

    public String getMd2pcIdModel() {
        return md2pcIdModel;
    }

    public void setMd2pcIdModel(String md2pcIdModel) {
        this.md2pcIdModel = md2pcIdModel;
    }

    public String getMainDataId() {
        return mainDataId;
    }

    public void setMainDataId(String mainDataId) {
        this.mainDataId = mainDataId;
    }

    public ModelPool getModelPoolId() {
        return modelPoolId;
    }

    public void setModelPoolId(ModelPool modelPoolId) {
        this.modelPoolId = modelPoolId;
    }

    public Details getDetailsIdDetail() {
        return detailsIdDetail;
    }

    public void setDetailsIdDetail(Details detailsIdDetail) {
        this.detailsIdDetail = detailsIdDetail;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LotControl)) {
            return false;
        }
        LotControl other = (LotControl) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.LotControl[ id=" + id + " ]";
    }

    @XmlTransient
    public Collection<Back2pack> getBack2packCollection() {
        return back2packCollection;
    }

    public void setBack2packCollection(Collection<Back2pack> back2packCollection) {
        this.back2packCollection = back2packCollection;
    }

    @XmlTransient
    public Collection<Front2back> getFront2backCollection() {
        return front2backCollection;
    }

    @XmlTransient
    public Collection<Front2backMaster> getFront2backMasterCollection() {
        return front2backMasterCollection;
    }

    public void setFront2backMasterCollection(Collection<Front2backMaster> front2backMasterCollection) {
        this.front2backMasterCollection = front2backMasterCollection;
    }
    
    public void setFront2backCollection(Collection<Front2back> front2backCollection) {
        this.front2backCollection = front2backCollection;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public int getStatusApprove() {
        return statusApprove;
    }

    public void setStatusApprove(int statusApprove) {
        this.statusApprove = statusApprove;
    }

    public String getUserApprove() {
        return userApprove;
    }

    public void setUserApprove(String userApprove) {
        this.userApprove = userApprove;
    }
    
    @XmlTransient
    public Collection<Assy2auto> getAssy2autoCollection() {
        return assy2autoCollection;
    }

    public void setAssy2autoCollection(Collection<Assy2auto> assy2autoCollection) {
        this.assy2autoCollection = assy2autoCollection;
    }
    
    @XmlTransient
    public Collection<MainData> getMainDataCollection() {
        return mainDataCollection;
    }

    public void setMainDataCollection(Collection<MainData> mainDataCollection) {
        this.mainDataCollection = mainDataCollection;
    }
    
   

    public int getQtyOutput() {
        return qtyOutput;
    }

    public void setQtyOutput(int qtyOutput) {
        this.qtyOutput = qtyOutput;
    }

    public int getWipQty() {
        return wipQty;
    }

    public void setWipQty(int wipQty) {
        this.wipQty = wipQty;
    }
    
   

}
