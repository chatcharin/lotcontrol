/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data; 

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "current_process", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CurrentProcess.findAll", query = "SELECT c FROM CurrentProcess c"),
    @NamedQuery(name = "CurrentProcess.findById", query = "SELECT c FROM CurrentProcess c WHERE c.id = :id"),
    @NamedQuery(name = "CurrentProcess.findByCurrentProc", query = "SELECT c FROM CurrentProcess c WHERE c.currentProc = :currentProc"),
    @NamedQuery(name = "CurrentProcess.findByStatus", query = "SELECT c FROM CurrentProcess c WHERE c.status = :status"),
     @NamedQuery(name = "CurrentProcess.findBymainDataId", query = "SELECT c FROM CurrentProcess c WHERE c.mainDataId = :mainDataId"),
    @NamedQuery(name = "CurrentProcess.findBymainDataIdAndStatus", query = "SELECT c FROM CurrentProcess c WHERE (c.mainDataId = :mainDataId) and (c.status =:status)"), 
    @NamedQuery(name = "CurrentProcess.findByTime", query = "SELECT c FROM CurrentProcess c WHERE c.timeCurrent = :timeCurrent")})
public class CurrentProcess implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Column(name = "current_proc", length = 255)
    private String currentProc;
    @Column(name = "status", length = 255)
    private String status;
    @Column(name = "time_current")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeCurrent;
    @Column(name = "shift", length = 255)
    private String shift;
    @Column(name = "line", length = 255)
    private String line;
    @Column(name = "hr", length = 255)
    private String hr;
    @Column(name = "remark", length = 255)
    private String remark;  
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "currentProcessId")
    private Collection<NgNormal> ngNormalCollection;
    @JoinColumn(name = "main_data_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private MainData mainDataId;
    @JoinColumn(name = "mc_id", referencedColumnName = "id")
    @ManyToOne
    private Mc mcId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "currentProcessId")
    private Collection<NgRepair> ngRepairCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "currentProcessId")
    private Collection<McDown> mcDownCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "currentProcessId")
    private Collection<PcsQty> pcsQtyCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "currentProcessId")
    private Collection<Hold> holdCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "currentProcessId")
    private Collection<McMove> mcMoveCollection;
    @Column(name = "first_open", nullable = false)
    private int firstOpen;
     public CurrentProcess() {
    }

    public CurrentProcess(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCurrentProc() {
        return currentProc;
    }

    public void setCurrentProc(String currentProc) {
        this.currentProc = currentProc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getTimeCurrent() {
        return timeCurrent;
    }

    public void setTimeCurrent(Date timeCurrent) {
        this.timeCurrent = timeCurrent;
    } 
    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getHr() {
        return hr;
    }

    public void setHr(String hr) {
        this.hr = hr;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    } 
    @XmlTransient
    public Collection<NgNormal> getNgNormalCollection() {
        return ngNormalCollection;
    }

    public void setNgNormalCollection(Collection<NgNormal> ngNormalCollection) {
        this.ngNormalCollection = ngNormalCollection;
    }

    public MainData getMainDataId() {
        return mainDataId;
    }

    public void setMainDataId(MainData mainDataId) {
        this.mainDataId = mainDataId;
    }

    public Mc getMcId() {
        return mcId;
    }

    public void setMcId(Mc mcId) {
        this.mcId = mcId;
    }

    @XmlTransient
    public Collection<NgRepair> getNgRepairCollection() {
        return ngRepairCollection;
    }

    public void setNgRepairCollection(Collection<NgRepair> ngRepairCollection) {
        this.ngRepairCollection = ngRepairCollection;
    }

    @XmlTransient
    public Collection<McDown> getMcDownCollection() {
        return mcDownCollection;
    }

    public void setMcDownCollection(Collection<McDown> mcDownCollection) {
        this.mcDownCollection = mcDownCollection;
    }

    @XmlTransient
    public Collection<PcsQty> getPcsQtyCollection() {
        return pcsQtyCollection;
    }

    public void setPcsQtyCollection(Collection<PcsQty> pcsQtyCollection) {
        this.pcsQtyCollection = pcsQtyCollection;
    }

    @XmlTransient
    public Collection<Hold> getHoldCollection() {
        return holdCollection;
    }

    public void setHoldCollection(Collection<Hold> holdCollection) {
        this.holdCollection = holdCollection;
    }

    @XmlTransient
    public Collection<McMove> getMcMoveCollection() {
        return mcMoveCollection;
    }

    public void setMcMoveCollection(Collection<McMove> mcMoveCollection) {
        this.mcMoveCollection = mcMoveCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    } 
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CurrentProcess)) {
            return false;
        }
        CurrentProcess other = (CurrentProcess) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    } 
    @Override
    public String toString() {
        return "com.tct.data.CurrentProcess[ id=" + id + " ]";
    } 
    public int getFirstOpen() {
        return firstOpen;
    }

    public void setFirstOpen(int firstOpen) {
        this.firstOpen = firstOpen;
    } 
}
