/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "front2back", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Front2back.findAll", query = "SELECT f FROM Front2back f"),
    @NamedQuery(name = "Front2back.findByFront2backId", query = "SELECT f FROM Front2back f WHERE f.front2backId = :front2backId")})
public class Front2back implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "front2back_id", nullable = false, length = 36)
    private String front2backId;
    @JoinColumn(name = "details_id_detail", referencedColumnName = "id_detail", nullable = false)
    @ManyToOne(optional = false)
    private Details detailsIdDetail;
    @JoinColumn(name = "lotcontrol_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private LotControl lotcontrolId;

    public Front2back() {
    }

    public Front2back(String front2backId) {
        this.front2backId = front2backId;
    }

    public String getFront2backId() {
        return front2backId;
    }

    public void setFront2backId(String front2backId) {
        this.front2backId = front2backId;
    }

    public Details getDetailsIdDetail() {
        return detailsIdDetail;
    }

    public void setDetailsIdDetail(Details detailsIdDetail) {
        this.detailsIdDetail = detailsIdDetail;
    }

    public LotControl getLotcontrolId() {
        return lotcontrolId;
    }

    public void setLotcontrolId(LotControl lotcontrolId) {
        this.lotcontrolId = lotcontrolId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (front2backId != null ? front2backId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Front2back)) {
            return false;
        }
        Front2back other = (Front2back) object;
        if ((this.front2backId == null && other.front2backId != null) || (this.front2backId != null && !this.front2backId.equals(other.front2backId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.Front2back[ front2backId=" + front2backId + " ]";
    }
    
}
