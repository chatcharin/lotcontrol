/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author tawat
 */
@Entity
@Table(name = "manual_target", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ManualTarget.findAll", query = "SELECT m FROM ManualTarget m"),
    @NamedQuery(name = "ManualTarget.findByIdManaulTarget", query = "SELECT m FROM ManualTarget m WHERE m.idManaulTarget = :idManaulTarget"),
    @NamedQuery(name = "ManualTarget.findByDefect", query = "SELECT m FROM ManualTarget m WHERE m.defect = :defect"),
    @NamedQuery(name = "ManualTarget.findByDefectRate", query = "SELECT m FROM ManualTarget m WHERE m.defectRate = :defectRate"),
    @NamedQuery(name = "ManualTarget.findByDeleted", query = "SELECT m FROM ManualTarget m WHERE m.deleted = :deleted"),
    @NamedQuery(name = "ManualTarget.findByQty", query = "SELECT m FROM ManualTarget m WHERE m.qty = :qty"),
    @NamedQuery(name = "ManualTarget.findByResult", query = "SELECT m FROM ManualTarget m WHERE m.result = :result"),
    @NamedQuery(name = "ManualTarget.findByTime", query = "SELECT m FROM ManualTarget m WHERE m.time = :time")})
public class ManualTarget implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_manaul_target", nullable = false, length = 36)
    private String idManaulTarget;
    @Basic(optional = false)
    @Column(name = "defect", nullable = false, length = 255)
    private String defect;
    @Basic(optional = false)
    @Column(name = "defect_rate", nullable = false, length = 255)
    private String defectRate;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    @Basic(optional = false)
    @Column(name = "qty", nullable = false, length = 255)
    private String qty;
    @Basic(optional = false)
    @Column(name = "result", nullable = false, length = 255)
    private String result;
    @Basic(optional = false)
    @Column(name = "time", nullable = false, length = 100)
    private String time;
    @JoinColumn(name = "id_line", referencedColumnName = "line_id", nullable = false)
    @ManyToOne(optional = false)
    private Line idLine;

    public ManualTarget() {
    }

    public ManualTarget(String idManaulTarget) {
        this.idManaulTarget = idManaulTarget;
    }

    public ManualTarget(String idManaulTarget, String defect, String defectRate, int deleted, String qty, String result, String time) {
        this.idManaulTarget = idManaulTarget;
        this.defect = defect;
        this.defectRate = defectRate;
        this.deleted = deleted;
        this.qty = qty;
        this.result = result;
        this.time = time;
    }

    public String getIdManaulTarget() {
        return idManaulTarget;
    }

    public void setIdManaulTarget(String idManaulTarget) {
        this.idManaulTarget = idManaulTarget;
    }

    public String getDefect() {
        return defect;
    }

    public void setDefect(String defect) {
        this.defect = defect;
    }

    public String getDefectRate() {
        return defectRate;
    }

    public void setDefectRate(String defectRate) {
        this.defectRate = defectRate;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Line getIdLine() {
        return idLine;
    }

    public void setIdLine(Line idLine) {
        this.idLine = idLine;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idManaulTarget != null ? idManaulTarget.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ManualTarget)) {
            return false;
        }
        ManualTarget other = (ManualTarget) object;
        if ((this.idManaulTarget == null && other.idManaulTarget != null) || (this.idManaulTarget != null && !this.idManaulTarget.equals(other.idManaulTarget))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.ManualTarget[ idManaulTarget=" + idManaulTarget + " ]";
    }
    
}
