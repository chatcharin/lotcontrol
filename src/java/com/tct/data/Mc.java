/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "mc", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Mc.findAll", query = "SELECT m FROM Mc m"),
    @NamedQuery(name = "Mc.findById", query = "SELECT m FROM Mc m WHERE m.deleted ='0' and m.id = :id"),
    @NamedQuery(name = "Mc.findByMcName", query = "SELECT m FROM Mc m WHERE m.deleted ='0' and m.mcName LIKE :mcName"),
    @NamedQuery(name = "Mc.findByMcStatus", query = "SELECT m FROM Mc m WHERE m.deleted ='0' and m.mcStatus = :mcStatus ORDER BY m.mcNum ASC"),
    @NamedQuery(name = "Mc.findAllNotDelete", query = "SELECT m FROM Mc m Where  m.deleted ='0' ORDER By m.mcName ASC"),
    @NamedQuery(name = "Mc.findByDescription", query = "SELECT m FROM Mc m WHERE m.description = :description")})
public class Mc implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Basic(optional = false)
    @Column(name = "mc_name", nullable = false, length = 255)
    private String mcName;
    @Column(name = "mc_status", length = 255)
    private String mcStatus;
    @Column(name = "description", length = 255)
    private String description;
    @OneToMany(mappedBy = "mcId")
    private Collection<CurrentProcess> currentProcessCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mcId")
    private Collection<McDown> mcDownCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "beforMc")
    private Collection<McMove> mcMoveCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "aftorMc")
    private Collection<McMove> mcMoveCollection1;
    @Column(name = "mc_num", length = 3)
    private int mcNum;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    @Column(name = "mc_type", length = 255)
    private String mcType;
    //@pang ==============================
    @Column(name = "brand_name", length = 255)
    private String brandName;
    @Column(name = "mc_model_no", length = 255)
    private String mcModelNo;
    @Column(name = "mc_serial_no", length = 255)
    private String mcSerailNo;
    @Column(name = "mc_code", length = 255)
    private String mcCode;
    @Column(name = "power_supply", length = 255)
    private String powerSupply;
    @Column(name = "mfg_name", length = 255)
    private String mfgName;
    @Column(name = "fixed_asset", length = 255)
    private String fixedAsset;
    @Column(name = "remarks", length = 255)
    private String remarks;
    @Column(name = "path_priture", length = 255)
    private String pathPriture;
    @Column(name = "product_rate", length = 255)
    private String productRate;
    @Column(name = "plan_production_time", length = 255)
    private String planProductionTime;
    @Column(name = "barcode_mc", length = 255)
    private String barcodeMc; 
    //=====================================
    public Mc() {
    }

    public Mc(String id) {
        this.id = id;
    }

    public Mc(String id, String mcName) {
        this.id = id;
        this.mcName = mcName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMcName() {
        return mcName;
    }

    public void setMcName(String mcName) {
        this.mcName = mcName;
    }

    public String getMcStatus() {
        return mcStatus;
    }

    public void setMcStatus(String mcStatus) {
        this.mcStatus = mcStatus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMcType() {
        return mcType;
    }

    public void setMcType(String mcType) {
        this.mcType = mcType;
    }

    @XmlTransient
    public Collection<CurrentProcess> getCurrentProcessCollection() {
        return currentProcessCollection;
    }

    public void setCurrentProcessCollection(Collection<CurrentProcess> currentProcessCollection) {
        this.currentProcessCollection = currentProcessCollection;
    }

    @XmlTransient
    public Collection<McDown> getMcDownCollection() {
        return mcDownCollection;
    }

    public void setMcDownCollection(Collection<McDown> mcDownCollection) {
        this.mcDownCollection = mcDownCollection;
    }

    @XmlTransient
    public Collection<McMove> getMcMoveCollection() {
        return mcMoveCollection;
    }

    public void setMcMoveCollection(Collection<McMove> mcMoveCollection) {
        this.mcMoveCollection = mcMoveCollection;
    }

    @XmlTransient
    public Collection<McMove> getMcMoveCollection1() {
        return mcMoveCollection1;
    }

    public void setMcMoveCollection1(Collection<McMove> mcMoveCollection1) {
        this.mcMoveCollection1 = mcMoveCollection1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mc)) {
            return false;
        }
        Mc other = (Mc) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.Mc[ id=" + id + " ]";
    }

    /**
     * @return the mcNum
     */
    public int getMcNum() {
        return mcNum;
    }

    /**
     * @param mcNum the mcNum to set
     */
    public void setMcNum(int mcNum) {
        this.mcNum = mcNum;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getFixedAsset() {
        return fixedAsset;
    }

    public void setFixedAsset(String fixedAsset) {
        this.fixedAsset = fixedAsset;
    }

    public String getMcCode() {
        return mcCode;
    }

    public void setMcCode(String mcCode) {
        this.mcCode = mcCode;
    }

    public String getMcModelNo() {
        return mcModelNo;
    }

    public void setMcModelNo(String mcModelNo) {
        this.mcModelNo = mcModelNo;
    }

    public String getMcSerailNo() {
        return mcSerailNo;
    }

    public void setMcSerailNo(String mcSerailNo) {
        this.mcSerailNo = mcSerailNo;
    }

    public String getMfgName() {
        return mfgName;
    }

    public void setMfgName(String mfgName) {
        this.mfgName = mfgName;
    }

    public String getPowerSupply() {
        return powerSupply;
    }

    public void setPowerSupply(String powerSupply) {
        this.powerSupply = powerSupply;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getPathPriture() {
        return pathPriture;
    }

    public void setPathPriture(String pathPriture) {
        this.pathPriture = pathPriture;
    }

    public String getProductRate() {
        return productRate;
    }

    public void setProductRate(String productRate) {
        this.productRate = productRate;
    }

    public String getPlanProductionTime() {
        return planProductionTime;
    }

    public void setPlanProductionTime(String planProductionTime) {
        this.planProductionTime = planProductionTime;
    }

    public String getBarcodeMc() {
        return barcodeMc;
    } 
    public void setBarcodeMc(String barcodeMc) {
        this.barcodeMc = barcodeMc;
    } 
}
