/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "md2pc", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Md2pc.findAll", query = "SELECT m FROM Md2pc m"),
    @NamedQuery(name = "Md2pc.findByMd2pcId", query = "SELECT m FROM Md2pc m WHERE m.md2pcId = :md2pcId and m.deleted = 0"),
    @NamedQuery(name = "Md2pc.findByCreateDate", query = "SELECT m FROM Md2pc m WHERE m.createDate = :createDate"),
    @NamedQuery(name = "Md2pc.findByUserCreate", query = "SELECT m FROM Md2pc m WHERE m.userCreate = :userCreate"),
    @NamedQuery(name = "Md2pc.findByUserApprove", query = "SELECT m FROM Md2pc m WHERE m.userApprove = :userApprove"),

    @NamedQuery(name = "Md2pc.findSqProc", query = "SELECT m FROM Md2pc m WHERE m.userApprove = :userApprove and m.deleted = 0"),
    @NamedQuery(name = "Md2pc.checkSqProcess", query = "SELECT m FROM Md2pc m WHERE (m.modelPoolId = :modelPoolId) and (m.sqProcess =:sqProcess) and m.deleted = 0"),
    @NamedQuery(name = "Md2pc.findBySqProcess", query = "SELECT m FROM Md2pc m WHERE (m.modelPoolId = :modelPoolId)  and (m.processPoolIdProc =:processPoolIdProc) and  m.deleted = 0"),


    //@NamedQuery(name = "Md2pc.findBySqProcess", query = "SELECT m FROM Md2pc m WHERE m.sqProcess = :sqProcess"),
    @NamedQuery(name = "Md2pc.findBymodelPoolId", query="SELECT m FROM Md2pc m WHERE m.modelPoolId = :modelPoolId AND m.deleted = 0 ORDER BY m.sqProcess ASC"),
    
//    @NamedQuery(name = "Md2pc.findBySqProcess", query = "SELECT m FROM Md2pc m WHERE m.sqProcess = :sqProcess"),
   // @NamedQuery(name = "Md2pc.findBymodelPoolId", query="SELECT m FROM Md2pc m WHERE m.modelPoolId = :modelPoolId ORDER BY m.sqProcess ASC"),
    @NamedQuery(name = "Md2pc.findByProcessPoolIdProc",query="SELECT m FROM Md2pc m WHERE m.processPoolIdProc = :processPoolIdProc   and m.deleted = 0")

})

public class Md2pc implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "md2pc_id", nullable = false, length = 36)
    private String md2pcId;
    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "user_create", nullable = false, length = 36)
    private String userCreate;
    @Column(name = "user_approve", length = 36)
    private String userApprove;
    @Column(name = "sq_process", length = 11)
    private int sqProcess;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    @JoinColumn(name = "process_pool_id_proc", referencedColumnName = "id_proc", nullable = false)
    @ManyToOne(optional = false)
    private ProcessPool processPoolIdProc;
    @JoinColumn(name = "model_pool_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private ModelPool modelPoolId;

    public Md2pc() {
    }

    public Md2pc(String md2pcId) {
        this.md2pcId = md2pcId;
    }

    public Md2pc(String md2pcId, String userCreate) {
        this.md2pcId = md2pcId;
        this.userCreate = userCreate;
    }

    public String getMd2pcId() {
        return md2pcId;
    }

    public void setMd2pcId(String md2pcId) {
        this.md2pcId = md2pcId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserApprove() {
        return userApprove;
    }

    public void setUserApprove(String userApprove) {
        this.userApprove = userApprove;
    }

    public int getSqProcess() {
        return sqProcess;
    }

    public void setSqProcess(int sqProcess) {
        this.sqProcess = sqProcess;
    }

    public ProcessPool getProcessPoolIdProc() {
        return processPoolIdProc;
    }

    public void setProcessPoolIdProc(ProcessPool processPoolIdProc) {
        this.processPoolIdProc = processPoolIdProc;
    }

    public ModelPool getModelPoolId() {
        return modelPoolId;
    }

    public void setModelPoolId(ModelPool modelPoolId) {
        this.modelPoolId = modelPoolId;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (md2pcId != null ? md2pcId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Md2pc)) {
            return false;
        }
        Md2pc other = (Md2pc) object;
        if ((this.md2pcId == null && other.md2pcId != null) || (this.md2pcId != null && !this.md2pcId.equals(other.md2pcId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.Md2pc[ md2pcId=" + md2pcId + " ]";
    }
    
}
