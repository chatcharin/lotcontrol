/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author tawat
 */
@Entity
@Table(name = "radar", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Radar.findAll", query = "SELECT r FROM Radar r"),
    @NamedQuery(name = "Radar.findByIdRadar", query = "SELECT r FROM Radar r WHERE r.idRadar = :idRadar"),
    @NamedQuery(name = "Radar.findByActualRadar", query = "SELECT r FROM Radar r WHERE r.actualRadar = :actualRadar"),
    @NamedQuery(name = "Radar.findByDeleted", query = "SELECT r FROM Radar r WHERE r.deleted = :deleted"),
    @NamedQuery(name = "Radar.findByLineNo", query = "SELECT r FROM Radar r WHERE r.lineNo = :lineNo"),
    @NamedQuery(name = "Radar.findByNameRadar", query = "SELECT r FROM Radar r WHERE r.nameRadar = :nameRadar"),
    @NamedQuery(name = "Radar.findByPercentRadar", query = "SELECT r FROM Radar r WHERE r.percentRadar = :percentRadar"),
    @NamedQuery(name = "Radar.findByShowRadar", query = "SELECT r FROM Radar r WHERE r.showRadar = :showRadar"),
    @NamedQuery(name = "Radar.findByTargetRadar", query = "SELECT r FROM Radar r WHERE r.targetRadar = :targetRadar")})
public class Radar implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_radar", nullable = false, length = 36)
    private String idRadar;
    @Basic(optional = false)
    @Column(name = "actual_radar", nullable = false, length = 255)
    private String actualRadar;
    @Basic(optional = false)
    @Column(name = "deleted", nullable = false)
    private int deleted;
    @Basic(optional = false)
    @Column(name = "line_no", nullable = false, length = 10)
    private String lineNo;
    @Basic(optional = false)
    @Column(name = "name_radar", nullable = false, length = 255)
    private String nameRadar;
    @Basic(optional = false)
    @Column(name = "percent_radar", nullable = false, length = 255)
    private String percentRadar;
    @Basic(optional = false)
    @Column(name = "show_radar", nullable = false, length = 255)
    private String showRadar;
    @Basic(optional = false)
    @Column(name = "target_radar", nullable = false, length = 255)
    private String targetRadar;
    @JoinColumn(name = "id_color", referencedColumnName = "id_radar_color", nullable = false)
    @ManyToOne(optional = false)
    private RadarColor idColor;

    public Radar() {
    }

    public Radar(String idRadar) {
        this.idRadar = idRadar;
    }

    public Radar(String idRadar, String actualRadar, int deleted, String lineNo, String nameRadar, String percentRadar, String showRadar, String targetRadar) {
        this.idRadar = idRadar;
        this.actualRadar = actualRadar;
        this.deleted = deleted;
        this.lineNo = lineNo;
        this.nameRadar = nameRadar;
        this.percentRadar = percentRadar;
        this.showRadar = showRadar;
        this.targetRadar = targetRadar;
    }

    public String getIdRadar() {
        return idRadar;
    }

    public void setIdRadar(String idRadar) {
        this.idRadar = idRadar;
    }

    public String getActualRadar() {
        return actualRadar;
    }

    public void setActualRadar(String actualRadar) {
        this.actualRadar = actualRadar;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public String getLineNo() {
        return lineNo;
    }

    public void setLineNo(String lineNo) {
        this.lineNo = lineNo;
    }

    public String getNameRadar() {
        return nameRadar;
    }

    public void setNameRadar(String nameRadar) {
        this.nameRadar = nameRadar;
    }

    public String getPercentRadar() {
        return percentRadar;
    }

    public void setPercentRadar(String percentRadar) {
        this.percentRadar = percentRadar;
    }

    public String getShowRadar() {
        return showRadar;
    }

    public void setShowRadar(String showRadar) {
        this.showRadar = showRadar;
    }

    public String getTargetRadar() {
        return targetRadar;
    }

    public void setTargetRadar(String targetRadar) {
        this.targetRadar = targetRadar;
    }

    public RadarColor getIdColor() {
        return idColor;
    }

    public void setIdColor(RadarColor idColor) {
        this.idColor = idColor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRadar != null ? idRadar.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Radar)) {
            return false;
        }
        Radar other = (Radar) object;
        if ((this.idRadar == null && other.idRadar != null) || (this.idRadar != null && !this.idRadar.equals(other.idRadar))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.Radar[ idRadar=" + idRadar + " ]";
    }
    
}
