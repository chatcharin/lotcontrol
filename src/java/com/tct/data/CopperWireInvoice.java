/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "copper_wire_invoice", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CopperWireInvoice.findAll", query = "SELECT c FROM CopperWireInvoice c"),
    @NamedQuery(name = "CopperWireInvoice.findById", query = "SELECT c FROM CopperWireInvoice c WHERE c.id = :id"),
    @NamedQuery(name = "CopperWireInvoice.findByPastCode", query = "SELECT c FROM CopperWireInvoice c WHERE c.pastCode = :pastCode"),
    @NamedQuery(name = "CopperWireInvoice.findByInvoiceCopper", query = "SELECT c FROM CopperWireInvoice c WHERE c.invoiceCopper = :invoiceCopper"),
    @NamedQuery(name = "CopperWireInvoice.detailsIdDetail", query = "SELECT c FROM CopperWireInvoice c WHERE c.detailsIdDetail = :detailsIdDetail")
})
public class CopperWireInvoice implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Basic(optional = false)
    @Column(name = "past_code", nullable = false, length = 255)
    private String pastCode;
    @Basic(optional = false)
    @Column(name = "invoice_copper", nullable = false, length = 255)
    private String invoiceCopper;
    @JoinColumn(name = "details_id_detail", referencedColumnName = "id_detail", nullable = false)
    @ManyToOne(optional = false)
    private Details detailsIdDetail;

    public CopperWireInvoice() {
    }

    public CopperWireInvoice(String id) {
        this.id = id;
    }

    public CopperWireInvoice(String id, String pastCode, String invoiceCopper) {
        this.id = id;
        this.pastCode = pastCode;
        this.invoiceCopper = invoiceCopper;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPastCode() {
        return pastCode;
    }

    public void setPastCode(String pastCode) {
        this.pastCode = pastCode;
    }

    public String getInvoiceCopper() {
        return invoiceCopper;
    }

    public void setInvoiceCopper(String invoiceCopper) {
        this.invoiceCopper = invoiceCopper;
    }

    public Details getDetailsIdDetail() {
        return detailsIdDetail;
    }

    public void setDetailsIdDetail(Details detailsIdDetail) {
        this.detailsIdDetail = detailsIdDetail;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CopperWireInvoice)) {
            return false;
        }
        CopperWireInvoice other = (CopperWireInvoice) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.CopperWireInvoice[ id=" + id + " ]";
    }
}
