/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author The_Boy_Cs
 */
@Entity
@Table(name = "assy2auto", catalog = "tct_project", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Assy2auto.findAll", query = "SELECT a FROM Assy2auto a"),
    @NamedQuery(name = "Assy2auto.findById", query = "SELECT a FROM Assy2auto a WHERE a.id = :id"),
    @NamedQuery(name = "Assy2auto.findByQty", query = "SELECT a FROM Assy2auto a WHERE a.qty = :qty"),
    @NamedQuery(name = "Assy2auto.findByCreatedate", query = "SELECT a FROM Assy2auto a WHERE a.createdate = :createdate")})
public class Assy2auto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false, length = 36)
    private String id;
    @Basic(optional = false)
    @Column(name = "qty", nullable = false, length = 255)
    private String qty;
    @Column(name = "createdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdate;
    @JoinColumn(name = "detail_id", referencedColumnName = "id_detail", nullable = false)
    @ManyToOne(optional = false)
    private Details detailId;
    @JoinColumn(name = "lotcontrol_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private LotControl lotcontrolId;

    public Assy2auto() {
    }

    public Assy2auto(String id) {
        this.id = id;
    }

    public Assy2auto(String id, String qty) {
        this.id = id;
        this.qty = qty;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public Details getDetailId() {
        return detailId;
    }

    public void setDetailId(Details detailId) {
        this.detailId = detailId;
    }

    public LotControl getLotcontrolId() {
        return lotcontrolId;
    }

    public void setLotcontrolId(LotControl lotcontrolId) {
        this.lotcontrolId = lotcontrolId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Assy2auto)) {
            return false;
        }
        Assy2auto other = (Assy2auto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tct.data.Assy2auto[ id=" + id + " ]";
    }
    
}
