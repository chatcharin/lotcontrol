package com.tct.dashboard;





import java.io.File;
import java.sql.*;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ramki
 
@ManagedBean
@SessionScoped*/

public class ReportBean {
  private static void deletefile(String file){
      File f1 = new File(file);
      boolean success = f1.delete();
      if (!success) {
          System.out.println("Deletion failed.");
          System.exit(0);
      } else {
          System.out.println("File deleted.");
      }
  }
     public boolean pdf(String reportname,String outputname,HashMap input){
        System.out.println("Start ------------------------------- ");
          Connection con=null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = (Connection) DriverManager.getConnection ("jdbc:mysql://localhost:3306/tct_project", "root", "root");
            System.out.println("step  -------------------------------  [-2]");
            String reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath(reportname); 
            String outputPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath(outputname);
             //deletefile(outputPath);
            System.out.println("step  -------------------------------  [-1]"+ "path ="+reportPath);
            JasperPrint jasperPrint = null;
            String barcodePath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/images/barcode/");
            input.put("barcode", barcodePath);
            try {
                jasperPrint = JasperFillManager.fillReport(reportPath,input,con);
                 System.out.println("step  -------------------------------  [0]");
            } catch (JRException ex) {
                Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
            System.out.println("step  -------------------------------  [1]");
          
           
            System.out.println("step  -------------------------------  [2]");

            System.out.println("step  -------------------------------  [3]");

                JRPdfExporter pdfexport =new JRPdfExporter();
                pdfexport.setParameter(JRExporterParameter.JASPER_PRINT,jasperPrint);
                   System.out.println("step  -------------------------------  [5]");
                pdfexport.setParameter(JRExporterParameter.OUTPUT_FILE_NAME,outputPath);
                   System.out.println("step  -------------------------------  [6]");
                   System.out.println("step  -------------------------------  [7]");
                pdfexport.exportReport();
               
                   System.out.println("step  -------------------------------  [8]");

                 System.out.println("step  -------------------------------  [9]");
        } catch (SQLException ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
             return false;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
             return false;
        } catch (JRException ex) {
                Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }finally {
            try {
                if(con!=null&&!con.isClosed())
                    con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
            }
       }
        
        return true;
    }

      Connection con=null;
       public Connection getConnection(String brand){
        if(brand.equals("mysql"))
           try {
            Class.forName("com.mysql.jdbc.Driver");
            return  (Connection) DriverManager.getConnection ("jdbc:mysql://localhost:3306/tct_project", "admin", "admin");
        } catch (Exception ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        else  try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            return  (Connection) DriverManager.getConnection ("jdbc:sqlserver://localhost\\SQLEXPRESS:1433;databaseName=tct_project", "tctproject", "F9gdup8vp");
        } catch (Exception ex) {
            Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
            
       }
       public boolean pdfSelect(String reportname,String outputname,String sql){
        System.out.println("Start ------------------------------- ");
        // Create Connection 
        Statement st = null;
        ResultSet rs = null;
        if(con==null)
             con = getConnection("mysql");
        if(con!=null){
            // Query SQL
            try {
               
                st=con.createStatement();
                System.out.println("step  -------------------------------  [0]");
            } catch (SQLException ex) {
                Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                rs =st.executeQuery(sql);
                System.out.println("step  -------------------------------  [1]");
            } catch (SQLException ex) {
                Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            // End Query SQL 
           
            System.out.println("step  -------------------------------  [-2]");
            String reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath(reportname);
           
            String outputPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath(outputname);
            System.out.println("step  -------------------------------  [-1]"+ "path ="+reportPath);
//            deletefile(outputPath);
            JasperPrint jasperPrint = null;
            JRResultSetDataSource jasperReports = new JRResultSetDataSource(rs);
            System.out.println("step  -------------------------------  [2]");
            String barcodePath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/images/barcode/");
            HashMap bcpath = new HashMap();
            bcpath.put("barcode", barcodePath);
            try {
                jasperPrint = JasperFillManager.fillReport(reportPath, bcpath,jasperReports);
                System.out.println("step  -------------------------------  [3]");
            } catch (JRException ex) {
                Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
            }
                JRPdfExporter pdfexport =new JRPdfExporter();
                pdfexport.setParameter(JRExporterParameter.JASPER_PRINT,jasperPrint);
                System.out.println("step  -------------------------------  [5]");
                pdfexport.setParameter(JRExporterParameter.OUTPUT_FILE_NAME,outputPath);
                System.out.println("step  -------------------------------  [6]");
               
            try {
                pdfexport.exportReport();
                System.out.println("step  -------------------------------  [7]");
            } catch (JRException ex) {
                Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
            }
                 System.out.println("step  -------------------------------  [8]");
                 System.out.println("step  -------------------------------  [9]");
            try {
                if(con!=null&&!con.isClosed())
                    con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return true;
    }
}
