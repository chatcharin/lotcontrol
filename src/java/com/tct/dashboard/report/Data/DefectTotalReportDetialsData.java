/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.dashboard.report.Data;
/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Oct 23, 2012, Time : 11:20:23 AM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class DefectTotalReportDetialsData {
    private String index;
    private String   dated;
    private String problemOccured;
    private String correctiveAction;

    public DefectTotalReportDetialsData(String index, String dated, String problemOccured, String correctiveAction) {
        this.index = index;
        this.dated = dated;
        this.problemOccured = problemOccured;
        this.correctiveAction = correctiveAction;
    } 
    public String getCorrectiveAction() {
        return correctiveAction;
    }

    public void setCorrectiveAction(String correctiveAction) {
        this.correctiveAction = correctiveAction;
    }

    public String getDated() {
        return dated;
    }

    public void setDated(String dated) {
        this.dated = dated;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getProblemOccured() {
        return problemOccured;
    }

    public void setProblemOccured(String problemOccured) {
        this.problemOccured = problemOccured;
    }  
}
