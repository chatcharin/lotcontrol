/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tct.dashboard.report.Data;

import java.util.Date;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Oct 22, 2012, Time : 4:57:28 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class DefectTotalReportControlData {
    private String process;
    private String  processType;
    private String  defectName;
    private Date  start;
    private Date end;
    private String model;

    public DefectTotalReportControlData(String process, String processType, String defectName, Date start, Date end, String model) {
        this.process = process;
        this.processType = processType;
        this.defectName = defectName;
        this.start = start;
        this.end = end;
        this.model = model;
    }

    public String getDefectName() {
        return defectName;
    }

    public void setDefectName(String defectName) {
        this.defectName = defectName;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getProcessType() {
        return processType;
    }

    public void setProcessType(String processType) {
        this.processType = processType;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    } 
}
