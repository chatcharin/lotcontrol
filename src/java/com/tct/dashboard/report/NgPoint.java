/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.dashboard.report;

import java.util.Date;

/**
 *
 * @author TheBoyCs
 */
public class NgPoint {

    private String ngname = "";//ชื่อของ ng
    private String comeon = "";//ที่มา
    private Date date;
    private double numpcs = 0; // จำนวน ng/Pcs.
    private double numpercen = 0;// จำนวน ng คิดเป็น %

    public NgPoint() {
    }

    public NgPoint(String ngname, String comeon, Date date, double numpcs, double numpercen) {
        this.ngname = ngname;
        this.comeon = comeon;
        this.date = date;
        this.numpcs = numpcs;
        this.numpercen = numpercen;
    }

    public String getComeon() {
        return comeon;
    }

    public void setComeon(String comeon) {
        this.comeon = comeon;
    }

    public String getNgname() {
        return ngname;
    }

    public void setNgname(String ngname) {
        this.ngname = ngname;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getNumpcs() {
        return numpcs;
    }

    public void setNumpcs(double numpcs) {
        this.numpcs = numpcs;
    }

    public double getNumpercen() {
        return numpercen;
    }

    public void setNumpercen(double numpercen) {
        this.numpercen = numpercen;
    }
}
