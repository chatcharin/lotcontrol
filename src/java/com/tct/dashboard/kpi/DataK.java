/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.dashboard.kpi;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Aek
 */
public class DataK {
    //Module

    public String module_name = "";
    // Target
    public double target_line1 = 0;
    public double target_line2 = 0;
    public double target_line3 = 0;
    public double target_line4 = 0;
    public double sum_target_line = 0;
    // Result
    public double result_line1 = 0;
    public double result_line2 = 0;
    public double result_line3 = 0;
    public double result_line4 = 0;
    public double sum_result_line = 0;
    // Judgment
    public String st_typ1_line1 = "-";
    public String st_typ1_line2 = "-";
    public String st_typ1_line3 = "-";
    public String st_typ1_line4 = "-";
    // Defect 
    public double defect_line1 = 0.00;
    public double defect_line2 = 0.00;
    public double defect_line3 = 0.00;
    public double defect_line4 = 0.00;
    public double sum_defect_line = 0.00;
    public double defect_rate_line1 = 0.00;
    public double defect_rate_line2 = 0.00;
    public double defect_rate_line3 = 0.00;
    public double defect_rate_line4 = 0.00;
    public double defect_rate_line = 0.00;
    //Line
    public int line = 0;

    public DataK() {
//        checkFail();
    }

    public void checkFail() {
        double result1 = (result_line1 / target_line1) * 100;
        if (target_line1 > 0 && result1 < 50.0) {
            class_judgment1 = "fail_tb";
            class_judgment_tb1 = "number_red_tb";
            st_typ1_line1 = "Fail";
        } else if (target_line1 > 0){
            class_judgment1 = "archive_tb";
            class_judgment_tb1 = "number_green_tb";
            st_typ1_line1 = "ARCHIVE";
        }
        double result2 = (result_line2 / target_line2) * 100;
        if (target_line2 > 0 && result2 < 50.0) {
            class_judgment2 = "fail_tb";
            class_judgment_tb2 = "number_red_tb";
            st_typ1_line2 = "Fail";
        } else if (target_line2 > 0){
            class_judgment2 = "archive_tb";
            class_judgment_tb2 = "number_green_tb";
            st_typ1_line2 = "ARCHIVE";
        }
        double result3 = (result_line3 / target_line3) * 100;
        if (target_line3 > 0 && result3 < 50.0) {
            class_judgment3 = "fail_tb";
            class_judgment_tb3 = "number_red_tb";
            st_typ1_line3 = "Fail";
        } else if (target_line3 > 0){
            class_judgment3 = "archive_tb";
            class_judgment_tb3 = "number_green_tb";
            st_typ1_line3 = "ARCHIVE";
        }
        double result4 = (result_line4 / target_line4) * 100;
        if (target_line4 > 0 && result4 < 50.0) {
            class_judgment4 = "fail_tb";
            class_judgment_tb4 = "number_red_tb";
            st_typ1_line4 = "Fail";
        } else if (target_line4 > 0){
            class_judgment4 = "archive_tb";
            class_judgment_tb4 = "number_green_tb";
            st_typ1_line4 = "ARCHIVE";
        }
    }

    public String getClass_judgment_tb1() {
        return class_judgment_tb1;
    }

    public String getClass_judgment_tb2() {
        return class_judgment_tb2;
    }

    public String getClass_judgment_tb3() {
        return class_judgment_tb3;
    }

    public String getClass_judgment_tb4() {
        return class_judgment_tb4;
    }
    //Class
    public String class_judgment1 = "archive_tb";
    public String class_judgment2 = "archive_tb";
    public String class_judgment3 = "archive_tb";
    public String class_judgment4 = "archive_tb";
    //Class tb
    public String class_judgment_tb1 = "number_green_tb";
    public String class_judgment_tb2 = "number_green_tb";
    public String class_judgment_tb3 = "number_green_tb";

    public void setClass_judgment_tb1(String class_judgment_tb1) {
        this.class_judgment_tb1 = class_judgment_tb1;
    }

    public void setClass_judgment_tb2(String class_judgment_tb2) {
        this.class_judgment_tb2 = class_judgment_tb2;
    }

    public void setClass_judgment_tb3(String class_judgment_tb3) {
        this.class_judgment_tb3 = class_judgment_tb3;
    }

    public void setClass_judgment_tb4(String class_judgment_tb4) {
        this.class_judgment_tb4 = class_judgment_tb4;
    }
    public String class_judgment_tb4 = "number_green_tb";
    public String date = "";

    public void setClass_judgment1(String class_judgment1) {
        this.class_judgment1 = class_judgment1;
    }

    public void setClass_judgment2(String class_judgment2) {
        this.class_judgment2 = class_judgment2;
    }

    public void setClass_judgment3(String class_judgment3) {
        this.class_judgment3 = class_judgment3;
    }

    public void setClass_judgment4(String class_judgment4) {
        this.class_judgment4 = class_judgment4;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getClass_judgment1() {
        return class_judgment1;
    }

    public String getClass_judgment2() {
        return class_judgment2;
    }

    public String getClass_judgment3() {
        return class_judgment3;
    }

    public String getClass_judgment4() {
        return class_judgment4;
    }

    public String getDate() {
        date = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        return date;
    }

    public double getDefect_line1() {
        return defect_line1;
    }

    public double getDefect_line2() {
        return defect_line2;
    }

    public double getDefect_line3() {
        return defect_line3;
    }

    public double getDefect_line4() {
        return defect_line4;
    }

    public int getLine() {
        return line;
    }

    public String getModule_name() {
        return module_name;
    }

    public double getResult_line1() {
        return result_line1;
    }

    public double getResult_line2() {
        return result_line2;
    }

    public double getResult_line3() {
        return result_line3;
    }

    public double getResult_line4() {
        return result_line4;
    }

    public String getSt_typ1_line1() {
//        if (target_line1 > result_line1) {
//            class_judgment1 = "fail_tb";
//            class_judgment_tb1 = "number_red_tb";
//            st_typ1_line1 = "Fail";
//        }
        return st_typ1_line1;
    }

    public String getSt_typ1_line2() {
//        if (target_line2 > result_line2) {
//            class_judgment2 = "fail_tb";
//            class_judgment_tb2 = "number_red_tb";
//            st_typ1_line2 = "Fail";
//        }
        return st_typ1_line2;
    }

    public String getSt_typ1_line3() {
//        if (target_line3 > result_line3) {
//            class_judgment3 = "fail_tb";
//            class_judgment_tb3 = "number_red_tb";
//            st_typ1_line3 = "Fail";
//        }

        return st_typ1_line3;
    }

    public String getSt_typ1_line4() {
//        if (target_line4 > result_line4) {
//            class_judgment4 = "fail_tb";
//            class_judgment_tb4 = "number_red_tb";
//            st_typ1_line4 = "Fail";
//        }

        return st_typ1_line4;
    }

    public double getSum_defect_line() {
        return sum_defect_line;
    }

    public double getSum_result_line() {
        return sum_result_line;
    }

    public double getSum_target_line() {
        return sum_target_line;
    }

    public double getTarget_line1() {
        return target_line1;
    }

    public double getTarget_line2() {
        return target_line2;
    }

    public double getTarget_line3() {
        return target_line3;
    }

    public double getTarget_line4() {
        return target_line4;
    }

    public void setDefect_line1(double defect_line1) {
        this.defect_line1 = defect_line1;
    }

    public void setDefect_line2(double defect_line2) {
        this.defect_line2 = defect_line2;
    }

    public void setDefect_line3(double defect_line3) {
        this.defect_line3 = defect_line3;
    }

    public void setDefect_line4(double defect_line4) {
        this.defect_line4 = defect_line4;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public void setModule_name(String module_name) {
        this.module_name = module_name;
    }

    public void setResult_line1(double result_line1) {
        this.result_line1 = result_line1;
    }

    public void setResult_line2(double result_line2) {
        this.result_line2 = result_line2;
    }

    public void setResult_line3(double result_line3) {
        this.result_line3 = result_line3;
    }

    public void setResult_line4(double result_line4) {
        this.result_line4 = result_line4;
    }

    public void setSt_typ1_line1(String st_typ1_line1) {
        this.st_typ1_line1 = st_typ1_line1;
    }

    public void setSt_typ1_line2(String st_typ1_line2) {
        this.st_typ1_line2 = st_typ1_line2;
    }

    public void setSt_typ1_line3(String st_typ1_line3) {
        this.st_typ1_line3 = st_typ1_line3;
    }

    public void setSt_typ1_line4(String st_typ1_line4) {
        this.st_typ1_line4 = st_typ1_line4;
    }

    public void setSum_defect_line(double sum_defect_line) {
        this.sum_defect_line = sum_defect_line;
    }

    public void setSum_result_line(double sum_result_line) {
        this.sum_result_line = sum_result_line;
    }

    public void setSum_target_line(double sum_target_line) {
        this.sum_target_line = sum_target_line;
    }

    public void setTarget_line1(double target_line1) {
        this.target_line1 = target_line1;
    }

    public void setTarget_line2(double target_line2) {
        this.target_line2 = target_line2;
    }

    public void setTarget_line3(double target_line3) {
        this.target_line3 = target_line3;
    }

    public void setTarget_line4(double target_line4) {
        this.target_line4 = target_line4;
    }

    public double getDefect_rate_line() {
        return defect_rate_line;
    }

    public void setDefect_rate_line(double defect_rate_line) {
        this.defect_rate_line = defect_rate_line;
    }

    public double getDefect_rate_line1() {
        double df_rate = 0;
        if(result_line1 > 0)
            df_rate = (defect_line1 / result_line1) * 1000000;
        return df_rate;//defect_rate_line1;
    }

    public void setDefect_rate_line1(double defect_rate_line1) {
       System.out.println("rate : "+defect_rate_line1);
       this.defect_rate_line1 = defect_rate_line1;
    }

    public double getDefect_rate_line2() {
        double df_rate = 0;
        if(result_line2 > 0)
            df_rate = (defect_line2 / result_line2) * 1000000;
        return df_rate;//defect_rate_line2;
    }

    public void setDefect_rate_line2(double defect_rate_line2) {
        this.defect_rate_line2 = defect_rate_line2;
    }

    public double getDefect_rate_line3() {
        double df_rate = 0;
        if(result_line3 > 0)
            df_rate = (defect_line3 / result_line3) * 1000000;
        return df_rate;//defect_rate_line3;
    }

    public void setDefect_rate_line3(double defect_rate_line3) {
        this.defect_rate_line3 = defect_rate_line3;
    }

    public double getDefect_rate_line4() {
        double df_rate = 0;
        if(result_line4 > 0)
            df_rate = (defect_line4 / result_line4) * 1000000;
        return df_rate;// defect_rate_line4;
    }

    public void setDefect_rate_line4(double defect_rate_line4) {
        System.out.println(defect_rate_line4);
        this.defect_rate_line4 = defect_rate_line4;
    }
}
