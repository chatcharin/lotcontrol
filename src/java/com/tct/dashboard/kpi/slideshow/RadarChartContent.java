/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.dashboard.kpi.slideshow;

/**
 *
 * @author Tawat
 *
 */
public class RadarChartContent {

    private String ticksname;// = "[[0,'Test1'],[1,'Test2'],[2,'Test3']]";
    private String actualvalue;// = "[[0,10],[1,20],[2,30]]";
    private String targetvalue;// = "[[0,20],[1,30],[2,40]]";
    private String percenvalue;// = "[[0,30],[1,20],[2,10]]";

    public RadarChartContent(String ticksname, String actualvalue,String targetvalue,String percenvalue) {
        this.ticksname = ticksname;
        this.actualvalue = actualvalue;
        this.targetvalue = targetvalue;
        this.percenvalue = percenvalue;
    }

    public RadarChartContent() {
    }

    public String getTicksname() {
        return ticksname;
    }

    public void setTicksname(String ticksname) {
        this.ticksname = ticksname;
    }

    public String getActualvalue() {
        return actualvalue;
    }

    public void setActualvalue(String actualvalue) {
        this.actualvalue = actualvalue;
    }

    public String getPercenvalue() {
        return percenvalue;
    }

    public void setPercenvalue(String percenvalue) {
        this.percenvalue = percenvalue;
    }

    public String getTargetvalue() {
        return targetvalue;
    }

    public void setTargetvalue(String targetvalue) {
        this.targetvalue = targetvalue;
    }
}
