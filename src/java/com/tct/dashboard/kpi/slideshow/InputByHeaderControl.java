/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.dashboard.kpi.slideshow;

import com.appCinfigpage.bean.userLogin;
import com.tct.data.Line;
import com.tct.data.jpa.LineJpaController;
import com.tct.data.jsf.util.PaginationHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.persistence.Persistence;

/**
 *
 * @author TheBoyCs
 */
public class InputByHeaderControl {

    private Line currentline;
    private List<lineLists> linelist;
    private DataModel<lineLists> listTable;
    private String textAll;

    public InputByHeaderControl() {
        loadLine();
    }

    public void loadLine() {
        linelist = null;
        currentline = null;
        listTable = null;
        print("=============== load line =======================================");
        try {
            List<Line> lines = getLineJpaController().findAllLineNotDeletd();
            print("Line0 : " + lines.get(0).getLineName());
            int i = 1;
            for (Line line : lines) {
                print("Line : " + line.getLineName());
                getLinelist().add(new lineLists(i, line.getLineName(), line.getDescritpion(), line));
            }
            setListTable();
        } catch (Exception e) {
            print("Error : " + e);
        }
    }

    public void changToView() {
        print("================== chang to view ================================");
        try {
            lineLists lists = getListTable().getRowData();
            String lineid = lists.getLine().getLineId();
            String link = "report/slideshowkpi/kpislideshowbyline.xhtml?lineid=" + lineid;
            FacesContext fc = FacesContext.getCurrentInstance();
            ExternalContext externalContext = fc.getExternalContext();
            externalContext.redirect(link);
//            return "";
        } catch (Exception e) {
            print(" Error ; " + e);
//            return "";
        }
    }

    public void changToEdit() {
        print("================== chang to edit ================================");
        try {
            lineLists lists = getListTable().getRowData();
            currentline = lists.getLine();
            textAll = currentline.getHeaderComment();
            String link = "pages/setting/inputbyheader/editview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (Exception e) {
            print("Error : " + e);
        }
    }

    public void previous() {
        getPaginationHelper().previousPage();
        setListTable();
    }

    public void next() {
        getPaginationHelper().nextPage();
        setListTable();
    }

    public int getAllPages() {
        int all = getPaginationHelper().getItemsCount();
        int pagesize = getPaginationHelper().getPageSize();
        return ((int) all / pagesize) + 1;
    }
    private PaginationHelper paginationHelper;

    public PaginationHelper getPaginationHelper() {
        if (paginationHelper == null) {
            paginationHelper = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    return getLinelist().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    print("PageFirst : " + getPageFirstItem());
                    print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (getLinelist().size() > 0) {
                        listmodel = new ListDataModel(getLinelist().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(getLinelist());
                    }
                    return listmodel;
                }
            };
        }
        return paginationHelper;
    }

    public DataModel<lineLists> getListTable() {
//        if (listTable == null) {
//            listTable = new ArrayDataModel<lineLists>();
//        }
        return listTable;
    }

    public void setListTable() {
        listTable = getPaginationHelper().createPageDataModel();
    }

    public List<lineLists> getLinelist() {
        if (linelist == null) {
            linelist = new ArrayList<lineLists>();
        }
        return linelist;
    }

    public void changToSave() {
        print("=============== chang to save ===================================");
        try {
            String tmp = textAll;//.replace(" ", "");
            print("Tmp : " + tmp);
            if (!tmp.isEmpty()) {
                currentline.setHeaderComment(tmp);
                getLineJpaController().edit(currentline);
                loadLine();
                String link = "pages/setting/inputbyheader/listview.xhtml";
                userLogin.getSessionMenuBarBean().setParam(link);
            }
        } catch (Exception e) {
            print("Error update line : " + e);
        }
    }

    public void changTocancel() {
        print("=============== chang to cancel =================================");
        try {
            currentline = null;
            String link = "pages/setting/inputbyheader/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (Exception e) {
            print("Error : " + e);
        }
    }
//    public List<SelectItem> getLinelist() {
//        try {
//            List<Line> lines = getLineJpaController().findAllLineNotDeletd();
//            List<SelectItem> linelist = new ArrayList<SelectItem>();
//            for (Line line : lines) {
//                linelist.add(new SelectItem(line.getLineId(), line.getLineName()));
//            }
//            return linelist;
//        } catch (Exception e) {
//            print("Error : " + e);
//            return null;
//        }
//    }
    private LineJpaController lineJpaController;

    private LineJpaController getLineJpaController() {
        if (lineJpaController == null) {
            lineJpaController = new LineJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lineJpaController;
    }

    public Line getCurrentline() {
        if (currentline == null) {
            currentline = new Line(UUID.randomUUID().toString());
        }
        return currentline;
    }

    public void setCurrentline(Line currentline) {
        this.currentline = currentline;
    }

    private void print(String string) {
        System.out.println(string);
    }

    public String getTextAll() {
        return textAll;
    }

    public void setTextAll(String textAll) {
        this.textAll = textAll;
    }

    public class lineLists {

        private int num;
        private String linename;
        private String linedescription;
        private Line line;

        public lineLists(int num, String linename, String linedescription, Line line) {
            this.num = num;
            this.linename = linename;
            this.linedescription = linedescription;
            this.line = line;
        }

        public Line getLine() {
            return line;
        }

        public void setLine(Line line) {
            this.line = line;
        }

        public String getLinedescription() {
            return linedescription;
        }

        public void setLinedescription(String linedescription) {
            this.linedescription = linedescription;
        }

        public String getLinename() {
            return linename;
        }

        public void setLinename(String linename) {
            this.linename = linename;
        }

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }
    }
}
