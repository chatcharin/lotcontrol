/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.dashboard.kpi.slideshow;

import com.appCinfigpage.bean.fc;
import com.tct.data.*;
import com.tct.data.jpa.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Persistence;

/**
 *
 * @author Tawat Development by Tawat Saekue Date : Sep 27, 2012 Copy Right
 * 4Xtreme Co.,Ltd.
 */
public class KpiSlideShowManualControl {

    private List<slideshow> slideshowlist;
    private List<ManualTarget> manualtargetlist;
    private List<Line> linelist;
    private List<TargetResult> targetResultslist;
    private TargetResult targetResult;
    private NextModelList nextModelList;
    private List<NextModel> nextmodellist;
    private List<NextModelList> nextModelLists;
    private RadarChartContent radarChartContent;
    private List<Radar> radarlist;
    private long contenpx = 10000;
    private long count;
    private RadarColor radarColor;
    private List<PicTureList> piclist;
    private List<KpiPicture> kpiPicturelist;
    private int n = 2;
    private String blink;
    private List<Settimetorefresh> settimetorefresh;

    public KpiSlideShowManualControl() {
//        loadSlideShow();
    }

    public int getRefreshLine() {
        print("=========== getseting ==============");
        try {
//            if (settimetorefresh == null) {
            settimetorefresh = getSettimetorefreshJpaController().findSettimetorefreshAllAndFrist();
//            }
            print("Refresh : " + settimetorefresh.get(0).getRefreshLine());
            return settimetorefresh.get(0).getRefreshManual().intValue() * 1000;
        } catch (Exception e) {
            print("========== Error : " + e);
            return 50000;
        }
    }

    public int getSlideLine() {
        print("============ getslideline ===========");
        try {
//            if (settimetorefresh == null) {
            settimetorefresh = getSettimetorefreshJpaController().findSettimetorefreshAllAndFrist();
//            }
            print("Slide : " + settimetorefresh.get(0).getSlideLine());
            return settimetorefresh.get(0).getSlideManual().intValue();
        } catch (Exception e) {
            print("========= Error : " + e);
            return 5;
        }
    }
    private SettimetorefreshJpaController settimetorefreshJpaController;

    private SettimetorefreshJpaController getSettimetorefreshJpaController() {
        if (settimetorefreshJpaController == null) {
            settimetorefreshJpaController = new SettimetorefreshJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return settimetorefreshJpaController;
    }

    public long getCount() {
        return count;
    }

    public List<PicTureList> getPiclist() {
        if (piclist == null) {
            piclist = new ArrayList<PicTureList>();
        }
        return piclist;
    }

    private void loadPicList() {
        print("=========== load kpi picture list ===============================");
        try {
            piclist = null;
            kpiPicturelist = getKpiPictureJpaController().findAllNotDelete();
            for (KpiPicture kpipicture : kpiPicturelist) {
                if (kpipicture.getShowPicture().equals("true")) {
                    getPiclist().add(new PicTureList(kpipicture.getTitleKpiPicture().toString(), kpipicture.getPictureUrl(), n));
                    n++;
                }
            }
        } catch (Exception e) {
            print("=== Error load pic list : " + e);
        }
    }
    private KpiPictureJpaController kpiPictureJpaController;

    private KpiPictureJpaController getKpiPictureJpaController() {
        if (kpiPictureJpaController == null) {
            kpiPictureJpaController = new KpiPictureJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return kpiPictureJpaController;
    }

    private void loadRadarChart() {
        print("============ loadradartchart ====================================");
        try {
            radarlist = getRadarJpaController().findAllAndNotDeleted();
            radarChartContent = null;
            String ticksname = "[";
            String actualvalue = "[";
            String targetvalue = "[";
            String percenvalue = "[";
            int i = 0;
            for (Radar radar : radarlist) {
                if (radar.getShowRadar().equals("true")) {
                    if (i == (radarlist.size() - 1)) {
                        ticksname += "[" + i + ", '" + radar.getNameRadar() + "']";
                        actualvalue += "[" + i + ", " + radar.getActualRadar() + "]";
                        targetvalue += "[" + i + ", " + radar.getTargetRadar() + "]";
                        percenvalue += "[" + i + ", " + radar.getPercentRadar() + "]";
                    } else {
                        ticksname += "[" + i + ", '" + radar.getNameRadar() + "'],";
                        actualvalue += "[" + i + ", " + radar.getActualRadar() + "],";
                        targetvalue += "[" + i + ", " + radar.getTargetRadar() + "],";
                        percenvalue += "[" + i + ", " + radar.getPercentRadar() + "],";
                    }
                    i++;
                }
            }
            ticksname += "]";
            actualvalue += "]";
            targetvalue += "]";
            percenvalue += "]";

            print("Ticksname : " + ticksname);
            print("Actualvalue : " + actualvalue);
            print("Targetvalue : " + targetvalue);
            print("Percenvalue : " + percenvalue);

            print("============= load radar color ==============================");
            radarColor = getRadarColorJpaController().findAllOne();

            radarChartContent = new RadarChartContent(ticksname, actualvalue, targetvalue, percenvalue);
        } catch (Exception e) {
            print("Error : " + e);
        }
    }

    private void loadSlideShow() {
        print("============= load slide show ===================================");
        blink = "[";
        linelist = getLineJpaController().findAllLineNotDeletd();
        int i = 0;
        slideshowlist = null;
        for (Line line : linelist) {
            targetResultslist = null;
            nextModelList = null;
            fc.print("==== line id ===="+line.getLineId());
            i = 0;
            manualtargetlist = getManualTargetJpaController().findByLineAndNotDeleted(line);
            for (ManualTarget manualTarget : manualtargetlist) {
                fc.print("Manaul target :"+manualTarget.getIdManaulTarget());
                String resultclass = "black";
                String defectclass = "black";
                String defectrateclass = "black";
                if (Integer.parseInt(manualTarget.getQty()) > Integer.parseInt(manualTarget.getResult())) {
                    resultclass = "red";
                }
                if (Integer.parseInt(manualTarget.getDefect()) > 0) {
                    blink += " 'defect" + i + "',";
                    defectclass = "red";
                }
                if (Integer.parseInt(manualTarget.getDefectRate()) > Integer.parseInt(line.getTargetDefect())) {
                    blink += " 'defectrate" + i + "',";
                    defectrateclass = "red";
                }
                targetResult = new TargetResult(i, manualTarget.getTime(), manualTarget.getQty(), manualTarget.getResult(), manualTarget.getDefect(), manualTarget.getDefectRate(), resultclass, defectclass, defectrateclass);
                getTargetResultslist().add(targetResult);
                if (i >= 6) {
                    break;
                }
                i++;
            }

            nextmodellist = getNextModelJpaController().findByLineAndNotDeleted(line);
            String datenow = getSimpledate(new Date());
            String currentmodel = "";
            nextModelLists = null;
            for (NextModel nextModel : nextmodellist) {
                String dateclass = "next_model";
                String datehistory = getSimpledate(nextModel.getDated());
                currentmodel = nextModel.getModel();
                if (datehistory.equals(datenow)) {
                    dateclass = "now";
                }
                nextModelList = new NextModelList(datehistory, nextModel.getModel(), nextModel.getQty(), dateclass);
                getNextModelLists().add(nextModelList);
            }
            // ทำให้ next model ครบ 7
            for (int l = getNextModelLists().size(); l < 7; l++) {
                print("L : " + l);
                getNextModelLists().add(new NextModelList("-", "-", "-", "next_model"));
            }
            // ทำให้ targetresult ครบ 6
            for (int m = getTargetResultslist().size(); m < 6; m++) {
                print("M : " + m);
                getTargetResultslist().add(new TargetResult(i, "-", "-", "-", "-", "-", "black", "black", "black"));
                i++;
            }
            getSlideshowlist().add(new slideshow(nextModelLists, targetResultslist, line.getLineName(), currentmodel, line.getStdTime(), line.getStdWorker(), n,"./../../images/acmmanaul/"+line.getPicManaul()));
            n++;
        }
        count = getPiclist().size() + getSlideshowlist().size() + 2;
        if (count > 0) {
            contenpx = count * 2000;
        }
        print("ContenPx : " + contenpx);
    }

    public RadarColor getRadarColor() {
        return radarColor;
    }

    public List<Radar> getRadarlist() {
        if (radarlist == null) {
            radarlist = new ArrayList<Radar>();
        }
        return radarlist;
    }

    public RadarChartContent getRadarChartContent() {
        if (radarChartContent == null) {
            radarChartContent = new RadarChartContent();
        }
        return radarChartContent;
    }

    private String getSimpledate(Date date) {
        return new SimpleDateFormat("dd/MM/yyyy").format(date);
    }
    private RadarColorJpaController radarColorJpaController;

    private RadarColorJpaController getRadarColorJpaController() {
        if (radarColorJpaController == null) {
            radarColorJpaController = new RadarColorJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return radarColorJpaController;
    }
    private RadarJpaController radarJpaController;

    private RadarJpaController getRadarJpaController() {
        if (radarJpaController == null) {
            radarJpaController = new RadarJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return radarJpaController;
    }
    private NextModelJpaController nextModelJpaController;

    private NextModelJpaController getNextModelJpaController() {
        if (nextModelJpaController == null) {
            nextModelJpaController = new NextModelJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return nextModelJpaController;
    }
    private LineJpaController lineJpaController;

    private LineJpaController getLineJpaController() {
        if (lineJpaController == null) {
            lineJpaController = new LineJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lineJpaController;
    }
    private ManualTargetJpaController manualTargetJpaController;

    private ManualTargetJpaController getManualTargetJpaController() {
        if (manualTargetJpaController == null) {
            manualTargetJpaController = new ManualTargetJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return manualTargetJpaController;
    }

    public long getContentPx() {
        n = 2;
        loadRadarChart();
        loadPicList();
        loadSlideShow();
        return contenpx;
    }

    public String getBlink() {
//        String blink = "[";
//        for (slideshow slideshow_ : slideshowlist) {
//            List<TargetResult> targetResultslist = slideshow_.getTargetresultlist();
//            for (TargetResult targetResult : targetResultslist) {
//                blink += "'defect" + targetResult.getIndex() + "', 'defectrate" + targetResult.getIndex() + "',";
//            }
//        }
        print(blink + "]");
        return blink + "]";
    }

    private void print(String str) {
        System.out.println(str);
    }

    public List<slideshow> getSlideshowlist() {
        if (slideshowlist == null) {
            slideshowlist = new ArrayList<slideshow>();
//            List<NextModelList> nextmodel = new ArrayList<NextModelList>();
//            nextmodel.add(new NextModelList("26/09/2555", "TTRN-0520H-000T", "2000.00", "next_model"));
//            nextmodel.add(new NextModelList("27/09/2555", "TTRN-0520H-000T", "2000.00", "now"));
//            nextmodel.add(new NextModelList("28/09/2555", "TTRN-0520H-000T", "2000.00", "next_model"));
//            nextmodel.add(new NextModelList("29/09/2555", "TTRN-0520H-000T", "2000.00", "next_model"));
//            nextmodel.add(new NextModelList("30/09/2555", "TTRN-0520H-000T", "2000.00", "next_model"));
//            nextmodel.add(new NextModelList("01/10/2555", "TTRN-0520H-000T", "2000.00", "next_model"));
//            nextmodel.add(new NextModelList("02/10/2555", "TTRN-0520H-000T", "2000.00", "next_model"));
//
//            List<TargetResult> targetresult = new ArrayList<TargetResult>();
//            targetresult.add(new TargetResult(0, "07:00-10:10", "1000.00", "900.00", "100.00", "100000", "red", "red", "red"));
//            targetresult.add(new TargetResult(1, "10:20-12:30", "1000.00", "900.00", "100.00", "100000", "red", "red", "red"));
//            targetresult.add(new TargetResult(2, "07:00-10:10", "1000.00", "900.00", "100.00", "100000", "black", "black", "black"));
//            targetresult.add(new TargetResult(3, "07:00-10:10", "1000.00", "900.00", "100.00", "100000", "red", "red", "red"));
//            targetresult.add(new TargetResult(4, "07:00-10:10", "1000.00", "900.00", "100.00", "100000", "red", "red", "red"));
//
//            slideshowlist.add(new slideshow(nextmodel, targetresult, "line", "model", "standardtime", "standardwork"));
        }
        return slideshowlist;
    }

    public List<TargetResult> getTargetResultslist() {
        if (targetResultslist == null) {
            targetResultslist = new ArrayList<TargetResult>();
        }
        return targetResultslist;
    }

    public List<NextModelList> getNextModelLists() {
        if (nextModelLists == null) {
            nextModelLists = new ArrayList<NextModelList>();
        }
        return nextModelLists;
    }
}
