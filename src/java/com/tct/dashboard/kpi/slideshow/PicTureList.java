/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.dashboard.kpi.slideshow;

/**
 *
 * @author The_Boy_Cs Development by Tawat Saekue Copy Right 4 Xtreme Co.,Ltd.
 */
public class PicTureList {

    private final String urlpic = "./../../images/kpipicture/";
    private String titles;
    private String url;
    private int index;

    public PicTureList(String title, String url, int index) {
        this.titles = title;
        this.url = url;
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getTitles() {
        return titles;
    }

    public void setTitles(String titles) {
        this.titles = titles;
    }

    public String getUrl() {
        print("URL : " + urlpic + url);
        return urlpic + url;
    }

    private void print(String str) {
        System.out.println(str);
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
