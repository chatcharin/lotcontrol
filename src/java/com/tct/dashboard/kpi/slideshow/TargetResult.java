/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.dashboard.kpi.slideshow;

/**
 *
 * @author Tawat
 */
public class TargetResult {
    private String time = "xx:xx-xx:xx";
    private String target = "xx,xxx.xx";
    private String result = "xx,xxx.xx";
    private String resultclass = "red";   // red or black  it's styleclass name
    private String defect = "x,xxx.xx";
    private String defectclass = "red";  // red or black it's styleclass name
    private String defectrate = "x,xxx.xx";
    private String defectrateclass = "red";  // red or black it's styleclass name
    private int index;

    public TargetResult(int index, String time, String target, String result, String defect, String defectrate, String resultclass, String defectclass, String defectrateclass) {
        this.time = time;
        this.target = target;
        this.result = result;
        this.defect = defect;
        this.defectrate = defectrate;
        this.resultclass = resultclass;
        this.defectclass = defectclass;
        this.defectrateclass = defectrateclass;
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getDefectclass() {
        return defectclass;
    }

    public void setDefectclass(String defectclass) {
        this.defectclass = defectclass;
    }

    public String getDefectrateclass() {
        return defectrateclass;
    }

    public void setDefectrateclass(String defectrateclass) {
        this.defectrateclass = defectrateclass;
    }

    public String getResultclass() {
        return resultclass;
    }

    public void setResultclass(String resultclass) {
        this.resultclass = resultclass;
    }

    public String getDefect() {
        return defect;
    }

    public void setDefect(String defect) {
        this.defect = defect;
    }

    public String getDefectrate() {
        return defectrate;
    }

    public void setDefectrate(String defectrate) {
        this.defectrate = defectrate;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
    
}
