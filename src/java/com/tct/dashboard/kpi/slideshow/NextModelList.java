/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.dashboard.kpi.slideshow;

/**
 *
 * @author Tawat
 */
public class NextModelList {

    private String date = "xx/xx/xxxx";
    private String dateclass = "now";  // now or next_model this name for style class
    private String model = "xxxx-xxxxx-xxxx";
    private String qty = "xx,xxx.xx";

    public NextModelList(String date, String model, String qty, String dateclass) {
        this.date = date;
        this.model = model;
        this.qty = qty;
        this.dateclass = dateclass;
    }

    public String getDateclass() {
        return dateclass;
    }

    public void setDateclass(String dateclass) {
        this.dateclass = dateclass;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }
}
