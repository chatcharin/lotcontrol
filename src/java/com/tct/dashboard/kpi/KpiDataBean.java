/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tct.dashboard.kpi;

import com.tct.dashboard.ReportBean;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aek
 *
 */
public class KpiDataBean implements Serializable {

    public static final String PROP_SAMPLE_PROPERTY = "sampleProperty";
    private String sampleProperty;
    private PropertyChangeSupport propertySupport;
    List<DataK> data_kpi = null;

    public KpiDataBean() {
        propertySupport = new PropertyChangeSupport(this);
        findByModel();
//        DataK datak1 = new DataK();
//        datak1.setLine(1);
//        datak1.module_name = "000T";
//        getKPI().add(datak1);
//
//
//        DataK datak2 = new DataK();
//        datak2.setLine(2);
//        datak2.module_name = "001T";
//        getKPI().add(datak2);
//
//
//        DataK datak3 = new DataK();
//        datak3.setLine(3);
//        datak3.module_name = "002T";
//        getKPI().add(datak3);
//
//
//        DataK datak4 = new DataK();
//        datak4.setLine(4);
//        datak4.module_name = "003T";
//        getKPI().add(datak4);
    }

    public Connection getConnection(String brand) {
        if (brand.equals("mysql")) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                return (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tct_project", "root", "root");
            } catch (Exception ex) {
                Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        } else {
            try {
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                return (Connection) DriverManager.getConnection("jdbc:sqlserver://localhost\\SQLEXPRESS:1433;databaseName=tct_project", "test", "1234");
            } catch (Exception ex) {
                Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }

    }

    public String getDate() {
        //'2012-09-02 07:40:00'     
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }
    public String DateNow = "";
    public String start_time_1 = "";
    public String end_time_1 = "";
    public String start_time_2 = "";
    public String end_time_2 = "";
    public String start_time_3 = "";
    public String end_time_3 = "";
    public String start_time_4 = "";
    public String end_time_4 = "";

//    public void setDatetime() {
//        DateNow = getDate() + "00:00:00";
//        start_time_1 = "07:40:00";
//    }
    public String getDateNow() {
        DateNow = getDate() + " 00:00:01";
        return DateNow;
    }

    public String getEnd_time_1() {
        end_time_1 = getDate() + " 09:50:00";
        return end_time_1;
    }

    public String getEnd_time_2() {
        end_time_2 = getDate() + " 11:50:00";
        return end_time_2;
    }

    public String getEnd_time_3() {
        end_time_3 = getDate() + " 15:00:00";
        return end_time_3;
    }

    public String getEnd_time_4() {
        end_time_4 = getDate() + " 17:40:00";
        return end_time_4;
    }

    public String getStart_time_1() {
        start_time_1 = getDate() + " 07:40:00";
        return start_time_1;
    }

    public String getStart_time_2() {
        start_time_2 = getDate() + " 10:00:00";
        return start_time_2;
    }

    public String getStart_time_3() {
        start_time_3 = getDate() + " 12:30:00";
        return start_time_3;
    }

    public String getStart_time_4() {
        start_time_4 = getDate() + " 15:10:00";
        return start_time_4;
    }
//    
//    SELECT current_process.`time` AS current_process_time, details.`model` AS details_model, details.`model_series` AS details_model_series
//FROM  `details` details
//RIGHT OUTER JOIN  `lot_control` lot_control ON details.`id_detail` = lot_control.`details_id_detail` 
//RIGHT OUTER JOIN  `main_data` main_data ON lot_control.`id` = main_data.`lot_control_id` 
//RIGHT OUTER JOIN  `current_process` current_process ON main_data.`id` = current_process.`main_data_id` 
//WHERE  `time` >  '2012-09-01 00:00:00'
//GROUP BY details_model, details_model_series
    public List<String> model = new ArrayList<String>();
    public List<String> series = new ArrayList<String>();

    private void findByModel() {
        data_kpi = null;
//        String sql =" SELECT current_process.time AS current_process_time, details.model AS details_model, details.model_series AS details_model_series"
//                + " FROM  details details "
//                + " RIGHT OUTER JOIN  lot_control lot_control ON details.id_detail = lot_control.details_id_detail "
//                + " RIGHT OUTER JOIN  `main_data` main_data ON lot_control.`id` = main_data.`lot_control_id` "
//                + " RIGHT OUTER JOIN  `current_process` current_process ON main_data.`id` = current_process.`main_data_id` ";
//        //sql += " WHERE  `time` >  '"+DateNow+"' ";
//        sql += " WHERE  `time` >  '2012-09-01 00:00:00' "; 
//        sql += " GROUP BY details_model, details_model_series ";

        String sql = ""; // this query source by tawat  ===============
        sql += "SELECT"
                + "     target.targettime AS target_targettime," // object index 1
                + "     model_pool.model_name AS model_pool_model_name," // object index 2
                + "     model_pool.model_series AS model_pool_model_series," // object index 3

                + "     target.line AS target_line," // object index 4

                + "     target.date_create AS target_date_create," // object index 5

                + "     target.target_qty AS target_target_qty," // object index 6

                + "     SUM(lot_control.qty_output) AS lot_control_qty_output," // object index 7

                + "     model_pool.model_group AS model_pool_model_group,"
                + "     model_pool.model_type AS model_pool_model_type,"
                + "     lot_control.barcode AS lot_control_barcode," // object index 10
                + "     current_process.line AS current_process_line,"
                + "     main_data.current_number AS main_data_current_number"
                + " FROM"
                + "     model_pool model_pool INNER JOIN target target ON model_pool.id = target.model_id"
                + "     INNER JOIN lot_control lot_control ON model_pool.id = lot_control.model_pool_id"
                + "     INNER JOIN main_data main_data ON lot_control.id = main_data.lot_control_id"
                + "     INNER JOIN current_process current_process ON main_data.id = current_process.main_data_id"
                + "     AND main_data.current_number = current_process.id"
                + " WHERE"
                + " current_process.line = target.line AND"
                //                + " target.date_create < '" + getDateNow() + "'"
                + " target.date_create > current_date"
                + "  AND current_process.time > current_date"
                + " AND lot_control.deleted = 0"
                + " AND current_process.status = 'Finish'"
                + " GROUP BY target.line,target.targettime";  //============
        System.out.println("sql : " + sql);
        Connection con = getConnection("mysql");
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                System.out.println("Time   :" + rs.getObject(1).toString());
                System.out.println("Model  :" + rs.getObject(2).toString());
                System.out.println("Series :" + rs.getObject(3).toString());
                System.out.println("Barcode : " + rs.getObject(10).toString()); //              model.add(rs.getObject(2).toString());
                        //              series.add(rs.getObject(3).toString());
                for (int i = 1; i <= 4; i++) {

                    if (rs.getObject(4).equals("" + i)) {
                        getDataK().setLine(1);
                        getDataK().setModule_name(rs.getObject(2).toString());

                        if (rs.getObject(1).equals("07:40-09:50")) {
                            String line1Sql = "SELECT"
                                    + "     ng_normal.`name` AS ng_normal_name," // object index 1
                                    + "     SUM(ng_normal.`qty`) AS ng_normal_qty,"// object index 2
                                    + "     current_process.`id` AS current_process_id,"
                                    + "     current_process.`status` AS current_process_status,"
                                    + "     current_process.`time` AS current_process_time,"
                                    + "     current_process.`line` AS current_process_line"
                                    + " FROM"
                                    + "     `current_process` current_process "
                                    + " INNER JOIN `ng_normal` ng_normal ON current_process.`id` = ng_normal.`current_process_id`"
                                    + " WHERE"
                                    + "     (current_process.`status` = 'Finish'"
                                    + "  OR current_process.`status` = 'Close')"
                                    + " AND current_process.`time` < '" + getEnd_time_1() + "' AND current_process.`time` > '" + getStart_time_1() + "'"
                                    + " AND current_process.`line` = '" + i + "' GROUP BY current_process.`line`";
                            System.out.println(" sql ng normal1 : " + line1Sql);

//                            Connection cons = getConnection("mysql");
                            Statement sts = con.createStatement();
                            ResultSet l1rs = sts.executeQuery(line1Sql);
                            double defact = 0;
                            int lop = 0;
                            while (l1rs.next()) {
                                System.out.println(" aaa1 :" + l1rs.getObject(2).toString());
                                defact = Double.parseDouble(l1rs.getObject(2).toString());
                                System.out.println("Defact1 : " + defact);
                                lop++;
                                getDataK().setDefect_line1(defact);
                            }
                            System.out.println("Lop1 : " + lop);
                            double target = Double.parseDouble(rs.getObject(6).toString());
                            System.out.println("Target 1 : " + target);
                            getDataK().setTarget_line1(target);
                            double result = Double.parseDouble(rs.getObject(7).toString());
                            System.out.println("result : " + result);
                            getDataK().setResult_line1(result);
//                            double defact = CalJudgment(target, result);
//                            System.out.println("Defact : " + defact);
//                            getDataK().setDefect_line1(defact);
//                            double defactrate = Calrate(defact, result);
//                            System.out.println("Defect rate : " + defactrate);
//                            getDataK().setDefect_rate_line1(defactrate);
//                            getDataK().checkFail();
                        }
                        if (rs.getObject(1).equals("10:00-11:50")) {
                            String line1Sql = "SELECT"
                                    + "     ng_normal.`name` AS ng_normal_name," // object index 1
                                    + "     SUM(ng_normal.`qty`) AS ng_normal_qty,"// object index 2
                                    + "     current_process.`id` AS current_process_id,"
                                    + "     current_process.`status` AS current_process_status,"
                                    + "     current_process.`time` AS current_process_time,"
                                    + "     current_process.`line` AS current_process_line"
                                    + " FROM"
                                    + "     `current_process` current_process "
                                    + " INNER JOIN `ng_normal` ng_normal ON current_process.`id` = ng_normal.`current_process_id`"
                                    + " WHERE"
                                    + "     (current_process.`status` = 'Finish'"
                                    + "  OR current_process.`status` = 'Close')"
                                    + " AND current_process.`time` < '" + getEnd_time_2() + "' AND current_process.`time` > '" + getStart_time_2() + "'"
                                    + " AND current_process.`line` = '" + i + "' GROUP BY current_process.`line`";
                            System.out.println(" sql ng normal2 : " + line1Sql);

//                            Connection cons = getConnection("mysql");
                            Statement sts = con.createStatement();
                            ResultSet l1rs = sts.executeQuery(line1Sql);
                            double defact = 0.0;
                            int lop = 0;
                            while (l1rs.next()) {
                                System.out.println(" aaa2 :" + l1rs.getObject(2).toString());
                                defact = Double.parseDouble(l1rs.getObject(2).toString());
                                System.out.println("Defact2 : " + defact);
                                lop++;
                                getDataK().setDefect_line1(defact);
                            }
                            System.out.println("Lop2 : " + lop);
                            double target = Double.parseDouble(rs.getObject(6).toString());
                            System.out.println("Target 1 : " + target);
                            getDataK().setTarget_line2(target);
                            double result = Double.parseDouble(rs.getObject(7).toString());
                            System.out.println("result : " + result);
                            getDataK().setResult_line2(result);
//                            double defact = CalJudgment(target, result);
//                            System.out.println("Judgment : " + defact);
//                            System.out.println("Defact : " + defact);
//                            getDataK().setDefect_line2(defact);
//                            double defactrate = Calrate(defact, result);
//                            System.out.println("Defect rate : " + defactrate);
//                            getDataK().setDefect_rate_line2(defactrate);
//                            getDataK().checkFail();
                        }
                        if (rs.getObject(1).equals("12:30-15:00")) {
                            String line1Sql = "SELECT"
                                    + "     ng_normal.`name` AS ng_normal_name," // object index 1
                                    + "     SUM(ng_normal.`qty`) AS ng_normal_qty,"// object index 2
                                    + "     current_process.`id` AS current_process_id,"
                                    + "     current_process.`status` AS current_process_status,"
                                    + "     current_process.`time` AS current_process_time,"
                                    + "     current_process.`line` AS current_process_line"
                                    + " FROM"
                                    + "     `current_process` current_process "
                                    + " INNER JOIN `ng_normal` ng_normal ON current_process.`id` = ng_normal.`current_process_id`"
                                    + " WHERE"
                                    + "     (current_process.`status` = 'Finish'"
                                    + "  OR current_process.`status` = 'Close')"
                                    + " AND current_process.`time` < '" + getEnd_time_3() + "' AND current_process.`time` > '" + getStart_time_3() + "'"
                                    + " AND current_process.`line` = '" + i + "' GROUP BY current_process.`line`";
                            System.out.println(" sql ng normal3 : " + line1Sql);

//                            Connection cons = getConnection("mysql");
                            Statement sts = con.createStatement();
                            ResultSet l1rs = sts.executeQuery(line1Sql);
                            double defact = 0;
                            int lop = 0;
                            while (l1rs.next()) {
                                System.out.println(" aaa3 :" + l1rs.getObject(2).toString());
                                defact = Double.parseDouble(l1rs.getObject(2).toString());
                                System.out.println("Defact3 : " + defact);
                                lop++;
                                getDataK().setDefect_line1(defact);
                            }
                            System.out.println("Lop3 : " + lop);
                            double target = Double.parseDouble(rs.getObject(6).toString());
                            System.out.println("Target 1 : " + target);
                            getDataK().setTarget_line3(target);
                            double result = Double.parseDouble(rs.getObject(7).toString());
                            System.out.println("result : " + result);
                            getDataK().setResult_line3(result);
//                            double defact = CalJudgment(target, result);
//                            System.out.println("Judgment : " + defact);
//                            System.out.println("Defact : " + defact);
//                            getDataK().setDefect_line3(defact);
//                            double defactrate = Calrate(defact, result);
//                            System.out.println("Defect rate : " + defactrate);
//                            getDataK().setDefect_rate_line3(defactrate);
//                            getDataK().checkFail();
                        }
                        if (rs.getObject(1).equals("15:10-17:40")) {
                            String line1Sql = "SELECT"
                                    + "     ng_normal.`name` AS ng_normal_name," // object index 1
                                    + "     SUM(ng_normal.`qty`) AS ng_normal_qty,"// object index 2
                                    + "     current_process.`id` AS current_process_id,"
                                    + "     current_process.`status` AS current_process_status,"
                                    + "     current_process.`time` AS current_process_time,"
                                    + "     current_process.`line` AS current_process_line"
                                    + " FROM"
                                    + "     `current_process` current_process "
                                    + " INNER JOIN `ng_normal` ng_normal ON current_process.`id` = ng_normal.`current_process_id`"
                                    + " WHERE"
                                    + "     (current_process.`status` = 'Finish'"
                                    + "  OR current_process.`status` = 'Close')"
                                    + " AND current_process.`time` < '" + getEnd_time_4() + "' AND current_process.`time` > '" + getStart_time_4() + "'"
                                    + " AND current_process.`line` = '" + i + "' GROUP BY current_process.`line`";
                            System.out.println(" sql ng normal4 : " + line1Sql);

//                            Connection cons = getConnection("mysql");
                            Statement sts = con.createStatement();
                            ResultSet l1rs = sts.executeQuery(line1Sql);
                            double defact = 0;
                            int lop = 0;
                            while (l1rs.next()) {
                                System.out.println(" aaa4 :" + l1rs.getObject(2).toString());
                                defact = Double.parseDouble(l1rs.getObject(2).toString());
                                System.out.println("Defact4 : " + defact);
                                lop++;
                                getDataK().setDefect_line1(defact);
                            }
                            System.out.println("Lop4 : " + lop);
                            double target = Double.parseDouble(rs.getObject(6).toString());
                            System.out.println("Target 1 : " + target);
                            getDataK().setTarget_line4(target);
                            double result = Double.parseDouble(rs.getObject(7).toString());
                            System.out.println("result : " + result);
                            getDataK().setResult_line4(result);
//                            double defact = CalJudgment(target, result);
//                            System.out.println("Judgment : " + defact);
//                            System.out.println("Defact : " + defact);
//                            getDataK().setDefect_line4(defact);
//                            double defactrate = Calrate(defact, result);
//                            System.out.println("Defect rate : " + defactrate);
//                            getDataK().setDefect_rate_line4(defactrate);
//                            getDataK().checkFail();
                        }
                        getDataK().checkFail();
                        getKPI().add(getDataK());
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.print("Error : " + ex);
            Logger.getLogger(KpiDataBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private double Calrate(double deface, double result) {
        double rate = (deface / result) * 1000000;
        return rate;
    }

    private double CalJudgment(double a, double b) {
        System.out.println("A : " + a);
        System.out.println("B : " + b);
        double result = (b / a) * 100;
        System.out.println(" Result : " + result);
        return result;
    }
    private DataK datak = null;

    private DataK getDataK() {
        if (datak == null) {
            datak = new DataK();
        }
        return datak;
    }

    public void findKpi(String type) {
//        try {
//               
//                st=con.createStatement();
//                System.out.println("step  -------------------------------  [0]");
//            } catch (SQLException ex) {
//                Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            try {
//                rs =st.executeQuery(sql);
//                System.out.println("step  -------------------------------  [1]");
//            } catch (SQLException ex) {
//                Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
//            }
    }

    public String getSampleProperty() {
        return sampleProperty;
    }

    public void setSampleProperty(String value) {
        String oldValue = sampleProperty;
        sampleProperty = value;
        propertySupport.firePropertyChange(PROP_SAMPLE_PROPERTY, oldValue, sampleProperty);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.removePropertyChangeListener(listener);
    }

    public List<DataK> getKPI() {
        if (data_kpi == null) {
            data_kpi = new ArrayList<DataK>();
//            findByModel();
        }
        return data_kpi;
    }
}
