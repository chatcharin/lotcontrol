/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kpipicture.Data;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Oct 19, 2012, Time : 11:00:29 AM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class DefaultAcmChart {
    private  String index;
    private  String lineName;

    public DefaultAcmChart(String index, String lineName) {
        this.index = index;
        this.lineName = lineName;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getLineName() {
        return lineName;
    }

    public void setLineName(String lineName) {
        this.lineName = lineName;
    } 
}
