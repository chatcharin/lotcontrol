/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kpipicture.Data;

/**
 *
 * @author panglao
 */
public class KpiPictureData {
    private String index;
    private String number;
    private String picture;
    private String titleName;
    private String show;

    public KpiPictureData(String index, String number, String picture, String titleName, String show) {
        this.index = index;
        this.number = number;
        this.picture = picture;
        this.titleName = titleName;
        this.show = show;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getShow() {
        return show;
    }

    public void setShow(String show) {
        this.show = show;
    }

    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    } 
}
