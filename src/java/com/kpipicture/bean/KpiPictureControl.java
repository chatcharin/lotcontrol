/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kpipicture.bean;

import com.appCinfigpage.bean.fc;
import com.appCinfigpage.bean.userLogin;
import com.kpipicture.Data.KpiPictureData;
import com.setting.DataProcess.NGPool;
import com.tct.data.jsf.util.PaginationHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import com.tct.data.KpiPicture;
import com.tct.data.jpa.KpiPictureJpaController;
import java.io.*;
import java.util.Collections;
import javax.faces.context.FacesContext;
import javax.persistence.Persistence;
import org.icefaces.ace.component.fileentry.FileEntry;
import org.icefaces.ace.component.fileentry.FileEntryEvent;
import org.icefaces.ace.component.fileentry.FileEntryResults;
/**
 *
 * @author panglao
 */
public class KpiPictureControl {
    private List<KpiPictureData> itemPictue;
    private KpiPicture  cuurentKpiPicture;
    private String closepopup = "window.close();";
    private String showpopup;
    private String idPic;
    private KpiPictureJpaController  kpiPictureJpaController = null;
    private String linkImage ;
    private boolean statusButtonUpLoad = false;
    private boolean statusButtonSave = false;
    private DataModel<KpiPictureData>  kpiPictureModel;
    public KpiPictureControl() {
        loadDataAll();
    }
    public void loadDataAll()
    {
        prapareValueDel();
        linkImage  = "./images/no_image.jpg";
    }
    public KpiPictureJpaController getKpiPictureJpaController()
    {
        if(kpiPictureJpaController ==null)
        {
            kpiPictureJpaController  = new KpiPictureJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return kpiPictureJpaController;
    } 
    public void changeToCancel()
    {
        fc.print("==== change to cancel picture ===="); 
        cuurentKpiPicture  = new KpiPicture();
        linkImage  = "./images/no_image.jpg";
        statusButtonSave     = false;
        statusButtonUpLoad   = true;
//        String link = "report/index.xhtml";
//        userLogin.getSessionMenuBarBean().setParam(link);
    }
    public void changeToUpload()
    {
        fc.print("==== change to upload picture ===="); 
        if(checkTitleName() == true){
        
        getItemPictue().add(new KpiPictureData(idPic, getCuurentKpiPicture().getTitleKpiPicture(),getCuurentKpiPicture().getPictureUrl(),getCuurentKpiPicture().getTitleKpiPicture(),getCuurentKpiPicture().getShowPicture()));
        
        setAutoNumber(); 
        for(KpiPictureData objList:getItemPictue())
        {
            cuurentKpiPicture  = new KpiPicture(); 
            fc.print("id "+objList.getIndex());
            cuurentKpiPicture  = getKpiPictureJpaController().findKpiPicture(objList.getIndex());
            if(getCuurentKpiPicture().getIdKpiPicture() == null){
            if(objList.getIndex().equals(idPic))
            { 
                    getCuurentKpiPicture().setSequences(Integer.parseInt(objList.getNumber())); 
                    getCuurentKpiPicture().setDeleted(0);
                    getCuurentKpiPicture().setIdKpiPicture(idPic);
                    getCuurentKpiPicture().setPictureUrl(objList.getPicture());
                    getCuurentKpiPicture().setShowPicture(objList.getShow());
                    getCuurentKpiPicture().setTitleKpiPicture(objList.getTitleName());
                    createKpiPicture();
                } 
             }
                else
                {
                     getCuurentKpiPicture().setSequences(Integer.parseInt(objList.getNumber()));
                     editKpiPicture();
                }
             
          }  
        }
        setKpiPictureModel();
        resetItemPicture();
    } 
    public void editKpiPicture()
    {
        try
        {
           getKpiPictureJpaController().edit(cuurentKpiPicture);
        }
        catch(Exception e)
        {
            fc.print("====== Error  not to update number ."+e);
        }
    
    }
    public void createKpiPicture()
    {
        try
        {
            getKpiPictureJpaController().create(getCuurentKpiPicture());
        }
        catch(Exception e)
        {
            fc.print("====== Error not to insert kpiPicture ======"+e);
        }
    }
    public boolean checkTitleName()
    {
        boolean check = false;
        if(!getCuurentKpiPicture().getTitleKpiPicture().trim().isEmpty() && getCuurentKpiPicture().getTitleKpiPicture()!= null)
        {
            check = true;
        }
        else
        {
            fc.print("======== Invalid input is null ===========");
        }
        return check;
    }
    public void resetItemPicture()
    {
        cuurentKpiPicture  = new KpiPicture();
        idPic = null;
    }
    public void changeToDelelte(String indexDel)
    {
        fc.print("===== change to delete =====");
//        String idDel  = getItemPictue().get(worksheet.getRowIndex()).getIndex();
        
        for(KpiPictureData objList:getItemPictue())
        {
            if(objList.getIndex().equals(indexDel))
            {
                 getItemPictue().remove(objList);
                 cuurentKpiPicture  = new KpiPicture();
                 cuurentKpiPicture  = getKpiPictureJpaController().findKpiPicture(objList.getIndex()); 
                 getCuurentKpiPicture().setDeleted(1); 
                 editKpiPicture();
                 break;
            }
        } 
       prapareValueDel(); 
       setAutoNumber();
       cuurentKpiPicture = new KpiPicture();
    } 
    public void prapareValueDel()
    {
       int i = 0 ;
       itemPictue  = new ArrayList<KpiPictureData>();
       List<KpiPicture>  objKpiPicture  = getKpiPictureJpaController().findAllNotDelete();
       for(KpiPicture objList:objKpiPicture)
       {    i++;
            getItemPictue().add(new KpiPictureData(objList.getIdKpiPicture(),Integer.toString(i),objList.getPictureUrl(),objList.getTitleKpiPicture(),objList.getShowPicture()));
       }
       setKpiPictureModel();
        statusButtonSave     = false;
        statusButtonUpLoad   = true;
    }
    public void setAutoNumber()
    {
        int  i = 0 ; 
        for(KpiPictureData objList: getItemPictue())
        {
            i++;
            objList.setNumber(Integer.toString(i)); 
            cuurentKpiPicture  = new KpiPicture();
            cuurentKpiPicture = getKpiPictureJpaController().findKpiPicture(objList.getIndex());
            if(getCuurentKpiPicture().getIdKpiPicture() != null)
            {
            try
            {
                getKpiPictureJpaController().edit(getCuurentKpiPicture());
            }
            catch(Exception e)
            {
                fc.print("=== Error not to edit =="+e);
            }
            }
        }
        cuurentKpiPicture  = new KpiPicture();
    }
    public String gemId()
    {
        String id = UUID.randomUUID().toString();
        return id;
    }
    public List<KpiPictureData> getItemPictue() {
        if(itemPictue == null)
        {
            itemPictue  = new ArrayList<KpiPictureData>();
        }
        return itemPictue;
    } 
    public void setItemPictue(List<KpiPictureData> itemPictue) {
        this.itemPictue = itemPictue;
    } 
    public void uploadImage()
    {
        fc.print("================= upload image =======================");
        closepopup = "";
        showpopup = "window.showModalDialog('pages/setting/kpipicture/popuploadpic.xhtml', 'Upload pic', 'dialig');";
        
    }
     public void listener(FileEntryEvent event) {
        fc.print("====================== listener ===============");
        try {
            FileEntry fileEntry = (FileEntry) event.getSource();
            FileEntryResults results = fileEntry.getResults();
            String partfile = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/images/kpipicture/");
            for (FileEntryResults.FileInfo fileInfo : results.getFiles()) {
                if (fileInfo.isSaved()) {
                    fc.print("== filename 1 : " + fileInfo.getFileName());
                    File file = fileInfo.getFile();
                    fc.print("=== file name 2 : " + file.getName());
//                    String filename = file.getName();
                    String[] arrname = file.getName().split("\\.");
                    fc.print("= Lenght : " + arrname.length);
                    fc.print(" == filetype : " + arrname[1]);
                    String filename;
                    if(getCuurentKpiPicture().getIdKpiPicture() != null){
                        filename = getCuurentKpiPicture().getIdKpiPicture() + "." + arrname[arrname.length - 1];
                    }
                    else
                    {
                        idPic = gemIdPic();
                        filename = idPic+ "." + arrname[arrname.length - 1];
                    }
                    fc.print("= file name 3 : " + filename);
                    partfile += "/" + filename;
                    if (writFile(file, partfile)) {
                        getCuurentKpiPicture().setPictureUrl(filename);
                        showpopup = "";
                        closepopup = "window.close();";
                    }
                }
            }
        } catch (Exception e) {
            fc.print("Error0 : " + e);
        }
    }
    public String gemIdPic()
    {
        String id = UUID.randomUUID().toString();
        return id;
    }
    private boolean writFile(File file, String filePath) throws FileNotFoundException, IOException {
        fc.print("========== Create file ==================================");
        fc.print("= file parth : " + filePath);
        try {
            File outputFile = new File(filePath);
            FileInputStream in = new FileInputStream(file);
            FileOutputStream out = new FileOutputStream(outputFile);
            int c;
            while ((c = in.read()) != -1) {
                out.write(c);
            }
            file.delete();
            in.close();
            out.close();
            return true;
        } catch (IOException ioe) {
            fc.print("= IO Error3 : " + ioe);
            return false;
        } catch (Exception e) {
            fc.print("= Error2 : " + e);
            return false;
        }
    }
    public void changeToEdit(String indexEdit)
    {
        fc.print("===== Change To Edit ======== : "+indexEdit);
        cuurentKpiPicture  = new KpiPicture();
        cuurentKpiPicture  = getKpiPictureJpaController().findKpiPicture(indexEdit);
        linkImage  = "./images/kpipicture/"+getCuurentKpiPicture().getPictureUrl();
        statusButtonSave  = true;
        statusButtonUpLoad  = false;
    } 
    public void changeToSave()
    {
        fc.print("====== Change To Save =====");
        editKpiPicture();
        cuurentKpiPicture  = new KpiPicture();
        itemPictue = new ArrayList<KpiPictureData>();
        loadDataAll();
    }
//    public DataModel getWorksheet() {
//        return worksheet;
//    }
//
//    public void setWorksheet() {
//        this.worksheet = getWorksheetpagination().createPageDataModel();
//    }
//    public void previous() {
//        getWorksheetpagination().previousPage();
//        setWorksheet();
//    }
//
//    public void next() {
//        getWorksheetpagination().nextPage();
//        setWorksheet();
//    }
//
//    public int getAllPages() {
//        int all = getWorksheetpagination().getItemsCount();
//        int pagesize = getWorksheetpagination().getPageSize();
//        return ((int) all / pagesize) + 1;
//    } 
//    public PaginationHelper getWorksheetpagination() {
//        if (worksheetpagination == null) {
//            worksheetpagination = new PaginationHelper(20) {
//
//                @Override
//                public int getItemsCount() {
//                    return getItemPictue().size();
//                }
//
//                @Override
//                public DataModel createPageDataModel() {
//                    fc.print("PageFirst : " + getPageFirstItem());
//                    fc.print("PageLase : " + getPageLastItem());
//                    ListDataModel listmodel;
//                    if (getItemPictue().size() > 0) {
//                        listmodel = new ListDataModel(getItemPictue().subList(getPageFirstItem(), getPageLastItem() + 1));
//                    } else {
//                        listmodel = new ListDataModel(getItemPictue());
//                    }
//                    return listmodel;
//                }
//            };
//        }
//        return worksheetpagination;
//    }   
    
  public void up()
    { 
       KpiPictureData rowdataUp = getKpiPictureModel().getRowData();
       if(getKpiPictureModel().getRowIndex()!=0){
        for(KpiPictureData  objListProcess: itemPictue)
        {
            if(rowdataUp.equals(objListProcess))
            {
                Collections.swap(itemPictue,getKpiPictureModel().getRowIndex(),getKpiPictureModel().getRowIndex()-1);
                fc.print("up");
                break;
            }
        } 
        updateSequence();
        setAutoNumber();
        setKpiPictureModel();
       }
      // fc.print("========== up ========== : "+processed);
    }
    public void updateSequence()
    {
        int i = 0;
        for(KpiPictureData objList : getItemPictue())
        {
            i++;
            cuurentKpiPicture  = new KpiPicture();
            cuurentKpiPicture  =  getKpiPictureJpaController().findKpiPicture(objList.getIndex());
            getCuurentKpiPicture().setSequences(i);
            editKpiPicture();
        }
         
    }
    public void down()
    { 
       KpiPictureData rowdataDown = getKpiPictureModel().getRowData();
       try{
            int moveDownIndex   =  getKpiPictureModel().getRowIndex()+1; 
           for(KpiPictureData  objListProcess: itemPictue)
            {
                if(rowdataDown.equals(objListProcess))
                {
                    Collections.swap(itemPictue,getKpiPictureModel().getRowIndex(),moveDownIndex);
                    fc.print("down index "+getKpiPictureModel().getRowIndex()+" move to index "+moveDownIndex);
                    break;
                }
            }
            updateSequence();
            setAutoNumber();
            setKpiPictureModel();}
       catch(Exception e)
       {
           fc.print("Error  : not to down"+e);
       }
    } 
    private PaginationHelper processpagination;

    public PaginationHelper getProcesspagination() {
        if (processpagination == null) {
            processpagination = new PaginationHelper(getItemPictue().size()) {

                @Override
                public int getItemsCount() {
                    return getItemPictue().size();
                }

                @Override
                public DataModel<NGPool> createPageDataModel() {
                    fc.print("PageFirst : " + getPageFirstItem());
                    fc.print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel = null;
//                    if (getProcesspoollist().size() > 0) {
//                        listmodel = new ListDataModel(getProcesspoollist().subList(getPageFirstItem(), getPageLastItem() + 1));
//                    } else {
                    listmodel = new ListDataModel(getItemPictue());
//                    }
                    return listmodel;
                }
            };
        }
        return processpagination;
    }
    public KpiPicture getCuurentKpiPicture() {
        if(cuurentKpiPicture == null)
        {
            cuurentKpiPicture = new KpiPicture();
        }
        return cuurentKpiPicture;
    }

    public void setCuurentKpiPicture(KpiPicture cuurentKpiPicture) {
        this.cuurentKpiPicture = cuurentKpiPicture;
    }

    public String getClosepopup() {
        return closepopup;
    }

    public void setClosepopup(String closepopup) {
        this.closepopup = closepopup;
    }

    public String getShowpopup() {
        return showpopup;
    }

    public void setShowpopup(String showpopup) {
        this.showpopup = showpopup;
    }

    public String getIdPic() { 
        return idPic;
    }

    public void setIdPic(String idPic) {
        this.idPic = idPic;
    }

    public String getLinkImage() {
        return linkImage;
    }

    public void setLinkImage(String linkImage) {
        this.linkImage = linkImage;
    }

    public boolean isStatusButtonSave() {
        return statusButtonSave;
    }

    public void setStatusButtonSave(boolean statusButtonSave) {
        this.statusButtonSave = statusButtonSave;
    }

    public boolean isStatusButtonUpLoad() {
        return statusButtonUpLoad;
    }

    public void setStatusButtonUpLoad(boolean statusButtonUpLoad) {
        this.statusButtonUpLoad = statusButtonUpLoad;
    }

    public DataModel<KpiPictureData> getKpiPictureModel() {
        return kpiPictureModel;
    }

    public void setKpiPictureModel() {
        this.kpiPictureModel = getProcesspagination().createPageDataModel();
    }
    
}
