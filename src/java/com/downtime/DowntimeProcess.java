package com.downtime;

import com.appCinfigpage.bean.fc;
import com.moduleFontline.DataWorksheet.barcode;
import java.util.ArrayList;
import java.util.List;
import com.component.barcodeauto.BarcodeAuto;
import javax.faces.model.SelectItem; 
import com.tct.data.LotControl;
import com.tct.data.jpa.LotControlJpaController;
import javax.persistence.Persistence;
import com.tct.data.jpa.MainDataJpaController;
import com.tct.data.MainData;
import com.tct.data.CurrentProcess;
import com.tct.data.jpa.CurrentProcessJpaController;
import com.openProcess.openProcess;
import com.shareData.ShareDataOfSession;
import com.tct.data.Mc;
import com.tct.data.jpa.McJpaController;
import javax.faces.event.ValueChangeEvent;
import com.tct.data.Md2pc;
import com.tct.data.jpa.Md2pcJpaController;
import com.tct.data.jsf.util.JsfUtil;
import com.time.timeCurrent;
import java.util.ResourceBundle;
import com.tct.data.McDown;
import com.tct.data.jpa.McDownJpaController;
import com.tct.data.PcsQty;
import com.tct.data.jpa.PcsQtyJpaController;


import java.util.UUID;
import javax.faces.context.FacesContext;

public class DowntimeProcess {
	//----------downtime popup-----------------------
	 private boolean  downtimeProcessPopup            = false;
	 private boolean  downtimeProcessPopupConfirm     = false; 
	 private boolean  downtimeProcessPopupConfirmYes  = false;
	 private boolean  downtimeProcessPopupConfirmNo   = false; 
	
	 //----------------------------------------------
	 private String  typeClose;
	 private String  visibles;
	 
	 //--------select reason------------------------
	 private List<SelectItem> type;
	 private String typeselected;
	 private List<SelectItem> Series;
	 private String seriesselected;
	 private List<SelectItem> Model;
	 private String moduleselected;
		
	 private String newtype;
	 private String newseries;
	 private String newmodel;
	
         private  String mcName =null;   
         private  LotControlJpaController  lotControlJpaController = null;
         private  MainDataJpaController   mainDataJpaController  = null;
         private  CurrentProcessJpaController  currentProcessJpaController  = null;
         private  McJpaController mcJpaController = null;
         private  Md2pcJpaController  md2pcJpaController  = null; 
         private CurrentProcess currentProcessAdd =null;
         private int selectedItemIndex;
         private McDownJpaController  mcDownJpaController  = null;
         private PcsQtyJpaController  pcsQtyJpaController  = null;
         
         private String validateReason    = null;
         private String validateMcWipQty  = null;
         private String qtyWip = "0";
         public ShareDataOfSession getShareData(){
          FacesContext context = FacesContext.getCurrentInstance();
          return   (ShareDataOfSession) context.getExternalContext().getSessionMap().get("sharedata");  
        }  
         public BarcodeAuto getBarcodeAuto(){  
            FacesContext context = FacesContext.getCurrentInstance();
            return (BarcodeAuto) context.getExternalContext().getSessionMap().get("barcode"); 
        } 
        
         
         public  LotControlJpaController  getlLotControlJpaController(){
             if(lotControlJpaController ==null){
                 lotControlJpaController  = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU")); 
             }
             return lotControlJpaController; 
         }
         
         public  MainDataJpaController  getlMainDataJpaController(){
             if(mainDataJpaController ==null){
                 mainDataJpaController  = new MainDataJpaController(Persistence.createEntityManagerFactory("tct_projectxPU")); 
             }
             return mainDataJpaController; 
         }
         
          public  CurrentProcessJpaController  getCurrentProcessJpaController(){
             if(currentProcessJpaController ==null){
                 currentProcessJpaController  = new CurrentProcessJpaController(Persistence.createEntityManagerFactory("tct_projectxPU")); 
             }
             return currentProcessJpaController; 
         } 
          public  McJpaController  getMcController(){
             if(mcJpaController ==null){
                 mcJpaController  = new McJpaController(Persistence.createEntityManagerFactory("tct_projectxPU")); 
             }
             return mcJpaController; 
         } 
         public Md2pcJpaController getMd2pcJpaController(){
             if(md2pcJpaController == null){
                 md2pcJpaController  = new Md2pcJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
             }
             return md2pcJpaController;
         } 
         public McDownJpaController getMcDownJpaController(){
             if(mcDownJpaController ==null){
                 mcDownJpaController = new McDownJpaController(Persistence.createEntityManagerFactory("tct_projectxPU")); 
             }
             return mcDownJpaController;
         } 
         public PcsQtyJpaController getPcsQtyJpaController(){
             if(pcsQtyJpaController  == null){
                 pcsQtyJpaController  = new PcsQtyJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
             }
             return pcsQtyJpaController;
         }
	 public void closePopup(String typeClose) {
		   this.typeClose = typeClose;
	 if(this.typeClose.equals("downtimeProcessPopup"))
	  {
               
//		  downtimeProcessPopupConfirm  = true;
              int wipMcQty = 0;  
              boolean check  = false;  
              try
                {
                    wipMcQty  = Integer.parseInt(qtyWip);
                    check = true;
                }
                catch(Exception e)
                {
                    System.out.println("==== Error conver wip qty "+e);
                }
               
                if(!reasonSelectOne.equals("0") && check)
                {
                    downtimeProcessPopup         = false;    
                    insertDowntime();
                    getShareData().openProcessButton      = true;
                    getBarcodeAuto().openProcessButtonShow  = "open_process_button_disable";
                    getShareData().mcDownTimeButton       = true;
                    getBarcodeAuto().mcDownTimeButtonShow   = "mc_downtime_button_disable";
                    getShareData().stopTimeButton         = true;
                    getBarcodeAuto().stopTimeButtonShow     = "stop_downtime_button_disable";
                    getShareData().moveProcessButton      = true;
                    getBarcodeAuto().moveProcessButtonShow  = "move_mc_button_disable";
                    getShareData().holdTimeButton         =  true;
                    getBarcodeAuto().holdTimeButtonShow     = "hold_process_button_disable";
                    getShareData().closeProcessButton     = true; 
                    getBarcodeAuto().closeProcessButtonShow = "close_process_button_disable";
                    getBarcodeAuto().status  = "Downtime"; 
                    validateReason  = "";
                    reasonSelectOne = "0";
                    qtyWip          = "0";
                    remark          = null;
                }
                else
                {
                    if(!check)
                    {
                        validateMcWipQty  = "Invalid input";
                    }
                    if(check)
                    {
                        validateMcWipQty  = "";
                    } 
                }
	  } 
	  else if(this.typeClose.equals("downtimeProcessPopupClose"))
	  {
		  downtimeProcessPopup      = false;
                  reasonSelectOne = "0";
                  qtyWip          = "0";
                  remark          = null;
	  }
	  else if(this.typeClose.equals("downtimeProcessPopupConfirmYes"))
	  {
                //downtimeProcessPopupConfirmYes  = true;
                downtimeProcessPopupConfirm     = false; 
               // insertDowntime();
          }    
	  else if(this.typeClose.equals("downtimeProcessPopupConfirmNo"))
	  {
		  //downtimeProcessPopupConfirmNo   = true;
		  downtimeProcessPopupConfirm     = false;
	  }
	  else if(this.typeClose.equals("downtimeProcessPopupConfirmYesClose") || this.typeClose.equals("downtimeProcessPopupConfirmNoClose") )
	  {
		  downtimeProcessPopupConfirmNo   = false;
		  downtimeProcessPopupConfirmYes  = false;
	  }  
         
	
	 }
	 public void openPopupType(String visibles)
	    {  
	    	this.visibles = visibles;
	    	if(this.visibles.equals("mcDowntime"))
	    	{
	    		if(!getShareData().mcDownTimeButton ){
                            downtimeProcessPopup = true;
                            findMcName();
                            
                            //================ Set memu  
                        }     
	    	}
	    	
	    }   
	 
	 public boolean isDowntimeProcessPopup() {
			return downtimeProcessPopup;
		}
		public boolean isDowntimeProcessPopupConfirm() {
			return downtimeProcessPopupConfirm;
		} 
		public boolean isDowntimeProcessPopupConfirmYes() {
			return downtimeProcessPopupConfirmYes;
		}
		public boolean isDowntimeProcessPopupConfirmNo() {
			return downtimeProcessPopupConfirmNo;
		}
		
                //============================================
		private List<SelectItem> processSteplist; 
                private String reasonSelectOne  =  "";
                public String getReasonSelectOne(){
                    return reasonSelectOne;
                }   
                public void setReasonSelectOne(String reasonSelectOne){
                    this.reasonSelectOne  = reasonSelectOne;
                }
                
                public void ChangeDowntimeValue(ValueChangeEvent event) {
                     System.out.println("================== Change Reason value =======================");
                        reasonSelectOne = (String) event.getNewValue(); 
                    if(reasonSelectOne.equals("0"))
                    {
                        validateReason  = "Select one reason";
                    }
                    if(!reasonSelectOne.equals("0"))
                    {
                        validateReason  = "";
                    }
                    // PrintOut(shifPart);
                }
                 
                
		public DowntimeProcess(){
			type = new ArrayList<SelectItem>();
			Series = new ArrayList<SelectItem>();
			Model = new ArrayList<SelectItem>();
			for(int i=0;i<5;i++){
				type.add(new SelectItem("A"+i));
				Series.add(new SelectItem("B"+i));
				Model.add(new SelectItem("C"+i));
			}
			
			processSteplist = new ArrayList<SelectItem>(); 
                        processSteplist.add(new SelectItem("0","---- Select one -----"));
			processSteplist.add(new SelectItem("Abnormal production"));
			processSteplist.add(new SelectItem("Change over"));
			processSteplist.add(new SelectItem("Electric shutdown"));
			processSteplist.add(new SelectItem("Equipment breakdowns"));
			processSteplist.add(new SelectItem("Machine idle"));
			processSteplist.add(new SelectItem("Others"));
			processSteplist.add(new SelectItem("Preventive maintenance"));
			processSteplist.add(new SelectItem("Process failure"));
			processSteplist.add(new SelectItem("Production adjustment"));
			processSteplist.add(new SelectItem("Quality defects"));
			processSteplist.add(new SelectItem("Set up"));
			processSteplist.add(new SelectItem("Unplan"));
			
		}
               //===================Find mc  name ====================
               private String nameMcDown = "";
               public String  getNameMcDown(){
                   return nameMcDown ;
               } 
               public void setNameMcDown(String nameMcDown ){
                   this.nameMcDown   = nameMcDown ;
               } 
               private CurrentProcess  currentProcess = null;
               private MainData mainDataCurrent       = null;
               public  void  findMcName(){
                 System.out.println("================================ Mc Downtime Name ==============================");
//                 System.out.println("================ Barcode :"+BarcodeAuto.textProcess);
                 
                 List<LotControl>   lotcontrolData  =  getlLotControlJpaController().findLotcontrolByBarcode(getShareData().textProcess);
                 String currentProcessMainDataId  =  lotcontrolData.get(0).getMainDataId(); 
                 System.out.println("=================================================================================");
                 System.out.println("============================  Maindata Current ==================================");
                 System.out.println("================ Current  Process MainDataId :"+currentProcessMainDataId);
                 
                 mainDataCurrent  =  getlMainDataJpaController().findMainData(currentProcessMainDataId);
                 String currentNumberInCurentProcess =  mainDataCurrent.getCurrentNumber();
                 currentProcess  = getCurrentProcessJpaController().findCurrentProcess(currentNumberInCurentProcess);
                 nameMcDown      = currentProcess.getMcId().getMcName();
                
                 System.out.println("================ Mc name  :"+nameMcDown);
                 System.out.println("=================================================================================");  
               } 
               
               //============== Insert Downtime
               public void insertDowntime()
               {  
                    CurrentProcess   oldProcessDowntime  = getCurrentProcessJpaController().findCuurentDowntime(mainDataCurrent); 
                    if(oldProcessDowntime != null)
                    {
                    oldProcessDowntime.setCurrentProc("0");
                    try
                    {
                        getCurrentProcessJpaController().edit(oldProcessDowntime);
                    }
                    catch(Exception e)
                    {
                        fc.print("====Error not to edit downtme ");
                    }
                    }
                    System.out.println("================== Insert Current Process Status Downtimd===============================");
                    String uuidDowntime = UUID.randomUUID().toString();
                    getCurrentProcessAdd().setId(uuidDowntime);
                    getCurrentProcessAdd().setMainDataId(mainDataCurrent); 
                    getCurrentProcessAdd().setMcId(currentProcess.getMcId());
                    getCurrentProcessAdd().setCurrentProc("1");
                    getCurrentProcessAdd().setStatus("Downtime");
                    timeCurrent  timeNew  =  new  timeCurrent();
                    getCurrentProcessAdd().setTimeCurrent(timeNew.getDate());
                    getCurrentProcessAdd().setShift(currentProcess.getShift());
                    getCurrentProcessAdd().setLine(currentProcess.getLine());
                    getCurrentProcessAdd().setHr(currentProcess.getHr());
                    createDowntimeCurrent();

                    System.out.println("================================= Update Mc  downtime   =======================================");
                    System.out.println("=========== Mc Id        :"+currentProcess.getMcId());
                     
                    currentProcess.getMcId().setMcStatus("Downtime");
                        try {
                            System.out.println("============ Change mc status to << Downtime  >> =========");  
                            getMcController().edit(currentProcess.getMcId()); 
                        } catch (Exception e) {
                            System.out.println("======== Eror  ====="+e); 
                        }  
                    
                   //================== Update  Main data  of Loncotrol
                    System.out.println("=====================================  Update Maindata Id in  LotControl ====================================");    
                    System.out.println("============== MainDataId     :"+mainDataCurrent.getId()); 
                    mainDataCurrent.getLotControlId().setMd2pcIdModel(mainDataCurrent.getId());
                    try {
                         getlLotControlJpaController().edit( mainDataCurrent.getLotControlId());
                         System.out.println("===== Change  MaindataId  ====="); 
                    } catch (Exception e) { 
                        System.out.println("======== Eror====="+e);
                    }  
               
                    
                    //===========Update maindata current number
                    System.out.println("===================================== Update current number  in Main data ===================================");
                    currentProcess.getMainDataId().setCurrentNumber(uuidDowntime); 
                    try {
                        System.out.println("===== Change  Current number =====");  
                        getlMainDataJpaController().edit(currentProcess.getMainDataId()); 
                    } catch (Exception e) {
                        System.out.println("======== Eror====="+e); 
                    }  
                    
                    //============ Insert pcsqty
                    String uuidPcsQty  = UUID.randomUUID().toString();
                    getCurrentPcsQty().setId(uuidPcsQty);
                    getCurrentPcsQty().setCurrentProcessId(getCurrentProcessAdd());
                    getCurrentPcsQty().setQtyType("Downtime");
                     
                    CurrentProcess oldCurrent  = getCurrentProcessJpaController().findCurrentProcess(getShareData().mainDataId.getCurrentNumber());
                    PcsQty  oldPcsQty  = getPcsQtyJpaController().findPcsQtyCurrentProcessId(oldCurrent); 
                    getCurrentPcsQty().setQty(oldPcsQty.getQty());
                    getCurrentPcsQty().setWipQty(qtyWip);
                    
                    createPcsQtyDowntime();
                    //============ Insert mc_down
                    String uuidMcName  =  UUID.randomUUID().toString();
                    getCurrentDown().setId(uuidMcName);
                    getCurrentDown().setCurrentProcessId(getCurrentProcessAdd());
                    getCurrentDown().setMcId(getCurrentProcessAdd().getMcId());
                    getCurrentDown().setRemark(remark); 
                    getCurrentDown().setReson(reasonSelectOne); 
                    getCurrentDown().setRefDownTimeStart("");
                    createMcDown();
                    
                    prepareCreate();
                    preparePcsQty();
               } 
            
                public CurrentProcess getCurrentProcessAdd() {
                    if(currentProcessAdd == null){
                        currentProcessAdd = new CurrentProcess();
                    }
                    return currentProcessAdd;
                } 
                
                 public void prepareCreate() {
                    currentProcessAdd = new CurrentProcess();
                    selectedItemIndex = -1;
                    //return "Create";
                }
                
                
                public void createDowntimeCurrent() {
                    try {
                       currentProcessJpaController.create(currentProcessAdd);
                        //JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("MainDataCreated"));
//                        return prepareCreateDowntimeCurent();
                       
//                        return prepareCreate();
                        } catch (Exception e) {
                           // JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
//                        
                            System.out.println("======== Error  :"+e);
//                          return null;
                        }
                }  
                
                //=============== Create  Downtime 
                private McDown  currentDown  =  null;
                public McDown getCurrentDown() {
                    if(currentDown == null){
                        currentDown = new McDown();
                    }
                    return currentDown;
                }
                public void createMcDown(){
                    try{
                      getMcDownJpaController().create(currentDown);
                      currentDown  = new McDown();
                    } 
                    catch (Exception e){
                      System.err.println("=========== Error    :"+e);
                    }
                }
                //============= Create pcs_qty  downtime
                private PcsQty  currentPcsQty =null;
                public PcsQty getCurrentPcsQty(){
                   if(currentPcsQty ==null){
                       currentPcsQty  = new PcsQty();
                   }
                   return currentPcsQty;
                }
                public void preparePcsQty(){
                    currentPcsQty  = new PcsQty();
                 }
                public void createPcsQtyDowntime(){
                    try{
                       getPcsQtyJpaController().create(currentPcsQty); 
                    }
                    catch(Exception e){
                        System.out.println("=========== Error    :"+e);
                    }
                }  
                //============= Get Reason
                private String remark  = "";
                public  String getRemark(){
                    return remark;
                }
                public void setRemark(String remark){
                    this.remark  = remark;
                    System.out.println("============ remark   :"+remark);
                }
		public List<SelectItem> getType() {
			return type;
		}
                //============== Get Qty
               
                public  String getQtyWip(){
                    return qtyWip;
                }
                public void setQtyWip(String qtyWip){
                    this.qtyWip  = qtyWip;
                
                }
                 
		/**
		 * @param type the type to set
		 */
		public void setType(List<SelectItem> type) {
			this.type = type;
		}

		/**
		 * @return the series
		 */
		public List<SelectItem> getSeries() {
			return Series;
		}

		/**
		 * @param series the series to set
		 */
		public void setSeries(List<SelectItem> series) {
			Series = series;
		}

		/**
		 * @return the model
		 */
		public List<SelectItem> getModel() {
			return Model;
		}

		/**
		 * @param model the model to set
		 */
		public void setModel(List<SelectItem> model) {
			Model = model;
		}

		/**
		 * @return the newtype
		 */
		public String getNewtype() {
			return newtype;
		}

		/**
		 * @param newtype the newtype to set
		 */
		public void setNewtype(String newtype) {
			this.newtype = newtype;
		}

		/**
		 * @return the newseries
		 */
		public String getNewseries() {
			return newseries;
		}

		/**
		 * @param newseries the newseries to set
		 */
		public void setNewseries(String newseries) {
			this.newseries = newseries;
		}

		/**
		 * @return the newmodel
		 */
		public String getNewmodel() {
			return newmodel;
		}

		/**
		 * @param newmodel the newmodel to set
		 */
		public void setNewmodel(String newmodel) {
			this.newmodel = newmodel;
		}

		/**
		 * @return the typeselected
		 */
		public String getTypeselected() {
			return typeselected;
		}

		/**
		 * @param typeselected the typeselected to set
		 */
		public void setTypeselected(String typeselected) {
			this.typeselected = typeselected;
		}

		/**
		 * @return the seriesselected
		 */
		public String getSeriesselected() {
			return seriesselected;
		}

		/**
		 * @param seriesselected the seriesselected to set
		 */
		public void setSeriesselected(String seriesselected) {
			this.seriesselected = seriesselected;
		}

		/**
		 * @return the moduleselected
		 */
		public String getModuleselected() {
			return moduleselected;
		}

		/**
		 * @param moduleselected the moduleselected to set
		 */
		public void setModuleselected(String moduleselected) {
			this.moduleselected = moduleselected;
		}

		/**
		 * @return the processSteplist
		 */
		public List<SelectItem> getProcessSteplist() {
			return processSteplist;
		}

		/**
		 * @param processSteplist the processSteplist to set
		 */
		public void setProcessSteplist(List<SelectItem> processSteplist) {
			this.processSteplist = processSteplist;
		}

    /**
     * @return the validateReason
     */
    public String getValidateReason() {
        return validateReason;
    }

    /**
     * @param validateReason the validateReason to set
     */
    public void setValidateReason(String validateReason) {
        this.validateReason = validateReason;
    }

    /**
     * @return the validateMcWipQty
     */
    public String getValidateMcWipQty() {
        return validateMcWipQty;
    }

    /**
     * @param validateMcWipQty the validateMcWipQty to set
     */
    public void setValidateMcWipQty(String validateMcWipQty) {
        this.validateMcWipQty = validateMcWipQty;
    }

		/**
		 * @return the processlistpool
		 */
		/*public List<SelectItem> getProcesslistpool() {
			return processlistpool;
		}

		*//**
		 * @param processlistpool the processlistpool to set
		 *//*
		public void setProcesslistpool(List<SelectItem> processlistpool) {
			this.processlistpool = processlistpool;
		}		*/
		
	 
}
