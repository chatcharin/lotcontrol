/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shareData;
import com.tct.data.*;
/**
 *
 * @author pang
 */
public class ShareDataOfSession { 
    public  boolean openProcessButton = true ;
    public  String openProcessButtonShow = "open_process_button_disble";
    public String status = null ;
    public ProcessPool idProc = null;
    public LotControl  lotControlId;
    public String  lotNumber = null;  
    public String  md2pcId = null;
    public String  nameProcess = null; 
    public ProcessPool procPoolId = null;
    public String sqProcess = null;
    public boolean closeProcessButton = true;
    public String closeProcessButtonShow = null;
    public boolean mcDownTimeButton = true;
    public String mcDownTimeButtonShow = null;
    public boolean holdTimeButton = true;
    public String  holdTimeButtonShow = null;
    public MainData mainDataId = null;
    public int  outputQty = 0;
    public boolean stopTimeButton = true;
    public boolean moveProcessButton = true;
    public String stopTimeButtonShow = null;
    public String moveProcessButtonShow = null;
    public String textProcess   = null; 
    
//    private String  test  = "open_process_button_disable";  
//    public String getTest() {
//        return test;
//    } 
//    public void setTest(String test) {
//        this.test = test;
//    }

    /**
     * @return the openProcessButton
     */
//    public boolean isOpenProcessButton() {
//        return openProcessButton;
//    }
//
//    /**
//     * @param openProcessButton the openProcessButton to set
//     */
//    public void setOpenProcessButton(boolean openProcessButton) {
//        this.openProcessButton = openProcessButton;
//    }
//
//    /**
//     * @return the openProcessButtonShow
//     */
//    public String getOpenProcessButtonShow() {
//        return openProcessButtonShow;
//    }
//
//    /**
//     * @param openProcessButtonShow the openProcessButtonShow to set
//     */
//    public void setOpenProcessButtonShow(String openProcessButtonShow) {
//        this.openProcessButtonShow = openProcessButtonShow;
//    }
//
//    /**
//     * @return the status
//     */
//    public String getStatus() {
//        return status;
//    }
//
//    /**
//     * @param status the status to set
//     */
//    public void setStatus(String status) {
//        this.status = status;
//    }
//
//    /**
//     * @return the idProc
//     */
//    public ProcessPool getIdProc() {
//        return idProc;
//    }
//
//    /**
//     * @param idProc the idProc to set
//     */
//    public void setIdProc(ProcessPool idProc) {
//        this.idProc = idProc;
//    }
//
//    /**
//     * @return the lotControlId
//     */
//    public LotControl getLotControlId() {
//        return lotControlId;
//    }
//
//    /**
//     * @param lotControlId the lotControlId to set
//     */
//    public void setLotControlId(LotControl lotControlId) {
//        this.lotControlId = lotControlId;
//    }
//
//    /**
//     * @return the lotNumber
//     */
//    public String getLotNumber() {
//        return lotNumber;
//    }
//
//    /**
//     * @param lotNumber the lotNumber to set
//     */
//    public void setLotNumber(String lotNumber) {
//        this.lotNumber = lotNumber;
//    }
//
//    /**
//     * @return the md2pcId
//     */
//    public String getMd2pcId() {
//        return md2pcId;
//    }
//
//    /**
//     * @param md2pcId the md2pcId to set
//     */
//    public void setMd2pcId(String md2pcId) {
//        this.md2pcId = md2pcId;
//    }
//
//    /**
//     * @return the nameProcess
//     */
//    public String getNameProcess() {
//        return nameProcess;
//    }
//
//    /**
//     * @param nameProcess the nameProcess to set
//     */
//    public void setNameProcess(String nameProcess) {
//        this.nameProcess = nameProcess;
//    }
//
//    /**
//     * @return the procPoolId
//     */
//    public ProcessPool getProcPoolId() {
//        return procPoolId;
//    }
//
//    /**
//     * @param procPoolId the procPoolId to set
//     */
//    public void setProcPoolId(ProcessPool procPoolId) {
//        this.procPoolId = procPoolId;
//    }
//
//    /**
//     * @return the sqProcess
//     */
//    public String getSqProcess() {
//        return sqProcess;
//    }
//
//    /**
//     * @param sqProcess the sqProcess to set
//     */
//    public void setSqProcess(String sqProcess) {
//        this.sqProcess = sqProcess;
//    }
//
//    /**
//     * @return the closeProcessButton
//     */
//    public boolean isCloseProcessButton() {
//        return closeProcessButton;
//    }
//
//    /**
//     * @param closeProcessButton the closeProcessButton to set
//     */
//    public void setCloseProcessButton(boolean closeProcessButton) {
//        this.closeProcessButton = closeProcessButton;
//    }
//
//    /**
//     * @return the mcDownTimeButtonShow
//     */
//    public String getMcDownTimeButtonShow() {
//        return mcDownTimeButtonShow;
//    }
//
//    /**
//     * @param mcDownTimeButtonShow the mcDownTimeButtonShow to set
//     */
//    public void setMcDownTimeButtonShow(String mcDownTimeButtonShow) {
//        this.mcDownTimeButtonShow = mcDownTimeButtonShow;
//    }
//
//    /**
//     * @return the closeProcessButtonShow
//     */
//    public String getCloseProcessButtonShow() {
//        return closeProcessButtonShow;
//    }
//
//    /**
//     * @param closeProcessButtonShow the closeProcessButtonShow to set
//     */
//    public void setCloseProcessButtonShow(String closeProcessButtonShow) {
//        this.closeProcessButtonShow = closeProcessButtonShow;
//    }
//
//    /**
//     * @return the mcDownTimeButton
//     */
//    public boolean isMcDownTimeButton() {
//        return mcDownTimeButton;
//    }
//
//    /**
//     * @param mcDownTimeButton the mcDownTimeButton to set
//     */
//    public void setMcDownTimeButton(boolean mcDownTimeButton) {
//        this.mcDownTimeButton = mcDownTimeButton;
//    }
//
//    /**
//     * @return the holdTimeButtonShow
//     */
//    public String getHoldTimeButtonShow() {
//        return holdTimeButtonShow;
//    }
//
//    /**
//     * @param holdTimeButtonShow the holdTimeButtonShow to set
//     */
//    public void setHoldTimeButtonShow(String holdTimeButtonShow) {
//        this.holdTimeButtonShow = holdTimeButtonShow;
//    }
//
//    /**
//     * @return the mainDataId
//     */
//    public MainData getMainDataId() {
//        return mainDataId;
//    }
//
//    /**
//     * @param mainDataId the mainDataId to set
//     */
//    public void setMainDataId(MainData mainDataId) {
//        this.mainDataId = mainDataId;
//    }
//
//    /**
//     * @return the outputQty
//     */
//    public int getOutputQty() {
//        return outputQty;
//    }
//
//    /**
//     * @param outputQty the outputQty to set
//     */
//    public void setOutputQty(int outputQty) {
//        this.outputQty = outputQty;
//    }
//
//    /**
//     * @return the stopTimeButtonShow
//     */
//    public String getStopTimeButtonShow() {
//        return stopTimeButtonShow;
//    }
//
//    /**
//     * @param stopTimeButtonShow the stopTimeButtonShow to set
//     */
//    public void setStopTimeButtonShow(String stopTimeButtonShow) {
//        this.stopTimeButtonShow = stopTimeButtonShow;
//    }
//
//    /**
//     * @return the moveProcessButtonShow
//     */
//    public String getMoveProcessButtonShow() {
//        return moveProcessButtonShow;
//    }
//
//    /**
//     * @param moveProcessButtonShow the moveProcessButtonShow to set
//     */
//    public void setMoveProcessButtonShow(String moveProcessButtonShow) {
//        this.moveProcessButtonShow = moveProcessButtonShow;
//    }
//
//    /**
//     * @return the textProcess
//     */
//    public String getTextProcess() {
//        return textProcess;
//    }
//
//    /**
//     * @param textProcess the textProcess to set
//     */
//    public void setTextProcess(String textProcess) {
//        this.textProcess = textProcess;
//    }
//
//    /**
//     * @return the holdTimeButton
//     */
//    public boolean isHoldTimeButton() {
//        return holdTimeButton;
//    }
//
//    /**
//     * @param holdTimeButton the holdTimeButton to set
//     */
//    public void setHoldTimeButton(boolean holdTimeButton) {
//        this.holdTimeButton = holdTimeButton;
//    }
//
//    /**
//     * @return the stopTimeButton
//     */
//    public boolean isStopTimeButton() {
//        return stopTimeButton;
//    }
//
//    /**
//     * @param stopTimeButton the stopTimeButton to set
//     */
//    public void setStopTimeButton(boolean stopTimeButton) {
//        this.stopTimeButton = stopTimeButton;
//    }
//
//    /**
//     * @return the moveProcessButton
//     */
//    public boolean isMoveProcessButton() {
//        return moveProcessButton;
//    }
//
//    /**
//     * @param moveProcessButton the moveProcessButton to set
//     */
//    public void setMoveProcessButton(boolean moveProcessButton) {
//        this.moveProcessButton = moveProcessButton;
//    }
//    
}
