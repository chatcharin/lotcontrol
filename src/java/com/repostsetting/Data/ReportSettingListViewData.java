/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.repostsetting.Data;

/**
 *
 * Machine User
 *
 * @author pang Development by Chusak laojang Date : Oct 19, 2012, Time :
 * 2:59:00 PM Copy Right 4 Xtreme Co.,Ltd.
 */
public class ReportSettingListViewData {

    private String index;
    private String name;
    private String description;
    private int number;

    public ReportSettingListViewData(String index, String name, String description, int number) {
        this.index = index;
        this.name = name;
        this.description = description;
        this.number = number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
