/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.repostsetting.Data;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Oct 18, 2012, Time : 6:37:27 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class reportSettingName {
    private String index;
    private String name;
    private String description;

    public reportSettingName(String index, String name, String description) {
        this.index = index;
        this.name = name;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    } 
}
