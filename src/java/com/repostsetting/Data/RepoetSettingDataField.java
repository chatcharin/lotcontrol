/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.repostsetting.Data;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Oct 18, 2012, Time : 6:30:11 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class RepoetSettingDataField {
    private String indexReport;
    private String name;
    private String value;
    private String indexFiled;

    public RepoetSettingDataField(String indexReport, String name, String value, String indexFiled) {
        this.indexReport = indexReport;
        this.name = name;
        this.value = value;
        this.indexFiled = indexFiled;
    }

    public String getIndexFiled() {
        return indexFiled;
    }

    public void setIndexFiled(String indexFiled) {
        this.indexFiled = indexFiled;
    }

    public String getIndexReport() {
        return indexReport;
    }

    public void setIndexReport(String indexReport) {
        this.indexReport = indexReport;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    } 
}
