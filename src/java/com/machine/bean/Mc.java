/**
 * 
 */
package com.machine.bean;

import java.util.ArrayList;
import java.util.List;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

/**
 * @author The_Boy_Cs
 *
 */
public class Mc {
	
	private String mcnumber;
	private String mccode;
	private String detail1;
	private String detail2;
	
	private String mclistselected;
	private List<SelectItem> mclist;
	private String mcdetaillistselected;
	private List<SelectItem> mcdetaillist;
	
	
	
	public Mc(){
		mclist = new ArrayList<SelectItem>();
		mcdetaillist = new ArrayList<SelectItem>();
		for(int i =0;i<30;i++){
			mclist.add(new SelectItem("A"+i));
			mcdetaillist.add(new SelectItem("B"+i));
		}
	}
	
	public void viewMcList(ValueChangeEvent event){
		System.out.println(event.getNewValue());
		mcdetaillist = new ArrayList<SelectItem>();
		for(int i =0;i<30;i++){
			mcdetaillist.add(new SelectItem("B"+i));
		}
	}
	
	public void clearMcdetail(ActionEvent event){
		System.out.println("ClearMC"+ event.toString());
		mcdetaillist = null;
	}

	public void AddMc(ActionEvent e){
		System.out.println("Add Mc.");
	}
	
	/**
	 * @return the mcnumber
	 */
	public String getMcnumber() {
		return mcnumber;
	}

	/**
	 * @param mcnumber the mcnumber to set
	 */
	public void setMcnumber(String mcnumber) {
		this.mcnumber = mcnumber;
	}

	/**
	 * @return the mccode
	 */
	public String getMccode() {
		return mccode;
	}

	/**
	 * @param mccode the mccode to set
	 */
	public void setMccode(String mccode) {
		this.mccode = mccode;
	}

	/**
	 * @return the detail1
	 */
	public String getDetail1() {
		return detail1;
	}

	/**
	 * @param detail1 the detail1 to set
	 */
	public void setDetail1(String detail1) {
		this.detail1 = detail1;
	}

	/**
	 * @return the detail2
	 */
	public String getDetail2() {
		return detail2;
	}

	/**
	 * @param detail2 the detail2 to set
	 */
	public void setDetail2(String detail2) {
		this.detail2 = detail2;
	}

	/**
	 * @return the mclistselected
	 */
	public String getMclistselected() {
		return mclistselected;
	}

	/**
	 * @return the mclist
	 */
	public List<SelectItem> getMclist() {
		return mclist;
	}

	/**
	 * @param mclist the mclist to set
	 */
	public void setMclist(List<SelectItem> mclist) {
		this.mclist = mclist;
	}

	/**
	 * @param mclistselected the mclistselected to set
	 */
	public void setMclistselected(String mclistselected) {
		this.mclistselected = mclistselected;
	}

	/**
	 * @return the mcdetaillistselected
	 */
	public String getMcdetaillistselected() {
		return mcdetaillistselected;
	}

	/**
	 * @return the mcdetaillist
	 */
	public List<SelectItem> getMcdetaillist() {
		return mcdetaillist;
	}

	/**
	 * @param mcdetaillist the mcdetaillist to set
	 */
	public void setMcdetaillist(List<SelectItem> mcdetaillist) {
		this.mcdetaillist = mcdetaillist;
	}

	/**
	 * @param mcdetaillistselected the mcdetaillistselected to set
	 */
	public void setMcdetaillistselected(String mcdetaillistselected) {
		this.mcdetaillistselected = mcdetaillistselected;
	}
	
}
