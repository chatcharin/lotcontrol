/**
 * 
 */
package com.machine.bean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import javax.faces.model.SelectItem;

import com.component.table.sortable.SortableList;
import com.machine.CMPerformanceData.mcPerforData;

/**
 * @author The_Boy_Cs
 * 
 */
public class mcPerformance extends SortableList{

	private static final String downtime_dateColum = "Downtime date";
	private static final String startTimeColum = "Start Time";
	private static final String stopTimeColum = "Stop Time";
	private static final String reasonColum = "Reason";
	private static final String processColum = "Process";
	private static final String remaskColum ="Remask";
	
	private String mc;
	private List<SelectItem> selectMc;
	private String process;
	private List<SelectItem> selectProcess;
	private String fromdate;
	private String todate;

	public mcPerformance() {
		super(downtime_dateColum);
		selectMc = new ArrayList<SelectItem>();
		selectProcess = new ArrayList<SelectItem>();
		
		selectMc.add(new SelectItem("----- Select One -----"));
		selectProcess.add(new SelectItem("----- Select One -----"));
		for(int i = 0;i<5;i++){
			selectMc.add(new SelectItem(" A "+i));
			//selectProcess.add(new SelectItem(" B "+i));
		}
		
		for(int j = 0;j<mcperfordata.length;j++){
			selectProcess.add(new SelectItem(" B "+mcperfordata[j].getProcess()));
		}
	}

	/**
	 * @return the downtimeDatecolum
	 */
	public String getDowntimeDatecolum() {
		return downtime_dateColum;
	}



	/**
	 * @return the starttimecolum
	 */
	public String getStarttimecolum() {
		return startTimeColum;
	}



	/**
	 * @return the stoptimecolum
	 */
	public String getStoptimecolum() {
		return stopTimeColum;
	}



	/**
	 * @return the reasoncolum
	 */
	public String getReasoncolum() {
		return reasonColum;
	}



	/**
	 * @return the processcolum
	 */
	public String getProcesscolum() {
		return processColum;
	}



	/**
	 * @return the remaskcolum
	 */
	public String getRemaskcolum() {
		return remaskColum;
	}



	/**
	 * @param selectMc
	 *            the selectMc to set
	 */
	public void setSelectMc(List<SelectItem> selectMc) {
		this.selectMc = selectMc;
	}

	/**
	 * @return the selectMc
	 */
	public List<SelectItem> getSelectMc() {
		return selectMc;
	}

	/**
	 * @return the mc
	 */
	public String getMc() {
		return mc;
	}

	/**
	 * @param mc
	 *            the mc to set
	 */
	public void setMc(String mc) {
		this.mc = mc;
	}

	/**
	 * @return the process
	 */
	public String getProcess() {
		return process;
	}

	/**
	 * @param process the process to set
	 */
	public void setProcess(String process) {
		this.process = process;
	}

	/**
	 * @return the selectProcess
	 */
	public List<SelectItem> getSelectProcess() {
		return selectProcess;
	}

	/**
	 * @param selectProcess the selectProcess to set
	 */
	public void setSelectProcess(List<SelectItem> selectProcess) {
		this.selectProcess = selectProcess;
	}
	
	/**
	 * @param mcperfordata the mcperfordata to set
	 */
	public void setMcperfordata(mcPerforData[] mcperfordata) {
		this.mcperfordata = mcperfordata;
	}

	/**
	 * @return the fromdate
	 */
	public String getFromdate() {
		return fromdate;
	}

	/**
	 * @param fromdate the fromdate to set
	 */
	public void setFromdate(String fromdate) {
		this.fromdate = fromdate;
	}

	/**
	 * @return the todate
	 */
	public String getTodate() {
		return todate;
	}

	/**
	 * @param todate the todate to set
	 */
	public void setTodate(String todate) {
		this.todate = todate;
	}	
	
	@SuppressWarnings("unchecked")
	@Override
	protected void sort() {
		// TODO Auto-generated method stub
		Comparator comparator = new Comparator() {
	        public int compare(Object o1, Object o2) {
	        	mcPerforData c1 = (mcPerforData) o1;
	        	mcPerforData c2 = (mcPerforData) o2;
	            if (sortColumnName == null) {
	                return 0;
	            }
	            if (sortColumnName.equals(downtime_dateColum)) {
	                return ascending ?
	                Integer.valueOf(c1.getDowntimedate()).compareTo(Integer.valueOf(c2.getDowntimedate())) :
	                Integer.valueOf(c2.getDowntimedate()).compareTo(Integer.valueOf(c1.getDowntimedate()));
	            } else if (sortColumnName.equals(startTimeColum)) {
	                return ascending ? c1.getStarttime().compareTo(c2.getStarttime()) :
	                c2.getStarttime().compareTo(c1.getStarttime());
	            } else if (sortColumnName.equals(stopTimeColum)) {
	                return ascending ? c1.getStoptime().compareTo(c2.getStoptime()) :
	                c2.getStoptime().compareTo(c1.getStoptime());
	            } else if (sortColumnName.equals(reasonColum)) {
	                return ascending ?
	                c1.getReson().compareTo(c2.getReson()) :
	                c2.getReson().compareTo(c1.getReson());
	            } else if (sortColumnName.equals(processColum)) {
	                return ascending ?
	                c1.getProcess().compareTo(c2.getProcess()) :
	                c2.getProcess().compareTo(c1.getProcess());
	            }  else if (sortColumnName.equals(remaskColum)) {
	                return ascending ?
	                c1.getRemask().compareTo(c2.getRemask()) :
	                c2.getRemask().compareTo(c1.getRemask());
	            }  else return 0;
	        }
	    };
	    Arrays.sort(mcperfordata, comparator);
	}

	@Override
	protected boolean isDefaultAscending(String sortColumn) {
		// TODO Auto-generated method stub
		return true;
	}
	
////----------------------------------------------------------------------------------------------------------------------
	private mcPerforData[] mcperfordata = new mcPerforData[]{
            new mcPerforData("1", "a", "b",  "c", "d","e"),
            new mcPerforData("2", "b", "c", "d" ,"e","f"),
            new mcPerforData("3", "c", "d", "e", "f","g"),
            new mcPerforData("4", "d", "e", "f" , "g","h"),
            new mcPerforData("5", "e", "f", "g", "h","i")
    };

	
	
    /**
	 * @return the mcperfordata
	 */
	public mcPerforData[] getMcperfordata() {
		if (!oldSort.equals(sortColumnName) ||
    	        oldAscending != ascending){
    	        sort();
    	        oldSort = sortColumnName;
    	        oldAscending = ascending;
    	    }
		return mcperfordata;
	}

}
