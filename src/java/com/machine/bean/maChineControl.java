/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machine.bean;

import com.appCinfigpage.bean.fc;
import com.appCinfigpage.bean.userLogin;
import com.lowagie.text.pdf.ArabicLigaturizer;
import com.tct.data.Mc;
import com.tct.data.jpa.McJpaController;
import com.tct.data.jsf.util.PaginationHelper;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence; 
import org.icefaces.ace.component.fileentry.FileEntry;
import org.icefaces.ace.component.fileentry.FileEntryEvent;
import org.icefaces.ace.component.fileentry.FileEntryResults;
//import com.machine.bean.McDetail;
/**
 *
 * @author The_Boy_Cs
 */
public class maChineControl {
//    == Search =======================
    private String statusSearch;
    private String mcnameSearch;
//    =================================
    private Mc currentMc ;
    private List<McDetail> itemsMachine;
    private List<SelectItem>  itemStatus;  
    public maChineControl() {
        searchByData();
        loadItemStatus();
    }
 
    public List<McDetail> getItemsMachine() {
        if(itemsMachine == null)
        {
            itemsMachine  = new ArrayList<McDetail>();
        }
        return itemsMachine;
    }

    public void setItemsMachine(List<McDetail> itemsMachine) {
       
        this.itemsMachine = itemsMachine;
    }
    
    public void changToEdit(ActionEvent event){
        print("================= chang to edit =================================");
        try{
            currentMc  = null;
            objMcList  = null;
            McDetail  mcdetail  = (McDetail) getWorksheet().getRowData();
            currentMc  = getMcJpaController().findMc(mcdetail.getIndexMc());
            String link = "pages/machine/editview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        }catch(Exception e){
            print(" chang to edit Error : "+e);
        }
    }

    public void changToSave(ActionEvent event) {
        print("================= Save Edit =====================================");
        try {
            getCurrentMc().setMcName(currentMc.getMcName());
            getCurrentMc().setDescription(currentMc.getDescription());
            getCurrentMc().setMcStatus(currentMc.getMcStatus());
            //@pang
            getMcJpaController().edit(currentMc);
            //Thread.sleep(500);
            itemsMachine  = new ArrayList<McDetail>();
            currentMc  = null;
            objMcList  = null;
            worksheet  = null; 
            loadDataTableMcList();
            String link = "pages/machine/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (Exception e) {
            print("= Save edit Error : " + e);
        }
    }

    public void create(ActionEvent event) {
        print("================= Create ========================================");
        fc.print("mc name : "+currentMc.getMcName()); 
        getCurrentMc().setId(idMc);
        try {
            getMcJpaController().create(currentMc); 
        } catch (Exception e) {
            print("= create Error : " + e);
        }
            String link = "pages/machine/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            // Thread.sleep(500);
            fc.print("add data sucess");
//            currentMc  = null;
            objMcList  = null;
            worksheet  = null;
            itemsMachine  = new ArrayList<McDetail>();
            loadDataTableMcList();
    }

    public void changTocancel(ActionEvent event) {
        print("================== Cancel =======================================");
        try {
            currentMc = null;
            String link = "pages/machine/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            
        } catch (Exception e) {
            print("= Cancel Error : " + e);
        }
    }
    private  String idMc = null;

    public String getIdMc() {
        return idMc;
    }

    public void setIdMc(String idMc) {
        this.idMc = idMc;
    }
    
    
    public void changToInsert(ActionEvent event) {
        print("================== Inser view ===================================");
        try {
            currentMc  = null;
            idMc  = null;
            objMcList  = null;
            idMc  = UUID.randomUUID().randomUUID().toString();
            String link = "pages/machine/createview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            print("= Link : " + link);
        } catch (Exception e) {
            print("= inser view Error : " + e);
        }
    }
    public void changeToDelelte(String index)
    {
        currentMc  = null;
        objMcList  = null;  
        itemsMachine  = new ArrayList<McDetail>(); 
        currentMc   = getMcJpaController().findMc(index);
        fc.print("del");
        getCurrentMc().setDeleted(1);
        try
        {
            getMcJpaController().edit(getCurrentMc());
            fc.print("Delete success");
        }
        catch(Exception e)
        {
           fc.print("Error noe update"+e);
        } 
        
        String link = "pages/machine/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
        loadDataTableMcList();
    }
    //@pang
    private String closepopup = "window.close();";
    private String showpopup;
    private String mcPic ;

    public String getMcPic() {
        try {
            String fileparth = "./images/mcphoto"; 
            if (getCurrentMc().getPathPriture() == null && getCurrentMc().getPathPriture().isEmpty()) {
                getCurrentMc().setPathPriture("no_image.jpg");
            }
            return fileparth += "/" + getCurrentMc().getPathPriture();
        } catch (Exception e) {
            print("error1 : " + e);
            return "../images/no_image.jpg";
        } 
    }

    public void setMcPic(String mcPic) {
        this.mcPic = mcPic;
    } 
    public String getClosepopup() {
        return closepopup;
    }

    public void setClosepopup(String closepopup) {
        this.closepopup = closepopup;
    }

    public String getShowpopup() {
        return showpopup;
    }

    public void setShowpopup(String showpopup) {
        this.showpopup = showpopup;
    }
   public void listener(FileEntryEvent event) {
        print("====================== listener ===============");
        try {
            FileEntry fileEntry = (FileEntry) event.getSource();
            FileEntryResults results = fileEntry.getResults();
            String partfile = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/images/mcphoto/");
            for (FileEntryResults.FileInfo fileInfo : results.getFiles()) {
                if (fileInfo.isSaved()) {
                    print("== filename 1 : " + fileInfo.getFileName());
                    File file = fileInfo.getFile();
                    print("=== file name 2 : " + file.getName());
//                    String filename = file.getName();
                    String[] arrname = file.getName().split("\\.");
                    print("= Lenght : " + arrname.length);
                    print(" == filetype : " + arrname[1]);
                    String filename;
                    if(getCurrentMc().getId() != null){
                        filename = getCurrentMc().getId() + "." + arrname[arrname.length - 1];
                    }
                    else
                    {
                        filename = idMc + "." + arrname[arrname.length - 1];
                    }
                    print("= file name 3 : " + filename);
                    partfile += "/" + filename;
                    if (writFile(file, partfile)) {
                        getCurrentMc().setPathPriture(filename);
                        showpopup = "";
                        closepopup = "window.close();";
                    }
                }
            }
        } catch (Exception e) {
            print("Error0 : " + e);
        }
    }
      private boolean writFile(File file, String filePath) throws FileNotFoundException, IOException {
        print("========== Create file ==================================");
        print("= file parth : " + filePath);
        try {
            File outputFile = new File(filePath);
            FileInputStream in = new FileInputStream(file);
            FileOutputStream out = new FileOutputStream(outputFile);
            int c;
            while ((c = in.read()) != -1) {
                out.write(c);
            }
            file.delete();
            in.close();
            out.close();
            return true;
        } catch (IOException ioe) {
            print("= IO Error3 : " + ioe);
            return false;
        } catch (Exception e) {
            print("= Error2 : " + e);
            return false;
        }
    }
    public void uploadImage()
    {
        fc.print("================= upload image =======================");
        closepopup = "";
        showpopup = "window.showModalDialog('pages/machine/popuploadpic.xhtml', 'Upload pic', 'dialig');";
        
    }
    private void searchByMcName() 
    { 
         worksheetpagination = null;
         currentMc  = null;
         objMcList  = null; 
         itemsMachine  = new ArrayList<McDetail>();
         objMcList  = (List<Mc>) getMcJpaController().findByMcname(mcnameSearch);
           for(Mc mcList : objMcList)
           { 
               print(mcList.getMcName());
               getItemsMachine().add(new McDetail(mcList.getId(), mcList.getMcName(), mcList.getDescription(), mcList.getMcStatus()));
           }     
       setWorksheet(); 
    }
    private void searchByMcStatus() { 
        currentMc  = null;
        objMcList  = null; 
        itemsMachine  = new ArrayList<McDetail>();
        objMcList  = (List<Mc>) getMcJpaController().findListMCGood(statusSearch);
           for(Mc mcList : objMcList)
           { 
               print(mcList.getMcName());
               getItemsMachine().add(new McDetail(mcList.getId(), mcList.getMcName(), mcList.getDescription(), mcList.getMcStatus()));
           }     
       setWorksheet(); 
    }
    
    private McJpaController mcJpaController = null;

    private McJpaController getMcJpaController() {
        if (mcJpaController == null) {
            mcJpaController = new McJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mcJpaController;
    }

    public Mc getCurrentMc() {
        if (currentMc == null) {
            currentMc = new Mc();
        }
        return currentMc;
    }

    public void setCurrentMc(Mc currentMc) {
        this.currentMc = currentMc; 
        if(this.currentMc == null)
        {
            currentMc = new Mc();
        }
    }

    private void print(String str) {
        System.out.println(str);
    }

    public String getMcnameSearch() {
        return mcnameSearch;
    }

    public void setMcnameSearch(String mcnameSearch) {
        this.mcnameSearch = mcnameSearch;
    }

    public String getStatusSearch() {
        return statusSearch;
    }

    public void setStatusSearch(String statusSearch) {
        this.statusSearch = statusSearch;
    }
    public void searchByData()
    {
       
        if(mcnameSearch != null && !mcnameSearch.trim().isEmpty())
        {
            searchByMcName();
            fc.print("searchMcName");
        }
        else if(statusSearch !=null && !statusSearch.trim().isEmpty())
        {
            searchByMcStatus();
            fc.print("searchStatus");
        }
        else
        {    
            loadDataTableMcList();
            fc.print("searchAll");
        }
    }
    public void loadDataTableMcList() {  
        itemsMachine  = new ArrayList<McDetail>();
        print("======== test ========="); 
           objMcList  = getMcJpaController().findAllMcNotDelete();
           for(Mc mcList : objMcList)
           { 
               print(mcList.getMcName());
               getItemsMachine().add(new McDetail(mcList.getId(), mcList.getMcName(), mcList.getDescription(), mcList.getMcStatus()));
           }     
       setWorksheet(); 
    }
    private List<Mc> objMcList ;

    public List<Mc> getObjMcList() {
        return objMcList;
    }

    public void setObjMcList(List<Mc> objMcList) {
        if(objMcList  == null)
        {
            objMcList  = new ArrayList<Mc>();
        }
        this.objMcList = objMcList;
    }
    
    private DataModel worksheet;
    public DataModel getWorksheet() {
        return worksheet;
    }

    public void setWorksheet() {
        this.worksheet = getWorksheetpagination().createPageDataModel();
    }
    public void previous() {
        getWorksheetpagination().previousPage();
        setWorksheet();
    }

    public void next() {
        getWorksheetpagination().nextPage();
        setWorksheet();
    }

    public int getAllPages() {
        int all = getWorksheetpagination().getItemsCount();
        int pagesize = getWorksheetpagination().getPageSize();
        return ((int) all / pagesize) + 1;
    }
    private PaginationHelper worksheetpagination;
    public PaginationHelper getWorksheetpagination() {
        if (worksheetpagination == null) {
            worksheetpagination = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    return getItemsMachine().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    print("PageFirst : " + getPageFirstItem());
                    print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (getItemsMachine().size() > 0) {
                        listmodel = new ListDataModel(getItemsMachine().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(getItemsMachine());
                    }
                    return listmodel;
                }
            };
        }
        return worksheetpagination;
    }

    private void loadItemStatus() {
        if(itemStatus  == null)
        {
            itemStatus  = new ArrayList<SelectItem>();
        }
        itemStatus.add(new SelectItem("","----Select one ----"));
        itemStatus.add(new SelectItem("downtime","Downtime"));
        itemStatus.add(new SelectItem("good","Good"));
        itemStatus.add(new SelectItem("use","Use")); 
    } 
    public  void ChangeItemStatus(ValueChangeEvent event)
    {
        mcnameSearch = (String) event.getNewValue();   
    }
    public List<SelectItem> getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(List<SelectItem> itemStatus) {
        this.itemStatus = itemStatus;
    }

}
