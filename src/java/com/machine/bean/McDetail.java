/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machine.bean;
/**
 *
 * @author pang
 */
public class McDetail {
    private String indexMc;
    private String mcName ;
    private String mcCode;
    private String mcStatus;

    public McDetail(String indexMc, String mcName, String mcCode, String mcStatus) {
        this.indexMc = indexMc;
        this.mcName = mcName;
        this.mcCode = mcCode;
        this.mcStatus = mcStatus;
    }

    public void setIndexMc(String indexMc) {
        this.indexMc = indexMc;
    }

    public void setMcCode(String mcCode) {
        this.mcCode = mcCode;
    }

    public void setMcName(String mcName) {
        this.mcName = mcName;
    }

    public void setMcStatus(String mcStatus) {
        this.mcStatus = mcStatus;
    }

    public String getIndexMc() {
        return indexMc;
    }

    public String getMcCode() {
        return mcCode;
    }

    public String getMcName() {
        return mcName;
    }

    public String getMcStatus() {
        return mcStatus;
    } 
}
