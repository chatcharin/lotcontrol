package com.machine.CMPerformanceData;

public class mcPerforData {
	
	private String downtimedate;
	private String starttime;
	private String stoptime;
	private String reson;
	private String process;
	private String remask;
	
	public mcPerforData(){
	}
	
	public mcPerforData(String downtimedate, String starttime, String stoptime, String reson, String process, String remask){
		this.downtimedate = downtimedate;
		this.starttime = starttime;
		this.stoptime = stoptime;
		this.reson = reson;
		this.process = process;
		this.remask = remask;
	}
	/**
	 * @return the downtimedate
	 */
	public String getDowntimedate() {
		return downtimedate;
	}

	/**
	 * @return the starttime
	 */
	public String getStarttime() {
		return starttime;
	}

	/**
	 * @return the stoptime
	 */
	public String getStoptime() {
		return stoptime;
	}

	/**
	 * @return the reson
	 */
	public String getReson() {
		return reson;
	}

	/**
	 * @return the process
	 */
	public String getProcess() {
		return process;
	}

	/**
	 * @return the remask
	 */
	public String getRemask() {
		return remask;
	}
	
}
