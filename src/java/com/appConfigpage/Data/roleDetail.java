/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.appConfigpage.Data;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 7, 2012, Time : 3:24:10 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class roleDetail {
    private String index;
    private String number;
    private String roleName;
    private String descrition;

    public roleDetail(String index, String number, String roleName, String descrition) {
        this.index = index;
        this.number = number;
        this.roleName = roleName;
        this.descrition = descrition;
    }

    public String getDescrition() {
        return descrition;
    }

    public void setDescrition(String descrition) {
        this.descrition = descrition;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

}
