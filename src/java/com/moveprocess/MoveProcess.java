package com.moveprocess;
import com.component.barcodeauto.BarcodeAuto;
import com.component.barcodeauto.ProcList;
import com.shareData.ShareDataOfSession;
import com.tct.data.*;
import com.tct.data.jpa.McJpaController;
import com.tct.data.jpa.CurrentProcessJpaController;
import java.util.ArrayList;
import java.util.List;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import com.tct.data.jpa.McMoveJpaController;
import com.tct.data.jpa.PcsQtyJpaController;
import com.tct.data.jpa.*;
import java.rmi.server.UID;
import com.time.timeCurrent;
import java.util.UUID;
import javax.faces.context.FacesContext;

import javax.persistence.Persistence;
public class MoveProcess {
	private boolean moveProcessPopup = false;
	private boolean moveProcessPopupConfirm = false;
	private boolean moveProcessPopupConfirmYes = false;
	private boolean moveProcessPopupConfirmNo = false;
        private String  mcName = null;

	private String typeClose;
	private String visibles;
        private List<SelectItem> selectMcGood = null;
        private String mcNameUse = null;
        
        private CurrentProcess currentNewProcess = null;
        private McMove   currentMcmove = null;
        private PcsQty  currentPcsQty  = null; 
        
        private String validateSelectMc  = null;
        public ShareDataOfSession getShareData(){
          FacesContext context = FacesContext.getCurrentInstance();
          return   (ShareDataOfSession) context.getExternalContext().getSessionMap().get("sharedata");  
        }
        public BarcodeAuto getBarcodeAuto(){  
            FacesContext context = FacesContext.getCurrentInstance();
            return (BarcodeAuto) context.getExternalContext().getSessionMap().get("barcode"); 
        }
        
        CurrentProcessJpaController  currentProcessJpaController  = null;
        McJpaController   mcJpaController  = null;
        McMoveJpaController  mcMoveJpaController = null;
        PcsQtyJpaController  pcsQtyJpaController  = null;
        MainDataJpaController  mainDataJpaController = null;
        Md2pcJpaController  md2pcJpaController = null;
        public CurrentProcessJpaController getCurrentProcessJpaController()
        {
            if(currentProcessJpaController  == null)
            {
                currentProcessJpaController = new CurrentProcessJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
            }
            return currentProcessJpaController;
        }
        public McJpaController  getMcJpaController()
        {
            if(mcJpaController ==null)
            {
                mcJpaController = new McJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
            }
            return mcJpaController;
        }
        public   McMoveJpaController getMcMoveJpaController()
        {
            if(mcMoveJpaController  == null)
            {
                mcMoveJpaController  = new McMoveJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
            }
            return mcMoveJpaController;
        } 
        public PcsQtyJpaController  getPcsQtyJpaController()
        {
            if(pcsQtyJpaController == null)
            {
                pcsQtyJpaController = new PcsQtyJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
            }
            return pcsQtyJpaController;
        }
        public MainDataJpaController getMainDataJpaController()
        {
            if(mainDataJpaController  == null)
            {
                mainDataJpaController  = new MainDataJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
            }
            return mainDataJpaController ;
        }
         private Md2pcJpaController getMd2pcJpaController()
            {
             if(md2pcJpaController == null)
             {
                 md2pcJpaController  = new Md2pcJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
             }  
             return md2pcJpaController;
            } 
        
        
        
	public void closePopup(String typeClose) {
		this.typeClose = typeClose;
		if (this.typeClose.equals("moveProcessPopup")) {
			
                     if(!mcNameUse.equals("0")){
                        moveProcessPopup = false;
			moveProcessPopupConfirm = true;
                     }
                     else
                     {
                         validateSelectMc ="Select one mc";
                     }
                        
		} else if (this.typeClose.equals("moveProcessPopupClose")) {
			moveProcessPopup = false;
                        mcNameUse = "0";
                        validateSelectMc  = null;
		} else if (this.typeClose.equals("moveProcessPopupConfirmYes")) {
			
                    
                        
                        insertData();
                        //==== Set menu
                         ///========= Set menu
                         getBarcodeAuto().status  = "Open";   
                         getShareData().openProcessButton      = true;
                         getBarcodeAuto().openProcessButtonShow   = "open_process_button_disable";
                         getShareData().closeProcessButton        = true;
                         getBarcodeAuto().closeProcessButtonShow    = "close_process_button_disable";
                         getShareData().mcDownTimeButton       = true;
                         getBarcodeAuto().mcDownTimeButtonShow   = "mc_downtime_button_disable";
                         getShareData().holdTimeButton           = true;
                         getBarcodeAuto().holdTimeButtonShow       = "hold_process_button_disable"; 
                         getShareData().stopTimeButton           = true;
                         getBarcodeAuto().stopTimeButtonShow       = "stop_downtime_button_disable";
                         getShareData().moveProcessButton        = true;
                         getBarcodeAuto().moveProcessButtonShow    = "move_mc_button_disable";
                         mcNameUse = "0";
                         validateSelectMc  = null;
                         moveProcessPopupConfirm = false;
			//moveProcessPopupConfirmYes = true;
		} else if (this.typeClose.equals("moveProcessPopupConfirmNo")) {
			moveProcessPopupConfirm = false;
			//moveProcessPopupConfirmNo = true;
		} else if (this.typeClose.equals("moveProcessPopupConfirmYesClose")) {
			moveProcessPopupConfirmYes = false;
		} else if (this.typeClose.equals("moveProcessPopupConfirmNoClose")) {
			moveProcessPopupConfirmNo = false;
		}
	}

	public void openPopupType(String visibles) {
		this.visibles = visibles;
		if(this.visibles.equals("Movemc"))
                {
                    if(!getShareData().moveProcessButton)         
                        moveProcessPopup   = true; 
                        loadData();
                }
	}

	public boolean isMoveProcessPopup() {
		return moveProcessPopup;
	}

	public boolean isMoveProcessPopupConfirm() {
		return moveProcessPopupConfirm;
	}

	public boolean isMoveProcessPopupConfirmYes() {
		return moveProcessPopupConfirmYes; 
                
	}

	public boolean isMoveProcessPopupConfirmNo() {
		return moveProcessPopupConfirmNo;
	}

    /**
     * @return the mcName
     */
    public String getMcName() {
        return mcName;
    }

    /**
     * @param mcName the mcName to set
     */
    public void setMcName(String mcName) {
        this.mcName = mcName;
    }
    public void loadData()
    {
        
        CurrentProcess  dataListCurrent  =  getCurrentProcessJpaController().findCurrentProcess( getShareData().mainDataId.getCurrentNumber());
        mcName  = dataListCurrent.getMcId().getMcName();
        
        String  mcGStatusGood  ="good";
        List<Mc>   lisAlltMcGood  =  getMcJpaController().findListMCGood(mcGStatusGood); 
        selectMcGood = new ArrayList<SelectItem>();
        selectMcGood.add(new SelectItem("0","------ Select One -------"));
        for(Mc displayListOne : lisAlltMcGood){
            System.out.println("id  mc : "+displayListOne.getId()+" Mc name :  "+displayListOne.getMcName());
            selectMcGood.add(new SelectItem(displayListOne.getId(),displayListOne.getMcName()));  
        }
        
    }
      public void ChangNameValue(ValueChangeEvent event) {
            //System.out.println("================== Change mc =======================");
        mcNameUse = (String) event.getNewValue();   
        if(mcNameUse.equals("0"))
        {
              validateSelectMc  = "Select one mc";
        }
        else
        {
              validateSelectMc  = "";;
        }
        
//            PrintOut(mcNameUse);
     }

    /**
     * @return the selectMcGood
     */
    public List<SelectItem> getSelectMcGood() {
        return selectMcGood;
    }

    /**
     * @param selectMcGood the selectMcGood to set
     */
    public void setSelectMcGood(List<SelectItem> selectMcGood) {
        this.selectMcGood = selectMcGood;
    }

    /**
     * @return the mcNameUse
     */
    public String getMcNameUse() {
        return mcNameUse;
    }

    /**
     * @param mcNameUse the mcNameUse to set
     */
    public void setMcNameUse(String mcNameUse) {
        this.mcNameUse = mcNameUse;
    } 
    public void insertData()
    {
        timeCurrent  timeNow  =  new timeCurrent();
        System.out.println("======= Insert data ======="); 
        
        //======Preview old process 
        CurrentProcess  oldProcess  =  getCurrentProcessJpaController().findCurrentProcess(getBarcodeAuto().mainDataId.getCurrentNumber());
        //===== update  current proces in current table
        oldProcess.setCurrentProc("0");
        try
        {
            getCurrentProcessJpaController().edit(oldProcess);
        }
        catch(Exception e)
        {
            System.out.println("=== Error not set Current proc "+e);
        }
        //======= create new current process
        String uuidCurrents  =  UUID.randomUUID().toString();
        getCurrentNewProcess().setId(uuidCurrents);
        getCurrentNewProcess().setMainDataId(oldProcess.getMainDataId());
        Mc changeObjmcNameuse  =  getMcJpaController().findMc(mcNameUse);
        getCurrentNewProcess().setMcId(changeObjmcNameuse);
        getCurrentNewProcess().setCurrentProc("1");
        getCurrentNewProcess().setStatus("Open");
        getCurrentNewProcess().setTimeCurrent(timeNow.getDate());
        getCurrentNewProcess().setShift(oldProcess.getShift());
        getCurrentNewProcess().setHr(oldProcess.getHr());
        getCurrentNewProcess().setLine(oldProcess.getLine());
        try
        {
            getCurrentProcessJpaController().create(getCurrentNewProcess());
        }
        catch(Exception e)
        {
            System.out.println("==== Error not to create new current process "+e);
        } 
        //======= update current number in maindata
        getBarcodeAuto().mainDataId.setCurrentNumber(getCurrentNewProcess().getId());
        
        System.out.println("===================== mainDataid "+getBarcodeAuto().mainDataId);
        try
        {
            getMainDataJpaController().edit(getBarcodeAuto().mainDataId);
        }
        catch(Exception e)
        {
            System.out.println("==== Error not update maindata id"+e);
        }
        //======= create pcsqty
        String  uuidPcsQty  = UUID.randomUUID().toString();
        getCurrentPcsqty().setId(uuidPcsQty);
        getCurrentPcsqty().setWipQty("0");
        getCurrentPcsqty().setCurrentProcessId(getCurrentNewProcess());
        getCurrentPcsqty().setQtyType("in");
        
        PcsQty oldPcsQty =  getPcsQtyJpaController().findPcsQtyCurrentProcessId(oldProcess); 
        getCurrentPcsqty().setQty(oldPcsQty.getQty());
        try
        {
            getPcsQtyJpaController().create(getCurrentPcsqty());
        }
        catch(Exception e)
        {
            System.out.println("=== Error not to create  new pcsqty ");
        }        
         
        //======= create move mc 
        String  uuidMcMove  = UUID.randomUUID().toString();
        getCurrentMcmove().setId(uuidMcMove);
        getCurrentMcmove().setCurrentProcessId(getCurrentNewProcess());
        getCurrentMcmove().setAftorMc(oldProcess.getMcId());
        getCurrentMcmove().setBeforMc(getCurrentNewProcess().getMcId());
        getCurrentMcmove().setRefMoveIdCurrent(oldProcess.getId());
        try
        {
            getMcMoveJpaController().create(getCurrentMcmove());
        }
        catch(Exception e)
        {
            System.out.println("===== Error not to create mc move "+e);
        } 
         Md2pc  objDataNextProcess  = getMd2pcJpaController().findSqProcess(getBarcodeAuto().idProc,getBarcodeAuto().lotControlId.getModelPoolId()); 
         int newSqPocess   = objDataNextProcess.getSqProcess();
          for (ProcList objProList : getBarcodeAuto().processListData)
            {
                 if(objProList.getSqpProcNum() == newSqPocess)
                 { 
                     try{
                        objProList.setMcName(getCurrentNewProcess().getMcId().getMcName());
                     }
                     catch(Exception e)
                     {
                        objProList.setMcName(null);
                     }  
                     objProList.setStartTime(getCurrentNewProcess().getTimeCurrent().toString());
                     objProList.setIndex(getCurrentNewProcess());
                     objProList.setNgQty("");
                     objProList.setNgRepair("");
                     objProList.setOuPutQty("");
                     objProList.setCssCurrentStatusProcess("process_td_now");
                     objProList.setPercentNgNormal("0.00 %");
                     objProList.setPercentNgRepair("0.00 %");
                     break;
                 }
            
            }
        
        
        
    } 
    public CurrentProcess getCurrentNewProcess()
    {
        if(currentNewProcess  == null)
        {
            currentNewProcess  = new CurrentProcess();
        }
        return currentNewProcess;
    }
    public McMove  getCurrentMcmove()
    {
        if(currentMcmove  == null)
        {
            currentMcmove  = new McMove();
        }
        return currentMcmove ;
    }
    public PcsQty getCurrentPcsqty()
    {
        if(currentPcsQty == null)
        {
            currentPcsQty  = new PcsQty();
        }
        return currentPcsQty ; 
    }

    /**
     * @return the validateSelectMc
     */
    public String getValidateSelectMc() {
        return validateSelectMc;
    }

    /**
     * @param validateSelectMc the validateSelectMc to set
     */
    public void setValidateSelectMc(String validateSelectMc) {
        this.validateSelectMc = validateSelectMc;
    }
}
