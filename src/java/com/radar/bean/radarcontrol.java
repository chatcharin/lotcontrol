/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.radar.bean;

import com.appCinfigpage.bean.fc;
import com.appCinfigpage.bean.userLogin;
import com.radar.Data.RadarData;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import com.tct.data.Radar;
import com.tct.data.jpa.RadarJpaController;
import com.tct.data.RadarColor;
import com.tct.data.jpa.RadarColorJpaController;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 28, 2012, Time : 1:58:01 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class radarcontrol {
   private List<RadarData> itemRadar;
   private List<SelectItem> itemLineNo; 
   private RadarData  currentRadarData;
   private Radar  currentRadar ;
   private RadarJpaController  radarJpaController = null;
   private RadarColor currentColor;
   private RadarColorJpaController  radarColorJpaController = null;
   private List<SelectItem> itemTargetColor;
   private List<SelectItem> itemActualColor;
   private List<SelectItem> itemPercentColor;
   private String validateTarget = "";
   private String validaetActualColor ="";
   private String validatePercentColor = "";
   private String validatelineNo;
   private String validateTargetNum;
   private String validatePercentNum;
   private String validateLineName;
   private String validateActual;
    
   public radarcontrol() {
       loadItemRadar();
       loadDataAll();
       if(getCurrentColor().getIdRadarColor()!=null)
       {
           loadRadarDetail(getCurrentColor());
       }
   }
   
   public RadarColorJpaController getRadarColorJpaController()
   {
       if(radarColorJpaController == null)
       {
           radarColorJpaController = new RadarColorJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
       }
       return  radarColorJpaController;
   }
   public RadarJpaController getRadarJpaController()
   {  if(radarJpaController == null){
          radarJpaController = new RadarJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
      }
      return radarJpaController;
   }
   public void loadItemRadar()
   { 
        getItemLineNo().add(new SelectItem("0","---- Select One ----"));
        for(int i= 1 ; i <=8 ;i++)
        {
            getItemLineNo().add(new SelectItem(Integer.toString(i),Integer.toString(i)));
        }
      
   }
   public void loadDataAll()
   {
       prepareColor(); 
       currentColor = getRadarColorJpaController().findAllOne(); 
       fc.print("id color : "+getCurrentColor().getIdRadarColor()); 
   }
   public void loadRadarDetail(RadarColor idRadar)
   {
       fc.print("======== Detail Rader =========");
       List<Radar>  objRadar  = getRadarJpaController().findByIdColor(idRadar);
        
        for(Radar objList:objRadar)
        {
            getItemRadar().add(new RadarData(objList.getIdRadar(), objList.getLineNo(), objList.getNameRadar(), objList.getTargetRadar(), objList.getActualRadar(), objList.getPercentRadar(), objList.getShowRadar(),idRadar.getIdRadarColor())); 
             
        }   
      
       for(RadarData objList2:getItemRadar()){
         for(SelectItem objList:getItemLineNo())
            {      
                    if(objList.getLabel().equals(objList2.getLineNo())){
                        fc.print("label"+objList.getLabel());
                        getItemLineNo().remove(objList);
                        break;
                    }
            }
        }
   }
   public void prepareColor()
   {
       currentColor = new RadarColor();
   } 
   public void changeToSave()
   {
       if(validaetActualColor=="" && validatePercentColor =="" && validateTarget ==""){
       if(getCurrentColor().getIdRadarColor()==null){
            fc.print("========== Change to save =======");
            getCurrentColor().setIdRadarColor(gemId());
            getCurrentColor().setDeleted(0);
            getCurrentColor().setRandarName("RD");
            getCurrentColor().setCActual(getCurrentColor().getCActual());
            getCurrentColor().setCActualShow(getCurrentColor().getCActualShow());
            getCurrentColor().setCPercentShow(getCurrentColor().getCPercentShow());
            getCurrentColor().setCPrecent(getCurrentColor().getCPrecent());
            getCurrentColor().setCTarget(getCurrentColor().getCTarget());
            getCurrentColor().setCTargetShow(getCurrentColor().getCTargetShow());
            create();
            
            if(getItemRadar().size()>0)
            {
                for(RadarData objList:getItemRadar())
                { 
                    prepareRadar();
                    fc.print("id :"+objList.getIndex()+" color id :"+getCurrentColor().getIdRadarColor());
                    
                    fc.print("id :"+objList.getIndex());
                    fc.print("id "+objList.getActual());
                    fc.print("id color "+getCurrentColor());
                    fc.print("line no "+objList.getLineNo());
                    fc.print("percent : "+objList.getPercent());
                    fc.print("show  :" + objList.getShow());
                    fc.print("target : "+objList.getTarget());
                     
                    
                    
                    getCurrentRadar().setIdRadar(objList.getIndex());
                    getCurrentRadar().setActualRadar(objList.getActual());
                    getCurrentRadar().setDeleted(0);
                    getCurrentRadar().setIdColor(getCurrentColor());
                    getCurrentRadar().setLineNo(objList.getLineNo());
                    getCurrentRadar().setNameRadar(objList.getLineName());
                    getCurrentRadar().setPercentRadar(objList.getPercent());
                    getCurrentRadar().setShowRadar(objList.getShow().toString());
                    getCurrentRadar().setTargetRadar(objList.getTarget()); 
                    createRadar();
                }
            }  
       }
       else
       {
           fc.print("====== Edit =========");
          // currentColor = getRadarColorJpaController().findAllOne();
//           getCurrentColor().setCTargetShow(g);
//           getCurrentColor().setCActualShow(currentColor.getCActualShow());
           editColorRadar();
           if(getItemRadar().size()>0){
                for(RadarData objList:getItemRadar())
                {
                    prepareRadar();
                    currentRadar = getRadarJpaController().findRadar(objList.getIndex());
                    fc.print("id current :"+getCurrentRadar() +"id real :"+objList.getIndex());
                    if(getCurrentRadar().getIdRadar()== null)
                    {   
                            getCurrentRadar().setIdRadar(gemId());
                            getCurrentRadar().setActualRadar(objList.getActual());
                            getCurrentRadar().setDeleted(0);
                            getCurrentRadar().setIdColor(getCurrentColor());
                            getCurrentRadar().setLineNo(objList.getLineNo());
                            getCurrentRadar().setNameRadar(objList.getLineName());
                            getCurrentRadar().setPercentRadar(objList.getPercent());
                            getCurrentRadar().setShowRadar(objList.getShow().toString());
                            getCurrentRadar().setTargetRadar(objList.getTarget());  
                            createRadar();
                    }  
                }
           }
           
       }
       resetDataNew(); 
       String link  = "pages/setting/radar/detailview.xhtml";
       userLogin.getSessionMenuBarBean().setParam(link);  
       }
   }
   public void resetDataNew()
   {
       itemActualColor  = new ArrayList<SelectItem>();
       itemPercentColor = new ArrayList<SelectItem>();
       itemTargetColor  = new ArrayList<SelectItem>();
       
   
   }
   public void create()
   {
       try
       {
          getRadarColorJpaController().create(getCurrentColor());
       }
       catch(Exception e)
       {
           fc.print("==== Error not to inset data"+e);
       }
   }
   public void editRadar()
   {
       try
       {
           getRadarJpaController().edit(currentRadar);
       }
       catch(Exception e)
       {
           fc.print("Error not to edit"+e);
       }
       
   }
   public void editColorRadar()
   {
       try
       {
           getRadarColorJpaController().edit(currentColor);
       }
       catch(Exception e)
       {
          fc.print("Error not to edit"+e);
       }
   }
   public void changeToCancel()
   { 
        String link  = "pages/setting/radar/detailview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);  
        prepareColor();
        resetDataAll();
        resetDataNew();
        loadDataAll();
   } 
    public void changeToEdit()
   {
       String link  = "pages/setting/radar/createview.xhtml";
       userLogin.getSessionMenuBarBean().setParam(link);  
       //loadItemRadar(); 
       prepareColor();
       prepareRadar();
       itemRadar = new ArrayList<RadarData>();
       loadColor(); 
       currentColor = getRadarColorJpaController().findAllOne();
       if(getCurrentColor().getIdRadarColor()!=null)
       {
           loadRadarDetail(getCurrentColor());
       }
   }
   private void loadColor()
   {
       String [] colorName  = {"Black","Bule","Gray","Green","Purple","Red ","Yellow"}; 
       getItemActualColor().add(new SelectItem("null","--- Select Color ---"));
       getItemPercentColor().add(new SelectItem("null","--- Select Color ---"));
       getItemTargetColor().add(new SelectItem("null","--- Select Color ---"));
       for(int i=0;i < colorName.length;i++)
       {
           getItemActualColor().add(new SelectItem(colorName[i],colorName[i]));
           getItemPercentColor().add(new SelectItem(colorName[i],colorName[i]));
           getItemTargetColor().add(new SelectItem(colorName[i],colorName[i]));
       }
   
   }
   public void prepareRadar()
   {
       currentRadar = new Radar();
   }
   public void createRadar()
   {
       try
       {
           getRadarJpaController().create(getCurrentRadar());
       }
       catch(Exception e)
       {
           fc.print("Error not to insert data"+e);
       }
   }
   public void changeToDel(String indexDel)
   {
       fc.print("===== Change To delete");
       for(RadarData objList : getItemRadar())
       {
           if(objList.getIndex().equals(indexDel))
           { 
                prepareRadar();
                currentRadar  = getRadarJpaController().findRadar(indexDel);
                getCurrentRadar().setDeleted(1);
                editRadar();
                getItemLineNo().add(new SelectItem(objList.getLineNo(),objList.getLineNo()));
                getItemRadar().remove(objList);
                break;
           }
       }  
       
       List<String> intNum  = new ArrayList<String>(); 
       for(SelectItem objList:getItemLineNo()){
            intNum.add(objList.getValue().toString());
       }
       Collections.sort(intNum); 
       itemLineNo = new ArrayList<SelectItem>(); 
       for(String objList:intNum)
       {
           if(objList.equals("0")){
                getItemLineNo().add(new SelectItem(objList,"---- Select One ----"));
           }
           else
           {
                getItemLineNo().add(new SelectItem(objList,objList));
           }
       }
   }
   public void resetDataAll()
   {
       currentRadarData  = new RadarData(null, null, null, null, null, null, null, null);
   } 
   public void removeLineNo(String index)
   {
       fc.print("====== Remove Line No ========");
       for(SelectItem objList:getItemLineNo()){
           if(objList.getLabel().equals(index)){ 
                getItemLineNo().remove(objList);
                break;
           }
       }
   } 
   public void changeToCreate()
   {
       //checkLineName();
       fc.print("====== Crate  Radar ======="); 
       if(getItemRadar().size()<=8 && !getCurrentRadarData().getLineNo().equals("0")){
           fc.print("Line no. : "+getCurrentRadarData().getLineNo());
            getItemRadar().add(new RadarData(gemId(),getCurrentRadarData().getLineNo(),getCurrentRadarData().getLineName(), getCurrentRadarData().getTarget(), getCurrentRadarData().getActual(), getCurrentRadarData().getPercent(),getCurrentRadarData().getShow(), null));
            removeLineNo(getCurrentRadarData().getLineNo()); 
            resetLineNo(getItemRadar());
            resetDataAll();
       }
       else
       {
           fc.print("==== More than line 8 ===");
       }
   }
   public void resetLineNo(List<RadarData>  itemRadarAll)
   {
       List<String> intString = new ArrayList<String>();
       List<RadarData> buffer = new ArrayList<RadarData>();
       for(RadarData objList : itemRadarAll)
       {
           intString.add(objList.getLineNo());
           buffer.add(new RadarData(objList.getIndex(),objList.getLineNo(), objList.getLineName(), objList.getTarget(), objList.getActual(), objList.getPercent(), objList.getShow(), null));
       }
       itemRadar = new ArrayList<RadarData>();
       Collections.sort(intString); 
       for(String objList2 : intString)
       {
           for(RadarData objList : buffer){
                if(objList2.equals(objList.getLineNo()))
                {
                    getItemRadar().add(new RadarData(objList.getIndex(),objList.getLineNo(), objList.getLineName(), objList.getTarget(), objList.getActual(), objList.getPercent(), objList.getShow(), null));    
                }
           }
       }
       
   }
   public String gemId()
   {
       String id  = UUID.randomUUID().toString();
       return id;
   }
   public void changeTargetColor(ValueChangeEvent event)
   { 
       fc.print("==== Change event =======");
        
       if(!event.getNewValue().toString().equals("null"))
       {
           validateTarget = "";
       }
       else
       {
           validateTarget = "Select one";
       }
   }
   public void changeActualColor(ValueChangeEvent event)
   {
       if(!event.getNewValue().toString().equals("null"))
       {
           validaetActualColor = "";
       }
       else
       {
           validaetActualColor = "Select one";
       } 
   }
   public void changePercentColor(ValueChangeEvent event)
   {
       if(!event.getNewValue().toString().equals("null"))
       {
           validatePercentColor ="";
       }
       else
       {
           validatePercentColor = "Select one";
       } 
   }
   public void changeLineNo(ValueChangeEvent event)
   {
       fc.print("====== Change line no ======");
       if(!event.getNewValue().toString().equals("0"))
       {
           validatelineNo  = "";
       }
       else
       {
           validatelineNo  = "Select one";
       }
   }
   public void  checkLineName()
   {
       if(!getCurrentRadar().getNameRadar().equals(""))
       {
           validateLineName  = "";
       }
       else
       {
            validateLineName  = "Invalid input ";
       }
   }
   public void checkTarget()
   {
   
   }
   public void checkActual()
   {
   
   }
   public void checkPercent()
   {
   }
   public List<RadarData> getItemRadar() {
        if(itemRadar ==null)
        {
            itemRadar = new ArrayList<RadarData>();
        }
        return itemRadar;
    }

    public void setItemRadar(List<RadarData> itemRadar) {
        this.itemRadar = itemRadar;
    }

    public List<SelectItem> getItemLineNo() {
        if(itemLineNo == null)
        {
            itemLineNo = new ArrayList<SelectItem>();
        }
        return itemLineNo;
    }

    public void setItemLineNo(List<SelectItem> itemLineNo) {
        this.itemLineNo = itemLineNo;
    }

    public RadarData getCurrentRadarData() {
        if(currentRadarData == null)
        {
            currentRadarData  = new RadarData(null, null, null, null, null, null, null, null);
        }
        return currentRadarData;
    }

    public void setCurrentRadarData(RadarData currentRadarData) {
        this.currentRadarData = currentRadarData;
    }

    public RadarColor getCurrentColor() {
        if(currentColor == null)
        {
            currentColor = new RadarColor();
        }
        return currentColor;
    }

    public void setCurrentColor(RadarColor currentColor) {
        this.currentColor = currentColor;
    }

    public Radar getCurrentRadar() {
        if(currentRadar == null)
        {
            currentRadar = new Radar();
        }
        return currentRadar;
    }

    public void setCurrentRadar(Radar currentRadar) {
        this.currentRadar = currentRadar;
    }

    public List<SelectItem> getItemActualColor() {
        if(itemActualColor == null)
        {
            itemActualColor = new ArrayList<SelectItem>();
        }
        return itemActualColor;
    }

    public void setItemActualColor(List<SelectItem> itemActualColor) {
        this.itemActualColor = itemActualColor;
    }

    public List<SelectItem> getItemPercentColor() {
        if(itemPercentColor == null)
        {
            itemPercentColor = new ArrayList<SelectItem>();
        }
        return itemPercentColor;
    }

    public void setItemPercentColor(List<SelectItem> itemPercentColor) {
        this.itemPercentColor = itemPercentColor;
    }

    public List<SelectItem> getItemTargetColor() {
        if(itemTargetColor == null)
        {
            itemTargetColor  = new ArrayList<SelectItem>();
        }
        return itemTargetColor;
    }

    public void setItemTargetColor(List<SelectItem> itemTargetColor) {
        this.itemTargetColor = itemTargetColor;
    }

    public String getValidatePercentColor() {
        return validatePercentColor;
    }

    public void setValidatePercentColor(String validatePercentColor) {
        this.validatePercentColor = validatePercentColor;
    }
 
    public String getValidaetActualColor() {
        return validaetActualColor;
    }

    public void setValidaetActualColor(String validaetActualColor) {
        this.validaetActualColor = validaetActualColor;
    }

    public String getValidateTarget() {
        return validateTarget;
    }

    public void setValidateTarget(String validateTarget) {
        this.validateTarget = validateTarget;
    }

    public String getValidateActual() {
        return validateActual;
    }

    public void setValidateActual(String validateActual) {
        this.validateActual = validateActual;
    }

    public String getValidateLineName() {
        return validateLineName;
    }

    public void setValidateLineName(String validateLineName) {
        this.validateLineName = validateLineName;
    }

    public String getValidatePercentNum() {
        return validatePercentNum;
    }

    public void setValidatePercentNum(String validatePercentNum) {
        this.validatePercentNum = validatePercentNum;
    }

    public String getValidateTargetNum() {
        return validateTargetNum;
    }

    public void setValidateTargetNum(String validateTargetNum) {
        this.validateTargetNum = validateTargetNum;
    }

    public String getValidatelineNo() {
        return validatelineNo;
    }

    public void setValidatelineNo(String validatelineNo) {
        this.validatelineNo = validatelineNo;
    }
    
}   

