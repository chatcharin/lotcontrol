/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.radar.Data;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 28, 2012, Time : 2:03:25 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class RadarData {
    private String index;
    private String lineNo;
    private String lineName;
    private String target;
    private String actual;
    private String percent;
    private String show;
    private String indexRadarColor;

    public RadarData(String index, String lineNo, String lineName, String target, String actual, String percent, String show, String indexRadarColor) {
        this.index = index;
        this.lineNo = lineNo;
        this.lineName = lineName;
        this.target = target;
        this.actual = actual;
        this.percent = percent;
        this.show = show;
        this.indexRadarColor = indexRadarColor;
    }

    public String getActual() {
        return actual;
    }

    public void setActual(String actual) {
        this.actual = actual;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getIndexRadarColor() {
        return indexRadarColor;
    }

    public void setIndexRadarColor(String indexRadarColor) {
        this.indexRadarColor = indexRadarColor;
    }

    public String getLineName() {
        return lineName;
    }

    public void setLineName(String lineName) {
        this.lineName = lineName;
    }

    public String getLineNo() {
        return lineNo;
    }

    public void setLineNo(String lineNo) {
        this.lineNo = lineNo;
    } 
    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    public String getShow() {
        return show;
    }

    public void setShow(String show) {
        this.show = show;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    } 
}
