/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reportsetting.bean;

import com.appCinfigpage.bean.fc;
import com.tct.data.ReportSetting;
import com.tct.data.ReportSettingAttribute;
import com.tct.data.jpa.ReportSettingAttributeJpaController;
import com.tct.data.jpa.ReportSettingJpaController;
import com.appCinfigpage.bean.userLogin;
import com.repostsetting.Data.RepoetSettingDataField;
import com.repostsetting.Data.ReportSettingBufferData;
import com.repostsetting.Data.ReportSettingListViewData;
import com.tct.data.jsf.util.PaginationHelper;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.faces.model.ArrayDataModel;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.persistence.Persistence;

/**
 *
 * Machine User
 *
 * @author pang Development by Chusak laojang Date : Oct 18, 2012, Time :
 * 5:56:58 PM Copy Right 4 Xtreme Co.,Ltd.
 */
public class ReportSettingControl {

    private List<RepoetSettingDataField> currentReportsettinField;
    private ReportSetting currentReportSetting;
    private ReportSettingAttribute currentReportSettingAttribute;
    private RepoetSettingDataField currentReportSettingDataField;
    private ReportSettingAttributeJpaController reportSettingAttributeJpaController = null;
    private ReportSettingJpaController reportSettingJpaController = null;
    private ReportSettingBufferData currentBufferDta;
    private DataModel dataModelReportSetting;
    private List<ReportSettingListViewData> itemAllReportSetting;
    private PaginationHelper worksheetpagination;

    public ReportSettingControl() {
        loadAllData();
    }

    public ReportSettingAttributeJpaController getReportSettingAttributeJpaController() {
        if (reportSettingAttributeJpaController == null) {
            reportSettingAttributeJpaController = new ReportSettingAttributeJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return reportSettingAttributeJpaController;
    }

    public ReportSettingJpaController getReportSettingJpaController() {
        if (reportSettingJpaController == null) {
            reportSettingJpaController = new ReportSettingJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return reportSettingJpaController;
    }

    public void changeToInsert() {
        resetDataAll();
        String link = "pages/setting/reportsetting/createview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
        getCurrentReportSetting().setIdReportSetting(gemId());
    }

    public void changeToCancel() {
        String link = "pages/setting/reportsetting/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
        resetDataAll();
        loadAllData();
    }
//    loadAllData

    public void prepareItem() {
        itemAllReportSetting = new ArrayList<ReportSettingListViewData>();
    }

    public void loadAllData() {
        int i = 1;
        prepareItem();
        List<ReportSetting> objReportSetting = getReportSettingJpaController().findAllNotDeleted();
        for (ReportSetting objList : objReportSetting) {
            getItemAllReportSetting().add(new ReportSettingListViewData(objList.getIdReportSetting(), objList.getNameSetting(), objList.getDescriptionSetting(), i));
            i++;
        }
        setDataModelReportSetting();

    }

    public void changeToCreate() {
        getCurrentReportSetting().setDeleted(0);
        createReportSetting();
        if (!getCurrentReportsettinField().isEmpty()) {
            for (RepoetSettingDataField objList : getCurrentReportsettinField()) {
                prepareReportSettingAttribute();
                getCurrentReportSettingAttribute().setDeleted(0);
                getCurrentReportSettingAttribute().setIdReportSetting(getCurrentReportSetting());
                getCurrentReportSettingAttribute().setIdReportSettingAtt(objList.getIndexFiled());
                getCurrentReportSettingAttribute().setNameReportAtt(objList.getName());
                getCurrentReportSettingAttribute().setValueReportAtt(objList.getValue());
                createReportSettingAttribute();
            }
        }

        resetDataAll();
        loadAllData();
        String link = "pages/setting/reportsetting/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
    }

    public void prepareReportSettingAttribute() {
        currentReportSettingAttribute = new ReportSettingAttribute();
    }

    public void createReportSettingAttribute() {
        try {
            getReportSettingAttributeJpaController().create(getCurrentReportSettingAttribute());
        } catch (Exception e) {
            fc.print("Error not to insert report setting attribute " + e);
        }
    }

    public void resetDataAll() {
        worksheetpagination = null;
        currentReportSetting = new ReportSetting();
        currentReportsettinField = new ArrayList<RepoetSettingDataField>();
        dataModelReportSetting = new ArrayDataModel();
    }

    public void createReportSetting() {
        try {
            getReportSettingJpaController().create(currentReportSetting);
        } catch (Exception e) {
            fc.print("error" + e);
        }
        if (!currentReportsettinField.isEmpty()) {
        }
    }

    public void changeToSave() {
        editReportSetting();
        if (!getCurrentReportsettinField().isEmpty()) {
            for (RepoetSettingDataField objList : getCurrentReportsettinField()) {
                fc.print("id edit : " + objList.getIndexFiled());
                prepareCurrentReportSettingDataField();
                currentReportSettingAttribute = getReportSettingAttributeJpaController().findReportSettingAttribute(objList.getIndexReport());
                if (currentReportSettingAttribute!= null) {
//                fc.print(currentReportSettingAttribute.getNameReportAtt());
                    getCurrentReportSettingAttribute().setNameReportAtt(objList.getName());
                    getCurrentReportSettingAttribute().setValueReportAtt(objList.getValue());
                    editReportSettingField();
                } else {
                    getCurrentReportSettingAttribute().setNameReportAtt(objList.getName());
                    getCurrentReportSettingAttribute().setDeleted(0);
                    getCurrentReportSettingAttribute().setIdReportSetting(currentReportSetting);
                    getCurrentReportSettingAttribute().setIdReportSettingAtt(gemId());
                    getCurrentReportSettingAttribute().setValueReportAtt(objList.getValue());
                    createReportSettingAttribute();
                }
            }
        }
        String link = "pages/setting/reportsetting/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
        loadAllData();
    }

    public void prepareCurrentReportSettingDataField() {
        currentReportSettingAttribute = new ReportSettingAttribute();
    }

    public void editReportSettingField() {
        try {
            getReportSettingAttributeJpaController().edit(getCurrentReportSettingAttribute());
        } catch (Exception e) {
            fc.print("Error not to edit report setting field" + e);
        }
    }

    public void editReportSetting() {
        try {
            getReportSettingJpaController().edit(currentReportSetting);
        } catch (Exception e) {
            fc.print("Errorr not to edit report setting" + e);
        }
    }

    public void changeToEdit() {
        String resportSettingEdit = getItemAllReportSetting().get(getDataModelReportSetting().getRowIndex()).getIndex();
        resetDataAll();
        currentReportSetting = getReportSettingJpaController().findReportSetting(resportSettingEdit);
        fc.print("id edit :" + resportSettingEdit + "obj list" + currentReportSetting.getNameSetting());
        loadItemListReportSetting();
        String link = "pages/setting/reportsetting/editview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
    }

    public void changeToAddEdit() {
    }

    public void loadItemListReportSetting() {
        List<ReportSettingAttribute> objAttribute = getReportSettingAttributeJpaController().findReportSettingAttributeNotDelete(getCurrentReportSetting());
        for (ReportSettingAttribute objList : objAttribute) {
            getCurrentReportsettinField().add(new RepoetSettingDataField(objList.getIdReportSettingAtt(), objList.getNameReportAtt(), objList.getValueReportAtt(), objList.getIdReportSetting().toString()));
        }
    }

    public void changeToDeleted(String idDel) {
        fc.print("Id del : " + idDel);
        for (RepoetSettingDataField objList : getCurrentReportsettinField()) {
            if (objList.getIndexFiled().equals(idDel)) {
                getCurrentReportsettinField().remove(objList);
                break;
            }
        }
    }

    public void changeToDeletedListView() {
        fc.print("del report setting");
        String resportSettingEdit = getItemAllReportSetting().get(getDataModelReportSetting().getRowIndex()).getIndex();
        resetDataAll();
        currentReportSetting = getReportSettingJpaController().findReportSetting(resportSettingEdit);
        getCurrentReportSetting().setDeleted(1);
        editReportSetting();
        resetDataAll();
        loadAllData();
    }

    public void changeToAdd() {
        currentBufferDta.setIndex(gemId());
        getCurrentReportsettinField().add(new RepoetSettingDataField(currentBufferDta.getIndex(), currentBufferDta.getName(), currentBufferDta.getValue(), gemId()));
        resetBuffer();
    }

    public void resetBuffer() {
        currentBufferDta = new ReportSettingBufferData(null, null, null);
    }

    public String gemId() {
        String id = UUID.randomUUID().toString();
        return id;
    }
    //    date model

    public void previous() {
        getWorksheetpagination().previousPage();
        setDataModelReportSetting();
    }

    public void next() {
        getWorksheetpagination().nextPage();
        setDataModelReportSetting();
    }

    public int getAllPages() {
        int all = getWorksheetpagination().getItemsCount();
        int pagesize = getWorksheetpagination().getPageSize();
        return ((int) all / pagesize) + 1;
    }

    public PaginationHelper getWorksheetpagination() {
        if (worksheetpagination == null) {
            worksheetpagination = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    //fc.print("size of data : "+getMasterListViewData().size());
                    return getItemAllReportSetting().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    fc.print("PageFirst : " + getPageFirstItem());
                    fc.print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (getItemAllReportSetting().size() > 0) {
                        listmodel = new ListDataModel(getItemAllReportSetting().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(getItemAllReportSetting());
                    }
                    return listmodel;
                }
            };
        }
        return worksheetpagination;
    }

    public List<RepoetSettingDataField> getCurrentReportsettinField() {
        if (currentReportsettinField == null) {
            currentReportsettinField = new ArrayList<RepoetSettingDataField>();
        }
        return currentReportsettinField;
    }

    public void setCurrentReportsettinField(List<RepoetSettingDataField> currentReportsettinField) {
        this.currentReportsettinField = currentReportsettinField;
    }

    public void changeToDelField(String indexDel) {
    }

    public ReportSetting getCurrentReportSetting() {
        if (currentReportSetting == null) {
            currentReportSetting = new ReportSetting();
        }
        return currentReportSetting;
    }

    public void setCurrentReportSetting(ReportSetting currentReportSetting) {
        this.currentReportSetting = currentReportSetting;
    }

    public RepoetSettingDataField getCurrentReportSettingDataField() {
        return currentReportSettingDataField;
    }

    public void setCurrentReportSettingDataField(RepoetSettingDataField currentReportSettingDataField) {
        this.currentReportSettingDataField = currentReportSettingDataField;
    }

    public ReportSettingBufferData getCurrentBufferDta() {
        if (currentBufferDta == null) {
            currentBufferDta = new ReportSettingBufferData(null, null, null);
        }
        return currentBufferDta;
    }

    public void setCurrentBufferDta(ReportSettingBufferData currentBufferDta) {
        this.currentBufferDta = currentBufferDta;
    }

    public ReportSettingAttribute getCurrentReportSettingAttribute() {
        if (currentReportSettingAttribute == null) {
            currentReportSettingAttribute = new ReportSettingAttribute();
        }
        return currentReportSettingAttribute;
    }

    public void setCurrentReportSettingAttribute(ReportSettingAttribute currentReportSettingAttribute) {
        this.currentReportSettingAttribute = currentReportSettingAttribute;
    }

    public DataModel getDataModelReportSetting() {
        return dataModelReportSetting;
    }

    public void setDataModelReportSetting() {
        this.dataModelReportSetting = getWorksheetpagination().createPageDataModel();
    }

    public List<ReportSettingListViewData> getItemAllReportSetting() {
        if (itemAllReportSetting == null) {
            itemAllReportSetting = new ArrayList<ReportSettingListViewData>();
        }
        return itemAllReportSetting;
    }

    public void setItemAllReportSetting(List<ReportSettingListViewData> itemAllReportSetting) {
        this.itemAllReportSetting = itemAllReportSetting;
    }
}
