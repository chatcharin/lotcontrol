/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.myaccount.bean;

import com.appCinfigpage.bean.fc;
import com.appCinfigpage.bean.userLogin;
import com.appConfigpage.Data.roleDetail;
import com.tct.data.RoleUser;
import com.tct.data.RoleUser;
import com.tct.data.Users;
import com.tct.data.jpa.UsersJpaController;
import com.tct.data.jpa.RoleUserJpaController;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Persistence;
/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 13, 2012, Time : 10:44:45 AM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class Myaccount {
    private String firstName ;
    private String lastName ;
    private String username;
    private String userName;
    private String passWord;
    private String departMent;
    private String description;
    private RoleUserJpaController  roleUserJpaController = null;
    private List<RoleUser>  objRoleUser ;
    private List<roleDetail>  itemRoleUser;
    private Users  objUser;
    private UsersJpaController  usersJpaController = null ;
    public UsersJpaController getUsersJpaController()
    {
        if(usersJpaController == null)
        {
            usersJpaController  = new UsersJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return usersJpaController;
    }
    
    public RoleUserJpaController getRoleUserJpaController()
    {
        if(roleUserJpaController == null)
        {
            roleUserJpaController = new RoleUserJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return roleUserJpaController;
    }
    public void showMyaccount()
    {
            
            firstName = userLogin.getSessionUser().getFirstName();
            lastName  = userLogin.getSessionUser().getLastName();
            userName  = userLogin.getSessionUser().getUserName();
            passWord  = userLogin.getSessionUser().getUserPassword();
            departMent = userLogin.getSessionUser().getDepartment();
            description = userLogin.getSessionUser().getDepartment();
            objRoleUser  = new ArrayList<RoleUser>();
            itemRoleUser  = null;
            loadUserRole();
            String link = "pages/appConfig/myaccount/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
    } 
    private int count;
    public void loadUserRole()
    {
            count = 0;
            objRoleUser  = getRoleUserJpaController().findByUserId(userLogin.getSessionUser());
            for(RoleUser listRoleUser:objRoleUser)
            {
                count++;
                getItemRoleUser().add(new roleDetail(listRoleUser.getId(),Integer.toString(count), listRoleUser.getRoleId().getRoleName(), listRoleUser.getRoleId().getDesscrition()));
            }
    }
    
    public void changeToEdit()
    {
            String link = "pages/appConfig/myaccount/editview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
    }
    
    public void changeToSave()
    {
            objUser  = userLogin.getSessionUser();
            getObjUser().setFirstName(firstName); 
            getObjUser().setLastName(lastName);
            getObjUser().setUserName(userName);
            getObjUser().setUserPassword(passWord);
            getObjUser().setDepartment(departMent);
            getObjUser().setDiscription(description);
            try
            {
                getUsersJpaController().edit(getObjUser());
            }
            catch(Exception e)
            {
                fc.print("Error not to update data"+e);
            }
            String link = "pages/appConfig/myaccount/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            
    }
    public List<roleDetail> getItemRoleUser() {
        if(itemRoleUser ==null)
        {
            itemRoleUser  = new ArrayList<roleDetail>();
        }
        return itemRoleUser;
    }

    public void setItemRoleUser(List<roleDetail> itemRoleUser) { 
        this.itemRoleUser = itemRoleUser;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Users getObjUser() {
        if(itemRoleUser ==null)
        {
            objUser  = new Users();
        }
        return objUser;
    }

    public void setObjUser(Users objUser) {
        this.objUser = objUser;
    }
    
    
    public List<RoleUser> getObjRoleUser() {
        if(objRoleUser == null)
        {
            objRoleUser  = new ArrayList<RoleUser>(); 
        }
        return objRoleUser;
    }

    public void setObjRoleUser(List<RoleUser> objRoleUser) {
        this.objRoleUser = objRoleUser;
    } 
    
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDepartMent() {
        return departMent;
    }

    public void setDepartMent(String departMent) {
        this.departMent = departMent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
}
