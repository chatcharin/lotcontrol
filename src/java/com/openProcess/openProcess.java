package com.openProcess;
import com.appCinfigpage.bean.fc;
import com.component.barcodeauto.BarcodeAuto; 
import com.component.barcodeauto.ProcList;
import com.tct.data.MainData;
import com.tct.data.Md2pc;
import com.tct.data.ProcessPool;
import com.tct.data.jpa.MainDataJpaController;
import com.tct.data.jpa.Md2pcJpaController;
import com.tct.data.jsf.util.JsfUtil;
import java.util.List;
import java.util.ResourceBundle;
import java.util.UUID;
import javax.persistence.Persistence;
import com.tct.data.CurrentProcess;
import com.tct.data.jpa.CurrentProcessJpaController; 
import com.time.timeCurrent;
import com.tct.data.PcsQty;
import com.tct.data.jpa.PcsQtyJpaController;
import com.tct.data.jpa.LotControlJpaController;
import com.tct.data.Mc;
import com.tct.data.jpa.McJpaController;
import java.util.ArrayList;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import com.shareData.ShareDataOfSession;
import com.tct.data.*;
import com.tct.data.jpa.LineJpaController;
import javax.faces.context.FacesContext;
public  class openProcess  {
	public String date; // Get value from file timeCurrent.java 
	private String  inputQty = "0" ;   
	private boolean openProcessPopup = false;
	private boolean openProcessPopupConfirm = false;
	private boolean openProcessPopupConfirmYes = false;
	private boolean openProcessPopupConfirmNo = false;
    
	private String typeClose;
	private String visibles; 
	
	public ProcessPool idProcess; 
        public int sqprocess;
        
        
        private Md2pcJpaController jpaController = null;
        private MainDataJpaController jpaController2 =null;
        private CurrentProcessJpaController jpaController3 =null;
        private  PcsQtyJpaController   jpaController4=null;
        private LotControlJpaController jpalotcontrol = null;
        private LineJpaController     lineJpaController =null;
        
        private PcsQty currentPcsQty;
        private MainData current;
        private CurrentProcess currentProcess;
        private int selectedItemIndex;
        public static String uuid;
        
        public static int mcUse =0 ;
        
        //===== Validate
        private String  validateInputQty = null ; 
        private String validateMc  = null; 
        private String shiftValidate = null;
        private List<SelectItem> lineSelect  =  null;
        private String lineUse = null;
        private String hr   = "0";
        private String validateHr  = null;
        private String validateSelectLine  = null;
        private boolean checkMcUse  = false;
        public ShareDataOfSession getShareData(){
          FacesContext context = FacesContext.getCurrentInstance();
          return   (ShareDataOfSession) context.getExternalContext().getSessionMap().get("sharedata");  
        }
        private LineJpaController getLineJpaController()
        {
            if(lineJpaController ==null)
            {
                lineJpaController  = new LineJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
            }
            return lineJpaController;
        }
        private Md2pcJpaController getJpaController() {
            if (jpaController == null) {
                jpaController = new Md2pcJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
            }
            return jpaController;
        }
         private MainDataJpaController getJpaController2() {
            if (jpaController2 == null) {
                jpaController2 = new MainDataJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
            }
            return jpaController2;
        }
        private CurrentProcessJpaController getJpaController3() {
            if (jpaController3 == null) {
                jpaController3 = new CurrentProcessJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
            }
            return jpaController3;
        }
         private PcsQtyJpaController getJpaController4() {
            if (jpaController4 == null) {
                jpaController4 = new PcsQtyJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
            }
            return jpaController4;
        }
        private LotControlJpaController getLotControlJpaController(){
             if (jpalotcontrol == null) {
                jpalotcontrol = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
            }
            return jpalotcontrol;
        } 
        McJpaController   mcController  = null;
        private McJpaController  getMcController(){
            if(mcController  == null)
            {
                mcController  =  new McJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
            }
            return mcController;
        }
        public BarcodeAuto getBarcodeAuto(){  
            FacesContext context = FacesContext.getCurrentInstance();
            return (BarcodeAuto) context.getExternalContext().getSessionMap().get("barcode"); 
        } 
	public void closePopup(String typeClose) {
		this.typeClose = typeClose; 
		// ================= Open Processs
		if (this.typeClose.equals("openProcessPopupStart")) {
			//==== Check validate inputQty
                    
                   double intHR =0;
                        boolean check  =false; 
                        try
                        { 
                            intHR  = Double.parseDouble(hr); 
                        }
                        catch(Exception e)
                        {
                            intHR =0;
                        }
                        if(!lineUse.equals("0") && check)
                        { 
                    
                        }
                            else
                            {
                                if(lineUse.equals("0"))
                                {
                                    validateSelectLine ="Select one";
                                }
                                if(!lineUse.equals("0"))
                                {
                                    validateSelectLine ="";
                                }    

                            }
                     //=================================
                    
                   int  inputWorksheet   = 0;
                    try{
                          inputWorksheet  = Integer.parseInt(inputQty);  
                      }catch(Exception e )
                      {
                          System.out.println("+++++++++++++ Not number +++++++++++++ : "+e);  
                          openProcessPopup  = true;
                          inputWorksheet    = 0;
                          validateInputQty  = "Invalid input"; 
                      }  
                    
                    boolean  checkValidateAll = false; 
                    //==== Check  validate mc# 
                    System.out.println(inttypeId);
                    if(checkMcUse){
                        if(mcNameUse.equals("0") || mcNameUse.equals("1000") || shifPart.equals("0") || inputWorksheet <= 0 ||inputWorksheet >Integer.parseInt(getBarcodeAuto().getCurrentMasterDefaultData().getProductQty())  || intHR <=0 || lineUse.equals("0"))
                        {
                                int  intMcNamesUse  = Integer.parseInt(mcNameUse);
                                switch(intMcNamesUse)
                                {
                                    case 0 : if(mcNameUse.equals("0"))
                                            {
                                                System.out.println("Test0");    
                                                validateMc      = "Select one"; 

                                            }
                                            else if(!mcNameUse.equals("0")){
                                                System.out.println("Test2");  
                                                validateMc      ="";
                                            }

                                    break;
                                    case 1000:
                                            if(mcNameUse.equals("1000"))
                                            {
                                                System.out.println("Test3");  
                                                validateMc      = "Machine is busy"; 

                                            }
                                            else if(!mcNameUse.equals("1000")){
                                                System.out.println("Test4");  
                                                validateMc      ="";
                                            }
                                    break;    
                                } 
                            if(shifPart.equals("0")){
                                shiftValidate   = "Select one";
                            }
                            else{
                                shiftValidate  ="";
                            }
                            if(inputWorksheet <= 0 ) 
                            {
                                fc.print("******** product qty **********:"+getBarcodeAuto().getCurrentMasterDefaultData().getProductQty());
                                    openProcessPopup = true;
                                    validateInputQty    = "Invalid input";
                            }
                            else{
                                
                                validateInputQty   = ""; 
                            }
                            if (inputWorksheet > Integer.parseInt(getBarcodeAuto().getCurrentMasterDefaultData().getProductQty())) {
                                    fc.print("******* inputwork > product qty *****");
                                    validateInputQty = "Input qty should be less than product qty";
                                    openProcessPopup = true;
                            } 
                            else{
                                
                                validateInputQty   = ""; 
                            }
                            if(intHR <=0)
                            {
                                validateHr  = "Invalid input";
                            }
                            else
                            {
                                validateHr  = "";
                            }
                            if(lineUse.equals("0"))
                            {
                                validateSelectLine  = "Select one";
                            }
                            else
                            {
                                validateSelectLine  = "";
                            }

                        }
                        else
                        {
                        //============
                           checkValidateAll  = true; 
                        }  
                         
                    }
                    else{  
                        if(shifPart.equals("0") || inputWorksheet <= 0 ||inputWorksheet > Integer.parseInt(getBarcodeAuto().getCurrentMasterDefaultData().getProductQty())|| intHR <=0 || lineUse.equals("0"))
                        {
                            if(shifPart.equals("0")){
                                shiftValidate   = "Select one";
                            }
                            else{
                                shiftValidate  ="";
                            }

                            if(inputWorksheet <= 0 ) 
                            {
                                System.out.println("++++++++++++  Invalid input +++++++++++++++++");
                                openProcessPopup = true;
                                validateInputQty    = "Invalid input";
                            //                            openProcessPopupConfirm = false; 
                            }
                            else{
                                validateInputQty   = ""; 
                            }
                            if (inputWorksheet > Integer.parseInt(getBarcodeAuto().getCurrentMasterDefaultData().getProductQty())) {
                                    fc.print("******* inputwork > product qty *****");
                                    validateInputQty = "Input qty should be less than "+getBarcodeAuto().getCurrentMasterDefaultData().getProductQty();
                                    openProcessPopup = true;
                            } 
                            else{
                                
                                validateInputQty   = ""; 
                            }
                            if(intHR <=0)
                            {
                                validateHr  = "Invalid input";
                            }
                            else
                            {
                                validateHr  = "";
                            }
                            if(lineUse.equals("0"))
                            {
                                validateSelectLine  = "Select one";
                            }
                            else
                            {
                                validateSelectLine  = "";
                            }
                            
                        }else
                        {
                            checkValidateAll  = true; 
                        }    
                    }
                    //===================
                    if(checkValidateAll)
                    {
                            openProcessPopup = false;
                            openProcessPopupConfirm = true; 
                            validateMc      ="";
                            shiftValidate  ="";
                            validateInputQty  = ""; 
                            validateSelectLine  ="";
                            validateHr          ="";
                    
                    } 
                      
		} else if (this.typeClose.equals("openProcessPopupClose")) {
			openProcessPopup = false;
                        setInputQty("0");
                        setMcNameUse("0");
                        setShifPart("0");  
                        validateMc      ="";
                        validateInputQty   = ""; 
                        shiftValidate  ="";
                        lineUse        = "0";
                        hr             =  "0";
                        validateSelectLine  ="";
                        validateHr          ="";
                        
		} else if (this.typeClose.equals("openProcessPopupConfirmYes")) { 
                            ShareDataOfSession  objShare  = getShareData(); 
                            openProcessPopupConfirm = false;
                            insertData();
                            getShareData().openProcessButton = true;
                            getBarcodeAuto().openProcessButtonShow = "open_process_button_disable";
                            getBarcodeAuto().status   = "Open"; 
                            setInputQty("0");
                            setMcNameUse("0");
                            setShifPart("0"); 
                            setLineUse("0"); 
                            hr      =  "0";
                        
		} else if (this.typeClose.equals("openProcessPopupConfirmNo")) {
			openProcessPopupConfirm = false;
                        setInputQty("0");
                        setMcNameUse("0");
                        setShifPart("0");     
			//openProcessPopupConfirmNo = true;
		}
	}
         public void insertData()
        {  
             ShareDataOfSession  objShare  = getShareData(); 
             uuid = UUID.randomUUID().toString(); //----random  uuid    
            //================= Insert Main data 
             
            String uuidCurrent = UUID.randomUUID().toString(); 
            getCurrent().setId(uuid); 
            getCurrent().setIdProc(idProcess);  
            getCurrent().setLotControlId(objShare.lotControlId);
            getCurrent().setLotNumber(objShare.lotNumber); 
            getCurrent().setCurrentNumber(uuidCurrent); 
            create();   
            
            List<CurrentProcess> objListOne  = getJpaController3().findCurrentStatus(getCurrent());
            
            
            objShare.lotControlId.setMainDataId(uuid); 
            System.out.println("============== md2pcId :"+ objShare.md2pcId); 
            objShare.lotControlId.setMd2pcIdModel(objShare.md2pcId); 
            try {
                getLotControlJpaController().edit(objShare.lotControlId);
                JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("DetailAssyUpdated"));
                //return "View";
            } catch (Exception e) {
                JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                //return null;
            }  
            
            //================= Insert  Current process
            fc.print("======== sq process ========="+getBarcodeAuto().sqProcess);
            
            getCurrentProcess().setId(uuidCurrent);   
            getCurrentProcess().setMainDataId(current); 
            getCurrentProcess().setCurrentProc("1");
            getCurrentProcess().setStatus("Open");
            
            if(objListOne.isEmpty())
            {
               getCurrentProcess().setFirstOpen(1);
            } 
            else
            {
               getCurrentProcess().setFirstOpen(0);
            }
            
            if(checkMcUse){  
                Mc dataMcGoodId   = getMcController().findMc(mcNameUse); 
                getCurrentProcess().setMcId(dataMcGoodId);
            }
            else
            {
                getCurrentProcess().setMcId(null);
            }
            getCurrentProcess().setShift(shifPart);
            timeCurrent  timeNew  =  new  timeCurrent();
            getCurrentProcess().setTimeCurrent(timeNew.getDate());
            getCurrentProcess().setLine(lineUse);
            getCurrentProcess().setHr(hr); 
            createCurrentProcess(); 
            
            if(checkMcUse){ 
            //===========Update maindata current number
            CurrentProcess idMcCurrent  = getJpaController3().findCurrentProcess(getCurrentProcess().getId());
            System.out.println("======= ID mc  ====="+idMcCurrent.getMcId()); 
            idMcCurrent.getMainDataId().setCurrentNumber(uuidCurrent);
            idMcCurrent.getMcId().setMcStatus("use");
              try {
                System.out.println("===== Change mc status to << Use >> =====");  
                getJpaController2().edit(idMcCurrent.getMainDataId()); 
            } catch (Exception e) {
                System.out.println("======== Eror====="+e); 
            } 
             
            //===========Update  status  mc  : Use ===========
           
                idMcCurrent.getMcId().setMcStatus("use");
                try {
                    System.out.println("===== Change mc status to << Use >> =====");  
                    getMcController().edit(idMcCurrent.getMcId()); 
                } catch (Exception e) {
                    System.out.println("======== Eror====="+e); 
                } 
            }
            //================= Insert pcsqty  
            String uuidPcsQty = UUID.randomUUID().toString();
            getPcsQty().setId(uuidPcsQty);
            getPcsQty().setCurrentProcessId(currentProcess); 
            getPcsQty().setQty(inputQty);
            getPcsQty().setQtyType("in");   
            getPcsQty().setWipQty("0");  
            createPcsQty();
            
            System.out.println("======================= Insert current sucess ==================");
            
            //============= update process status
            
            
            for (ProcList objProList : getBarcodeAuto().processListData)
            {
                 if(objProList.getSqpProcNum() == sqprocess)
                 {
                     objProList.setSqpProcNum(sqprocess);
                     objProList.setShift(getCurrentProcess().getShift());
                     try{
                        objProList.setMcName(getCurrentProcess().getMcId().getMcName());
                     }
                     catch(Exception e)
                     {
                        objProList.setMcName(null);
                     }  
                     objProList.setStartTime(getCurrentProcess().getTimeCurrent().toString());
                     objProList.setIndex(currentProcess);
                     objProList.setInputQty(inputQty);
                     objProList.setNgQty("");
                     objProList.setNgRepair("");
                     objProList.setOuPutQty("");
                     objProList.setCssCurrentStatusProcess("process_td_now");
                     objProList.setPercentNgNormal("0.00 %");
                     objProList.setPercentNgRepair("0.00 %");
                     break;
                 }
            
            }
            prepareCreate();
            prepareCreateCurrentProcess();
        }  
         public String prepareCreate() {
            current = new MainData();
            selectedItemIndex = -1;
            return "Create";
        }
        
        public void create() {
            try {
                getJpaController2().create(current);
                JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("MainDataCreated"));
//                return prepareCreate();
                } catch (Exception e) {
                    JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
//                    return null;
                }
        } 
        public MainData getCurrent() {
            if(current == null){
                current = new MainData();
            }
            return current;
        }
        
        //-------------insert current process 
         public String prepareCreateCurrentProcess() {
            currentProcess = new CurrentProcess();
            selectedItemIndex = -1;
            return "Create";
        }
        
        public void  createCurrentProcess() {
            try {
                getJpaController3().create(currentProcess);
                JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("MainDataCreated"));
               // return prepareCreateCurrentProcess();
                } catch (Exception e) {
                    JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                    //return null;
                }
        } 
        public CurrentProcess getCurrentProcess() {
            if(currentProcess == null){
                currentProcess = new CurrentProcess();
            }
            return currentProcess;
        }
        
        //---------------------------------------------------
         public String prepareCreatePcsQty() {
            currentPcsQty = new PcsQty();
            selectedItemIndex = -1;
            return "Create";
        }
        
        public String createPcsQty() {
            try {
                getJpaController4().create(currentPcsQty);
                JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("MainDataCreated"));
                return prepareCreatePcsQty();
                } catch (Exception e) {
                    JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                    return null;
                }
        } 
        public PcsQty getPcsQty() {
            if(currentPcsQty == null){
                currentPcsQty = new PcsQty();
            }
            return currentPcsQty;
        } 
        //---------------------------------------------------
        private boolean displayMC  = false;
        public  boolean getDisplayMC(){
            return displayMC;
            
        }
        public  void  setDisplayMC(boolean displayMC){
            this.displayMC  = displayMC;
        
        }
	public void openPopupType(String visibles) { 
		this.visibles = visibles;  
		if(this.visibles.equals("Open"))
                {
                     ShareDataOfSession  objShare  = getShareData();
                     if(!objShare.openProcessButton){ 
                        openProcessPopup   = true;
                        findInputQty();
                        findListMC(); 
                        findListShitfPart();  
                        findNameProcess();
                        findLineAll();
                      }
                } 
	}
        private List<SelectItem>  itemLine  ;
        public List<SelectItem> getItemLine() {
            if(itemLine == null)
            {
                itemLine  = new ArrayList<SelectItem>();
            }
            return itemLine;
        }
        public void setItemLine(List<SelectItem> itemLine) {
            this.itemLine = itemLine;
        } 
        private void  findLineAll()
        {
            itemLine  = new ArrayList<SelectItem>();
            List<Line> objLine  = getLineJpaController().findAllLineNotDeletd();
            getItemLine().add(new SelectItem("0","---- Select one ---"));
            for(Line objList:objLine)
            {
                getItemLine().add(new SelectItem(objList.getLineId(),objList.getLineName()));
            }
        }
        private void  findInputQty()
        {
            inputQty = getBarcodeAuto().getInputQty();  //***** Get barcode qty.
        }
        //---findListMC  is  good.
        private boolean displayMcGoodList  = false;
        private List<SelectItem> selectMcGood = null;
        private List<SelectItem> selectShit   = null;
        public  List<SelectItem>  getSelectShift(){
            return selectShit;
        }
        public void  setSelectShift(List<SelectItem> selectShit){
            this.selectShit  =  selectShit;
        }
        public  boolean getDisplayMcGoodList(){
            return displayMcGoodList;
        } 
        public void setDisplayMcGoodList(boolean displayMcGoodList){
            this.displayMcGoodList  = displayMcGoodList;
        }
        
        public static  String mcNameUse = "";
        private String shifPart =  "";
        public  String getMcNameUse(){
        
            return mcNameUse;
        }
        public  void setMcNameUse(String mcNameUse){
            this.mcNameUse  = mcNameUse;
        } 
        //==========================shift======================
        public String getShifPart(){
            return shifPart;
        
        }
        public void setShifPart(String shifPart){
            this.shifPart  = shifPart;
        }
        public void ChangShiftValue(ValueChangeEvent event) {
           // System.out.println("================== Change Shift value=======================");
            shifPart = (String) event.getNewValue();   
            if(shifPart.equals("0"))
            {
                shiftValidate  = "Select one";
            }
            else
            {
                shiftValidate  = "";
            }
//            PrintOut(shifPart);
        }  
        public void findListShitfPart(){
             selectShit = new ArrayList<SelectItem>();
             selectShit.add(new SelectItem("0","------ Select One -------"));
             selectShit.add(new SelectItem("A","A"));
             selectShit.add(new SelectItem("B","B"));
             selectShit.add(new SelectItem("Day","Day"));
             selectShit.add(new SelectItem("Night","Night"));
        }
        //============================ mc ========================
        public void ChangNameValue(ValueChangeEvent event) {
            //System.out.println("================== Change mc =======================");
            mcNameUse = (String) event.getNewValue();  
             if(mcNameUse.equals("0"))
             {
                 validateMc  = "Select one";
             }
             else
             {
                 validateMc  = "";
             }
//            PrintOut(mcNameUse);
        }
        private void PrintOut(String str) {
            System.out.println(str.toUpperCase() + " : " + str);
        }
        public  List<SelectItem>  getSelectMcGood(){
            return selectMcGood;
        }
        public void  setSelectMcGood(List<SelectItem> selectMcGood ){
            this.selectMcGood   = selectMcGood;
        } 
        private int inttypeId  ;
        public void findListMC()
        {  
            ShareDataOfSession  objShare  = getShareData(); 
            System.out.println("=============== Insert Id Process       : "+getBarcodeAuto().idProc); 
            if(getShareData().idProc == null)
            {
                Md2pc obj  = getJpaController().findByMd2pcId(getBarcodeAuto().md2pcId);
                idProcess = obj.getProcessPoolIdProc();
                sqprocess = obj.getSqProcess();
            }
            else
            {
                idProcess = getBarcodeAuto().idProc;  
                sqprocess =  Integer.parseInt(getBarcodeAuto().sqProcess);
            } 
                System.out.println("sqprocess "+sqprocess +"mc use : "+idProcess.getMcUse());
                if(idProcess.getMcUse()==1)
                {
                    setInttypeId(1000);
                    System.out.println("Use mc");
                    checkMcUse  = true; 
                    String  mcGStatusGood  ="good";
                    List<Mc>   lisAlltMcGood  =  getMcController().findListMCGood(mcGStatusGood); 
                    if(lisAlltMcGood.isEmpty()){  //no have  mc ids good.
                        System.out.println("================= mc all  is  not  good. ====================");
                        setDisplayMcGoodList(true);
                        selectMcGood = new ArrayList<SelectItem>();
                        selectMcGood.add(new SelectItem("1000","------ Select One -------"));
                        validateMc  = "Machine is busy";
                    }
                    else{
                        selectMcGood = new ArrayList<SelectItem>();
                        selectMcGood.add(new SelectItem("0","------ Select One -------"));
                        for(Mc displayListOne : lisAlltMcGood){
                            System.out.println("id  mc : "+displayListOne.getId()+" Mc name :  "+displayListOne.getMcName());
                            setDisplayMcGoodList(true);
                            selectMcGood.add(new SelectItem(displayListOne.getId(),displayListOne.getMcName()));  
                        }
                    } 
                }
                else
                {
                    System.out.println("Not use mc");
                    checkMcUse  = false; 
                    setInttypeId(0);
                    
                }
        } 
        
        //--------------------------------------------------------- 

	public boolean isOpenProcessPopup() {
		return openProcessPopup;
	}

	public boolean isOpenProcessPopupConfirm() {
		return openProcessPopupConfirm;
	}

	public boolean isOpenProcessPopupConfirmYes() {
		return openProcessPopupConfirmYes;
	}

	public boolean isOpenProcessPopupConfirmNo() {
		return openProcessPopupConfirmNo;
	}

	/**
	 * @return the inputQty
	 */
	public String  getInputQty() {
		return inputQty;
	}

	/**
	 * @param inputQty the inputQty to set
	 */
	public void setInputQty(String inputQty) {	 
		this.inputQty = inputQty;
	}  
        
        
        //======= Invalid  inputQty
        public String getValidateInputQty(){
            return validateInputQty;
        }
        public void setValidateInputQty(String validateInputQty)
        {
            this.validateInputQty  = validateInputQty;
        }
        
        //======= Invalide mc validate
//        private String validateMc  = null;
        public String getValidateMc()
        {
            return validateMc;
        }
        public void setValidateMc(String validateMc)
        {
            this.validateMc  = validateMc;
        }
        
        
        //======= Invalide shitf  validate
//        private String shiftValidate = null;
        public String getShiftValidate()
        {
            return shiftValidate;
        }
        public void setShiftValidate(String shiftValidate)
        {
            this.shiftValidate  = shiftValidate;
        }
        public void findNameProcess()
        { 
             ShareDataOfSession  objShare  = getShareData(); 
             System.out.println("=========="+objShare.md2pcId);
             Md2pc   objMd2pc  = getJpaController().findByMd2pcId(objShare.md2pcId);
             System.out.println("=== name process "+objMd2pc.getProcessPoolIdProc().getProcName());
             getBarcodeAuto().nameProcess =  objMd2pc.getProcessPoolIdProc().getProcName();
        }  
        public void findLine()
        {
            lineSelect   = new ArrayList<SelectItem>();
            lineSelect.add(new SelectItem("0","------- Select one ------"));
            lineSelect.add(new SelectItem("1"));
            lineSelect.add(new SelectItem("2"));
            lineSelect.add(new SelectItem("3"));
            lineSelect.add(new SelectItem("4"));
            lineSelect.add(new SelectItem("5"));
        }
   public void ChangLineNameValue(ValueChangeEvent event) {
            //System.out.println("================== Change mc =======================");
            lineUse = (String) event.getNewValue();  
            if(lineUse.equals("0"))
            {
                validateSelectLine = "Select one";
            }
            else
            {
                validateSelectLine  = "";
            }
            
//            PrintOut(mcNameUse);
   }
    /**
     * @return the lineSelect
     */
    public List<SelectItem> getLineSelect() {
        return lineSelect;
    }

    /**
     * @param lineSelect the lineSelect to set
     */
    public void setLineSelect(List<SelectItem> lineSelect) {
        this.lineSelect = lineSelect;
    }

    /**
     * @return the lineUse
     */
    public String getLineUse() {
        return lineUse;
    }

    /**
     * @param lineUse the lineUse to set
     */
    public void setLineUse(String lineUse) {
        this.lineUse = lineUse;
    }

    /**
     * @return the hr
     */
    public String getHr() {
        return hr;
    }

    /**
     * @param hr the hr to set
     */
    public void setHr(String hr) {
        this.hr = hr;
    }

    /**
     * @return the validateHr
     */
    public String getValidateHr() {
        return validateHr;
    }

    /**
     * @param validateHr the validateHr to set
     */
    public void setValidateHr(String validateHr) {
        this.validateHr = validateHr;
    }

    /**
     * @return the validateSelectLine
     */
    public String getValidateSelectLine() {
        return validateSelectLine;
    }

    /**
     * @param validateSelectLine the validateSelectLine to set
     */
    public void setValidateSelectLine(String validateSelectLine) {
        this.validateSelectLine = validateSelectLine;
    }

    /**
     * @return the checkMcUse
     */
    public boolean getCheckMcUse() {
        return checkMcUse;
    }

    /**
     * @param checkMcUse the checkMcUse to set
     */
    public void setCheckMcUse(boolean checkMcUse) {
        this.checkMcUse = checkMcUse;
    }

    /**
     * @return the inttypeId
     */
    public int getInttypeId() {
        return inttypeId;
    }

    /**
     * @param inttypeId the inttypeId to set
     */
    public void setInttypeId(int inttypeId) {
        this.inttypeId = inttypeId;
    }
}