/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sql.bean;

import com.appCinfigpage.bean.fc;
import com.mysql.jdbc.Connection;
import com.tct.dashboard.ReportBean;
import groovyjarjarcommonscli.ParseException;
import java.sql.DriverManager;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * Machine User
 *
 * @author pang Development by Chusak laojang Date : Oct 21, 2012, Time :
 * 11:23:05 AM Copy Right 4 Xtreme Co.,Ltd.
 */
public class Sql {

    public static Date strDate(String str_date) throws java.text.ParseException {
        try {
//            String str_date = "11-June-07";
            DateFormat formatter;
            Date dated;
            formatter = new SimpleDateFormat("yyyy/MM/dd");
            dated = (Date) formatter.parse(str_date);
            System.out.println("Today is " + dated);
            return dated;
        } catch (Exception e) {
            System.out.println("Exception :" + e);
            return null;
        }
    }

    public static String formatDateSqlForNg(Date inputDate) {
        return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(inputDate);
    }

    public static String formatDateSqlForNgDetails2(Date inputDate) {
        return new SimpleDateFormat("dyyyy/MM/dd").format(inputDate);
    }

    public static String formatDateSqlForNgDetails(Date inputDate) {
        return new SimpleDateFormat("dd/MM/yyyy").format(inputDate);
    }

    public static String generateSqlForNg(String start, String end, String line) {
        String sql = "";
        sql += "SELECT ";
        sql += "sum( pcs_qty.qty) AS 'psc', ";
        sql += "ng_normal.name, ";
        sql += "sum( ng_normal.qty) AS 'ng', ";
        sql += "(( sum( ng_normal.qty)/ sum( pcs_qty.qty))*100) AS percent, ";
        sql += "main.time_current, ";
        sql += "current_process.time_current, ";
        sql += "line.line_name ";
        sql += "FROM ";
        sql += "current_process main INNER JOIN pcs_qty ON main.id = pcs_qty.current_process_id ";
        sql += "INNER JOIN main_data ON main.main_data_id = main_data.id ";
        sql += "INNER JOIN current_process ON main_data.id = current_process.main_data_id ";
        sql += "INNER JOIN ng_normal ON current_process.id = ng_normal.current_process_id ";
        sql += "INNER JOIN line ON current_process.line = line.line_id ";
        sql += "WHERE  current_process.time_current >= ";
        sql += "'" + start + "' and  current_process.time_current <= '" + end + "' ";
        sql += "and   current_process.line='" + line + "' ";
        sql += "and pcs_qty.qty_type = 'in' ";
        sql += "GROUP BY ";
        sql += "ng_normal.name ";
        sql += "ORDER BY ";
        sql += "ng_normal.name ASC ";
        return sql;
    }

    public static String generateSqlForNgRepair(String start, String end, String line) {
        String sql = "";
        sql += "SELECT ";
        sql += "sum( pcs_qty.qty) AS 'psc', ";
        sql += "ng_repair.name, ";
        sql += "sum( ng_repair.qty) AS 'ng', ";
        sql += "(( sum( ng_repair.qty)/ sum( pcs_qty.qty))*100) AS percent, ";
        sql += "main.time_current, ";
        sql += "current_process.time_current, ";
        sql += "line.line_name ";
        sql += "FROM ";
        sql += "current_process main INNER JOIN pcs_qty ON main.id = pcs_qty.current_process_id ";
        sql += "INNER JOIN main_data ON main.main_data_id = main_data.id ";
        sql += "INNER JOIN current_process ON main_data.id = current_process.main_data_id ";
        sql += "INNER JOIN ng_repair ON current_process.id = ng_repair.current_process_id ";
        sql += "INNER JOIN line ON current_process.line = line.line_id ";
        sql += "WHERE  current_process.time_current >= ";
        sql += "'" + start + "' and  current_process.time_current <= '" + end + "' ";
        sql += "and   current_process.line='" + line + "' ";
        sql += "and pcs_qty.qty_type = 'in' ";
        sql += "GROUP BY ";
        sql += "ng_repair.name ";
        sql += "ORDER BY ";
        sql += "ng_repair.name ASC ";
        return sql;
    }
    // This function check model to use.

    public static String generateSqlForModelUse() {
        String sql = "";
        sql += "SELECT ";
        sql += "model_pool.`id` AS model_pool_id, ";
        sql += "model_pool.`model_name` AS model_pool_model_name ";
        sql += "FROM ";
        sql += "`lot_control` lot_control INNER JOIN `main_data` main_data ON lot_control.`id` = main_data.`lot_control_id` ";
        sql += "INNER JOIN `model_pool` model_pool ON lot_control.`model_pool_id` = model_pool.`id` ";
        sql += "WHERE model_pool.`deleted` = 0 ";
        sql += "GROUP BY ";
        sql += "model_pool_id;";
        return sql;
    }

    public static String generateSqlCheckDetails() {
        String sql = "";
        sql += "SELECT ";
        sql += "lot_control.`details_id_detail` AS lot_control_details_id_detail ";

        sql += "FROM ";
        sql += "`lot_control` lot_control INNER JOIN `main_data` main_data ON lot_control.`id` = main_data.`lot_control_id` ";
        sql += "INNER JOIN `model_pool` model_pool ON lot_control.`model_pool_id` = model_pool.`id` ";
        sql += " INNER JOIN `details` details ON lot_control.`details_id_detail` = details.`id_detail` ";
        return sql;
    }

    public static String generateSqlStdTime(String fillter) {
        String sql = "";
        sql += "SELECT ";
        sql += "current_process.`id` AS current_process_id, ";
        sql += "current_process.`time_current` AS current_process_time_current, ";
        sql += "line.`line_name` AS line_line_name, ";
        sql += "process_pool.`proc_name` AS process_pool_proc_name, ";
        sql += "model_pool.`model_series` AS model_pool_model_series, ";
        sql += "model_pool.`model_name` AS model_pool_model_name, ";
        sql += "pcs_qty.`qty` AS pcs_qty_qty ";
        sql += "FROM ";
        sql += "`current_process` current_process INNER JOIN `line` line ON current_process.`line` = line.`line_id` ";
        sql += "INNER JOIN `main_data` main_data ON current_process.`main_data_id` = main_data.`id` ";
        sql += "INNER JOIN `pcs_qty` pcs_qty ON current_process.`id` = pcs_qty.`current_process_id` ";
        sql += "INNER JOIN `process_pool` process_pool ON main_data.`id_proc` = process_pool.`id_proc` ";
        sql += "INNER JOIN `lot_control` lot_control ON main_data.`lot_control_id` = lot_control.`id` ";
        sql += "INNER JOIN `model_pool` model_pool ON lot_control.`model_pool_id` = model_pool.`id` ";
        sql += "WHERE ";
        sql += "current_process.`status` = 'Finished' ";
        sql += fillter;
        sql += " ;";
        return sql;
    }

    public static String generateSqlForInsetNeWSettingRecord() {
        String sql = "";
        sql += "SELECT  *  FROM std_data_show_input  group by std_data_show_input.id_show";
        return sql;
    }

    public static String generateOeeReportChart(String fillter) {
        String sql = "";
        sql += "SELECT"
                + "     current1.time_current AS datetimes,"// index : 1
                + "     pcs_qty.qty_type,"// index : 2
                + "     pcs_qty.qty,"// index : 3
                + "     sum( lot_control.qty_output) AS goodpiece,"// index : 4
                + "     sum( if( pcs_qty.qty_type= 'in', pcs_qty.qty,0)) AS totalpiece,"// index : 5
                + "     mc.mc_name AS mcname,"// index : 6
                + "     mc.mc_code AS mccode,"// index : 7
                + "     mc.plan_production_time AS planproductiontime,"// index : 8
                + "     mc.product_rate AS productrate,"// index : 9
                + "     line.line_name AS linename,"// index : 10
                + "     ( mc.plan_production_time*60*60)-( SUM( current3.time_current- current4.time_current)*60/1000) AS operationtime,"// index : 11
                + "     current3.time_current AS timecurrent3,"// index : 12
                + "     current4.time_current AS timecurrent4,"// index : 13
                + "     lot_control.barcode,"// index : 14
                + "     (( mc.plan_production_time*60*60)-( SUM( current3.time_current- current4.time_current)*60/1000))/( ( mc.plan_production_time*60*60))*100 AS avability1,"// index : 15
                + "     ( sum( if( pcs_qty.qty_type= 'in', pcs_qty.qty,0)))/ mc.product_rate*100 AS performance,"// index : 16
                + "     1-( sum( lot_control.qty_output))/ sum( if( pcs_qty.qty_type= 'in', pcs_qty.qty,0)) AS qulity,"// index : 17
                + "     ((((( mc.plan_production_time*60*60)-(SUM( current3.time_current- current4.time_current)*60/1000))/(mc.plan_production_time*60*60)))*((1-( sum( lot_control.qty_output))/ sum( if( pcs_qty.qty_type= 'in', pcs_qty.qty,0)))/100)*((( sum( if( pcs_qty.qty_type= 'in', pcs_qty.qty,0)))/ mc.product_rate*100)/100))*100 AS percenoee,"// index : 18
                + "     mcdown.mc_id,"// index : 19
                + "     count(mcdown.mc_id) AS frequency,"// index : 20
                + "      SUM(current3.time_current- current4.time_current) AS totaldowntime,"// index : 21
                + "     ((mc.plan_production_time*60*60)-( SUM(current3.time_current- current4.time_current)))/count(mcdown.mc_id) AS mtbf,"// index : 22
                + "     ( SUM(current3.time_current- current4.time_current))/count(mcdown.mc_id) AS mttr,"// index : 23
                + "     (((mc.plan_production_time*60*60)-( SUM(current3.time_current- current4.time_current)))/count(mcdown.mc_id)/(((mc.plan_production_time*60*60)-( SUM(current3.time_current- current4.time_current)))/count(mcdown.mc_id)+( SUM(current3.time_current- current4.time_current))/count(mcdown.mc_id)))*100 AS avability2,"// index : 24
                + "     process_pool.proc_name AS processname"// index : 25
                + " FROM"
                + "     current_process current1 INNER JOIN mc_down mcdown ON current1.id = mcdown.current_process_id"
                + "     INNER JOIN mc mc ON mcdown.mc_id = mc.id"
                + "     INNER JOIN current_process current3 ON mcdown.current_process_id = current3.id"
                + "     INNER JOIN current_process current4 ON mcdown.ref_downTimeStart = current4.id"
                + "     INNER JOIN line line ON current1.line = line.line_id"
                + "     INNER JOIN pcs_qty ON current1.id = pcs_qty.current_process_id"
                + "     INNER JOIN main_data ON current1.main_data_id = main_data.id"
                + "     INNER JOIN lot_control ON main_data.lot_control_id = lot_control.id"
                + "     INNER JOIN process_pool ON main_data.id_proc = process_pool.id_proc WHERE ";
        sql += fillter;
        sql += "  GROUP BY"
                + "     mcdown.mc_id"
                + " ORDER BY"
                + "     current1.time_current ASC";
        return sql;
    }

    public static String generateSqlForMcDowntime(String fillter) {
        String sql = "";
        sql += " SELECT ";
        sql += "line.`line_name` AS line_line_name,";
        sql += "current_process.`time_current` AS current_process_time_current,";
        sql += "current_process.`status` AS current_process_status,";
        sql += "mc_down.`reson` AS mc_down_reson,";
        sql += "process_pool.`proc_name` AS process_pool_proc_name,";
        sql += "mc.`mc_name` AS mc_mc_name,";
        sql += "mc_down_A.`reson` AS mc_down_A_reson,";
        sql += "current_process_A.`time_current` AS current_process_A_time_current,";
        sql += "current_process_A.`status` AS current_process_A_status ,";
        sql += "(current_process.`time_current`-current_process_A.`time_current`)*60/1000 AS total_downtime ,";
        sql += "mc.plan_production_time as plan ,";
        sql += "(mc.plan_production_time*60*60)-((current_process.`time_current`-current_process_A.`time_current`)*60/1000 ) as operationtime ,";
        sql += "(mc.plan_production_time)+((current_process.`time_current`-current_process_A.`time_current`)*60/1000)+((mc.plan_production_time*60*60)-((current_process.`time_current`-current_process_A.`time_current`)*60/1000 )) as totalall ";
        sql += " FROM ";
        sql += "`current_process` current_process INNER JOIN `mc_down` mc_down ON current_process.`id` = mc_down.`current_process_id` ";
        sql += "INNER JOIN `main_data` main_data ON current_process.`main_data_id` = main_data.`id` ";
        sql += "INNER JOIN `line` line ON current_process.`line` = line.`line_id` ";
        sql += "INNER JOIN `process_pool` process_pool ON main_data.`id_proc` = process_pool.`id_proc` ";
        sql += "INNER JOIN `mc` mc ON mc_down.`mc_id` = mc.`id` ";
        sql += "INNER JOIN `current_process` current_process_A ON mc_down.`ref_downTimeStart` = current_process_A.`id` ";
        sql += "INNER JOIN `mc_down` mc_down_A ON current_process_A.`id` = mc_down_A.`current_process_id` ";
        sql += " WHERE current_process.`status`='Open' ";
        sql += fillter;
        sql += " order by current_process.`time_current` asc ";
        return sql;
    }

    public static String generateHistoryDowntime(String fillter) {
        String sql = "";
        sql += " SELECT ";
        sql += " current_process.`time_current` as timeDate, ";
        sql += " if(current_process_A.`time_current` <>'',current_process_A.`time_current`,if(current_process_C.`time_current` <>'',current_process_C.`time_current`,if(current_process_B.`time_current` <>'',current_process_B.`time_current`,0))) AS currentAll, ";
        sql += " (current_process.`time_current`-if(current_process_A.`time_current` <>'',current_process_A.`time_current`,if(current_process_C.`time_current` <>'',current_process_C.`time_current`,if(current_process_B.`time_current` <>'',current_process_B.`time_current`,0))))*60 /1000 as caltime ,"
                + "if(current_process_C.`status` <>'',current_process_C.`status`,if(current_process_A.`status` <>'',current_process_A.`status`,if(current_process_B.`status` <>'',current_process_B.`status`,0))) AS statusAll, ";
        sql += " process_pool.`proc_name` AS process_pool_proc_name, ";
        sql += " if(mc_down.`reson`<>'',mc_down.`reson`,'-') AS mc_down_reson, ";
        sql += " current_process.`time_current` AS current_process_time_current, ";
        sql += " line.`line_name` AS line_line_name ";
        sql += " FROM ";
        sql += " `main_data` main_data INNER JOIN `current_process` current_process ON main_data.`id` = current_process.`main_data_id` ";
        sql += " INNER JOIN `process_pool` process_pool ON main_data.`id_proc` = process_pool.`id_proc` ";
        sql += " LEFT OUTER JOIN `mc_down` mc_down ON current_process.`id` = mc_down.`current_process_id` ";
        sql += " LEFT OUTER JOIN `hold` hold ON current_process.`id` = hold.`current_process_id` ";
        sql += " LEFT OUTER JOIN `mc_move` mc_move ON current_process.`id` = mc_move.`current_process_id` ";
        sql += " LEFT OUTER JOIN `line` line ON current_process.`line` = line.`line_id` ";
        sql += " LEFT OUTER JOIN `current_process` current_process_C ON mc_move.`ref_moveIdCurrent` = current_process_C.`id` ";
        sql += " LEFT OUTER JOIN `current_process` current_process_B ON hold.`ref_idHoldStart` = current_process_B.`id` ";
        sql += " LEFT OUTER JOIN `current_process` current_process_A ON mc_down.`ref_downTimeStart` = current_process_A.`id` ";
        sql += " WHERE ";
        sql += " current_process.`status` <> 'Finished' ";
        sql += " and (current_process_A.`time_current` <> '' ";
        sql += " or current_process_C.`time_current` <> '' ";
        sql += " or current_process_B.`time_current` <> '') ";
        sql += fillter;
        sql += " order by   current_process.`time_current`  asc; ";
        return sql;
    }

    public static Connection getConnection(String brand) {
        fc.print(brand);
        if (brand.equals("mysql")) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                return (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tct_project", "root", "");
            } catch (Exception ex) {
                Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        } else {
            try {
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                return (Connection) DriverManager.getConnection("jdbc:sqlserver://localhost\\SQLEXPRESS:1433;databaseName=tct_project", "tctproject", "F9gdup8vp");
            } catch (Exception ex) {
                Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }

    }
}
