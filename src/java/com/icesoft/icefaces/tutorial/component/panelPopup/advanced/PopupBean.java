package com.icesoft.icefaces.tutorial.component.panelPopup.advanced;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
/**
 * Class used to allow the dynamic opening and closing of panelPopups
 * That means the visibility status is tracked, as well as supporting
 *  methods for button clicks on the page
 */ 
@ApplicationScoped
@ManagedBean(name="popup")

public class PopupBean
{
    //Grop  open process popup. 
    private boolean  openProcessPopup            = false;
    private boolean  openProcessPopupConfirm     = false; 
    private boolean  openProcessPopupConfirmYes  = false;
    private boolean  openProcessPopupConfirmNo   = false;  
    
    //Grop  close process popup.
    private boolean  closeProcessPopup           = false;
    private boolean  closeProcessConfirm         = false;
    private boolean  closeProcessConfirmYes      = false;
    private boolean  closeProcessConfirmNo  	 = false;
/*    private boolean  x2_24  = false; */
    private boolean  inputNG   					 = false;
    private boolean  inputNGRepair               = false;
    
    //Grop downtime process popup.
    private boolean  downtimeProcessPopup            = false;
    private boolean  downtimeProcessPopupConfirm     = false; 
    private boolean  downtimeProcessPopupConfirmYes  = false;
    private boolean  downtimeProcessPopupConfirmNo   = false;  
    
     
    //Grop hold process popup.
    private boolean  holdProcessPopup            = false;
    
    //Grop stopDowntime
    private boolean  stopDowntimeProcessPopup    = false;
    
    //Grop move M/C
    private boolean  moveProcessPopup            = false;
    private boolean  moveProcessPopupConfirm     = false; 
    private boolean  moveProcessPopupConfirmYes  = false;
    private boolean  moveProcessPopupConfirmNo   = false;
    
    private String  typeClose;
    private String  visibles;   
    
   public void closePopup(String typeClose) {
	   this.typeClose = typeClose;
	   
	   //--------------------open processs
	   if(this.typeClose.equals("openProcessPopupStart"))
	   {
		   openProcessPopup        = false;
		   openProcessPopupConfirm = true;
	   }
	   else if(this.typeClose.equals("openProcessPopupClose"))
	   {
		   openProcessPopup  = false;
	   } 
	   else if(this.typeClose.equals("openProcessPopupConfirmYes"))
	   {	
		   openProcessPopupConfirm    = false;
		   openProcessPopupConfirmYes = true;
	   }
	   else if(this.typeClose.equals("openProcessPopupConfirmNo"))
	   {	
		   openProcessPopupConfirm   = false; 
		   openProcessPopupConfirmNo = true;
	   }
	   else if(this.typeClose.equals("openProcessPopupConfirmYesClose"))
	   {
		   openProcessPopupConfirmYes = false;
	   }
	   else if(this.typeClose.equals("openProcessPopupConfirmNoClose"))
	   {
		   openProcessPopupConfirmNo = false;
	   }
	   
	   
	   //---------------------------------------close process
	   else if(this.typeClose.equals("inputNG") || this.typeClose.equals("inputNGRepair"))
	   {
		   inputNG            = false;
		   inputNGRepair      = false;
	   } 
	   else if(this.typeClose.equals("inputNGClose"))
	   {
		   inputNG = false; 
	   }	
	   else if(this.typeClose.equals("inputNGRepairClose"))
	   {
		   inputNGRepair    = false;
	   }
	   else if(this.typeClose.equals("closeProcessPopupClose")){
		   closeProcessConfirm   = true;
		   closeProcessPopup     = false;
	   }
	   else if(this.typeClose.equals("closeProcessPopupClose")){ 
		   closeProcessPopup     = false;
	   }
	   else if(this.typeClose.equals("closeProcessConfirmNo")){
		   closeProcessConfirmNo   = true;
		   closeProcessConfirm     = false; 
	   }
	   else if(this.typeClose.equals("closeProcessConfirmYes")){
		   closeProcessConfirm       = false;
		   closeProcessConfirmYes    = true; 
	   } 
	 /* else if(this.typeClose.equals("x2_23")){
		   closeProcessConfirm     = false;
		   closeProcessConfirmNo   = true;
	  }*/
	  else if(this.typeClose.equals("closeProcessConfirmYesClose") || this.typeClose.equals("closeProcessConfirmNoClose") ){
		  closeProcessConfirmYes    = false;
		  closeProcessConfirmNo     = false;
	  }  
	  
	   
	  //---------------------------------------downtime process
	  else if(this.typeClose.equals("downtimeProcessPopup"))
	  {
		  downtimeProcessPopup         = false;
		  downtimeProcessPopupConfirm  = true;
		   
	  } 
	  else if(this.typeClose.equals("downtimeProcessPopupClose"))
	  {
		  downtimeProcessPopup      = false;
	  }
	  else if(this.typeClose.equals("downtimeProcessPopupConfirmYes"))
	  {
		  downtimeProcessPopupConfirmYes  = true;
		  downtimeProcessPopupConfirm     = false;
	  }
	  else if(this.typeClose.equals("downtimeProcessPopupConfirmNo"))
	  {
		  downtimeProcessPopupConfirmNo   = true;
		  downtimeProcessPopupConfirm     = false;
	  }
	  else if(this.typeClose.equals("downtimeProcessPopupConfirmYesClose") || this.typeClose.equals("downtimeProcessPopupConfirmNoClose") )
	  {
		  downtimeProcessPopupConfirmNo   = false;
		  downtimeProcessPopupConfirmYes  = false;
	  }  
	   //---------------------------------------Hold process
	   
	  else if(this.typeClose.equals("HoldProcessPopup"))
	  {
		  holdProcessPopup              = false;
	  }
	  else if(this.typeClose.equals("HoldProcessPopupClose"))
	  {
		  holdProcessPopup              = false; 
	  }
	   
	  else if(this.typeClose.equals("stopDowntimeProcessPopup"))
	  {
		  stopDowntimeProcessPopup       = false;
	  }
	  else if(this.typeClose.equals("stopDowntimeProcessPopupClose"))
	  {
		  stopDowntimeProcessPopup       = false;
	  } 
	   
	   //--------------------------------------------move m/c process
	  else if(this.typeClose.equals("moveProcessPopup"))
	  {
		  moveProcessPopup               = false; 
		  moveProcessPopupConfirm        = true; 
	  }
	  else if(this.typeClose.equals("moveProcessPopupClose"))
	  {
		  moveProcessPopup               = false;
	  }	  
	  else if(this.typeClose.equals("moveProcessPopupConfirmYes"))
	  {	
		  moveProcessPopupConfirm       = false;
		  moveProcessPopupConfirmYes    = true;
	  }
	  else if(this.typeClose.equals("moveProcessPopupConfirmNo"))
	  {	
		  moveProcessPopupConfirm       = false;
		  moveProcessPopupConfirmNo     = true;
	  }
	  else if(this.typeClose.equals("moveProcessPopupConfirmYesClose"))
	  {
		  moveProcessPopupConfirmYes    = false;
	  }
	  else if(this.typeClose.equals("moveProcessPopupConfirmNoClose"))
	  {
		  moveProcessPopupConfirmNo = false;
	  }  
        
 } 
    public void openPopupType(String visibles)
    {  
    	this.visibles = visibles;
    	if(this.visibles.equals("openProcessPopup"))
    	{  
    		openProcessPopup  = true; 	 
    	}  
    	else if(this.visibles.equals("closeProcessPopup"))
    	{  
    		closeProcessPopup = true;
    	}
    	else if(this.visibles.equals("inputNG"))
    	{
    		inputNG = true; 
    	}
    	else if(this.visibles.equals("inputNGrepair"))
    	{
    		inputNGRepair = true;
    	}
    	else if(this.visibles.equals("mcDowntime"))
    	{
    		downtimeProcessPopup = true;
    	}
    	else if(this.visibles.equals("Hold"))
    	{
    		holdProcessPopup  = true;
    		
    	}
    	else if(this.visibles.equals("StopDowntime"))
    	{
    		stopDowntimeProcessPopup  = true;
    	}
    	else if(this.visibles.equals("Movemc"))
    	{
    		moveProcessPopup   = true;
    		
    	}
    } 
	/**
	 * @return the x
	 */ 
	/**
	 * @return the x1
	 */
    //----------------Open Process
	public boolean isOpenProcessPopup() {
		return openProcessPopup;
	}
	public boolean isOpenProcessPopupConfirm() {
		return openProcessPopupConfirm;
	}
	public boolean isOpenProcessPopupConfirmYes() {
		return openProcessPopupConfirmYes;
	}
	public boolean isOpenProcessPopupConfirmNo() {
		return openProcessPopupConfirmNo;
	}
	
	//------------------Close Process
	public boolean isCloseProcessPopup() {
		return closeProcessPopup;
	}
	public boolean isCloseProcessConfirm () {
		return closeProcessConfirm ;
	}
	public boolean isCloseProcessConfirmYes() {
		return closeProcessConfirmYes;
	}
	public boolean isCloseProcessConfirmNo() {
		return closeProcessConfirmNo;
	}  
	public boolean isInputNG() {
		return inputNG;
	}
	public boolean isInputNGRepair() {
		return inputNGRepair;
	}
	
	//-----------------Grop downtime 
	public boolean isDowntimeProcessPopup() {
		return downtimeProcessPopup;
	}
	public boolean isDowntimeProcessPopupConfirm() {
		return downtimeProcessPopupConfirm;
	} 
	public boolean isDowntimeProcessPopupConfirmYes() {
		return downtimeProcessPopupConfirmYes;
	}
	public boolean isDowntimeProcessPopupConfirmNo() {
		return downtimeProcessPopupConfirmNo;
	}
	 
	//----------------Hole Process
	public boolean isHoldProcessPopup() {
		return holdProcessPopup;
	}
	
	//---------------Move Process
	public boolean isStopDowntimeProcessPopup() {
		return stopDowntimeProcessPopup;
	}
	
	public boolean isMoveProcessPopup() {
		return moveProcessPopup;
	}
	public boolean isMoveProcessPopupConfirm() {
		return moveProcessPopupConfirm;
	}
	public boolean isMoveProcessPopupConfirmYes() {
		return moveProcessPopupConfirmYes;
	}
	public boolean isMoveProcessPopupConfirmNo() {
		return moveProcessPopupConfirmNo;
	} 
}
	 
 