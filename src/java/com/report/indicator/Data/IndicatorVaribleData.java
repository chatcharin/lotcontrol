/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.report.indicator.Data;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Nov 6, 2012, Time : 4:01:31 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class IndicatorVaribleData {
    private double avability;
    private double performance;
    private double quality;
    private double oee;

    public IndicatorVaribleData(double avability, double performance, double quality, double oee) {
        this.avability = avability;
        this.performance = performance;
        this.quality = quality;
        this.oee = oee;
    }

    public double getAvability() {
        return avability;
    }

    public void setAvability(double avability) {
        this.avability = avability;
    }

    public double getPerformance() {
        return performance;
    }

    public void setPerformance(double performance) {
        this.performance = performance;
    }

    public double getQuality() {
        return quality;
    }

    public void setQuality(double quality) {
        this.quality = quality;
    }

    public double getOee() {
        return oee;
    }

    public void setOee(double oee) {
        this.oee = oee;
    }

   
}
