/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.report;

import com.appCinfigpage.bean.fc;
import com.tct.dashboard.ReportBean;
import com.tct.data.jpa.*;
import com.tct.data.jsf.util.PaginationHelper;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.persistence.Persistence;
import com.tct.data.jpa.DetailSpecialJpaController;
/**
 *
 * @author Tawat Saekue
 *
 */
public class NgHistoryControl {
//  ng and output qty
    private DetailSpecialJpaController  detailSpecialJpaController = null;
    public DetailSpecialJpaController getDetailSpecialJpaController()
    {
        if(detailSpecialJpaController == null)
        {
            detailSpecialJpaController  = new DetailSpecialJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailSpecialJpaController;
    }
    private final String sql_ngnormal = "SELECT"
            + "     CONCAT(DAY(currentprocess.time_current),'/',MONTH(currentprocess.time_current),'/',YEAR(currentprocess.time_current)) AS currentdates," // index : 1
            + "     lotcontrol.barcode AS barcode,"// index : 2
            + "     model_pool.model_series AS series,"// index : 3
            + "     CONCAT(model_pool.model_type,'-',model_pool.model_series,'-',model_pool.model_name) AS model,"// index : 4
            + "     process_pool.proc_name AS pcname,"// index : 5
            + "     process_pool.type AS pctype,"// index : 6
            + "     ng_normal.name AS ngnormalname,"// index : 7
            + "     if(pcs_qty.qty_type='in',pcs_qty.qty,0) AS input,"// index : 8
            + "     lotcontrol.qty_output AS output,"// index : 9
            + "     sum(ng_normal.qty) AS ngnormalqty,"// index : 10
            + "     if(sum(ng_normal.qty)!=null||sum(ng_normal.qty)>0,(sum(ng_normal.qty)/pcs_qty.qty)*100,0) AS percenng,"// index : 11
            + "     if(sum(ng_repair.qty)!=null||sum(ng_repair.qty)>0,(sum(ng_repair.qty)/pcs_qty.qty)*100,0) AS percenngrepair,"// index : 12
            + "     if(if(sum(ng_normal.qty)!=null||sum(ng_normal.qty)>0,(sum(ng_normal.qty)/pcs_qty.qty)*100,0)>0,100-if(sum(ng_normal.qty)!=null||sum(ng_normal.qty)>0,(sum(ng_normal.qty)/pcs_qty.qty)*100,0),100) AS percenyield"// index : 13
            + " FROM"
            + "     main_data maindata LEFT OUTER JOIN lot_control lotcontrol ON maindata.lot_control_id = lotcontrol.id"
            + "     LEFT OUTER JOIN process_pool ON maindata.id_proc = process_pool.id_proc"
            + "     LEFT OUTER JOIN current_process currentprocess ON maindata.id = currentprocess.main_data_id"
            + "     RIGHT OUTER JOIN current_process currentprocess1 ON maindata.id = currentprocess1.main_data_id"
            + "     RIGHT OUTER JOIN ng_normal ON currentprocess1.id = ng_normal.current_process_id"
            + "     LEFT OUTER JOIN ng_repair ON currentprocess1.id = ng_repair.current_process_id"
            + "     LEFT OUTER JOIN pcs_qty ON currentprocess.id = pcs_qty.current_process_id"
            + "     LEFT OUTER JOIN model_pool ON lotcontrol.model_pool_id = model_pool.id";
    //  Search 
    private String barcodeinput;
    private String processname;
    private String processtype;
    private Date startdate;
    private Date todate;
    private String defectname;
    private boolean ngnormal = true;
    private boolean ngrepair = true;
    // Valiable
    private DataModel<NgList> dataModelTable;
    private PaginationHelper paginationHelper;
    private List<NgList> ngLists;

    public NgHistoryControl() {
        try {
            searchNgHostory();
        } catch (SQLException ex) {
            fc.print("Error : "+ex);
//            Logger.getLogger(NgHistoryControl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    //if(condition,true,false)

    private void searchByDefectName() throws SQLException {
        ngLists = null;
        fc.print("========= search by monthly : " + getDateFormat("MM / yyyy", new Date()));
        Connection conn = getConnection("mysql");
        Statement st = conn.createStatement();
        try {
            Date date = new Date();
//            String dates = getDateFormat("yyyy-MM-dd HH:mm:ss", date);
            String fdate = "";
            String barcode = "";
            String pcname = "";
            String pctype = "";
            String ngname = "";
            double ngqty = 0;
            String series = "";
            String model = "";
            String stamp = "stamp";
            double input = 0;
            double output = 0;
            double percenng = 0;
            double percenrepair = 0;
            double percenyield = 0;


            String sql = sql_ngnormal + ngFieldterByNgname(defectname) + GroupByNGname() + orderByASC(" currentprocess.time_current ");
            fc.print("SQL : " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                fdate = rs.getObject(1).toString();
                barcode = rs.getObject(2).toString();
                series = rs.getObject(3).toString();
                model = rs.getObject(4).toString();
                pcname = rs.getObject(5).toString();
                pctype = getDetailSpecialJpaController().findDetailSpecial(rs.getObject(6).toString()).getName();
//                pctype = rs.getObject(6).toString();
                ngname = rs.getObject(7).toString();
                input = Double.parseDouble(rs.getObject(8).toString());
                output = Double.parseDouble(rs.getObject(9).toString());
                ngqty = Double.parseDouble(rs.getObject(10).toString());
                percenng = Double.parseDouble(rs.getObject(11).toString());
                percenrepair = Double.parseDouble(rs.getObject(12).toString());
                percenyield = Double.parseDouble(rs.getObject(13).toString());

                getNgLists().add(new NgList(fdate, barcode, pcname, pctype, ngname, convertDoubleFormat(ngqty),
                        series, model, stamp, convertDoubleFormat(input), convertDoubleFormat(output), convertDoubleFormat(percenng) + " %",
                        convertDoubleFormat(percenrepair), convertDoubleFormat(percenyield) + " %"));
                fc.print("complete");
            }
//            setDataModelTable();
            conn.close();
            st.close();
        } catch (Exception e) {
            fc.print("Error search by month : " + e);
            conn.close();
            st.close();
        }
    }

    private String ngFieldterByNgname(String ngname) {
        String sql = " WHERE "
                + " ng_normal.name LIKE '" + ngname + "%' "
                + " AND currentprocess.time_current > '" + getDateFormat("yyyy-MM-dd HH:mm:ss", startdate) + "' "
                + " AND currentprocess.time_current < '" + getDateFormat("yyyy-MM-dd HH:mm:ss", todate) + "'";
        return sql;
    }

    private String GroupByNGname() {
        String sql = " AND pcs_qty.qty_type='in' AND (lotcontrol.qty_output <> null || lotcontrol.qty_output > 0) GROUP BY ng_normal.name ";
        return sql;
    }

    private void searchByProcessType() throws SQLException {
        ngLists = null;
        fc.print("========= search by monthly : " + getDateFormat("MM / yyyy", new Date()));
        Connection conn = getConnection("mysql");
        Statement st = conn.createStatement();
        try {
            Date date = new Date();
            String dates = getDateFormat("yyyy-MM-dd HH:mm:ss", date);
            String fdate = "";
            String barcode = "";
            String pcname = "";
            String pctype = "";
            String ngname = "";
            double ngqty = 0;
            String series = "";
            String model = "";
            String stamp = "stamp";
            double input = 0;
            double output = 0;
            double percenng = 0;
            double percenrepair = 0;
            double percenyield = 0;


            String sql = sql_ngnormal + ngFieldterByProcesstype(processtype) + GroupByNGname() + orderByASC(" currentprocess.time_current ");
            fc.print("SQL : " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                fdate = rs.getObject(1).toString();
                barcode = rs.getObject(2).toString();
                series = rs.getObject(3).toString();
                model = rs.getObject(4).toString();
                pcname = rs.getObject(5).toString();
                pctype = getDetailSpecialJpaController().findDetailSpecial(rs.getObject(6).toString()).getName();
                ngname = rs.getObject(7).toString();
                input = Double.parseDouble(rs.getObject(8).toString());
                output = Double.parseDouble(rs.getObject(9).toString());
                ngqty = Double.parseDouble(rs.getObject(10).toString());
                percenng = Double.parseDouble(rs.getObject(11).toString());
                percenrepair = Double.parseDouble(rs.getObject(12).toString());
                percenyield = Double.parseDouble(rs.getObject(13).toString());

                getNgLists().add(new NgList(fdate, barcode, pcname, pctype, ngname, convertDoubleFormat(ngqty),
                        series, model, stamp, convertDoubleFormat(input), convertDoubleFormat(output), convertDoubleFormat(percenng) + " %",
                        convertDoubleFormat(percenrepair), convertDoubleFormat(percenyield) + " %"));
                fc.print("complete");
            }
//            setDataModelTable();
            conn.close();
            st.close();
        } catch (Exception e) {
            fc.print("Error search by month : " + e);
            conn.close();
            st.close();
        }
    }

    private String ngFieldterByProcesstype(String pctype) {
        String sql = " WHERE "
                + " process_pool.type LIKE '" + pctype + "%' "
                + " AND currentprocess.time_current > '" + getDateFormat("yyyy-MM-dd HH:mm:ss", startdate) + "' "
                + " AND currentprocess.time_current < '" + getDateFormat("yyyy-MM-dd HH:mm:ss", todate) + "'";
        return sql;
    }

    private void searchByProcess() throws SQLException {
        ngLists = null;
        fc.print("========= search by monthly : " + getDateFormat("MM / yyyy", new Date()));
        Connection conn = getConnection("mysql");
        Statement st = conn.createStatement();
        try {
            Date date = new Date();
            String dates = getDateFormat("yyyy-MM-dd HH:mm:ss", date);
            String fdate = "";
            String barcode = "";
            String pcname = "";
            String pctype = "";
            String ngname = "";
            double ngqty = 0;
            String series = "";
            String model = "";
            String stamp = "stamp";
            double input = 0;
            double output = 0;
            double percenng = 0;
            double percenrepair = 0;
            double percenyield = 0;


            String sql = sql_ngnormal + ngFieldterByProcess(processname) + GroupByNGname() + orderByASC(" currentprocess.time_current ");
            fc.print("SQL : " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                fdate = rs.getObject(1).toString();
                barcode = rs.getObject(2).toString();
                series = rs.getObject(3).toString();
                model = rs.getObject(4).toString();
                pcname = rs.getObject(5).toString();
                pctype = getDetailSpecialJpaController().findDetailSpecial(rs.getObject(6).toString()).getName();
                ngname = rs.getObject(7).toString();
                input = Double.parseDouble(rs.getObject(8).toString());
                output = Double.parseDouble(rs.getObject(9).toString());
                ngqty = Double.parseDouble(rs.getObject(10).toString());
                percenng = Double.parseDouble(rs.getObject(11).toString());
                percenrepair = Double.parseDouble(rs.getObject(12).toString());
                percenyield = Double.parseDouble(rs.getObject(13).toString());

                getNgLists().add(new NgList(fdate, barcode, pcname, pctype, ngname, convertDoubleFormat(ngqty),
                        series, model, stamp, convertDoubleFormat(input), convertDoubleFormat(output), convertDoubleFormat(percenng) + " %",
                        convertDoubleFormat(percenrepair), convertDoubleFormat(percenyield) + " %"));
                fc.print("complete");
            }
//            setDataModelTable();
            st.close();
            conn.close();
        } catch (Exception e) {
            fc.print("Error search by month : " + e);
            st.close();
            conn.close();
        }
    }

    private String ngFieldterByProcess(String pcname) {
        String sql = " WHERE "
                + " process_pool.proc_name LIKE '" + pcname + "%'"
                + " AND currentprocess.time_current > '" + getDateFormat("yyyy-MM-dd HH:mm:ss", startdate) + "' "
                + " AND currentprocess.time_current < '" + getDateFormat("yyyy-MM-dd HH:mm:ss", todate) + "'";
        return sql;
    }

    private void searchByBarcode() throws SQLException {
        ngLists = null;
        fc.print("========= search by monthly : " + getDateFormat("MM / yyyy", new Date()));
        Connection conn = getConnection("mysql");
        Statement st = conn.createStatement();
        try {
            Date date = new Date();
            String dates = getDateFormat("yyyy-MM-dd HH:mm:ss", date);
            String fdate = "";
            String barcode = "";
            String pcname = "";
            String pctype = "";
            String ngname = "";
            double ngqty = 0;
            String series = "";
            String model = "";
            String stamp = "stamp";
            double input = 0;
            double output = 0;
            double percenng = 0;
            double percenrepair = 0;
            double percenyield = 0;


            String sql = sql_ngnormal + ngFieldterByBarcode(barcodeinput) + GroupByNGname() + orderByASC(" currentprocess.time_current ");
            fc.print("SQL : " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                fdate = rs.getObject(1).toString();
                barcode = rs.getObject(2).toString();
                series = rs.getObject(3).toString();
                model = rs.getObject(4).toString();
                pcname = rs.getObject(5).toString(); 
                pctype = getDetailSpecialJpaController().findDetailSpecial(rs.getObject(6).toString()).getName();
                ngname = rs.getObject(7).toString();
                input = Double.parseDouble(rs.getObject(8).toString());
                output = Double.parseDouble(rs.getObject(9).toString());
                ngqty = Double.parseDouble(rs.getObject(10).toString());
                percenng = Double.parseDouble(rs.getObject(11).toString());
                percenrepair = Double.parseDouble(rs.getObject(12).toString());
                percenyield = Double.parseDouble(rs.getObject(13).toString());

                getNgLists().add(new NgList(fdate, barcode, pcname, pctype, ngname, convertDoubleFormat(ngqty),
                        series, model, stamp, convertDoubleFormat(input), convertDoubleFormat(output), convertDoubleFormat(percenng) + " %",
                        convertDoubleFormat(percenrepair), convertDoubleFormat(percenyield) + " %"));
                fc.print("complete");
            }
//            setDataModelTable();
            conn.close();
            st.close();
        } catch (Exception e) {
            fc.print("Error search by month : " + e);
            conn.close();
            st.close();
        }
    }

    private String ngFieldterByBarcode(String barcode) {
        String sql = " WHERE "
                + " lotcontrol.barcode LIKE '" + barcode + "%'";
        return sql;
    }

    public void searchByDaily() throws SQLException {
        ngLists = null;
        fc.print("========= search by monthly : " + getDateFormat("MM / yyyy", new Date()));
        Connection conn = getConnection("mysql");
        Statement st = conn.createStatement();
        try {
            Date date = new Date();
            String dates = getDateFormat("yyyy-MM-dd HH:mm:ss", date);
            String fdate = "";
            String barcode = "";
            String pcname = "";
            String pctype = "";
            String ngname = "";
            double ngqty = 0;
            String series = "";
            String model = "";
            String stamp = "stamp";
            double input = 0;
            double output = 0;
            double percenng = 0;
            double percenrepair = 0;
            double percenyield = 0;


            String sql = sql_ngnormal + ngFieldterByDate() + GroupByNGname() + orderByASC(" currentprocess.time_current ");
            fc.print("SQL : " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                fdate = rs.getObject(1).toString();
                barcode = rs.getObject(2).toString();
                series = rs.getObject(3).toString();
                model = rs.getObject(4).toString();
                pcname = rs.getObject(5).toString();
                pctype = getDetailSpecialJpaController().findDetailSpecial(rs.getObject(6).toString()).getName();
                ngname = rs.getObject(7).toString();
                input = Double.parseDouble(rs.getObject(8).toString());
                output = Double.parseDouble(rs.getObject(9).toString());
                ngqty = Double.parseDouble(rs.getObject(10).toString());
                percenng = Double.parseDouble(rs.getObject(11).toString());
                percenrepair = Double.parseDouble(rs.getObject(12).toString());
                percenyield = Double.parseDouble(rs.getObject(13).toString());

                getNgLists().add(new NgList(fdate, barcode, pcname, pctype, ngname, convertDoubleFormat(ngqty),
                        series, model, stamp, convertDoubleFormat(input), convertDoubleFormat(output), convertDoubleFormat(percenng) + " %",
                        convertDoubleFormat(percenrepair), convertDoubleFormat(percenyield) + " %"));
                fc.print("complete");
            }
            conn.close();
            st.close();
//            setDataModelTable();
        } catch (Exception e) {
            fc.print("Error search by month : " + e);
            conn.close();
            st.close();
        }
        setDataModelTable();
    }

    public void searchByWeekly() throws SQLException {
        ngLists = null;
        fc.print("========= search by monthly : " + getDateFormat("MM / yyyy", new Date()));
        Connection conn = getConnection("mysql");
        Statement st = conn.createStatement();
        try {
            Date date = new Date();
            String dates = getDateFormat("yyyy-MM-dd HH:mm:ss", date);
            String fdate = "";
            String barcode = "";
            String pcname = "";
            String pctype = "";
            String ngname = "";
            double ngqty = 0;
            String series = "";
            String model = "";
            String stamp = "stamp";
            double input = 0;
            double output = 0;
            double percenng = 0;
            double percenrepair = 0;
            double percenyield = 0;


            String sql = sql_ngnormal + ngFieldterWeekly(dates) + GroupByNGname() + orderByASC(" currentprocess.time_current ");
            fc.print("SQL : " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                fdate = rs.getObject(1).toString();
                barcode = rs.getObject(2).toString();
                series = rs.getObject(3).toString();
                model = rs.getObject(4).toString();
                pcname = rs.getObject(5).toString();
                pctype = getDetailSpecialJpaController().findDetailSpecial(rs.getObject(6).toString()).getName();
                ngname = rs.getObject(7).toString();
                input = Double.parseDouble(rs.getObject(8).toString());
                output = Double.parseDouble(rs.getObject(9).toString());
                ngqty = Double.parseDouble(rs.getObject(10).toString());
                percenng = Double.parseDouble(rs.getObject(11).toString());
                percenrepair = Double.parseDouble(rs.getObject(12).toString());
                percenyield = Double.parseDouble(rs.getObject(13).toString());

                getNgLists().add(new NgList(fdate, barcode, pcname, pctype, ngname, convertDoubleFormat(ngqty),
                        series, model, stamp, convertDoubleFormat(input), convertDoubleFormat(output), convertDoubleFormat(percenng) + " %",
                        convertDoubleFormat(percenrepair), convertDoubleFormat(percenyield) + " %"));
                fc.print("complete");
            }
//            setDataModelTable();
            conn.close();
            st.close();
        } catch (Exception e) {
            fc.print("Error search by month : " + e);
            conn.close();
            st.close();
        }
        setDataModelTable();
    }

    public void searchByMonthly() throws SQLException {
        ngLists = null;
        fc.print("========= search by monthly : " + getDateFormat("MM / yyyy", new Date()));
        Connection conn = getConnection("mysql");
            Statement st = conn.createStatement();
        try {
            Date date = new Date();
            String year = getDateFormat("yyyy", date);
            String month = getDateFormat("MM", date);
            String fdate = "";
            String barcode = "";
            String pcname = "";
            String pctype = "";
            String ngname = "";
            double ngqty = 0;
            String series = "";
            String model = "";
            String stamp = "stamp";
            double input = 0;
            double output = 0;
            double percenng = 0;
            double percenrepair = 0;
            double percenyield = 0;

            
            String sql = sql_ngnormal + ngFieldterMonthAndYear(month, year) + GroupByNGname() + orderByASC(" currentprocess.time_current ");
            fc.print("SQL : " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                fdate = rs.getObject(1).toString();
                barcode = rs.getObject(2).toString();
                series = rs.getObject(3).toString();
                model = rs.getObject(4).toString();
                pcname = rs.getObject(5).toString();
                pctype = getDetailSpecialJpaController().findDetailSpecial(rs.getObject(6).toString()).getName();
                ngname = rs.getObject(7).toString();
                input = Double.parseDouble(rs.getObject(8).toString());
                output = Double.parseDouble(rs.getObject(9).toString());
                ngqty = Double.parseDouble(rs.getObject(10).toString());
                percenng = Double.parseDouble(rs.getObject(11).toString());
                percenrepair = Double.parseDouble(rs.getObject(12).toString());
                percenyield = Double.parseDouble(rs.getObject(13).toString());

                getNgLists().add(new NgList(fdate, barcode, pcname, pctype, ngname, convertDoubleFormat(ngqty),
                        series, model, stamp, convertDoubleFormat(input), convertDoubleFormat(output), convertDoubleFormat(percenng) + " %",
                        convertDoubleFormat(percenrepair), convertDoubleFormat(percenyield) + " %"));
                fc.print("complete");
            }
//            setDataModelTable();
            conn.close();
            st.close();
        } catch (Exception e) {
            fc.print("Error search by month : " + e);
            conn.close();
            st.close();
        }
        setDataModelTable();
    }

    public void searchByYearlys() throws SQLException {
        ngLists = null;
        fc.print("========== search Yearly by sqlquery : " + getDateFormat("yyyy", new Date()));
        Connection conn = getConnection("mysql");
            Statement st = conn.createStatement();
        try {
            String year = getDateFormat("yyyy", new Date());

            String fdate = "";
            String barcode = "";
            String pcname = "";
            String pctype = "";
            String ngname = "";
            double ngqty = 0;
            String series = "";
            String model = "";
            String stamp = "stamp";
            double input = 0;
            double output = 0;
            double percenng = 0;
            double percenrepair = 0;
            double percenyield = 0;

            
            String sql = sql_ngnormal + ngFieldterYear(year) + GroupByNGname() + orderByASC(" currentprocess.time_current ");
            fc.print("SQL : " + sql);
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                fdate = rs.getObject(1).toString();
                barcode = rs.getObject(2).toString();
                series = rs.getObject(3).toString();
                model = rs.getObject(4).toString();
                pcname = rs.getObject(5).toString();
                pctype = getDetailSpecialJpaController().findDetailSpecial(rs.getObject(6).toString()).getName();
                ngname = rs.getObject(7).toString();
                input = Double.parseDouble(rs.getObject(8).toString());
                output = Double.parseDouble(rs.getObject(9).toString());
                ngqty = Double.parseDouble(rs.getObject(10).toString());
                percenng = Double.parseDouble(rs.getObject(11).toString());
                percenrepair = Double.parseDouble(rs.getObject(12).toString());
                percenyield = Double.parseDouble(rs.getObject(13).toString());

                getNgLists().add(new NgList(fdate, barcode, pcname, pctype, ngname, convertDoubleFormat(ngqty),
                        series, model, stamp, convertDoubleFormat(input), convertDoubleFormat(output), convertDoubleFormat(percenng) + " %",
                        convertDoubleFormat(percenrepair), convertDoubleFormat(percenyield) + " %"));
                fc.print("complete");
            }
//            setDataModelTable();
            conn.close();
            st.close();
        } catch (Exception e) {
            fc.print("Error : " + e);
            conn.close();
            st.close();
        }
        setDataModelTable();
    }

    // sql select ng and output where yearly
    private String ngFieldterYear(String fieldter) {
        String sql = " WHERE "
                + " YEAR(currentprocess.time_current) = '" + fieldter + "'";
        return sql;
    }
// fieldter by month and yer

    private String ngFieldterMonthAndYear(String month, String year) {
        String sql = " WHERE "
                + " MONTH(currentprocess.time_current) ='" + month + "' AND YEAR(currentprocess.time_current) = '" + year + "'";
        return sql;
    }

    private String ngFieldterWeekly(String date) {
        String sql = " WHERE "
                + " WEEK(currentprocess.time_current) = WEEK('" + date + "')";
        return sql;
    }

    private String ngFieldterByDate() {
        String sql = " WHERE "
                + " DAY(currentprocess.time_current) = '" + getDateFormat("dd", new Date()) + "'";
        return sql;
    }

    private String orderByASC(String field) {
        String sql = " ORDER BY " + field + " ASC";
        return sql;
    }

    public Connection getConnection(String brand) {
        fc.print(brand);
        if (brand.equals("mysql")) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                return (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tct_project", "root", "root");
            } catch (Exception ex) {
                Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        } else {
            try {
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                return (Connection) DriverManager.getConnection("jdbc:sqlserver://localhost\\SQLEXPRESS:1433;databaseName=tct_project", "test", "1234");
            } catch (Exception ex) {
                Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }

    }

    private String convertDoubleFormat(double d) {
        return new DecimalFormat("#,###.##").format(d);
    }

//    private int getOutput(MainData mainData) {
//        fc.print("=========== get input ========================================");
//        try {
//            int output = 0;
////            String sql = "SELECT"
////                    + "     pcs_qty.qty," // index 1
////                    + "     pcs_qty.qty_type" // index 2 
////                    + " FROM"
////                    + "     main_data INNER JOIN current_process ON main_data.id = current_process.main_data_id"
////                    + "     INNER JOIN pcs_qty ON current_process.id = pcs_qty.current_process_id"
////                    + " WHERE main_data.id = '" + mainData.getId() + "' AND pcs_qty.qty_type = 'out'";
//            List<CurrentProcess> currentProcesses = getCurrentProcessJpaController().findCurrentStatus(mainData);
//            for (CurrentProcess currentProcess : currentProcesses) {
//                if (currentProcess.getPcsQtyCollection().size() > 0) {
//                    Collection<PcsQty> pcsQtys = currentProcess.getPcsQtyCollection();
//                    for (PcsQty pcsQty : pcsQtys) {
//                        if (pcsQty.getQtyType().equals("out")) {
//                            output = Integer.parseInt(pcsQty.getQty());
//                        }
//                    }
//                }
//            }
//            return output;
//        } catch (Exception e) {
//            fc.print("Error : " + e);
//            return 0;
//        }
//    }
    private int getDateOfMonth(int month) {
        int dd = 0;
        switch (month) {
            case 1:  //มกราคม
                dd = 31;
                break;
            case 2:  // กุมภาพันธ์
                dd = 28;
                break;
            case 3:  // มีนาคม
                dd = 31;
                break;
            case 4:  // เมษายน
                dd = 30;
                break;
            case 5:  // พฤษภาคม
                dd = 31;
                break;
            case 6:  // มิถุนายน
                dd = 30;
                break;
            case 7:  // กรกฏาคม
                dd = 31;
                break;
            case 8:  // สิงหาคม
                dd = 31;
                break;
            case 9:  // กันยายน
                dd = 30;
                break;
            case 10:  // ตุลาคม
                dd = 31;
                break;
            case 11: //พฤศจิกายน
                dd = 30;
                break;
            case 12: // ธันวาคม
                dd = 31;
                break;
            case 13:  // ทุกๆ รอบ 4 ปี 
                dd = 29;
                break;
            default:
                fc.print("Error : " + month);
                break;
        }
        return dd;
    }

    private int getDateOfYear(int years) {
        try {
            Date enddate = new SimpleDateFormat("dd/MM/yyyy").parse("31/12/" + years);
            int numdateperyear = Integer.parseInt(getDateFormat("D", enddate));
            fc.print("Numdate per year : " + numdateperyear);
            return numdateperyear;
        } catch (Exception e) {
            fc.print("Error : " + e);
            return -1;
        }
    }

    private String ConvertToString(int i) {
        if (i + "".length() == 1) {
            return "0" + i;
        } else {
            return i + "";
        }
    }

    private int getDateNumInWeek(String day) {
        if (day.equals("Mon")) {
            return 1;
        } else if (day.equals("Tue")) {
            return 2;
        } else if (day.equals("Wed")) {
            return 3;
        } else if (day.equals("Thu")) {
            return 4;
        } else if (day.equals("Fri")) {
            return 5;
        } else if (day.equals("Sae")) {
            return 6;
        } else {
            return 7;
        }
    }

    public void searchNgHostory() throws SQLException {
        fc.print("============== Search NG History =============================");
        ngLists = null;
        paginationHelper = null;
        if (checkProcesstype()) {
            searchByProcessType();
        } else if (checkProcessname()) {
            searchByProcess();
        } else if (checkDefectname()) {
            searchByDefectName();
        } else if (checkBarcode()) {
            searchByBarcode();
        }
        setDataModelTable();
    }

//    private void searchByProcessType() {
//        fc.print("============== Search By Process Type : " + processtype + " ==");
//        try {
//            List<ProcessPool> processPools = getProcessPoolJpaController().findProcessPoolByType(processtype);
//            for (ProcessPool processPool : processPools) {
//                fc.print("Process : " + processPool.getProcName());
//                Collection<MainData> mainDatas = processPool.getMainDataCollection();
//                for (MainData mainData : mainDatas) {
//                    fc.print("MainData : " + mainData.getId());
//                    LotControl lotControl = mainData.getLotControlId();
//                    Collection<CurrentProcess> currentProcesses = mainData.getCurrentProcessCollection();
//                    for (CurrentProcess currentProcess : currentProcesses) {
//                        Date date = currentProcess.getTimeCurrent();
//                        if (startdate.before(date) && todate.after(date)) {
//                            fc.print("Todate compare to startdate" + todate.compareTo(startdate));
//                            if (ngnormal) {
//                                Collection<NgNormal> ngNormals = currentProcess.getNgNormalCollection();
//                                for (NgNormal ngNormal : ngNormals) {
////                                    getNgLists().add(new NgList(getDateFormat("dd/MM/yyyy", date), lotControl.getBarcode(),
////                                            mainData.getIdProc().getProcName(), mainData.getIdProc().getType(), ngNormal.getName(), Long.parseLong(ngNormal.getQty())));
//                                }
//                            }
//                            if (ngrepair) {
//                                Collection<NgRepair> ngRepairs = currentProcess.getNgRepairCollection();
//                                for (NgRepair ngRepair : ngRepairs) {
////                                    getNgLists().add(new NgList(getDateFormat("dd/MM/yyyy", date), lotControl.getBarcode(),
////                                            mainData.getIdProc().getProcName(), mainData.getIdProc().getType(), ngRepair.getName(), Long.parseLong(ngRepair.getQty())));
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        } catch (Exception e) {
//            fc.print("Error search by process type : " + e);
//        }
//    }
//    private void searchByProcessName() {
//        fc.print("============== Search By Process name : " + processname + " ==");
//        try {
//            List<ProcessPool> processPools = getProcessPoolJpaController().findProcessPoolByName(processname);
//            for (ProcessPool processPool : processPools) {
//                Collection<MainData> mainDatas = processPool.getMainDataCollection();
//                for (MainData mainData : mainDatas) {
//                    LotControl lotControl = mainData.getLotControlId();
//                    Collection<CurrentProcess> currentProcesses = mainData.getCurrentProcessCollection();
//                    for (CurrentProcess currentProcess : currentProcesses) {
//                        Date date = currentProcess.getTimeCurrent();
//                        if (startdate.before(date) && todate.after(date)) {
//                            fc.print("Todate compare to startdate" + todate.compareTo(startdate));
//                            if (ngnormal) {
//                                Collection<NgNormal> ngNormals = currentProcess.getNgNormalCollection();
//                                for (NgNormal ngNormal : ngNormals) {
////                                    getNgLists().add(new NgList(getDateFormat("dd/MM/yyyy", date), lotControl.getBarcode(),
////                                            mainData.getIdProc().getProcName(), mainData.getIdProc().getType(), ngNormal.getName(), Long.parseLong(ngNormal.getQty())));
//                                }
//                            }
//                            if (ngrepair) {
//                                Collection<NgRepair> ngRepairs = currentProcess.getNgRepairCollection();
//                                for (NgRepair ngRepair : ngRepairs) {
////                                    getNgLists().add(new NgList(getDateFormat("dd/MM/yyyy", date), lotControl.getBarcode(),
////                                            mainData.getIdProc().getProcName(), mainData.getIdProc().getType(), ngRepair.getName(), Long.parseLong(ngRepair.getQty())));
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        } catch (Exception e) {
//            fc.print("Error search by process : " + e);
//        }
//    }
//    private void searchByDefectName() {
//        fc.print("============== Search By DefectName : " + defectname + " =========");
//        try {
//            int i = 0;
//            if (ngnormal) {
//                fc.print("Normal");
//                List<NgNormal> ngNormals = getNgNormalJpaController().findByName(defectname);
//                for (NgNormal ngNormal : ngNormals) {
//                    CurrentProcess currentProcess = ngNormal.getCurrentProcessId();
//                    if (currentProcess.getTimeCurrent().after(startdate) && currentProcess.getTimeCurrent().before(todate)) {
//                        fc.print("Normal --------------- " + (i++));
//                        MainData mainData = currentProcess.getMainDataId();
//                        LotControl lotControl = mainData.getLotControlId();
////                        getNgLists().add(new NgList(getDateFormat("dd/MM/yyyy", currentProcess.getTimeCurrent()), lotControl.getBarcode(),
////                                mainData.getIdProc().getProcName(), mainData.getIdProc().getType(), ngNormal.getName(), Long.parseLong(ngNormal.getQty())));
//                    }
//                }
//            }
//            i = 0;
//            if (ngrepair) {
//                fc.print("Repair");
//                List<NgRepair> ngRepairs = getNgRepairJpaController().findByName(defectname);
//                for (NgRepair ngRepair : ngRepairs) {
//                    CurrentProcess currentProcess = ngRepair.getCurrentProcessId();
//                    if (startdate.before(currentProcess.getTimeCurrent()) && todate.after(currentProcess.getTimeCurrent())) {
//                        fc.print("Repair --------------- " + (i++));
//                        MainData mainData = currentProcess.getMainDataId();
//                        LotControl lotControl = mainData.getLotControlId();
////                        getNgLists().add(new NgList(getDateFormat("dd/MM/yyyy", currentProcess.getTimeCurrent()), lotControl.getBarcode(),
////                                mainData.getIdProc().getProcName(), mainData.getIdProc().getType(), ngRepair.getName(), Long.parseLong(ngRepair.getQty())));
//                    }
//                }
//            }
//        } catch (Exception e) {
//            fc.print("Error Search By Defect name : " + e);
//        }
//    }
//    private void searchByBarcode() {
//        fc.print("============== Serch By Barcode : " + barcodeinput + "============");
//        try {
//            List<LotControl> lotControls = getLotControlJpaController().findLotcontrolByBarcode(barcodeinput);
//            for (LotControl lotControl : lotControls) {
//                Collection<MainData> mainDatas = lotControl.getMainDataCollection();
//                for (MainData mainData : mainDatas) {
//                    Collection<CurrentProcess> currentProcesses = mainData.getCurrentProcessCollection();
//                    for (CurrentProcess currentProcess : currentProcesses) {
//                        Date date = currentProcess.getTimeCurrent();
//                        fc.print("Date of process : " + date);
////                        if (startdate.before(date) && todate.after(date)) {
//                        fc.print("Todate compare to startdate" + todate.compareTo(startdate));
//                        if (ngnormal) {
//                            Collection<NgNormal> ngNormals = currentProcess.getNgNormalCollection();
//                            for (NgNormal ngNormal : ngNormals) {
////                                getNgLists().add(new NgList(getDateFormat("dd/MM/yyyy", date), lotControl.getBarcode(),
////                                        mainData.getIdProc().getProcName(), mainData.getIdProc().getType(), ngNormal.getName(), Long.parseLong(ngNormal.getQty())));
//                            }
//                        }
//                        if (ngrepair) {
//                            Collection<NgRepair> ngRepairs = currentProcess.getNgRepairCollection();
//                            for (NgRepair ngRepair : ngRepairs) {
////                                getNgLists().add(new NgList(getDateFormat("dd/MM/yyyy", date), lotControl.getBarcode(),
////                                        mainData.getIdProc().getProcName(), mainData.getIdProc().getType(), ngRepair.getName(), Long.parseLong(ngRepair.getQty())));
//                            }
//                        }
////                        }
//                    }
//                }
//            }
//        } catch (Exception e) {
//            fc.print("Error Search By Barcode : " + e);
//        }
//    }
    public void previous() {
        getPaginationHelper().previousPage();
        setDataModelTable();
    }

    public void next() {
        getPaginationHelper().nextPage();
        setDataModelTable();
    }

    public int getAllPages() {
        fc.print("Step ---------- 1");
        int all = getPaginationHelper().getItemsCount();
        fc.print("Step ---------- 2");
        int pagesize = getPaginationHelper().getPageSize();
        fc.print("Step ---------- 3");
        return ((int) all / pagesize) + 1;
    }

    private String getDateFormat(String patarn, Date dtae) {
        return new SimpleDateFormat(patarn).format(dtae);
    }

    public DataModel<NgList> getDataModelTable() {
        return dataModelTable;
    }

    public void setDataModelTable() {
        this.dataModelTable = getPaginationHelper().createPageDataModel();
    }

    public PaginationHelper getPaginationHelper() {
        if (paginationHelper == null) {
            paginationHelper = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    return getNgLists().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    fc.print("PageFirst : " + getPageFirstItem());
                    fc.print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (getNgLists().size() > 0) {
                        listmodel = new ListDataModel(getNgLists().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(getNgLists());
                    }
                    return listmodel;
                }
            };
        }
        return paginationHelper;
    }

    private boolean checkDefectname() {
        if (defectname != null && !defectname.trim().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkProcesstype() {
        if (processtype != null && !processtype.trim().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkProcessname() {
        if (processname != null && !processname.trim().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkBarcode() {
        if (barcodeinput != null && !barcodeinput.trim().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public String getBarcodeinput() {
        return barcodeinput;
    }

    public void setBarcodeinput(String barcodeinput) {
        this.barcodeinput = barcodeinput;
    }

    public String getDefectname() {
        return defectname;
    }

    public void setDefectname(String defectname) {
        this.defectname = defectname;
    }

    public String getProcessname() {
        return processname;
    }

    public void setProcessname(String processname) {
        this.processname = processname;
    }

    public String getProcesstype() {
        return processtype;
    }

    public void setProcesstype(String processtype) {
        this.processtype = processtype;
    }

    public Date getStartdate() {
        if (startdate == null) {
            startdate = new Date();
        }
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getTodate() {
        if (todate == null) {
            todate = new Date();
        }
        return todate;
    }

    public void setTodate(Date todate) {
        this.todate = todate;
    }

    public List<NgList> getNgLists() {
        if (ngLists == null) {
            ngLists = new ArrayList<NgList>();
        }
        return ngLists;
    }
    private CurrentProcessJpaController currentProcessJpaController;

    private CurrentProcessJpaController getCurrentProcessJpaController() {
        if (currentProcessJpaController == null) {
            currentProcessJpaController = new CurrentProcessJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return currentProcessJpaController;
    }
    private ProcessPoolJpaController processPoolJpaController;

    private ProcessPoolJpaController getProcessPoolJpaController() {
        if (processPoolJpaController == null) {
            processPoolJpaController = new ProcessPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return processPoolJpaController;
    }
    private MainDataJpaController mainDataJpaController;

    private MainDataJpaController getMainDataJpaController() {
        if (mainDataJpaController == null) {
            mainDataJpaController = new MainDataJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mainDataJpaController;
    }
    private LotControlJpaController lotControlJpaController;

    private LotControlJpaController getLotControlJpaController() {
        if (lotControlJpaController == null) {
            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotControlJpaController;
    }
    private NgNormalJpaController ngNormalJpaController;

    private NgNormalJpaController getNgNormalJpaController() {
        if (ngNormalJpaController == null) {
            ngNormalJpaController = new NgNormalJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return ngNormalJpaController;
    }
    private NgRepairJpaController ngRepairJpaController;

    private NgRepairJpaController getNgRepairJpaController() {
        if (ngRepairJpaController == null) {
            ngRepairJpaController = new NgRepairJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return ngRepairJpaController;
    }

    public boolean isNgnormal() {
        return ngnormal;
    }

    public void setNgnormal(boolean ngnormal) {
        this.ngnormal = ngnormal;
    }

    public boolean isNgrepair() {
        return ngrepair;
    }

    public void setNgrepair(boolean ngrepair) {
        this.ngrepair = ngrepair;
    }

    public class NgList {

        private String date;
        private String barcode;
        private String process;
        private String processtype;
        private String defectname;
        private String qty;
        // new 
        private String series;
        private String model;
        private String stamp;
        private String input;
        private String output;
        private String percenng;
        private String percenrepair;
        private String percenyield;

        public NgList(String date, String barcode, String process, String processtype, String defectname, String qty,
                String series, String model, String stamp, String input, String output, String percenng, String percenrepair,
                String percenyield) {
            this.date = date;
            this.barcode = barcode;
            this.process = process;
            this.processtype = processtype;
            this.defectname = defectname;
            this.qty = qty;
            // new 
            this.series = series;
            this.model = model;
            this.stamp = stamp;
            this.input = input;
            this.output = output;
            this.percenng = percenng;
            this.percenrepair = percenrepair;
            this.percenyield = percenyield;
        }

        public String getBarcode() {
            return barcode;
        }

        public void setBarcode(String barcode) {
            this.barcode = barcode;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getDefectname() {
            return defectname;
        }

        public void setDefectname(String defectname) {
            this.defectname = defectname;
        }

        public String getProcess() {
            return process;
        }

        public void setProcess(String process) {
            this.process = process;
        }

        public String getProcesstype() {
            return processtype;
        }

        public void setProcesstype(String processtype) {
            this.processtype = processtype;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }
        // new 

        public String getInput() {
            return input;
        }

        public void setInput(String input) {
            this.input = input;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getOutput() {
            return output;
        }

        public void setOutput(String output) {
            this.output = output;
        }

        public String getPercenng() {
            return percenng;
        }

        public void setPercenng(String percenng) {
            this.percenng = percenng;
        }

        public String getPercenrepair() {
            return percenrepair;
        }

        public void setPercenrepair(String percenrepair) {
            this.percenrepair = percenrepair;
        }

        public String getPercenyield() {
            return percenyield;
        }

        public void setPercenyield(String percenyield) {
            this.percenyield = percenyield;
        }

        public String getSeries() {
            return series;
        }

        public void setSeries(String series) {
            this.series = series;
        }

        public String getStamp() {
            return stamp;
        }

        public void setStamp(String stamp) {
            this.stamp = stamp;
        }
    }
}
