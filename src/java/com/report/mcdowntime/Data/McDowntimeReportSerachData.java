/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.report.mcdowntime.Data;

import java.util.Date;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Nov 8, 2012, Time : 2:05:00 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class McDowntimeReportSerachData {
    private String process;
    private String mcName;
    private String line;
    private Date startFromDate;
    private Date toDate;
    private String downTimeReason;

    public McDowntimeReportSerachData(String process, String mcName, String line, Date startFromDate, Date toDate, String downTimeReason) {
        this.process = process;
        this.mcName = mcName;
        this.line = line;
        this.startFromDate = startFromDate;
        this.toDate = toDate;
        this.downTimeReason = downTimeReason;
    } 
    public String getDownTimeReason() {
        return downTimeReason;
    }

    public void setDownTimeReason(String downTimeReason) {
        this.downTimeReason = downTimeReason;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getMcName() {
        return mcName;
    }

    public void setMcName(String mcName) {
        this.mcName = mcName;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public Date getStartFromDate() {
        return startFromDate;
    }

    public void setStartFromDate(Date startFromDate) {
        this.startFromDate = startFromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    } 
}
