/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.report.mcdowntime.Data;

import java.util.Date;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Nov 8, 2012, Time : 11:40:28 AM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class McDowntimeReportData {
    private String dated;
    private String mcName;
    private String process;
    private String line; 
    private String planOperation;
    private String startDownTime;
    private String stopDowntime;
    private String totalDownTime;
    private String operationTime;
    private String downtimeReason;
    private String capa;
    private String totalSummary;

    public McDowntimeReportData(String dated, String mcName, String process, String line, String planOperation, String startDownTime, String stopDowntime, String totalDownTime, String operationTime, String downtimeReason, String capa, String totalSummary) {
        this.dated = dated;
        this.mcName = mcName;
        this.process = process;
        this.line = line;
        this.planOperation = planOperation;
        this.startDownTime = startDownTime;
        this.stopDowntime = stopDowntime;
        this.totalDownTime = totalDownTime;
        this.operationTime = operationTime;
        this.downtimeReason = downtimeReason;
        this.capa = capa;
        this.totalSummary = totalSummary;
    } 
    public String getCapa() {
        return capa;
    }

    public void setCapa(String capa) {
        this.capa = capa;
    }

    public String getDated() {
        return dated;
    }

    public void setDated(String dated) {
        this.dated = dated;
    }

    public String getDowntimeReason() {
        return downtimeReason;
    }

    public void setDowntimeReason(String downtimeReason) {
        this.downtimeReason = downtimeReason;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getMcName() {
        return mcName;
    }

    public void setMcName(String mcName) {
        this.mcName = mcName;
    }

    public String getOperationTime() {
        return operationTime;
    }

    public void setOperationTime(String operationTime) {
        this.operationTime = operationTime;
    }

    public String getPlanOperation() {
        return planOperation;
    }

    public void setPlanOperation(String planOperation) {
        this.planOperation = planOperation;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getStartDownTime() {
        return startDownTime;
    }

    public void setStartDownTime(String startDownTime) {
        this.startDownTime = startDownTime;
    }

    public String getStopDowntime() {
        return stopDowntime;
    }

    public void setStopDowntime(String stopDowntime) {
        this.stopDowntime = stopDowntime;
    }

    public String getTotalDownTime() {
        return totalDownTime;
    }

    public void setTotalDownTime(String totalDownTime) {
        this.totalDownTime = totalDownTime;
    }

    public String getTotalSummary() {
        return totalSummary;
    }

    public void setTotalSummary(String totalSummary) {
        this.totalSummary = totalSummary;
    } 
}
