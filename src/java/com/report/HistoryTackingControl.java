/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.report;

import com.appCinfigpage.bean.fc;
import com.appCinfigpage.bean.userLogin;
import com.tct.data.*;
import com.tct.data.jpa.*;
import com.tct.data.jsf.util.PaginationHelper;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;

/**
 *
 * @author Tawat Saekue
 */
public class HistoryTackingControl {

    //  Search 
    private String barcodeinput;
    private String stampp;
    private String lineinput;
    private String machine;
    private String ordernumber;
    private Date date;
    private Date enddate;
    // Variable
    private List<HistoryTackDataTable> historyTackDataTables;
    private DataModel<HistoryTackDataTable> datatable;

    public HistoryTackingControl() {
    }

    public void searchHistoryTacking() {
        historyTackDataTables = null;
        fc.print("========== search history tacking ============================");
        try {
            if (checkBarcodeinput()) {
//                searchByBarcodeInput();
                searchByLot();
            } else if (checkStampp()) {
                searchByStampp();
            } else if (checkLineinput()) {
                searchByLineInput();
            } else if (checkMachine()) {
                searchByMachine();
            } else if (checkOrdernumber()) {
//                searchByOrderNumber();
                searchByOrder();
            } else if (checkDate()) {
                searchByDate();
            }
            setDatatable();
        } catch (Exception e) {
            fc.print("Error search history tacking : " + e);
        }
    }

    private void loopLot(Collection<Front2backMaster> front2backMasters) {
        fc.print("===== looplot ================================================");
        try {
            for (Front2backMaster front2backMaster : front2backMasters) {
                LotControl lotControl1 = front2backMaster.getLotcontrolId();
                Details details = lotControl1.getDetailsIdDetail();
                fc.print("Test Lot front2back2 : " + lotControl1.getBarcode());
                setDataByLot(lotControl1);
                Collection<Front2backMaster> front2backMasters1 = details.getFront2backMasterCollection();
                loopLot(front2backMasters1);
            }
        } catch (Exception e) {
            fc.print("Error : " + e);
        }
    }

    private void setDataByLot(LotControl lotControl) {
        ModelPool modelPool = lotControl.getModelPoolId();
        Collection<MainData> mainDatas = lotControl.getMainDataCollection();
        if (!mainDatas.isEmpty()) {
            for (MainData mainData : mainDatas) {
                fc.print("Maindata : " + mainData.getId());
                ProcessPool processPool = mainData.getIdProc();
                Collection<CurrentProcess> currentProcesses = mainData.getCurrentProcessCollection();
                if (!currentProcesses.isEmpty()) {
                    for (CurrentProcess currentProcess : currentProcesses) {
                        fc.print("CurrentProcess : " + currentProcess.getId() + " :: Status : " + currentProcess.getStatus());
                        if (currentProcess.getId().equals(mainData.getCurrentNumber())) {
                            getHistoryTackDataTables().add(new HistoryTackDataTable(getSimpleDate("dd/MM/yyyy HH:mm:ss", currentProcess.getTimeCurrent()), lotControl.getBarcode(), lotControl.getId(), modelPool.getModelSeries(), modelPool.getModelGroup() + "-" + modelPool.getModelSeries() + "-" + modelPool.getModelName(), "stamppp", processPool.getProcName(), processPool.getType(), currentProcess.getStatus()));
                        }
                    }
                } else {
                    getHistoryTackDataTables().add(new HistoryTackDataTable(getSimpleDate("dd/MM/yyyy HH:mm:ss", new Date()), lotControl.getBarcode(), lotControl.getId(), modelPool.getModelSeries(), modelPool.getModelGroup() + "-" + modelPool.getModelSeries() + "-" + modelPool.getModelName(), "stamppp", processPool.getProcName(), processPool.getType(), ""));
                }
            }
        } else {
            Collection<Md2pc> md2pcs = modelPool.getMd2pcCollection();
            for (Md2pc md2pc : md2pcs) {
                ProcessPool processPool = md2pc.getProcessPoolIdProc();
                getHistoryTackDataTables().add(new HistoryTackDataTable(getSimpleDate("dd/MM/yyyy HH:mm:ss", new Date()), lotControl.getBarcode(), lotControl.getId(), modelPool.getModelSeries(), modelPool.getModelGroup() + "-" + modelPool.getModelSeries() + "-" + modelPool.getModelName(), "stamppp", processPool.getProcName(), processPool.getType(), ""));
            }
        }
    }
    // search by barcodeinput new <<<<<

    private void searchByLot() {
        fc.print("========= search by lot : " + barcodeinput + "================");
        try {
            List<LotControl> lotControls = getLotControlJpaController().findLotcontrolByBarcode(barcodeinput);
            for (LotControl lotControl : lotControls) {
                Details details = lotControl.getDetailsIdDetail();
                fc.print("Test Lot front2back1 : " + lotControl.getBarcode());
                setDataByLot(lotControl);
                Collection<Front2backMaster> front2backMasters = details.getFront2backMasterCollection();
                fc.print("Front2Back size : " + front2backMasters.size());
                loopLot(front2backMasters);
            }
        } catch (Exception e) {
            fc.print("Error search by lot : " + e);
        }
    }

    private void searchByStampp() {
        fc.print("========= search by Stampp : " + stampp + " ==================");
        try {
        } catch (Exception e) {
            fc.print("Error search by Stampp : " + e);
        }
    }

    public void actionLink() {
        String lotcontrolid = getDatatable().getRowData().getLinkbarcode();
        fc.print("======== action link : " + lotcontrolid + "========================");
        try {
            String link = "";
            LotControl lotControl = getLotControlJpaController().findLotControl(lotcontrolid);
            Details details = lotControl.getDetailsIdDetail();
//            if(details.getd){
//                
//            }
            fc.print("Detail view : " + lotcontrolid);
//            userLogin.getSessionMenuBarBean().setParam(link);

        } catch (Exception e) {
            fc.print("Error action link : " + e);
        }
    }

    private void searchByDate() {
        fc.print("========= search by date : " + date + "=======================");
        try {
            String dates = getSimpleDate("dd/MM/yyyy", date);
            String enddates = getSimpleDate("dd/MM/yyyy", enddate);
            Date fdate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(dates + " 00:00:00");
            Date ldate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(enddates + " 23:59:59");
            List<CurrentProcess> currentProcesses = getCurrentProcessJpaController().findCurrenntProcessByFristtimeAndLasttime(fdate, ldate);
            for (CurrentProcess currentProcess : currentProcesses) {
                MainData mainData = currentProcess.getMainDataId();
                LotControl lotControl = mainData.getLotControlId();
                ModelPool modelPool = lotControl.getModelPoolId();
                ProcessPool processPool = mainData.getIdProc();
                fc.print("CurrentProcess : " + currentProcess.getCurrentProc());
                getHistoryTackDataTables().add(new HistoryTackDataTable(getSimpleDate("dd/MM/yyyy HH:mm:ss", currentProcess.getTimeCurrent()), lotControl.getBarcode(), lotControl.getId(), modelPool.getModelSeries(), modelPool.getModelGroup() + "-" + modelPool.getModelSeries() + "-" + modelPool.getModelName(), "stamppp", processPool.getProcName(), processPool.getType(), currentProcess.getStatus()));
            }
        } catch (Exception e) {
            fc.print("Error search by date : " + e);
        }
    }
// search by ordernumber new <<<<<

    private void searchByOrder() {
        fc.print("========= search by otder : " + ordernumber + "===============");
        try {
            List<Details> detailses = getDetailsJpaController().findDetailsByOrdernumber(ordernumber);
            for (Details details : detailses) {
                Collection<LotControl> lotControls = details.getLotControlCollection();
                for (LotControl lotControl : lotControls) {
                    setDataByLot(lotControl);
                }
                Collection<Front2backMaster> front2backMasters = details.getFront2backMasterCollection();
                loopLot(front2backMasters);
            }
        } catch (Exception e) {
            fc.print("Error search by order : " + e);
        }
    }
// search by ordernumber frist <<<<<

    private void searchByOrderNumber() {
        fc.print("========= search by order number : " + ordernumber + "=========");
        try {
            List<Details> detailses = getDetailsJpaController().findDetailsByOrdernumber(ordernumber);
            for (Details details : detailses) {
                Collection<LotControl> lotControls = details.getLotControlCollection();
                for (LotControl lotControl : lotControls) {
                    fc.print("Lotcontrol : " + lotControl.getBarcode());
                    ModelPool modelPool = lotControl.getModelPoolId();
                    Collection<MainData> mainDatas = lotControl.getMainDataCollection();
                    if (!mainDatas.isEmpty()) {
                        for (MainData mainData : mainDatas) {
                            fc.print("Maindata : " + mainData.getId());
                            ProcessPool processPool = mainData.getIdProc();
                            Collection<CurrentProcess> currentProcesses = mainData.getCurrentProcessCollection();
                            if (!currentProcesses.isEmpty()) {
                                for (CurrentProcess currentProcess : currentProcesses) {
                                    fc.print("CurrentProcess : " + currentProcess.getId() + " :: Status : " + currentProcess.getStatus());
                                    if (currentProcess.getId().equals(mainData.getCurrentNumber())) {
                                        getHistoryTackDataTables().add(new HistoryTackDataTable(getSimpleDate("dd/MM/yyyy HH:mm:ss", currentProcess.getTimeCurrent()), lotControl.getBarcode(), lotControl.getId(), modelPool.getModelSeries(), modelPool.getModelGroup() + "-" + modelPool.getModelSeries() + "-" + modelPool.getModelName(), "stamppp", processPool.getProcName(), processPool.getType(), currentProcess.getStatus()));
                                    }
                                }
                            } else {
                                getHistoryTackDataTables().add(new HistoryTackDataTable(getSimpleDate("dd/MM/yyyy HH:mm:ss", new Date()), lotControl.getBarcode(), lotControl.getId(), modelPool.getModelSeries(), modelPool.getModelGroup() + "-" + modelPool.getModelSeries() + "-" + modelPool.getModelName(), "stamppp", processPool.getProcName(), processPool.getType(), ""));
                            }
                        }
                    } else {
                        Collection<Md2pc> md2pcs = modelPool.getMd2pcCollection();
                        for (Md2pc md2pc : md2pcs) {
                            ProcessPool processPool = md2pc.getProcessPoolIdProc();
                            getHistoryTackDataTables().add(new HistoryTackDataTable(getSimpleDate("dd/MM/yyyy HH:mm:ss", new Date()), lotControl.getBarcode(), lotControl.getId(), modelPool.getModelSeries(), modelPool.getModelGroup() + "-" + modelPool.getModelSeries() + "-" + modelPool.getModelName(), "stamppp", processPool.getProcName(), processPool.getType(), ""));
                        }
                    }
                }
            }
        } catch (Exception e) {
            fc.print("Error search by order number : " + e);
        }
    }

    private void searchByMachine() {
        fc.print("========= search by machine : " + machine + "=================");
        try {
            Mc mc = getMcJpaController().findMc(machine);
            List<CurrentProcess> currentProcesses = getCurrentProcessJpaController().findCurrentProcessByLine(mc);
            for (CurrentProcess currentProcess : currentProcesses) {
                MainData mainData = currentProcess.getMainDataId();
                LotControl lotControl = mainData.getLotControlId();
                ModelPool modelPool = lotControl.getModelPoolId();
                ProcessPool processPool = mainData.getIdProc();
                fc.print("CurrentProcess : " + currentProcess.getCurrentProc());
                getHistoryTackDataTables().add(new HistoryTackDataTable(getSimpleDate("dd/MM/yyyy HH:mm:ss", currentProcess.getTimeCurrent()), lotControl.getBarcode(), lotControl.getId(), modelPool.getModelSeries(), modelPool.getModelGroup() + "-" + modelPool.getModelSeries() + "-" + modelPool.getModelName(), "stamppp", processPool.getProcName(), processPool.getType(), currentProcess.getStatus()));
            }
        } catch (Exception e) {
            fc.print("Error search by machine : " + e);
        }
    }

    private void searchByLineInput() {
        fc.print("========= search by line : " + lineinput + " =================");
        try {
//            Line lines = getLineJpaController().findLine(lineinput);
            List<CurrentProcess> currentProcesses = getCurrentProcessJpaController().findCurrrentProcessByLine(lineinput);
            for (CurrentProcess currentProcess : currentProcesses) {
                MainData mainData = currentProcess.getMainDataId();
                LotControl lotControl = mainData.getLotControlId();
                ModelPool modelPool = lotControl.getModelPoolId();
                ProcessPool processPool = mainData.getIdProc();
                fc.print("CurrentProcess : " + currentProcess.getCurrentProc());
                getHistoryTackDataTables().add(new HistoryTackDataTable(getSimpleDate("dd/MM/yyyy HH:mm:ss", currentProcess.getTimeCurrent()), lotControl.getBarcode(), lotControl.getId(), modelPool.getModelSeries(), modelPool.getModelGroup() + "-" + modelPool.getModelSeries() + "-" + modelPool.getModelName(), "stamppp", processPool.getProcName(), processPool.getType(), currentProcess.getStatus()));
            }
        } catch (Exception e) {
            fc.print("Error search by line : " + e);
        }
    }
// search by barcode input frist <<<<<

    private void searchByBarcodeInput() {
        fc.print("========= search by barcode input " + barcodeinput + " =======");
        try {
            List<LotControl> lotControls = getLotControlJpaController().findLotcontrolByBarcode(barcodeinput);
            for (LotControl lotControl : lotControls) {
                fc.print("Lotcontrol : " + lotControl.getBarcode());
                ModelPool modelPool = lotControl.getModelPoolId();
                Collection<MainData> mainDatas = lotControl.getMainDataCollection();
                if (!mainDatas.isEmpty()) {
                    for (MainData mainData : mainDatas) {
                        fc.print("Maindata : " + mainData.getId());
                        ProcessPool processPool = mainData.getIdProc();
                        Collection<CurrentProcess> currentProcesses = mainData.getCurrentProcessCollection();
                        if (!currentProcesses.isEmpty()) {
                            for (CurrentProcess currentProcess : currentProcesses) {
                                fc.print("CurrentProcess : " + currentProcess.getId() + " :: Status : " + currentProcess.getStatus());
                                if (currentProcess.getId().equals(mainData.getCurrentNumber())) {
                                    getHistoryTackDataTables().add(new HistoryTackDataTable(getSimpleDate("dd/MM/yyyy HH:mm:ss", currentProcess.getTimeCurrent()), lotControl.getBarcode(), lotControl.getId(), modelPool.getModelSeries(), modelPool.getModelGroup() + "-" + modelPool.getModelSeries() + "-" + modelPool.getModelName(), "stamppp", processPool.getProcName(), processPool.getType(), currentProcess.getStatus()));
                                }
                            }
                        } else {
                            getHistoryTackDataTables().add(new HistoryTackDataTable(getSimpleDate("dd/MM/yyyy HH:mm:ss", new Date()), lotControl.getBarcode(), lotControl.getId(), modelPool.getModelSeries(), modelPool.getModelGroup() + "-" + modelPool.getModelSeries() + "-" + modelPool.getModelName(), "stamppp", processPool.getProcName(), processPool.getType(), ""));
                        }
                    }
                } else {
                    Collection<Md2pc> md2pcs = modelPool.getMd2pcCollection();
                    for (Md2pc md2pc : md2pcs) {
                        ProcessPool processPool = md2pc.getProcessPoolIdProc();
                        getHistoryTackDataTables().add(new HistoryTackDataTable(getSimpleDate("dd/MM/yyyy HH:mm:ss", new Date()), lotControl.getBarcode(), lotControl.getId(), modelPool.getModelSeries(), modelPool.getModelGroup() + "-" + modelPool.getModelSeries() + "-" + modelPool.getModelName(), "stamppp", processPool.getProcName(), processPool.getType(), ""));
                    }
                }
            }
        } catch (Exception e) {
            fc.print("Error search by barcode input : " + e);
        }
    }

    private String getSimpleDate(String patern, Date date) {
        return new SimpleDateFormat(patern).format(date);
    }

    private boolean checkDate() {
        if (date != null) {
            return true;
        }
        return false;
    }

    private boolean checkOrdernumber() {
        if (ordernumber != null && !ordernumber.isEmpty()) {
            return true;
        }
        return false;
    }

    private boolean checkMachine() {
        if (machine != null && !machine.isEmpty()) {
            return true;
        }
        return false;
    }

    private boolean checkLineinput() {
        if (lineinput != null && !lineinput.isEmpty()) {
            return true;
        }
        return false;
    }

    private boolean checkStampp() {
        if (stampp != null && !stampp.isEmpty()) {
            return true;
        }
        return false;
    }

    private boolean checkBarcodeinput() {
        if (barcodeinput != null && !barcodeinput.isEmpty()) {
            return true;
        }
        return false;
    }

    public void previous() {
        getPaginationHelper().previousPage();
        setDatatable();
    }

    public void next() {
        getPaginationHelper().nextPage();
        setDatatable();
    }

    public int getAllPages() {
        int all = getPaginationHelper().getItemsCount();
        int pagesize = getPaginationHelper().getPageSize();
        return ((int) all / pagesize) + 1;
    }
    private PaginationHelper paginationHelper;

    public PaginationHelper getPaginationHelper() {
        if (paginationHelper == null) {
            paginationHelper = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    return getHistoryTackDataTables().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    ListDataModel<HistoryTackDataTable> dataModel;
                    if (getHistoryTackDataTables().size() > 0) {
                        dataModel = new ListDataModel<HistoryTackDataTable>(getHistoryTackDataTables().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        dataModel = new ListDataModel<HistoryTackDataTable>(getHistoryTackDataTables());
                    }
                    return dataModel;
                }
            };
        }
        return paginationHelper;
    }

    public List<SelectItem> getLinelist() {
        fc.print("=========== get line list ====================================");
        try {
            List<SelectItem> linelist = new ArrayList<SelectItem>();
            List<Line> lines = getLineJpaController().findAllLineNotDeletd();
            for (Line line : lines) {
                linelist.add(new SelectItem(line.getLineId(), line.getLineName()));
            }
            return linelist;
        } catch (Exception e) {
            fc.print("Error get line : " + e);
            return null;
        }
    }

    public List<SelectItem> getMachinelist() {
        fc.print("=========== get machine list =================================");
        try {
            List<SelectItem> machinelist = new ArrayList<SelectItem>();
            List<Mc> mcs = getMcJpaController().findAllMcNotDelete();
            for (Mc mc : mcs) {
                machinelist.add(new SelectItem(mc.getId(), mc.getMcName()));
            }
            return machinelist;
        } catch (Exception e) {
            fc.print("Error get Machine : " + e);
            return null;
        }
    }
    private DetailsJpaController detailsJpaController;

    private DetailsJpaController getDetailsJpaController() {
        if (detailsJpaController == null) {
            detailsJpaController = new DetailsJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailsJpaController;
    }
    private CurrentProcessJpaController currentProcessJpaController;

    private CurrentProcessJpaController getCurrentProcessJpaController() {
        if (currentProcessJpaController == null) {
            currentProcessJpaController = new CurrentProcessJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return currentProcessJpaController;
    }
    private ProcessPoolJpaController processPoolJpaController;

    private ProcessPoolJpaController getProcessPoolJpaController() {
        if (processPoolJpaController == null) {
            processPoolJpaController = new ProcessPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return processPoolJpaController;
    }
    private LotControlJpaController lotControlJpaController;

    private LotControlJpaController getLotControlJpaController() {
        if (lotControlJpaController == null) {
            lotControlJpaController = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotControlJpaController;
    }
    private LineJpaController lineJpaController;

    private LineJpaController getLineJpaController() {
        if (lineJpaController == null) {
            lineJpaController = new LineJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lineJpaController;
    }
    private McJpaController mcJpaController;

    private McJpaController getMcJpaController() {
        if (mcJpaController == null) {
            mcJpaController = new McJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mcJpaController;
    }

    public DataModel<HistoryTackDataTable> getDatatable() {
        return datatable;
    }

    public void setDatatable() {
        this.datatable = getPaginationHelper().createPageDataModel();
    }

    public List<HistoryTackDataTable> getHistoryTackDataTables() {
        if (historyTackDataTables == null) {
            historyTackDataTables = new ArrayList<HistoryTackDataTable>();
        }
        return historyTackDataTables;
    }

    public String getBarcodeinput() {
        return barcodeinput;
    }

    public void setBarcodeinput(String barcodeinput) {
        this.barcodeinput = barcodeinput;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }
     
    public String getLineinput() {
        return lineinput;
    }

    public void setLineinput(String lineinput) {
        this.lineinput = lineinput;
    }

    public String getMachine() {
        return machine;
    }

    public void setMachine(String machine) {
        this.machine = machine;
    }

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    public String getStampp() {
        return stampp;
    }

    public void setStampp(String stampp) {
        this.stampp = stampp;
    }
}
