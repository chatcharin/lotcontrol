/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.report.historydowntime.Data;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Nov 9, 2012, Time : 1:39:25 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class HistoryDowntimeItemData {
    private String date;
    private String process;
    private String line;
    private String status;
    private String starttime;
    private String stoptime;
    private String reason;
    private String total;

    public HistoryDowntimeItemData(String date, String process, String line, String status, String starttime, String stoptime, String reason, String total) {
        this.date = date;
        this.process = process;
        this.line = line;
        this.status = status;
        this.starttime = starttime;
        this.stoptime = stoptime;
        this.reason = reason;
        this.total = total;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getStoptime() {
        return stoptime;
    }

    public void setStoptime(String stoptime) {
        this.stoptime = stoptime;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    } 
}
