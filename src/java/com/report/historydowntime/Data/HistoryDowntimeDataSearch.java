/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.report.historydowntime.Data;

import java.util.Date;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Nov 9, 2012, Time : 1:05:53 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class HistoryDowntimeDataSearch {
    private String process;
    private String line;
    private Date startdate;
    private Date todatdate;
    private String status;

    public HistoryDowntimeDataSearch(String process, String line, Date startdate, Date todatdate, String status) {
        this.process = process;
        this.line = line;
        this.startdate = startdate;
        this.todatdate = todatdate;
        this.status = status;
    } 
    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getTodatdate() {
        return todatdate;
    }

    public void setTodatdate(Date todatdate) {
        this.todatdate = todatdate;
    }
    
}
