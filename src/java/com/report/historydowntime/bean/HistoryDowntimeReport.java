/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.report.historydowntime.bean;

import com.appCinfigpage.bean.fc;
import com.mysql.jdbc.Statement;
import com.report.historydowntime.Data.HistoryDowntimeDataSearch;
import com.report.historydowntime.Data.HistoryDowntimeItemData;
import com.report.mcdowntime.Data.McDowntimeReportData;
import com.sql.bean.Sql;
import com.tct.data.Line;
import com.tct.data.ProcessPool;
import com.tct.data.jpa.LineJpaController;
import com.tct.data.jpa.ProcessPoolJpaController;
import com.tct.data.jsf.util.PaginationHelper;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.faces.model.ArrayDataModel;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;

/**
 *
 * Machine User
 *
 * @author pang Development by Chusak laojang Date : Nov 9, 2012, Time : 1:04:50
 * PM Copy Right 4 Xtreme Co.,Ltd.
 */
public class HistoryDowntimeReport {

    private HistoryDowntimeDataSearch currentHistoryDowntimeDataSearch;
    private ProcessPoolJpaController processPoolJpaController = null;
    private LineJpaController lineJpaController = null;
    private DataModel hostoryModel;
    private List<HistoryDowntimeItemData> itemListHistory;

    private LineJpaController getLineJpaController() {
        if (lineJpaController == null) {
            lineJpaController = new LineJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lineJpaController;
    }

    private ProcessPoolJpaController getProcessPoolJpaController() {
        if (processPoolJpaController == null) {
            processPoolJpaController = new ProcessPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return processPoolJpaController;
    }

    public void changeToSearch() throws SQLException {
        fc.print("***** change to search *****");
        prepareData();
        findData();
        resetSearch();
    }

    public void resetSearch() {
        currentHistoryDowntimeDataSearch = new HistoryDowntimeDataSearch("null", "null", null, null, "null");
    }
    private Date dately;
    private Date weekly = null;
    private Date monthly = null;
    private Date yearly = null;

    private String getDateFormat(String pattern, Date date) {
        return new SimpleDateFormat(pattern).format(date);
    }

    public String getFillter() {
        String fillter = "";
        if (!currentHistoryDowntimeDataSearch.getProcess().equals("null")) {
            fillter += " and process_pool.`id_proc` >='" + currentHistoryDowntimeDataSearch.getProcess() + "' ";
        }
        if (!currentHistoryDowntimeDataSearch.getLine().equals("null")) {
            fillter += " and line.`line_id` ='" + currentHistoryDowntimeDataSearch.getLine() + "' ";
        }
        if (!currentHistoryDowntimeDataSearch.getStatus().equals("null")) {
            fc.print("******* status *****"+currentHistoryDowntimeDataSearch.getStatus());
            fillter += " and statusAll='" + currentHistoryDowntimeDataSearch.getStatus() + "' ";
        }
        if (currentHistoryDowntimeDataSearch.getStartdate() != null) {
            String startdate2 = getDateFormat("yyyy-MM-dd", currentHistoryDowntimeDataSearch.getStartdate()) + " 00:00:00";
            fillter += " and current_process.`time_current` >='" + startdate2 + "' ";
        }
        if (currentHistoryDowntimeDataSearch.getTodatdate() != null) {
            String enddate2 = getDateFormat("yyyy-MM-dd", currentHistoryDowntimeDataSearch.getTodatdate()) + " 00:00:00";
            fillter += " and current_process.`time_current` <='" + enddate2 + "' ";
        }
        if (dately != null) {
            String datelyStr = getDateFormat("yyyy-MM-dd", dately) + " 00:00:00";
            fillter += " and current_process.`time_current` >='" + datelyStr + "' ";
        }
        if (weekly != null) {
            String datelyStr = getDateFormat("yyyy-MM-dd", weekly) + " 00:00:00";
            fillter += " and WEEK(current_process.time_current) = WEEK('" + datelyStr + "') ";
            fillter += " and MONTH(current_process.time_current) = MONTH('" + datelyStr + "')";
            fillter += " and YEAR(current_process.time_current) = YEAR('" + datelyStr + "')";
        }
        if (monthly != null) {
            String monthlyStr = getDateFormat("yyyy-MM-dd", monthly) + " 00:00:00";
            fillter += " and MONTH(current_process.time_current) = MONTH('" + monthlyStr + "') ";
            fillter += " and YEAR(current_process.time_current) = YEAR('" + monthlyStr + "')";
        }
        if (yearly != null) {
            String yearlyStr = getDateFormat("yyyy-MM-dd", yearly) + " 00:00:00";
            fillter += " and YEAR(current_process.time_current) = YEAR('" + yearlyStr + "')";
        }
        return fillter;
    }

    public void searchByDaily() throws SQLException {
        prepareData();
        dately = new Date();
        findData();
        resetSearch();
    }

    public void searchWeekly() throws SQLException {
        prepareData();
        weekly = new Date();
        findData();
        resetSearch();
    }

    public void searchMonthly() throws SQLException {
        prepareData();
        monthly = new Date();
        findData();
        resetSearch();
    }

    public void searchYearly() throws SQLException {
        prepareData();
        yearly = new Date();
        findData();
        resetSearch();
    }

    public void prepareData() {
        itemListHistory = new ArrayList<HistoryDowntimeItemData>();
        worksheetpagination = null;
        hostoryModel = new ArrayDataModel();
        dately = null;
        weekly = null;
        monthly = null;
        yearly = null;
    }

    public void findData() throws SQLException {
        fc.print("sql: " + Sql.generateHistoryDowntime(getFillter()));
        com.mysql.jdbc.Connection conn = Sql.getConnection("mysql"); //connection SQL
        Statement stm = (Statement) conn.createStatement();
        if (stm.execute(Sql.generateHistoryDowntime(getFillter()))) {
            ResultSet result = stm.executeQuery(Sql.generateHistoryDowntime(getFillter()));
            while (result.next()) {
                getItemListHistory().add(new HistoryDowntimeItemData(result.getObject(1).toString(), result.getObject(5).toString(), result.getObject(8).toString(), result.getObject(4).toString(), result.getObject(2).toString(), result.getObject(7).toString(), result.getObject(6).toString(), result.getObject(3).toString()));
            }
            setHostoryModel();
        }
    }

    public List<SelectItem> itemProcess() {
        List<SelectItem> itemProcess = new ArrayList<SelectItem>();
        itemProcess.add(new SelectItem("null", "---- Select one ----"));
        List<ProcessPool> objProcess = getProcessPoolJpaController().findProcessPoolByNameNotDeteltedAsc();
        if (!objProcess.isEmpty()) {
            for (ProcessPool objList : objProcess) {
                itemProcess.add(new SelectItem(objList.getIdProc(), objList.getProcName()));
            }
        }

        return itemProcess;
    }

    public List<SelectItem> itemStatus() {
        List<SelectItem> itemStaus = new ArrayList<SelectItem>();
        itemStaus.add(new SelectItem("null", "---- Select one ----"));
        itemStaus.add(new SelectItem("Downtime", "Downtime"));
        itemStaus.add(new SelectItem("Holdtime", "Holdtime"));
        return itemStaus;
    }

    public HistoryDowntimeDataSearch getCurrentHistoryDowntimeDataSearch() {
        if (currentHistoryDowntimeDataSearch == null) {
            currentHistoryDowntimeDataSearch = new HistoryDowntimeDataSearch("null", "null", null, null, "null");
        }
        return currentHistoryDowntimeDataSearch;
    }

    public List<SelectItem> itemLine() {
        List<SelectItem> itemLine = new ArrayList<SelectItem>();
        itemLine.add(new SelectItem("null", "---- Select one ----"));
        List<Line> objLine = getLineJpaController().findAllLineNotDeletd();
        if (!objLine.isEmpty()) {
            for (Line objList : objLine) {
                itemLine.add(new SelectItem(objList.getLineId(), objList.getLineName()));
            }
        }
        return itemLine;
    }

    public void previous() {
        getWorksheetpagination().previousPage();
        setHostoryModel();
    }

    public void next() {
        getWorksheetpagination().nextPage();
        setHostoryModel();
    }

    public int getAllPages() {
        int all = getWorksheetpagination().getItemsCount();
        int pagesize = getWorksheetpagination().getPageSize();
        return ((int) all / pagesize) + 1;
    }
    private PaginationHelper worksheetpagination;

    public PaginationHelper getWorksheetpagination() {
        if (worksheetpagination == null) {
            worksheetpagination = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    //fc.print("size of data : "+getMasterListViewData().size());
                    return getItemListHistory().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    fc.print("PageFirst : " + getPageFirstItem());
                    fc.print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (getItemListHistory().size() > 0) {
                        listmodel = new ListDataModel(getItemListHistory().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(getItemListHistory());
                    }
                    return listmodel;
                }
            };
        }
        return worksheetpagination;
    }

    public void setCurrentHistoryDowntimeDataSearch(HistoryDowntimeDataSearch currentHistoryDowntimeDataSearch) {
        this.currentHistoryDowntimeDataSearch = currentHistoryDowntimeDataSearch;
    }

    public DataModel getHostoryModel() {
        return hostoryModel;
    }

    public void setHostoryModel() {
        this.hostoryModel = getWorksheetpagination().createPageDataModel();
    }

    public List<HistoryDowntimeItemData> getItemListHistory() {
        if (itemListHistory == null) {
            itemListHistory = new ArrayList<HistoryDowntimeItemData>();
        }
        return itemListHistory;
    }

    public void setItemListHistory(List<HistoryDowntimeItemData> itemListHistory) {
        this.itemListHistory = itemListHistory;
    }

    public Date getDately() {
        return dately;
    }

    public void setDately(Date dately) {
        this.dately = dately;
    }

    public Date getMonthly() {
        return monthly;
    }

    public void setMonthly(Date monthly) {
        this.monthly = monthly;
    }

    public Date getWeekly() {
        return weekly;
    }

    public void setWeekly(Date weekly) {
        this.weekly = weekly;
    }

    public Date getYearly() {
        return yearly;
    }

    public void setYearly(Date yearly) {
        this.yearly = yearly;
    }
}
