/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.report;

import com.tct.data.Mc;
import com.tct.data.ProcessPool;

/**
 *
 * @author Tawat Saekue
 * 
 */
public class OeeListMc {
    private Mc mc;
    private ProcessPool processPool;
    private long totaldowntime;

    public OeeListMc(Mc mc, long totaldowntime) {
        this.mc = mc;
        this.totaldowntime = totaldowntime;
    }
    
    public OeeListMc(ProcessPool processPool, long totaldowntime){
        this.processPool = processPool;
        this.totaldowntime = totaldowntime;
    }

    public Mc getMc() {
        return mc;
    }

    public void setMc(Mc mc) {
        this.mc = mc;
    }

    public long getTotaldowntime() {
        return totaldowntime;
    }

    public void setTotaldowntime(long totaldowntime) {
        this.totaldowntime = totaldowntime;
    }

    public ProcessPool getProcessPool() {
        return processPool;
    }

    public void setProcessPool(ProcessPool processPool) {
        this.processPool = processPool;
    }
    
}
