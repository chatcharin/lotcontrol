/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.report.quality.bean;

import com.appCinfigpage.bean.fc;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import com.report.quality.Data.SearchData;
import com.sql.bean.Sql;
import com.time.timeCurrent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Oct 21, 2012, Time : 10:14:29 AM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class Yield {
    private SearchData    currentSeach;
    private String dataYield = null;
    private String tickYield = null ;
    public void changeToSearch() throws SQLException
    {
        fc.print("===== Change to search chart ==== : start Date :"+currentSeach.getStart().toString()+" End date :"+currentSeach.getEnd().toString());
        String startFromdate  = Sql.formatDateSqlForNg(currentSeach.getStart());
        String toDate  = Sql.formatDateSqlForNg(currentSeach.getEnd());
        String line   = currentSeach.getLine();
        loadYieldChart(startFromdate,toDate,line);
    }
    public void bufferDefaultDataYied() throws SQLException
    { 
        fc.print("****** Load default ng data *******"); 
        String startFromdate = "14/10/2012 11:11:40";
        String toDate  = Sql.formatDateSqlForNg(new timeCurrent().getDate());
        String line  = "00200cad-988f-47da-b233-eb8e40324ff2";
        loadYieldChart(startFromdate,toDate,line);
    }
    //**** loadYield chart
    public void loadYieldChart(String startDate,String stopDate,String line) throws SQLException
    {
        fc.print("****** find chart yield *****");
         String sql = Sql.generateSqlForNg(startDate, stopDate, line);
        fc.print("SQL :" + sql);
        Connection conn = Sql.getConnection("mysql");
        Statement stm = (Statement) conn.createStatement();
        if (stm.execute(sql)) {
            ResultSet result = stm.executeQuery(sql);
            int i = 1;
            dataYield = "[[0,''],";
            tickYield = "[[0,''],";
            while (result.next()) {
                fc.print("Ng name :" + result.getObject(2).toString() + " percent:" + result.getObject(4).toString());  
                dataYield += "[" + i + "," +calYield(result.getObject(4).toString())+ "],";
                tickYield += "[" + i + "," + "'" + result.getObject(2) + "'" + "],";
                i++;
            }
            dataYield += "[]]";
            tickYield += "[,'']]";
        }
        fc.print("dataYield : " + dataYield);
        fc.print("tickYield : " + tickYield);
    }
    public String calYield(String percent)
    {
        double cal = 0; 
        cal   = 100 - Double.parseDouble(percent) ;
        return new DecimalFormat("#,###.##").format(cal);
    }
    public SearchData getCurrentSeach() {
        if(currentSeach == null)
        {
            currentSeach  = new SearchData(null, null,null);
        }
        return currentSeach;
    } 
    public void setCurrentSeach(SearchData currentSeach) {
        this.currentSeach = currentSeach;
    }

    public String getDataYield() {
        return dataYield;
    }

    public void setDataYield(String dataYield) {
        this.dataYield = dataYield;
    }

    public String getTickYield() {
        return tickYield;
    }

    public void setTickYield(String tickYield) {
        this.tickYield = tickYield;
    }
    
}
