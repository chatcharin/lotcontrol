/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.report.quality.bean;

import com.appCinfigpage.bean.fc;
import com.mysql.jdbc.Connection;
import com.report.quality.Data.SeachNgData;
import com.sql.bean.Sql;
import com.tct.data.Line;
import com.tct.data.jpa.LineJpaController;
import com.time.timeCurrent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;

/**
 *
 * Machine User
 *
 * @author pang Development by Chusak laojang Date : Oct 21, 2012, Time :
 * 10:15:08 AM Copy Right 4 Xtreme Co.,Ltd.
 */
public class NgRepair {

    private SeachNgData currentSeach;
    private LineJpaController lineJpaController = null;
    private String dataNgRepair;
    private String ticksNgRepair;

    public LineJpaController getLineJpaController() {
        if (lineJpaController == null) {
            lineJpaController = new LineJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lineJpaController;
    }

    public void changeTosearch() throws SQLException {
        fc.print("====search === : start date : " + getCurrentSeach().getStart() + " End : " + getCurrentSeach().getEnd() + " Line :" + getCurrentSeach().getLine());
        String sql = Sql.generateSqlForNgRepair(Sql.formatDateSqlForNg(currentSeach.getStart()), Sql.formatDateSqlForNg(currentSeach.getEnd()), currentSeach.getLine());
        fc.print("SQL :" + sql);
        Connection conn = Sql.getConnection("mysql");
        Statement stm = conn.createStatement();
        if (stm.execute(sql)) {
            ResultSet result = stm.executeQuery(sql);
            int i = 1;
            dataNgRepair = "[";
            ticksNgRepair = "[";
            while (result.next()) {
                fc.print("Ng name :" + result.getObject(2).toString() + " percent:" + result.getObject(4).toString());
                dataNgRepair += "[" + i + "," + result.getObject(4) + "],";
                ticksNgRepair += "[" + i + "," + "'" + result.getObject(2) + "'" + "],";
                i++;
            }
            dataNgRepair += "[]]";
            ticksNgRepair += "[,'']]";
        }
        fc.print("dateNG : " + dataNgRepair);
        fc.print("ticks : " + ticksNgRepair);
//        resetDataSearch();
    }
    public void loadDefaultBufferData() throws SQLException {
        fc.print("****** Load default ng data *******");
        String start = "14/10/2012 11:11:40";
        String stop  = Sql.formatDateSqlForNg(new timeCurrent().getDate());
        String line  = "00200cad-988f-47da-b233-eb8e40324ff2"; 
        String sql = Sql.generateSqlForNgRepair(start,stop,line);
        fc.print("SQL :" + sql);
        Connection conn = Sql.getConnection("mysql");
        Statement stm = conn.createStatement();
        if (stm.execute(sql)) {
            ResultSet result = stm.executeQuery(sql);
            int i = 1;
            dataNgRepair = "[";
            ticksNgRepair = "[";
            while (result.next()) {
                fc.print("Ng name :" + result.getObject(2).toString() + " percent:" + result.getObject(4).toString());
                dataNgRepair += "[" + i + "," + result.getObject(4) + "],";
                ticksNgRepair += "[" + i + "," + "'" + result.getObject(2) + "'" + "],";
                i++;
            }
            dataNgRepair += "[]]";
            ticksNgRepair += "[,'']]";
        }
        fc.print("dateNG : " + dataNgRepair);
        fc.print("ticks : " + ticksNgRepair);
        resetDataSearch();
    }
    public void resetDataSearch() {
        currentSeach.setEnd(null);
        currentSeach.setStart(null);
        currentSeach.setLine("null");
    }
//    load lind

    public List<SelectItem> loadLindeItemAll() {
        List<SelectItem> objSlect = new ArrayList<SelectItem>();
        objSlect.add(new SelectItem("null", "---- Select one ----"));
        for (Line objList : getLineJpaController().findAllLineNotDeletd()) {
            objSlect.add(new SelectItem(objList.getLineId(), objList.getLineName()));
        }
        return objSlect;
    }

    public SeachNgData getCurrentSeach() {
        if (currentSeach == null) {
            currentSeach = new SeachNgData(null, null, null);
        }
        return currentSeach;
    }

    public void setCurrentSeach(SeachNgData currentSeach) {
        this.currentSeach = currentSeach;
    }

    public String getDataNg() {
        return dataNgRepair;
    }

    public void setDataNg(String dataNgRepair) {
        this.dataNgRepair = dataNgRepair;
    }

    public String getTicks() {
        return ticksNgRepair;
    }

    public void setTicks(String ticksNgRepair) {
        this.ticksNgRepair = ticksNgRepair;
    }
}
