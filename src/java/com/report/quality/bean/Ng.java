/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.report.quality.bean;

import com.appCinfigpage.bean.fc;
import com.mysql.jdbc.Connection;
import com.report.quality.Data.DetailNgChart;
import com.report.quality.Data.SeachNgData;
import com.sql.bean.Sql;
import java.util.List;
import javax.faces.model.SelectItem;
import com.tct.data.*;
import com.tct.data.jpa.*;
import java.util.ArrayList;
import javax.persistence.Persistence;
import com.sql.bean.Sql;
import com.tct.dashboard.report.NgPoint;
import com.time.timeCurrent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

/**
 *
 * Machine User
 *
 * @author pang Development by Chusak laojang Date : Oct 21, 2012, Time :
 * 10:14:43 AM Copy Right 4 Xtreme Co.,Ltd.
 */
public class Ng {

    private SeachNgData currentSeach;
    private LineJpaController lineJpaController = null;
    private String dataNg;
    private String ticks;
    private DetailNgChart currentDetailNgChart;
    public LineJpaController getLineJpaController() {
        if (lineJpaController == null) {
            lineJpaController = new LineJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lineJpaController;
    }

    public void changeTosearch() throws SQLException {
        String start = Sql.formatDateSqlForNg(currentSeach.getStart());
        String stop = Sql.formatDateSqlForNg(currentSeach.getEnd()); 
        String line = currentSeach.getLine();
        loadNgChart(start, stop, line);
        getCurrentDetailNgChart().setStart(start);
        getCurrentDetailNgChart().setStop(stop);
        getCurrentDetailNgChart().setLineName(getLineJpaController().findLine(line).getLineName());
        //resetDataSearch();
    }  
    //**** load default ng data chart 

    public void loadDefaultNgDataChart() throws SQLException {
        fc.print("****** Load default ng data *******");
        String start = "14/10/2012 11:11:40";
        String stop  = Sql.formatDateSqlForNg(new timeCurrent().getDate());
        String line  = "00200cad-988f-47da-b233-eb8e40324ff2";
        loadNgChart(start, stop, line);
        getCurrentDetailNgChart().setStart(start);
        getCurrentDetailNgChart().setStop(stop);
        getCurrentDetailNgChart().setLineName(getLineJpaController().findLine(line).getLineName());
    } 
    public void loadNgChart(String start, String stop, String line) throws SQLException {
//        fc.print("====load Chart === : start date : " + getCurrentSeach().getStart() + " End : " + getCurrentSeach().getEnd() + " Line :" + getCurrentSeach().getLine());
        String sql = Sql.generateSqlForNg(start, stop, line);
        fc.print("SQL :" + sql);
        Connection conn = Sql.getConnection("mysql");
        Statement stm = conn.createStatement();
        if (stm.execute(sql)) {
            ResultSet result = stm.executeQuery(sql);
            int i = 1;
            dataNg = "[[0,''],";
            ticks = "[[0,''],";
            while (result.next()) {
                fc.print("Ng name :" + result.getObject(2).toString() + " percent:" + result.getObject(4).toString());
                dataNg += "[" + i + "," + result.getObject(4) + "],";
                ticks += "[" + i + "," + "'" + result.getObject(2) + "'" + "],";
                i++;
            }
            dataNg += "[]]";
            ticks += "[,'']]";
        }
        fc.print("dateNG : " + dataNg);
        fc.print("ticks : " + ticks);
    } 
    public void resetDataSearch() {
        currentSeach.setEnd(null);
        currentSeach.setStart(null);
        currentSeach.setLine("null");
    }
//    load lind

    public List<SelectItem> loadLindeItemAll() {
        List<SelectItem> objSlect = new ArrayList<SelectItem>();
        objSlect.add(new SelectItem("null", "---- Select one ----"));
        for (Line objList : getLineJpaController().findAllLineNotDeletd()) {
            objSlect.add(new SelectItem(objList.getLineId(), objList.getLineName()));
        }
        return objSlect;
    }

    public SeachNgData getCurrentSeach() {
        if (currentSeach == null) {
            currentSeach = new SeachNgData(null, null, null);
        }
        return currentSeach;
    }

    public void setCurrentSeach(SeachNgData currentSeach) {
        this.currentSeach = currentSeach;
    }

    public String getDataNg() {
        return dataNg;
    }

    public void setDataNg(String dataNg) {
        this.dataNg = dataNg;
    }

    public String getTicks() {
        return ticks;
    }

    public void setTicks(String ticks) {
        this.ticks = ticks;
    }

    public DetailNgChart getCurrentDetailNgChart() {
        if(currentDetailNgChart == null)
        {
            currentDetailNgChart  = new DetailNgChart(null, null, null);
        }
        return currentDetailNgChart;
    }

    public void setCurrentDetailNgChart(DetailNgChart currentDetailNgChart) {
        this.currentDetailNgChart = currentDetailNgChart;
    }
    
}
