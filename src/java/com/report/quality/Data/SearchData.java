/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.report.quality.Data;

import java.util.Date;

/**
 *
 * Machine User
 *
 * @author pang Development by Chusak laojang Date : Oct 21, 2012, Time :
 * 10:18:25 AM Copy Right 4 Xtreme Co.,Ltd.
 */
public class SearchData {

    private Date start;
    private Date end;
    private String line;
    public SearchData(Date start, Date end,String line) {
        this.start = start;
        this.end = end;
        this.line  = line;
    } 
    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }
}
