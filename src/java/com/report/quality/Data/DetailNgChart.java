/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.report.quality.Data;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Oct 26, 2012, Time : 11:33:42 AM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class DetailNgChart {
    private String start;
    private String stop;
    private String lineName;

    public DetailNgChart(String start, String stop, String lineName) {
        this.start = start;
        this.stop = stop;
        this.lineName = lineName;
    } 
    public String getLineName() {
        return lineName;
    }

    public void setLineName(String lineName) {
        this.lineName = lineName;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getStop() {
        return stop;
    }

    public void setStop(String stop) {
        this.stop = stop;
    } 
}
