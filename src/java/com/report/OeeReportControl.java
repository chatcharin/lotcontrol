/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.report;

import com.appCinfigpage.bean.fc;
import com.mysql.jdbc.Connection;
import com.tct.dashboard.ReportBean;
import com.tct.data.*;
import com.tct.data.jpa.*;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;

/**
 *
 * @author Tawat Saekue
 */
public class OeeReportControl {

    private final String sql_selectoee = "SELECT"
             + "     current1.time_current AS datetimes,"// index : 1
                + "     pcs_qty.qty_type,"// index : 2
                + "     pcs_qty.qty,"// index : 3
                + "     sum( lot_control.qty_output) AS goodpiece,"// index : 4
                + "     sum( if( pcs_qty.qty_type= 'in', pcs_qty.qty,0)) AS totalpiece,"// index : 5
                + "     mc.mc_name AS mcname,"// index : 6
                + "     mc.mc_code AS mccode,"// index : 7
                + "     mc.plan_production_time AS planproductiontime,"// index : 8
                + "     mc.product_rate AS productrate,"// index : 9
                + "     line.line_name AS linename,"// index : 10
                + "     ( mc.plan_production_time*60*60)-( SUM( current3.time_current- current4.time_current)*60/1000) AS operationtime,"// index : 11
                + "     current3.time_current AS timecurrent3,"// index : 12
                + "     current4.time_current AS timecurrent4,"// index : 13
                + "     lot_control.barcode,"// index : 14
                + "     (( mc.plan_production_time*60*60)-( SUM( current3.time_current- current4.time_current)*60/1000))/( ( mc.plan_production_time*60*60))*100 AS avability1,"// index : 15
                + "     ( sum( if( pcs_qty.qty_type= 'in', pcs_qty.qty,0)))/ mc.product_rate*100 AS performance,"// index : 16
                + "     1-( sum( lot_control.qty_output))/ sum( if( pcs_qty.qty_type= 'in', pcs_qty.qty,0)) AS qulity,"// index : 17
                + "     ((((( mc.plan_production_time*60*60)-(SUM( current3.time_current- current4.time_current)*60/1000))/(mc.plan_production_time*60*60)))*((1-( sum( lot_control.qty_output))/ sum( if( pcs_qty.qty_type= 'in', pcs_qty.qty,0)))/100)*((( sum( if( pcs_qty.qty_type= 'in', pcs_qty.qty,0)))/ mc.product_rate*100)/100))*100 AS percenoee,"// index : 18
                + "     mcdown.mc_id,"// index : 19
                + "     count(mcdown.mc_id) AS frequency,"// index : 20
                + "      SUM(current3.time_current- current4.time_current) AS totaldowntime,"// index : 21
                + "     ((mc.plan_production_time*60*60)-( SUM(current3.time_current- current4.time_current)))/count(mcdown.mc_id) AS mtbf,"// index : 22
                + "     ( SUM(current3.time_current- current4.time_current))/count(mcdown.mc_id) AS mttr,"// index : 23
                + "     (((mc.plan_production_time*60*60)-( SUM(current3.time_current- current4.time_current)))/count(mcdown.mc_id)/(((mc.plan_production_time*60*60)-( SUM(current3.time_current- current4.time_current)))/count(mcdown.mc_id)+( SUM(current3.time_current- current4.time_current))/count(mcdown.mc_id)))*100 AS avability2,"// index : 24
                + "     process_pool.proc_name AS processname"// index : 25
                + " FROM"
                + "     current_process current1 INNER JOIN mc_down mcdown ON current1.id = mcdown.current_process_id"
                + "     INNER JOIN mc mc ON mcdown.mc_id = mc.id"
                + "     INNER JOIN current_process current3 ON mcdown.current_process_id = current3.id"
                + "     INNER JOIN current_process current4 ON mcdown.ref_downTimeStart = current4.id"
                + "     INNER JOIN line line ON current1.line = line.line_id"
                + "     INNER JOIN pcs_qty ON current1.id = pcs_qty.current_process_id"
                + "     INNER JOIN main_data ON current1.main_data_id = main_data.id"
                + "     INNER JOIN lot_control ON main_data.lot_control_id = lot_control.id"
                + "     INNER JOIN process_pool ON main_data.id_proc = process_pool.id_proc ";
//            + " WHERE"
//            + "     current1.time_current > '2012-10-1 00:00:00'"
//            + " AND current1.time_current < current_date";
    private final String sql_groupby = " GROUP BY"
            + "     mcdown.mc_id"
            + " ORDER BY"
            + "     current1.time_current ASC";
    private Date startfromdate;
    private Date todate;
    private String mcid;
    private String processid;
    private String lineid;
    private List<OeeReportDataTable> dataTables;

    private void searchByLineAndDate() throws SQLException {
        fc.print("=================== search by process and date ===============");
        Connection conn = getConnection("mysql");
        Statement stm = conn.createStatement();
        try {
            String startdate = getDateFormat("yyyy-MM-dd", startfromdate) + " 00:00:00";
            String enddate = getDateFormat("yyyy-MM-dd", todate) + " 23:59:59";
            String sql = sql_selectoee + getWhereLineAndDate(lineid, startdate, enddate) + sql_groupby;
            fc.print("SQL : " + sql);
            if (stm.execute(sql)) {
                ResultSet rs = stm.executeQuery(sql);
                setValue(rs);
            }
            conn.close();
            stm.close();
        } catch (Exception e) {
            fc.print("Error : " + e);
            conn.close();
            stm.close();
        }
    }

    private String getWhereLineAndDate(String lineid_, String startdate, String enddate) {
        String sql = " WHERE "
                + "  current1.line = '" + lineid_ + "'"
                + " AND current1.time_current > '" + startdate + "'"
                + " AND current1.time_current < '" + enddate + "'";
        return sql;
    }

    private void searchByProcessAndDate() throws SQLException {
        fc.print("=================== search by process and date ===============");
        Connection conn = getConnection("mysql");
        Statement stm = conn.createStatement();
        try {
            String startdate = getDateFormat("yyyy-MM-dd", startfromdate) + " 00:00:00";
            String enddate = getDateFormat("yyyy-MM-dd", todate) + " 23:59:59";
            String sql = sql_selectoee + getWhereProcessAndDate(processid, startdate, enddate) + sql_groupby;
            fc.print("SQL : " + sql);
            if (stm.execute(sql)) {
                ResultSet rs = stm.executeQuery(sql);
                setValue(rs);
            }
            conn.close();
            stm.close();
        } catch (Exception e) {
            fc.print("Error : " + e);
            conn.close();
            stm.close();
        }
    }

    private String getWhereProcessAndDate(String pcid_, String startdate, String enddate) {
        String sql = " WHERE "
                + " process_pool.id_proc = '" + pcid_ + "'"
                + " AND current1.time_current > '" + startdate + "'"
                + " AND current1.time_current < '" + enddate + "'";
        return sql;
    }

    private void searchByMachineAndDate() throws SQLException {
        fc.print("=================== search by machine and date ===============");
        Connection conn = getConnection("mysql");
        Statement stm = conn.createStatement();
        try {
            String startdate = getDateFormat("yyyy-MM-dd", startfromdate) + " 00:00:00";
            String enddate = getDateFormat("yyyy-MM-dd", todate) + " 23:59:59";
            String sql = sql_selectoee + getWhereMachineAndDate(mcid, startdate, enddate) + sql_groupby;
            fc.print("SQL : " + sql);
            if (stm.execute(sql)) {
                ResultSet rs = stm.executeQuery(sql);
                setValue(rs);
            }
            conn.close();
            stm.close();
        } catch (Exception e) {
            fc.print("Error : " + e);
            conn.close();
            stm.close();
        }
    }

    private String getWhereMachineAndDate(String mcid_, String startdate, String enddate) {
        String sql = " WHERE "
                + "  mc.id = '" + mcid_ + "'"
                + " AND current1.time_current > '" + startdate + "'"
                + " AND current1.time_current < '" + enddate + "'";
        return sql;
    }

    private void setValue(ResultSet rs) {
        try {
            while (rs.next()) {
                String date = getDateFormat("dd/MM/yyyy", rs.getDate(1));
                String mcnumber = rs.getString(7);
                String mcname = rs.getString(6);
                String processname = rs.getString(25);
                String linename = rs.getString(10);
                String planproductiontime = rs.getObject(8).toString();
                String operationtime = rs.getObject(11).toString();
                String avability1 = getDecimal("#,###.####", rs.getObject(15).toString());
                String productrate = rs.getObject(9).toString();
                String performance = getDecimal("#,###.####", rs.getObject(16).toString());//rs.getObject(16).toString();
                String goodpiece = getDecimal("#,###.####", rs.getObject(4).toString());//rs.getObject(4).toString();
                String totalpiece = getDecimal("#,###.####", rs.getObject(5).toString());//rs.getObject(5).toString();
                String quality = getDecimal("#,###.####", rs.getObject(17).toString());//rs.getObject(17).toString();
                String percenoee = getDecimal("#,###.##", rs.getObject(18).toString());//rs.getObject(18).toString();
                String frequency = rs.getObject(20).toString();
                String totaldowntime = getDecimal("#,###.##", rs.getObject(21).toString());//rs.getObject(21).toString();
                String mtbf = getDecimal("#,###.####", rs.getObject(22).toString());//rs.getObject(22).toString();
                String mttr = getDecimal("#,###.####", rs.getObject(22).toString());//rs.getObject(23).toString();
                String avability2 = getDecimal("#,###.####", rs.getObject(24).toString());//rs.getObject(24).toString();
                OeeReportDataTable oeeReportDataTable = new OeeReportDataTable(date,
                        mcnumber, mcname, processname, linename, planproductiontime,
                        operationtime, avability1, productrate, performance, goodpiece,
                        totalpiece, quality, percenoee, frequency, totaldowntime, mtbf,
                        mttr, avability2);
                getDataTables().add(oeeReportDataTable);
            }
        } catch (SQLException ex) {
            Logger.getLogger(OeeReportControl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void searchBystartFromDateAndToDate() throws SQLException {
        fc.print("==================== search by start date and end date =======");
        Connection conn = getConnection("mysql");
        Statement stm = conn.createStatement();
        try {
            String startdate = getDateFormat("yyyy-MM-dd", startfromdate) + " 00:00:00";
            String enddate = getDateFormat("yyyy-MM-dd", todate) + " 23:59:59";
            String sql = sql_selectoee + getWhereStartdateAndEnddate(startdate, enddate) + sql_groupby;
            fc.print("SQL : " + sql);
            if (stm.execute(sql)) {
                ResultSet rs = stm.executeQuery(sql);
                setValue(rs);
            }
            conn.close();
            stm.close();
        } catch (Exception e) {
            conn.close();
            stm.close();
            fc.print("Error : " + e);
        }
    }

    private String getWhereStartdateAndEnddate(String startdate, String enddate) {
        String sql = " WHERE"
                + " current1.time_current > '" + startdate + "'"
                + " AND current1.time_current < '" + enddate + "'";
        return sql;
    }

    private String getDateFormat(String pattern, Date date) {
        return new SimpleDateFormat(pattern).format(date);
    }

    private String getDecimal(String pattern, String number) {
        double d = Double.parseDouble(number);
        return new DecimalFormat(pattern).format(d);
    }

    public List<SelectItem> getMachineList() {
        List<SelectItem> machinelist = new ArrayList<SelectItem>();
        try {
            List<Mc> mcs = getMcJpaController().findAllMcNotDelete();
            for (Mc mc : mcs) {
                machinelist.add(new SelectItem(mc.getId(), mc.getMcName()));
            }
            return machinelist;
        } catch (Exception e) {
            fc.print("Error : " + e);
            return null;
        }
    }

    public List<SelectItem> getProcessList() {
        List<SelectItem> processlist = new ArrayList<SelectItem>();
        try {
            List<ProcessPool> processPools = getProcessPoolJpaController().findProcessPoolByNameNotDeteltedAsc();
            for (ProcessPool processPool : processPools) {
                processlist.add(new SelectItem(processPool.getIdProc(), processPool.getProcName()));
            }
            return processlist;
        } catch (Exception e) {
            fc.print("Error : " + e);
            return null;
        }
    }

    public List<SelectItem> getLineList() {
        List<SelectItem> linelist = new ArrayList<SelectItem>();
        try {
            List<Line> lines = getLineJpaController().findAllLineNotDeletd();
            for (Line line : lines) {
                linelist.add(new SelectItem(line.getLineId(), line.getLineName()));
            }
            return linelist;
        } catch (Exception e) {
            fc.print("Error : " + e);
            return null;
        }
    }

    public void searchOeeReport() {
        fc.print("========== search All ========================================");
        dataTables = null;
        try {
            if (mcid != null && !mcid.isEmpty()) {
                searchByMachineAndDate();
            } else if (processid != null && !processid.isEmpty()) {
                searchByProcessAndDate();
            } else if (lineid != null && !lineid.isEmpty()) {
                searchByLineAndDate();
            } else {
                searchBystartFromDateAndToDate();
            }
        } catch (Exception e) {
            fc.print("Error search all : " + e);
        }
    }

    private long convertTimeToTime(String type, long time) {
        if (type.equals("datetohours")) {
            return time * 60;
        } else if (type.equals("hourstominutes")) {
            return time * 60;
        } else if (type.equals("minutestosecounds")) {
            return time * 60;
        } else if (type.equals("secoundstomilisec")) {
            return time * 1000;
        } else {
            return 0;
        }
    }

    private long convertTimeStampToTime(String type, long stamp) {
        if (type.equals("secounds")) {
            return stamp / 1000;
        } else if (type.equals("minutes")) {
            return stamp / (60 * 1000);
        } else if (type.equals("hours")) {
            return stamp / (60 * 60 * 1000);
        } else if (type.equals("date")) {
            return stamp / (24 * 60 * 60 * 1000);
        } else {
            return 0;
        }
    }

    private boolean checkOeeListMc(CurrentProcess currentProcess, List<OeeListMc> oeeListMcs) {
        for (OeeListMc oeeListMc : oeeListMcs) {
            if (oeeListMc.getProcessPool().getIdProc().equals(currentProcess.getMainDataId().getIdProc().getIdProc())) {
                return true;
            }
        }
        return false;
    }

    private long getFrequency(String date, Mc mc) {
        fc.print("========== get frequency by mc :" + mc.getMcName() + " and date " + date);
        try {
            Date fDate = ConvertStringToDate("dd/MM/yyyy HH:mm:ss", date + " 00:00:00");
            Date lDate = ConvertStringToDate("dd/MM/yyyy HH:mm:ss", date + " 23:59:59");
            long l = 0;
            List<CurrentProcess> currentProcesses = getCurrentProcessJpaController().findCurrentProcessByDateAndMc(mc, fDate, lDate);

            return currentProcesses.size();
        } catch (Exception e) {
            fc.print("Error : " + e);
            return 0;
        }
    }

    private long getGoodPiece(String date, Mc mc) {
        fc.print("========== get goodpiece by mc :" + mc.getMcName() + " and date " + date);
        try {
            Date fDate = ConvertStringToDate("dd/MM/yyyy HH:mm:ss", date + " 00:00:00");
            Date lDate = ConvertStringToDate("dd/MM/yyyy HH:mm:ss", date + " 23:59:59");
            long l = 0;
            List<CurrentProcess> currentProcesses = getCurrentProcessJpaController().findCurrentProcessByDateAndMc(mc, fDate, lDate);
            for (CurrentProcess currentProcess : currentProcesses) {
                if (currentProcess.getStatus().equals("Finished")) {
                    l += currentProcess.getMainDataId().getLotControlId().getQtyOutput();
                }
            }
            return l;
        } catch (Exception e) {
            fc.print("Error : " + e);
            return 0;
        }
    }

    private long getTotalPiece(String date, Mc mc) {
        fc.print("========== get totalpiece by mc :" + mc.getMcName() + " and date " + date);
        try {
            Date fDate = ConvertStringToDate("dd/MM/yyyy HH:mm:ss", date + " 00:00:00");
            Date lDate = ConvertStringToDate("dd/MM/yyyy HH:mm:ss", date + " 23:59:59");
            long l = 0;
            List<CurrentProcess> currentProcesses = getCurrentProcessJpaController().findCurrentProcessByDateAndMc(mc, fDate, lDate);
            for (CurrentProcess currentProcess : currentProcesses) {
                Collection<PcsQty> pcsQtys = currentProcess.getPcsQtyCollection();
                for (PcsQty pcsQty : pcsQtys) {
                    if (pcsQty.getQtyType().equals("in")) {
                        l += Long.parseLong(pcsQty.getQty());
                    }
                }
            }
            return l;
        } catch (Exception e) {
            fc.print("Error : " + e);
            return 0;
        }
    }

    private ProcessPoolJpaController processPoolJpaController;

    private ProcessPoolJpaController getProcessPoolJpaController() {
        if (processPoolJpaController == null) {
            processPoolJpaController = new ProcessPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return processPoolJpaController;
    }
    private McJpaController mcJpaController;

    private McJpaController getMcJpaController() {
        if (mcJpaController == null) {
            mcJpaController = new McJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mcJpaController;
    }
    private McDownJpaController mcDownJpaController;

    private McDownJpaController getMcDownJpaController() {
        if (mcDownJpaController == null) {
            mcDownJpaController = new McDownJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mcDownJpaController;
    }
    private LineJpaController lineJpaController;

    private LineJpaController getLineJpaController() {
        if (lineJpaController == null) {
            lineJpaController = new LineJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lineJpaController;
    }
    private CurrentProcessJpaController currentProcessJpaController;

    private CurrentProcessJpaController getCurrentProcessJpaController() {
        if (currentProcessJpaController == null) {
            currentProcessJpaController = new CurrentProcessJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return currentProcessJpaController;
    }

    public Connection getConnection(String brand) {
        fc.print(brand);
        if (brand.equals("mysql")) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                return (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/tct_project", "root", "root");
            } catch (Exception ex) {
                Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        } else {
            try {
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                return (Connection) DriverManager.getConnection("jdbc:sqlserver://localhost\\SQLEXPRESS:1433;databaseName=tct_project", "test", "1234");
            } catch (Exception ex) {
                Logger.getLogger(ReportBean.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        }

    }

    private Date ConvertStringToDate(String patern, String source) throws ParseException {
        return new SimpleDateFormat(patern).parse(source);
    }

    private String ConvertDateToString(String patern, Date date) {
        return new SimpleDateFormat(patern).format(date);
    }

    public Date getStartfromdate() {
        return startfromdate;
    }

    public void setStartfromdate(Date startfromdate) {
        this.startfromdate = startfromdate;
    }

    public Date getTodate() {
        return todate;
    }

    public void setTodate(Date todate) {
        this.todate = todate;
    }

    public String getMcid() {
        return mcid;
    }

    public void setMcid(String mcid) {
        this.mcid = mcid;
    }

    public String getLineid() {
        return lineid;
    }

    public void setLineid(String lineid) {
        this.lineid = lineid;
    }

    public String getProcessid() {
        return processid;
    }

    public void setProcessid(String processid) {
        this.processid = processid;
    }

    public List<OeeReportDataTable> getDataTables() {
        if (dataTables == null) {
            dataTables = new ArrayList<OeeReportDataTable>();
        }
        return dataTables;
    }
}
