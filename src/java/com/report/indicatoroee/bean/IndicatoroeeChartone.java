/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.report.indicatoroee.bean;

import com.appCinfigpage.bean.fc;
import com.mysql.jdbc.Statement;
import com.report.indicator.Data.IndicatorVaribleData;
import com.sql.bean.Sql;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 *
 * Machine User
 *
 * @author pang Development by Chusak laojang Date : Nov 6, 2012, Time : 3:50:09
 * PM Copy Right 4 Xtreme Co.,Ltd.
 */
public class IndicatoroeeChartone {

    private Date startDate;
    private Date stopDate;
    private IndicatorVaribleData currentIndicatorVaribleData;
    private String dataForChart;
    private String dataReal;

    public void changeToSearch() throws SQLException {
        fc.print("**** Change To Search ***** :start date :" + startDate + "   : stop date " + stopDate);
        findData();
    }

    private String getDateFormat(String pattern, Date date) {
        return new SimpleDateFormat(pattern).format(date);
    }

    public String getFillter() {
        String fillter = "";
        String startdate2 = getDateFormat("yyyy-MM-dd", startDate) + " 00:00:00";
        String enddate2 = getDateFormat("yyyy-MM-dd", stopDate) + " 00:00:00";
        if (startdate2 != null) {
            fillter += "  current1.time_current >=  '" + startdate2 + "'   ";
        }
        if (enddate2 != null) {
            fillter += " AND current1.time_current <=  '" + enddate2 + "'   ";
        }
        return fillter;
    }

    public String randomValue() {
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(100);
        return Integer.toString(randomInt);
    }
    double avability1 = 0;
    double performance1 = 0;
    double quality1 = 0;
    double percenoee1 = 0;
    double count = 1;

    public void findData() throws SQLException {
        fc.print("***** sql chart: " + Sql.generateOeeReportChart(getFillter()));
        com.mysql.jdbc.Connection conn = Sql.getConnection("mysql"); //connection SQL
        Statement stm = (Statement) conn.createStatement();
        avability1 = 0;
        performance1 = 0;
        quality1 = 0;
        count = 1;
        if (stm.execute(Sql.generateOeeReportChart(getFillter()))) {
            ResultSet result = stm.executeQuery(Sql.generateOeeReportChart(getFillter()));
            while (result.next()) {
                avability1 += Double.parseDouble(result.getObject(15).toString());
                performance1 += Double.parseDouble(result.getObject(16).toString());
                quality1 += Double.parseDouble(result.getObject(17).toString());
                percenoee1 += Double.parseDouble(result.getObject(18).toString());
                count++;
            }
        }
        fc.print("***** Find data all ******");
        
        dataForChart = "[[" + "1,'Avalibity'],";
        dataForChart += "[" + "2,'Perfermance'],";
        dataForChart += "[" + "3,'Quality'],";
        dataForChart += "[" + "4,'%Oee']]";
        fc.print("count **** " + count);
        avability1 = avability1 / count;
        performance1 = performance1 / count;
        quality1 = quality1 / count;
        percenoee1 = percenoee1 / count;
        fc.print("availity" + avability1);
        /*
        currentIndicatorVaribleData.setAvability(Double.toString(avability1));
        currentIndicatorVaribleData.setOee(Double.toString(performance1));
        currentIndicatorVaribleData.setPerformance(Double.toString(quality1));
        currentIndicatorVaribleData.setQuality(Double.toString(percenoee1)); */
        currentIndicatorVaribleData.setAvability(avability1);
        currentIndicatorVaribleData.setOee(performance1);
        currentIndicatorVaribleData.setPerformance(quality1);
        currentIndicatorVaribleData.setQuality(percenoee1);
        dataReal = "[[" + "1," + avability1 + "],";
        dataReal += "[" + "2," + performance1 + "],";
        dataReal += "[" + "3," + quality1 + "],";
        dataReal += "[" + "4," + percenoee1 + "]]";
    }

    public Date getStopDate() {
        return stopDate;
    }

    public void setStopDate(Date stopDate) {
        this.stopDate = stopDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public IndicatorVaribleData getCurrentIndicatorVaribleData() {
        if (currentIndicatorVaribleData == null) {
            currentIndicatorVaribleData = new IndicatorVaribleData(0, 0, 0, 0);
        }
        return currentIndicatorVaribleData;
    }

    public void setCurrentIndicatorVaribleData(IndicatorVaribleData currentIndicatorVaribleData) {
        this.currentIndicatorVaribleData = currentIndicatorVaribleData;
    }

    public String getDataForChart() {
        return dataForChart;
    }

    public void setDataForChart(String dataForChart) {
        this.dataForChart = dataForChart;
    }

    public String getDataReal() {
        return dataReal;
    }

    public void setDataReal(String dataReal) {
        this.dataReal = dataReal;
    }
}
