/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.report;

/**
 *
 * @author Tawat Seakue
 */
public class HistoryTackDataTable {

    private String date;
    private String barcode;
    private String linkbarcode;
    private String series;
    private String model;
    private String stampp;
    private String process;
    private String section;
    private String status;

    public HistoryTackDataTable(String date, String barcode, String linkbarcode, String series, String model, String stampp, String process, String section, String status) {
        this.date = date;
        this.barcode = barcode;
        this.linkbarcode = linkbarcode;
        this.series = series;
        this.model = model;
        this.stampp = stampp;
        this.process = process;
        this.section = section;
        this.status = status;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLinkbarcode() {
        return linkbarcode;
    }

    public void setLinkbarcode(String linkbarcode) {
        this.linkbarcode = linkbarcode;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getStampp() {
        return stampp;
    }

    public void setStampp(String stampp) {
        this.stampp = stampp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Date : " + date + "\n"
                + "Barcode : " + barcode + "\n"
                + "Barcode Link : " + linkbarcode + "\n"
                + "Model : " + model + "\n"
                + "Process : " + process + "\n"
                + "Series : " + series + "\n"
                + "Section : " + section + "\n"
                + "Stampp : " + stampp + "\n"
                + "Status : " + status + "";
    }
}
