/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.report;

/**
 *
 * @author Tawat Saekue
 */
public class OeeReportDataTable {
    private String date;
    private String mcNumber;
    private String mcName;
    private String process;
    private String line;
    private String planproductiontime;
    private String operationtime;
    private String avability1;
    private String productrate;
    private String performance;
    private String goodprice;
    private String totalprice;
    private String quqlity;
    private String percenoee;
    private String frequency;
    private String totaldowntime;
    private String mtbf;
    private String mttr;
    private String avability2;

    public OeeReportDataTable(String date, String mcNumber, String mcName, String process, String line, String planproductiontime, String operationtime, String avability1, String productrate, String performance, String goodprice, String totalprice, String quqlity, String percenoee, String frequency, String totaldowntime, String mtbf, String mttr, String avability2) {
        this.date = date;
        this.mcNumber = mcNumber;
        this.mcName = mcName;
        this.process = process;
        this.line = line;
        this.planproductiontime = planproductiontime;
        this.operationtime = operationtime;
        this.avability1 = avability1;
        this.productrate = productrate;
        this.performance = performance;
        this.goodprice = goodprice;
        this.totalprice = totalprice;
        this.quqlity = quqlity;
        this.percenoee = percenoee;
        this.frequency = frequency;
        this.totaldowntime = totaldowntime;
        this.mtbf = mtbf;
        this.mttr = mttr;
        this.avability2 = avability2;
    }

    public String getAvability1() {
        return avability1;
    }

    public void setAvability1(String avability1) {
        this.avability1 = avability1;
    }

    public String getAvability2() {
        return avability2;
    }

    public void setAvability2(String avability2) {
        this.avability2 = avability2;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getGoodprice() {
        return goodprice;
    }

    public void setGoodprice(String goodprice) {
        this.goodprice = goodprice;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getMcName() {
        return mcName;
    }

    public void setMcName(String mcName) {
        this.mcName = mcName;
    }

    public String getMcNumber() {
        return mcNumber;
    }

    public void setMcNumber(String mcNumber) {
        this.mcNumber = mcNumber;
    }

    public String getMtbf() {
        return mtbf;
    }

    public void setMtbf(String mtbf) {
        this.mtbf = mtbf;
    }

    public String getMttr() {
        return mttr;
    }

    public void setMttr(String mttr) {
        this.mttr = mttr;
    }

    public String getOperationtime() {
        return operationtime;
    }

    public void setOperationtime(String operationtime) {
        this.operationtime = operationtime;
    }

    public String getPercenoee() {
        return percenoee;
    }

    public void setPercenoee(String percenoee) {
        this.percenoee = percenoee;
    }

    public String getPerformance() {
        return performance;
    }

    public void setPerformance(String performance) {
        this.performance = performance;
    }

    public String getPlanproductiontime() {
        return planproductiontime;
    }

    public void setPlanproductiontime(String planproductiontime) {
        this.planproductiontime = planproductiontime;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getProductrate() {
        return productrate;
    }

    public void setProductrate(String productrate) {
        this.productrate = productrate;
    }

    public String getQuqlity() {
        return quqlity;
    }

    public void setQuqlity(String quqlity) {
        this.quqlity = quqlity;
    }

    public String getTotaldowntime() {
        return totaldowntime;
    }

    public void setTotaldowntime(String totaldowntime) {
        this.totaldowntime = totaldowntime;
    }

    public String getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(String totalprice) {
        this.totalprice = totalprice;
    }
    
}
