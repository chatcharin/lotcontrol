/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.report.mcdowntimereport.bean;

import com.appCinfigpage.bean.fc;
import com.mysql.jdbc.Statement;
import com.report.mcdowntime.Data.McDowntimeReportData;
import com.report.mcdowntime.Data.McDowntimeReportSerachData;
import com.sql.bean.Sql;
import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;
import com.tct.data.ProcessPool;
import com.tct.data.jpa.ProcessPoolJpaController;
import com.tct.data.Mc;
import com.tct.data.jpa.McJpaController;
import com.tct.data.jpa.LineJpaController;
import com.tct.data.Line;
import com.tct.data.jpa.McDownJpaController;
import com.tct.data.McDown;
import com.tct.data.jsf.util.PaginationHelper;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.model.ArrayDataModel;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.persistence.Persistence;

/**
 *
 * Machine User
 *
 * @author pang Development by Chusak laojang Date : Nov 8, 2012, Time :
 * 11:19:35 AM Copy Right 4 Xtreme Co.,Ltd.
 */
public class McDowntimeReport {

    private ProcessPoolJpaController processPoolJpaController = null;
    private McJpaController mcJpaController = null;
    private LineJpaController lineJpaController = null;
    private McDownJpaController mcDownJpaController = null;
    private McDowntimeReportSerachData currentMcDowntimeReportSerachData;
    private List<McDowntimeReportData> itemMcDowntimeList;
    private DataModel mcDowntimeModel;

    private McJpaController getMcJpaController() {
        if (mcJpaController == null) {
            mcJpaController = new McJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mcJpaController;
    }

    private ProcessPoolJpaController getProcessPoolJpaController() {
        if (processPoolJpaController == null) {
            processPoolJpaController = new ProcessPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return processPoolJpaController;
    }

    private LineJpaController getLineJpaController() {
        if (lineJpaController == null) {
            lineJpaController = new LineJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lineJpaController;
    }

    private McDownJpaController getMcDownJpaController() {
        if (mcDownJpaController == null) {
            mcDownJpaController = new McDownJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mcDownJpaController;
    }

    public void changeToSearch() throws SQLException {
        fc.print("**** Change to search *****");
        prepareDataAll();
        findData();
        resetSearchData();
    }

    public void prepareDataAll() {
        itemMcDowntimeList = new ArrayList<McDowntimeReportData>();
        worksheetpagination = null;
        mcDowntimeModel = new ArrayDataModel();
        dately = null;
        weekly = null;
        monthly = null;
        yearly = null;
    }

    public void resetSearchData() {
        currentMcDowntimeReportSerachData = new McDowntimeReportSerachData("null", "null", "null", null, null, "0");
        dately = null;
        weekly = null;
        monthly = null;
        yearly = null;
    }

    private String getDateFormat(String pattern, Date date) {
        return new SimpleDateFormat(pattern).format(date);
    }
    private String dateconvert = null;
    private String dateConvert2 = null;
    private String dateConvert3 = null;
    private Date dately;
    private Date weekly = null;
    private Date monthly = null;
    private Date yearly = null;

    public void findData() throws SQLException {
        fc.print("***** find data *****");
        fc.print("process name " + currentMcDowntimeReportSerachData.getProcess());
        com.mysql.jdbc.Connection conn = Sql.getConnection("mysql"); //connection SQL
        Statement stm = (Statement) conn.createStatement();
        fc.print("sql : " + Sql.generateSqlForMcDowntime(getFillter()));
        if (stm.execute(Sql.generateSqlForMcDowntime(getFillter()))) {
            ResultSet result = stm.executeQuery(Sql.generateSqlForMcDowntime(getFillter()));
            while (result.next()) {
                getItemMcDowntimeList().add(new McDowntimeReportData(result.getObject(2).toString(), result.getObject(6).toString(), result.getObject(5).toString(), result.getObject(1).toString(), result.getObject(11).toString(), result.getObject(8).toString(), result.getObject(2).toString(), result.getObject(10).toString(), result.getObject(12).toString(), result.getObject(4).toString(), result.getObject(7).toString(), result.getObject(13).toString()));
            }
        }
        setMcDowntimeModel();
    }

    public void searchByDaily() throws SQLException {
        prepareDataAll();
        dately  = new Date();
        findData();
        resetSearchData();
    }

    public void searchWeekly() throws SQLException {
        prepareDataAll();
        weekly = new Date();
        findData();
        resetSearchData();
    }

    public void searchMonthly() throws SQLException {
        prepareDataAll();
        monthly  = new Date();
        findData();
        resetSearchData();
    }

    public void searchYearly() throws SQLException {
        prepareDataAll();
        yearly = new Date();
        findData();
        resetSearchData();
    }

    public String getFillter() {
        fc.print("*** fillter *****");
        String fillter = "";

        if (!currentMcDowntimeReportSerachData.getProcess().equals("null")) {
            fillter += " and process_pool.`id_proc` >='" + currentMcDowntimeReportSerachData.getProcess() + "' ";
        }
        if (!currentMcDowntimeReportSerachData.getMcName().equals("null")) {
            fillter += " and mc.`id` ='" + currentMcDowntimeReportSerachData.getMcName() + "' ";
        }
        if (!currentMcDowntimeReportSerachData.getLine().equals("null")) {
            fillter += " and line.`line_id` ='" + currentMcDowntimeReportSerachData.getLine() + "' ";
        }
        if (currentMcDowntimeReportSerachData.getStartFromDate() != null) {
            String startdate2 = getDateFormat("yyyy-MM-dd", currentMcDowntimeReportSerachData.getStartFromDate()) + " 00:00:00";
            fillter += " and current_process.`time_current` >='" + startdate2 + "' ";
        }
        if (currentMcDowntimeReportSerachData.getToDate() != null) {
            String enddate2 = getDateFormat("yyyy-MM-dd", currentMcDowntimeReportSerachData.getToDate()) + " 00:00:00";
            fillter += " and current_process.`time_current` <='" + enddate2 + "' ";
        }
        if (!currentMcDowntimeReportSerachData.getDownTimeReason().equals("0")) {
            fillter += " and mc_down.`id`='" + currentMcDowntimeReportSerachData.getDownTimeReason() + "' ";
        } 
        if (dately != null) {
            String datelyStr = getDateFormat("yyyy-MM-dd",dately) + " 00:00:00";
            fillter += " and current_process.`time_current` >='" + datelyStr + "' ";
        }
        if ( weekly != null) 
        {   String datelyStr = getDateFormat("yyyy-MM-dd",weekly) + " 00:00:00";
            fillter +=" and WEEK(current_process.time_current) = WEEK('" + datelyStr + "') ";
            fillter +=" and MONTH(current_process.time_current) = MONTH('" + datelyStr + "')"; 
            fillter +=" and YEAR(current_process.time_current) = YEAR('" + datelyStr + "')";
        }
        if (monthly != null)
        {
            String monthlyStr = getDateFormat("yyyy-MM-dd",monthly) + " 00:00:00";
            fillter +=" and MONTH(current_process.time_current) = MONTH('" + monthlyStr + "') "; 
            fillter +=" and YEAR(current_process.time_current) = YEAR('" + monthlyStr + "')";
        }
        if(yearly != null)
        {
            String yearlyStr = getDateFormat("yyyy-MM-dd",yearly) + " 00:00:00";
            fillter +=" and YEAR(current_process.time_current) = YEAR('" + yearlyStr + "')";
        }
        return fillter;
    }

    public List<SelectItem> itemProcess() {
        List<SelectItem> itemProcess = new ArrayList<SelectItem>();
        itemProcess.add(new SelectItem("null", "---- Select one ----"));
        List<ProcessPool> objProcess = getProcessPoolJpaController().findProcessPoolByNameNotDeteltedAsc();
        if (!objProcess.isEmpty()) {
            for (ProcessPool objList : objProcess) {
                itemProcess.add(new SelectItem(objList.getIdProc(), objList.getProcName()));
            }
        }

        return itemProcess;
    }

    public List<SelectItem> itemMcName() {
        List<SelectItem> itemMcName = new ArrayList<SelectItem>();
        itemMcName.add(new SelectItem("null", "---- Select one ----"));
        List<Mc> objMc = getMcJpaController().findAllMcNotDelete();
        if (!objMc.isEmpty()) {
            for (Mc objList : objMc) {
                itemMcName.add(new SelectItem(objList.getId(), objList.getMcName()));
            }
        }

        return itemMcName;
    }

    public List<SelectItem> itemLine() {
        List<SelectItem> itemLine = new ArrayList<SelectItem>();
        itemLine.add(new SelectItem("null", "---- Select one ----"));
        List<Line> objLine = getLineJpaController().findAllLineNotDeletd();
        if (!objLine.isEmpty()) {
            for (Line objList : objLine) {
                itemLine.add(new SelectItem(objList.getLineId(), objList.getLineName()));
            }
        }
        return itemLine;
    }

    public List<SelectItem> itemDowntimeReason() {
        List<SelectItem> itemDowntimeReason = new ArrayList<SelectItem>();
        itemDowntimeReason.add(new SelectItem("0", "---- Select one -----"));
        itemDowntimeReason.add(new SelectItem("Abnormal production"));
        itemDowntimeReason.add(new SelectItem("Change over"));
        itemDowntimeReason.add(new SelectItem("Electric shutdown"));
        itemDowntimeReason.add(new SelectItem("Equipment breakdowns"));
        itemDowntimeReason.add(new SelectItem("Machine idle"));
        itemDowntimeReason.add(new SelectItem("Others"));
        itemDowntimeReason.add(new SelectItem("Preventive maintenance"));
        itemDowntimeReason.add(new SelectItem("Process failure"));
        itemDowntimeReason.add(new SelectItem("Production adjustment"));
        itemDowntimeReason.add(new SelectItem("Quality defects"));
        itemDowntimeReason.add(new SelectItem("Set up"));
        itemDowntimeReason.add(new SelectItem("Unplan"));
        return itemDowntimeReason;
    }

    public void previous() {
        getWorksheetpagination().previousPage();
        setMcDowntimeModel();
    }

    public void next() {
        getWorksheetpagination().nextPage();
        setMcDowntimeModel();
    }

    public int getAllPages() {
        int all = getWorksheetpagination().getItemsCount();
        int pagesize = getWorksheetpagination().getPageSize();
        return ((int) all / pagesize) + 1;
    }
    private PaginationHelper worksheetpagination;

    public PaginationHelper getWorksheetpagination() {
        if (worksheetpagination == null) {
            worksheetpagination = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    //fc.print("size of data : "+getMasterListViewData().size());
                    return getItemMcDowntimeList().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    fc.print("PageFirst : " + getPageFirstItem());
                    fc.print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (getItemMcDowntimeList().size() > 0) {
                        listmodel = new ListDataModel(getItemMcDowntimeList().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(getItemMcDowntimeList());
                    }
                    return listmodel;
                }
            };
        }
        return worksheetpagination;
    }

    public List<McDowntimeReportData> getItemMcDowntimeList() {
        if (itemMcDowntimeList == null) {
            itemMcDowntimeList = new ArrayList<McDowntimeReportData>();
        }
        return itemMcDowntimeList;
    }

    public void setItemMcDowntimeList(List<McDowntimeReportData> itemMcDowntimeList) {
        this.itemMcDowntimeList = itemMcDowntimeList;
    }

    public McDowntimeReportSerachData getCurrentMcDowntimeReportSerachData() {
        if (currentMcDowntimeReportSerachData == null) {
            currentMcDowntimeReportSerachData = new McDowntimeReportSerachData(null, null, null, null, null, null);
        }
        return currentMcDowntimeReportSerachData;
    }

    public void setCurrentMcDowntimeReportSerachData(McDowntimeReportSerachData currentMcDowntimeReportSerachData) {
        this.currentMcDowntimeReportSerachData = currentMcDowntimeReportSerachData;
    }

    public String getDateconvert() {
        return dateconvert;
    }

    public void setDateconvert(String dateconvert) {
        this.dateconvert = dateconvert;
    }

    public String getDateConvert2() {
        return dateConvert2;
    }

    public void setDateConvert2(String dateConvert2) {
        this.dateConvert2 = dateConvert2;
    }

    public String getDateConvert3() {
        return dateConvert3;
    }

    public void setDateConvert3(String dateConvert3) {
        this.dateConvert3 = dateConvert3;
    }

    public DataModel getMcDowntimeModel() {
        return mcDowntimeModel;
    }

    public void setMcDowntimeModel() {
        this.mcDowntimeModel = getWorksheetpagination().createPageDataModel();
    }

    public Date getMonthly() {
        return monthly;
    }

    public void setMonthly(Date monthly) {
        this.monthly = monthly;
    }

    public Date getWeekly() {
        return weekly;
    }

    public void setWeekly(Date weekly) {
        this.weekly = weekly;
    }

    public Date getYearly() {
        return yearly;
    }

    public void setYearly(Date yearly) {
        this.yearly = yearly;
    }

    public Date getDately() {
        return dately;
    }

    public void setDately(Date dately) {
        this.dately = dately;
    }
}
