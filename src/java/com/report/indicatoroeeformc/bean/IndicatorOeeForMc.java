/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.report.indicatoroeeformc.bean;

import com.appCinfigpage.bean.fc;
import com.mysql.jdbc.Statement;
import com.sql.bean.Sql;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 *
 * Machine User
 *
 * @author pang Development by Chusak laojang Date : Nov 6, 2012, Time : 4:50:15
 * PM Copy Right 4 Xtreme Co.,Ltd.
 */
public class IndicatorOeeForMc {

    private Date startdate;
    private Date stopDate;
    private String mcData;
    private String mcTicks;

    public void changeToSearch() throws SQLException {
        fc.print("***** change to search **** : date  start :" + startdate + "   : stop date : " + stopDate);
        findData();
    }

    private String getDateFormat(String pattern, Date date) {
        return new SimpleDateFormat(pattern).format(date);
    }

    public String getFillter() {
        String fillter = "";
        String startdate2 = getDateFormat("yyyy-MM-dd", startdate) + " 00:00:00";
        String enddate2 = getDateFormat("yyyy-MM-dd", stopDate) + " 00:00:00";
        if (startdate2 != null) {
            fillter += "  current1.time_current >=  '" + startdate2 + "'   ";
        }
        if (enddate2 != null) {
            fillter += " AND current1.time_current <=  '" + enddate2 + "'   ";
        }
        return fillter;
    }

    public int randomValue() {
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        return randomInt;
    }

    public String randomValue2() {
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(100);
        return Integer.toString(randomInt);
    }

    public void findData() throws SQLException {
        mcData = "[";
        mcTicks = "[";
        fc.print("***** sql chart: " + Sql.generateOeeReportChart(getFillter()));
        com.mysql.jdbc.Connection conn = Sql.getConnection("mysql"); //connection SQL
        Statement stm = (Statement) conn.createStatement();
        int i = 1;
        if (stm.execute(Sql.generateOeeReportChart(getFillter()))) {
            ResultSet result = stm.executeQuery(Sql.generateOeeReportChart(getFillter()));
            while (result.next()) {
                mcData += "[" + i + "," + result.getObject(18).toString() + "],";
                mcTicks += "[" + i + ",'" + result.getObject(6).toString() + "'],";
                i++;
            }
        }
        mcData += "[]]";
        mcTicks += "[]]";
    }

    public Date getStopDate() {
        return stopDate;
    }

    public void setStopDate(Date stopDate) {
        this.stopDate = stopDate;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public String getMcData() {
        return mcData;
    }

    public void setMcData(String mcData) {
        this.mcData = mcData;
    }

    public String getMcTicks() {
        return mcTicks;
    }

    public void setMcTicks(String mcTicks) {
        this.mcTicks = mcTicks;
    }
}
