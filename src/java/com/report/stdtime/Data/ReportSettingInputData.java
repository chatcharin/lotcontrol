/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.report.stdtime.Data;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Nov 2, 2012, Time : 8:52:22 AM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class ReportSettingInputData {
    private String index;
    private String name;
    private String value;

    public ReportSettingInputData(String index, String name, String value) {
        this.index = index;
        this.name = name;
        this.value = value;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    } 
}
