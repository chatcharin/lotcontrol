/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.report.stdtime.Data;

import java.util.Date;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Nov 2, 2012, Time : 11:06:59 AM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class ReportSettingSearchData {
    private Date startDate;
    private Date endDate;
    private String groubModel;
    private String model;
    private String line;
    private String process;

    public ReportSettingSearchData(Date startDate, Date endDate, String groubModel, String model, String line, String process) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.groubModel = groubModel;
        this.model = model;
        this.line = line;
        this.process = process;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getGroubModel() {
        return groubModel;
    }

    public void setGroubModel(String groubModel) {
        this.groubModel = groubModel;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    } 
}
