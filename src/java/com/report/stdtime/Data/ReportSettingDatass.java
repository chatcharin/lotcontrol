/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.report.stdtime.Data;

import java.util.Date;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Nov 1, 2012, Time : 4:39:10 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class ReportSettingDatass {
    private String index;
    private Date date;
    private String type;
    private String line;
    private String process;
    private String product;
    private String plan;
    private String productFG;
    private String totalworkTime;
    private String stdTime; 
    private boolean inputStatus ;
    private boolean saveButton ;
    private boolean editButton ;

    public ReportSettingDatass(String index, Date date, String type, String line, String process, String product, String plan, String productFG, String totalworkTime, String stdTime, boolean inputStatus, boolean saveButton, boolean editButton) {
        this.index = index;
        this.date = date;
        this.type = type;
        this.line = line;
        this.process = process;
        this.product = product;
        this.plan = plan;
        this.productFG = productFG;
        this.totalworkTime = totalworkTime;
        this.stdTime = stdTime;
        this.inputStatus = inputStatus;
        this.saveButton = saveButton;
        this.editButton = editButton;
    } 
    public boolean isEditButton() {
        return editButton;
    }

    public void setEditButton(boolean editButton) {
        this.editButton = editButton;
    }

    public boolean isInputStatus() {
        return inputStatus;
    }

    public void setInputStatus(boolean inputStatus) {
        this.inputStatus = inputStatus;
    }

    public boolean isSaveButton() {
        return saveButton;
    }

    public void setSaveButton(boolean saveButton) {
        this.saveButton = saveButton;
    } 
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getProductFG() {
        return productFG;
    }

    public void setProductFG(String productFG) {
        this.productFG = productFG;
    }

    public String getStdTime() {
        return stdTime;
    }

    public void setStdTime(String stdTime) {
        this.stdTime = stdTime;
    }

    public String getTotalworkTime() {
        return totalworkTime;
    }

    public void setTotalworkTime(String totalworkTime) {
        this.totalworkTime = totalworkTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    } 
}
