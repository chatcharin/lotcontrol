/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.report.stdtime.bean;

import com.appCinfigpage.bean.fc;
import com.report.stdtime.Data.ReportSettingDatass;
import com.report.stdtime.Data.ReportSettingInputData;
import com.report.stdtime.Data.ReportSettingSearchData;
import com.sql.bean.Sql;
import com.sun.jndi.ldap.Connection;
import com.tct.data.*;
import com.tct.data.jpa.StdTimeSettingJpaController;
import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;
import com.tct.data.jpa.LineJpaController;
import com.tct.data.jpa.ModelPoolJpaController;
import com.tct.data.jpa.ProcessPoolJpaController;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import com.tct.data.jpa.StdDataShowInputJpaController;
import com.tct.data.jpa.StdDataShowJpaController;
import com.tct.data.jsf.util.PaginationHelper;
import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.ArrayDataModel;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import com.tct.data.jpa.CurrentProcessJpaController;
import com.tct.data.CurrentProcess;
import com.tct.data.jpa.PcsQtyJpaController;
import com.tct.data.PcsQty;

/**
 *
 * Machine User
 *
 * @author pang Development by Chusak laojang Date : Nov 1, 2012, Time :
 * 11:38:40 AM Copy Right 4 Xtreme Co.,Ltd.
 */
public class StdTime {

    private List<ReportSettingDatass> itemStdSetting;
    private List<ReportSettingInputData> itemReportSettingInputData;
    private StdTimeSettingJpaController stdTimeSettingJpaController = null;
    private ModelPoolJpaController modelPoolJpaController = null;
    private LineJpaController lineJpaController = null;
    private ProcessPoolJpaController processPoolJpaController = null;
    private boolean statusOpen = false;
    private ReportSettingSearchData currentReportSettingSearchDataSearch;
    private DataModel stdDataModel;
    private StdDataShow currentStdDataShow;
    private StdDataShowInput currentStdDataShowInput;
    private StdDataShowInputJpaController stdDataShowInputJpaController = null;
    private StdDataShowJpaController stdDataShowJpaController = null;
    private String indexOpenButton = null;
    private StdTimeSetting currentStdSetting;
    private CurrentProcessJpaController currentProcessJpaController = null;
    private CurrentProcess currentProcess;
    private PcsQtyJpaController pcsQtyJpaController = null;
    private PcsQty pcsQty;
    private String paln;

    private PcsQtyJpaController getPcsQtyJpaController() {
        if (pcsQtyJpaController == null) {
            pcsQtyJpaController = new PcsQtyJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return pcsQtyJpaController;
    }

    private CurrentProcessJpaController getCurrentProcessJpaController() {
        if (currentProcessJpaController == null) {
            currentProcessJpaController = new CurrentProcessJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return currentProcessJpaController;
    }

    private StdDataShowInputJpaController getDataShowInputJpaController() {
        if (stdDataShowInputJpaController == null) {
            stdDataShowInputJpaController = new StdDataShowInputJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return stdDataShowInputJpaController;
    }

    private StdDataShowJpaController getShowJpaController() {
        if (stdDataShowJpaController == null) {
            stdDataShowJpaController = new StdDataShowJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return stdDataShowJpaController;
    }

    private ModelPoolJpaController getModelPoolJpaController() {
        if (modelPoolJpaController == null) {
            modelPoolJpaController = new ModelPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return modelPoolJpaController;
    }

    private LineJpaController getLineJpaController() {
        if (lineJpaController == null) {
            lineJpaController = new LineJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lineJpaController;
    }

    private ProcessPoolJpaController getProcessPoolJpaController() {
        if (processPoolJpaController == null) {
            processPoolJpaController = new ProcessPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return processPoolJpaController;
    }

    private StdTimeSettingJpaController getStdTimeSettingJpaController() {
        if (stdTimeSettingJpaController == null) {
            stdTimeSettingJpaController = new StdTimeSettingJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return stdTimeSettingJpaController;
    }

    public String getFiller() {
        fc.print("end date :" + getCurrentReportSettingSearchDataSearch().getEndDate());
        fc.print("start date:" + getCurrentReportSettingSearchDataSearch().getStartDate());
        fc.print("group model:" + getCurrentReportSettingSearchDataSearch().getGroubModel());
        fc.print("line:" + getCurrentReportSettingSearchDataSearch().getLine());
        fc.print("model :" + getCurrentReportSettingSearchDataSearch().getModel());
        fc.print("process :" + getCurrentReportSettingSearchDataSearch().getProcess());
        String fillter = "";
        if (getCurrentReportSettingSearchDataSearch().getEndDate() != null) {
            fillter += " and current_process.`timeCurrent` <= Date('" + getCurrentReportSettingSearchDataSearch().getEndDate().toString() + "') ";
        }
        if (getCurrentReportSettingSearchDataSearch().getStartDate() != null) {
            fillter += " and current_process.`timeCurrent` >=Date('" + getCurrentReportSettingSearchDataSearch().getStartDate().toString() + "') ";
        }
        if (!getCurrentReportSettingSearchDataSearch().getGroubModel().equals("null")) {
            fillter += " and model_pool.`model_series`='" + getCurrentReportSettingSearchDataSearch().getGroubModel() + "' ";
        }
        if (!getCurrentReportSettingSearchDataSearch().getLine().equals("null")) {
            fillter += " and line.`line_id`='" + getCurrentReportSettingSearchDataSearch().getLine() + "' ";
        }
        if (!getCurrentReportSettingSearchDataSearch().getModel().equals("null")) {
            fillter += " and model_pool.`id`='" + getCurrentReportSettingSearchDataSearch().getModel() + "' ";
        }
        if (!getCurrentReportSettingSearchDataSearch().getProcess().equals("null")) {
            fillter += " and process_pool.`id_proc`='" + getCurrentReportSettingSearchDataSearch().getProcess() + "' ";
        }
        return fillter;
    }
    boolean statusInputButton = false;
    boolean statusSaveButton = false;
    boolean statusEditButton = false;
    private Date dated;

    public void changeToShow() throws SQLException, ParseException {
        numberOfColumns = checkSizeStdSetting();
        updateModel();
        prepareItemStdSetting();
        fc.print("******** Change to show *********");
        fc.print("**************************************");
        fc.print("Sql :" + Sql.generateSqlStdTime(getFiller()));

        com.mysql.jdbc.Connection conn = Sql.getConnection("mysql"); //connection SQL
        Statement stm = conn.createStatement();
        if (stm.execute(Sql.generateSqlStdTime(getFiller()))) {
            ResultSet result = stm.executeQuery(Sql.generateSqlStdTime(getFiller()));
            String[] dbSpecialColumn = new String[numberOfColumns + 9];
            while (result.next()) {
                prepareStdShow();
                currentStdDataShow = getShowJpaController().findCurrentProcessId(result.getObject(1).toString());
                for (int k = 0; k < numberOfColumns + 8; k++) {
                    if (k == 1) {
                        dbSpecialColumn[k] = (String) result.getObject(5);
                    } else if (k == 2) {
                        dbSpecialColumn[k] = (String) result.getObject(3);
                    } else if (k == 3) {
                        dbSpecialColumn[k] = (String) result.getObject(4);
                    } else if (k == 4) {
                        dbSpecialColumn[k] = "";
                    } else if (k == 5) {
                        dbSpecialColumn[k] = (String) result.getObject(7);
                    } else {
                        dbSpecialColumn[k] = "";
                    }
                }
                if (currentStdDataShow != null) {
                    List<StdTimeSetting> objSetting = getStdTimeSettingJpaController().findAllNotDelete();
                    int i = 6;
                    for (StdTimeSetting objList : objSetting) {
                        currentStdDataShowInput = new StdDataShowInput();
                        currentStdDataShowInput = getDataShowInputJpaController().findDataProcessAndSetting(currentStdDataShow, objList);

                        if (currentStdDataShowInput != null) {
                            dbSpecialColumn[i] = currentStdDataShowInput.getValue();
                        } else {
                            dbSpecialColumn[i] = "";
                        }
                        i++;
                    }
                    dbSpecialColumn[4] = currentStdDataShow.getPlan();
                    dbSpecialColumn[i] = currentStdDataShow.getTotalWorktime();
                    dbSpecialColumn[i + 1] = currentStdDataShow.getStandardTime();
                }
                dbSpecialColumn[0] = result.getObject(2).toString();
                dbSpecialColumn[numberOfColumns + 8] = result.getObject(1).toString();
                users.add(new ArrayList(Arrays.asList(dbSpecialColumn)));
                rowModel = new ListDataModel(users);
            }
        }
        setStdDataModel();
        currentReportSettingSearchDataSearch = new ReportSettingSearchData(null, null, "null", "null", "null", "null");
        fc.print("**************************************");
    }

    public void prepareItemStdSetting() {
        itemStdSetting = new ArrayList<ReportSettingDatass>();
        users = new ArrayList();
        worksheetpagination = null;
        rowModel = null;
        users = new ArrayList();
    }

    public void changeToSave(String indexSave) {
        fc.print("******** Change To Save *************" + indexSave);
        prepareStdShow();
        fc.print("******** Change To Save 2 *************" + indexSave);
        currentStdDataShow = getShowJpaController().findCurrentProcessId(indexSave);
//        fc.print("id *** " + currentStdDataShow.getIdCurrentProc());
        if (currentStdDataShow != null) {
            fc.print("***** edit *****");
            currentStdDataShow.setTotalWorktime(new DecimalFormat("#,###.#####").format(calculateInput()));
            currentStdDataShow.setPlan(paln);
            currentStdDataShow.setStandardTime(new DecimalFormat("#,###.#####").format((Double.parseDouble(getPcsQtyJpaController().findPcsQtyCurrentProcessId(getCurrentProcessJpaController().findCurrentProcess(indexSave)).getQty()) / calculateInput())));
            editStdTimeShow();
            for (ReportSettingInputData objList : getItemReportSettingInputData()) {
                prepareInputShowData();
                currentStdDataShowInput = getDataShowInputJpaController().findDataProcessAndSetting(currentStdDataShow, getStdTimeSettingJpaController().findStdTimeSetting(objList.getIndex()));
                if (currentStdDataShowInput != null) {
                    currentStdDataShowInput.setValue(objList.getValue());
                    try {
                        getDataShowInputJpaController().edit(currentStdDataShowInput);
                    } catch (Exception e) {
                        fc.print("Error not to edit" + e);
                    }
                }else
                { 
                    prepareInputShowData();
                    currentStdDataShowInput.setIdShow(currentStdDataShow);
                    currentStdDataShowInput.setValue(objList.getValue());
                    currentStdDataShowInput.setIdSettingName(currentStdSetting);
                    currentStdDataShowInput.setIdStdShowDataInput(gemId());
                    try {
                        getDataShowInputJpaController().create(currentStdDataShowInput);
                    } catch (Exception e) {
                        fc.print("Error not to create" + e);
                    }
                }
            }
        } else {

            fc.print("***** create ");
            prepareStdShow();
            currentStdDataShow.setIdCurrentProc(indexSave);
            currentStdDataShow.setIdStdShow(gemId());
            currentStdDataShow.setTotalWorktime("");
            currentStdDataShow.setPlan(paln);
            currentStdDataShow.setStandardTime("");
            createStdShow();
            for (ReportSettingInputData objList : getItemReportSettingInputData()) {
                prepareInputShowData();
                currentStdDataShowInput.setIdStdShowDataInput(gemId());
                currentStdDataShowInput.setIdShow(currentStdDataShow);
                currentStdDataShowInput.setIdSettingName(getStdTimeSettingJpaController().findStdTimeSetting(objList.getIndex()));
                currentStdDataShowInput.setValue(objList.getValue());
                createInputShowData();
            }
            prepareStdShow();
            currentStdDataShow = getShowJpaController().findCurrentProcessId(indexSave);
            currentStdDataShow.setTotalWorktime(new DecimalFormat("#,###.#####").format(calculateInput()));
            currentStdDataShow.setPlan(paln);
            currentStdDataShow.setStandardTime(new DecimalFormat("#,###.#####").format((Double.parseDouble(getPcsQtyJpaController().findPcsQtyCurrentProcessId(getCurrentProcessJpaController().findCurrentProcess(indexSave)).getQty()) / calculateInput())));
            editStdTimeShow();
        }
        fc.print("******** Change To Save 3 *************" + indexSave);
    }

    public void prepareInputShowData() {
        currentStdDataShowInput = new StdDataShowInput();
    }

    public void createInputShowData() {
        try {
            getDataShowInputJpaController().create(currentStdDataShowInput);
        } catch (Exception e) {
            fc.print("***** Error not to input show data *** " + e);
        }
    }

    public void editStdTimeShow() {
        try {
            getShowJpaController().edit(currentStdDataShow);
        } catch (Exception e) {
            fc.print("Error " + e);
        }

    }

    public String gemId() {
        return UUID.randomUUID().toString();
    }

    public void prepareStdShow() {
        currentStdDataShow = new StdDataShow();
    }

    public void createStdShow() {
        fc.print("******** create std show ************");
        try {
            getShowJpaController().create(currentStdDataShow);
        } catch (Exception e) {
            fc.print("**** Error not to create std show " + e);
        }
    }

    public void createStdData() {
        fc.print("******** create std data ************");
        try {
            getDataShowInputJpaController().create(currentStdDataShowInput);
        } catch (Exception e) {
            fc.print("****** error not to create std data " + e);
        }
    }

    public List<SelectItem> loadGroupModel() {
        List<SelectItem> groupModel = new ArrayList<SelectItem>();
        groupModel.add(new SelectItem("null", "--- Select one ---"));
        List<ModelPool> objModelPool = getModelPoolJpaController().findByGroupModelNotDeleted();
        if (!objModelPool.isEmpty()) {
            for (ModelPool objList : objModelPool) {
                groupModel.add(new SelectItem(objList.getId(), objList.getModelGroup()));
            }
        }
        return groupModel;
    }

    public List<SelectItem> loadModel() {
        List<SelectItem> modelItem = new ArrayList<SelectItem>();
        modelItem.add(new SelectItem("null", "--- Select one ---"));
        List<ModelPool> objModelPool = getModelPoolJpaController().findModelPoolByNotDeleted();
        if (!objModelPool.isEmpty()) {
            for (ModelPool objList : objModelPool) {
                String model = objList.getModelType() + "-" + objList.getModelSeries() + "-" + objList.getModelName();
                modelItem.add(new SelectItem(objList.getId(), model));
            }
        }
        return modelItem;
    }

    public List<SelectItem> loadLine() {
        List<SelectItem> lineItem = new ArrayList<SelectItem>();
        lineItem.add(new SelectItem("null", "--- Select one ---"));
        List<Line> objLine = getLineJpaController().findAllLineNotDeletd();
        if (!objLine.isEmpty()) {
            for (Line objList : objLine) {
                lineItem.add(new SelectItem(objList.getLineId(), objList.getLineName()));
            }
        }
        return lineItem;
    }

    public List<SelectItem> loadProcess() {
        List<SelectItem> processItem = new ArrayList<SelectItem>();
        processItem.add(new SelectItem("null", "--- Select one ---"));
        List<ProcessPool> objProcessPool = getProcessPoolJpaController().findProcessPoolByNotdeleted();
        if (!objProcessPool.isEmpty()) {
            for (ProcessPool objList : objProcessPool) {
                processItem.add(new SelectItem(objList.getIdProc(), objList.getProcName()));
            }
        }
        return processItem;
    }

    public List<ReportSettingDatass> getItemStdSetting() {
        if (itemStdSetting == null) {
            itemStdSetting = new ArrayList<ReportSettingDatass>();
        }
        return itemStdSetting;
    }

    public void setItemStdSetting(List<ReportSettingDatass> itemStdSetting) {
        this.itemStdSetting = itemStdSetting;
    }

    public void openPopUpInput(String type, String index) {
        fc.print("**** index *****" + index);
        if (type.equals("open")) {
            fc.print("******** index current process input *********");
            indexOpenButton = index;
            statusOpen = true;
            loadStdSettingAll(index);
        } else if (type.equals("calculate")) {
            fc.print("****************** calculate ***************");
            try {
                statusOpen = false;
                for (ReportSettingDatass objList : getItemStdSetting()) {
                    if (objList.getIndex().equals(index)) {
                        objList.setSaveButton(true);
                        objList.setInputStatus(false);
                        objList.setEditButton(true);
                        objList.setTotalworkTime(new DecimalFormat("#,###.#####").format(calculateInput()));
                        objList.setStdTime(new DecimalFormat("#,###.#####").format((Double.parseDouble(objList.getProductFG()) / calculateInput())));
                    }
                }
                setStdDataModel();
                changeToSave(index);
                changeToShow();
            } catch (SQLException ex) {
                Logger.getLogger(StdTime.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ParseException ex) {
                Logger.getLogger(StdTime.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            fc.print("*******");
            statusOpen = false;
        }
    }

    public double calculateInput() {
        double sum = 0;
        for (ReportSettingInputData objList : getItemReportSettingInputData()) {
            prepareStdSetting();
            currentStdSetting = getStdTimeSettingJpaController().findStdTimeSetting(objList.getIndex());
            if (objList.getIndex().equals(currentStdSetting.getIdStdSetting())) {
                if (currentStdSetting.getHours().equals("0")) {
                    sum += (Double.parseDouble(objList.getValue()) * Double.parseDouble("0." + currentStdSetting.getMinute()));
                }
                if (currentStdSetting.getMinute().equals("0")) {
                    sum += (Double.parseDouble(objList.getValue()) * Double.parseDouble(currentStdSetting.getHours()));
                }
                if (!(currentStdSetting.getHours().equals("0") && currentStdSetting.getMinute().equals("0"))) {
                    sum += (Double.parseDouble(objList.getValue()) * Double.parseDouble(currentStdSetting.getHours() + "." + currentStdSetting.getMinute()));
                }
            }
        }
        fc.print("****** sum *******" + sum);
        return sum;
    }

    public void prepareStdSetting() {
        currentStdSetting = new StdTimeSetting();
    }

    public void changeToExport() {
        fc.print("***** Change To Export ******");

    }

    public void loadStdSettingAll(String indexProcess) {
        fc.print("***** load srd setting all :" + indexProcess);
        List<StdTimeSetting> objStdTimeSetting = getStdTimeSettingJpaController().findAllNotDelete();
        prepareLoadSettingAll();
        StdDataShow objStdShow = getShowJpaController().findCurrentProcessId(indexProcess);
        if (objStdShow == null) {
            for (StdTimeSetting objList : objStdTimeSetting) {
                getItemReportSettingInputData().add(new ReportSettingInputData(objList.getIdStdSetting(), objList.getHeaderName(), null));
            }
        } else {
            String indexShow = getShowJpaController().findCurrentProcessId(indexProcess).getIdStdShow();
            paln = getShowJpaController().findCurrentProcessId(indexProcess).getPlan();
            for (StdTimeSetting objList : objStdTimeSetting) {
                prepareInputShowData();

                String indexSetting = objList.getIdStdSetting();
                fc.print("indexShow :" + indexShow + "  : indexSetting " + indexSetting);
                currentStdDataShowInput = getDataShowInputJpaController().findDataProcessAndSetting(getShowJpaController().findCurrentProcessId(indexProcess), objList);
                if (currentStdDataShowInput != null) {
                    getItemReportSettingInputData().add(new ReportSettingInputData(objList.getIdStdSetting(), objList.getHeaderName(), currentStdDataShowInput.getValue()));
                } else {
                    getItemReportSettingInputData().add(new ReportSettingInputData(objList.getIdStdSetting(), objList.getHeaderName(), null));
                }
            }
        }
    }

    public void previous() {
        getWorksheetpagination().previousPage();
        setRowModel();
    }

    public void next() {
        getWorksheetpagination().nextPage();
        setRowModel();
    }

    public int getAllPages() {
        int all = getWorksheetpagination().getItemsCount();
        int pagesize = getWorksheetpagination().getPageSize();
        return ((int) all / pagesize) + 1;
    }
    private PaginationHelper worksheetpagination;

    public PaginationHelper getWorksheetpagination() {
        if (worksheetpagination == null) {
            worksheetpagination = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    //fc.print("size of data : "+getMasterListViewData().size());
                    return users.size();
                }

                @Override
                public DataModel createPageDataModel() {
                    fc.print("PageFirst : " + getPageFirstItem());
                    fc.print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (users.size() > 0) {
                        listmodel = new ListDataModel(users.subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(users);
                    }
                    return listmodel;
                }
            };
        }
        return worksheetpagination;
    }

    public void prepareLoadSettingAll() {
        itemReportSettingInputData = new ArrayList<ReportSettingInputData>();
    }

    public boolean isStatusOpen() {
        return statusOpen;
    }

    public void setStatusOpen(boolean statusOpen) {
        this.statusOpen = statusOpen;
    }

    public List<ReportSettingInputData> getItemReportSettingInputData() {
        if (itemReportSettingInputData == null) {
            itemReportSettingInputData = new ArrayList<ReportSettingInputData>();
        }
        return itemReportSettingInputData;
    }

    public void setItemReportSettingInputData(List<ReportSettingInputData> itemReportSettingInputData) {
        this.itemReportSettingInputData = itemReportSettingInputData;
    }

    public ReportSettingSearchData getCurrentReportSettingSearchDataSearch() {
        if (currentReportSettingSearchDataSearch == null) {
            currentReportSettingSearchDataSearch = new ReportSettingSearchData(null, null, null, null, null, null);
        }
        return currentReportSettingSearchDataSearch;
    }

    public void setCurrentReportSettingSearchDataSearch(ReportSettingSearchData currentReportSettingSearchDataSearch) {
        this.currentReportSettingSearchDataSearch = currentReportSettingSearchDataSearch;
    }

    public DataModel getStdDataModel() {
        return stdDataModel;
    }

    public void setStdDataModel() {
        this.stdDataModel = getWorksheetpagination().createPageDataModel();
    }

    public StdDataShow getCurrentStdDataShow() {
        if (currentStdDataShow == null) {
            currentStdDataShow = new StdDataShow();
        }
        return currentStdDataShow;
    }

    public void setCurrentStdDataShow(StdDataShow currentStdDataShow) {
        this.currentStdDataShow = currentStdDataShow;
    }

    public StdDataShowInput getCurrentStdDataShowInput() {
        if (currentStdDataShowInput == null) {
            currentStdDataShowInput = new StdDataShowInput();
        }
        return currentStdDataShowInput;
    }

    public void setCurrentStdDataShowInput(StdDataShowInput currentStdDataShowInput) {
        this.currentStdDataShowInput = currentStdDataShowInput;
    }

    public boolean isStatusEditButton() {
        return statusEditButton;
    }

    public void setStatusEditButton(boolean statusEditButton) {
        this.statusEditButton = statusEditButton;
    }

    public boolean isStatusSaveButton() {
        return statusSaveButton;
    }

    public void setStatusSaveButton(boolean statusSaveButton) {
        this.statusSaveButton = statusSaveButton;
    }

    public boolean isStatusInputButton() {
        return statusInputButton;
    }

    public void setStatusInputButton(boolean statusInputButton) {
        this.statusInputButton = statusInputButton;
    }

    public Date getDated() {
        return dated;
    }

    public void setDated(Date dated) {
        this.dated = dated;
    }

    public String getIndexOpenButton() {
        return indexOpenButton;
    }

    public void setIndexOpenButton(String indexOpenButton) {
        this.indexOpenButton = indexOpenButton;
    }

    public StdTimeSetting getCurrentStdSetting() {
        return currentStdSetting;
    }

    public void setCurrentStdSetting(StdTimeSetting currentStdSetting) {
        this.currentStdSetting = currentStdSetting;
    }
    private List users = new ArrayList();
    private DataModel rowModel;
    private DataModel columnsModel = null;
    private int numberOfColumns = checkSizeStdSetting();

    public int checkSizeStdSetting() {
        List<StdTimeSetting> objSetting = getStdTimeSettingJpaController().findAllNotDelete();
        return objSetting.size();
    }

    public String inconlumn() {
        int numprint = checkSizeStdSetting() + 8;
        String numPrintStr = "";
        for (int i = 0; i < numprint; i++) {
            numPrintStr += i;
            if (i < numprint) {
                numPrintStr += ",";
            }
        }
        return numPrintStr;
    }

    public void loadDataSpecailColumnData(int sizeRow) {
        fc.print("***** load column special data *****");
        String[] dbSpecialColumn = new String[numberOfColumns + 9];

        for (int k = 0; k < numberOfColumns + 8; k++) {
            dbSpecialColumn[k] = "" + k;
            System.out.print("(" + k + ") ");
        }
        dbSpecialColumn[numberOfColumns + 8] = "index";
//        }
        String[] dbSpecialColumn2 = new String[numberOfColumns + 9];

        for (int kk = 0; kk < numberOfColumns + 8; kk++) {
            dbSpecialColumn2[kk] = "" + kk + 2;
            System.out.print("(" + kk + ") ");
        }
        dbSpecialColumn2[numberOfColumns + 8] = "index";
        users.add(new ArrayList(Arrays.asList(dbSpecialColumn)));
        users.add(new ArrayList(Arrays.asList(dbSpecialColumn2)));
        rowModel = new ListDataModel(users);
    }

    public DataModel getRowModel() {
        return rowModel;
    }

    public void setRowModel() {
        this.rowModel = getWorksheetpagination().createPageDataModel();
    }

    public DataModel getColumnsModel() {
        if (columnsModel == null) {
            updateModel();
        }
        return columnsModel;
    }

    public void setColumnsModel(DataModel columnsModel) {
        this.columnsModel = columnsModel;
    }

    public Object getCellValue() {
        if (rowModel.isRowAvailable() && columnsModel.isRowAvailable()) {
            int col = columnsModel.getRowIndex();
//            fc.print("*** col number *** :" + col);
            return ((List) rowModel.getRowData()).get(col).toString();
//            return  "test";
        }
        return null;
    }

    public void updateModel() {
        fc.print("numberOfColumns : " + numberOfColumns);
        String[] array = new String[numberOfColumns + 8];
        array[0] = "Date";
        array[1] = "Type";
        array[2] = "Line";
        array[3] = "Process";
        array[4] = "Plan";
        array[5] = "Product(F/G)";
        List<StdTimeSetting> objSetting = getStdTimeSettingJpaController().findAllNotDelete();
        int i = 6;
        for (StdTimeSetting objList : objSetting) {
            array[i] = objList.getHeaderName();
            i++;
        }
        array[6 + numberOfColumns] = "Total work time";
        array[7 + numberOfColumns] = "Statndared Time";
        columnsModel = new ArrayDataModel(array);
    }

    public void setNumberOfColumns(int numberOfColumns) {
        this.numberOfColumns = numberOfColumns;
    }

    public int getNumberOfColumns() {
        return numberOfColumns;
    }

    public void changToEditSpecail(ActionEvent event) {
        int indexPos = numberOfColumns + 8;
        fc.print("index" + indexPos);
        System.out.println(rowModel.getRowData());
        String index = rowModel.getRowData().toString();
        String ar[] = index.split(",");
        String ar2[] = ar[indexPos].split("]");
        String ar3[] = ar2[0].split(" ");
        fc.print(" my index ***'" + ar3[1] + "'");
        indexOpenButton = ar3[1];
        paln = "";
        openPopUpInput("open", indexOpenButton);
    }

    public CurrentProcess getCurrentProcess() {
        return currentProcess;
    }

    public void setCurrentProcess(CurrentProcess currentProcess) {
        this.currentProcess = currentProcess;
    }

    public PcsQty getPcsQty() {
        return pcsQty;
    }

    public void setPcsQty(PcsQty pcsQty) {
        this.pcsQty = pcsQty;
    }

    public String getPaln() {
        return paln;
    }

    public void setPaln(String paln) {
        this.paln = paln;
    }
}
