/**
 *
 */
package com.appCinfigpage.bean;

import com.tct.data.Users;
import com.tct.data.jpa.UsersJpaController;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.persistence.Persistence;
import org.icefaces.ace.component.fileentry.FileEntry;
import org.icefaces.ace.component.fileentry.FileEntryEvent;
import org.icefaces.ace.component.fileentry.FileEntryResults;

/**
 * @author The_Boy_Cs
 *
 */
public class createUser {

    private String id;
    private String id_role;
    private String firstname;
    private String lastname;
    private String username;
    private String password;
    private String department;
    private String status;
    private Date datecreate;
    private String description;
    private int count = 0;
    private List<role> rolelist;
    private List<String> fileData;

    private Users current = null;
    private int selectedItemIndex;
    private UsersJpaController jpaController = null;
    
    public createUser() {
        loadRoleList();
    }

    public Users getSelected() {
        if (current == null) {
            current = new Users();
            selectedItemIndex = -1;
        }
        return current;
    }
    
    private UsersJpaController getJpaController() {
        if (jpaController == null) {
            jpaController = new UsersJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return jpaController;
    }
    
    public void create(ActionEvent event) {
        try {
            System.out.println("id = "+UUID.randomUUID().toString());
            current.setId(UUID.randomUUID().toString());
            System.out.println("id = "+UUID.randomUUID().toString());
            getJpaController().create(current);
//            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("UsersCreated"));
             prepareCreate();
        } catch (Exception e) {
            System.out.println("= Error : ");
//            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            //return null;
        }
    }
    public String prepareCreate() {
        current = new Users();
        selectedItemIndex = -1;
        return "Create";
    }
    
    private void loadRoleList() {
        count = 0;
        rolelist = new ArrayList<role>();
        rolelist.add(new role("admin"));
        rolelist.add(new role("user1"));
        rolelist.add(new role("user2"));
    }

    public void ValueChangStatus(ValueChangeEvent event) {
        System.out.println("Status : " + event.getNewValue());

    }

    public void createNewrole(ActionEvent event) {
        System.out.println("Event : " + event);

    }

    public void FileListener(FileEntryEvent e) {
        System.out.println("FileEntry");
        FileEntry fe = (FileEntry) e.getComponent();
        FileEntryResults results = fe.getResults();
        File parent = null;

        fileData = new ArrayList<String>();

        //get data About File

        for (FileEntryResults.FileInfo i : results.getFiles()) {
            fileData.add("File Name: " + i.getFileName());

            if (i.isSaved()) {
                fileData.add("File Size: " + i.getSize() + " bytes");

                File file = i.getFile();
                if (file != null) {
                    parent = file.getParentFile();
                }
            } else {
                fileData.add("File was not saved because: "
                        + i.getStatus().getFacesMessage(
                        FacesContext.getCurrentInstance(),
                        fe, i).getSummary());
            }
        }

        if (parent != null) {
            long dirSize = 0;
            int fileCount = 0;
            for (File file : parent.listFiles()) {
                fileCount++;
                dirSize += file.length();
            }
            fileData.add("Total Files In Upload Directory: " + fileCount);
            fileData.add("Total Size of Files In Directory: " + dirSize + " bytes");
        }
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the id_role
     */
    public String getId_role() {
        return id_role;
    }

    /**
     * @param id_role the id_role to set
     */
    public void setId_role(String id_role) {
        this.id_role = id_role;
    }

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the datecreate
     */
    public Date getDatecreate() {
        return datecreate;
    }

    /**
     * @param datecreate the datecreate to set
     */
    public void setDatecreate(Date datecreate) {
        this.datecreate = datecreate;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the rolelist
     */
    public List<role> getRolelist() {
        return rolelist;
    }

    /**
     * @param rolelist the rolelist to set
     */
    public void setRolelist(List<role> rolelist) {
        this.rolelist = rolelist;
    }

    /**
     * @return the count
     */
    public int getCount() {
        if (count >= rolelist.size()) {
            count = 0;
        }
        count++;
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * @return the fileData
     */
    public List<String> getFileData() {
        return fileData;
    }

    /**
     * @param fileData the fileData to set
     */
    public void setFileData(List<String> fileData) {
        this.fileData = fileData;
    }

    public class role {

        private String name;

        public role(String name) {
            this.name = name;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }
    }
}
