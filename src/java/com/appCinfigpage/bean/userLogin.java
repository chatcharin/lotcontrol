/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appCinfigpage.bean;

import com.checkrole.bean.CheckRoleUser;
import com.moduleBackLine.bean.createAssyWorksheet;
import com.moduleFontline.bean.FrontLineControl;
import com.moduleFontline.bean.worksheet;
import com.shareData.ShareDataOfSession;
import com.tct.data.Users;
import com.tct.data.jpa.RoleJpaController;
import com.tct.data.jpa.RoleUserJpaController;
import com.tct.data.jpa.UsersJpaController;
import com.tct.menubar.MenuBarBean;
import java.io.IOException;
import java.util.Locale;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.Persistence;

/**
 *
 * @author The_Boy_Cs
 */
public class userLogin {

    private String username;
    private String password;
    private static final String user = "user";
    private UsersJpaController jpaController = null;
    private RoleJpaController jpaRoleController = null;
    private Users current = null;
    private int selectedItemIndex;
    private String firstname;
    private String lastname;

    public userLogin() {
        checkLogin();
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String logins() {
        Locale.setDefault(new Locale("en", "US"));
        String log = "login";
        System.out.println("Test0 : " + username);
        try {
            current = getUsersJpaController().findByUsername(username);
            System.out.println("Test1 : ");
            if (current != null && current.getUserName().equals(username) && current.getUserPassword().equals(password)) {
                if (setSessionAttribute(current)) {
                    log = "main?Redirect";
                    firstname = current.getFirstName();
                    lastname = current.getLastName();
                    FacesContext fc = FacesContext.getCurrentInstance();
                    ExternalContext externalContext = fc.getExternalContext();
                    externalContext.redirect("main.xhtml");
                    System.out.println("Test2");
                    setMenu();
                }
                System.out.println("Test3");
            }
        } catch (Exception e) {
            //e.printStackTrace();
            System.err.println("Error login :" + e.getMessage());
        }
        System.out.println("Status : " + log);
        return log;
    }

    public void checkLogin() {
        Locale.setDefault(new Locale("en", "US"));
        System.out.println("Check Login.");
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext externalContext = fc.getExternalContext();
        externalContext.getSession(true);
        if (getSessionAttribute()) {
            try {
                System.out.println("Login Complete");
                externalContext.redirect("main.xhtml");
            } catch (IOException ioex) {
                print("= Error : " + ioex);
            }
//            return "main";
        }else{
            try {
                System.out.println("You don't login.");
                externalContext.redirect("login.xhtml");
            } catch (IOException ioex) {
                print("= Error : " + ioex);
            }
        }
//        return "login";
    }

    public String logout() {
        String log = "";
        try {
//            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("currentUser", null);
            FacesContext fc = FacesContext.getCurrentInstance();

            ExternalContext externalContext = fc.getExternalContext();
            externalContext.getSessionMap().put("currentUser", null);
            externalContext.invalidateSession();
            externalContext.redirect("login.xhtml");
            log = "login";
            System.out.println("Logout : ");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return log;
    }

    private boolean setSessionAttribute(Users current) {
        try {
            FacesContext context = FacesContext.getCurrentInstance();

            context.getExternalContext().getSessionMap().put("currentUser", current);
            context.getExternalContext().getSessionMap().put("sharedata", new ShareDataOfSession());
            return true;
        } catch (Exception e) {
            System.out.println("Error : setSession.");
            return false;
        }
    }

    public static Users getSessionUser() {
        FacesContext context = FacesContext.getCurrentInstance();
        Users currentuser = (Users) context.getExternalContext().getSessionMap().get("currentUser");
        return currentuser;
    }

    public Users getCurrentUser() {
        return getSessionUser();
    }

    public static MenuBarBean getSessionMenuBarBean() {
        FacesContext context = FacesContext.getCurrentInstance();
        MenuBarBean menu = (MenuBarBean) context.getExternalContext().getSessionMap().get("menuBar");
        System.out.println("menu" + menu.getParam());
        return menu;
    }

    public static worksheet getSessionWorksheet() {
        FacesContext context = FacesContext.getCurrentInstance();
        worksheet worksheets = (worksheet) context.getExternalContext().getSessionMap().get("worksheet");
        return worksheets;
    }

    private FacesContext getContext() {
        return FacesContext.getCurrentInstance();
    }

    private ExternalContext getExternalContext() {
        return getContext().getExternalContext();
    }

    public static FrontLineControl getSessionFrontLineControl() {
        FacesContext context = FacesContext.getCurrentInstance();
        return (FrontLineControl) context.getExternalContext().getSessionMap().get("frontlinecontrol");
    }

    public static createAssyWorksheet getSessionCreateAssyworksheet() {
        FacesContext context = FacesContext.getCurrentInstance();
        createAssyWorksheet createassyworksheet = (createAssyWorksheet) context.getExternalContext().getSessionMap().get("createassyworksheet");
        return createassyworksheet;
    }

    private void print(String str) {
        System.out.println(str);
    }

    private boolean getSessionAttribute() {
        try {
            System.out.println("Check session : User : " + getSessionUser().getUserName());
            if (getSessionUser() != null) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            //e.printStackTrace();
            return false;
        }
    }

    public Users getSelected() {
        if (current == null) {
            current = new Users();
            selectedItemIndex = -1;
        }
        return current;
    }

    private UsersJpaController getUsersJpaController() {
        if (jpaController == null) {
            jpaController = new UsersJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return jpaController;
    }

    private RoleJpaController getRoleJpaController() {
        if (jpaRoleController == null) {
            jpaRoleController = new RoleJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return jpaRoleController;
    }
    private RoleUserJpaController jparoleusercontroller;

    private RoleUserJpaController getRoleUserJpaController() {
        if (jparoleusercontroller == null) {
            jparoleusercontroller = new RoleUserJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return jparoleusercontroller;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    private boolean menuFl = true;

    public void setMenu() {
        CheckRoleUser editButtonClaim = new CheckRoleUser();
        if (!editButtonClaim.check("see_fl")) {
            menuFl = false;
            menufronline = "display:none";
            print(menufronline);
        }
    }
    private String menufronline = "display:block";

    public void setMenufronline(String menufronline) {
        this.menufronline = menufronline;
    }

    public String getMenufronline() {
        return menufronline;
    }

    /**
     * @return the menuFl
     */
    public boolean isMenuFl() {
        return menuFl;
    }

    /**
     * @param menuFl the menuFl to set
     */
    public void setMenuFl(boolean menuFl) {
        this.menuFl = menuFl;
    }
}
