/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appCinfigpage.bean;

import com.appConfigpage.Data.roleDetail;
import com.tct.data.Role;
import com.tct.data.Users;
import com.tct.data.jpa.RoleJpaController;
import com.tct.data.jpa.RoleUserJpaController;
import com.tct.data.jpa.UsersJpaController;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jsf.util.PaginationHelper;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;
import org.icefaces.ace.component.fileentry.FileEntry;
import org.icefaces.ace.component.fileentry.FileEntryEvent;
import org.icefaces.ace.component.fileentry.FileEntryResults;
import com.tct.data.RoleUser;
import com.time.timeCurrent;
import java.util.*;
import javax.faces.model.ArrayDataModel;
/**
 *
 * @author The_Boy_Cs
 */
public class UsersControl {
//========= search ===============================

    private String usernameSearch;
    private String firshnameSearch;
    private String positionSearch;
//  === 
    private boolean uppicvisible = false;
//  ===
    private Users currentusers;
    private String showpopup;
    private String closepopup;
//  ===
    private List<Users> userlist;
    private DataModel<Users> usersdatamodel;

    public UsersControl() {
        changToSearch();
        //loadRoleList();
    }

    public String getUserpic() {
        try {
            String fileparth = "./images/usersphoto";
            currentusers = userLogin.getSessionUser();
            if (getCurrentusers().getUserPicture() == null && getCurrentusers().getUserPicture().isEmpty()) {
                getCurrentusers().setUserPicture("no_image.jpg");
            }
            return fileparth += "/" + getCurrentusers().getUserPicture();
        } catch (Exception e) {
            print("error1 : " + e);
            return "../images/no_image.jpg";
        }
    }

    public void create(ActionEvent event) {
        print("=============== create ==========================================");
        try {
            getCurrentusers().setCreateDate(new Date());
            currentusers.setId(UUID.randomUUID().toString());
            getUsersJpaController().create(currentusers);

            if(!itemRoleDetail.isEmpty()){
            for(roleDetail listRole:itemRoleDetail)
            {
                String idRoleUser  = UUID.randomUUID().toString();
                getRoleUser().setId(idRoleUser);
                Role  roleList  =  getRoleJpaController().findRole(listRole.getNumber());
                getRoleUser().setRoleId(roleList);
                getRoleUser().setUsersId(getCurrentusers());
                getRoleUser().setUserCreate(userLogin.getSessionUser().getUserName());
                getRoleUser().setDeleted(0);
                getRoleUser().setCreateDate(new timeCurrent().getDate());
                try{
                    getRoleUserJpaController().create(getRoleUser());
                    roleUser   = new RoleUser();
                }
                catch(Exception e)
                {
                    fc.print("Error not to insert role"+e);
                }
            }
            }
            
            String link = "pages/appConfig/users/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            changToSearch();
        } catch (Exception e) {
            print("= create Error : " + e);
        }
    }

    public void listener(FileEntryEvent event) {
        print("====================== listener ===============");
        try {
            FileEntry fileEntry = (FileEntry) event.getSource();
            FileEntryResults results = fileEntry.getResults();
            String partfile = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/images/usersphoto/");
            for (FileEntryResults.FileInfo fileInfo : results.getFiles()) {
                if (fileInfo.isSaved()) {
                    print("== filename 1 : " + fileInfo.getFileName());
                    File file = fileInfo.getFile();
                    print("=== file name 2 : " + file.getName());
//                    String filename = file.getName();
                    String[] arrname = file.getName().split("\\.");
                    print("= Lenght : " + arrname.length);
                    print(" == filetype : " + arrname[1]);
                    String filename = getCurrentusers().getId() + "." + arrname[arrname.length - 1];
                    print("= file name 3 : " + filename);
                    partfile += "/" + filename;
                    if (writFile(file, partfile)) {
                        getCurrentusers().setUserPicture(filename);

                        showpopup = "";
                        closepopup = "window.close();";
//                        Thread.sleep(500);
//                        uppicvisible = false;
                    }
                }
            }
        } catch (Exception e) {
            print("Error0 : " + e);
        }
    }

    public void uppic(ActionEvent event) {
        print("=================== show up pic =================================");
        closepopup = "";
        showpopup = "window.showModalDialog('pages/appConfig/users/popupuploadpic.xhtml', 'Upload pic', 'dialig');";
//        uppicvisible = true;
        print("uppicvisible : " + uppicvisible);
    }

    public void changToCanceluppic(ActionEvent event) {
        print("================ chang cancel uppic =============================");
        uppicvisible = false;
        print("uppicvisible : " + uppicvisible);
    }

    public void changToCancel(ActionEvent event) {
        print("================== cancel create ================================");
        try {
            currentusers = null;
            itemRoleDetail  = null;
            itemrole  = null;
            itemRoleUser = null;
            objRole  = null;
            objRoleUse  = null;
            String link = "pages/appConfig/users/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (Exception e) {
            print("= cancel Error : " + e);
        }
    }

    public void changToEdit(ActionEvent event) {
        print("========== Edit =================================================");
        try {
            currentusers = new Users(); 
            currentusers = getUsersdatamodel().getRowData();
            print("= User Id : "+currentusers.getId());
            String link = "pages/appConfig/users/editview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (Exception e) {
            print("= edit Error : " + e);
        }
            objRole  = new ArrayList<Role>();
            itemRoleDetail = new ArrayList<roleDetail>();
            itemrole   = new ArrayList<SelectItem>();
            loadRoleListEdit();
    }

    public void changToSave(ActionEvent event) {
        print("============ changToSave ========================================");
        try {
            print("= User name : "+getCurrentusers().getUserName());
            getUsersJpaController().edit(getCurrentusers());
            String link = "pages/appConfig/users/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            if(!itemRoleDetail.isEmpty()){
            for(roleDetail listRole:itemRoleDetail)
            {
                RoleUser objRole = getRoleUserJpaController().findRoleUser(listRole.getIndex());
                if(objRole==null)
                { 
                String idRoleUser  = UUID.randomUUID().toString();
                getRoleUser().setId(idRoleUser);
                Role  roleList  =  getRoleJpaController().findRole(listRole.getNumber());
                getRoleUser().setRoleId(roleList);
                getRoleUser().setUsersId(getCurrentusers());
                getRoleUser().setUserCreate(userLogin.getSessionUser().getUserName());
                getRoleUser().setDeleted(0);
                getRoleUser().setCreateDate(new timeCurrent().getDate());
                try{
                    getRoleUserJpaController().create(getRoleUser());
                    roleUser   = new RoleUser();
                }
                catch(Exception e)
                {
                    fc.print("Error not to insert role"+e);
                }
                }
            }
            }
            currentusers = null;
            itemRoleDetail  = null;
            itemrole  = null;
            itemRoleUser = null;
            objRole  = null;
            objRoleUse  = null;
            changToSearch();
        } catch (NonexistentEntityException nex) {
            print("= saveError : " + nex);
        } catch (Exception e) {
            print("= saveError : " + e);
        }
    }

    public void changToInsert(ActionEvent event) {
        print("=================== Insert ======================================"); 
        objRole  = new ArrayList<Role>();
        itemRoleDetail = null;
        itemrole   = new ArrayList<SelectItem>();
        loadRoleList();
        
        try {
            currentusers = null;
            String link = "pages/appConfig/users/createview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (Exception e) {
            print("= insert Error : " + e);
        }
    }

    private boolean writFile(File file, String filePath) throws FileNotFoundException, IOException {
        print("========== Create file ==================================");
        print("= file parth : " + filePath);
        try {
            File outputFile = new File(filePath);
            FileInputStream in = new FileInputStream(file);
            FileOutputStream out = new FileOutputStream(outputFile);
            int c;
            while ((c = in.read()) != -1) {
                out.write(c);
            }
            file.delete();
            in.close();
            out.close();
            return true;
        } catch (IOException ioe) {
            print("= IO Error3 : " + ioe);
            return false;
        } catch (Exception e) {
            print("= Error2 : " + e);
            return false;
        }
    }

    public void changToSearch() {
        print("=============== Search ==========================================");
        if (usernameSearch !=null && !usernameSearch.trim().isEmpty()) {
            searchByUsername();
        }else if(firshnameSearch != null && !firshnameSearch.trim().isEmpty())
        {
            serchByFirstName();
//        }else if(positionSearch !=null && positionSearch.trim().isEmpty())
//        {
//            searchByPosition();
        }
        else {
            loadUserlist();
        }
        usernameSearch = "";
        firshnameSearch= "";
        positionSearch ="";
    }

    private void loadUserlist() {
        print("======== load Users =============================================");
        try {
            userlist = getUsersJpaController().findAll();
            setUsersdatamodel();
        } catch (Exception e) {
            print("= loaduser Error : " + e);
        }
    }
    private UsersJpaController usersJpaController;

    private UsersJpaController getUsersJpaController() {
        if (usersJpaController == null) {
            usersJpaController = new UsersJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return usersJpaController;
    }

    private void print(String str) {
        System.out.println(str);
    }

    public String getFirshnameSearch() {
        return firshnameSearch;
    }

    public void setFirshnameSearch(String firshnameSearch) {
        this.firshnameSearch = firshnameSearch;
    }

    public String getPositionSearch() {
        return positionSearch;
    }

    public void setPositionSearch(String positionSearch) {
        this.positionSearch = positionSearch;
    }

    public String getUsernameSearch() {
        return usernameSearch;
    }

    public void setUsernameSearch(String usernameSearch) {
        this.usernameSearch = usernameSearch;
    }

    public boolean isUppicvisible() {
        return uppicvisible;
    }

    public void setUppicvisible(boolean uppicvisible) {
        this.uppicvisible = uppicvisible;
    }

    public Users getCurrentusers() {
        if (currentusers == null) {
            currentusers = new Users(UUID.randomUUID().toString());
        }
        return currentusers;
    }

    public void setCurrentusers(Users currentusers) {
        this.currentusers = currentusers;
    }

    public String getClosepopup() {
        return closepopup;
    }

    public void setClosepopup(String closepopup) {
        this.closepopup = closepopup;
    }

    public String getShowpopup() {
        return showpopup;
    }

    public void setShowpopup(String showpopup) {
        this.showpopup = showpopup;
    }

    public List<Users> getUserlist() {
        if (userlist == null) {
            userlist = new ArrayList<Users>();
            userlist.add(new Users());
        }
        return userlist;
    }

    public void setUserlist(List<Users> userlist) {
        this.userlist = userlist;
    }

    public DataModel<Users> getUsersdatamodel() {
        return usersdatamodel;
    }

    public void setUsersdatamodel() {
        if(usersdatamodel ==null)
        {
            usersdatamodel  = new ArrayDataModel<Users>();
        }
        this.usersdatamodel = getUsermodelpagination().createPageDataModel();
    }
    public int getAllpages(){
        int allpages = getUsermodelpagination().getItemsCount() / getUsermodelpagination().getPageSize();
        return allpages+1;
    }
    public void previous() {
        getUsermodelpagination().previousPage();
        setUsersdatamodel();
    }

    public void next() {
        getUsermodelpagination().nextPage();
        setUsersdatamodel();
    }
    private PaginationHelper usermodelpagination;

    public PaginationHelper getUsermodelpagination() {
        if (usermodelpagination == null) {
            usermodelpagination = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    return getUserlist().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    print("PageFirst : " + getPageFirstItem());
                    print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (getUserlist().size() > 0) {
                        listmodel = new ListDataModel(getUserlist().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(getUserlist());
                    }
                    return listmodel;
                }
            };
        }
        return usermodelpagination;
    }
    //@pang
    private  RoleJpaController   roleJpaController  = null;
    private  RoleUserJpaController roleUserJpaController = null;
    
    public RoleJpaController  getRoleJpaController()
    {
        if(roleJpaController == null)
        {
            roleJpaController = new RoleJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return roleJpaController;
    }
    public  RoleUserJpaController getRoleUserJpaController()
    {
        if(roleUserJpaController ==null)
        {
            roleUserJpaController  = new RoleUserJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return roleUserJpaController;
    }
    
    private  List<SelectItem>  itemrole ;
    private  List<Role>  objRole;  
    private  String  roleUseIndex = null;
    private  RoleUser currentRoleUser  = null;
    private  Role     objRoleUse  = null;
    private  List<roleDetail>  itemRoleDetail ;
    private  RoleUser   roleUser = null;
    private  List<RoleUser>     itemRoleUser ;
    private  List<String> indexRoleUsers;

    public List<String> getIndexRoleUsers() {
        if(indexRoleUsers == null)
        {
            indexRoleUsers  = new ArrayList<String>();
        }
        return indexRoleUsers;
    }

    public void setIndexRoleUsers(List<String> indexRoleUsers) {
        this.indexRoleUsers = indexRoleUsers;
    }
    
    public void loadRoleListEdit()
    {
        fc.print("edit roleuser");
        itemRoleUser    = getRoleUserJpaController().findByUserId(currentusers);
        
        for(RoleUser listRoleuser : itemRoleUser)
        {
            getIndexRoleUsers().add(listRoleuser.getRoleId().getIdRole());
            getItemRoleDetail().add(new roleDetail(listRoleuser.getId(),listRoleuser.getRoleId().getIdRole(), listRoleuser.getRoleId().getRoleName(),listRoleuser.getRoleId().getDesscrition()));
        } 
          
        objRole   = getRoleJpaController().findByUserIdNotIn(getIndexRoleUsers());
        
        getItemrole().add(new SelectItem("00","-----Select roles -----"));
        for(Role listRole:objRole)
        { 
             getItemrole().add(new SelectItem(listRole.getIdRole(),listRole.getRoleName())); 
        }  
    }
    public void loadRoleList()
    {
        
            
            objRole  = getRoleJpaController().findRoleNameAllNotDelete();
            getItemrole().add(new SelectItem("00","---- Select roles -----"));
            for(Role roleList:objRole)
            { 
                getItemrole().add(new SelectItem(roleList.getIdRole(),roleList.getRoleName()));
            }
        
    }
    public void ChangeValueRoleItem(ValueChangeEvent event)
    {
        roleUseIndex  = (String) event.getNewValue();
        fc.print("Item role id :"+roleUseIndex);
    }
    private int num;
    public void addUserRole()
    {
        num = 0;
        if(!roleUseIndex.equals("00")){
            fc.print("Insert data in userRole"+roleUseIndex); 
            //objRoleUse  = getRoleJpaController().findRole(roleUseIndex); 
            objRole         = new ArrayList<Role>();
            itemRoleUser    = new ArrayList<RoleUser>();
            itemRoleUser    = getRoleUserJpaController().findByUserId(currentusers);
            indexRoleUsers  = new ArrayList<String>();
            for(RoleUser listRoleuser : itemRoleUser)
            {
                getIndexRoleUsers().add(listRoleuser.getRoleId().getIdRole());
                fc.print("=== index already : "+listRoleuser);
                
                //getItemRoleDetail().add(new roleDetail(listRoleuser.getId(),0, listRoleuser.getRoleId().getRoleName(),listRoleuser.getRoleId().getDesscrition()));
            }   
            objRole   = getRoleJpaController().findByUserIdNotIn(getIndexRoleUsers());
            if(objRole == null)
            {
                objRole = getRoleJpaController().findRoleNameAllNotDelete();
            }
            
            fc.print("roleUseIndex ;"+roleUseIndex);
            for(Role listRole :objRole){
                num++;
                if(listRole.getIdRole().equals(roleUseIndex)){
                    getItemRoleDetail().add(new roleDetail(roleUseIndex,roleUseIndex, listRole.getRoleName(),listRole.getDesscrition()));
                    fc.print("Add new role");
                    break;
                }   
                fc.print("all list role name :"+listRole.getRoleName()+" id : "+listRole.getIdRole()+" role use index :"+roleUseIndex);
            }
            fc.print("test");
            for(SelectItem listItem:itemrole)
            {
                if(listItem.getValue().equals(roleUseIndex)) 
                {
                    getItemrole().remove(listItem);
                    break;
                }
            }
          
        }
        roleUseIndex  = "00";
    }
    public  void showListRole()
    {
        
    
    }
    public void changtoDel(String index)
    {
        fc.print("index remove id: "+index);
        for(roleDetail listItemRoleDetial:itemRoleDetail)
        {
            if(listItemRoleDetial.getNumber().equals(index))
            {
                
                getItemrole().add(new SelectItem(listItemRoleDetial.getNumber(),listItemRoleDetial.getRoleName()));
                getItemRoleDetail().remove(listItemRoleDetial);
                fc.print(" Remove "); 
                try
                {
                     roleUser  = null;
                     roleUser  = getRoleUserJpaController().findRoleUser(listItemRoleDetial.getIndex());
                     getRoleUser().setDeleted(1);
                     try{
                       
                        getRoleUserJpaController().edit(getRoleUser());
                        indexRoleUsers.remove(listItemRoleDetial.getIndex());
                        fc.print("edit"); 
                     }catch(Exception e)
                     {
                         fc.print("Error :"+e);
                     }
                     fc.print("Del");
                }
                catch(Exception e)
                {
                     fc.print("Error : "+e);
                }
                break;
            }
        } 
    } 
    public List<SelectItem> getItemrole() {
        if(itemrole == null)
        {
            itemrole  = new ArrayList<SelectItem>();
        }
        return itemrole;
    }

    public void setItemrole(List<SelectItem> itemrole) {
        this.itemrole = itemrole;
    }
    
    public List<Role> getObjRole() {
        return objRole;
    }

    public void setObjRole(List<Role> objRole) {
        this.objRole = objRole; 
    }

    public List<RoleUser> getItemRoleUser() {
        return itemRoleUser;
    }

    public void setItemRoleUser(List<RoleUser> itemRoleUser) {
        this.itemRoleUser = itemRoleUser;
    }
     
    public String getRoleUseIndex() {
        return roleUseIndex;
    }

    public void setRoleUseIndex(String roleUseIndex) {
        this.roleUseIndex = roleUseIndex;
    }

    public RoleUser getCurrentRoleUser() {
        if(currentRoleUser == null)
        {
            currentRoleUser  =  new RoleUser();
        }
        return currentRoleUser;
    }

    public void setCurrentRoleUser(RoleUser currentRoleUser) {
        this.currentRoleUser = currentRoleUser;
    }

    public List<roleDetail> getItemRoleDetail() {
        if(itemRoleDetail == null)
        {
            itemRoleDetail  = new ArrayList<roleDetail>();
        }
        return itemRoleDetail;
    }

    public void setItemRoleDetail(List<roleDetail> itemRoleDetail) {
        this.itemRoleDetail = itemRoleDetail;
    }

    public Role getObjRoleUse() {
        return objRoleUse;
    }

    public void setObjRoleUse(Role objRoleUse) {
        this.objRoleUse = objRoleUse;
    }

    public RoleUser getRoleUser() {
        if(roleUser == null)
        {
            roleUser  = new RoleUser();
        }
        return roleUser;
    }

    public void setRoleUser(RoleUser roleUser) {
        this.roleUser = roleUser;
        if(roleUser == null )
        {
            roleUser  = new RoleUser();
        }
    }

    private void searchByUsername() {
       try {
            userlist = null;
            usersdatamodel = null;
            userlist = getUsersJpaController().findByUsernameList(usernameSearch);
            setUsersdatamodel();
        } catch (Exception e) {
            print("= loaduser Error : " + e);
        } 
    }

    private void serchByFirstName() {
        try {
            userlist  = null;
            usersdatamodel = null;
            userlist = getUsersJpaController().findByFisrtNameList(firshnameSearch);
            setUsersdatamodel();
        } catch (Exception e) {
            print("= loaduser Error : " + e);
        }
        
    }

//    private void searchByPosition() {
//        try {
//            userlist  = null;
//            usersdatamodel  = null;
//            userlist = getUsersJpaController().findAll();
//            setUsersdatamodel();
//        } catch (Exception e) {
//            print("= loaduser Error : " + e);
//        }
//        
//    }
    
    
    
}
