/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appCinfigpage.bean;

import com.icesoft.faces.component.ext.RowSelectorEvent;
import com.tct.data.Role;
import com.tct.data.RoleUser;
import com.tct.data.Users;
import com.tct.data.jpa.RoleJpaController;
import com.tct.data.jpa.RoleUserJpaController;
import com.tct.data.jpa.UsersJpaController;
import com.tct.data.jpa.exceptions.NonexistentEntityException;
import com.tct.data.jpa.exceptions.PreexistingEntityException;
import com.tct.data.jsf.util.PaginationHelper;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.faces.event.ActionEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.persistence.Persistence;

/**
 *
 * @author The_Boy_Cs
 */
public class userConfig {

    private List<Users> userlist = null;
    private List<RoleUser> selectlist = null;  // this is roleuser 
    private Role roleData = null;
    private PaginationHelper pagination;
    private DataModel items = null;  // this is role pool.
    private RoleJpaController jpaController = null;
    private String name;  // first_name
    private String username;  // username
    private String position;  // role_name
    private Users currentUserselected;

    public void SearchUser(ActionEvent event) {
        userlist = new ArrayList<Users>();
        if (!name.isEmpty()) {
            System.err.println("Name Search : " + name);
            userlist = getUserJpaController().findByName(name);
            System.err.println("Name count : " + userlist.size());
            //System.err.println("Name : " + userlist.get(0).getFirstName());
        } else if (!username.isEmpty()) {
            System.err.println("Username : " + username);
            userlist.add(getUserJpaController().findByUsername(username));
        } else if (!position.isEmpty()) {
            System.err.println("Position : " + position);
            List<RoleUser> roleuser = getRoleUserJpaController().findByRoleName(position);
            Iterator itr = roleuser.iterator();
            while (itr.hasNext()) {
                userlist.add(((RoleUser) itr.next()).getUsersId());
            }
        }
    }

    public void SelectUsers(RowSelectorEvent event) {
        System.out.println("new User : " + event.getSelectedRows());
        currentUserselected = userlist.get(event.getRow());
        setSelectlist((List<RoleUser>) getRoleUserJpaController().findByUserId(currentUserselected));
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getRoleJpaController().getRoleCount();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getRoleJpaController().findRoleEntities(getPageSize(), getPageFirstItem()));
                }
            };
        }
        return pagination;
    }
    private UsersJpaController jpaUsersController = null;

    private UsersJpaController getUserJpaController() {
        if (jpaUsersController == null) {
            jpaUsersController = new UsersJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return jpaUsersController;
    }

    private RoleJpaController getRoleJpaController() {
        if (jpaController == null) {
            jpaController = new RoleJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return jpaController;
    }
    private RoleUserJpaController jpaRoleUserController = null;

    private RoleUserJpaController getRoleUserJpaController() {
        if (jpaRoleUserController == null) {
            jpaRoleUserController = new RoleUserJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return jpaRoleUserController;
    }
//    private int rpindex = -1;
    private int rindex = -1;

    public void rolePoolSelectionListenert(RowSelectorEvent event) {
        System.out.println("selectRolepool : " + event.getRow());
        if (event.isSelected()) {
//            rpindex = event.getRow();
            System.out.println("selectRolepool : " + event.getRow());
            roleData = (Role) items.getRowData();
            System.out.println("Role Data : " + roleData.getRoleName());
        }
    }

    public void roleSelectionListenert(RowSelectorEvent event) {
        System.out.println("selectRole : " + event.getRow());
        if (event.isSelected()) {
            rindex = event.getRow();
        }
    }

    public void delRoleOutSelectlist(ActionEvent event) {
        if (rindex > -1) {
            System.out.println("SelectRole role id : " + selectlist.get(rindex).getRoleId());
            RoleUser roleUser = selectlist.get(rindex);
            selectlist.remove(roleUser);
            try {
                getRoleUserJpaController().destroyByRoleUser(roleUser);
            } catch (Exception e) {
                System.out.println("Error : " + e);
            }
        }
        rindex = -1;
    }

    public void addRoleToSelectlist(ActionEvent event) {
        Users user = userLogin.getSessionUser();
        RoleUser roleuser = new RoleUser("" + Math.floor(Math.random() * 1000));
        try {
//            roleuser.setId("" + Math.floor(Math.random()*1000));
            roleuser.setCreateDate(new Date());
            roleuser.setDeleted(0);
            roleuser.setUserCreate(user.getId());
            roleuser.setUsersId(currentUserselected);
            roleuser.setRoleId(roleData);
//            roleuser.setRoleName(roleData.getRoleName());
            //roleuser.setRolename(roleData.getRoleName());
            selectlist.add(roleuser);
            System.out.println("AddList Complete.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void SaveRoleUser(ActionEvent event) {
        if (!selectlist.isEmpty()) {
            Iterator itr = selectlist.iterator();
            while (itr.hasNext()) {
                RoleUser roleuser = (RoleUser) itr.next();
                try {
                    getRoleUserJpaController().destroy(roleuser.getId());
                } catch (NonexistentEntityException nonex) {
                    System.err.println("Error : " + nonex);
                }
                try {
                    roleuser.setId("" + Math.random());
                    getRoleUserJpaController().create(roleuser);
                    System.out.println("Insert Complete.");
                } catch (PreexistingEntityException pex) {
                    System.err.println("Error1 : " + pex);
                } catch (Exception e) {
                    System.err.println("Error2 : " + e);
                }
            }
        }
    }

    public List<RoleUser> getSelectlist() {
        if (selectlist == null) {
            selectlist = new ArrayList<RoleUser>();
        }
        return selectlist;
    }

    public void setSelectlist(List<RoleUser> selectlist) {
        this.selectlist = selectlist;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<Users> getUserlist() {
        if (userlist == null) {
            List<Users> list = getUserJpaController().findAll();
            System.out.println("List size : " + list.size());
            if (list.size() > 0) {
                userlist = list;
            } else {
                userlist = new ArrayList<Users>();
            }
        }
        return userlist;
    }

    public void setUserlist(List<Users> userlist) {
        this.userlist = userlist;
    }

    public Users getCurrentUserselected() {
        return currentUserselected;
    }

    public void setCurrentUserselected(Users currentUserselected) {
        this.currentUserselected = currentUserselected;
    }
}
