package com.appCinfigpage.bean;

import com.tct.data.Role;
import com.tct.data.jpa.RoleJpaController;
import com.tct.data.jsf.RoleController;
import com.tct.data.jsf.util.JsfUtil;
import java.util.Arrays;
import java.util.ResourceBundle;
import javax.faces.event.ValueChangeEvent;
import javax.persistence.Persistence;
import com.appCinfigpage.bean.fc;
import com.appConfigpage.Data.roleDetail;
import com.tct.data.jsf.util.PaginationHelper;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import javax.faces.component.UISelectBoolean;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

public class role { 
    private RoleJpaController jpaController = null;
    private PaginationHelper worksheetpagination;
    private String rolenames;
    private String description;
    private DataModel worksheet; 
    private int count;
    private RoleController rolecontroller = null;  // this is Role entity
    private Role current = null;
    private int[] rolesColumn   = new int[80];
    private int selectedItemIndex;
    private List<roleDetail> itemRoles;
    private List<Role> objRoles;
    private boolean[] show_Fl_Checkbox  = new boolean[2];
    private boolean[] value_Fl_CheclBox = new boolean[2];
    public role() {
        getSelected(); 
        searchRoleList();
    }
    
    private void getSelected() {
        if (current == null) {
            current = new Role();
            selectedItemIndex = -1;
            System.out.print("new CurrentRole");
        }
        //return current;
    }

    private RoleJpaController getJpaController() {
        if (jpaController == null) {
            jpaController = new RoleJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return jpaController;
    }
    public String prepareCreate() {
        current = new Role();
        selectedItemIndex = -1;
        return "Create";
    }
    public void create() {  // Save new role to db.
        try {
            int id = (int) Math.floor(Math.random()*1000);
            current.setIdRole(""+id);
            current.setRoleName(rolenames);
            current.setDesscrition(description);
            current.setDeleted(0);
            getJpaController().create(current);
            
            String link = "pages/appConfig/role/listview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
            objRoles   = null;
            itemRoles   = new ArrayList<roleDetail>();
            rolenames     = null;
            description   = null;
            printViewColumn();
            resetRolesColumn();
            resetCheck_fl();
            disble_fl    = true; 
            resetBlAll();
            disble_bl   = true;
            resetPcbAll();
            disble_pcb  = true;
            resetSetting(); 
            disble_setting  = true;
            searchRoleList(); 
           
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            //return null;
        }
    }

    // set role value    --------------------------------------------------- 
    //================================= FL ==================================
    private  boolean  disble_fl  = true;
    public   boolean  check_fl_1  =false; 
    private  boolean  check_fl_2  =false; 
    private  boolean  check_fl_3  =false; 
    private  boolean  check_fl_4  =false; 
    private  boolean  check_fl_5  =false;

    public boolean isDisble_fl() {
        return disble_fl;
    }

    public void setDisble_fl(boolean disble_fl) {
        this.disble_fl = disble_fl;
    }

    

    public boolean isCheck_fl_1() {
        return check_fl_1;
    }

    public void setCheck_fl_1(boolean check_fl_1) {
        this.check_fl_1 = check_fl_1;
    }

    public boolean isCheck_fl_2() {
        return check_fl_2;
    }

    public void setCheck_fl_2(boolean check_fl_2) {
        this.check_fl_2 = check_fl_2;
    }

    public boolean isCheck_fl_3() {
        return check_fl_3;
    }

    public void setCheck_fl_3(boolean check_fl_3) {
        this.check_fl_3 = check_fl_3;
    }

    public boolean isCheck_fl_4() {
        return check_fl_4;
    }

    public void setCheck_fl_4(boolean check_fl_4) {
        this.check_fl_4 = check_fl_4;
    }

    public boolean isCheck_fl_5() {
        return check_fl_5;
    }

    public void setCheck_fl_5(boolean check_fl_5) {
        this.check_fl_5 = check_fl_5;
    }
    
    public void  resetCheck_fl()
    {
         check_fl_1  =false; 
         check_fl_2  =false; 
         check_fl_3  =false; 
         check_fl_4  =false; 
         check_fl_5  =false;
    }
    
    //==========================================================================
    public void ListernerChangValueRole1(ValueChangeEvent event) { 
        System.out.println("new Value 1: " + event.getNewValue());
        if ((Boolean) event.getNewValue()) { 
            rolesColumn[0] = 1;
            resetCheck_fl();
            divsible_fl_One        = "display:none;";
            divsible_fl_Two        = "display:;";
            disble_fl    = false; 
        } else{ 
            resetCheck_fl();
            divsible_fl_One         = "display:;";
            divsible_fl_Two         = "display:none;"; 
            disble_fl    = true; 
            rolesColumn[0] = 0;
            ListernerChangValueRole2(event);
            ListernerChangValueRole3(event);
            ListernerChangValueRole4(event);
            ListernerChangValueRole5(event);
            ListernerChangValueRole6(event); 
        } 
        current.setSeeFl(rolesColumn[0]);
        System.out.println("Role1 : "+current.getSeeFl());
    } 
    public void ListernerChangValueRole2(ValueChangeEvent event) { 
        System.out.println("new Value 2 : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) { 
            rolesColumn[1] = 1; 
        } else { 
            rolesColumn[1] = 0;   
        } 
        current.setCreateFl(rolesColumn[1]);
    }

    public void ListernerChangValueRole3(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) { 
            rolesColumn[2] = 1; 
        } else { 
            rolesColumn[2] = 0;
        } 
        current.setListFl(rolesColumn[2]);
    }

    public void ListernerChangValueRole4(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) { 
             rolesColumn[3] = 1;
        } else { 
            rolesColumn[3] = 0;
        } 
        current.setDeleteFl(rolesColumn[3]);
    }

    public void ListernerChangValueRole5(ValueChangeEvent event) {
        System.out.println("new Value 5 :  " + event.getNewValue());
        if ((Boolean) event.getNewValue()) { 
            rolesColumn[4]  = 1;
        } else { 
             rolesColumn[4] = 0;  
        } 
        current.setEditFl( rolesColumn[4]);
    } 
    public void ListernerChangValueRole6(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) { 
              rolesColumn[5] = 1; 
        } else {
              rolesColumn[5] = 0;  
        }  
        current.setDetailFrontLine(rolesColumn[5]);
    }
    
    //============================== BL =======================================
    private boolean disble_bl   = true;
    private String divsible_bl_One         = "display:;";
    private String divsible_bl_Two         = "display:none;"; 
    private boolean  check_bl_1            = false;    
    private boolean  check_bl_2            = false;  
    private boolean  check_bl_3            = false;  
    private boolean  check_bl_4            = false;  
    private boolean  check_bl_5            = false;  
    private boolean  check_bl_6            = false;  
    private boolean  check_bl_7            = false;  
    private boolean  check_bl_8            = false;  
    private boolean  check_bl_9            = false;  
    private boolean  check_bl_10            = false;
    
    private boolean  check_bl_11            = false;  
    private boolean  check_bl_12            = false;  
    private boolean  check_bl_13            = false;  
    private boolean  check_bl_14            = false;  
    private boolean  check_bl_15            = false;  
    private boolean  check_bl_16            = false;  
    private boolean  check_bl_17            = false;  
    private boolean  check_bl_18            = false;  
    private boolean  check_bl_19            = false;  
    private boolean  check_bl_20            = false; 
    
    private boolean  check_bl_21            = false;  
    private boolean  check_bl_22            = false;  
    private boolean  check_bl_23            = false;  
    private boolean  check_bl_24            = false;  
    
    public void callAllFuction(ValueChangeEvent event)
    {
        ListernerChangValueRole8(event);
        ListernerChangValueRole9(event);
        ListernerChangValueRole10(event);

        ListernerChangValueRole11(event);
        ListernerChangValueRole12(event);
        ListernerChangValueRole13(event);
        ListernerChangValueRole14(event);
        ListernerChangValueRole15(event);
        ListernerChangValueRole16(event);
        ListernerChangValueRole17(event);
        ListernerChangValueRole18(event);
        ListernerChangValueRole19(event);
        ListernerChangValueRole20(event);

        ListernerChangValueRole21(event);
        ListernerChangValueRole22(event);
        ListernerChangValueRole23(event);
        ListernerChangValueRole24(event);
        ListernerChangValueRole25(event);  
    }
    public void resetBlAll()
    {
        check_bl_1            = false;    
        check_bl_2            = false;  
        check_bl_3            = false;  
        check_bl_4            = false;  
        check_bl_5            = false;  
        check_bl_6            = false;  
        check_bl_7            = false;  
        check_bl_8            = false;  
        check_bl_9            = false;  
        check_bl_10            = false;

        check_bl_11            = false;
        check_bl_12            = false;  
        check_bl_13            = false;  
        check_bl_14            = false;  
        check_bl_15            = false;  
        check_bl_16            = false;  
        check_bl_17            = false;  
        check_bl_18            = false;  
        check_bl_19            = false;  
        check_bl_20            = false; 

        check_bl_21            = false;  
        check_bl_22            = false;  
        check_bl_23            = false;  
        check_bl_24            = false;  
    }
    public boolean isDisble_bl() {
        return disble_bl;
    }

    public void setDisble_bl(boolean disble_bl) {
        this.disble_bl = disble_bl;
    }

    public String getDivsible_bl_One() {
        return divsible_bl_One;
    }

    public void setDivsible_bl_One(String divsible_bl_One) {
        this.divsible_bl_One = divsible_bl_One;
    }

    public String getDivsible_bl_Two() {
        return divsible_bl_Two;
    }

    public void setDivsible_bl_Two(String divsible_bl_Two) {
        this.divsible_bl_Two = divsible_bl_Two;
    }

    public boolean isCheck_bl_1() {
        return check_bl_1;
    }

    public void setCheck_bl_1(boolean check_bl_1) {
        this.check_bl_1 = check_bl_1;
    }

    public boolean isCheck_bl_10() {
        return check_bl_10;
    }

    public void setCheck_bl_10(boolean check_bl_10) {
        this.check_bl_10 = check_bl_10;
    }

    public boolean isCheck_bl_11() {
        return check_bl_11;
    }

    public void setCheck_bl_11(boolean check_bl_11) {
        this.check_bl_11 = check_bl_11;
    }

    public boolean isCheck_bl_12() {
        return check_bl_12;
    }

    public void setCheck_bl_12(boolean check_bl_12) {
        this.check_bl_12 = check_bl_12;
    }

    public boolean isCheck_bl_13() {
        return check_bl_13;
    }

    public void setCheck_bl_13(boolean check_bl_13) {
        this.check_bl_13 = check_bl_13;
    }

    public boolean isCheck_bl_14() {
        return check_bl_14;
    }

    public void setCheck_bl_14(boolean check_bl_14) {
        this.check_bl_14 = check_bl_14;
    }

    public boolean isCheck_bl_15() {
        return check_bl_15;
    }

    public void setCheck_bl_15(boolean check_bl_15) {
        this.check_bl_15 = check_bl_15;
    }

    public boolean isCheck_bl_16() {
        return check_bl_16;
    }

    public void setCheck_bl_16(boolean check_bl_16) {
        this.check_bl_16 = check_bl_16;
    }

    public boolean isCheck_bl_17() {
        return check_bl_17;
    }

    public void setCheck_bl_17(boolean check_bl_17) {
        this.check_bl_17 = check_bl_17;
    }

    public boolean isCheck_bl_18() {
        return check_bl_18;
    }

    public void setCheck_bl_18(boolean check_bl_18) {
        this.check_bl_18 = check_bl_18;
    }

    public boolean isCheck_bl_19() {
        return check_bl_19;
    }

    public void setCheck_bl_19(boolean check_bl_19) {
        this.check_bl_19 = check_bl_19;
    }

    public boolean isCheck_bl_2() {
        return check_bl_2;
    }

    public void setCheck_bl_2(boolean check_bl_2) {
        this.check_bl_2 = check_bl_2;
    }

    public boolean isCheck_bl_20() {
        return check_bl_20;
    }

    public void setCheck_bl_20(boolean check_bl_20) {
        this.check_bl_20 = check_bl_20;
    }

    public boolean isCheck_bl_21() {
        return check_bl_21;
    }

    public void setCheck_bl_21(boolean check_bl_21) {
        this.check_bl_21 = check_bl_21;
    }

    public boolean isCheck_bl_22() {
        return check_bl_22;
    }

    public void setCheck_bl_22(boolean check_bl_22) {
        this.check_bl_22 = check_bl_22;
    }

    public boolean isCheck_bl_23() {
        return check_bl_23;
    }

    public void setCheck_bl_23(boolean check_bl_23) {
        this.check_bl_23 = check_bl_23;
    }

    public boolean isCheck_bl_24() {
        return check_bl_24;
    }

    public void setCheck_bl_24(boolean check_bl_24) {
        this.check_bl_24 = check_bl_24;
    }

    public boolean isCheck_bl_3() {
        return check_bl_3;
    }

    public void setCheck_bl_3(boolean check_bl_3) {
        this.check_bl_3 = check_bl_3;
    }

    public boolean isCheck_bl_4() {
        return check_bl_4;
    }

    public void setCheck_bl_4(boolean check_bl_4) {
        this.check_bl_4 = check_bl_4;
    }

    public boolean isCheck_bl_5() {
        return check_bl_5;
    }

    public void setCheck_bl_5(boolean check_bl_5) {
        this.check_bl_5 = check_bl_5;
    }

    public boolean isCheck_bl_6() {
        return check_bl_6;
    }

    public void setCheck_bl_6(boolean check_bl_6) {
        this.check_bl_6 = check_bl_6;
    }

    public boolean isCheck_bl_7() {
        return check_bl_7;
    }

    public void setCheck_bl_7(boolean check_bl_7) {
        this.check_bl_7 = check_bl_7;
    }

    public boolean isCheck_bl_8() {
        return check_bl_8;
    }

    public void setCheck_bl_8(boolean check_bl_8) {
        this.check_bl_8 = check_bl_8;
    }

    public boolean isCheck_bl_9() {
        return check_bl_9;
    }

    public void setCheck_bl_9(boolean check_bl_9) {
        this.check_bl_9 = check_bl_9;
    }
    
    //========================================================================
    public void ListernerChangValueRole7(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[6]= 1; 
            resetBlAll();
            disble_bl  = false;
            divsible_bl_One         = "display:none;";
            divsible_bl_Two         = "display:;"; 
        } else {
            rolesColumn[6] = 0; 
            resetBlAll();
            disble_bl   = true;
            divsible_bl_One         = "display:;";
            divsible_bl_Two         = "display:none;";
            callAllFuction(event);
        }
        current.setSeeBl(rolesColumn[6]);
    }

    public void ListernerChangValueRole8(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[7] = 1; 
        } else {
            rolesColumn[7] = 0; 
        }
        current.setCreateAssy(rolesColumn[7]);
    }

    public void ListernerChangValueRole9(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[8]= 1; 
        } else {
            rolesColumn[8]= 0; 
        }
        current.setListAssy(rolesColumn[8]);
    }

    public void ListernerChangValueRole10(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[9] = 1; 
        } else {
            rolesColumn[9] = 0; 
        }
        current.setDeleteAssy(rolesColumn[9]);
    } 
    public void ListernerChangValueRole11(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[10] = 1; 
        } else {
            rolesColumn[10] = 0; 
        }
        current.setEditAssy(rolesColumn[10]);
    }

    public void ListernerChangValueRole12(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[11] = 1; 
        } else {
            rolesColumn[11] = 0; 
        }
        current.setApproveAssy(rolesColumn[11]);
    }

    public void ListernerChangValueRole13(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[12] = 1; 
        } else {
            rolesColumn[12]  = 0; 
        }
        current.setDetailAssy(rolesColumn[12]);
    }

    public void ListernerChangValueRole14(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[13]  = 1; 
        } else {
            rolesColumn[13]  = 0; 
        }
        current.setCreateAuto(rolesColumn[13]);
    }

    public void ListernerChangValueRole15(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[14] = 1; 
        } else {
            rolesColumn[14] = 0; 
        }
        current.setListAuto(rolesColumn[14]);
    }

    public void ListernerChangValueRole16(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
           rolesColumn[15] = 1; 
        } else {
           rolesColumn[15] = 0; 
        }
        current.setDeleteAuto(rolesColumn[15]);
    }

    public void ListernerChangValueRole17(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[16] = 1; 
        } else {
            rolesColumn[16] = 0; 
        }
        current.setEditAuto(rolesColumn[16]);
    }

    public void ListernerChangValueRole18(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[17] = 1; 
        } else {
            rolesColumn[17] = 0;  
        }
        current.setApproveAuto(rolesColumn[17]);
    }

    public void ListernerChangValueRole19(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[18] = 1; 
        } else {
            rolesColumn[18] = 0; 
        }
        current.setDetailAuto(rolesColumn[18]);
    }

    public void ListernerChangValueRole20(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[19]= 1; 
        } else {
            rolesColumn[19] = 0; 
        }
        current.setCreateAssembly(rolesColumn[19]);
    }

    public void ListernerChangValueRole21(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
             rolesColumn[20] = 1; 
        } else {
            rolesColumn[20] = 0; 
        }
        current.setListAssembly(rolesColumn[20]);
    }

    public void ListernerChangValueRole22(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[21] = 1; 
        } else {
            rolesColumn[21] = 0; 
        }
        current.setDeleteAssembly(rolesColumn[21]);
    }

    public void ListernerChangValueRole23(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[22] = 1; 
        } else {
            rolesColumn[22] = 0; 
        }
        current.setEditAssembly(rolesColumn[22]);
    }

    public void ListernerChangValueRole24(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[23] = 1; 
        } else {
            rolesColumn[23] = 0; 
        }
        current.setApproveAssembly(rolesColumn[23]);
    }

    public void ListernerChangValueRole25(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[24] = 1; 
        } else {
            rolesColumn[24] = 0; 
        }
        current.setDetailAssembly(rolesColumn[24]);
    }
 //=============================  Pcb   ========================================
    private boolean  disble_pcb  = true;
    private String   divible_pcb_one  =  "display:;";
    private String   divible_pcb_two  =  "display:none;"; 
    private boolean  pcb01_1          =   false;
    private boolean  pcb01_2          =   false;
    private boolean  pcb01_3          =   false;
    private boolean  pcb01_4          =   false;
    private boolean  pcb01_5          =   false;
    private boolean  pcb01_6          =   false;
    
    private boolean  pcb02_1          =   false;
    private boolean  pcb02_2          =   false;
    private boolean  pcb02_3          =   false;
    private boolean  pcb02_4          =   false;
    private boolean  pcb02_5          =   false;
    private boolean  pcb02_6          =   false;
    
    private boolean  pcb03_1          =   false;
    private boolean  pcb03_2          =   false;
    private boolean  pcb03_3          =   false;
    private boolean  pcb03_4          =   false;
    private boolean  pcb03_5          =   false;
    private boolean  pcb03_6          =   false;
    
    
    private void resetPcbAll()
    {
        pcb01_1          =   false;
        pcb01_2          =   false;
        pcb01_3          =   false;
        pcb01_4          =   false;
        pcb01_5          =   false;
        pcb01_6          =   false;

        pcb02_1          =   false;
        pcb02_2          =   false;
        pcb02_3          =   false;
        pcb02_4          =   false;
        pcb02_5          =   false;
        pcb02_6          =   false;

        pcb03_1          =   false;
        pcb03_2          =   false;
        pcb03_3          =   false;
        pcb03_4          =   false;
        pcb03_5          =   false;
        pcb03_6          =   false;
    }
    private void callPcbAllFuction(ValueChangeEvent event)
    {
        ListernerChangValueRole27(event);
        ListernerChangValueRole28(event);
        ListernerChangValueRole29(event);
        ListernerChangValueRole30(event); 
        
        ListernerChangValueRole31(event);
        ListernerChangValueRole32(event);
        ListernerChangValueRole33(event);
        ListernerChangValueRole34(event);
        ListernerChangValueRole35(event);
        ListernerChangValueRole36(event);
        ListernerChangValueRole37(event);
        ListernerChangValueRole38(event);
        ListernerChangValueRole39(event);
        ListernerChangValueRole40(event);
        
        ListernerChangValueRole41(event);
        ListernerChangValueRole42(event);
        ListernerChangValueRole43(event);
        ListernerChangValueRole44(event); 
    }        
    
    public boolean isDisble_pcb() {
        return disble_pcb;
    }

    public void setDisble_pcb(boolean disble_pcb) {
        this.disble_pcb = disble_pcb;
    }

    public String getDivible_pcb_one() {
        return divible_pcb_one;
    }

    public void setDivible_pcb_one(String divible_pcb_one) {
        this.divible_pcb_one = divible_pcb_one;
    }

    public String getDivible_pcb_two() {
        return divible_pcb_two;
    }

    public void setDivible_pcb_two(String divible_pcb_two) {
        this.divible_pcb_two = divible_pcb_two;
    }

    public boolean isPcb01_1() {
        return pcb01_1;
    }

    public void setPcb01_1(boolean pcb01_1) {
        this.pcb01_1 = pcb01_1;
    }

    public boolean isPcb01_2() {
        return pcb01_2;
    }

    public void setPcb01_2(boolean pcb01_2) {
        this.pcb01_2 = pcb01_2;
    }

    public boolean isPcb01_3() {
        return pcb01_3;
    }

    public void setPcb01_3(boolean pcb01_3) {
        this.pcb01_3 = pcb01_3;
    }

    public boolean isPcb01_4() {
        return pcb01_4;
    }

    public void setPcb01_4(boolean pcb01_4) {
        this.pcb01_4 = pcb01_4;
    }

    public boolean isPcb01_5() {
        return pcb01_5;
    }

    public void setPcb01_5(boolean pcb01_5) {
        this.pcb01_5 = pcb01_5;
    }

    public boolean isPcb01_6() {
        return pcb01_6;
    }

    public void setPcb01_6(boolean pcb01_6) {
        this.pcb01_6 = pcb01_6;
    }

    public boolean isPcb02_1() {
        return pcb02_1;
    }

    public void setPcb02_1(boolean pcb02_1) {
        this.pcb02_1 = pcb02_1;
    }

    public boolean isPcb02_2() {
        return pcb02_2;
    }

    public void setPcb02_2(boolean pcb02_2) {
        this.pcb02_2 = pcb02_2;
    }

    public boolean isPcb02_3() {
        return pcb02_3;
    }

    public void setPcb02_3(boolean pcb02_3) {
        this.pcb02_3 = pcb02_3;
    }

    public boolean isPcb02_4() {
        return pcb02_4;
    }

    public void setPcb02_4(boolean pcb02_4) {
        this.pcb02_4 = pcb02_4;
    }

    public boolean isPcb02_5() {
        return pcb02_5;
    }

    public void setPcb02_5(boolean pcb02_5) {
        this.pcb02_5 = pcb02_5;
    }

    public boolean isPcb02_6() {
        return pcb02_6;
    }

    public void setPcb02_6(boolean pcb02_6) {
        this.pcb02_6 = pcb02_6;
    }

    public boolean isPcb03_1() {
        return pcb03_1;
    }

    public void setPcb03_1(boolean pcb03_1) {
        this.pcb03_1 = pcb03_1;
    }

    public boolean isPcb03_2() {
        return pcb03_2;
    }

    public void setPcb03_2(boolean pcb03_2) {
        this.pcb03_2 = pcb03_2;
    }

    public boolean isPcb03_3() {
        return pcb03_3;
    }

    public void setPcb03_3(boolean pcb03_3) {
        this.pcb03_3 = pcb03_3;
    }

    public boolean isPcb03_4() {
        return pcb03_4;
    }

    public void setPcb03_4(boolean pcb03_4) {
        this.pcb03_4 = pcb03_4;
    }

    public boolean isPcb03_5() {
        return pcb03_5;
    }

    public void setPcb03_5(boolean pcb03_5) {
        this.pcb03_5 = pcb03_5;
    }

    public boolean isPcb03_6() {
        return pcb03_6;
    }

    public void setPcb03_6(boolean pcb03_6) {
        this.pcb03_6 = pcb03_6;
    }
    
 //=============================================================================   
    public void ListernerChangValueRole26(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[25] = 1;
            resetPcbAll();
            divible_pcb_one  =  "display:none;";
            divible_pcb_two  =  "display:;"; 
            disble_pcb       = false;
            
        } else {
            rolesColumn[25] = 0; 
            resetPcbAll(); 
            callPcbAllFuction(event);
            divible_pcb_one  =  "display:;";
            divible_pcb_two  =  "display:none;"; 
            disble_pcb       = true; 
        }
        current.setSeePcb(rolesColumn[25]);
    }

    public void ListernerChangValueRole27(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[26] = 1; 
        } else {
            rolesColumn[26] = 0; 
        }
        current.setCreatePcb01(rolesColumn[26]);
    }

    public void ListernerChangValueRole28(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[27] = 1; 
        } else {
            rolesColumn[27] = 0; 
        }
        current.setListPcb01(rolesColumn[27]);
    }

    public void ListernerChangValueRole29(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[28] = 1; 
        } else {
            rolesColumn[28] = 0; 
        }
        current.setDeletePcb01(rolesColumn[28]);
    }

    public void ListernerChangValueRole30(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[29] = 1; 
        } else {
            rolesColumn[29] = 0; 
        }
        current.setEditPcb01( rolesColumn[29]);
    }

    public void ListernerChangValueRole31(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[30] = 1; 
        } else {
            rolesColumn[30] = 0; 
        }
        current.setApprovePcb01(rolesColumn[30]);
    }

    public void ListernerChangValueRole32(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[31] = 1; 
        } else {
            rolesColumn[31] = 0; 
        }
        current.setDetailPcb01(rolesColumn[31]);
    }

    public void ListernerChangValueRole33(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[32] = 1; 
        } else {
            rolesColumn[32] = 0; 
        }
        current.setCreatePcb02(rolesColumn[32]);
    }

    public void ListernerChangValueRole34(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[33] = 1; 
        } else {
            rolesColumn[33] = 0; 
        }
        current.setListPcb02(rolesColumn[33]);
    }

    public void ListernerChangValueRole35(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[34] = 1; 
        } else {
            rolesColumn[34] = 0; 
        }
        current.setDeletePcb02(rolesColumn[34]);
    }

    public void ListernerChangValueRole36(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[35] = 1; 
        } else {
            rolesColumn[35] = 0; 
        }
        current.setEditPcb02(rolesColumn[35]);
    }

    public void ListernerChangValueRole37(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[36] = 1; 
        } else {
            rolesColumn[36] = 0; 
        }
        current.setApprovePcb02(rolesColumn[36]);
    }

    public void ListernerChangValueRole38(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[37] = 1; 
        } else {
            rolesColumn[37] = 0; 
        }
        current.setDetailPcb02(rolesColumn[37]);
    }

    public void ListernerChangValueRole39(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[38] = 1; 
        } else {
            rolesColumn[38] = 0; 
        }
        current.setCreatePcb03(rolesColumn[38]);
    }

    public void ListernerChangValueRole40(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[39] = 1; 
        } else {
            rolesColumn[39] = 0; 
        }
        current.setListPcb03(rolesColumn[39]);
    }

    public void ListernerChangValueRole41(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[40] = 1; 
        } else {
            rolesColumn[40] = 0; 
        }
        current.setDeletePcb03(rolesColumn[40]);
    }

    public void ListernerChangValueRole42(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[41] = 1; 
        } else {
            rolesColumn[41] = 0; 
        }
        current.setEditPcb03(rolesColumn[41]);
    }

    public void ListernerChangValueRole43(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[42] = 1; 
        } else {
            rolesColumn[42] = 0; 
        }
        current.setApprovePcb03(rolesColumn[42]);
    }

    public void ListernerChangValueRole44(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[43] = 1; 
        } else {
            rolesColumn[43] = 0; 
        }
        current.setDetailPcb03(rolesColumn[43]);
    }
//====================================== Refu ================================
    private boolean  disble_refu    =   true;
    private String   divible_refu_one  = "display:;";
    private String   divible_refu_two  = "display:none;";
    private boolean  refu01_1          = false;
    private boolean  refu01_2          = false;
    private boolean  refu01_3          = false;
    private boolean  refu01_4          = false;
    private boolean  refu01_5          = false;
    private boolean  refu01_6          = false;
        
    private boolean  refu02_1          = false;
    private boolean  refu02_2          = false;
    private boolean  refu02_3          = false;
    private boolean  refu02_4          = false;
    private boolean  refu02_5          = false;
    private boolean  refu02_6          = false;
    
    private boolean  refu03_1          = false;
    private boolean  refu03_2          = false;
    private boolean  refu03_3          = false;
    private boolean  refu03_4          = false;
    private boolean  refu03_5          = false;
    private boolean  refu03_6          = false;
    
    public void resetRefu()
    {
        refu01_1          = false;
        refu01_2          = false;
        refu01_3          = false;
        refu01_4          = false;
        refu01_5          = false;
        refu01_6          = false;

        refu02_1          = false;
        refu02_2          = false;
        refu02_3          = false;
        refu02_4          = false;
        refu02_5          = false;
        refu02_6          = false;

        refu03_1          = false;
        refu03_2          = false;
        refu03_3          = false;
        refu03_4          = false;
        refu03_5          = false;
        refu03_6          = false;
    }
    private void callFunctionRefuAll(ValueChangeEvent event)
    {
        ListernerChangValueRole46(event);
        ListernerChangValueRole47(event);
        ListernerChangValueRole48(event);
        ListernerChangValueRole49(event);
        ListernerChangValueRole50(event);
        
        ListernerChangValueRole51(event);
        ListernerChangValueRole52(event);
        ListernerChangValueRole53(event);
        ListernerChangValueRole54(event);
        ListernerChangValueRole55(event);
        ListernerChangValueRole56(event);
        ListernerChangValueRole57(event);
        ListernerChangValueRole58(event);
        ListernerChangValueRole59(event);
        ListernerChangValueRole60(event);
        
        ListernerChangValueRole61(event);
        ListernerChangValueRole62(event);
        ListernerChangValueRole63(event); 
    }        
    public boolean isDisble_refu() {
        return disble_refu;
    }

    public String getDivible_refu_one() {
        return divible_refu_one;
    }

    public void setDivible_refu_one(String divible_refu_one) {
        this.divible_refu_one = divible_refu_one;
    }

    public String getDivible_refu_two() {
        return divible_refu_two;
    }

    public void setDivible_refu_two(String divible_refu_two) {
        this.divible_refu_two = divible_refu_two;
    }
    
    public void setDisble_refu(boolean disble_refu) {
        this.disble_refu = disble_refu;
    }

    public boolean isRefu01_1() {
        return refu01_1;
    }

    public void setRefu01_1(boolean refu01_1) {
        this.refu01_1 = refu01_1;
    }

    public boolean isRefu01_2() {
        return refu01_2;
    }

    public void setRefu01_2(boolean refu01_2) {
        this.refu01_2 = refu01_2;
    }

    public boolean isRefu01_3() {
        return refu01_3;
    }

    public void setRefu01_3(boolean refu01_3) {
        this.refu01_3 = refu01_3;
    }

    public boolean isRefu01_4() {
        return refu01_4;
    }

    public void setRefu01_4(boolean refu01_4) {
        this.refu01_4 = refu01_4;
    }

    public boolean isRefu01_5() {
        return refu01_5;
    }

    public void setRefu01_5(boolean refu01_5) {
        this.refu01_5 = refu01_5;
    }

    public boolean isRefu01_6() {
        return refu01_6;
    }

    public void setRefu01_6(boolean refu01_6) {
        this.refu01_6 = refu01_6;
    }

    public boolean isRefu02_1() {
        return refu02_1;
    }

    public void setRefu02_1(boolean refu02_1) {
        this.refu02_1 = refu02_1;
    }

    public boolean isRefu02_2() {
        return refu02_2;
    }

    public void setRefu02_2(boolean refu02_2) {
        this.refu02_2 = refu02_2;
    }

    public boolean isRefu02_3() {
        return refu02_3;
    }

    public void setRefu02_3(boolean refu02_3) {
        this.refu02_3 = refu02_3;
    }

    public boolean isRefu02_4() {
        return refu02_4;
    }

    public void setRefu02_4(boolean refu02_4) {
        this.refu02_4 = refu02_4;
    }

    public boolean isRefu02_5() {
        return refu02_5;
    }

    public void setRefu02_5(boolean refu02_5) {
        this.refu02_5 = refu02_5;
    }

    public boolean isRefu02_6() {
        return refu02_6;
    }

    public void setRefu02_6(boolean refu02_6) {
        this.refu02_6 = refu02_6;
    }

    public boolean isRefu03_1() {
        return refu03_1;
    }

    public void setRefu03_1(boolean refu03_1) {
        this.refu03_1 = refu03_1;
    }

    public boolean isRefu03_2() {
        return refu03_2;
    }

    public void setRefu03_2(boolean refu03_2) {
        this.refu03_2 = refu03_2;
    }

    public boolean isRefu03_3() {
        return refu03_3;
    }

    public void setRefu03_3(boolean refu03_3) {
        this.refu03_3 = refu03_3;
    }

    public boolean isRefu03_4() {
        return refu03_4;
    }

    public void setRefu03_4(boolean refu03_4) {
        this.refu03_4 = refu03_4;
    }

    public boolean isRefu03_5() {
        return refu03_5;
    }

    public void setRefu03_5(boolean refu03_5) {
        this.refu03_5 = refu03_5;
    }

    public boolean isRefu03_6() {
        return refu03_6;
    }

    public void setRefu03_6(boolean refu03_6) {
        this.refu03_6 = refu03_6;
    }
    
    
    
//===========================================================================    
    public void ListernerChangValueRole45(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[44] = 1; 
            resetRefu();
            disble_refu  = false; 
            divible_refu_one  = "display:none;";
            divible_refu_two  = "display:;";
        } else {
            rolesColumn[44] = 0; 
            resetRefu();
            disble_refu  = true;
            divible_refu_one  = "display:;";
            divible_refu_two  = "display:none;";
            callFunctionRefuAll(event);
        }
        current.setSeeRefu(rolesColumn[44]);
    }

    public void ListernerChangValueRole46(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[45] = 1; 
        } else {
            rolesColumn[45] = 0; 
        }
        current.setCreateRefu01(rolesColumn[45]);
    }

    public void ListernerChangValueRole47(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[46] = 1; 
        } else {
            rolesColumn[46] = 0; 
        }
        current.setListRefu01(rolesColumn[46]);
    }

    public void ListernerChangValueRole48(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[47] = 1; 
        } else {
            rolesColumn[47] = 0; 
        }
        current.setDeleteRefu01(rolesColumn[47]);
    }

    public void ListernerChangValueRole49(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[48] = 1; 
        } else {
            rolesColumn[48] = 0; 
        }
        current.setEditRefu01(rolesColumn[48]);
    }

    public void ListernerChangValueRole50(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[49] = 1; 
        } else {
            rolesColumn[49] = 0; 
        }
        current.setApproveRefu01(rolesColumn[49]);
    }

    public void ListernerChangValueRole51(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[50] = 1; 
        } else {
            rolesColumn[50] = 0; 
        }
        current.setDetailRefu01(rolesColumn[50]);
    } 
    public void ListernerChangValueRole52(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[51] = 1; 
        } else {
            rolesColumn[51] = 0; 
        }
        current.setCreateRefu02(rolesColumn[51]);
    }

    public void ListernerChangValueRole53(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[52] = 1; 
        } else {
            rolesColumn[52] = 0; 
        }
        current.setListRefu02(rolesColumn[52]);
    }

    public void ListernerChangValueRole54(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[53] = 1; ;
        } else {
            rolesColumn[53] = 0; 
        }
        current.setDeleteRefu02(rolesColumn[53]);
    }

    public void ListernerChangValueRole55(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[54] = 1; 
        } else {
            rolesColumn[54] = 0; 
        }
        current.setEditRefu02(rolesColumn[54]);
    }

    public void ListernerChangValueRole56(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[55] = 1; 
        } else {
            rolesColumn[55] = 0; 
        }
        current.setApproveRefu02(rolesColumn[55]);
    }

    public void ListernerChangValueRole57(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[56] = 1; 
        } else {
            rolesColumn[56] = 0; 
        }
        current.setDetailRefu02(rolesColumn[56]);
    }

    public void ListernerChangValueRole58(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[57] = 1; 
        } else {
            rolesColumn[57] = 0; 
        }
        current.setCreateRefu03(rolesColumn[57]);
    }

    public void ListernerChangValueRole59(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[58] = 1;
            System.out.println("Role_59 : " + rolesColumn[58]);
        } else {
            rolesColumn[58] = 0;
            System.out.println("Role_59 : " + rolesColumn[58]);
        }
        current.setListRefu03(rolesColumn[58]);
    }

    public void ListernerChangValueRole60(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[59] = 1; 
        } else {
            rolesColumn[59] = 0; 
        }
        current.setDeleteRefu03(rolesColumn[59]);
    }

    public void ListernerChangValueRole61(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[60] = 1; 
        } else {
            rolesColumn[60] = 0; 
        }
        current.setEditRefu03(rolesColumn[60]);
    }

    public void ListernerChangValueRole62(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[61] = 1; 
        } else {
            rolesColumn[61] = 0; 
        }
        current.setApproveRefu03(rolesColumn[61]);
    }

    public void ListernerChangValueRole63(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[62] = 1; 
        } else {
            rolesColumn[62]  = 0; 
        }
        current.setDetailRefu03(rolesColumn[62] );
    }

    public void ListernerChangValueRole64(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[63]  = 1; 
        } else {
            rolesColumn[63] = 0; 
        }
        current.setSeeReport(rolesColumn[63]);
    }
//================================== Setting ==================================
    private boolean disble_setting   = true;
    private String divble_setting_one = "display:;";
    private String divble_setting_two = "display:none;";
    private boolean acc_1   = false;
    private boolean acc_2   = false;
    private boolean acc_3   = false;
    
    private boolean mc_1    = false;
    private boolean mc_2    = false;
    
    public void resetSetting()
    {
        acc_1   = false;
        acc_2   = false;
        acc_3   = false;

        mc_1    = false;
        mc_2    = false;
    }     
    public void callSettingFunction(ValueChangeEvent event)
    {
        ListernerChangValueRole75(event);
        ListernerChangValueRole76(event);
        ListernerChangValueRole77(event);
        
        
        ListernerChangValueRole72(event);
        ListernerChangValueRole73(event);
    }
    public boolean isDisble_setting() {
        return disble_setting;
    }

    public void setDisble_setting(boolean disble_setting) {
        this.disble_setting = disble_setting;
    }

    public String getDivble_setting_one() {
        return divble_setting_one;
    }

    public void setDivble_setting_one(String divble_setting_one) {
        this.divble_setting_one = divble_setting_one;
    }

    public String getDivble_setting_two() {
        return divble_setting_two;
    }

    public void setDivble_setting_two(String divble_setting_two) {
        this.divble_setting_two = divble_setting_two;
    }

    public boolean isAcc_1() {
        return acc_1;
    }

    public void setAcc_1(boolean acc_1) {
        this.acc_1 = acc_1;
    }

    public boolean isAcc_2() {
        return acc_2;
    }

    public void setAcc_2(boolean acc_2) {
        this.acc_2 = acc_2;
    }

    public boolean isAcc_3() {
        return acc_3;
    }

    public void setAcc_3(boolean acc_3) {
        this.acc_3 = acc_3;
    }

    public boolean isMc_1() {
        return mc_1;
    }

    public void setMc_1(boolean mc_1) {
        this.mc_1 = mc_1;
    }

    public boolean isMc_2() {
        return mc_2;
    }

    public void setMc_2(boolean mc_2) {
        this.mc_2 = mc_2;
    }
     
//============================================================================    
    public void ListernerChangValueRole65(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[64] = 1; 
            disble_setting  = false;
            resetSetting();
            divble_setting_one = "display:none;";
            divble_setting_two = "display:;";
        } else {
            rolesColumn[64] = 0; 
            disble_setting  = true;
            resetSetting();
            callSettingFunction(event);
            divble_setting_one = "display:;";
            divble_setting_two = "display:none;";
        }
        current.setSeeSetting(rolesColumn[64]);
    }

    public void ListernerChangValueRole66(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[65] = 1; 
        } else {
            rolesColumn[65] = 0; 
        }
        current.setEditModel(rolesColumn[65]);
    }

    public void ListernerChangValueRole67(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[66] = 1;
            System.out.println("Role_67 : " + rolesColumn[66]);
        } else {
            rolesColumn[66] = 0; 
        }
        current.setNewProcess(rolesColumn[66]);
    }

    public void ListernerChangValueRole68(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[67] = 1; 
        } else {
            rolesColumn[67] = 0; 
        }
        current.setDataProcess(rolesColumn[67]);
    }

    public void ListernerChangValueRole69(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[68] = 1; 
        } else {
            rolesColumn[68] = 0; 
        }
        current.setEditNg(rolesColumn[68]);
    }

    public void ListernerChangValueRole70(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[69] = 1; 
        } else {
            rolesColumn[69] = 0; 
        }
        current.setSelectBar(rolesColumn[69]);
    }

    public void ListernerChangValueRole71(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[70] = 1; 
        } else {
            rolesColumn[70] = 0; 
        }
        current.setSeeMc(rolesColumn[70]);
    }

    public void ListernerChangValueRole72(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[71] = 1; 
        } else {
            rolesColumn[71] = 0; 
        }
        current.setCreateMc(rolesColumn[71]);
    }

    public void ListernerChangValueRole73(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[72] = 1; 
        } else {
            rolesColumn[72] = 0; 
        }
        current.setMcPref(rolesColumn[72]);
    }

    public void ListernerChangValueRole74(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[73] = 1; 
        } else {
            rolesColumn[73] = 0; 
        }
        current.setSeeAppconf(rolesColumn[73]);
    }

    public void ListernerChangValueRole75(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[74] = 1; 
        } else {
            rolesColumn[74] = 0; 
        }
        current.setCreateUser(rolesColumn[74]);
    }

    public void ListernerChangValueRole76(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[75] = 1; 
        } else {
            rolesColumn[75] = 0; 
        }
        current.setUserConf(rolesColumn[75]);
    }

    public void ListernerChangValueRole77(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[76] = 1; 
        } else {
            rolesColumn[76] = 0; 
        }
        current.setAddUserRole(rolesColumn[76]);
    }

    public void ListernerChangValueRole78(ValueChangeEvent event) {
        System.out.println("new Value : " + event.getNewValue());
        if ((Boolean) event.getNewValue()) {
            rolesColumn[77] = 1; 
        } else {
            rolesColumn[77] = 0; 
        }
    }

    /*
     * public void ListernerChangValueRole79(ValueChangeEvent event){
     * System.out.println("new Value : "+event.getNewValue()); if((Boolean)
     * event.getNewValue()){ role_79 = 1; System.out.println("Role_79 :
     * "+role_79); }else{ role_79 = 0; System.out.println("Role_79 : "+role_79);
     * } } public void ListernerChangValueRole80(ValueChangeEvent event){
     * System.out.println("new Value : "+event.getNewValue()); if((Boolean)
     * event.getNewValue()){ role_80 = 1; System.out.println("Role_80 :
     * "+role_80); }else{ role_80 = 0; System.out.println("Role_80 : "+role_80);
     * }
	}
     */
    // End role value   ----------------------------------------------------
    /**
     * @return the rolenames
     */
    public String getRolenames() {
        return rolenames;
    }

    /**
     * @param rolenames the rolenames to set
     */
    public void setRolenames(String rolenames) {
        this.rolenames = rolenames;
    }
    
    //@pang 
    public  void insert()
    {
        rolenames     = null;
        description   = null;
        fc.print("=================== Insert ======================================");
        try { 
            String link = "pages/appConfig/role/createview.xhtml";
            userLogin.getSessionMenuBarBean().setParam(link);
        } catch (Exception e) {
            fc.print("= insert Error : " + e);
        }
    } 
    public void changeToDelete(String indexDel)
    {
        objRoles  = null;
        itemRoles = null;
        current   = null;  
        current  = getJpaController().findRole(indexDel);
        getCurrent().setDeleted(1);
        try
        {
            getJpaController().edit(getCurrent());
        }
        catch(Exception e)
        {
            fc.print("Error indexDel");
        } 
        String link = "pages/appConfig/role/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
        changToSearch();
    }
    public void changeToCancel()
    {
        String link = "pages/appConfig/role/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
        resetCheck_fl();
        disble_fl    = true; 
        resetBlAll();
        disble_bl   = true;
        resetPcbAll();
        disble_pcb  = true;
        resetSetting(); 
        disble_setting  = true;
        see_fl = false; 
        see_bl  = false;
        see_pcb = false;
        see_refu = false;
        disble_refu  = true;
        see_setting  = false;
        disble_setting = true;
        see_report   = false;
        changToSearch();
    }
    public void changeToSave()
    {
        getCurrent().setRoleName(current.getRoleName());
        getCurrent().setDesscrition(current.getDesscrition());
        try
        {
            getJpaController().edit(getCurrent());
        }
        catch(Exception e)
        {
            fc.print("Error"+e);
        }
        String link = "pages/appConfig/role/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
        printViewColumn();
        resetRolesColumn();
        resetCheck_fl();
        disble_fl    = true; 
        resetBlAll();
        disble_bl   = true;
        resetPcbAll();
        disble_pcb  = true;
        resetSetting(); 
        disble_setting  = true;
        see_fl = false; 
        see_bl  = false;
        see_pcb = false;
        see_refu = false;
        disble_refu  = true;
        see_setting  = false;
        disble_setting = true;
        see_report   = false;
        changToSearch();
    } 
    public void changToSearch()
    {
          objRoles  = null;
          itemRoles = null; 
          fc.print(rolenames);
          if(rolenames != null && !rolenames.trim().isEmpty())
          {
              searchByRolesName();
          }
          else
          {
              searchRoleList();
          }    
          rolenames  = null;
    }        
    public void edit(String index)
    { 
        fc.print("=================== Edit ======================================");
        objRoles  = null;
        itemRoles = null;
        current   = null;  
        current  = getJpaController().findRole(index);
        String link = "pages/appConfig/role/editview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link); 
        getValueCheckBox();
    }
    private  boolean see_fl  = false;
    private  boolean see_bl  = false;
    private  boolean see_pcb = false;
    private  boolean see_refu = false; 
    private  boolean see_report = false;
    private  boolean see_setting = false;
    private  boolean select_bar  = false;
    private  boolean other     = false;
    
    public boolean isSee_fl() {
        return see_fl;
    }

    public void setSee_fl(boolean see_fl) {
        this.see_fl = see_fl;
    }

    public boolean isSee_bl() {
        return see_bl;
    }

    public void setSee_bl(boolean see_bl) {
        this.see_bl = see_bl;
    }

    public boolean isSee_pcb() {
        return see_pcb;
    }

    public void setSee_pcb(boolean see_pcb) {
        this.see_pcb = see_pcb;
    }

    public boolean isSee_refu() {
        return see_refu;
    }

    public void setSee_refu(boolean see_refu) {
        this.see_refu = see_refu;
    }

    public boolean isSee_report() {
        return see_report;
    }

    public void setSee_report(boolean see_report) {
        this.see_report = see_report;
    }

    public boolean isOther() {
        return other;
    }

    public void setOther(boolean other) {
        this.other = other;
    }

    public boolean isSee_setting() {
        return see_setting;
    }

    public void setSee_setting(boolean see_setting) {
        this.see_setting = see_setting;
    }

    public boolean isSelect_bar() {
        return select_bar;
    }

    public void setSelect_bar(boolean select_bar) {
        this.select_bar = select_bar;
    }
    
     
    
    public void getValueCheckBox()
    {
        //======= fl
        if(current.getSeeFl()==1){
            see_fl  = true;
            disble_fl  =false;
            divsible_fl_One  = "display:none;";
            divsible_fl_Two  = "display:;";
            if(current.getCreateFl()==1)
            {
                check_fl_1   = true;
            } 
            if(current.getDeleteFl()==1)
            {
                check_fl_3   = true;
            }
            if(current.getEditFl()==1)
            {   
                check_fl_2   = true;
            }   
            if(current.getListFl()==1)
            {
                check_fl_4   = true;
            }
            if(current.getDetailFrontLine() == 1)
            {
                check_fl_5   = true;
            }
        } 
        
        //======== bl
        if(current.getSeeBl()==1)
        {
            see_bl  = true;
            disble_bl  = false;
            divsible_bl_One  = "display:none;";
            divsible_bl_Two  = "display:;";
            //assy
            if(current.getCreateAssy()==1)
            {
                check_bl_1  = true;
            }
            if(current.getEditAssy() == 1)
            {
                check_bl_2  = true;
            }
            if(current.getDeleteAssy()==1)
            {
                check_bl_3  = true;
            }
            if(current.getApproveAssy()==1)
            {
                check_bl_4  = true;
            }
            if(current.getListAssy()==1)
            {
                check_bl_5  = true;
            }
            if(current.getDetailAssy()==1)
            {
                check_bl_6 = true;
            } 
            
            //auto
            if(current.getCreateAuto()==1)
            {
                 check_bl_7 = true;
            }
            if(current.getEditAuto()==1)
            {
                check_bl_8  = true;
            }
            if(current.getDeleteAuto()==1)
            {
                check_bl_9 = true;
            }
            if(current.getApproveAuto()==1)
            {
                check_bl_10 = true;
            }
            if(current.getListAuto()==1)
            {
                check_bl_11 = true; 
            }
            if(current.getDetailAuto()==1)
            {
               check_bl_12 = true; 
            }
            
            //assembly
            if(current.getCreateAssembly()==1)
            {
                check_bl_13  = true; 
            }
            if(current.getEditAssembly()==1)
            {
                check_bl_14 =true;
            }
            if(current.getDeleteAssembly()==1)
            {
                check_bl_15 =true;
            }
            if(current.getApproveAssembly()==1)
            {
                check_bl_16  = true;
            }
            if(current.getListAssembly()==1)
            {
                check_bl_17 = true;
            }
            if(current.getDetailAssembly()==1)
            {
                check_bl_18  = true;
            }
            
            //packing
            
        }
        //pcb
        if(current.getSeePcb()==1)
        {
            see_pcb = true;
            divible_pcb_one  = "display:none;";
            divible_pcb_two  = "display:;";
            disble_pcb  = false;
            if(current.getCreatePcb01()==1)
            {
                pcb01_1  = true;
            }
            if(current.getEditPcb01()==1)
            {
                pcb01_2 = true;
            }
            if(current.getDeletePcb01() ==1)
            {
                pcb01_3 = true;
            }
            if(current.getApprovePcb01()==1)
            {
                pcb01_4 = true;
            }
            if(current.getListPcb01()==1)
            {
                pcb01_5 =true;
            }
            if(current.getDetailPcb01()==1)
            {
                pcb01_6 =true;
            }
            
            //pcb 02
            if(current.getCreatePcb02()==1)
            {
                pcb02_1  =true;
            }
            if(current.getEditPcb02()==1)
            {
                pcb02_2 =true;
            }
            if(current.getDeletePcb02()==1)
            {
                pcb02_3 =true;
            }
            if(current.getApprovePcb02()==1)
            {
                pcb02_4 = true;
            }
            if(current.getListPcb02()==1)
            {
                pcb02_5 =true;
            }
            if(current.getDetailPcb02()==1)
            {
                pcb02_6  =true;
            }
            
            //pcb 03
            if(current.getCreatePcb03()==1)
            {
                pcb03_1  = true;
            }
            if(current.getEditPcb03()==1)
            {
                pcb03_2  = true;
            }
            if(current.getDeletePcb03() ==1)
            {
                pcb03_3  = true;
            }
            if(current.getApprovePcb03() == 1)
            {
                pcb03_4  =true;
            }
            if(current.getListPcb03()==1)
            {
                pcb03_5 =true;
            }
            if(current.getDetailPcb03()==1)
            {
                pcb03_6  = true;
            }
        }
        
        //refu
       if(current.getSeeRefu()==1)
       {
           see_refu  = true;
           disble_refu  =false; 
           divible_refu_one  = "display:;";
           divible_refu_two  = "display:none;";
           if(current.getCreateRefu01()==1)
           {
               refu01_1  =true;
           }
           if(current.getEditRefu01()==1)
           {
               refu01_2  =true;
           }
           if(current.getDeleteRefu01()==1)
           {
               refu01_3 = true;
           }
           if(current.getApproveRefu01()==1)
           {
               refu01_4 =true;
           }
           if(current.getListRefu01()==1)
           {
               refu01_5 =true;
           }
           if(current.getDetailRefu01()==1)
           {
               refu01_6 =true;
           }
           
           //refu02
           if(current.getCreateRefu02()==1)
           {
               refu02_1 =true;
           }
           if(current.getEditRefu02()==1)
           {
               refu02_2 = true;
           }
           if(current.getDeleteRefu02()==1)
           {
               refu02_3 =true;
           }
           if(current.getApprovePcb02()==1)
           {
               refu02_4  = true;
           }
           if(current.getListRefu02() == 1)
           {
               refu02_5  = true;
           }
           if(current.getDetailRefu02() ==1)
           {
               refu02_6  = true;
           }
           
           //pcb03
           if(current.getCreateRefu03() ==1)
           {
               refu03_1  = true;
           }
           if(current.getEditRefu03()==1)
           {
               refu03_2 = true;
           }
           if(current.getDeleteRefu03()==1)
           {
               refu03_3  = true;
           }
           if(current.getApproveRefu03()==1)
           {
               refu03_4 =true;
           }
           if(current.getListRefu03() ==1)
           {
               refu03_5 = true;
           }
           if(current.getDetailRefu03() ==1)
           {
               refu03_6  =true;
           } 
           
       }
       //report
       if(current.getSeeReport()==1)
       {
           see_report  = true;
           disble_setting  = false;
           divble_setting_one  = "display:none;";
           divble_setting_two  = "display:;";
           if(current.getCreateUser()==1)
           {
               acc_1  =true;
           }
           if(current.getUserConf()==1)
           {
               acc_2  =true;
           }
           if(current.getAddUserRole()==1)
           {
               acc_3  =true;
           }
           if(current.getCreateMc()==1)
           {
               mc_1  =true;
           }
           if(current.getMcPref()==1)
           {
               mc_2 =true;
           }
       }
       //setting
       if(current.getSeeSetting()==1)
       {
           see_setting = true;
       }
       
       //other
//       if(current.gets)
//       {
//       }
        
    }
    public void previous() {
        getWorksheetpagination().previousPage();
        setWorksheet();
    }

    public void next() {
        getWorksheetpagination().nextPage();
        setWorksheet();
    }

    public int getAllPages() {
        int all = getWorksheetpagination().getItemsCount();
        int pagesize = getWorksheetpagination().getPageSize();
        return ((int) all / pagesize) + 1;
    }
    public PaginationHelper getWorksheetpagination() {
        if (worksheetpagination == null) {
            worksheetpagination = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    return getItemRoles().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    fc.print("PageFirst : " + getPageFirstItem());
                    fc.print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (getItemRoles().size() > 0) {
                        listmodel = new ListDataModel(getItemRoles().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(getItemRoles());
                    }
                    return listmodel;
                }
            };
        }
        return worksheetpagination;
    }
    private void searchByRolesName() { 
        fc.print(rolenames);
        count     = 1;
        objRoles  = getJpaController().findRoleNameSearchNotDelete(rolenames);
        for(Role objrole : objRoles)
        {
            getItemRoles().add(new roleDetail(objrole.getIdRole(),Integer.toString(count),objrole.getRoleName(),objrole.getDesscrition()));
            count++;
        }
        setWorksheet();
    } 
    private void searchRoleList() {
        objRoles   =  getJpaController().findRoleNameAllNotDelete();
        count      =  1;
        for(Role listRoles : objRoles)
        {
            getItemRoles().add(new roleDetail(listRoles.getIdRole(),Integer.toString(count),listRoles.getRoleName(),listRoles.getDesscrition()));
            count++;
        }
       setWorksheet(); 
    }

    public List<roleDetail> getItemRoles() {
        if(itemRoles  == null)
        {
            itemRoles  = new ArrayList<roleDetail>();
        }
        return itemRoles;
    }

    public void setItemRoles(List<roleDetail> itemRoles) {
        this.itemRoles = itemRoles;
    }

    public List<Role> getObjRoles() {
        return objRoles;
    }

    public void setObjRoles(List<Role> objRoles) {
        if(objRoles ==null)
        {
            objRoles  = new ArrayList<Role>();
        }
        this.objRoles = objRoles;
    }
    //@pang
    private String  divsible_fl_One  = "display:;";
    private String  divsible_fl_Two  = "display:none;";
    
    public String getDivsible_fl_One() {
        return divsible_fl_One;
    }

    public void setDivsible_fl_One(String divsible_fl_One) {
        this.divsible_fl_One = divsible_fl_One;
    }

    public String getDivsible_fl_Two() {
        return divsible_fl_Two;
    }

    public void setDivsible_fl_Two(String divsible_fl_Two) {
        this.divsible_fl_Two = divsible_fl_Two;
    }
     
     
    
    private  String [] valueOfFontline = new String[5];

    public String[] getValueOfFontline() {
        return valueOfFontline;
    }

    public void setValueOfFontline(String[] valueOfFontline) {
        this.valueOfFontline = valueOfFontline;
    }
   
    private void printViewColumn()
    {
        for(int i =0 ;i<80 ;i++)
        {
            fc.print("rolenames : "+i+" : "+rolesColumn[i]);
        } 
    
    }   
    private void resetRolesColumn() 
    {
        for(int i =0 ;i<80 ;i++)
        {
            rolesColumn[i] = 0;
        }    
    }   
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DataModel getWorksheet() {
        return worksheet;
    }

    public void setWorksheet() {
         this.worksheet = getWorksheetpagination().createPageDataModel();
    }

    public RoleController getRolecontroller() {
        return rolecontroller;
    }

    public void setRolecontroller(RoleController rolecontroller) {
        this.rolecontroller = rolecontroller;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int[] getRolesColumn() {
        return rolesColumn;
    }

    public void setRolesColumn(int[] rolesColumn) {
        this.rolesColumn = rolesColumn;
    }

    public Role getCurrent() {
        return current;
    }

    public void setCurrent(Role current) {
        if(current == null)
        {
            current  = new Role();
        }    
        this.current = current;
    }

    public boolean[] getShow_Fl_Checkbox() {
        return show_Fl_Checkbox;
    }

    public void setShow_Fl_Checkbox(boolean[] show_Fl_Checkbox) {
        this.show_Fl_Checkbox = show_Fl_Checkbox;
    }

    public boolean[] getValue_Fl_CheclBox() {
        return value_Fl_CheclBox;
    }

    public void setValue_Fl_CheclBox(boolean[] value_Fl_CheclBox) {
        this.value_Fl_CheclBox = value_Fl_CheclBox; 
    } 
} 
