package com.stopprocess;
import com.tct.data.CurrentProcess;
import com.tct.data.jpa.CurrentProcessJpaController;
import  com.component.barcodeauto.BarcodeAuto;
import com.shareData.ShareDataOfSession;
import com.tct.data.Mc;
import com.tct.data.jpa.McJpaController;
import java.util.List;
import javax.persistence.Persistence;
import com.tct.data.McDown;
import com.tct.data.jpa.McDownJpaController;
import com.tct.data.MainData;
import com.tct.data.jpa.MainDataJpaController; 
import com.tct.data.PcsQty;
import com.tct.data.jpa.PcsQtyJpaController;

import com.time.timeCurrent;
import java.awt.Event;
import java.util.ArrayList;
import java.util.UUID;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
public class StopProcess {
    private boolean  stopDowntimeProcessPopup    = false;
    private String  typeClose;
    private String  visibles;
    private String  mcName;
    private String  remark;
    
    private CurrentProcess  currentData = null;
    private PcsQty  currentPcsqty  = null;
    private McDown  mcDown  = null;
    private String  validateDowntimeReason = null;
    private List<SelectItem> stopTimeReason = null;
    private String resonUse = null; 
    private String startdowntime;
    CurrentProcessJpaController  currentProcessJpaController  = null; 
    McJpaController   mcJpaController = null;
    McDownJpaController   mcDownJpaController  = null;
    MainDataJpaController  mainDataJpaController =null;
    PcsQtyJpaController    pcsQtyJpaController  = null;
    
     public BarcodeAuto getBarcodeAuto(){  
            FacesContext context = FacesContext.getCurrentInstance();
            return (BarcodeAuto) context.getExternalContext().getSessionMap().get("barcode"); 
        } 
    
    public ShareDataOfSession getShareData(){
          FacesContext context = FacesContext.getCurrentInstance();
          return   (ShareDataOfSession) context.getExternalContext().getSessionMap().get("sharedata");  
        }
    public CurrentProcessJpaController getCurrentProcessJpaController()
    {
        if(currentProcessJpaController  == null)
        {
            currentProcessJpaController  = new CurrentProcessJpaController(Persistence.createEntityManagerFactory("tct_projectxPU")); 
        }
     return currentProcessJpaController;   
    }  
    public McJpaController getMcJpaController()
    {
        if(mcJpaController == null)
        {
            mcJpaController = new McJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return  mcJpaController;
    }
    public McDownJpaController getMcDownJpaController()
    {
        if(mcDownJpaController ==null)
        {
            mcDownJpaController  = new McDownJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mcDownJpaController;
    }
    public MainDataJpaController getMainDataJpaController(){
        if(mainDataJpaController == null)
        {
            mainDataJpaController  = new MainDataJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mainDataJpaController;
    } 
    public PcsQtyJpaController getPcsQtyJpaController()
    {
       if(pcsQtyJpaController == null)
       {
           pcsQtyJpaController  = new PcsQtyJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
       }
       return pcsQtyJpaController;
    }
     
    public void closePopup(String typeClose) {
        this.typeClose = typeClose;
        if(this.typeClose.equals("stopDowntimeProcessPopup") && !resonUse.equals("0"))
        {
            insertStatusDowntimeToOpen();
            stopDowntimeProcessPopup       = false; 
            ///========= Set menu
            getBarcodeAuto().status  = "Open";   
            getShareData().openProcessButton      = true;
            getBarcodeAuto().openProcessButtonShow = "open_process_button_disable";
            getShareData().closeProcessButton        = true;
            getBarcodeAuto().closeProcessButtonShow    = "close_process_button_disable";
            getShareData().mcDownTimeButton       = true;
            getBarcodeAuto().mcDownTimeButtonShow   = "mc_downtime_button_disable";
            getShareData().holdTimeButton           = true;
            getBarcodeAuto().holdTimeButtonShow       = "hold_process_button_disable"; 
            getShareData().stopTimeButton           = true;
            getBarcodeAuto().stopTimeButtonShow       = "stop_downtime_button_disable";
            getShareData().moveProcessButton        = true;
            getBarcodeAuto().moveProcessButtonShow    = "move_mc_button_disable";
            resonUse   = "0";
            remark     = null;
            
        }
        else if(this.typeClose.equals("stopDowntimeProcessPopupClose"))
        {
            stopDowntimeProcessPopup       = false;
            resonUse   = "0";
            remark     = null;
        } 

    }	
    public void openPopupType(String visibles)
    {  
        this.visibles = visibles;
        if(this.visibles.equals("StopDowntime"))
        {
            if(!getShareData().stopTimeButton)
                stopDowntimeProcessPopup  = true;
            
            //=========== This  function  to get data for Stop downtime
            prePareLoadData();
        }
    }

    public boolean isStopDowntimeProcessPopup() {
        return stopDowntimeProcessPopup;
    } 
    public void prePareLoadData(){ 
         
        //========Find  Mc
        System.out.println("======================= Prepare Downtime Data =============================");
        System.out.println("=========== Main Data id   : "+getShareData().mainDataId);
        
        //======== Find current processs
        List <CurrentProcess>  dataListMc    =  getCurrentProcessJpaController().findCurrentStatusAndOpen(getShareData().mainDataId,"Open");
        mcName = dataListMc.get(0).getMcId().getMcName();
        startdowntime  = dataListMc.get(0).getTimeCurrent().toString();
        //====== load list reason
        stopTimeReason  = new ArrayList<SelectItem>(); 
        stopTimeReason.add(new SelectItem("0","------- Select one --------"));
        stopTimeReason.add(new SelectItem("Abnormal production"));
        stopTimeReason.add(new SelectItem("Change over"));
        stopTimeReason.add(new SelectItem("Electric shutdown"));
        stopTimeReason.add(new SelectItem("Equipment breakdowns"));
        stopTimeReason.add(new SelectItem("Machine idle"));
        stopTimeReason.add(new SelectItem("Others"));
        stopTimeReason.add(new SelectItem("Preventive maintenance"));
        stopTimeReason.add(new SelectItem("Process failure"));
        stopTimeReason.add(new SelectItem("Production adjustment"));
        stopTimeReason.add(new SelectItem("Quality defects"));
        stopTimeReason.add(new SelectItem("Set up"));
        stopTimeReason.add(new SelectItem("Unplan"));
    }  
    public void ChangeStopTimeReason(ValueChangeEvent event )
    { 
            resonUse  = (String) event.getNewValue();
            System.out.println("===== Resonuse "+resonUse);
            if(resonUse.equals("0"))
            {
                validateDowntimeReason = "Select one reason";
            }
            else
            {
                validateDowntimeReason  = "";
            }
    }
    
    
    public String getMcName() {
        return mcName;
    } 
    public void setMcName(String mcName) {
        this.mcName = mcName;
    }
    //======== changeStatusDowntimeToOpen
    public void insertStatusDowntimeToOpen()
    {
        timeCurrent timeNow  = new timeCurrent();
        
       //=========== create status downtime to open
      //List <CurrentProcess> currentProcessOld   =  getCurrentProcessJpaController().findCurrentStatusAndOpen(getShareData().mainDataId,"Open");  
      CurrentProcess  currentProcessOldProces  = getCurrentProcessJpaController().findCuurentDowntime(getShareData().mainDataId);
      //=======Update status
      currentProcessOldProces.setCurrentProc("0");
      try
      {
          getCurrentProcessJpaController().edit(currentProcessOldProces);
      }
      catch(Exception e)
      {
          System.out.println("Error not update  current proccess in current table");
      }
         
      String uuidCurrent = UUID.randomUUID().toString();
      getCurrentProcess().setId(uuidCurrent);
      getCurrentProcess().setCurrentProc("Null");
      getCurrentProcess().setShift(currentProcessOldProces.getShift());
      getCurrentProcess().setStatus("Open");
      getCurrentProcess().setMainDataId(currentProcessOldProces.getMainDataId());
      getCurrentProcess().setCurrentProc("1");
      getCurrentProcess().setTimeCurrent(timeNow.getDate());
      getCurrentProcess().setMcId(currentProcessOldProces.getMcId()); 
      getCurrentProcess().setLine(currentProcessOldProces.getLine());
      getCurrentProcess().setHr(currentProcessOldProces.getHr()); 
      try
      {
          getCurrentProcessJpaController().create(getCurrentProcess());
      }
      catch(Exception e )
      {
          System.out.println("==== Not to create new main data"+e);  
      
      }
      //========= Update current process in maindata
      currentProcessOldProces.getMainDataId().setCurrentNumber(getCurrentProcess().getId());
      try{
          getMainDataJpaController().edit(currentProcessOldProces.getMainDataId());
      }
      catch(Exception e)
      {
          System.out.println("==== Maindata current"+e);
      }    
      
      
      //=========Update statuc mc to use
      currentProcessOldProces.getMcId().setMcStatus("use");
      try
      {
          getMcJpaController().edit(currentProcessOldProces.getMcId());
      }
      catch(Exception e)
      {
          System.out.println("Error mc"+e);
      }
      
      //========= create Pcsqty
      PcsQty pcsQtyOld  = getPcsQtyJpaController().findPcsQtyCurrentProcessId(currentProcessOldProces);
      
      String uuiPcsQty= UUID.randomUUID().toString();
      getPcsQty().setId(uuiPcsQty);
      getPcsQty().setWipQty("0");
      getPcsQty().setQtyType("in");
      getPcsQty().setQty(pcsQtyOld.getQty());
      getPcsQty().setCurrentProcessId(getCurrentProcess());
      try
      {
          getPcsQtyJpaController().create(getPcsQty());
      }
      catch(Exception e)
      {
          System.out.println("===== pcsqty error"+e);
      }
      
      //========= Create mc down
      
      String uuidMcDown= UUID.randomUUID().toString();
      getMcDown().setId(uuidMcDown);
      getMcDown().setCurrentProcessId(getCurrentProcess());
      getMcDown().setMcId(currentProcessOldProces.getMcId());
      getMcDown().setReson(resonUse);
      getMcDown().setRemark(remark); 
      getMcDown().setRefDownTimeStart(currentProcessOldProces.getId());
      try
      {
          getMcDownJpaController().create(getMcDown());
      }
      catch(Exception e)
      {
          System.out.println("Error create mc down");
      }     
         
    } 
    public String getRemark() {
        return remark;
    }
 
    public void setRemark(String remark) {
        this.remark = remark;
    }
    
    //========= 
    public CurrentProcess getCurrentProcess()
    {
        if(currentData  == null)
        {
            currentData = new CurrentProcess();
        }
        return  currentData;
    }
    public PcsQty  getPcsQty()
    {
        if(currentPcsqty  == null)
        {
            currentPcsqty = new PcsQty();
        }
        return  currentPcsqty;
    }
    public McDown getMcDown()
    {
        if(mcDown == null)
        {
             mcDown  = new McDown();   
        }
        return mcDown;
    }

    /**
     * @return the validateDowntimeReason
     */
    public String getValidateDowntimeReason() {
        return validateDowntimeReason;
    }

    /**
     * @param validateDowntimeReason the validateDowntimeReason to set
     */
    public void setValidateDowntimeReason(String validateDowntimeReason) {
        this.validateDowntimeReason = validateDowntimeReason;
    }

    /**
     * @return the stopTimeReason
     */
    public List<SelectItem> getStopTimeReason() {
        return stopTimeReason;
    }

    /**
     * @param stopTimeReason the stopTimeReason to set
     */
    public void setStopTimeReason(List<SelectItem> stopTimeReason) {
        this.stopTimeReason = stopTimeReason;
    }

    /**
     * @return the resonUse
     */
    public String getResonUse() {
        return resonUse;
    }

    /**
     * @param resonUse the resonUse to set
     */
    public void setResonUse(String resonUse) {
        this.resonUse = resonUse;
    }

    public String getStartdowntime() {
        return startdowntime;
    }

    public void setStartdowntime(String startdowntime) {
        this.startdowntime = startdowntime;
    }
    
}
