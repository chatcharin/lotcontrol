/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.component.barcodeauto;
import com.tct.data.CurrentProcess;
/**
 *
 * @author The_Boy_Cs
 */
public class ProcList{
        private int  sqProcNum;    
        String name;
        String shift;
        String startTime;
        String finishTime;
        String  inputQty ;
        String  ouPutQty ; 
        String  mcName;
        String  ngQty;
        CurrentProcess index;
        String  cssCurrentStatusProcess;
        String  ngRepair;
        String  percentNgNormal;
        String  percentNgRepair;
        private int     sqprocess ;
        String  tactTime;
        String  yield ;
        
        
        public ProcList(int sqpProcNum,String name,String shift, String startTime,String finishTime, String  inputQty , String  ouPutQty,String mcName,String ngQty,CurrentProcess index,String cssCurrentStatusProcess, String  ngRepair,String  percentNgNormal, String  percentNgRepair,String tactTime,String yield){
            this.sqProcNum  = sqpProcNum; 
            this.name  = name; 
            this.shift = shift;
            this.startTime = startTime;
            this.finishTime = finishTime;
            this.inputQty   = inputQty;
            this.ouPutQty   = ouPutQty;
            this.mcName     = mcName;
            this.ngQty      = ngQty;
            this.index      = index;
            this.cssCurrentStatusProcess  = cssCurrentStatusProcess;
            this.ngRepair   = ngRepair;
            this.percentNgNormal  = percentNgNormal;
            this.percentNgRepair  = percentNgRepair ;
            this.tactTime  = tactTime;
            this.yield    = yield;
        }

    public String getTactTime() {
        return tactTime;
    }

    public void setTactTime(String tactTime) {
        this.tactTime = tactTime;
    }

    public String getYield() {
        return yield;
    }

    public void setYield(String yield) {
        this.yield = yield;
    }
        
        public int getSqpProcNum(){
            return sqProcNum;
        } 
        public void setSqpProcNum(int sqProcNum)
        {
            this.sqProcNum = sqProcNum;
        }
        public String getName(){
            return name; 
        }
        public void setName(String name)
        {
            this.name   = name;
        }
        public String getShift(){
            return shift;
        }
        public void setShift(String shift)
        {
            this.shift  = shift;
        }        
        public String getStartTime(){
            return startTime;
        }
        public void setStartTime(String startTime)
        {
            this.startTime  = startTime;
        }
        public String getFinishTime(){
            return finishTime;
        }
        public void setFinishTime(String finishTime )
        {
            this.finishTime   = finishTime;
        }
        public String getInputQty(){
            return inputQty;
        }
        public void setInputQty(String inputQty)
        {
            this.inputQty  = inputQty;
        }
        public String getOuPutQty(){
            return ouPutQty;
        }
        public void setOuPutQty(String ouPutQty)
        {
            this.ouPutQty  = ouPutQty;
        }  
        public String getMcName(){
            return mcName;
        }
        public void setMcName(String mcName)
        {
            this.mcName  = mcName;
        }
        public String getNgQty(){
            return ngQty;
        }
        public void setNgQty(String ngQty)
        {
            this.ngQty  = ngQty;
        }
        public CurrentProcess getIndex(){
            return index;
       }
       public void setIndex(CurrentProcess index)
       {
           this.index  = index;
       } 
        public String getCssCurrentStatusProcess(){
            return cssCurrentStatusProcess;
       }
       public void setCssCurrentStatusProcess(String cssCurrentStatusProcess)
       {
           this.cssCurrentStatusProcess  = cssCurrentStatusProcess;
       } 
       public String getNgRepair(){
           return  ngRepair;
       } 
       public void setNgRepair(String ngRepair)
       {    
           this.ngRepair  = ngRepair;
       }
       public String getPercentNgNormal(){
           return  percentNgNormal;
       } 
       public void setPercentNgNormal(String percentNgNormal)
       {
           this.percentNgNormal  = percentNgNormal;
       }
       public String getPercentNgRepair(){
           return  percentNgRepair;
       }   
       public void setPercentNgRepair(String percentNgRepair)
       {
           this.percentNgRepair  = percentNgRepair;
       }

    /**
     * @return the sqprocess
     */
    public int getSqprocess() {
        return sqprocess;
    }

    /**
     * @param sqprocess the sqprocess to set
     */
    public void setSqprocess(int sqprocess) {
        this.sqprocess = sqprocess;
    }
    }
