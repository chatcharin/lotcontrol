/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.component.barcodeauto;

import com.tct.data.CurrentProcess;
import com.tct.data.NgNormal;
import com.tct.data.NgRepair;
import java.util.List;
import com.tct.data.jpa.NgRepairJpaController;
import com.tct.data.jpa.NgNormalJpaController;
import javax.persistence.Persistence;
import com.tct.data.Pc2ng;
import com.tct.data.jpa.Pc2ngJpaController;
import com.closeprocess.NgName;
import com.closeprocess.NgRepairList;
import com.tct.data.PcsQty;
import com.tct.data.jpa.PcsQtyJpaController;
import java.util.ArrayList;
import com.tct.data.ProcessPool;
import java.util.Collections;
import java.util.UUID;
import com.tct.data.NgPool;
import com.tct.data.jpa.NgPoolJpaController;
import java.math.BigDecimal;
import javax.faces.context.FacesContext;
/**
 *
 * @author pang
 */
public class viewNgAndNgRepair {
    private String ngQty = "0";
    private String outputQty  = "0"; 
    private int viewNgleft  = 0;
    private int viewNgRepairleft = 0 ;
    private int sumViewNgNormal;
    private int sumViewNgRepaor ;
    private String viewValidateNgLeft  = null;
    private boolean statusNgRepair  = false;
    private boolean statusNgNormal  = false;
    private String validateViewNgRepairLeft  = null;
    private boolean inputNgQtyButton  = false;
    private boolean inputNgRepaitButton  = false;
    private String  validateOutput  = null;
    private boolean confirmEdit   = false;
    private boolean editNgRepairButton  = false;
    //======= status Check to end
    private boolean checkNgNormalStatus  = false;
    private boolean checkNgRepairStattus = false;
    private boolean checkOutputStatus    = false;
    
    private boolean showConfirm    = false;
    
    public boolean viewDetial  = false;
    private NgNormalJpaController  ngNormalJpaController  = null;
    private NgRepairJpaController   ngRepairJpaController  = null;
    private Pc2ngJpaController      pc2ngJpaController  = null;
    private PcsQtyJpaController     pcsQtyJpaController  = null;
    private NgPoolJpaController    ngPoolJpaController    = null;
    
    //============DataUpdate
    PcsQty  currentPcsqty     = null;
    NgNormal currentNgNormal  = null;
    NgRepair currentNgRepair  = null;
     public BarcodeAuto getBarcodeAuto(){  
            FacesContext context = FacesContext.getCurrentInstance();
            return (BarcodeAuto) context.getExternalContext().getSessionMap().get("barcode"); 
        }
    
    public NgNormalJpaController getNgNormalJpaController()
    {
        if(ngNormalJpaController  == null)
        {
            ngNormalJpaController  = new NgNormalJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return ngNormalJpaController;
    }
    public NgRepairJpaController getNgRepairJpaController()
    {
        if(ngRepairJpaController  == null)
        {
            ngRepairJpaController   =  new NgRepairJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return ngRepairJpaController;
    }
    public Pc2ngJpaController getPc2ngJpaController()
    {
        if(pc2ngJpaController  == null)
        {
            pc2ngJpaController  = new Pc2ngJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return pc2ngJpaController;
    }
    public PcsQtyJpaController getPcsQtyJpaController()
    {
        if(pcsQtyJpaController == null)
        {
            pcsQtyJpaController  = new PcsQtyJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }   
        return pcsQtyJpaController;
    }
    public NgPoolJpaController getNgPoolJpaController()
    {
        if(ngPoolJpaController ==null)
        {
            ngPoolJpaController  = new NgPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return ngPoolJpaController;
    }
    
    
    private   List<NgNormal>  dataListngNormal = null;
    public List<NgNormal>  getDataListngNormal()
    {
        return dataListngNormal;
    }
    public void setDataListngNormal(List<NgNormal> dataListngNormal )
    {
        this.dataListngNormal = dataListngNormal;
    }
    
    //======== ViewNgNormal
    public  void viewNgNormal(CurrentProcess ngNumsAll)
    {
        System.out.println("======= num ng :"+ngNumsAll);
        int ingSumNgNormal  = 0;
        dataListngNormal  =  getNgNormalJpaController().findSumNg(ngNumsAll); 
        for(NgNormal sumNgnormal : dataListngNormal)
        {
            ingSumNgNormal += Integer.parseInt(sumNgnormal.getQty());
        }
        
        if(ingSumNgNormal >  0)
        {
           viewDetial  = true; 
        }
        else
        {
            viewDetial = false;  
            System.out.println("===== Equal 0");
        }  
    }
    public boolean getViewDetail()
    {
        return viewDetial;
    
    } 
    //====== ViewNgPair and edit
    private List<NgRepair> dataListNgRepair  = null;
    private boolean  viewDetailNgRepairPopup   = false;
    public boolean getViewDetailNgRepairPopup()
    {
        return viewDetailNgRepairPopup;
    }
    public void setViewDetailNgRepairPopup(boolean viewDetailNgRepairPopup)
    {
        this.viewDetailNgRepairPopup = viewDetailNgRepairPopup;
    }
    public List<NgRepair>  getDataListNgRepair()
    {
        return dataListNgRepair;
    }
    public void setDataListNgRepair( List<NgRepair>  dataListNgRepair)
    {
        this. dataListNgRepair  =  dataListNgRepair;
    }
    
    //======== View ng Repair and edit
    private int sumNgRepair ;
    private String ngRepairQty = "0";
      
    public int getSumNgRepair()
    {
        return sumNgRepair;
    }
    public void setSumNgRepair(int sumNgRepair)
    {
        this.sumNgRepair = sumNgRepair;
    }
    private List<NgName>  ngNormalList        = null;
    private List<NgRepairList>  ngRepairList  = null;
    CurrentProcess  currentProcesId = null ;
    
    public void viewNgRepair(CurrentProcess currentProcesId)
    {
        this.currentProcesId   = currentProcesId;
        System.out.println("++++++++++ Current process is "+this.currentProcesId);
        sumNgRepair =  0;
        dataListNgRepair  = getNgRepairJpaController().findSumNgRepair(this.currentProcesId );
        for(NgRepair ngRepairsum : dataListNgRepair)
        {
            sumNgRepair += Integer.parseInt(ngRepairsum.getQty());
        }
        ngRepairQty   = Integer.toString(sumNgRepair);
        
       //viewNgRepairleft  = sumNgRepair;
        System.out.println("++++++ sumNgRepair "+sumNgRepair);
       if(sumNgRepair == 0)
       {
           setEditNgRepairButton(false);
           viewDetailNgRepairPopup = false;
       }   
       else
       {
           setEditNgRepairButton(true);
           viewDetailNgRepairPopup = true;
       }
    } 
    //======= Close detail
    public void closeDetialPopup()
    {
        viewDetial  = false; 
        viewDetailNgRepairPopup = false;
        editNgrepairBooleanPopup  = false;
    } 
    
    //========== Edit ng repair
    private boolean editNgrepairBooleanPopup  = false;
    public boolean getEditNgrepairBooleanPopup()
    {
        return editNgrepairBooleanPopup;
    }
    public void setEditNgrepairBooleanPopup(boolean editNgrepairBooleanPopup)
    {
        this.editNgrepairBooleanPopup  = editNgrepairBooleanPopup;
    } 
    public void editNgRepair()
    {
        viewDetial  = false; 
        viewDetailNgRepairPopup = false;
        editNgrepairBooleanPopup  = true; 
        //==== Set default value
        setNgQty("0");
        setOutputQty("0");
    }
    
    //Calcutale NgRepair
    private String validateNgRepaitQty = null;
    int intngRepaienums ;
    public void calculateNgRepair()
    {
       System.out.println("================ calculateNgRepair() ============"); 
       intngRepaienums  = 0;
       int intInputNgQty    = Integer.parseInt(ngQty);
       int intOutQty        = Integer.parseInt(outputQty);
       intngRepaienums      = sumNgRepair -(intOutQty+intInputNgQty);
       viewNgRepairleft     = intngRepaienums; 
       if(intngRepaienums < 0)
       {
            ngRepairQty     = Integer.toString(intngRepaienums);
            inputNgRepaitButton  = false;
            setValidateNgRepaitQty("NgRepair more than or eqaul zero");
       }
       else
       { 
            ngRepairQty     = Integer.toString(intngRepaienums); 
            setValidateNgRepaitQty("");
            inputNgRepaitButton  = true;
            checkDetailNgRepair();
       }     
    }
     
    public String getNgQty() {
        return ngQty;
    }
    private String validateNgQty  = null;
    
    public void setNgQty(String ngQty) {
        this.ngQty = ngQty;
        boolean check  =false;
        int ngQtyNum;
        try{ 
            ngQtyNum    =  Integer.parseInt(ngQty);
            viewNgleft  =  ngQtyNum;
            inputNgQtyButton  = true;
            check  = true; 
            calculateNgRepair();
            setValidateNgQty(""); 
        }
        catch(Exception e)
        {
            setValidateNgQty("Invalid input ngQty");
            inputNgQtyButton  = false; 
        }
        if(check)
        {
            checkDetailNgNormal();
        }
    }
    public void checkDetailNgNormal()
    {
        if(ngQty.equals("0")){
            setValidateNgQty("");
            checkNgNormalStatus  = true;   
        }  
        else
        {
            if(sumViewNgNormal == Integer.parseInt(ngQty))
            {
                setValidateNgQty("");
                checkNgNormalStatus  = true;
            } 
            else 
            {
                setValidateNgQty("Input NgQty Detail");
                checkNgNormalStatus  = false;
            } 
        }
        showButtonCnfirm();
    }
    public String getOutputQty() {
        return outputQty;
    }
    public void setOutputQty(String outputQty) {
        this.outputQty = outputQty;
        int outputNum;
        try
        {
            outputNum  = Integer.parseInt(outputQty);
            setValidateOutput(""); 
            checkOutputStatus    = true;
            calculateNgRepair(); 
        }
        catch(Exception e)
        {
             setValidateOutput("Invalid input");
             checkOutputStatus    = false;
        } 
        showButtonCnfirm();
    } 
    /**
     * @return the ngRepair
     */
    public String getNgRepairQty() {
        return ngRepairQty;
    }

    /**
     * @param ngRepair the ngRepair to set
     */
    public void setNgRepairQty(String ngRepairQty) {
        this.ngRepairQty = ngRepairQty;  
    }
public void checkDetailNgRepair()
{
    if(ngRepairQty.equals("0"))
    {
        setValidateNgRepaitQty("");
        checkNgRepairStattus = true;
    }
    else{
        if(sumViewNgRepaor  == Integer.parseInt(ngRepairQty))
        {
            setValidateNgRepaitQty("");
            checkNgRepairStattus = true;
        } 
        else
        {
            setValidateNgRepaitQty("Input ngRepair Detail");
            checkNgRepairStattus = false;
        }
    }
    showButtonCnfirm();
}
    
//======= Show button confirm
public void showButtonCnfirm()
{
    System.out.println(checkNgRepairStattus +" : "+checkOutputStatus+" : "+checkNgNormalStatus);
    if((checkNgRepairStattus == true) &&  (checkOutputStatus    == true) && (checkNgNormalStatus  == true   ))
    {
        showConfirm  = true;
    }
    else
    {
        showConfirm  = false;
    }
}    
    /**
     * @return the validateNgRepaitQty
     */
    public String getValidateNgRepaitQty() {
        return validateNgRepaitQty;
    }

    /**
     * @param validateNgRepaitQty the validateNgRepaitQty to set
     */
    public void setValidateNgRepaitQty(String validateNgRepaitQty) {
        this.validateNgRepaitQty = validateNgRepaitQty;
    }

    /**
     * @return the validateNgQty
     */
    public String getValidateNgQty() {
        return validateNgQty;
    }

    /**
     * @param validateNgQty the validateNgQty to set
     */
    public void setValidateNgQty(String validateNgQty) {
        this.validateNgQty = validateNgQty;
    }
    
    //===== popup
    private boolean inputQtyPopup = false;
    private boolean inputNgRepairQtyPopup  = false;
    private List<Pc2ng> dataListPc2ng = null;
    public void openPopup(String typePopup)
    {
          ProcessPool proc  =   currentProcesId.getMainDataId().getIdProc();
          dataListPc2ng  = getPc2ngJpaController().findIdNgPool(proc);
        
        if(typePopup.equals("inputNgQty"))
        {
            if(!ngQty.equals("0")) 
            inputQtyPopup   = true; 
            List<String>   objNgNormal   = new ArrayList<String>(dataListPc2ng.size());  
            
            for(Pc2ng  dataOneListPc2ng:dataListPc2ng)
            {
                if(dataOneListPc2ng.getIdNg().getNgType().equals("ng"))
                {
                    objNgNormal.add(dataOneListPc2ng.getIdNg().getNgName());
                } 
            } 
            Collections.sort(objNgNormal);
            setNgNormalList(new ArrayList<NgName>());  
            for(String  listNg:objNgNormal)
            {
                 NgPool  objListOne  = getNgPoolJpaController().findByNgName(listNg); 
                 ngNormalList.add(new NgName(objListOne.getNgName(),"0",0, ""));
            }   
        }
        else if(typePopup.equals("inputRepairQty"))
        {
            if(!ngRepairQty.equals("0"))
            inputNgRepairQtyPopup  = true; 
            List<String>   objNgRepair   = new ArrayList<String>(dataListPc2ng.size());
            for(Pc2ng  dataOneRepairListPc2ng:dataListPc2ng)
            { 
                if(dataOneRepairListPc2ng.getIdNg().getNgType().equals("repair"))
                {
                    objNgRepair.add(dataOneRepairListPc2ng.getIdNg().getNgName());
                } 
            } 
            Collections.sort(objNgRepair);
            setNgRepairList(new ArrayList<NgRepairList>());  
            for(String listNgRepair :objNgRepair )
            {
                 NgPool  objRepairListOne  = getNgPoolJpaController().findByNgName(listNgRepair);
                 ngRepairList.add(new NgRepairList(objRepairListOne.getNgName(),"0",0, ""));
            } 
            
        }
        else if(typePopup.equals("confirmEdit"))
        {
             confirmEdit   = true;
             editNgrepairBooleanPopup  = false;
        }
    } 
    public void closePopup(String typePopup)
    {
        if(typePopup.equals("inputNgQty"))
        {
            inputQtyPopup   = false;
        }
        else if(typePopup.equals("inputRepairQty"))
        {
            inputNgRepairQtyPopup  = false;
        }  
        else if(typePopup.equals("confirmYes")){
             confirmEdit   = false; 
             sumNgNormal = 0;
             updateDataAllList(); 
            
        }
        else if(typePopup.equals("confirmNo")){
             confirmEdit   = false;
            
        }
    } 
    
    public boolean getInputQtyPopup() {
        return inputQtyPopup;
    } 
    
    public void setInputQtyPopup(boolean inputQtyPopup) {
        this.inputQtyPopup = inputQtyPopup;
    }
 
    public boolean isInputNgRepairQtyPopup() {
        return inputNgRepairQtyPopup;
    }
 
    public void setInputNgRepairQtyPopup(boolean inputNgRepairQtyPopup) {
        this.inputNgRepairQtyPopup = inputNgRepairQtyPopup;
    }
 
    public List<NgRepairList> getNgRepairList() {
        return ngRepairList;
    }
 
    public void setNgRepairList(List<NgRepairList> ngRepairList) {
        this.ngRepairList = ngRepairList;
    }
 
    public List<NgName> getNgNormalList() {
        return ngNormalList;
    } 
    public void setNgNormalList(List<NgName> ngNormalList) {
        this.ngNormalList = ngNormalList;
        System.out.println("========= Calculate ng ");   
        //caculateViewNgNormal();
    }  
    //====== Calculate ngNormal 
    int totalNgleft; 
    public void caculateViewNgNormal()
    {
        sumViewNgNormal = 0;
        for(NgName ngNormalListOne: ngNormalList)
        {
            try{
                sumViewNgNormal  += Integer.parseInt(ngNormalListOne.getNgNums());  
            }
            catch(Exception e)
            {
                sumViewNgNormal  = 0;
                setViewValidateNgLeft("Invalid input");
                setStatusNgRepair(false);
                break;
            }
       }
       totalNgleft  = 0 ;
       if(sumViewNgNormal != 0)
       {
           totalNgleft  = Integer.parseInt(ngQty)  -  sumViewNgNormal;
           if(totalNgleft != 0)
           {
                 setViewNgleft(totalNgleft);
                 setViewValidateNgLeft("Input Ng Qty equal zero");
                 setStatusNgNormal(false);
           }
           else
           {
                setViewNgleft(totalNgleft);
                setViewValidateNgLeft("");
                closePopup("inputNgQty");
                setStatusNgNormal(true);
                setValidateNgQty("");
                setCheckNgNormalStatus(true);
                showButtonCnfirm();
           }
       }  
    }  
    //==== calculate  ngViewRepairlefts
    int totalNgRepairleft; 
    public void caculdateViewNgRepairleft()
    {
        sumViewNgRepaor = 0;
        for(NgRepairList ngRepairListOne: ngRepairList)
        {
            try{
                sumViewNgRepaor  += Integer.parseInt(ngRepairListOne.getNgRepairNums());  
            }
            catch(Exception e)
            {
                sumViewNgRepaor  = 0;
                setValidateViewNgRepairLeft("Invalid input");
                setStatusNgRepair(false);
                break;
            }
       }
       totalNgRepairleft  = 0 ;
       if(sumViewNgRepaor != 0)
       {
           totalNgRepairleft  = Integer.parseInt(ngRepairQty)  -  sumViewNgRepaor;
           if(totalNgRepairleft != 0)
           {
                 setViewNgRepairleft(totalNgRepairleft);
                 setValidateViewNgRepairLeft("Input Ng Qty equal zero");
                 setStatusNgRepair(false);
           }
           else
           {
                setViewNgRepairleft(totalNgRepairleft);
                setValidateViewNgRepairLeft("");
                closePopup("inputRepairQty");
                setStatusNgRepair(true);
                setValidateNgRepaitQty("");
                setCheckNgRepairStattus(true);
                showButtonCnfirm();
           }
       }  
    }
    
    public int getViewNgleft() {
        return viewNgleft;
    } 
     
    public void setViewNgleft(int viewNgleft) {
        this.viewNgleft = viewNgleft;
    } 
    public int getSumViewNgNormal() {
        return sumViewNgNormal;
    } 
    public void setSumViewNgNormal(int sumViewNgNormal) {
        this.sumViewNgNormal = sumViewNgNormal;
    }

    /**
     * @return the viewValidateNgLeft
     */
    public String getViewValidateNgLeft() {
        return viewValidateNgLeft;
    }

    /**
     * @param viewValidateNgLeft the viewValidateNgLeft to set
     */
    public void setViewValidateNgLeft(String viewValidateNgLeft) {
        this.viewValidateNgLeft = viewValidateNgLeft;
    }

    /**
     * @return the statusNgRepair
     */
    public boolean getStatusNgRepair() {
        return statusNgRepair;
    }

    /**
     * @param statusNgRepair the statusNgRepair to set
     */
    public void setStatusNgRepair(boolean statusNgRepair) {
        this.statusNgRepair = statusNgRepair;
    }

    /**
     * @return the sumViewNgRepaor
     */
    public int getSumViewNgRepaor() {
        return sumViewNgRepaor;
    }

    /**
     * @param sumViewNgRepaor the sumViewNgRepaor to set
     */
    public void setSumViewNgRepaor(int sumViewNgRepaor) {
        this.sumViewNgRepaor = sumViewNgRepaor;
    }

    /**
     * @return the validateViewNgRepairLeft
     */
    public String getValidateViewNgRepairLeft() {
        return validateViewNgRepairLeft;
    }

    /**
     * @param validateViewNgRepairLeft the validateViewNgRepairLeft to set
     */
    public void setValidateViewNgRepairLeft(String validateViewNgRepairLeft) {
        this.validateViewNgRepairLeft = validateViewNgRepairLeft;
    }

    /**
     * @return the viewNgRepairleft
     */
    public int getViewNgRepairleft() {
        return viewNgRepairleft;
    }

    /**
     * @param viewNgRepairleft the viewNgRepairleft to set
     */
    public void setViewNgRepairleft(int viewNgRepairleft) {
        this.viewNgRepairleft = viewNgRepairleft;
    }

    /**
     * @return the statusNgNormal
     */
    public boolean isStatusNgNormal() {
        return statusNgNormal;
    }

    /**
     * @param statusNgNormal the statusNgNormal to set
     */
    public void setStatusNgNormal(boolean statusNgNormal) {
        this.statusNgNormal = statusNgNormal;
    }

    /**
     * @return the inputNgQtyButton
     */
    public boolean getInputNgQtyButton() {
        return inputNgQtyButton;
    }

    /**
     * @param inputNgQtyButton the inputNgQtyButton to set
     */
    public void setInputNgQtyButton(boolean inputNgQtyButton) {
        this.inputNgQtyButton = inputNgQtyButton;
    }

    /**
     * @return the inputNgRepaitButton
     */
    public boolean getInputNgRepaitButton() {
        return inputNgRepaitButton;
    }

    /**
     * @param inputNgRepaitButton the inputNgRepaitButton to set
     */
    public void setInputNgRepaitButton(boolean inputNgRepaitButton) {
        this.inputNgRepaitButton = inputNgRepaitButton;
    }

    /**
     * @return the validateOutput
     */
    public String getValidateOutput() {
        return validateOutput;
    }

    /**
     * @param validateOutput the validateOutput to set
     */
    public void setValidateOutput(String validateOutput) {
        this.validateOutput = validateOutput;
    }

    /**
     * @return the checkNgNormalStatus
     */
    public boolean getCheckNgNormalStatus() {
        return checkNgNormalStatus;
    }

    /**
     * @param checkNgNormalStatus the checkNgNormalStatus to set
     */
    public void setCheckNgNormalStatus(boolean checkNgNormalStatus) {
        this.checkNgNormalStatus = checkNgNormalStatus;
    }

    /**
     * @return the checkNgRepairStattus
     */
    public boolean getCheckNgRepairStattus() {
        return checkNgRepairStattus;
    }

    /**
     * @param checkNgRepairStattus the checkNgRepairStattus to set
     */
    public void setCheckNgRepairStattus(boolean checkNgRepairStattus) {
        this.checkNgRepairStattus = checkNgRepairStattus;
    }

    /**
     * @return the checkOutputStatus
     */
    public boolean getCheckOutputStatus() {
        return checkOutputStatus;
    }

    /**
     * @param checkOutputStatus the checkOutputStatus to set
     */
    public void setCheckOutputStatus(boolean checkOutputStatus) {
        this.checkOutputStatus = checkOutputStatus;
    }

    /**
     * @return the showConfirm
     */
    public boolean isShowConfirm() {
        return showConfirm;
    }

    /**
     * @param showConfirm the showConfirm to set
     */
    public void setShowConfirm(boolean showConfirm) {
        this.showConfirm = showConfirm;
    }

    /**
     * @return the confirmEdit
     */
    public boolean isConfirmEdit() {
        return confirmEdit;
    }

    /**
     * @param confirmEdit the confirmEdit to set
     */
    public void setConfirmEdit(boolean confirmEdit) {
        this.confirmEdit = confirmEdit;
    }
    
    //======== public updateDataAllList  : end 
    int sumRepair;
    int sumOutput;
    int sumNgNormal;
    public void updateDataAllList()
    {
        sumOutput =0;
        System.out.println("========= Update data all list ======");
        //=========== PcsQty table  when status : out
        PcsQty  dataOutPcsqty   =  getPcsQtyJpaController().findPcsQtyCurrentProcessId(currentProcesId); 
        sumOutput  = Integer.parseInt(dataOutPcsqty.getQty())+Integer.parseInt(outputQty);
        dataOutPcsqty.setQty(Integer.toString(sumOutput));
        try{
            getPcsQtyJpaController().edit(dataOutPcsqty); 
            System.out.println("========= Update output success");
        }
        catch(Exception e)
        {
            System.out.println("======== Error  : "+e);
        }  
        //=========== Ngnormal table
        if(!ngQty.equals("0")){
        List<NgNormal>  dataList  = getNgNormalJpaController().findSumNg(currentProcesId);
        for(NgNormal dataListOne : dataList)
        {   
            for(NgName ngNormalListOneUpdate: ngNormalList){
                
                if(dataListOne.getName().equals(ngNormalListOneUpdate.getNgname()))
                {
                     //=====  Add ngNormal 
                     sumNgNormal = Integer.parseInt(dataListOne.getQty())+Integer.parseInt(ngNormalListOneUpdate.getNgNums());
                     dataListOne.setQty(Integer.toString(sumNgNormal));
                     try
                     {
                         getNgNormalJpaController().edit(currentNgNormal);
                     }
                     catch(Exception e)
                     {
                         System.out.println("======== Error not to add NgNormal :"+e);
                     }
                }
                else
                {
                   if(!ngNormalListOneUpdate.getNgNums().equals("0"))
                   {
                       //==== Create new ngNormal row  
                       String uuidNgNormal = UUID.randomUUID().toString();
                       getCurrentNgNormal().setCurrentProcessId(currentProcesId);
                       getCurrentNgNormal().setIdNg(uuidNgNormal);
                       getCurrentNgNormal().setQty(ngNormalListOneUpdate.getNgNums());
                       getCurrentNgNormal().setName(ngNormalListOneUpdate.getNgname()); 
                       try{
                           getNgNormalJpaController().create(dataListOne);
                           System.out.println("=====Insert new ngNormal Success");
                           currentNgNormal  = new NgNormal();
                       }
                       catch(Exception e)
                       {
                           System.out.println("==== Error  to add ngNormal  "+e);
                       }
                   }
                  
                } 
            }        
         }
         
        }
        //=========== NgRepair table
       if(!ngRepairQty.equals("0")){
        List<NgRepair>  dataListNgRepaira   =  getNgRepairJpaController().findSumNgRepair(currentProcesId);
        
        for(NgRepair ngRepairListOne : dataListNgRepaira)
        {
            for(NgRepairList ngRepairListOneNew : ngRepairList)
            {   sumNgRepair = 0;
                if(ngRepairListOne.getName().equals(ngRepairListOneNew.getNgRepairname()))
                { 
                    //sumNgRepair = Integer.parseInt(ngRepairListOne.getQty())-Integer.parseInt(ngRepairListOneNew.getNgRepairNums());
                    ngRepairListOne.setQty(ngRepairListOneNew.getNgRepairNums());
                    try
                    {
                        getNgRepairJpaController().edit(ngRepairListOne);
                    }
                    catch(Exception e)
                    {
                        System.out.println("===== Error not update ng repair");
                    }
                }
                else
                {
                   if(!ngRepairListOneNew.getNgRepairNums().equals("0"))
                   {
                        String uuidNgRepair = UUID.randomUUID().toString();
                        getCurrentNgRepair().setCurrentProcessId(currentProcesId);
                        getCurrentNgRepair().setIdNg(uuidNgRepair);
                        getCurrentNgRepair().setName(ngRepairListOneNew.getNgRepairname());
                        getCurrentNgRepair().setQty(ngRepairListOneNew.getNgRepairNums());
                        try
                        {
                            getNgRepairJpaController().create(ngRepairListOne);
                            currentNgRepair  = new NgRepair();
                        }
                        catch(Exception e)
                        {
                            System.out.println("===== Not to create ng reapair");
                        } 
                   }
                
                }
            }
        
        }
        //=========
       }
       else
       {
        for(NgRepair ngRepairListOne : dataListNgRepair)
        {
            ngRepairListOne.setQty("0");
            try
            {
                getNgRepairJpaController().edit(ngRepairListOne);
            }
            catch(Exception e)
            {
            }
        }
       }  
       getBarcodeAuto().showListDetailProc(getBarcodeAuto().lotControlId); //*** Display list data ngrepair
    }
    //====pcsQty
    public  PcsQty getCurrentPcsqty(){ 
        if(currentPcsqty == null)
        {
            currentPcsqty  = new PcsQty();
        }
        return currentPcsqty;
    }
    
    //==== NgNormal
     public  NgNormal getCurrentNgNormal(){ 
        if(currentNgNormal == null)
        {
            currentNgNormal  = new NgNormal();
        }
        return currentNgNormal;
    }
    
    //==== NgRepair 
     public  NgRepair getCurrentNgRepair(){ 
        if(currentNgRepair == null)
        {
            currentNgRepair  = new NgRepair();
        }
        return currentNgRepair;
    }

    /**
     * @return the editNgRepairButton
     */
    public boolean getEditNgRepairButton() {
        return editNgRepairButton;
    }

    /**
     * @param editNgRepairButton the editNgRepairButton to set
     */
    public void setEditNgRepairButton(boolean editNgRepairButton) {
        this.editNgRepairButton = editNgRepairButton;
    }
     
}
