package com.component.barcodeauto;

import com.appCinfigpage.bean.fc;
import com.appCinfigpage.bean.userLogin;
import com.master.Data.MasterDefaultData;
import com.tct.data.*;
import com.tct.data.jpa.*;
import java.util.List;
import javax.persistence.Persistence;
import java.util.ArrayList;
import com.shareData.ShareDataOfSession;
import com.tct.data.ModelPool;
import com.time.timeCurrent;
import java.util.*;
import com.tct.data.McDown;
import com.tct.data.McMove;
import com.tct.data.Hold;
import java.text.DecimalFormat;
import javax.faces.context.FacesContext;

public class BarcodeAuto {

    public String textSeach = "";
    public String status = null;
    public String nameProcess;
    public String openProcessButtonShow = "open_process_button_disable";
    public String closeProcessButtonShow = "close_process_button_disable";
    public String mcDownTimeButtonShow = "mc_downtime_button_disable";
    public String holdTimeButtonShow = "hold_process_button_disable";
    public String stopTimeButtonShow = "stop_downtime_button_disable";
    public String moveProcessButtonShow = "move_mc_button_disable";
    public String qaProcessButtonShow = "qa_button_disable";
    public List<ProcList> processListData = null;
    private int countProcessdStatus = 0;
    public boolean openProcessButton = true;
    public boolean closeProcessButton = true;
    public boolean mcDownTimeButton = true;
    public boolean holdTimeButton = true;
    public boolean stopTimeButton = true;
    public boolean moveProcessButton = true;
    public boolean qaProcessButton = false;
    private String qaBarcodeLink = "";
    LotControlJpaController lotjpa = null;
    CurrentProcessJpaController lotjpa2 = null;
    PcsQtyJpaController lotjpa3 = null;
    Md2pcJpaController md2pc = null;
    DetailsJpaController details = null;
    MainDataJpaController mainController = null;
    NgNormalJpaController ngNormalJpaController = null;
    McJpaController mcJpaController = null;
    NgRepairJpaController ngRepairJpaController = null;
    QaSamplingJpaController qaSamplingJpaController = null;
    private QaSampling currentQaSampling;
    private HoldJpaController holdJpaController = null;
    private McDownJpaController mcDownJpaController = null;
    private McMoveJpaController mcMoveJpaController = null;
    private DetailSpecialJpaController detailSpecialJpaController = null;
    private ModelPoolJpaController modelPoolJpaController = null;
    Details detailsId;
    public String md2pcId = "";
    public ProcessPool procPoolId = null;
    public LotControl lotControlId = null;
    public String modelPoolId = "";
    public String lotNumber = "";
    public String lotControlId2;
    public int outputQty;
    public ProcessPool idProc = null;
    public MainData mainDataId;
    public String sqProcess;
    Md2pc md2pcData;
    //show detail worksheet
    private MasterDefaultData currentMasterDefaultData;
    private String nameWorkSheet;
    //--funtion  to show detail
    private String modelDetials = "";
    public String mainDataIdinLotControl = "";
    private String inputQty = "0";
    private String inputQtyQa;
    public CurrentProcess idCurrentFinished = null;
    public String textProcess = "";
    
    private ModelPoolJpaController getModelPoolJpaController() {
        if (modelPoolJpaController == null) {
            modelPoolJpaController = new ModelPoolJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return modelPoolJpaController;
    }

    private DetailSpecialJpaController getDetailSpecialJpaController() {
        if (detailSpecialJpaController == null) {
            detailSpecialJpaController = new DetailSpecialJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailSpecialJpaController;
    }

    private HoldJpaController getHoldJpaController() {
        if (holdJpaController == null) {
            holdJpaController = new HoldJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return holdJpaController;
    }

    private McDownJpaController getMcDownJpaController() {
        if (mcDownJpaController == null) {
            mcDownJpaController = new McDownJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mcDownJpaController;
    }

    private McMoveJpaController getMcMoveJpaController() {
        if (mcMoveJpaController == null) {
            mcMoveJpaController = new McMoveJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mcMoveJpaController;
    }

    private LotControlJpaController getJpaController() {
        if (lotjpa == null) {
            lotjpa = new LotControlJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotjpa;
    }

    private CurrentProcessJpaController getJpaController2() {
        if (lotjpa2 == null) {
            lotjpa2 = new CurrentProcessJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotjpa2;
    }

    private PcsQtyJpaController getJpaController3() {
        if (lotjpa3 == null) {
            lotjpa3 = new PcsQtyJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return lotjpa3;
    }

    private Md2pcJpaController getMd2pcJpaController() {
        if (md2pc == null) {
            md2pc = new Md2pcJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return md2pc;
    }

    private MainDataJpaController getMainJpaController() {
        if (mainController == null) {
            mainController = new MainDataJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mainController;
    }
    private NgNormalJpaController getNgNormalJpaController() {
        if (ngNormalJpaController == null) {
            ngNormalJpaController = new NgNormalJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return ngNormalJpaController;
    }

    private McJpaController getMcJpaController() {
        if (mcJpaController == null) {
            mcJpaController = new McJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return mcJpaController;
    }

    //=========Ng repair controller
    private NgRepairJpaController getNgRepairJpaController() {
        if (ngRepairJpaController == null) {
            ngRepairJpaController = new NgRepairJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));

        }
        return ngRepairJpaController;
    }

    private QaSamplingJpaController getQaSamplingJpaController() {
        if (qaSamplingJpaController == null) {
            qaSamplingJpaController = new QaSamplingJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return qaSamplingJpaController;
    }
    public String getTextSeach() {
        return textSeach;
    }
    

    public String getTextProcess() {
        return textProcess;
    }

    public void setTextProcess(String textProcess) {
        this.textProcess = textProcess;
    }
    int count = 0;

    public ShareDataOfSession getShareData() {
        FacesContext context = FacesContext.getCurrentInstance();
        return (ShareDataOfSession) context.getExternalContext().getSessionMap().get("sharedata");
    }

    public void setTextSeach(String textSeach) {
        count++;
        textProcess = textSeach;
        this.textSeach = textSeach;

        System.out.println("===== count" + count);
        if (count == 2) {

            count = 0;
            findProcessAndStatus();  //======= find barcode
            ShareDataOfSession objShare = getShareData();
            objShare.openProcessButton = openProcessButton;
            objShare.openProcessButtonShow = openProcessButtonShow;
            objShare.status = status;
            objShare.idProc = idProc;
            objShare.lotControlId = lotControlId;
            objShare.lotNumber = lotNumber;
            objShare.md2pcId = md2pcId;
            objShare.nameProcess = nameProcess;
            objShare.procPoolId = procPoolId;
            objShare.sqProcess = sqProcess;
            objShare.closeProcessButton = closeProcessButton;
            objShare.closeProcessButtonShow = closeProcessButtonShow;
            objShare.mcDownTimeButton = mcDownTimeButton;
            objShare.mcDownTimeButtonShow = mcDownTimeButtonShow;
            objShare.holdTimeButton = holdTimeButton;
            objShare.holdTimeButtonShow = holdTimeButtonShow;
            objShare.mainDataId = mainDataId;
            objShare.outputQty = outputQty;
            objShare.stopTimeButton = stopTimeButton;
            objShare.moveProcessButton = moveProcessButton;
            objShare.stopTimeButtonShow = stopTimeButtonShow;
            objShare.moveProcessButtonShow = moveProcessButtonShow;
            objShare.textProcess = textProcess;
            this.textSeach = "";
        }
    }

    public void findProcessAndStatus() {
        LotControl lotData = getJpaController().findProcessNameAndStatus(textProcess);
        nameWorkSheet = null;
        currentMasterDefaultData = new MasterDefaultData(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
        if (lotData == null) {
            openProcessButton = true;
            openProcessButtonShow = "open_process_button_disable";
            status = "";
            idProc = null;
            lotControlId = null;
            lotNumber = null;
            md2pcId = null;
            nameProcess = "Not found input worksheet code ?";
            procPoolId = null;
            sqProcess = null;
            closeProcessButton = true;
            closeProcessButtonShow = "close_process_button_disable";
            mcDownTimeButton = true;
            mcDownTimeButtonShow = "mc_downtime_button_disable";
            holdTimeButton = true;
            holdTimeButtonShow = "hold_process_button_disable";
            mainDataId = null;
            outputQty = 0;
            stopTimeButton = true;
            moveProcessButton = true;
            stopTimeButtonShow = "stop_downtime_button_disable";
            moveProcessButtonShow = "move_mc_button_disable";
            textProcess = null;
            setCountProcessdStatus(0);
            processListData = null;   //============ Null  : Not show process status
        } else if (lotData.getDeleted() == 1) {
            openProcessButton = true;
            openProcessButtonShow = "open_process_button_disable";
            status = "";
            idProc = null;
            lotControlId = null;
            lotNumber = null;
            md2pcId = null;
            nameProcess = "This worksheet is deleted ";
            procPoolId = null;
            sqProcess = null;
            closeProcessButton = true;
            closeProcessButtonShow = "close_process_button_disable";
            mcDownTimeButton = true;
            mcDownTimeButtonShow = "mc_downtime_button_disable";
            holdTimeButton = true;
            holdTimeButtonShow = "hold_process_button_disable";
            mainDataId = null;
            outputQty = 0;
            stopTimeButton = true;
            moveProcessButton = true;
            stopTimeButtonShow = "stop_downtime_button_disable";
            moveProcessButtonShow = "move_mc_button_disable";
            textProcess = null;
            setCountProcessdStatus(0);
            processListData = null;
        } else {
            System.out.println("=================== Locontrol id" + lotControlId);
            lotControlId = lotData;
            lotNumber = lotData.getLotNumber();
            modelPoolId = lotData.getModelPoolId().getId();
            md2pcId = lotData.getMd2pcIdModel();
            detailsId = lotData.getDetailsIdDetail();
            mainDataIdinLotControl = lotData.getMainDataId();
            findMaindata(lotControlId, modelPoolId, lotData.getModelPoolId(), lotData.getMainDataId());
            showDetailProc(detailsId, lotData.getLotNumber());
            showListDetailProc(lotData);
        }
    }

    public void findMaindata(LotControl lotControlId, String modelPoolId, ModelPool modelPool, String mainDataIdinLotControl) {
        System.out.println("================================ Find IdLocotrol in  Main Data table ==============================");
        List<MainData> dataFind = getJpaController().findMain(lotControlId, mainDataIdinLotControl);
        qaProcessButtonShow = "qa_button_disable";
        qaProcessButton = true;
        if (dataFind.isEmpty() == true) {
            sqProcess = "1";
            System.out.println("++++++++++++++++++++++++++++++++ Not Find  ++++++++++++++++++++++++++++++++++++++++++++++++++");
            inputQty = "0";
            setStatus("Wait of open");
            setNameProcess("None");
            setOpenProcessButton(false);
            setOpenProcessButtonShow("open_process_button");
            setMcDownTimeButton(true);
            setMcDownTimeButtonShow("mc_downtime_button_disable");
            setStopTimeButton(true);
            setStopTimeButtonShow("stop_downtime_button_disable");
            setMoveProcessButton(true);
            setMoveProcessButtonShow("move_mc_button_disable");
            setHoldTimeButton(true);
            setHoldTimeButtonShow("hold_process_button_disable");
            setCloseProcessButton(true);
            setCloseProcessButtonShow("close_process_button_disable");
            idProc = null;
            procPoolId = null;
            openProcessButtonShow = "open_process_button";
            String idWorksheet   = lotControlId.getDetailsIdDetail().getDetailName();
            findModelForMainData(modelPoolId,idWorksheet);
        } else {

            System.out.println("======== Find is  true ======== : CurrentProcess  Id"+ dataFind.get(0).getCurrentNumber()); 
            CurrentProcess currentProperties = getJpaController2().findCurrentProcess(dataFind.get(0).getCurrentNumber());
            setStatus(currentProperties.getStatus().toString());
            setNameProcess(dataFind.get(0).getIdProc().getProcName());

            if (currentProperties.getStatus().equals("Open")) {
                System.out.println("====================== Open ==================================");
                setOpenProcessButton(true);
                setOpenProcessButtonShow("open_process_button_disable");
                if (currentProperties.getMcId() != null) {
                    System.out.println("====== Check mc" + currentProperties.getMcId());
                    setMcDownTimeButton(false);
                    setMcDownTimeButtonShow("mc_downtime_button");
                } else {
                    setMcDownTimeButton(true);
                    setMcDownTimeButtonShow("mc_downtime_button_disable");
                }
                setStopTimeButton(true);
                setStopTimeButtonShow("stop_downtime_button_disable");
                setMoveProcessButton(true);
                setMoveProcessButtonShow("move_mc_button_disable");
                setHoldTimeButton(false);
                setHoldTimeButtonShow("hold_process_button");
                setCloseProcessButton(false);
                setCloseProcessButtonShow("close_process_button");
                System.out.println("currentProperties :" + currentProperties.getId());
                PcsQty objData = getJpaController3().findPcsQtyCurrentProcessId(currentProperties);
                setOutputQty(Integer.parseInt(objData.getQty()));
                idProc = dataFind.get(0).getIdProc();
                fc.print("================ idProc : " + idProc);
                mainDataId = currentProperties.getMainDataId();
                nameProcess = dataFind.get(0).getIdProc().getProcName();
                fc.print("nameProcess : " + nameProcess);
            } else if (currentProperties.getStatus().equals("Close")) {
                System.out.println("====================== Close ==================================");
                Md2pc objDataNextProcess = getMd2pcJpaController().findSqProcess(dataFind.get(0).getIdProc(), modelPool);
                //****** find next type only.
                String idworkSheet  = showListTypeOnly(lotControlId);
                Md2pc objDataCheckNewprocess = sqNewProcess(modelPool,idworkSheet,objDataNextProcess); 
                if (objDataCheckNewprocess == null) {
                    System.out.println("========================= This process is  Finished ========================");
                    setStatus("Finished");
                    qaProcessButtonShow = "qa_button";
                    qaProcessButton = false;
                    idCurrentFinished = currentProperties;
                    //idCurrentFinished  =  currentProperties;
                    currentProperties.setStatus("Finished");
                    try {
                        getJpaController2().edit(currentProperties);
                    } catch (Exception e) {
                        System.out.println("========== Not Change status Current process " + e);
                    }
                    PcsQty outPcsqty = getJpaController3().findPcsQtyCurrentProcessId(currentProperties);
                    outPcsqty.setWipQty(outPcsqty.getQty());
                    try {
                        getJpaController3().edit(outPcsqty);
                    } catch (Exception e) {
                        System.out.println("========== Not to add wip " + e);
                    }
                    //======= Update wip and qty_out in lotcontrol 
                    lotControlId.setWipQty(outputQty);
                    lotControlId.setQtyOutput(outputQty);
                    try {
                        getJpaController().edit(lotControlId);
                    } catch (Exception e) {
                        System.out.println("======= Error not to update wip and qtyoutput" + e);
                    }
                    //======================== Set menu
                    setOpenProcessButton(true);
                    setOpenProcessButtonShow("open_process_button_disable");
                    setMcDownTimeButton(true);
                    setMcDownTimeButtonShow("mc_downtime_button_disable");
                    setStopTimeButton(true);
                    setStopTimeButtonShow("stop_downtime_button_disable");
                    setMoveProcessButton(true);
                    setMoveProcessButtonShow("move_mc_button_disable");
                    setHoldTimeButton(true);
                    setHoldTimeButtonShow("hold_process_button_disable");
                    setCloseProcessButton(true);
                    setCloseProcessButtonShow("close_process_button_disable");
                    setOpenProcessButtonShow("close_process_button_disable");
                } else {
                    //idCurrentFinished = null;
                    PcsQty objPcsQty = getJpaController3().findByIdCurrentProcessAndStatusClose(currentProperties);
                    String outString = objPcsQty.getQty();
                    fc.print("=========== Input new process ======= : " + outString);
                    inputQty = outString;
                    sqProcess = Integer.toString(objDataCheckNewprocess.getSqProcess());
                    System.out.println("======================== New sqprocess      :" + sqProcess);
                    idProc = objDataCheckNewprocess.getProcessPoolIdProc();
                    //============= Set menu
                    setOpenProcessButton(false);
                    setOpenProcessButtonShow("open_process_button");
                    setMcDownTimeButton(true);
                    setMcDownTimeButtonShow("mc_downtime_button_disable");
                    setStopTimeButton(true);
                    setStopTimeButtonShow("stop_downtime_button_disable");
                    setMoveProcessButton(true);
                    setMoveProcessButtonShow("move_mc_button_disable");
                    setHoldTimeButton(true);
                    setHoldTimeButtonShow("hold_process_button_disable");
                    setCloseProcessButton(true);
                    setCloseProcessButtonShow("close_process_button_disable");
                    System.out.println("----load new sqProcesss ---------");
                    setStatus("Wait of open");
                    System.out.println("Model id :" + objDataCheckNewprocess.getModelPoolId() + "process id :" + objDataCheckNewprocess.getProcessPoolIdProc());
                    setNameProcess(objDataCheckNewprocess.getProcessPoolIdProc().getProcName());

                    md2pcId = objDataCheckNewprocess.getMd2pcId();
                    procPoolId = objDataCheckNewprocess.getProcessPoolIdProc();
                    System.out.println("=============== procPoolId" + md2pcId);
                    System.out.println("=============== procPoolId" + procPoolId);
                }

            } else if (currentProperties.getStatus().equals("Downtime")) {

                //================== Set menu
                setOpenProcessButton(true);
                setOpenProcessButtonShow("open_process_button_disable");
                setMcDownTimeButton(true);
                setMcDownTimeButtonShow("mc_downtime_button_disable");
                setStopTimeButton(false);
                setStopTimeButtonShow("stop_downtime_button");
                setMoveProcessButton(false);
                setMoveProcessButtonShow("move_mc_button");
                setHoldTimeButton(true);
                setHoldTimeButtonShow("hold_process_button_disable");
                setCloseProcessButton(true);
                setCloseProcessButtonShow("close_process_button_disable");
                mainDataId = currentProperties.getMainDataId();


            } else if (currentProperties.getStatus().equals("Holdtime")) {

                //================= Set menu
                setOpenProcessButton(true);
                setOpenProcessButtonShow("open_process_button_disable");
                setMcDownTimeButton(true);
                setMcDownTimeButtonShow("mc_downtime_button_disable");
                setStopTimeButton(true);
                setStopTimeButtonShow("stop_downtime_button_disable");
                setMoveProcessButton(true);
                setMoveProcessButtonShow("move_mc_button_disable");
                setHoldTimeButton(false);
                setHoldTimeButtonShow("hold_process_button");
                setCloseProcessButton(true);
                setCloseProcessButtonShow("close_process_button_disable");
                mainDataId = currentProperties.getMainDataId();
                nameProcess = dataFind.get(0).getIdProc().getProcName();

            } else if (currentProperties.getStatus().equals("Finished")) {
                //================= Set menu
                qaProcessButton = false;
                qaProcessButtonShow = "qa_button";
                idCurrentFinished = currentProperties;
                currentProperties.setStatus("finish");
                setOpenProcessButton(true);
                setOpenProcessButtonShow("open_process_button_disable");
                setMcDownTimeButton(true);
                setMcDownTimeButtonShow("mc_downtime_button_disable");
                setStopTimeButton(true);
                setStopTimeButtonShow("stop_downtime_button_disable");
                setMoveProcessButton(true);
                setMoveProcessButtonShow("move_mc_button_disable");
                setHoldTimeButton(true);
                setHoldTimeButtonShow("hold_process_button_disable");
                setCloseProcessButton(true);
                setCloseProcessButtonShow("close_process_button_disable");
            }
        }
    } 
    //**** function to load new process only type.
    public Md2pc sqNewProcess(ModelPool modelPool, String idType,Md2pc idMd2pc)
    {
        Md2pc newprocess = null;
        int sq_now  = idMd2pc.getSqProcess();
        for(Md2pc objList :  getMd2pcJpaController().findBymodelPoolId(modelPool))
            {
                if(objList.getProcessPoolIdProc().getType().equals(idType) && objList.getSqProcess() > sq_now)
                {
                    newprocess  = objList;
                    break;
                }
            }
        return newprocess;
    }
    
    //**** function to find first process only type.
    public String findOnlyTypeProcessOnly(ModelPool idModelPool,String idNameWorkSheet)
    {
        String indexFirst = null;
         
        for(Md2pc objList :  getMd2pcJpaController().findBymodelPoolId(idModelPool))
        {
            if(objList.getProcessPoolIdProc().getType().equals(idNameWorkSheet))
            {
                indexFirst  = objList.getMd2pcId();
                break;
            }
        }
        return indexFirst;
    } 
    public void findModelForMainData(String modelPoolId,String idNameWorkSheet) {
       
        ModelPool dataModelPool = getJpaController().findModel(modelPoolId); //check model in lotcontrol
        md2pcId = findOnlyTypeProcessOnly(dataModelPool,idNameWorkSheet);
        System.out.println("find load model  =============" + modelPoolId +"type worksheet ***** :"+"idNameWorkSheet+Md2pc ID :" + md2pcId);
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getNameProcess() {
        return nameProcess;
    }
    public void setNameProcess(String nameProcess) {
        this.nameProcess = nameProcess;
    }
    public boolean isOpenProcessButton() {
        return openProcessButton;
    }
    public void setOpenProcessButton(boolean openProcessButton) {
        this.openProcessButton = openProcessButton;
    }
    public boolean isCloseProcessButton() {
        return closeProcessButton;
    }
    public void setCloseProcessButton(boolean closeProcessButton) {
        this.closeProcessButton = closeProcessButton;
    }
    public boolean isMcDownTimeButton() {
        return mcDownTimeButton;
    }
    public void setMcDownTimeButton(boolean mcDownTimeButton) {
        this.mcDownTimeButton = mcDownTimeButton;
    }
    public boolean isHoldTimeButton() {
        return holdTimeButton;
    }
    public void setHoldTimeButton(boolean holdTimeButton) {
        this.holdTimeButton = holdTimeButton;
    }
    public boolean isStopTimeButton() {
        return stopTimeButton;
    }
    public void setStopTimeButton(boolean stopTimeButton) {
        this.stopTimeButton = stopTimeButton;
    }
    public boolean isMoveProcessButton() {
        return moveProcessButton;
    }
    public void setMoveProcessButton(boolean moveProcessButton) {
        this.moveProcessButton = moveProcessButton;
    }
    public int getOutputQty() {
        return outputQty;
    }
    public void setOutputQty(int outputQty) {
        this.outputQty = outputQty;
    }

    public void showDetailProc(Details idDetials, String lotControl) {
        modelDetials = idDetials.getModel();
        nameWorkSheet = getDetailSpecialJpaController().findDetailSpecial(idDetials.getDetailName()).getName();
        fc.print("name worksheet type :" + nameWorkSheet);
        currentMasterDefaultData.setAssyDate(idDetials.getAssyDate());
        currentMasterDefaultData.setIndex(idDetials.getIdDetail());
        currentMasterDefaultData.setInjectDate(idDetials.getInjectionDate());
        currentMasterDefaultData.setInvBobbin(idDetials.getInvBobin());
        currentMasterDefaultData.setInvBronze(idDetials.getInvBroze());
        currentMasterDefaultData.setLotControlQty(idDetials.getLotcontrol());
        ModelPool objModelPool = getModelPoolJpaController().findModelPool(idDetials.getModel());
        String model = objModelPool.getModelGroup() + "-" + objModelPool.getModelSeries() + "-" + objModelPool.getModelName();
        currentMasterDefaultData.setModel(model);
        currentMasterDefaultData.setOrderNo(idDetials.getOrderNumber());
        currentMasterDefaultData.setOrderType(idDetials.getOrderType());
        currentMasterDefaultData.setPlant(idDetials.getPlant());
        currentMasterDefaultData.setPlantingDate(idDetials.getPlatingDate());
        currentMasterDefaultData.setProductCode(idDetials.getPdCode());
        currentMasterDefaultData.setProductCodeName(idDetials.getPdCodeName());
        currentMasterDefaultData.setProductQty(idDetials.getPdQty());
        currentMasterDefaultData.setProductType(idDetials.getPdCodeType());
        currentMasterDefaultData.setRemarks(idDetials.getRemark());
        currentMasterDefaultData.setSeriee(idDetials.getModelseries());
    }

    public int getCountProcessdStatus() {
        return countProcessdStatus;
    }

    public void setCountProcessdStatus(int countProcessdStatus) {
        this.countProcessdStatus = countProcessdStatus;
    } 
    public String  showListTypeOnly(LotControl lotcontrolId)
    {
        return lotcontrolId.getDetailsIdDetail().getDetailName();
    }
    //*** prepare datalist 
    public void showListDetailProc(LotControl lotcontrolId) {
        List<Md2pc> groupListProcess = getMd2pcJpaController().findBymodelPoolId(lotcontrolId.getModelPoolId());
        setProcessListData(new ArrayList<ProcList>());
        int i = 0;
        for (Md2pc lotconlistProcLotControl : groupListProcess) {
            if (showListTypeOnly(lotcontrolId).equals(lotconlistProcLotControl.getProcessPoolIdProc().getType())) {
                String start = "";
                String finish = "";
                String shitf = "";
                String inputQty = "";
                String ouPutQty = "";
                String ngQty = "";
                String mcName = ""; 
                String cssCurrentStatusProcess = "process_td";
                String ngRepair = "";
                String percentNgNormal = "";
                String percentNgRepair = "";
                int sumNg = 0;
                int sumNgRepair = 0;
                String tactTime = "";
                String yield = "";
                CurrentProcess index = null;
                boolean checkFinished = false;
                double yieldNum = 0;
                long finishedTime = 0;
                long startTime = 0;
                long sumDownTime = 0;
                long sumMoveTime = 0;
                long sumHold = 0;
                long timestamp = 0;

                System.out.println("============================== Process List  : " + i + "    ==========================================");
                MainData dataMainList = getMainJpaController().findMainDataByProcessAndLotControl(lotconlistProcLotControl.getProcessPoolIdProc(), lotControlId);
                if (dataMainList != null) {
                    // System.out.println("=========== Main id  "+dataMainList.getId()); 
                    List<CurrentProcess> dataCurrent = getJpaController2().findCurrentStatus(dataMainList);
                    for (CurrentProcess dataList : dataCurrent) {
                        //fc.print("================== Status====================== : " +dataList.getStatus() +"date : "+dataList.getTime().getTime()); 
                        if (dataList.getStatus().equals("Close") || dataList.getStatus().equals("Finished")) {
                            finishedTime = dataList.getTimeCurrent().getTime();
                            // fc.print("======== Close ====="+finishedTime);
                        }
                        if (dataList.getStatus().equals("Open") && dataList.getFirstOpen() == 1) {
                            PcsQty dataInputPcsQty = getJpaController3().findPcsQtyCurrentProcessId(dataList);
                            inputQty = dataInputPcsQty.getQty();
                            startTime = dataList.getTimeCurrent().getTime();
                            start = dataList.getTimeCurrent().toString();
                            //fc.print("========== First open ======="+startTime); 
                        }
                        if (dataList.getStatus().equals("Open") && !(dataList.getFirstOpen() == 1)) {
                            // fc.print("=======Open time  ==="+dataList.getTime() +"id ref"); 
                            McDown objMcdownStop = getMcDownJpaController().findCheckDowntime(dataList);
                            if (objMcdownStop != null) {
                                CurrentProcess objStart = getJpaController2().findCurrentProcess(objMcdownStop.getRefDownTimeStart());
                                if (objStart != null) {
                                    long sumDownTimein = objMcdownStop.getCurrentProcessId().getTimeCurrent().getTime() - objStart.getTimeCurrent().getTime();
                                    sumDownTime += sumDownTimein;

                                    fc.print("===================== sum downtime====== : " + sumDownTime + " sum downtime in :" + sumDownTimein);
                                }
                            }
                            McMove objMoveStop = getMcMoveJpaController().fingCheckMcMove(dataList);
                            if (objMoveStop != null) {
                                CurrentProcess moveStart = getJpaController2().findCurrentProcess(objMoveStop.getRefMoveIdCurrent());
                                if (moveStart != null) {
                                    long sumMoveTimein = objMoveStop.getCurrentProcessId().getTimeCurrent().getTime() - moveStart.getTimeCurrent().getTime();
                                    sumMoveTime += sumMoveTimein;
                                    fc.print("=================== Sum move ========== : " + sumMoveTime + " sum move in :" + sumMoveTimein);
                                }
                            }
                            Hold objHold = getHoldJpaController().findCheckHold(dataList);
                            if (objHold != null) {
                                CurrentProcess holdStart = getJpaController2().findCurrentProcess(objHold.getRefIdHoldStart());
                                if (holdStart != null) {
                                    long sumHoldin = objHold.getCurrentProcessId().getTimeCurrent().getTime() - holdStart.getTimeCurrent().getTime();
                                    sumHold += sumHoldin;
                                    fc.print("================= Sum hold ========== : " + sumHold + " sum hold in :" + sumHoldin);
                                }
                            }
                        }
                        if (dataList.getStatus().equals("Open") && dataList.getCurrentProc().equals("1")) {
                            shitf = dataList.getShift();
                            try {
                                mcName = dataList.getMcId().getMcName();
                            } catch (Exception e) {
                                mcName = "";
                            }
                        }
                        if (dataList.getStatus().equals("Close") || dataList.getStatus().equals("Finished")) {
                            index = dataList;
                            PcsQty dataInputPcsQty = getJpaController3().findPcsQtyCurrentProcessId(dataList);
                            finish = dataList.getTimeCurrent().toString();
                            ouPutQty = dataInputPcsQty.getQty();

                            //Caculate tact time
                            List<NgNormal> ngNormal = getNgNormalJpaController().findSumNg(dataList);
                            for (NgNormal ngQtyfind : ngNormal) {
                                //System.out.println("========== ng     : "+ngQtyfind.getQty());
                                sumNg += Integer.parseInt(ngQtyfind.getQty()); 
                            }

                            List<NgRepair> ngRepairData = getNgRepairJpaController().findSumNgRepair(dataList);

                            for (NgRepair dataListNgRepair : ngRepairData) {
                                sumNgRepair += Integer.parseInt(dataListNgRepair.getQty());

                            }
                            ngQty = Integer.toString(sumNg);
                            ngRepair = Integer.toString(sumNgRepair);

                        } 
                        //======= cssCurrent status 
                        if (nameProcess.equals(lotconlistProcLotControl.getProcessPoolIdProc().getProcName())) {
                            //fc.print("=========== Status process =========== : "+dataList.getStatus());
                            if (dataList.getStatus().equals("Finished")) {
                                cssCurrentStatusProcess = "process_td_close";
                                checkFinished = true;
                            } else {
                                if(status.equals("Finished")|| status.equals("Close"))
                                {
                                    cssCurrentStatusProcess = "process_td_close"; 
                                }
                                else
                                {
                                    cssCurrentStatusProcess = "process_td_now";
                                }      
                            }
                        } else {
                            cssCurrentStatusProcess = "process_td_close";
                        }
                    }
                }
                if (!(inputQty.equals("0")) || sumNg != 0 || sumNgRepair != 0) {
                    try {
                        double inputNumQty = Double.parseDouble(inputQty);
                        double percentNgNormalNums1 = ((double) sumNg * 100) / inputNumQty; 
                        percentNgNormal = new DecimalFormat("#,###.##").format(percentNgNormalNums1)+" % ";
                                
                        yieldNum = 100 - percentNgNormalNums1; 
                        double percentNgRepairNums = (sumNgRepair * 100) / inputNumQty; 
                        percentNgRepair = new DecimalFormat("#,###.##").format(percentNgRepairNums)+" % ";

                        System.out.println("====    Percent ng Normal  :" + percentNgNormal + " ==== Percent ng repair :" + percentNgRepair);


                    } catch (Exception e) {
                        System.out.println(e);
                    }
                } else {
                    percentNgNormal = "0.00 %";
                    percentNgRepair = "0.00 %";
                    yieldNum = 100; 
                }
                if (finishedTime != 0) {
                    timestamp = finishedTime - startTime - (sumDownTime + sumHold + sumMoveTime);
                    fc.print("======== timestamp ===== : " + timestamp);

                    long diffSeconds = timestamp / 1000;
                    long diffMinutes = timestamp / (60 * 1000);
                    long diffHours = timestamp / (60 * 60 * 1000);
                    long diffDays = timestamp / (24 * 60 * 60 * 1000);
                    tactTime = diffDays + " D, " + diffHours + ":" + diffMinutes;
                } else {
                    tactTime = "";
                }
                yield = new DecimalFormat("#,###.##").format(yieldNum) + " %";
                if (checkFinished) {
                    cssCurrentStatusProcess = "process_td_close";
                }
                System.out.println("==================================================================================================");
                fc.print("Sum ng **** : "+sumNg);
                getProcessListData().add(new ProcList(lotconlistProcLotControl.getSqProcess(), lotconlistProcLotControl.getProcessPoolIdProc().getProcName(), shitf, start, finish, inputQty, ouPutQty, mcName, ngQty, index, cssCurrentStatusProcess, ngRepair, percentNgNormal, percentNgRepair, tactTime, yield));
                i++;
            }
            setCountProcessdStatus(i);
        }
    }
    private boolean showQaPopup = false;
    private String validateQaInput = "";
    private String qaOutPutQty = "0";

    public void popupQa(String typeQa) {
        if (!qaProcessButton) {
            if (typeQa.equals("open")) {
                showQaPopup = true;
                PcsQty objPcsQty = getJpaController3().findByIdCurrentProcessAndStatusClose(idCurrentFinished);
                fc.print("WIP " + objPcsQty.getWipQty() + " QTY : " + objPcsQty.getQty());
                qaOutPutQty = objPcsQty.getWipQty();
                qaBarcodeLink = textProcess + ".png";
            } else {
                showQaPopup = false;
                resetDataQa();
            }
        }

    }

    public void saveQaSample() {
        int inputQty = 0;
        try {
            inputQty = Integer.parseInt(inputQtyQa);

            showQaPopup = false;
            if (inputQty <= Integer.parseInt(qaOutPutQty)) {
                validateQaInput = "";
                updatePcsQty();
                showQaPopup = false;
            } else {
                validateQaInput = " Check input qty";
                showQaPopup = true;
            }
            inputQtyQa = "0";
        } catch (Exception e) {
            fc.print("===== Input invalid ===== : " + e);
            validateQaInput = "  Invalid input";
            showQaPopup = true;
        }
    }

    public void updatePcsQty() {
        fc.print("======= barcode process ========" + idCurrentFinished);
        PcsQty oPcsQty = getJpaController3().findPcsQtyCurrentProcessId(idCurrentFinished);
        int sum = Integer.parseInt(oPcsQty.getWipQty()) - Integer.parseInt(inputQtyQa);
        oPcsQty.setWipQty(Integer.toString(sum));
        oPcsQty.setQty(Integer.toString(sum));
        try {
            getJpaController3().edit(oPcsQty);
        } catch (Exception e) {
            fc.print("=== Error not to update ===" + e);
        }

        //insert in qasampling
        getCurrentQaSampling().setDateSampling(new timeCurrent().getDate());
        getCurrentQaSampling().setDeleted(0);
        getCurrentQaSampling().setIdPcsqty(oPcsQty);
        getCurrentQaSampling().setIdSampling(gemId());
        getCurrentQaSampling().setSamplingQty(inputQtyQa);
        getCurrentQaSampling().setUserCreate(userLogin.getSessionUser().getUserName());
        try {
            getQaSamplingJpaController().create(getCurrentQaSampling());
        } catch (Exception e) {
            fc.print("Error not to create" + e);
        }
        //@pang
//        LotControl   objLotControl  = getJpaController().findLotControl(textProcess);
        lotControlId.setWipQty(sum);
        lotControlId.setQtyOutput(sum);
        try {
            getJpaController().edit(lotControlId);
        } catch (Exception e) {
            fc.print("===== Error not to insert wip in lotcontrol :" + e);
        }
        showListDetailProc(lotControlId);
    }

    public void resetDataQa() {
        currentQaSampling = new QaSampling();
        inputQtyQa = "0";
        qaOutPutQty = "0";
    }

    public String gemId() {
        String id = UUID.randomUUID().toString();
        return id;
    }

    public String getQaOutPutQty() {
        return qaOutPutQty;
    }

    public void setQaOutPutQty(String qaOutPutQty) {
        this.qaOutPutQty = qaOutPutQty;
    }

    public String getValidateQaInput() {
        return validateQaInput;
    }

    public void setValidateQaInput(String validateQaInput) {
        this.validateQaInput = validateQaInput;
    }

    public String getInputQtyQa() {
        return inputQtyQa;
    }

    public void setInputQtyQa(String inputQtyQa) {
        this.inputQtyQa = inputQtyQa;
    }

    public boolean getShowQaPopup() {
        return showQaPopup;
    }

    public void setShowQaPopup(boolean showQaPopup) {
        this.showQaPopup = showQaPopup;
    }

    public String getModelDetials() {
        return modelDetials;
    }

    public void setModelDetials(String modelDetials) {
        this.modelDetials = modelDetials;
    }

    public List<ProcList> getProcessListData() {
        return processListData;
    }

    public void setProcessListData(List<ProcList> processListData) {
        this.processListData = processListData;
    }

    public String getInputQty() {
        return inputQty;
    }

    public void setInputQty(String inputQty) {
        this.inputQty = inputQty;
    }

    public CurrentProcess getIdCurrentFinished() {
        return idCurrentFinished;
    }

    public void setIdCurrentFinished(CurrentProcess idCurrentFinished) {
        this.idCurrentFinished = idCurrentFinished;
    }

    public QaSampling getCurrentQaSampling() {
        if (currentQaSampling == null) {
            currentQaSampling = new QaSampling();
        }
        return currentQaSampling;
    }

    public void setCurrentQaSampling(QaSampling currentQaSampling) {
        this.currentQaSampling = currentQaSampling;
    }

    public String getQaBarcodeLink() {
        return qaBarcodeLink;
    }

    public void setQaBarcodeLink(String qaBarcodeLink) {
        this.qaBarcodeLink = qaBarcodeLink;
    }

    public String getQaProcessButtonShow() {
        return qaProcessButtonShow;
    }

    public void setQaProcessButtonShow(String qaProcessButtonShow) {
        this.qaProcessButtonShow = qaProcessButtonShow;
    }

    public boolean isQaProcessButton() {
        return qaProcessButton;
    }

    public void setQaProcessButton(boolean qaProcessButton) {
        this.qaProcessButton = qaProcessButton;
    }

    public MasterDefaultData getCurrentMasterDefaultData() {
        return currentMasterDefaultData;
    }

    public void setCurrentMasterDefaultData(MasterDefaultData currentMasterDefaultData) {
        this.currentMasterDefaultData = currentMasterDefaultData;
    }

    public String getNameWorkSheet() {
        return nameWorkSheet;
    }

    public void setNameWorkSheet(String nameWorkSheet) {
        this.nameWorkSheet = nameWorkSheet;
    }
    public void setOpenProcessButtonShow(String openProcessButtonShow) {
        this.openProcessButtonShow = openProcessButtonShow;
    }

    public String getOpenProcessButtonShow() {
        return openProcessButtonShow;

    }

    public void setCloseProcessButtonShow(String closeProcessButtonShow) {
        this.closeProcessButtonShow = closeProcessButtonShow;
    }

    public String getCloseProcessButtonShow() {
        return closeProcessButtonShow;
    }

    public void setMcDownTimeButtonShow(String mcDownTimeButtonShow) {
        this.mcDownTimeButtonShow = mcDownTimeButtonShow;
    }

    public String getMcDownTimeButtonShow() {
        return mcDownTimeButtonShow;
    }

    public void setHoldTimeButtonShow(String holdTimeButtonShow) {
        this.holdTimeButtonShow = holdTimeButtonShow;
    }

    public String getHoldTimeButtonShow() {
        return holdTimeButtonShow;
    }

    public void setStopTimeButtonShow(String stopTimeButtonShow) {
        this.stopTimeButtonShow = stopTimeButtonShow;
    }

    public String getStopTimeButtonShow() {
        return stopTimeButtonShow;
    }

    public void setMoveProcessButtonShow(String moveProcessButtonShow) {
        this.moveProcessButtonShow = moveProcessButtonShow;
    }

    public String getMoveProcessButtonShow() {
        return moveProcessButtonShow;
    }

}