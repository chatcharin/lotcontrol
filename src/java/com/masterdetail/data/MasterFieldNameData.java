/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.masterdetail.data;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 19, 2012, Time : 4:15:53 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class MasterFieldNameData {
    private String index;
    private String name;
    private boolean report;
    private boolean request;
    private String  indexDetail;

    public MasterFieldNameData(String index, String name, boolean report, boolean request, String indexDetail) {
        this.index = index;
        this.name = name;
        this.report = report;
        this.request = request;
        this.indexDetail = indexDetail;
    }

    
    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getReport() {
        return report;
    }

    public void setReport(boolean report) {
        this.report = report;
    }

    public boolean getRequest() {
        return request;
    }

    public void setRequest(boolean request) {
        this.request = request;
    }

    public String getIndexDetail() {
        return indexDetail;
    }

    public void setIndexDetail(String indexDetail) {
        this.indexDetail = indexDetail;
    }
    
}
