/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.masterdetail.data;

/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 19, 2012, Time : 1:37:43 PM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class MasterDetailData {
    private int   numberAuto;
    private String indexFildName;
    private String nameFildName;
    private String description;

    public MasterDetailData(int numberAuto, String indexFildName, String nameFildName, String description) {
        this.numberAuto = numberAuto;
        this.indexFildName = indexFildName;
        this.nameFildName = nameFildName;
        this.description = description;
    } 
    public String getIndexFildName() {
        return indexFildName;
    }

    public void setIndexFildName(String indexFildName) {
        this.indexFildName = indexFildName;
    }

    public String getNameFildName() {
        return nameFildName;
    }

    public void setNameFildName(String nameFildName) {
        this.nameFildName = nameFildName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumberAuto() {
        return numberAuto;
    }

    public void setNumberAuto(int numberAuto) {
        this.numberAuto = numberAuto;
    }
    
}
