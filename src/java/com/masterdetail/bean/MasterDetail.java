/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.masterdetail.bean;

import com.appCinfigpage.bean.fc;
import com.tct.data.DetailSpecial;
import com.tct.data.jpa.DetailSpecialJpaController;
import com.appCinfigpage.bean.userLogin;
import com.masterdetail.data.MasterDetailData;
import com.masterdetail.data.MasterFieldNameData;
import com.tct.data.FieldName;
import com.tct.data.jpa.FieldNameJpaController;
import com.time.timeCurrent;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;
import org.eclipse.persistence.internal.jpa.parsing.jpql.antlr.JPQLParser;
import com.tct.data.DetailFieldLeft;
import com.tct.data.jpa.DetailFieldLeftJpaController;
import com.tct.data.DetailFieldRight;
import com.tct.data.jpa.DetailFieldRightJpaController;
import com.tct.data.jsf.util.PaginationHelper;
import java.awt.event.ActionEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import org.icefaces.ace.model.table.LazyDataModel;
/**
 *
 * Machine User @author pang
 * Development by Chusak laojang
 * Date : Sep 19, 2012, Time : 11:32:45 AM 
 * Copy Right 4 Xtreme Co.,Ltd.
 */
public class MasterDetail { 
    private FieldNameJpaController   fieldNameJpaController = null;
    private List<SelectItem>  itemFieldName; 
    private List<MasterDetailData> listTableDetailSpecail;
    private List<MasterFieldNameData>  tableLeftData;
    private List<MasterFieldNameData> tableRightData;
    private DetailSpecialJpaController detailSpecialJpaController = null;
    private DetailSpecial  currentDetailSpecail;
    private FieldName  cuurentFieldName =null;
    private DetailFieldLeft  currentDetialFieldLetf ;
    private DetailFieldLeftJpaController detailFieldLeftJpaController = null;
    private DetailFieldRight currentDetailFieldRight ;
    private DetailFieldRightJpaController detailFieldRightJpaController = null;
    private PaginationHelper worksheetpagination;
    private DataModel worksheet;
    private DataModel detialFieldLeft;
    private DataModel detailFieldRight;
    private List<DetailSpecial> objDetail ;
    private String nameMasterWorsheetSearch ;
    private String idMasterDetail;
    private List<MasterFieldNameData>  masterDetailDefaultLeft;
    private List<MasterFieldNameData>  masterDetailDefaultRight;
    
    public MasterDetail() {
        //loadFildNameAll();
        loadDataListAll();
    }
    public DetailFieldRightJpaController getDetailFieldRightJpaController()
    {
        if(detailFieldRightJpaController == null)
        {
            detailFieldRightJpaController = new DetailFieldRightJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailFieldRightJpaController;
    }
    public DetailFieldLeftJpaController getDetailFieldLeftJpaController()
    {
        if(detailFieldLeftJpaController  == null)
        {   
            detailFieldLeftJpaController  = new DetailFieldLeftJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailFieldLeftJpaController;
    }
    public FieldNameJpaController getFieldNameJpaController()
    {
        if(fieldNameJpaController == null)
        {
             fieldNameJpaController  =  new FieldNameJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return fieldNameJpaController;
    }
    public DetailSpecialJpaController getDetailSpecialJpaController()
    {
        if(detailSpecialJpaController ==null)
        {
            detailSpecialJpaController  = new DetailSpecialJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
        }
        return detailSpecialJpaController;
    }
    
    public void changeToInsert()
    {
        resetDataAll();
        loadFildNameAll(); 
        //@pang 12/10/2012
        //add default master detail
        loadMasterDetailDefaultLeft();
        loadMasterDetailDefaultRight();
        idMasterDetail  = gemId();
        
        String link = "pages/setting/masterdetail/createview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
    }
    //input type item 
    public List<SelectItem> inputType()
    {
        List<SelectItem>   inputTypeItem  = new  ArrayList<SelectItem>();
        inputTypeItem.add(new SelectItem("null",""));
        for(DetailSpecial objList : getDetailSpecialJpaController().findAlltNotDelete())
        {
            inputTypeItem.add(new SelectItem(objList.getId(),objList.getName()));
        }
        return inputTypeItem ; 
    }
    
    public void loadMasterDetailDefaultLeft()
    {
        fc.print("=========== Load master detial default left");
        String  masterDetailDefaulLefttList[]  = {"Order no","Product code name","Model","Product Qty","Plant","Remark"};
        boolean  report[] ={true,true,true,true,true,false} ;
        boolean  request[] = {true,true,true,true,true,false};
        
        for(int i =0 ; i< masterDetailDefaulLefttList.length;i++)
        {
            getMasterDetailDefaultLeft().add(new MasterFieldNameData(gemId(),masterDetailDefaulLefttList[i], request[i],report[i], idMasterDetail));
        }
    }
    public void loadMasterDetailDefaultRight()
    {
        fc.print("========== load master detail default rihgt");
        String masterDetailDefaultRightList[]  = {"Product type","Product code","Series","Lot controp qty","Order type"};
        boolean  report[] ={true,false,true,false,true,true,true,true} ;
        boolean  request[] = {true,true,true,true,true,true,true,true};
        
        for(int i = 0 ; i < masterDetailDefaultRightList.length;i++)
        {
            getMasterDetailDefaultRight().add(new MasterFieldNameData(gemId(),masterDetailDefaultRightList[i],request[i],report[i], idMasterDetail));
        }
    }
    public void changeToSearch()
    {
        resetDataAll();
        fc.print("======== Chage to search===========");
        if(nameMasterWorsheetSearch!=null&& !nameMasterWorsheetSearch.trim().isEmpty())
        {
            searchByNameWorkSheet();
            fc.print("Search by name");
        }
        else
        {
            loadDataListAll();
        }
    }
    public void searchByNameWorkSheet()
    {
        objDetail  = getDetailSpecialJpaController().findByNameNotDelete(nameMasterWorsheetSearch);
        int num =0;
        for(DetailSpecial objList:objDetail)
        {   num++;
            getListTableDetailSpecail().add(new MasterDetailData(num,objList.getId(),objList.getName(),objList.getDescrition()));
        }
        setWorksheet(); 
        nameMasterWorsheetSearch ="";
    }
    public void changeToCreate()
    {
        getCurrentDetailSpecail().setId(idMasterDetail);
        getCurrentDetailSpecail().setUserCreate(userLogin.getSessionUser().getUserName());
        getCurrentDetailSpecail().setDateCreate(new timeCurrent().getDate());
        getCurrentDetailSpecail().setInputType(idMasterDetail);
        getCurrentDetailSpecail().setDeleted(0); 
        createDetailSpecial();
        if(!getTableLeftData().isEmpty()){
            for(MasterFieldNameData objList:getTableLeftData()){
                getCurrentDetialFieldLetf().setId(gemId());
                cuurentFieldName = new FieldName();
                cuurentFieldName = getFieldNameJpaController().findFieldName(objList.getIndex());
                fc.print("curent file name : "+cuurentFieldName);
                getCurrentDetialFieldLetf().setIdFieldName(cuurentFieldName);
                getCurrentDetialFieldLetf().setIdDetailSpecail(getCurrentDetailSpecail());
                if(objList.getRequest()){
                    getCurrentDetialFieldLetf().setRequest(1);
                }
                else
                {
                    getCurrentDetialFieldLetf().setRequest(0);
                }
                if(objList.getReport())
                {
                    getCurrentDetialFieldLetf().setReport(1);
                }
                else
                {
                    getCurrentDetialFieldLetf().setReport(0);
                } 
                getCurrentDetialFieldLetf().setDeleted(0);
                createDetailFieldLeft();
            }
        }
        if(!getTableRightData().isEmpty())
        {
            for(MasterFieldNameData objList:getTableRightData()){
                cuurentFieldName = new FieldName();
                cuurentFieldName = getFieldNameJpaController().findFieldName(objList.getIndex());
                getCurrentDetailFieldRight().setId(gemId());
                getCurrentDetailFieldRight().setDeleted(0);
                if(objList.getReport()){
                    getCurrentDetailFieldRight().setReport(1);
                }else{
                    getCurrentDetailFieldRight().setReport(0);
                }
                if(objList.getRequest())
                {
                    getCurrentDetailFieldRight().setRequest(1);
                }
                else
                {
                    getCurrentDetailFieldRight().setRequest(0);
                }
                getCurrentDetailFieldRight().setIdFieldName(getCuurentFieldName());
                getCurrentDetailFieldRight().setIdDetailSpecail(getCurrentDetailSpecail());
                createDetailFielRight();
            }
        }
        resetDataAll();
        loadDataListAll();
        String link = "pages/setting/masterdetail/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
        
    }
    public void resetDataAll()
    {
         worksheetpagination = null;
         idAll  = new ArrayList<String>();
         detailFieldRight = new ListDataModel();
         detialFieldLeft  = new ListDataModel();
         listTableDetailSpecail = new ArrayList<MasterDetailData>(); 
         tableLeftData  = new ArrayList<MasterFieldNameData>();
         tableRightData = new ArrayList<MasterFieldNameData>();
         currentDetailSpecail  = new DetailSpecial(); 
         itemFieldName   = new ArrayList<SelectItem>();
         worksheet    = new ListDataModel();
         detailFieldRight  = new ListDataModel();
         detialFieldLeft   = new ListDataModel();
         idMasterDetail = null;
         masterDetailDefaultLeft = new ArrayList<MasterFieldNameData>();
         masterDetailDefaultRight  = new ArrayList<MasterFieldNameData>();
    }   
    public void createDetailFielRight()
    {
        try
        {
            getDetailFieldRightJpaController().create(getCurrentDetailFieldRight());
        }
        catch(Exception e)
        {
            fc.print("Error not to create detailRight : "+e);
        }
    }        
    public void createDetailFieldLeft()
    {
        try
        {
            getDetailFieldLeftJpaController().create(getCurrentDetialFieldLetf());
            currentDetialFieldLetf  = new DetailFieldLeft();
        }
        catch(Exception e)
        {
            fc.print("Not to create detailField left"+e);
        }
        
    }
    public void changeToCancel()
    {
        resetDataAll();
        loadDataListAll();
        String link = "pages/setting/masterdetail/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
    }
    public String gemId()
    {
        return UUID.randomUUID().toString();
    }
    public void editDetailSpecial()
    {
        try
        {
            getDetailSpecialJpaController().edit(currentDetailSpecail);
        }
        catch(Exception e)
        {
            fc.print("Error not to update ");
        }
    }
    public void changeToSave()
    { 
        editDetailSpecial();
        if(!getTableLeftData().isEmpty()){
            for(MasterFieldNameData objList:getTableLeftData()){  
                    fc.print("current file : "+getCuurentFieldName());
                    if(objList.getIndexDetail().equals("null")){ 
                    cuurentFieldName = new FieldName();
                    cuurentFieldName = getFieldNameJpaController().findFieldName(objList.getIndex());    
                    getCurrentDetialFieldLetf().setId(gemId());    
                    fc.print("curent file name : "+cuurentFieldName);
                    getCurrentDetialFieldLetf().setIdFieldName(cuurentFieldName);
                    getCurrentDetialFieldLetf().setIdDetailSpecail(getCurrentDetailSpecail());
                    if(objList.getRequest()){
                        getCurrentDetialFieldLetf().setRequest(1);
                    }
                    else
                    {
                        getCurrentDetialFieldLetf().setRequest(0);
                    }
                    if(objList.getReport())
                    {
                        getCurrentDetialFieldLetf().setReport(1);
                    }
                    else
                    {
                        getCurrentDetialFieldLetf().setReport(0);
                    } 
                    getCurrentDetialFieldLetf().setDeleted(0);
                    createDetailFieldLeft();
               }
               else
               {
                    currentDetialFieldLetf  = new DetailFieldLeft();
                    currentDetialFieldLetf  = getDetailFieldLeftJpaController().findDetailFieldLeft(objList.getIndexDetail());
                    if(objList.getRequest()){
                        getCurrentDetialFieldLetf().setRequest(1);
                    }
                    else
                    {
                        getCurrentDetialFieldLetf().setRequest(0);
                    }
                    if(objList.getReport())
                    {
                        getCurrentDetialFieldLetf().setReport(1);
                    }
                    else
                    {
                        getCurrentDetialFieldLetf().setReport(0);
                    } 
                    try
                    {
                        getDetailFieldLeftJpaController().edit(getCurrentDetialFieldLetf());
                        
                    }
                    catch(Exception e)
                    {
                        fc.print("Error not to edit"+e);
                    }
               }
                fc.print("List name "+ objList.getName()+" id "+objList.getIndex());
          }
        }
        if(!getTableRightData().isEmpty())
        {
            for(MasterFieldNameData objList:getTableRightData()){
                if(objList.getIndexDetail().equals("null"))
                { 
                
                cuurentFieldName = new FieldName();
                cuurentFieldName = getFieldNameJpaController().findFieldName(objList.getIndex());
                getCurrentDetailFieldRight().setId(gemId());
                getCurrentDetailFieldRight().setDeleted(0);
                if(objList.getReport()){
                    getCurrentDetailFieldRight().setReport(1);
                }else{
                    getCurrentDetailFieldRight().setReport(0);
                }
                if(objList.getRequest())
                {
                    getCurrentDetailFieldRight().setRequest(1);
                }
                else
                {
                    getCurrentDetailFieldRight().setRequest(0);
                }
                getCurrentDetailFieldRight().setIdFieldName(getCuurentFieldName());
                getCurrentDetailFieldRight().setIdDetailSpecail(getCurrentDetailSpecail());
                createDetailFielRight();
                }
               
                else
                {
                    currentDetailFieldRight  = new DetailFieldRight();
                    currentDetailFieldRight  = getDetailFieldRightJpaController().findDetailFieldRight(objList.getIndexDetail());
                    if(objList.getReport()){
                        getCurrentDetailFieldRight().setReport(1);
                    }else{
                        getCurrentDetailFieldRight().setReport(0);
                    }
                    if(objList.getRequest())
                    {
                        getCurrentDetailFieldRight().setRequest(1);
                    }
                    else
                    {
                        getCurrentDetailFieldRight().setRequest(0);
                    }
                    try
                    {
                       getDetailFieldRightJpaController().edit(currentDetailFieldRight);
                    }
                    catch(Exception e)
                    {
                        fc.print("Error not to edit");
                    }
                }
            }
        } 
        
        resetDataAll();
        loadDataListAll();
        String link = "pages/setting/masterdetail/listview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
    }
    public void changeToEdit()
    {
        String  idEdit = getListTableDetailSpecail().get(getWorksheet().getRowIndex()).getIndexFildName();
        currentDetailSpecail = getDetailSpecialJpaController().findById(idEdit);
        findEditTableleftData(currentDetailSpecail);
        findEditTableRightData(currentDetailSpecail);
        loadMasterDetailDefaultLeft();
        loadMasterDetailDefaultRight();
        loadFieldNameAllEdit();
        String link = "pages/setting/masterdetail/editview.xhtml";
        userLogin.getSessionMenuBarBean().setParam(link);
    
    }
    public void delEditTableLeft()
    {
        int index = getDetialFieldLeft().getRowIndex();
        String  idDetialFieldLeft  = getTableLeftData().get(index).getIndexDetail();
        currentDetialFieldLetf   = new DetailFieldLeft();
        if(!idDetialFieldLeft.equals("null")){
            currentDetialFieldLetf  = getDetailFieldLeftJpaController().findDetailFieldLeft(idDetialFieldLeft); 
            getCurrentDetialFieldLetf().setDeleted(1);
            getItemFieldName().add(new SelectItem(null,getCurrentDetailFieldRight().getIdFieldName().getName()));
            deleteFieldNameLeft();  
        }
        getTableLeftData().remove(index);
    }
    public void delEditTableRight()
    {
        int index = getDetailFieldRight().getRowIndex();
         
        String  idDetialFieldRight  = getTableRightData().get(index).getIndexDetail();
        currentDetailFieldRight  = new DetailFieldRight();
        if(!idDetialFieldRight.equals("null")){
            currentDetailFieldRight  = getDetailFieldRightJpaController().findDetailFieldRight(idDetialFieldRight);
            getCurrentDetailFieldRight().setDeleted(1);  
            getItemFieldName().add(new SelectItem(null,getCurrentDetailFieldRight().getIdFieldName().getName()));
            deleteFileNameRight(); 
        }
        getTableRightData().remove(index);
    }
    public void deleteFieldNameLeft()
    {
        try
        {
            getDetailFieldLeftJpaController().edit(getCurrentDetialFieldLetf());
            fc.print("deteted sucess");
            currentDetialFieldLetf   =  new DetailFieldLeft();
        }
        catch(Exception e)
        {
            fc.print("Error not tot delete"+e);
        }
    } 
    public void  deleteFileNameRight()
    {
        try
        {
            getDetailFieldRightJpaController().edit(currentDetailFieldRight);
            currentDetailFieldRight  = new DetailFieldRight();
        }
        catch(Exception e)
        {
            fc.print("Error not tot delete"+e);
        }
    
    } 
    public void findEditTableleftData(DetailSpecial detailSpecial)
    {
        List<DetailFieldLeft>  objlist  = getDetailFieldLeftJpaController().findByDetialSpecial(detailSpecial);
        for(DetailFieldLeft  objlistData : objlist)
        {
            boolean request = false;
            boolean report  = false;
            if(objlistData.getReport()==1)
            {
                report  = true;
            }
            if(objlistData.getRequest()==1)
            {
                request  = true;
            }
            getTableLeftData().add(new MasterFieldNameData(objlistData.getIdFieldName().getId(),objlistData.getIdFieldName().getName(), report, request,objlistData.getId()));
        }
        setDetialFieldLeft();
    }
    public void findEditTableRightData(DetailSpecial detailSpecial)
    {
        List<DetailFieldRight>  objlist  = getDetailFieldRightJpaController().findByDetialSpecial(detailSpecial);
        for(DetailFieldRight  objlistData : objlist)
        {
            boolean request = false;
            boolean report  = false;
            if(objlistData.getReport()==1)
            {
                report  = true;
            }
            if(objlistData.getRequest()==1)
            {
                request  = true;
            }
            getTableRightData().add(new MasterFieldNameData(objlistData.getIdFieldName().getId(),objlistData.getIdFieldName().getName(), report, request,objlistData.getId()));
        }
        setDetailFieldRight();
    }
    public void changeToDelete()
    { 
        String iddel = getListTableDetailSpecail().get(getWorksheet().getRowIndex()).getIndexFildName();
        currentDetailSpecail   = getDetailSpecialJpaController().findById(iddel);
        getCurrentDetailSpecail().setDeleted(1);
        deleteDetailSpacial();
        resetDataAll();
        loadDataListAll();
    }
    public void deleteDetailSpacial()
    {
        try
        {
            getDetailSpecialJpaController().edit(getCurrentDetailSpecail());
        }
        catch(Exception e)
        {
            fc.print("Not to delete detail special");
        }
    }
    public void createDetailSpecial()
    {
        try
        {
            getDetailSpecialJpaController().create(getCurrentDetailSpecail());
        }
        catch(Exception e)
        {
            fc.print("Error not to insert detail specail");
        }
    }
    private void loadFildNameAll() {
        List<FieldName> objField  = getFieldNameJpaController().findAllNotDeleted();
        getItemFieldName().add(new SelectItem("null","---- Select One ----"));
        for(FieldName objList:objField)
        {
            getItemFieldName().add(new SelectItem(objList.getId(),objList.getName()));
        }
        
    }
    private List<String> idAll;

    public List<String> getIdAll() {
        if(idAll ==null)
        {
            idAll  = new ArrayList<String>();
        }
        return idAll;
    }

    public void setIdAll(List<String> idAll) {
        this.idAll = idAll;
    } 
    private void loadFieldNameAllEdit()
    {
        idAll  = new ArrayList<String>();
        for(MasterFieldNameData objlist : getTableLeftData())
        {
            getIdAll().add(objlist.getIndex());
            fc.print("Index " +objlist.getIndex());
        }
        for(MasterFieldNameData objlist : getTableRightData())
        {
            getIdAll().add(objlist.getIndex());
        }
         
        fc.print("id all "+getIdAll());
        if(getIdAll().size()>0){
            List<FieldName> objField  = getFieldNameJpaController().findByAllFieldNotInAndNotDelete(getIdAll());
        
         getItemFieldName().add(new SelectItem("null","---- Select One ----"));
         for(FieldName objList:objField)
         { 
             getItemFieldName().add(new SelectItem(objList.getId(),objList.getName()));
             fc.print("find "+objList.getName());  
         }
        }
        else
        {
            loadFildNameAll();
        }
    }
    public void addLeftField()
    {
          if(!changNameIndex.equals("null")){    
            fc.print("========= Add Left field ==========");
            cuurentFieldName  = getFieldNameJpaController().findFieldName(changNameIndex); 
            getTableLeftData().add(new MasterFieldNameData(cuurentFieldName.getId(),cuurentFieldName.getName(),false,false,"null"));
            destroyItemField(changNameIndex);
          }
          setDetialFieldLeft();
    }
    public void destroyItemField(String idItem)
    {
       for(SelectItem objlist : getItemFieldName())
       {
           if(objlist.getValue().equals(idItem))
           { 
               getItemFieldName().remove(objlist);
               fc.print("destroy item sucess");
               break;
           }
       }
    }        
    public void delListTableLeftData(String idList)
    {
        destroyTableLeftList(idList);
        cuurentFieldName  = getFieldNameJpaController().findFieldName(idList); 
        getItemFieldName().add(new SelectItem(getCuurentFieldName().getId(),getCuurentFieldName().getName()));
        
        
    }
      public void delListTableRightData(String idList)
    {
        destroyTableRighttList(idList);
        cuurentFieldName  = getFieldNameJpaController().findFieldName(idList); 
        getItemFieldName().add(new SelectItem(getCuurentFieldName().getId(),getCuurentFieldName().getName()));
        
    }
    public void destroyTableLeftList(String idListLeftTable)
    {
        for(MasterFieldNameData objlist:getTableLeftData())
        {
            if(objlist.getIndex().equals(idListLeftTable))
            {
                getTableLeftData().remove(objlist);
//                if(!objlist.getIndexDetail().equals("null")){
//                    cuurentFieldName   = new FieldName();
//                    currentDetialFieldLetf   =  getDetailFieldLeftJpaController().findDetailFieldLeft(objlist.getIndexDetail());
//                    deleteFieldNameLeft(); 
//                    
//                }
                fc.print("destroy");
                break;
            }
        }
    }
    public void destroyTableRighttList(String idListRightTable)
    {
        for(MasterFieldNameData objlist:getTableRightData())
        {
            if(objlist.getIndex().equals(idListRightTable))
            {
                getTableRightData().remove(objlist);
                currentDetailFieldRight  = getDetailFieldRightJpaController().findDetailFieldRight(objlist.getIndex());
                if(getCurrentDetailFieldRight().getId()!=null)
                {
                    deleteFileNameRight();
                }
                fc.print("destroy");
                break;
            }
        }
    }
    public void addRightField()
    {
        fc.print("========= Add Right field ========="+changNameIndex);
        if(!changNameIndex.equals("null")){   
            cuurentFieldName  = getFieldNameJpaController().findFieldName(changNameIndex);
            getTableRightData().add(new MasterFieldNameData(cuurentFieldName.getId(),cuurentFieldName.getName(),false,false,"null"));
            destroyItemField(changNameIndex);
        }
        setDetailFieldRight();
    }
    public void requestSelectLeft(ValueChangeEvent event)
    {
        boolean list   = (Boolean) event.getNewValue();   
        int rowNum = getDetialFieldLeft().getRowIndex();
        if(list)
        {   
            if(getTableLeftData().get(rowNum).equals(getDetialFieldLeft().getRowData())){
                getTableLeftData().get(rowNum).setRequest(true);  
            }
        }
        else
        {
           if(getTableLeftData().get(rowNum).equals(getDetialFieldLeft().getRowData())){
                getTableLeftData().get(rowNum).setRequest(false); 
            } 
        }
        fc.print("Value boolean row num :"+ rowNum+"  is "+getTableLeftData().get(rowNum).getRequest());
//        currentDetialFieldLetf.setRequest(requestNum);  //set request .
    }
    public void reportSelectLeft(ValueChangeEvent event)
    {
       boolean report = (Boolean) event.getNewValue();
       int rowNum = getDetialFieldLeft().getRowIndex();
       if(report)
       {
           if(getTableLeftData().get(rowNum).equals(getDetialFieldLeft().getRowData()))
           {
               getTableLeftData().get(rowNum).setReport(true);
           }
       }
       else
       {
           if(getTableLeftData().get(rowNum).equals(getDetialFieldLeft().getRowData()))
           {
               getTableLeftData().get(rowNum).setReport(false);
           }
       }
       fc.print("report status :"+getTableLeftData().get(rowNum).getReport());
    }
    public void requestSelectRight(ValueChangeEvent event)
    {
        boolean check = (Boolean) event.getNewValue();
        int numRow  = getDetailFieldRight().getRowIndex();
        if(check)
        {
            getTableRightData().get(numRow).setRequest(true);
        }
        else
        {
            getTableRightData().get(numRow).setRequest(false);
        }
        fc.print("Request right is "+getTableRightData().get(numRow).getRequest());
    }
    public void reportSelectRight(ValueChangeEvent event)
    {
        boolean check = (Boolean) event.getNewValue();
        int numRow  = getDetailFieldRight().getRowIndex();
        if(check)
        {
            getTableRightData().get(numRow).setReport(true);
        }
        else
        {
            getTableRightData().get(numRow).setReport(false);
        }
        fc.print("Report right is "+getTableRightData().get(numRow).getReport());
    } 
    public DataModel getWorksheet() {
        return worksheet;
    }

    public void setWorksheet() {
        this.worksheet = getWorksheetpagination().createPageDataModel();
    }
    public void previous() {
        getWorksheetpagination().previousPage();
        setWorksheet();
    }

    public void next() {
        getWorksheetpagination().nextPage();
        setWorksheet();
    }

    public int getAllPages() {
        int all = getWorksheetpagination().getItemsCount();
        int pagesize = getWorksheetpagination().getPageSize();
        return ((int) all / pagesize) + 1;
    } 
    public PaginationHelper getWorksheetpagination() {
        if (worksheetpagination == null) {
            worksheetpagination = new PaginationHelper(20) {

                @Override
                public int getItemsCount() {
                    return getListTableDetailSpecail().size();
                }

                @Override
                public DataModel createPageDataModel() {
                    fc.print("PageFirst : " + getPageFirstItem());
                    fc.print("PageLase : " + getPageLastItem());
                    ListDataModel listmodel;
                    if (getObjDetail().size() > 0) {
                        listmodel = new ListDataModel(getListTableDetailSpecail().subList(getPageFirstItem(), getPageLastItem() + 1));
                    } else {
                        listmodel = new ListDataModel(getListTableDetailSpecail());
                    }
                    return listmodel;
                }
            };
        }
        return worksheetpagination;
    }  
    
    
    
    private  String changNameIndex = null;

    public String getChangNameIndex() {
        return changNameIndex;
    }

    public void setChangNameIndex(String changNameIndex) {
        this.changNameIndex = changNameIndex;
    }
 
    public void attributeDetailValueChange(ValueChangeEvent event)
    {
        changNameIndex  = (String) event.getNewValue();
        fc.print("value change : "+changNameIndex); 
    }
    
    public void loadDataListAll()
    {
        objDetail  = getDetailSpecialJpaController().findAlltNotDelete();
        int num =0;
        for(DetailSpecial objList:objDetail)
        {  
            num++;
            getListTableDetailSpecail().add(new MasterDetailData(num,objList.getId(),objList.getName(),objList.getDescrition()));
        }
         setWorksheet();  
    }
    public List<SelectItem> getItemFieldName() {
        if(itemFieldName == null)
        {
            itemFieldName = new ArrayList<SelectItem>();
        }
        return itemFieldName;
    }

    public void setItemFieldName(List<SelectItem> itemFieldName) {
        this.itemFieldName = itemFieldName;
    }

    public List<MasterFieldNameData> getTableLeftData() {
        if(tableLeftData ==null)
        {
            tableLeftData  = new ArrayList<MasterFieldNameData>();
        }
        return tableLeftData;
    }

    public void setTableLeftData(List<MasterFieldNameData> tableLeftData) {
        this.tableLeftData = tableLeftData;
    }

    public List<MasterFieldNameData> getTableRightData() {
        if(tableRightData==null)
        {
            tableRightData  = new ArrayList<MasterFieldNameData>();
        }
        return tableRightData;
    }

    public void setTableRightData(List<MasterFieldNameData> tableRightData) {
        this.tableRightData = tableRightData;
    }

    public DetailSpecial getCurrentDetailSpecail() {
        if(currentDetailSpecail == null)
        {
            currentDetailSpecail  = new DetailSpecial();
        }
        return currentDetailSpecail;
    }

    public void setCurrentDetailSpecail(DetailSpecial currentDetailSpecail) {
        this.currentDetailSpecail = currentDetailSpecail;
    }

    public List<MasterDetailData> getListTableDetailSpecail() {
        if(listTableDetailSpecail==null)
        {
            listTableDetailSpecail  = new ArrayList<MasterDetailData>();
        }
        return listTableDetailSpecail;
    }

    public void setListTableDetailSpecail(List<MasterDetailData> listTableDetailSpecail) {
        this.listTableDetailSpecail = listTableDetailSpecail;
    }

    public List<DetailSpecial> getObjDetail() {
        if(objDetail ==null)
        {
            objDetail = new ArrayList<DetailSpecial>();
        }
        return objDetail;
    }

    public void setObjDetail(List<DetailSpecial> objDetail) {
        this.objDetail = objDetail;
    }

    public FieldName getCuurentFieldName() {
        return cuurentFieldName;
    }

    public void setCuurentFieldName(FieldName cuurentFieldName) {
        this.cuurentFieldName = cuurentFieldName;
    }

    public DetailFieldLeft getCurrentDetialFieldLetf() {
        if(currentDetialFieldLetf  == null)
        {
            currentDetialFieldLetf  = new DetailFieldLeft();    
        }
        return currentDetialFieldLetf;
    }

    public void setCurrentDetialFieldLetf(DetailFieldLeft currentDetialFieldLetf) {
        this.currentDetialFieldLetf = currentDetialFieldLetf;
    }

    public DetailFieldRight getCurrentDetailFieldRight() {
        if(currentDetailFieldRight == null)
        {
            currentDetailFieldRight  = new DetailFieldRight();
        }
        return currentDetailFieldRight;
    }

    public void setCurrentDetailFieldRight(DetailFieldRight currentDetailFieldRight) {
        this.currentDetailFieldRight = currentDetailFieldRight;
    }

    public DataModel getDetialFieldLeft() {
        return detialFieldLeft;
    }

    public void setDetialFieldLeft() {
         detialFieldLeft = new ListDataModel(getTableLeftData());
    }

    public DataModel getDetailFieldRight() {
        return detailFieldRight;
    }

    public void setDetailFieldRight() {
        this.detailFieldRight = new ListDataModel(getTableRightData());
    }

    public String getNameMasterWorsheetSearch() {
        return nameMasterWorsheetSearch;
    }

    public void setNameMasterWorsheetSearch(String nameMasterWorsheetSearch) {
        this.nameMasterWorsheetSearch = nameMasterWorsheetSearch;
    }

    public String getIdMasterDetail() {
        return idMasterDetail;
    }

    public void setIdMasterDetail(String idMasterDetail) {
        this.idMasterDetail = idMasterDetail;
    }

    public List<MasterFieldNameData> getMasterDetailDefaultLeft() {
        if(masterDetailDefaultLeft == null)
        {
            masterDetailDefaultLeft  = new ArrayList<MasterFieldNameData>();
        }
        return masterDetailDefaultLeft;
    }

    public void setMasterDetailDefaultLeft(List<MasterFieldNameData> masterDetailDefaultLeft) {
        this.masterDetailDefaultLeft = masterDetailDefaultLeft;
    }

    public List<MasterFieldNameData> getMasterDetailDefaultRight() {
        if(masterDetailDefaultRight == null)
        {
            masterDetailDefaultRight = new ArrayList<MasterFieldNameData>();
        }
        return masterDetailDefaultRight;
    }

    public void setMasterDetailDefaultRight(List<MasterFieldNameData> masterDetailDefaultRight) {
        this.masterDetailDefaultRight = masterDetailDefaultRight;
    } 
}
