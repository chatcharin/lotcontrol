package com.holdprocess;

import com.component.barcodeauto.BarcodeAuto;
import com.shareData.ShareDataOfSession;
import com.tct.data.CurrentProcess;
import com.tct.data.Hold;
import com.tct.data.PcsQty;
import com.tct.data.jpa.CurrentProcessJpaController;
import com.tct.data.jpa.HoldJpaController;
import com.tct.data.jpa.MainDataJpaController;
import com.tct.data.jpa.PcsQtyJpaController;
import com.time.timeCurrent;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.persistence.Persistence;
public class HoldProcess {
	private boolean holdProcessPopup = false;
        private String mcName  = null;
        private String QtyWorkinProcess  = null;
        
        private CurrentProcessJpaController   currentProcessJpaController  = null;
        private MainDataJpaController  mainDataJpaController  = null;
        private HoldJpaController      holdJpaController  = null;
        private PcsQtyJpaController    pcsQtyJpaController  = null;
        
        private CurrentProcess  currentProcess  = null;
        private Hold            currentHold     = null; 
        private PcsQty          currentPcsQty   = null;
        private CurrentProcess  oldcurrentProcesslist  = null;
        private List<SelectItem>  shiftPart     =  null;
        private String shiftUse   = null;
        
	private String typeClose;
	private String visibles;
        private boolean startHoltTime  = false; 
        private boolean stopHoldTime   = false;
        private String  validateShift  = null;
        private String  validateQtyWord = null;
        
        public BarcodeAuto getBarcodeAuto(){  
            FacesContext context = FacesContext.getCurrentInstance();
            return (BarcodeAuto) context.getExternalContext().getSessionMap().get("barcode"); 
        } 
        public ShareDataOfSession getShareData(){
          FacesContext context = FacesContext.getCurrentInstance();
          return   (ShareDataOfSession) context.getExternalContext().getSessionMap().get("sharedata");  
        }
        public CurrentProcessJpaController getCurrentProcessJpaController()
        {
            if(currentProcessJpaController == null)
            {
                currentProcessJpaController  = new CurrentProcessJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
            }
            return currentProcessJpaController;
        }
        public MainDataJpaController getMainDataJpaController()
        {
            if(mainDataJpaController == null)
            {
                mainDataJpaController  = new MainDataJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
            }
            return mainDataJpaController;
        }
        public HoldJpaController getHoldJpaController()
        {
            if(holdJpaController == null)
            {
                holdJpaController  = new HoldJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
            }
            return holdJpaController;
        }
        public  PcsQtyJpaController  getPcsQtyJpaController()
        {
            if(pcsQtyJpaController  == null)
            {
                pcsQtyJpaController  = new PcsQtyJpaController(Persistence.createEntityManagerFactory("tct_projectxPU"));
            }
            return pcsQtyJpaController;
        }
         
	public void closePopup(String typeClose) {
		this.typeClose = typeClose;
		if (this.typeClose.equals("HoldProcessPopup")) {
			holdProcessPopup = false;
                        validateShift  = "";
                        shiftUse       = "0"; 
                        QtyWorkinProcess = null;
                        validateQtyWord  = "";
		} else if (this.typeClose.equals("HoldProcessPopupClose")) {
			holdProcessPopup = false;
                        validateShift  = "";
                        shiftUse       = "0"; 
                        QtyWorkinProcess = null;
                        validateQtyWord  = "";
		}else if(this.typeClose.equals("startHold"))
                {
                    int qtyWorkSheet ;
                    boolean qtyWorkOverInput  = false;
                    boolean check = false;
                    try
                    {
                      qtyWorkSheet = 0;
                      qtyWorkSheet  = Integer.parseInt(QtyWorkinProcess);
                      PcsQty  objInput  =  getPcsQtyJpaController().findPcsQtyCurrentProcessId(oldcurrentProcesslist);
                      if(qtyWorkSheet > Integer.parseInt(objInput.getQty()))
                      {
                         qtyWorkOverInput  =true; 
                      }    
                      else{
                        check  = true;
                      }
                    }
                    catch(Exception e)
                    {
                        System.out.println("==== Qty work not int ====");
                    }
                    
                    if(check){
                    insertDataHoldStart();

                        //========== Set menu
                        getBarcodeAuto().status  = "Holdtime";   
                        getShareData().openProcessButton      = true;
                        getBarcodeAuto().openProcessButtonShow ="open_process_button_disable";
                        getShareData().closeProcessButton        = true;
                        getBarcodeAuto().closeProcessButtonShow    = "close_process_button_disable";
                        getShareData().mcDownTimeButton       = true;
                        getBarcodeAuto().mcDownTimeButtonShow   = "mc_downtime_button_disable";
                        getShareData().holdTimeButton           = true;
                        getBarcodeAuto().holdTimeButtonShow       = "hold_process_button_disable"; 
                        getShareData().stopTimeButton           = true;
                        getBarcodeAuto().stopTimeButtonShow       = "stop_downtime_button_disable";
                        getShareData().moveProcessButton        = true;
                        getBarcodeAuto().moveProcessButtonShow    = "move_mc_button_disable"; 
                        holdProcessPopup = false;
                        startHoltTime  = false;
                        stopHoldTime     = false; 
                        QtyWorkinProcess = null;
                        validateQtyWord  = "";
                    }
                    else
                    {
                        if(qtyWorkOverInput){
                            validateQtyWord = "Qty work letter and equal input qty"; 
                        }
                        else{
                            validateQtyWord  = "Invalid input";
                        }
                    }
                }
                else if(this.typeClose.equals("stopHold"))
                {
                    if(!shiftUse.equals("0")){
                        insertDataStopHold();

                        //========== Set menu
                        getBarcodeAuto().status  = "Open";   
                        getShareData().openProcessButton      = true;
                        getBarcodeAuto().openProcessButtonShow  ="open_process_button_disable";
                        getShareData().closeProcessButton        = true;
                        getBarcodeAuto().closeProcessButtonShow    = "close_process_button_disable";
                        getShareData().mcDownTimeButton       = true;
                        getBarcodeAuto().mcDownTimeButtonShow   = "mc_downtime_button_disable";
                        getShareData().holdTimeButton           = true;
                        getBarcodeAuto().holdTimeButtonShow       = "hold_process_button_disable"; 
                        getShareData().stopTimeButton           = true;
                        getBarcodeAuto().stopTimeButtonShow       = "stop_downtime_button_disable";
                        getShareData().moveProcessButton        = true;
                        getBarcodeAuto().moveProcessButtonShow    = "move_mc_button_disable";
                        holdProcessPopup = false;
                        validateShift    = "";
                    }
                    else
                    {  
                        holdProcessPopup = true;
                        validateShift    = "Select shift";
                    }
                }
	}

	public void openPopupType(String visibles) {
		this.visibles = visibles;
		if (this.visibles.equals("Hold")) {
                        if(!getShareData().holdTimeButton)
			holdProcessPopup = true;
                        loadData();
                        loadStatusHold();

		}

	}

	public boolean isHoldProcessPopup() {
		return holdProcessPopup;
	}
        public void loadData()
        { 
            oldcurrentProcesslist  = getCurrentProcessJpaController().findCurrentProcess(getShareData().mainDataId.getCurrentNumber());
            try{
                mcName = oldcurrentProcesslist.getMcId().getMcName();
            }
            catch(Exception e)
            {
                mcName =""; 
            }   
            //==== Shift part
             shiftPart = new ArrayList<SelectItem>();
             shiftPart.add(new SelectItem("0","------ Select One -------"));
             shiftPart.add(new SelectItem("A","A"));
             shiftPart.add(new SelectItem("B","B"));
             shiftPart.add(new SelectItem("Day","Day"));
             shiftPart.add(new SelectItem("Night","Night"));
        }
        public void loadStatusHold()
        {
            System.out.println("======= Status hold ===========");
            String statusHold = oldcurrentProcesslist.getStatus();
            if(statusHold.equals("Holdtime"))
            {
                  startHoltTime  = false;
                  stopHoldTime   = true;
            }
            else
            {
                  startHoltTime  = true; 
                  stopHoldTime   = false;
            }
        } 
    /**
     * @return the mcName
     */
    public String getMcName() {
        return mcName;
    }

    /**
     * @param mcName the mcName to set
     */
    public void setMcName(String mcName) {
        this.mcName = mcName;
    }

    /**
     * @return the QtyWorkinProcess
     */
    public String getQtyWorkinProcess() {
        return QtyWorkinProcess;
    }

    /**
     * @param QtyWorkinProcess the QtyWorkinProcess to set
     */
    public void setQtyWorkinProcess(String QtyWorkinProcess) {
        this.QtyWorkinProcess = QtyWorkinProcess;
    }
    public void  insertDataHoldStart()
    {
        
       //============ insert current status hold
         
        String  uuidCurrent  = UUID.randomUUID().toString();
        getCurrentProcess().setId(uuidCurrent);
        getCurrentProcess().setMainDataId(getShareData().mainDataId);
        getCurrentProcess().setMcId(oldcurrentProcesslist.getMcId());
        getCurrentProcess().setCurrentProc("0");
        getCurrentProcess().setStatus("Holdtime");
        timeCurrent  timeNow  =  new  timeCurrent();
        getCurrentProcess().setTimeCurrent(timeNow.getDate());
        getCurrentProcess().setShift(oldcurrentProcesslist.getShift());
        getCurrentProcess().setLine(oldcurrentProcesslist.getLine());
        getCurrentProcess().setHr(oldcurrentProcesslist.getHr());
        try
        {
            getCurrentProcessJpaController().create(getCurrentProcess());
        }
        catch(Exception e )
        {
            System.out.println("===== Error not to create new curret process"+e);
        }
       
        
        
       //============ update maindata current number 
       getShareData().mainDataId.setCurrentNumber(getCurrentProcess().getId());
       try
       {
           getMainDataJpaController().edit(getShareData().mainDataId);
       }
       catch(Exception e)
       {
           System.out.println("======= Error not to update "+e);
       }
       //============ create  hold
       String uuidHold  =  UUID.randomUUID().toString();
       getCurrentHold().setId(uuidHold);
       getCurrentHold().setCurrentProcessId(getCurrentProcess());
       getCurrentHold().setStatus("Start");
       getCurrentHold().setQty(QtyWorkinProcess);
       getCurrentHold().setRefIdHoldStart(null);
       try
       {
           getHoldJpaController().create(getCurrentHold());
       }
       catch(Exception e)
       {
           System.out.println("====== Error not to create  hold start");
       }
       
    }
    public  CurrentProcess getCurrentProcess()
    {
        if(currentProcess == null)
        {
            currentProcess  = new CurrentProcess();
        }
        return currentProcess;
    }
    public Hold  getCurrentHold()
    {
        if(currentHold  == null)
        {
            currentHold  = new Hold();
        }
        return currentHold;
    }

    /**
     * @return the shiftPart
     */
    public List<SelectItem> getShiftPart() {
        return shiftPart;
    }

    /**
     * @param shiftPart the shiftPart to set
     */
    public void setShiftPart(List<SelectItem> shiftPart) {
        this.shiftPart = shiftPart;
    }
     public void ChangShiftPart(ValueChangeEvent event) {
            //System.out.println("================== Change mc =======================");
            setShiftUse((String) event.getNewValue());  
            if(shiftUse.equals("0"))
            {
                validateShift   = "Select shift";
            }
            else
            {
                validateShift   = "";
            }    
//            PrintOut(mcNameUse);
        }

    /**
     * @return the shiftUse
     */
    public String getShiftUse() {
        return shiftUse;
    }

    /**
     * @param shiftUse the shiftUse to set
     */
    public void setShiftUse(String shiftUse) {
        this.shiftUse = shiftUse;
    }
    public void insertDataStopHold()
    {
        currentPcsQty   = null;
        currentProcess  = null; 
        currentHold     = null;
         
        System.out.println("========== Shitf "+shiftUse);
        List<CurrentProcess>  currentOpenQty  = getCurrentProcessJpaController().findCurrentStatusAndOpen(getShareData().mainDataId,"Open");
        String currentQtyNew  = null;
        for(CurrentProcess  currentQty : currentOpenQty)
        {   
            if(currentQty.getCurrentProc().equals("1"))
            {
                //currentQtyNew  = currentQty.ge
                System.out.println("Id    "+currentQty.getId());
                PcsQty   pcsqty  =  getPcsQtyJpaController().findPcsQtyCurrentProcessId(currentQty); 
                currentQtyNew  =  pcsqty.getQty();
                currentQty.setCurrentProc("0");
                try
                {
                    getCurrentProcessJpaController().edit(currentQty);
                }
                catch(Exception e)
                {
                    System.out.println("=== Error not update");
                }
                
                break; 
            }
        }  
        //========= Update crrent proc is 0
        getShareData().mainDataId.getCurrentNumber();
        CurrentProcess   oldCurrentProcess  =  getCurrentProcessJpaController().findCurrentProcess(getShareData().mainDataId.getCurrentNumber());
        oldCurrentProcess.setCurrentProc("0");
        try
        {
            getCurrentProcessJpaController().edit(oldCurrentProcess);
        }
        catch(Exception e)
        {
            System.out.println("=====Error not to update current proc "+e);
        } 
        //========= create new curren processs  and set status is open
        String  uuidCurrentProcessNew  =  UUID.randomUUID().toString();
        getCurrentProcess().setId(uuidCurrentProcessNew);
        getCurrentProcess().setMainDataId(getShareData().mainDataId);
        getCurrentProcess().setMcId(oldCurrentProcess.getMcId());
        getCurrentProcess().setCurrentProc("1");
        getCurrentProcess().setStatus("Open");
        timeCurrent  timeNow   =   new timeCurrent();
        getCurrentProcess().setTimeCurrent(timeNow.getDate());
        getCurrentProcess().setShift(shiftUse);
        getCurrentProcess().setLine(oldCurrentProcess.getLine());
        getCurrentProcess().setHr(oldCurrentProcess.getHr()); 
        try
        {
            getCurrentProcessJpaController().create(getCurrentProcess());
        }
        catch(Exception e)
        {
            System.out.println("==== Error  not to create new current process "+e);
        }
        
        //========= Update Current number in main data
        getShareData().mainDataId.setCurrentNumber(getCurrentProcess().getId());
        try
        {
            getMainDataJpaController().edit(getShareData().mainDataId);
        }
        catch(Exception e)
        {
            System.out.println("===== Error not to update current number "+e);
        }
        //========= create Pcsqty 
        String uuidPcsqty   =  UUID.randomUUID().toString();
        getCurrentPcsQty().setId(uuidPcsqty);
        getCurrentPcsQty().setWipQty("0");
        getCurrentPcsQty().setCurrentProcessId(getCurrentProcess());
        getCurrentPcsQty().setQtyType("in");
        getCurrentPcsQty().setQty(currentQtyNew);
        try
        {
             getPcsQtyJpaController().create(getCurrentPcsQty());
        }
        catch(Exception e)
        {
            System.out.println("===== Error not to create new record pcsqty"+e);
        } 
        //========= create hold new record
        String  uuidHold  =  UUID.randomUUID().toString();
        getCurrentHold().setId(uuidHold);
        getCurrentHold().setCurrentProcessId(getCurrentProcess());
        getCurrentHold().setStatus("Stop");
        Hold   oldHold    = getHoldJpaController().findHoldByIdCurrentProcess(oldCurrentProcess); 
        getCurrentHold().setQty(oldHold.getQty());
        getCurrentHold().setRefIdHoldStart(oldCurrentProcess.getId());
        try
        {
            getHoldJpaController().create(getCurrentHold());
        }
        catch(Exception e)
        {
            System.out.println("==== Error not to create  new hold");
        } 
         
    }      
    public PcsQty  getCurrentPcsQty()
    {
        if(currentPcsQty == null)
        {
            currentPcsQty   =  new PcsQty();
        }
        return currentPcsQty  ;
    }

    /**
     * @return the startHoltTime
     */
    public boolean isStartHoltTime() {
        return startHoltTime;
    }

    /**
     * @param startHoltTime the startHoltTime to set
     */
    public void setStartHoltTime(boolean startHoltTime) {
        this.startHoltTime = startHoltTime;
    }

    /**
     * @return the stopHoldTime
     */
    public boolean isStopHoldTime() {
        return stopHoldTime;
    }

    /**
     * @param stopHoldTime the stopHoldTime to set
     */
    public void setStopHoldTime(boolean stopHoldTime) {
        this.stopHoldTime = stopHoldTime;
    }

    /**
     * @return the validateShift
     */
    public String getValidateShift() {
        return validateShift;
    }

    /**
     * @param validateShift the validateShift to set
     */
    public void setValidateShift(String validateShift) {
        this.validateShift = validateShift;
    }

    /**
     * @return the validateQtyWord
     */
    public String getValidateQtyWord() {
        return validateQtyWord;
    }

    /**
     * @param validateQtyWord the validateQtyWord to set
     */
    public void setValidateQtyWord(String validateQtyWord) {
        this.validateQtyWord = validateQtyWord;
    }
    
}
