/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
  
function loop(started,end,dataUtilityAll,lineAll)
{ 
    var ii = 0;  
    var ended   = started+end;
    for(var start = started; start < ended ;start++)
    { 
          
         var ff = Flotr.draw($('container'+start), [
                    {label: "Target", color: "#00A8F0", data:lineAll[ii], xaxis: 1, yaxis: 1}
                    , 
                    {label: "Result", color: "#C0D800", data:dataUtilityAll[ii], bars: {"show": true}, xaxis: 1, yaxis: 1}
        ]);
        ii++;
    }
    
}
//Acm chart auto
function loopDrawAcmChart(start,end,dataUtlility,dataLine)
{
  var ii=0;  
//  alert(dataUtlility[0]);
//  alert(dataLine[0])
//  alert(start);
//  alert(end); 
  for(var started = start ; started <= end ; started++)
  {
       var ff = Flotr.draw($('container_'+started), [
                    {label: "Target", color: "#00A8F0", data:dataLine[ii], xaxis: 1, yaxis: 1}
                    , 
                    {label: "Result", color: "#C0D800", data:dataUtlility[ii], bars: {"show": true}, xaxis: 1, yaxis: 1}
        ]);
        ii++;
  }
} 
function loopDramAcmChartDefaultPage(start,end,dataUtlility,dataLine)
{
    var ii=0;
    for(var started = start;started <= end;started++)
    {
        var position  = ii % 2;  //display position  chart of acm
        //alert(position);
        var ff = Flotr.draw($('container_'+position+'_'+started), [
                    {label: "Target", color: "#00A8F0", data:dataLine[ii], xaxis: 1, yaxis: 1}
                    , 
                    {label: "Result", color: "#C0D800", data:dataUtlility[ii], bars: {"show": true}, xaxis: 1, yaxis: 1}
        ]);
        ii++;
        
    }
    
}
 

